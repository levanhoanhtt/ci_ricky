ALTER TABLE `customerconsults` ADD `PromotionId` INT(11) NOT NULL AFTER `OrderChannelId`;
ALTER TABLE `customerconsults` ADD `TotalPrice` DOUBLE NOT NULL AFTER `PromotionId`;
ALTER TABLE `customerconsults` ADD `DiscountPrice` DOUBLE NOT NULL AFTER `TotalPrice`;