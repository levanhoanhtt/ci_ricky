ALTER TABLE `transports` CHANGE COLUMN `PendingStatusId` `PendingStatusId` SMALLINT NOT NULL  ;
CREATE  TABLE `storecirculationtransports` (
  `StoreCirculationTransportId` INT NOT NULL AUTO_INCREMENT ,
  `StoreCirculationId` INT NOT NULL ,
  `TransportCode` VARCHAR(15) NOT NULL ,
  `TransportStatusId` TINYINT NOT NULL ,
  `PendingStatusId` SMALLINT NOT NULL ,
  `TransportTypeId` SMALLINT NOT NULL ,
  `TransporterId` SMALLINT NOT NULL ,
  `Tracking` VARCHAR(15) NOT NULL ,
  `Weight` INT NOT NULL ,
  `ShipCost` INT NOT NULL ,
  `Comment` VARCHAR(650) NOT NULL ,
  `CancelReasonId` SMALLINT NULL ,
  `CancelComment` VARCHAR(650) NULL ,
  `CrUserId` INT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  `UpdateUserId` INT NULL ,
  `UpdateDateTime` DATETIME NULL ,
  PRIMARY KEY (`StoreCirculationTransportId`) );
CREATE TABLE `storecirculationtransportcomments` (
  `StoreCirculationTransportCommentId` int(11) NOT NULL AUTO_INCREMENT,
  `StoreCirculationTransportId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Comment` varchar(250) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  PRIMARY KEY (`StoreCirculationTransportCommentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `transactioninternals` ADD COLUMN `BankToText` VARCHAR(250) NOT NULL  AFTER `BankId` , ADD COLUMN `PayerId` INT NOT NULL  AFTER `TreasurerId` ;