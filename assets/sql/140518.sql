ALTER TABLE `usertimekeepings` CHANGE COLUMN `TimeIn` `DateTimeIn` DATETIME NOT NULL  , CHANGE COLUMN `TimeOut` `DateTimeOut` DATETIME NULL DEFAULT NULL  ;
CREATE  TABLE `guaranteeimagegroups` (
  `GuaranteeImageGroupId` SMALLINT NOT NULL AUTO_INCREMENT ,
  `GuaranteeImageGroupName` VARCHAR(250) NOT NULL ,
  `StatusId` TINYINT NOT NULL ,
  PRIMARY KEY (`GuaranteeImageGroupId`) );
  CREATE  TABLE `guaranteeimages` (
  `GuaranteeImageId` INT NOT NULL AUTO_INCREMENT ,
  `Image` VARCHAR(250) NOT NULL ,
  `GuaranteeImageGroupId` SMALLINT NOT NULL ,
  `UserId` INT NOT NULL ,
  `Comment` VARCHAR(250) NOT NULL ,
  `CrDateTime` DATETIME NOT NULL
  PRIMARY KEY (`GuaranteeImageId`) );
