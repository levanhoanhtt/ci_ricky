CREATE TABLE `usertimekeepingregisters` (
  `UserTimeKeepingRegisterId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `KeepingDate` date NOT NULL,
  `DateTimeIn` datetime NOT NULL,
  `DateTimeOut` datetime DEFAULT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `Duration` smallint(6) NOT NULL,
  `KeepingTypeId` tinyint(4) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`UserTimeKeepingRegisterId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `transactionbusiness` (
  `TransactionBusinessId` int(11) NOT NULL AUTO_INCREMENT,
  `TransactionTypeId` tinyint(4) NOT NULL,
  `TransactionKindId` smallint(6) NOT NULL,
  `TransactionStatusId` TINYINT NOT NULL,
  `PaidCost` int(11) NOT NULL,
  `Comment` varchar(650) NOT NULL,
  `TransactionDate` DATE NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`TransactionBusinessId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;