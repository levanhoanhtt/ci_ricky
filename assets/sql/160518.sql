ALTER TABLE `importproducts` ADD COLUMN `GuaranteeMonth` TINYINT NOT NULL  AFTER `Quantity` ;
CREATE  TABLE `importproducterrors` (
  `ImportProductErrorId` INT NOT NULL AUTO_INCREMENT ,
  `ImportId` INT NOT NULL ,
  `ProductId` INT NOT NULL ,
  `ProductChildId` INT NOT NULL ,
  `Quantity` SMALLINT NOT NULL ,
  `Comment` VARCHAR(650) NOT NULL ,
  PRIMARY KEY (`ImportProductErrorId`) );
  ALTER TABLE `usertimekeepingregisters` ADD `Shift` VARCHAR(50) NOT NULL AFTER `KeepingTypeId`;
