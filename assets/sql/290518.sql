ALTER TABLE `customerconsults` ADD COLUMN `CommentCancel` VARCHAR(250) NULL  AFTER `OrderId` , ADD COLUMN `CommentComplete` VARCHAR(250) NULL  AFTER `CommentCancel` ;
ALTER TABLE `storeusers` ADD COLUMN `StoreUserTypeId` TINYINT NOT NULL  AFTER `UserId` ;
CREATE  TABLE `consultusercomments` (
  `ConsultUserCommentId` INT NOT NULL AUTO_INCREMENT ,
  `CustomerConsultUserId` INT NOT NULL ,
  `Comment` VARCHAR(250) NOT NULL ,
  `PercentLog` VARCHAR(250) NOT NULL ,
  `CrUserId` INT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  PRIMARY KEY (`ConsultUserCommentId`) );
  CREATE  TABLE `tablelogs` (
  `TableLogId` INT NOT NULL AUTO_INCREMENT ,
  `ItemId` INT NOT NULL,
  `TableName` VARCHAR(250) NOT NULL ,
  `PrimaryKeyName` VARCHAR(250) NOT NULL,
  `StatusIdOld` TINYINT NOT NULL ,
  `IsBack` TINYINT NOT NULL ,
  `Comment` VARCHAR(250) NOT NULL ,
  `CrUserId` INT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  PRIMARY KEY (`TableLogId`) );