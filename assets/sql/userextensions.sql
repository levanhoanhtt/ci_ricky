/*
 Navicat Premium Data Transfer

 Source Server         : all_db
 Source Server Type    : MySQL
 Source Server Version : 100136
 Source Host           : localhost:3306
 Source Schema         : ricky

 Target Server Type    : MySQL
 Target Server Version : 100136
 File Encoding         : 65001

 Date: 12/12/2018 10:59:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for userextensions
-- ----------------------------
DROP TABLE IF EXISTS `userextensions`;
CREATE TABLE `userextensions`  (
  `UserExtensionId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `ExtId` int(11) NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime(0) NOT NULL,
  PRIMARY KEY (`UserExtensionId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of userextensions
-- ----------------------------
INSERT INTO `userextensions` VALUES (1, 5, 246, 2, 0, '0000-00-00 00:00:00');
INSERT INTO `userextensions` VALUES (2, 6, 258, 2, 0, '0000-00-00 00:00:00');
INSERT INTO `userextensions` VALUES (3, 7, 257, 2, 0, '0000-00-00 00:00:00');
INSERT INTO `userextensions` VALUES (4, 8, 256, 2, 0, '0000-00-00 00:00:00');
INSERT INTO `userextensions` VALUES (5, 9, 255, 2, 0, '0000-00-00 00:00:00');
INSERT INTO `userextensions` VALUES (6, 10, 254, 2, 0, '0000-00-00 00:00:00');
INSERT INTO `userextensions` VALUES (8, 12, 252, 2, 0, '0000-00-00 00:00:00');
INSERT INTO `userextensions` VALUES (9, 13, 251, 2, 0, '0000-00-00 00:00:00');
INSERT INTO `userextensions` VALUES (10, 14, 250, 2, 0, '0000-00-00 00:00:00');
INSERT INTO `userextensions` VALUES (11, 15, 244, 2, 0, '0000-00-00 00:00:00');
INSERT INTO `userextensions` VALUES (12, 16, 238, 2, 0, '0000-00-00 00:00:00');
INSERT INTO `userextensions` VALUES (15, 19, 235, 2, 0, '0000-00-00 00:00:00');
INSERT INTO `userextensions` VALUES (16, 21, 267, 2, 0, '0000-00-00 00:00:00');
INSERT INTO `userextensions` VALUES (17, 1, 234, 2, 2, '2018-11-01 15:43:52');
INSERT INTO `userextensions` VALUES (18, 4, 296, 2, 1, '2018-11-01 16:29:06');
INSERT INTO `userextensions` VALUES (19, 23, 297, 2, 2, '2018-11-07 10:07:33');
INSERT INTO `userextensions` VALUES (20, 24, 298, 2, 2, '2018-11-07 10:08:24');
INSERT INTO `userextensions` VALUES (21, 25, 299, 2, 2, '2018-11-07 10:08:40');
INSERT INTO `userextensions` VALUES (22, 26, 300, 2, 2, '2018-11-07 10:08:55');
INSERT INTO `userextensions` VALUES (23, 27, 306, 2, 2, '2018-11-07 10:09:11');
INSERT INTO `userextensions` VALUES (24, 28, 307, 2, 2, '2018-11-07 10:09:24');
INSERT INTO `userextensions` VALUES (25, 29, 308, 2, 2, '2018-11-07 10:09:39');
INSERT INTO `userextensions` VALUES (26, 31, 309, 2, 2, '2018-11-07 10:09:53');
INSERT INTO `userextensions` VALUES (27, 32, 310, 2, 2, '2018-11-07 10:10:04');
INSERT INTO `userextensions` VALUES (28, 33, 311, 2, 2, '2018-11-07 10:10:30');
INSERT INTO `userextensions` VALUES (29, 34, 312, 2, 2, '2018-11-07 10:10:47');
INSERT INTO `userextensions` VALUES (30, 16, 313, 2, 2, '2018-11-07 10:11:00');
INSERT INTO `userextensions` VALUES (31, 35, 314, 2, 2, '2018-11-07 11:50:02');
INSERT INTO `userextensions` VALUES (32, 38, 454, 2, 0, '0000-00-00 00:00:00');

SET FOREIGN_KEY_CHECKS = 1;
