ALTER TABLE `orderpromotions` DROP COLUMN `PromotionTypeId` ;
ALTER TABLE `customergroups` ADD COLUMN `CustomerKindId` TINYINT NOT NULL  AFTER `CustomerGroupName` ;
UPDATE customergroups SET CustomerKindId = 1;
CREATE  TABLE `ricky`.`transportimages` (
  `TransportImageId` INT NOT NULL AUTO_INCREMENT ,
  `TransportId` INT NOT NULL ,
  `Image` VARCHAR(250) NOT NULL ,
  `TransportStatusId` TINYINT NOT NULL ,
  PRIMARY KEY (`TransportImageId`) );