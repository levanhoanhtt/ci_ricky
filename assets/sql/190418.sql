ALTER TABLE `ricky`.`customers` ADD COLUMN `CTVId` INT NOT NULL  AFTER `IsReceiveAd` ;
ALTER TABLE `ricky`.`orders` DROP COLUMN `ShareDateTime` , DROP COLUMN `ShareUserId` , DROP COLUMN `ShareComment` , DROP COLUMN `IsShare` , DROP COLUMN `StaffId` ;
ALTER TABLE `ricky`.`orders` ADD COLUMN `CTVId` INT NOT NULL  AFTER `CancelComment` ;