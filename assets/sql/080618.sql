CREATE  TABLE `smslogs` (
  `SMSLogId` INT NOT NULL AUTO_INCREMENT ,
  `TranId` VARCHAR(45) NOT NULL ,
  `TotalSMS` SMALLINT NOT NULL ,
  `TotalPrice` INT NOT NULL ,
  `InvalidPhones` TEXT NOT NULL ,
  `SendJson` TEXT NOT NULL ,
  `ResponseJson` TEXT NOT NULL ,
  `SendPhones` TEXT NOT NULL ,
  `SendContent` VARCHAR(250) NOT NULL ,
  `SMSTypeId` TINYINT NOT NULL ,
  `SenderName` VARCHAR(45) NOT NULL ,
  `StatusName` VARCHAR(45) NOT NULL ,
  `ErrorCode` VARCHAR(3) NOT NULL ,
  `ErrorMessage` VARCHAR(650) NOT NULL,
  `CrUserId` INT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  PRIMARY KEY (`SMSLogId`) );
  CREATE  TABLE `sendsmsstatus` (
  `SendSMSStatusId` INT NOT NULL AUTO_INCREMENT ,
  `SMSLogId` INT NOT NULL ,
  `TranId` VARCHAR(45) NOT NULL ,
  `ResponseJson` TEXT NOT NULL ,
  `StatusName` VARCHAR(45) NOT NULL ,
  `ErrorCode` VARCHAR(3) NOT NULL ,
  `ErrorMessage` VARCHAR(650) NOT NULL ,
  `CrUserId` INT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  PRIMARY KEY (`SendSMSStatusId`) );
  CREATE  TABLE `deliverysmsstatus` (
  `DeliverySMSStatusId` INT NOT NULL AUTO_INCREMENT ,
  `SMSLogId` INT NOT NULL ,
  `TranId` VARCHAR(45) NOT NULL ,
  `PhoneNumber` VARCHAR(45) NOT NULL ,
  `StatusId` SMALLINT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  PRIMARY KEY (`DeliverySMSStatusId`) );
  CREATE  TABLE `incomingsms` (
  `IncomingSmsId` INT NOT NULL AUTO_INCREMENT ,
  `PhoneNumber` VARCHAR(45) NOT NULL ,
  `SMSContent` VARCHAR(250) NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  PRIMARY KEY (`IncomingSmsId`) );
  ALTER TABLE `customergroups` ADD COLUMN ` CustomerGroupCode` VARCHAR(45) NOT NULL  AFTER `CustomerGroupId` , ADD COLUMN `CrUserId` INT NOT NULL  AFTER `FilterId` , ADD COLUMN `CrDateTime` DATETIME NOT NULL  AFTER `CrUserId` , ADD COLUMN `UpdateUserId` INT NULL  AFTER `CrDateTime` , ADD COLUMN `UpdateDateTime` DATETIME NULL  AFTER `UpdateUserId` , CHANGE COLUMN `StatusId` `StatusId` TINYINT(4) NOT NULL  AFTER `CustomerKindId` , CHANGE COLUMN `Conditions` `FilterId` SMALLINT NOT NULL  ;
CREATE  TABLE `customergroupcustomers` (
  `CustomerGroupCustomerId` INT NOT NULL AUTO_INCREMENT ,
  `CustomerId` INT NOT NULL ,
  `CustomerGroupId` SMALLINT NOT NULL ,
  PRIMARY KEY (`CustomerGroupCustomerId`) );
  CREATE  TABLE `smscampaigns` (
  `SMSCampaignId` INT NOT NULL AUTO_INCREMENT ,
  `SMSCampaignName` VARCHAR(250) NOT NULL ,
  `SMSCampaignDesc` VARCHAR(650) NOT NULL ,
  `StatusId` TINYINT NOT NULL ,
  `CustomerGroupId` SMALLINT NOT NULL ,
  `CrUserId` INT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  PRIMARY KEY (`SMSCampaignId`) );
ALTER TABLE `smslogs` ADD COLUMN `SMSCampaignId` INT NOT NULL  AFTER `ResponseJson` ;
