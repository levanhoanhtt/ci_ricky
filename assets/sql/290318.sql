-- drop table `quantitychanges`;
CREATE TABLE `pricechanges` (
  `PriceChangeId` int(11) NOT NULL AUTO_INCREMENT,
  `ProductId` int(11) NOT NULL,
  `ProductChildId` int(11) NOT NULL,
  `Price` int(11) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`PriceChangeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `pricechangelogs` (
  `PriceChangeLogId` int(11) NOT NULL AUTO_INCREMENT,
  `ProductId` int(11) NOT NULL,
  `ProductChildId` int(11) NOT NULL,
  `ImportId` int(11) NOT NULL,
  `OldPrice` int(11) NOT NULL,
  `Price` int(11) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  PRIMARY KEY (`PriceChangeLogId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE  TABLE `promotiontypes` (
  `PromotionTypeId` SMALLINT NOT NULL AUTO_INCREMENT ,
  `PromotionTypeName` VARCHAR(250) NOT NULL ,
  `StatusId` TINYINT NOT NULL ,
  PRIMARY KEY (`PromotionTypeId`) );

  ALTER TABLE `orderpromotions` ADD COLUMN `PromotionTypeId` SMALLINT NOT NULL  AFTER `DiscountPercent` ;

  ALTER TABLE `orderproducts` DROP COLUMN `DiscountReason` , DROP COLUMN `IsChildren` , CHANGE COLUMN `OriginalPrice` `PriceCapital` INT(11) NOT NULL  ;
ALTER TABLE `orders` DROP COLUMN `OrderParentId` ;