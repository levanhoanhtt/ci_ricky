CREATE  TABLE `productaccessories` (
  `ProductAccessoryId` INT NOT NULL AUTO_INCREMENT ,
  `ProductId` INT NOT NULL ,
  `AccessoryName` VARCHAR(250) NOT NULL ,
  PRIMARY KEY (`ProductAccessoryId`) );
CREATE  TABLE `guarantees` (
  `GuaranteeId` INT NOT NULL AUTO_INCREMENT ,
  `GuaranteeCode` VARCHAR(45) NOT NULL ,
  `GuaranteeStep` TINYINT NOT NULL ,
  `GuaranteeImage` VARCHAR(250) NOT NULL ,
  `CustomerId` INT NOT NULL ,
  `ReceiptTypeId` SMALLINT NOT NULL ,
  `PaymentTypeId` SMALLINT NOT NULL ,
  `ShipCost` INT NOT NULL ,
  `CrUserId` INT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  `UpdateUserId` INT NULL ,
  `UpdateDateTime` DATETIME NULL ,
  PRIMARY KEY (`GuaranteeId`) );
  CREATE  TABLE `ricky`.`guaranteesteps` (
  `GuaranteeStepId` INT NOT NULL AUTO_INCREMENT ,
  `GuaranteeId` INT NOT NULL ,
  `GuaranteeStep` TINYINT NOT NULL ,
  `CrUserId` INT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  PRIMARY KEY (`GuaranteeStepId`) );
CREATE  TABLE `guaranteeproducts` (
  `GuaranteeProductId` INT NOT NULL AUTO_INCREMENT ,
  `OrderId` INT NOT NULL ,
  `ProductId` INT NOT NULL ,
  `ProductChildId` INT NOT NULL ,
  `ProductImage` VARCHAR(250) NOT NULL ,
  `ProductAccessoryIds` VARCHAR(250) NOT NULL ,
  `UsageStatusId` SMALLINT NOT NULL ,
  `Comment` VARCHAR(250) NOT NULL ,
  `CrUserId` INT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  `UpdateUserId` INT NULL ,
  `UpdateDateTime` DATETIME NULL ,
  PRIMARY KEY (`GuaranteeProductId`) );
  CREATE TABLE `usagestatus` (
  `UsageStatusId` smallint(6) NOT NULL AUTO_INCREMENT,
  `UsageStatusName` varchar(250) NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  PRIMARY KEY (`UsageStatusId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `guaranteecomments` (
  `GuaranteeCommentId` int(11) NOT NULL AUTO_INCREMENT,
  `GuaranteeId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Comment` varchar(250) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  PRIMARY KEY (`GuaranteeCommentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;