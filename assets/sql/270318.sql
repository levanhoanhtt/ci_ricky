ALTER TABLE `importproducts` ADD COLUMN `Price` INT NOT NULL  AFTER `Quantity` , ADD COLUMN `VAT` TINYINT NOT NULL  AFTER `Price` ;
ALTER TABLE `importproducts` ADD COLUMN `AveragePrice` INT NOT NULL  AFTER `VAT` ;
ALTER TABLE `imports` ADD COLUMN `TransportCost` INT NOT NULL  AFTER `StoreId` , ADD COLUMN `DiscountCost` INT NOT NULL  AFTER `TransportCost` , ADD COLUMN `PaymentCost` INT NOT NULL  AFTER `DiscountCost`;
CREATE TABLE `importservices` (
  `ImportServiceId` int(11) NOT NULL AUTO_INCREMENT,
  `ImportId` int(11) NOT NULL,
  `OtherServiceId` int(11) NOT NULL,
  `ServiceCost` int(11) NOT NULL,
  PRIMARY KEY (`ImportServiceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;