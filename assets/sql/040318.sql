CREATE  TABLE `ricky`.`quantitychanges` (
  `QuantityChangeId` INT NOT NULL AUTO_INCREMENT ,
  `ProductId` INT NOT NULL ,
  `ProductChildId` INT NOT NULL ,
  `StoreId` SMALLINT NOT NULL ,
  PRIMARY KEY (`QuantityChangeId`) );