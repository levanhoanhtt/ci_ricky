ALTER TABLE `ricky`.`customerconsults` ADD COLUMN `CustomerConsultCode` VARCHAR(45) NULL  AFTER `CustomerConsultId` ;
UPDATE customerconsults SET CustomerConsultCode = CONCAT('TVL-', CustomerConsultId);
UPDATE customerconsults SET RemindStatusId = 2 WHERE RemindStatusId = 1;