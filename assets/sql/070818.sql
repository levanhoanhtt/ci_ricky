CREATE  TABLE ricky.itemcontents (
  ItemContentId INT NOT NULL AUTO_INCREMENT ,
  FieldName VARCHAR(250) NOT NULL ,
  FieldValue TEXT NOT NULL ,
  FileUrl VARCHAR(250) NOT NULL ,
  ContentTypeId TINYINT NOT NULL ,
  DisplayOrder SMALLINT NOT NULL ,
  EffectId INT NOT NULL ,
  CrUserId INT NOT NULL ,
  CrDateTime DATETIME NOT NULL ,
  UpdateUserId INT NULL ,
  UpdateDateTime DATETIME NULL ,
  PRIMARY KEY (ItemContentId) );
