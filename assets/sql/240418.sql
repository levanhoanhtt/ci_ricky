ALTER TABLE `usagestatus` ADD COLUMN `UsageStatusTypeId` TINYINT NOT NULL  AFTER `StatusId` , ADD COLUMN `ParentUsageStatusId` SMALLINT NOT NULL  AFTER `UsageStatusTypeId` ;
ALTER TABLE `guaranteeproducts` ADD COLUMN `UsageStatusId2` SMALLINT NOT NULL COMMENT 'tinh trang su dung 1'  AFTER `UsageStatusId1` , ADD COLUMN `UsageStatusId3` SMALLINT NOT NULL COMMENT 'tinh trang su dung 2'  AFTER `UsageStatusId2` , CHANGE COLUMN `UsageStatusId` `UsageStatusId1` SMALLINT(6) NOT NULL COMMENT 'hinh thuc'  ;
CREATE  TABLE `guaranteesolutions` (
  `GuaranteeSolutionId` SMALLINT NOT NULL AUTO_INCREMENT ,
  `GuaranteeSolutionName` VARCHAR(250) NOT NULL ,
  `StatusId` TINYINT NOT NULL ,
  `ParentGuaranteeSolutionId` SMALLINT NOT NULL,
  PRIMARY KEY (`GuaranteeSolutionId`) );