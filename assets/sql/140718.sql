CREATE TABLE `offsetreasons` (
  `OffsetReasonId` smallint(6) NOT NULL AUTO_INCREMENT,
  `OffsetReasonName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  PRIMARY KEY (`OffsetReasonId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
CREATE TABLE `offsettypes` (
  `OffsetTypeId` smallint(6) NOT NULL AUTO_INCREMENT,
  `OffsetTypeName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  PRIMARY KEY (`OffsetTypeId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
CREATE  TABLE `ricky`.`ordermetas` (
  `OrderMetaId` INT NOT NULL AUTO_INCREMENT ,
  `OrderId` INT NOT NULL ,
  `OffsetReasonId` SMALLINT NOT NULL ,
  `OffsetTypeId` SMALLINT NOT NULL ,
  `CancelReasonId` SMALLINT NOT NULL ,
  `CancelComment` VARCHAR(250) NOT NULL ,
  PRIMARY KEY (`OrderMetaId`) );
