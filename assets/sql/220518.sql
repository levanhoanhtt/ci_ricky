CREATE  TABLE `productformalstatus` (
  `ProductFormalStatusId` SMALLINT NOT NULL AUTO_INCREMENT ,
  `ProductFormalStatusName` VARCHAR(250) NOT NULL ,
  `StatusId` TINYINT NOT NULL ,
  PRIMARY KEY (`ProductFormalStatusId`) );
  CREATE  TABLE `productusagestatus` (
  `ProductUsageStatusId` SMALLINT NOT NULL AUTO_INCREMENT ,
  `ProductUsageStatusName` VARCHAR(250) NOT NULL ,
  `StatusId` TINYINT NOT NULL ,
  PRIMARY KEY (`ProductUsageStatusId`) );
  CREATE  TABLE `productaccessorystatus` (
  `ProductAccessoryStatusId` SMALLINT NOT NULL AUTO_INCREMENT ,
  `ProductAccessoryStatusName` VARCHAR(250) NOT NULL ,
  `StatusId` TINYINT NOT NULL ,
  PRIMARY KEY (`ProductAccessoryStatusId`) );