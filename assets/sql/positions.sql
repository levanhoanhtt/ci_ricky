/*
 Navicat Premium Data Transfer

 Source Server         : all_db
 Source Server Type    : MySQL
 Source Server Version : 100136
 Source Host           : localhost:3306
 Source Schema         : ricky

 Target Server Type    : MySQL
 Target Server Version : 100136
 File Encoding         : 65001

 Date: 12/12/2018 10:59:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for positions
-- ----------------------------
DROP TABLE IF EXISTS `positions`;
CREATE TABLE `positions`  (
  `PositionId` smallint(6) NOT NULL AUTO_INCREMENT,
  `PositionName` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  PRIMARY KEY (`PositionId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
