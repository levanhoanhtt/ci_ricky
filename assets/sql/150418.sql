ALTER TABLE `roles` ADD COLUMN `PartId` SMALLINT NOT NULL  AFTER `RoleName` ;
CREATE TABLE `funds` (
  `FundId` smallint(6) NOT NULL AUTO_INCREMENT,
  `FundCode` varchar(45) NOT NULL,
  `FundName` varchar(250) NOT NULL,
  `ItemStatusId` tinyint(4) NOT NULL,
  `TreasureId` int(11) NOT NULL,
  `Balance` int(11) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`FundId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `storefunds` (
  `StoreFundId` int(11) NOT NULL AUTO_INCREMENT,
  `StoreId` smallint(6) NOT NULL,
  `FundId` smallint(6) NOT NULL,
  PRIMARY KEY (`StoreFundId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
ALTER TABLE `transactions` ADD `FundId` SMALLINT NOT NULL AFTER `TransportId`;