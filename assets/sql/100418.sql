CREATE  TABLE `transportstatuslogs` (
  `TransportStatusLogId` INT NOT NULL AUTO_INCREMENT ,
  `TransportId` INT NOT NULL ,
  `TransportStatusId` TINYINT NOT NULL ,
  `CrUserId` INT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  PRIMARY KEY (`TransportStatusLogId`) );
ALTER TABLE `transactions` ADD COLUMN `OrderId` INT NOT NULL  AFTER `PaidCost` ;