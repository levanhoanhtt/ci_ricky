CREATE  TABLE `librarysentences` (
  `LibrarySentenceId` INT NOT NULL AUTO_INCREMENT ,
  `LibraryName` VARCHAR(250) NOT NULL ,
  `StatusId` TINYINT NOT NULL ,
  `CrUserId` INT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  PRIMARY KEY (`LibrarySentenceId`) );

  CREATE  TABLE `librarypages` (
  `LibraryPageId` INT NOT NULL AUTO_INCREMENT ,
  `LibrarySentenceId` INT NOT NULL ,
  `FbPageId` SMALLINT NOT NULL ,
  PRIMARY KEY (`LibraryPageId`) );

  CREATE  TABLE `sentences` (
  `SentenceId` INT NOT NULL AUTO_INCREMENT ,
  `SentenceTitle` VARCHAR(250) NOT NULL ,
  `SentenceContent` TINYTEXT NOT NULL ,
  `LibrarySentenceId` INT NOT NULL,
  `SentenceGroupId` SMALLINT NOT NULL ,
  PRIMARY KEY (`SentenceId`) );

  CREATE  TABLE `sentencegroups` (
  `SentenceGroupId` SMALLINT NOT NULL AUTO_INCREMENT ,
  `SentenceGroupName` VARCHAR(250) NOT NULL ,
  PRIMARY KEY (`SentenceGroupId`) );