ALTER TABLE `ricky`.`customerconsults` ADD COLUMN `CustomerInfo` TEXT NOT NULL  AFTER `PhoneNumber` ;
ALTER TABLE `ricky`.`transports` DROP COLUMN `EnhancedService`;
CREATE TABLE `changestatusreasons` (
  `ChangeStatusReasonId` smallint(6) NOT NULL AUTO_INCREMENT,
  `ChangeStatusReasonName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  PRIMARY KEY (`ChangeStatusReasonId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
