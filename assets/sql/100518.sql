ALTER TABLE `ordercomments` ADD COLUMN `CommentTypeId` TINYINT NOT NULL  AFTER `Comment` ;
UPDATE ordercomments SET CommentTypeId = 1;
ALTER TABLE `consultproducts` ADD `Quantity` INT(11) NOT NULL AFTER `Comment`;
ALTER TABLE `consultproducts` ADD `Price` INT(11) NOT NULL AFTER `Quantity`;
ALTER TABLE `customerconsults` ADD COLUMN `OrderChannelId` TINYINT NOT NULL  AFTER `PartId` ;
ALTER TABLE `customerconsults` ADD COLUMN `CTVId` INT NOT NULL  AFTER `OrderChannelId` ;
