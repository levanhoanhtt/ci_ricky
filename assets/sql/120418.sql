ALTER TABLE `groups` ADD COLUMN `Comment` VARCHAR(650) NULL  AFTER `StatusId` , ADD COLUMN `CrUserId` INT NOT NULL  AFTER `Comment` , ADD COLUMN `CrDateTime` DATETIME NOT NULL  AFTER `CrUserId` ;
CREATE  TABLE `usergroups` (
  `UserGroupId` INT NOT NULL AUTO_INCREMENT ,
  `UserId` INT NOT NULL ,
  `GroupId` SMALLINT NOT NULL ,
  PRIMARY KEY (`UserGroupId`) );
ALTER TABLE `banks` ADD COLUMN `DisplayOrder` SMALLINT NOT NULL  AFTER `Balance` ;
UPDATE banks SET DisplayOrder = 1;

ALTER TABLE `users` DROP COLUMN `GroupId` ;