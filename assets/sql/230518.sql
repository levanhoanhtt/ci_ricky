ALTER TABLE `products` CHANGE COLUMN `FormalStatus` `ProductFormalStatusId` SMALLINT NOT NULL  , CHANGE COLUMN `UsageStatus` `ProductUsageStatusId` SMALLINT NOT NULL  , CHANGE COLUMN `AccessoryStatus` `ProductAccessoryStatusId` SMALLINT NOT NULL  ;
ALTER TABLE `productchilds` ADD COLUMN `Comment` VARCHAR(250) NOT NULL  AFTER `ProductAccessoryStatusId` , CHANGE COLUMN `IMEI` `IMEI` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL  , CHANGE COLUMN `FormalStatus` `ProductFormalStatusId` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL  , CHANGE COLUMN `UsageStatus` `ProductUsageStatusId` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL  , CHANGE COLUMN `AccessoryStatus` `ProductAccessoryStatusId` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL  ;
ALTER TABLE `customerconsults` ADD COLUMN `OrderId` INT NOT NULL  AFTER `TimeProcessed` ;
CREATE  TABLE `customerconsultusers` (
  `CustomerConsultUserId` INT NOT NULL AUTO_INCREMENT ,
  `CustomerConsultId` INT NOT NULL ,
  `OrderId` INT NOT NULL,
  `UserId1` INT NOT NULL COMMENT 'ng tao don' ,
  `Percent1` TINYINT NOT NULL ,
  `UserId2` INT NOT NULL ,
  `Percent2` TINYINT NOT NULL ,
  `UserId3` INT NOT NULL ,
  `Percent3` TINYINT NOT NULL ,
  `UserId4` INT NOT NULL ,
  `Percent4` TINYINT NOT NULL ,
  `UpdateDateTime` DATETIME NULL ,
  PRIMARY KEY (`CustomerConsultUserId`) );