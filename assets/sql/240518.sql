ALTER TABLE `products` ADD COLUMN `StatusComment` VARCHAR(250) NOT NULL  AFTER `VideoUrls` ;
CREATE  TABLE `transactionbusinessitems` (
  `TransactionBusinessItemId` INT NOT NULL AUTO_INCREMENT ,
  `TransactionBusinessId` INT NOT NULL ,
  `TransactionKindId` SMALLINT NOT NULL ,
  `TransactionStatusId` TINYINT NOT NULL ,
  `PaidCost` INT NOT NULL ,
  `Comment` VARCHAR(250) NOT NULL ,
  `CrUserId` INT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  `UpdateUserId` INT NULL ,
  `UpdateDateTime` DATETIME NULL ,
  PRIMARY KEY (`TransactionBusinessItemId`) );
  ALTER TABLE `transactionbusiness` DROP COLUMN `Comment` , DROP COLUMN `PaidCost` , DROP COLUMN `TransactionStatusId` , DROP COLUMN `TransactionKindId` ;
