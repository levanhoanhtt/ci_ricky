ALTER TABLE `customerconsultusers` DROP COLUMN `Percent4` , DROP COLUMN `UserId4` , DROP COLUMN `Percent3` , DROP COLUMN `UserId3` , DROP COLUMN `Percent2` , DROP COLUMN `UserId2` , CHANGE COLUMN `UserId1` `UserId` INT(11) NOT NULL COMMENT 'ng tao don'  , CHANGE COLUMN `Percent1` `Percent` TINYINT(4) NOT NULL  , CHANGE COLUMN `StatusId` `CrUserId` INT NOT NULL  , CHANGE COLUMN `UpdateDateTime` `CrDateTime` DATETIME NOT NULL  ;
ALTER TABLE `customerconsultusers` DROP COLUMN `CrUserId` , ADD COLUMN `UpdateDateTime` DATETIME NULL  AFTER `CrDateTime` ;
CREATE  TABLE `consultsells` (
  `ConsultSellId` INT NOT NULL AUTO_INCREMENT ,
  `CustomerConsultId` INT NOT NULL ,
  `OrderId` INT NOT NULL ,
  `StatusId` TINYINT NOT NULL ,
  PRIMARY KEY (`ConsultSellId`) );
ALTER TABLE `customerconsultusers` DROP COLUMN `OrderId` , CHANGE COLUMN `CustomerConsultId` `ConsultSaleId` INT(11) NOT NULL  ;
ALTER TABLE `customerconsultusers` ADD COLUMN `CustomerConsultId` INT NOT NULL  AFTER `ConsultSaleId` ;
ALTER TABLE `customerconsultusers` ADD COLUMN `IsCrOrder` TINYINT NULL  AFTER `UserId` ;
ALTER TABLE `customerconsultusers` CHANGE COLUMN `ConsultSaleId` `ConsultSellId` INT(11) NOT NULL  ;
ALTER TABLE `customerconsultusers` ADD COLUMN `Comment` VARCHAR(650) NOT NULL  AFTER `Percent` ;
drop table `consultusercomments`;