ALTER TABLE `orders` CHANGE COLUMN `OrderCode` `OrderCode` VARCHAR(15) NULL  ;
ALTER TABLE `transactions` CHANGE COLUMN `TransactionCode` `TransactionCode` VARCHAR(45) NULL  ;
ALTER TABLE `reminds` CHANGE COLUMN `RemindCode` `RemindCode` VARCHAR(45) NULL  ;
ALTER TABLE `returngoods` CHANGE COLUMN `ReturnGoodCode` `ReturnGoodCode` VARCHAR(15) NULL  ;
ALTER TABLE `storecirculations` CHANGE COLUMN `StoreCirculationCode` `StoreCirculationCode` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL  ;
ALTER TABLE `storecirculationtransports` CHANGE COLUMN `TransportCode` `TransportCode` VARCHAR(15) NULL  ;
ALTER TABLE `transactioninternals` CHANGE COLUMN `TransactionCode` `TransactionCode` VARCHAR(45) NULL  ;
ALTER TABLE `orders` CHANGE COLUMN `CancelReasonId` `CancelReasonId` SMALLINT(6) NULL  , CHANGE COLUMN `CancelComment` `CancelComment` VARCHAR(250) NULL  , CHANGE COLUMN `IsShare` `IsShare` TINYINT(4) NULL  , CHANGE COLUMN `ShareComment` `ShareComment` VARCHAR(250) NULL  ;

ALTER TABLE `customers` CHANGE COLUMN `PhoneNumber2` `PhoneNumber2` VARCHAR(15) NULL  ;
ALTER TABLE `customers` CHANGE COLUMN `ZipCode` `ZipCode` VARCHAR(45) NULL DEFAULT NULL  ;
