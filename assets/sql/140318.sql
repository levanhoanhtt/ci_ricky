ALTER TABLE `ricky`.`sentences` ADD COLUMN `FileTypeId` TINYINT NOT NULL  AFTER `LibrarySentenceId` ;

ALTER TABLE `storecirculations` DROP COLUMN `HandleDate` , DROP COLUMN `CancelReason` , DROP COLUMN `Comment` , DROP COLUMN `DeliveryTypeId` , DROP COLUMN `OrderStatusId` , CHANGE COLUMN `StatusId` `StoreCirculationStatusId` TINYINT(4) NOT NULL  ;
CREATE TABLE `storecirculationcomments` (
  `StoreCirculationCommentId` int(11) NOT NULL AUTO_INCREMENT,
  `StoreCirculationId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Comment` varchar(250) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  PRIMARY KEY (`StoreCirculationCommentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;