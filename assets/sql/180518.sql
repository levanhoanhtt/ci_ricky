ALTER TABLE `usertimekeepings` ADD COLUMN `CommentOut` VARCHAR(250) NULL  AFTER `CommentIn` , CHANGE COLUMN `Comment` `CommentIn` VARCHAR(250) NOT NULL  ;
ALTER TABLE `products` DROP COLUMN `IsManageExtraWarehouse` ;
ALTER TABLE `products` ADD COLUMN `IMEI` VARCHAR(45) NULL  AFTER `Sku` ;
ALTER TABLE `productchilds` ADD COLUMN `ParentProductChildId` INT NOT NULL  AFTER `GuaranteeMonth` , ADD COLUMN `IMEI` VARCHAR(45) NULL  AFTER `ParentProductChildId` , ADD COLUMN `FormalStatus` VARCHAR(250) NULL  AFTER `IMEI` , ADD COLUMN `UsageStatus` VARCHAR(250) NULL  AFTER `FormalStatus` , ADD COLUMN `AccessoryStatus` VARCHAR(250) NULL  AFTER `UsageStatus` ;

