CREATE  TABLE `productunits` (
  `ProductUnitId` SMALLINT NOT NULL AUTO_INCREMENT ,
  `ProductUnitName` VARCHAR(250) NOT NULL ,
  `StatusId` TINYINT NOT NULL ,
  PRIMARY KEY (`ProductUnitId`) );
  ALTER TABLE `products` ADD COLUMN `ProductUnitId` SMALLINT NOT NULL  AFTER `GuaranteeMonth` ;
  INSERT INTO `productunits` (`ProductUnitId`, `ProductUnitName`, `StatusId`) VALUES (1, 'Cái', 2);
  UPDATE products SET ProductUnitId = 1;