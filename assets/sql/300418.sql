ALTER TABLE `transactionkinds` ADD COLUMN `ParentTransactionKindId` SMALLINT NOT NULL  AFTER `TransactionTypeId` ;
ALTER TABLE `transactions` ADD COLUMN `TransactionKindId` SMALLINT NOT NULL  AFTER `FundId` ;

ALTER TABLE `transactionkinds` ADD COLUMN `TransactionKindTypeId` TINYINT NOT NULL  AFTER `ParentTransactionKindId` ;
UPDATE transactionkinds SET TransactionKindTypeId = 1;
CREATE TABLE `familytransactions` (
  `FamilyTransactionId` int(11) NOT NULL AUTO_INCREMENT,
  `TransactionCode` varchar(45) DEFAULT NULL,
  `TransactionTypeId` tinyint(4) NOT NULL,
  `TransactionStatusId` tinyint(4) NOT NULL,
  `VerifyLevelId` tinyint(4) NOT NULL COMMENT 'cap phe duyet',
  `MoneySourceId` tinyint(4) NOT NULL,
  `MoneyPhoneId` smallint(6) NOT NULL,
  `BankId` smallint(6) NOT NULL COMMENT 'dung cho phieu thu',
  `BankToText` varchar(250) NOT NULL,
  `PaidCost` int(11) NOT NULL,
  `Comment` varchar(650) NOT NULL,
  `TransactionKindId` smallint(6) NOT NULL,
  `PayerName` varchar(250) NOT NULL,
  `PayerPhone` varchar(15) NOT NULL,
  `CrUserId` int(11) NOT NULL COMMENT 'co the la thu quy - ng xuat tien',
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`FamilyTransactionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `transactions` DROP COLUMN `MoneyTypeId` , CHANGE COLUMN `MoneySourceId` `MoneySourceId` TINYINT NOT NULL  ;

