ALTER TABLE `ricky`.`orders` ADD COLUMN `ParentOrderId` INT NOT NULL  AFTER `CTVId` ;
CREATE  TABLE `ricky`.`ordermetas` (
  `OrderMetaId` INT NOT NULL AUTO_INCREMENT ,
  `OrderId` INT NOT NULL ,
  `MetaName` VARCHAR(45) NOT NULL ,
  `MetaValue` VARCHAR(650) NOT NULL ,
  `UpdateUserId` INT NULL ,
  `UpdateDateTime` DATETIME NULL ,
  PRIMARY KEY (`OrderMetaId`) );