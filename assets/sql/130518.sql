ALTER TABLE `customerconsults` ADD COLUMN `Facebook` VARCHAR(100) NOT NULL  AFTER `PhoneNumber` ;
CREATE  TABLE `usertimekeepings` (
  `UserTimeKeepingId` INT NOT NULL AUTO_INCREMENT ,
  `UserId` INT NOT NULL ,
  `KeepingDate` DATE NOT NULL ,
  `TimeIn` TIME NOT NULL ,
  `TimeOut` TIME NULL ,
  `StatusId` TINYINT NOT NULL,
  `Duration` SMALLINT NOT NULL ,
  `KeepingTypeId` TINYINT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  `UpdateDateTime` DATETIME NULL,
  PRIMARY KEY (`UserTimeKeepingId`) );
ALTER TABLE `orderproducts` ADD COLUMN `ProductKindId` TINYINT NOT NULL  AFTER `ProductChildId` ;
UPDATE orderproducts SET ProductKindId = 1;
UPDATE orders SET OrderReasonId = 8 WHERE OrderReasonId = 0;
ALTER TABLE `inventories` ADD COLUMN `IsManual` TINYINT NOT NULL  AFTER `StatusId` ;
UPDATE inventories SET IsManual = 1;