/*
 Navicat Premium Data Transfer

 Source Server         : all_db
 Source Server Type    : MySQL
 Source Server Version : 100136
 Source Host           : localhost:3306
 Source Schema         : ricky

 Target Server Type    : MySQL
 Target Server Version : 100136
 File Encoding         : 65001

 Date: 12/12/2018 10:58:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for exts
-- ----------------------------
DROP TABLE IF EXISTS `exts`;
CREATE TABLE `exts`  (
  `ExtId` int(11) NOT NULL,
  `UserId` int(11) NULL DEFAULT NULL,
  `Caller` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Domain` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `PhoneId` int(11) NULL DEFAULT NULL,
  `PhoneNumber` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `State` tinyint(1) NULL DEFAULT NULL,
  `CreatedAt` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ExtId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of exts
-- ----------------------------
INSERT INTO `exts` VALUES (234, 20043, 'Agent 20043', 'ceo@phone', 'ceocorp.vn', 27, '842871006688', 1, '2018-09-24 16:35:09');
INSERT INTO `exts` VALUES (235, 20044, 'Agent 20044', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-09-24 16:35:09');
INSERT INTO `exts` VALUES (238, 20047, 'Agent 20047', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-09-24 16:35:10');
INSERT INTO `exts` VALUES (244, 20053, 'Agent 20053', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-09-25 16:40:15');
INSERT INTO `exts` VALUES (246, 20055, 'Agent 20055', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-09-25 16:54:14');
INSERT INTO `exts` VALUES (250, 20059, 'Agent 20059', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-09-26 09:06:54');
INSERT INTO `exts` VALUES (251, 20060, 'Agent 20060', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-09-26 10:36:41');
INSERT INTO `exts` VALUES (252, 20061, 'Agent 20061', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-09-26 10:59:10');
INSERT INTO `exts` VALUES (254, 20063, 'Agent 20063', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-09-26 14:46:23');
INSERT INTO `exts` VALUES (255, 20064, 'Agent 20064', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-09-26 14:46:38');
INSERT INTO `exts` VALUES (256, 20065, 'Agent 20065', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-09-26 14:46:52');
INSERT INTO `exts` VALUES (257, 20066, 'Agent 20066', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-09-26 14:52:08');
INSERT INTO `exts` VALUES (258, 20067, 'Agent 20067', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-09-26 16:44:08');
INSERT INTO `exts` VALUES (267, 20071, 'Agent 20071', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-10-01 10:18:11');
INSERT INTO `exts` VALUES (296, 20092, 'Agent 20092', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-10-15 14:31:07');
INSERT INTO `exts` VALUES (297, 20093, 'Agent 20093', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-10-15 14:31:12');
INSERT INTO `exts` VALUES (298, 20094, 'Agent 20094', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-10-15 14:31:17');
INSERT INTO `exts` VALUES (299, 20095, 'Agent 20095', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-10-16 08:24:23');
INSERT INTO `exts` VALUES (300, 20096, 'Agent 20096', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-10-16 09:55:50');
INSERT INTO `exts` VALUES (306, 20097, 'Agent 20097', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-10-16 16:00:10');
INSERT INTO `exts` VALUES (307, 20098, 'Agent 20098', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-10-16 16:00:16');
INSERT INTO `exts` VALUES (308, 20099, 'Agent 20099', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-10-16 16:01:33');
INSERT INTO `exts` VALUES (309, 20100, 'Agent 20100', 'ceo@phone', 'ceocorp.vn', 27, '842871006688', 1, '2018-10-16 17:04:10');
INSERT INTO `exts` VALUES (310, 20101, 'Agent 20101', 'ceo@phone', 'ceocorp.vn', 27, '842871006688', 1, '2018-10-16 17:05:51');
INSERT INTO `exts` VALUES (311, 20102, 'Agent 20102', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-10-17 10:22:44');
INSERT INTO `exts` VALUES (312, 20103, 'Agent 20103', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-10-17 10:28:51');
INSERT INTO `exts` VALUES (313, 20104, 'Agent 20104', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-10-17 10:34:12');
INSERT INTO `exts` VALUES (314, 20105, 'Agent 20105', 'ceo@phone', 'ceocorp.vn', 27, '842871006688', 1, '2018-10-17 10:52:26');
INSERT INTO `exts` VALUES (369, 20158, 'Agent 20158', 'ceo@phone', 'ceocorp.vn', 26, '842471006688', 1, '2018-11-27 16:42:03');
INSERT INTO `exts` VALUES (447, 20211, 'Agent 20211', 'urBO5avSXs', 'ceocorp.vn', 27, '842871006688', 1, '2018-12-09 21:52:49');
INSERT INTO `exts` VALUES (448, 20212, 'Agent 20212', 'S99Dq6MH35', 'ceocorp.vn', 26, '842471006688', 1, '2018-12-10 11:58:18');
INSERT INTO `exts` VALUES (449, 20213, 'Agent 20213', 'Jc5K6Z5yt0', 'ceocorp.vn', 26, '842471006688', 1, '2018-12-10 11:58:55');
INSERT INTO `exts` VALUES (450, 20214, 'Agent 20214', 'dndZ8xO5D3', 'ceocorp.vn', 26, '842471006688', 1, '2018-12-10 12:00:54');
INSERT INTO `exts` VALUES (451, 20215, 'Agent 20215', 'In92iCV0Sn', 'ceocorp.vn', 27, '842871006688', 1, '2018-12-10 13:41:20');
INSERT INTO `exts` VALUES (454, 20218, 'Agent 20218', 'Ac459g8jfv', 'ceocorp.vn', 26, '842471006688', 1, '2018-12-10 17:06:25');

SET FOREIGN_KEY_CHECKS = 1;
