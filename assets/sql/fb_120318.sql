CREATE TABLE `fb_posts_1759192184381061` (
  `FbPostId` int(11) NOT NULL AUTO_INCREMENT,
  `PostId` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `FbPageId` smallint(6) NOT NULL,
  `PostContent` text COLLATE utf8_unicode_ci NOT NULL,
  `FbUserId` int(11) NOT NULL,
  `PostLink` varchar(650) COLLATE utf8_unicode_ci NOT NULL,
  `SenderId` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `SenderName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `CreatedDateTime` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `CrDateTime` datetime NOT NULL,
  PRIMARY KEY (`FbPostId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `fb_comments_1759192184381061` (
  `FbCommentId` int(11) NOT NULL AUTO_INCREMENT,
  `FbPageId` smallint(6) NOT NULL,
  `FbUserId` int(11) NOT NULL,
  `CommentId` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `SenderId` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `SenderName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `FbPostCode` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `FbPostId` int(11) NOT NULL,
  `ParentId` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `ParentCommentId` int(11) NOT NULL,
  `Message` text COLLATE utf8_unicode_ci NOT NULL,
  `ViewStatusId` tinyint(4) NOT NULL,
  `AnswerStatusId` tinyint(4) NOT NULL,
  `CreatedDateTime` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `CrDateTime` datetime NOT NULL,
  PRIMARY KEY (`FbCommentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;