ALTER TABLE `customerconsults` DROP COLUMN `Comments` , DROP COLUMN `ProductChildId` , DROP COLUMN `ProductId` , ADD COLUMN `OutOfDate` TINYINT NOT NULL  AFTER `Comment` , ADD COLUMN `TimeProcessed` TINYINT NOT NULL  AFTER `OutOfDate` , CHANGE COLUMN `ConsultStatusId` `RemindStatusId` TINYINT(4) NOT NULL  ;
CREATE  TABLE `consultproducts` (
  `ConsultProductId` INT NOT NULL AUTO_INCREMENT ,
  `CustomerConsultId` INT NOT NULL ,
  `ProductId` INT NOT NULL ,
  `ProductChildId` INT NOT NULL ,
  `Comment` VARCHAR(650) NOT NULL ,
  PRIMARY KEY (`ConsultProductId`) );
  CREATE  TABLE `consultcomments` (
  `ConsultCommentId` INT NOT NULL AUTO_INCREMENT ,
  `CustomerConsultId` INT NOT NULL ,
  `UserId` INT NOT NULL ,
  `Comment` VARCHAR(250) NOT NULL ,
  `CommentTypeId` TINYINT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  PRIMARY KEY (`ConsultCommentId`) );

  ALTER TABLE `chatstaffs` ADD COLUMN `StaffRoleId` TINYINT NOT NULL  AFTER `StaffId` ;

  ALTER TABLE `customerconsults` ADD COLUMN `UserId` INT NOT NULL  AFTER `RemindStatusId` , ADD COLUMN `PartId` SMALLINT NOT NULL  AFTER `UserId` ;

ALTER TABLE `customerconsults` ADD COLUMN `ConsultTitle` VARCHAR(250) NOT NULL  AFTER `CustomerConsultId` ;
ALTER TABLE `customerconsults` DROP COLUMN `Comment` ;


ALTER TABLE `customerconsults` ADD COLUMN `RemindStatusId` INT NOT NULL  AFTER `ConsultDate`