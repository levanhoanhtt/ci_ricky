CREATE  TABLE `receipttypes` (
  `ReceiptTypeId` SMALLINT NOT NULL AUTO_INCREMENT ,
  `ReceiptTypeName` VARCHAR(250) NOT NULL ,
  `StatusId` TINYINT NOT NULL ,
  PRIMARY KEY (`ReceiptTypeId`) );
  CREATE  TABLE `paymenttypes` (
  `PaymentTypeId` SMALLINT NOT NULL AUTO_INCREMENT ,
  `PaymentTypeName` VARCHAR(250) NOT NULL ,
  `StatusId` TINYINT NOT NULL ,
  PRIMARY KEY (`PaymentTypeId`) );
  ALTER TABLE `guarantees` ADD COLUMN `StoreId` SMALLINT NOT NULL  AFTER `ShipCost` ;
  ALTER TABLE `guaranteeproducts` CHANGE COLUMN `OrderId` `GuaranteeId` INT(11) NOT NULL  ;
  ALTER TABLE `guaranteeproducts` ADD COLUMN `OrderId` INT NOT NULL  AFTER `GuaranteeId` ;
  ALTER TABLE `guaranteeproducts` ADD COLUMN `StatusId` TINYINT NOT NULL  AFTER `Comment` ;
