$(document).ready(function(){
    $("#tbodyChangeStatusReason").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#changeStatusReasonId').val(id);
        $('input#changeStatusReasonName').val($('td#changeStatusReasonName_' + id).text());
        scrollTo('input#changeStatusReasonName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteChangeStatusReasonUrl').val(),
                data: {
                    ChangeStatusReasonId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#changeStatusReason_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#changeStatusReasonForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#changeStatusReasonForm')) {
            var form = $('#changeStatusReasonForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="changeStatusReason_' + data.ChangeStatusReasonId + '">';
                            html += '<td id="changeStatusReasonName_' + data.ChangeStatusReasonId + '">' + data.ChangeStatusReasonName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.ChangeStatusReasonId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.ChangeStatusReasonId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyChangeStatusReason').prepend(html);
                        }
                        else $('td#changeStatusReasonName_' + data.ChangeStatusReasonId).text(data.ChangeStatusReasonName);
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});