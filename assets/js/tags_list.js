$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Tags',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){
        	if (actionCode == 'remove_all_tags') {
        		if(confirm('Bạn có thực sự muốn xóa ?')){
                    if($('select#selectData').val() != 'all') removeTag(JSON.stringify(itemIds));
                    else removeTag($("input#itemTagAllIds").val());
		        }
		        return false;
            }
        }
    });

    $("#tbodyTag").on("click", "a.link_delete", function(){
        if(confirm('Bạn có thực sự muốn xóa ?')){
            var ids =[parseInt($(this).attr('data-id'))];
            removeTag(ids);
        }
        return false;
    })
});

function renderContentTags(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        for (var item = 0; item < data.length; item++) {
            html += '<tr id="trItem_'+data[item].TagId+'">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].TagId + '"></td>';
            html += '<td>'+ data[item].TagName +'</td>';
            html += '<td>'+ data[item].ItemTypeName +'</td>';
            html += '<td><a href="javascript:void(0);" class="link_delete" data-id="'+data[item].TagId+'"><i class="fa fa-trash-o"></i></a></td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="3" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}

function removeTag(ids){
    $.ajax({
        type: "POST",
        url: $('input#deleteTagUrl').val(),
        data: {
            TagId: ids,
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if (json.code == 1) redirect(true,'');
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}