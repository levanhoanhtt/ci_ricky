$(document).ready(function(){
    $("#tbodyGuaranteeImageGroup").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#guaranteeImageGroupId').val(id);
        $('input#guaranteeImageGroupName').val($('td#guaranteeImageGroupName_' + id).text());
        scrollTo('input#guaranteeImageGroupName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteGuaranteeImageGroupUrl').val(),
                data: {
                    GuaranteeImageGroupId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#guaranteeImageGroup_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#guaranteeImageGroupForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#guaranteeImageGroupForm')) {
            var form = $('#guaranteeImageGroupForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="guaranteeImageGroup_' + data.GuaranteeImageGroupId + '">';
                            html += '<td id="guaranteeImageGroupName_' + data.GuaranteeImageGroupId + '">' + data.GuaranteeImageGroupName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.GuaranteeImageGroupId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.GuaranteeImageGroupId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyGuaranteeImageGroup').prepend(html);
                        }
                        else $('td#guaranteeImageGroupName_' + data.GuaranteeImageGroupId).text(data.GuaranteeImageGroupName);
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});