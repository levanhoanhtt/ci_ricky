$(document).ready(function(){
    $('.startDate').datetimepicker({
        format: 'dd/mm/yyyy hh:ii',
    });
    $("body").on('click', '.btnShowDate', function(){
        var startDate = $("input.startDate").val();
        $("input#showTime").val('');
        if(startDate !== ''){
            $.ajax({
                type: "POST",
                url: $('input#urlShowTime').val(),
                data: {
                    StartDate: startDate,
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        $("input.showTime").val(json.data);
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        } else showNotification("Chọn ngày bắt đầu", 0);
        return false; 
    });
});