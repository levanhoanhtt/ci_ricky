var line = 0;

function lineDistance(x, y, x0, y0) {
    return Math.sqrt((x -= x0) * x + (y -= y0) * y);
};

function drawLine(a, b, line) {
    var pointA = $('body ' + a).offset();
    var pointB = $('body ' + b).offset();
    if (typeof(pointA) != 'undefined' && typeof(pointB) != 'undefined') {
        var pointAcenterX = $('body ' + a).width() / 2;
        var pointAcenterY = $('body ' + a).height() / 2;
        var pointBcenterX = $('body ' + b).width() / 2;
        //var pointBcenterY = $('body ' + b).height() / 2;
        var pointTop = pointB.top - pointA.top;
        var pointLeft = pointB.left - pointA.left;
        var angle = Math.atan2(pointTop, pointLeft) * 180 / Math.PI;
        var distance = lineDistance(pointA.left, pointA.top, pointB.left, pointB.top);
        // Set Angle
        $(line).css('transform', 'rotate(' + angle + 'deg)');

        // Set Width
        $(line).css('width', distance + 'px');

        // Set Position
        $(line).css('position', 'absolute');
        if (pointA.top) {
        }
        if (pointB.left < pointA.left) {
            $(line).offset({top: pointA.top + pointAcenterY, left: pointB.left + pointBcenterX});
        } else {
            $(line).offset({top: pointA.top + pointAcenterY, left: pointA.left + pointAcenterX});
        }
    }
}

setInterval(function () {
    $('body .boxLine .line').each(function () {
        var dataStart = $(this).attr('data-start');
        var dataEnd = $(this).attr('data-end');
        var id = $(this).attr('id');
        drawLine('.' + dataStart, '.' + dataEnd, '.' + id);
    })
});

$('#Start').click(function () {
    $('body .orgchart .node').each(function () {
        $(this).addClass('addLine');
        $(this).find('div').addClass('disabledbutton');
    })
    $('#Start').hide();
    $('#Stop').show();
})

$('#Stop').click(function () {
    $('body .orgchart .node').each(function () {
        $(this).removeClass('addLine');
        $(this).find('div').removeClass('disabledbutton');
    })
    $('#Start').show();
    $('#Stop').hide();
})

$('body').on('click', '.addLine', function () {
    var value = $(this).find('.id-node').val();
    var dataCheck = $(this).find('.id-node').attr('data-check');
    var addStart = $('#valueLine').attr('data-start');
    var addEnd = $('#valueLine').attr('data-end');
    // if(dataCheck) {
    //   showNotification('Điểm này bạn đã chọn', 0);
    //   return false;
    // }
    if (!addStart) {
        $('#modalAddPointStart').modal('show');
        $('.savePointStart').attr('data-start', value);
        return false;
    }
    if (!addEnd) {
        $('#modalAddPointEnd').modal('show');
        $('.savePointEnd').attr('data-end', value);
        return false;
    }
})

$('body').on('click', '.savePointStart', function () {
    var value = $(this).attr('data-start');
    $('#valueLine').attr('data-start', value);
    appendDiv(value);
    $('#modalAddPointStart').modal('hide');
});

$('body').on('click', '.savePointEnd', function () {
    var value = $(this).attr('data-end');
    var dataStart = $('#valueLine').attr('data-start');
    appendDiv(value);
    $('.boxLine').append('<span class="line tab' + dataStart + '' + value + '" data-idstart="' + dataStart + '" data-idend="' + value + '" data-start="connect' + dataStart + '" data-end="connect' + value + '" id="tab' + dataStart + '' + value + '"></span>');
    savePositionRelate(dataStart, value);
    $('#valueLine').attr('data-start', '');
    $('#valueLine').attr('data-end', '');
    $("#Stop").trigger("click");
    $('#modalAddPointEnd').modal('hide');
});

function appendDiv(value) {
    $('.id-node').each(function () {
        var valueNode = $(this).val();
        if (valueNode == value) {
            $(this).closest('.disabledbutton').find('.id-node').attr('data-check', 1);
        }
    })
}

function savePositionRelate(BeginPositionId, EndPositionId) {
    $.ajax({
        type: "POST",
        url: $("#savePositionRelateUrl").val(),
        data: {
            BeginPositionId: BeginPositionId,
            EndPositionId: EndPositionId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

//delete line
$('body #OrganizationalStructure').on('click', '.line', function () {
    var idStart = $(this).attr('data-idstart');
    var idEnd = $(this).attr('data-idend');
    $('#modalDeleteRelationship .delete').attr('data-start', idStart);
    $('#modalDeleteRelationship .delete').attr('data-end', idEnd);
    $('#modalDeleteRelationship').modal('show');
})

$('#modalDeleteRelationship .delete').click(function () {
    var BeginPositionId = $(this).attr('data-start');
    var EndPositionId = $(this).attr('data-end');
    $.ajax({
        type: "POST",
        url: $("#deletePositionRelateUrl").val(),
        data: {
            BeginPositionId: BeginPositionId,
            EndPositionId: EndPositionId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if (json.code == 1) {
                $('#modalDeleteRelationship').modal('hide');
                $('body').find('.tab' + BeginPositionId + EndPositionId).remove();
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
})

