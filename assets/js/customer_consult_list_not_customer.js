$(document).ready(function () {
    // dateRangePicker();
    actionItemAndSearch({
        ItemName: 'Danh sách data',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){
        	if(actionCode == 'active_customer'){
        		if($('select#selectData').val() != 'all') $('input#formDataIds').val(JSON.stringify(itemIds));
                else $('input#formDataIds').val($("input#itemTagAllIds").val());
                if (confirm('Bạn có muốn chuyển dữ liệu này thành khách hàng?')) {
                    $.ajax({
                        type: "POST",
                        url: $('#urlUpdateCustomer').val(),
                        data: {
                            ItemIds: $('input#formDataIds').val()
                        },
                        success: function (response) {
                            var json = $.parseJSON(response);
                            showNotification(json.message, json.code);
                            if (json.code == 1) {
                                for (var i = 0; i < itemIds.length; i++) $('#trItem_' + itemIds[i]).remove();
                            }
                        },
                        error: function (response) {
                            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        }
                    });
                }
        	}
        }
    });

});



function renderContentCustomerConsults(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditCustomerConsult = $('#urlEditCustomerConsult').val() + '/';
        var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        var urlInfoCustomer = $('#urlInfoCustomer').val() + '/';
        var trClass = '';
        for (var item = 0; item < data.length; item++) {
            trClass = '';
            if(data[item].RemindDayDiff > 0 && data[item].RemindStatusId == 1) trClass = ' trItemCancel';
            html += '<tr id="trItem_'+data[item].CustomerConsultId+'" class="trItem' + trClass + '">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].CustomerConsultId + '"></td>';
            // html += '<td><a href="' + urlEditCustomerConsult + data[item].CustomerConsultId  + '">DT-' + (10000 + parseInt(data[item].CustomerConsultId)) + '</a></td>';
            html += '<td>'+ getDayText(data[item].CrDayDiff) +  data[item].CrDateTime +'</td>';
            if(data[item].CustomerId != '0') html += '<td class="'+data[item].Color+'"><a href="' + urlEditCustomer + data[item].CustomerConsultId + '">'+ data[item].FullName +'</a></td>';
            else html += '<td class="'+data[item].Color+'"><a href="' + urlInfoCustomer + data[item].CustomerConsultId + '">'+ data[item].FullName +'</a></td>';
            html += '<td>'+ data[item].PhoneNumber +'</td>';
            html += '<td>'+ data[item].CustomerAddress +'</td>';
            html += '<td>'+ data[item].CrFullName +'</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].OrderChannelId] + '">' + data[item].OrderChannel + '</span></td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="10" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
        
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}