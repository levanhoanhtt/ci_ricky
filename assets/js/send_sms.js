$(document).ready(function(){
	var body = $("body");
	var tags = [];
    var inputTag = $('input#phoneNumber');
    inputTag.tagsInput({
        'width': '100%',
		'height': '49px',
        'interactive': true,
        'defaultText': '',
        'onAddTag': function(tag){
            tags.push(tag);
        },
        'onRemoveTag': function(tag){
            var index = tags.indexOf(tag);
            if(index >= 0) tags.splice(index, 1);
        },
        'delimiter': [',', ';'],
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 0
    });
	inputTag.keypress(function(event){
        if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
            event.preventDefault();
    	}
	});
	var $remaining = $('#character_number'),
    $messages = $remaining.next();
	$('#message').keyup(function(){
        $(this).val(convertCharacter($(this).val()));
	    var chars = this.value.length,
	        messages = Math.ceil(chars / 160),
	        remaining = messages * 160 - (chars % (messages * 160) || messages * 160);

	    $remaining.text(remaining);
	    $messages.text(messages);
	});

	$("select#sMSCampaignId").change(function(){
		if($(this).val() == '0') $("div.hide_campaign").show();
		else $("div.hide_campaign").hide();
	});

	$(".submit").click(function(){
		var customerGroupId = $("select#customerGroupId").val();
		if(tags.length == 0 && customerGroupId == '0'){
			showNotification("Vui lòng điền số điện thoại hoặc nhóm khách hàng", 0);
			return false;
		}
		var message = $("#message").val().trim();
		if(message == ""){
			showNotification("Vui lòng nhập tin nhắn cần gửi", 0);
			return false;
		}
		var sMSCampaignId = $("select#sMSCampaignId").val();
		var sMSCampaignName = $("input#sMSCampaignName").val().trim();
		if(sMSCampaignId == '0' && sMSCampaignName == ''){
			showNotification("Tên chiến dịch không được bỏ trống", 0);
			return false;
		}
		$.ajax({
			type: "POST",
			url: $('input#sendSmsUrl').val(),
			data: {
				PhoneNumbers: JSON.stringify(tags),
				CustomerGroupId: customerGroupId,
				Message: message,
				SMSCampaignId: sMSCampaignId,
				SMSCampaignName: sMSCampaignName,
				SMSCampaignDesc: $("#sMSCampaignDesc").val().trim()
			},
			success: function (response) {
				var json = $.parseJSON(response);
				showNotification(json.message, json.code);
				if(json.code == 1) redirect(false, $('a#smsListUrl').attr('href'));
			},
			error: function (response) {
				showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
				
			}
		});
	})

	body.on('click', '#sample_sentences', function(){
        console.log("sadfads")
        var keySearch = "";
        var sentenceGroupId = $("select#sentenceGroupId").val().trim();
        getContentSentences(keySearch,sentenceGroupId);
        $("#modalSampleSentences").modal('show');
    });

    // search-entences
    var statusSearch2 = null;
    body.on('keydown', 'input.search-entences', function(){
        if (statusSearch2 != null) {
            clearTimeout(statusSearch2);
            statusSearch2 = null;
        }
    }).keyup(function (e) {
        if (statusSearch2 == null) {
            statusSearch2 = setTimeout(function () {
                var keySearch = $("input.search-entences").val().trim();
                var sentenceGroupId = $("select#sentenceGroupId").val().trim();
                if(keySearch != ""){
                    getContentSentences(keySearch, sentenceGroupId);
                } 
            }, 500);
        }
    });

    body.on("click", 'select#sentenceGroupId', function(){
        var sentenceGroupId = $(this).val().trim();
        var keySearch = "";
        getContentSentences(keySearch, sentenceGroupId);
    })
    
    body.on('click', '#tbodyContent td', function(){
        var customerCare = $("select#customer_care").val();
        var customerNeedCare = $("select#customers_need_care").val();
        if(customerCare != "" && customerNeedCare != "" ){
            var mapObj = {
               '{u}': customerCare,
               '{i}': customerNeedCare
            };
            var content = $(this).find('p').text();
            var re = new RegExp(Object.keys(mapObj).join("|"),"gi");
            content = content.replace(re, function(matched){
              return mapObj[matched];
            });
            $("textarea#message").val(content);
            $("#modalSampleSentences").modal('hide');
        }else{
            showNotification("Vui lòng chọn ngôi thứ xưng hô", 0);
            return false;
        }

        
    })
});

function convertCharacter(text){
	return text.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g,"o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g,"i");
}

// lẫy nội dung mẫu câu có sẵn ra
function getContentSentences(keySearch, sentenceGroupId){
    $.ajax({
        type: "POST",
        url: $("#getUrlContentSentences").val().trim(),
        data: {
            keySearch : keySearch,
            SentenceGroupId: sentenceGroupId
        },// now data come in this function
        success: function (response) {
        	console.log(response)
            $("#tbodyContent").html('');
            var json =  $.parseJSON(response);
            if(json.code = 1){
                
                var html = "";
                $.each(json.data, function(key, value){
                    html += `
                        <tr>
                            <td><label>${value.SentenceTitle}</label><p>${value.SentenceContent}</p></td>
                        </tr>
                    `;
                })
                $("#tbodyContent").html(html);
                $("input.search-entences").val('');
                // listPostComment(response.data)
            }else showNotification(json.message, json.code);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}