$(document).ready(function(){
    $("body").on("click", "a.addDepartmentType", function(){
        $('#departmentTypeName').val('');
        $('#modalAddDepartmentType').modal('show');
    });

    $('#btnUpdateDepartmentType').click(function(){
        var departmentTypeName = $('#departmentTypeName').val();
        if(departmentTypeName == ''){
            showNotification('Tên không được để trống', 0);
            return false;
        }else {
            $.ajax({
                type: "POST",
                url: $('#updateDepartmentTypeUrl').val(),
                data: {
                    DepartmentTypeName: departmentTypeName,
                    UserId: 	        $('#userLoginId').val()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        showFloor();
                        $('#modalAddDepartmentType').modal('hide');
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
    });
});
