var app = app || {};
app.init = function (totalColumnTable) {
    app.updateTracking();
    app.comment();
    app.hideAndShow();
    app.updateShipCost();
    app.updateTransportStatus();
    app.filter();
    app.configTable(totalColumnTable);
};
$(document).ready(function(){
    // $('#table-data').dragableColumns();
    // $("#table-data").tableHeadFixer({"left": 0});
    // var totalColumnTable = $('#table-data tr th').length;
    // $("input#txtCountColTable").val('Hiển thị '+totalColumnTable+'/'+totalColumnTable)
    actionItemAndSearch({
        ItemName: 'Vận chuyển',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){
            if(actionCode == 'print_order'){
                if($('select#selectData').val() != 'all') $('input#transportIds').val(JSON.stringify(itemIds));
                else $('input#transportIds').val($("input#itemTagAllIds").val());
                $('#printForm').attr('action', $('input#printOrderMultiple').val()).submit();
            }
            else if(actionCode == 'print_transport'){
                if($('select#selectData').val() != 'all') $('input#transportIds').val(JSON.stringify(itemIds));
                else $('input#transportIds').val($("input#itemTagAllIds").val());
                $('#printForm').attr('action', $('input#printTransportMultiple').val()).submit();
            }
            else if(actionCode == 'export_transport'){
                if($('select#selectData').val() != 'all') $('input#transportIds').val(JSON.stringify(itemIds));
                else $('input#transportIds').val($("input#itemTagAllIds").val());
                $('#printForm').attr('action', $('input#exportTransport').val()).submit();
            }
            else if(actionCode == 'change_status'){
                if($('select#selectData').val() != 'all') $('input#transportIds').val(JSON.stringify(itemIds));
                else $('input#transportIds').val($("input#itemTagAllIds").val());
                $('#modalTransportStatus').modal('show');
            }
        }
    });
    $('#btnUpdateTransportStatus').click(function(){
        var btn = $(this);
        btn.prop('disabled', true);
        var transportIds = $('input#transportIds').val();
        $.ajax({
            type: "POST",
            url: $('input#changeStatusBatchUrl').val(),
            data: {
                TransportIds: transportIds,
                TransportStatusId: $('select#transportStatusId').val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1){
                    transportIds = $.parseJSON(transportIds);
                    for(var i = 0; i < transportIds.length; i++) $('#tdStatus_' + transportIds[i]).html(json.data.StatusName);
                    $('#modalTransportStatus').modal('hide');
                }
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
    });

    
});

function resetTableTranport(){
    actionItemAndSearch({
        ItemName: 'Vận chuyển',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
}

function resetTableFilter(){
    actionItemAndSearchFilter({
        ItemName: 'Bộ lộc',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){

        }
    });
}

function renderContentTransports(dataAll){
    var data = dataAll.dataTransports;
    // console.log(dataAll);return false;
    $("#html-thead").html(dataAll.htmlTableTh)
    var html = '';
    if(data!=null) {
        // var labelCss = [];
        // if(data.length > 0) labelCss = data[0].labelCss;
        // var sumCODCost = 0;
        // var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        // var urlEditOrder = $('#urlEditOrder').val() + '/';
        // var urlEditTransport = $('#urlEditTransport').val() + '/';
        // var trClass = '';
        // for (var item = 0; item < data.length; item++) {
        //     sumCODCost += parseInt(data[item].CODCost);
        //     trClass = '';
        //     if(data[item].TransportStatusId == '5') trClass = ' trItemCancel';
        //     html += '<tr id="trItem_' + data[item].TransportId + '" class="dnd-moved trItem' + trClass + '">';
        //     html += '<td><a href="javascript:void(0);" transport-id="' + data[item].TransportId + '" class="treetable_open fa fa-angle-right  parent_all" style="margin-right: 9px;"></a><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].TransportId + '"></td>';
        //     html += '<td><a href="' + urlEditTransport + data[item].TransportId + '">' + data[item].TransportCode + '</a></td>';
        //     html += '<td><a href="' + urlEditOrder + data[item].OrderId + '">' + data[item].OrderCode + '</a></td>';
        //     html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
        //     html += '<td><a href="' + urlEditCustomer + data[item].CustomerId + '">' + data[item].FullName + '</a></td>';
        //     html += '<td>' + (data[item].TransporterName != null ? data[item].TransporterName : '') + '</td>';
        //     html += '<td class="text-center" id="tdStatus_'+data[item].TransportId+'"><span class="' + labelCss.TransportStatusCss[data[item].TransportStatusId] + '">' + data[item].TransportStatus + '</span></td>';
        //     html += '<td class="text-center"><span class="' + labelCss.CODStatusCss[data[item].CODStatusId] + '">' + data[item].CODStatus + '</span></td>';
        //     html += '<td class="text-right">' + formatDecimal(data[item].CODCost) + '</td>';
        //     html += '<td>' + data[item].StoreName + '</td>';
        //     html += '</tr>';
        //     html += '<tr class="chose-all" style="display:none;" id="child_' + data[item].TransportId + '"><td colspan="10" id="content_data_detail_' + data[item].TransportId + '"></td></tr>';
        // }

        var htmlPaginate = '';

        // if(html != '') htmlPaginate += '<tr><td colspan="8"></td><td class="text-right">' + formatDecimal(sumCODCost.toString()) + '</td><td></td></tr>';
        htmlPaginate += '<div class="row box-tools "><div class="col-md-2 total-data"></div><div class="col-md-2"><label>Số dòng mỗi trang</label><input id="changeLimit" value="20"></div><div class="col-sm-8 paginate_table pull-right"></div></div>';
        $('#tbodyTransport').html(data);
        $(".box-footer").html(htmlPaginate);
        // $("#table-data").tableHeadFixer({"left": 0});

        $('#table-data').dragableColumns();
        $("#table-data").tableHeadFixer({"left": 0});
        var totalColumnTable = $('#table-data tr th').length;
        $("input#txtCountColTable").val('Hiển thị '+totalColumnTable+'/'+totalColumnTable);
        app.init(totalColumnTable);

    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}

app.comment = function () {
    $('#tbodyTransport').on('click', '.aShowComment', function () {
        var userImagePath = $('input#userImagePath').val();
        var json = $(this).parent().find('span.jsonComment').text();
        var html = "";
        $.each($.parseJSON(json), function (k, v) {
            html += '<div class="box-customer mb10"><table><tbody><tr>'+
                '<th rowspan="2" valign="top" style="width: 50px;"><img src="'+userImagePath+v.Avatar+'" alt=""></th>'+
                '<th><a href="javascript:void(0)" class="name">'+v.FullName+'</a></th>'+
                '<th class="time">'+v.CrDateTime+'</th></tr><tr>'+
                '<td colspan="2"><p class="pComment">'+v.Comment+'</p></td></tr></tbody></table></div>';
        });
        $(".listCommentAll").html(html);
        $("#modalItemComment").modal('show');
    }).on('click', '.btnInsertComment', function () {
        var transportId = $(this).attr('transport-id');
        var comment = $('input#comment_' + transportId).val().trim();
        if (comment != '') {
            if (transportId > 0) {
                var btn = $(this);
                btn.prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('input#insertTransportCommentUrl').val(),
                    data: {
                        TransportId: transportId,
                        Comment: comment
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) {
                            $('div#listComment_' + transportId).prepend(genItemComment(comment));
                            $('input#comment_' + transportId).val('');
                        }
                        else showNotification(json.message, json.code);
                        btn.prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop('disabled', false);
                    }
                });
            }
            else {
                $('div#listComment_' + transportId).prepend(genItemComment(comment));
                $('input#comment_' + transportId).val('');
            }
        }
        else {
            showNotification('Vui lòng nhập ghi chú', 0);
            $('input#comment_' + transportId).focus();
        }
    });

};


app.hideAndShow = function () {
    $('#tbodyTransport').on('click', 'a.treetable_open', function () {
        var id = $(this).attr('transport-id');
        $('.chose-all').hide();
        $(this).removeClass('treetable_open fa fa-angle-right').addClass('treetable_close fa fa-angle-down');
        $('.fa-angle-down').css("margin-right", 5);
        getListTransposts(id);
        return false;
    }).on('click', 'a.treetable_close', function () {
        var id = $(this).attr('transport-id');
        $('#child_' + id).hide();
        $(this).removeClass('treetable_close fa fa-angle-down').addClass('treetable_open fa fa-angle-right');
        $('.fa-angle-right').css("margin-right", 9);
        return false;
    })
};

app.updateTracking = function(){
    $("body").on('click','.aTracking', function(){
        $("input#transportId").val($(this).attr('transport-id'));
        var transportStatusId = parseInt($(this).attr('transport-status-id'));
        if(transportStatusId == 1 || transportStatusId == 2 || transportStatusId == 3 || transportStatusId == 4) {
            var tracking = $(this).text();
            if (tracking == 'Cập nhật') tracking = '';
            $('input#tracking').val(tracking);
            $('#modalTracking').modal('show');
        }
        return false;
    });
    $('#btnUpdateTracking').click(function(){
        var transportId = parseInt($("input#transportId").val());
        var tracking = $('input#tracking').val().trim();
        if(tracking != '' && transportId > 0){
            var tracking1 = $('.aTracking').text();
            if(tracking1 == 'Cập nhật') tracking1 = '';
            if(tracking != tracking1) {
                var btn = $(this);
                btn.prop('disabled', true);
                updateTransportField(transportId, 'Tracking', tracking, function(data){
                    $('.aTracking').text(data.Tracking);
                    $('#modalTracking').modal('hide');
                    btn.prop('disabled', false);
                }, function(){
                    btn.prop('disabled', false);
                });
            }
            else showNotification('Vui lòng chọn mã vận đơn khác', 0);
        }
        else{
            showNotification('Mã vận đơn không được bỏ trống', 0);
            $('input#tracking').focus();
        }
    });
};
app.updateShipCost = function(){
    $("body").on('click','.aShipCost', function(){
        $("input#transportId").val($(this).attr('transport-id'));
        var transportStatusId = parseInt($(this).attr('transport-status-id'));
        if(transportStatusId == 1 || transportStatusId == 2 || transportStatusId == 3 || transportStatusId == 4) {
            var shipCost = $(this).find('span').text();
            if (shipCost == 'Cập nhật') shipCost = '0';
            $('input#shipCost').val(shipCost);
            $('#modalShipCost').modal('show');
        }
        return false;
    });
    $('#btnUpdateShipCost').click(function(){
        var shipCost = replaceCost($('input#shipCost').val(), true);
        var transportId = parseInt($("input#transportId").val());
        if(shipCost > 0 && transportId > 0){
            var shipCost1 = $('#aShipCost').find('span').text();
            if(shipCost1 == 'Cập nhật') shipCost1 = '0';
            if(shipCost != replaceCost(shipCost1, true)){
                var btn = $(this);
                btn.prop('disabled', true);
                updateTransportField(transportId, 'ShipCost', shipCost, function (data) {
                    $('.aShipCost span').text(formatDecimal(shipCost.toString()));
                    $('#modalShipCost').modal('hide');
                    btn.prop('disabled', false);
                }, function(){
                    btn.prop('disabled', false);
                });
            }
            else showNotification('Vui lòng chọn phí ship khác', 0);
        }
        else{
            showNotification('Phí ship thực tế phải lớn hơn 0', 0);
            $('input#shipCost').focus();
        }
    });
};
app.updateTransportStatus = function(){
    $('select#transportStatusId').change(function(){
        if($(this).val() == '5' || $(this).val() == '6' || $(this).val() == '7') $('#divCancelReason').fadeIn();
        else{
            $('#divCancelReason').fadeOut();
            $('select#cancelReasonId').val('0');
            $('input#cancelComment').val('');
        }
    });
    $("body").on('click','.aTransportStatus', function(){
        $("input#transportStatusId").val($(this).attr('transport-status-id'));
        $("input#transportId").val($(this).attr('transport-id'));
        $("span#jsonTransport").text($(this).parent().find('span.jsonTransport').text());
        var transportStatusId = parseInt($(this).attr('transport-status-id'));
        if(transportStatusId != 4 && transportStatusId != 5 && transportStatusId != 7) $('#modalTransportStatus').modal('show');
        return false;
    });
    $('#btnUpdateTransportStatus').click(function(){
        var transportStatusId = $('select#transportStatusId').val();
        var jsonTransport = $.parseJSON($("span#jsonTransport").text());
        if(transportStatusId != $("input#transportStatusId").val()) {
            var cancelComment = $('input#cancelComment').val().trim();
            if(transportStatusId == '5' || transportStatusId == '6' || transportStatusId == '7'){
                if(cancelComment.length < 30){
                    showNotification('Ghi chú hủy giao hàng quá ngắn - Hãy nhập có trách nhiệm !', 0);
                    return false;
                }
            }
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateFieldUrl').val(),
                data: {
                    TransportId: $("input#transportId").val(),
                    FieldName: 'TransportStatusId',
                    FieldValue: transportStatusId,
                    OrderId: jsonTransport.OrderId,
                    CancelReasonId: jsonTransport.CancelReasonId,
                    CancelComment: cancelComment,
                    StoreId: jsonTransport.StoreId,
                    CustomerId: jsonTransport.CustomerId,
                    CODCost: jsonTransport.CODCost
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) redirect(true, '');
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else showNotification('Vui lòng chọn trạng thái khác', 0);
    });
};
function getListTransposts(transportId) {
    if($('#spanTranspost_' + transportId).length > 0){
        var data =  $.parseJSON($('#spanTranspost_' + transportId).text());
        genPanelTransport(transportId, data);
    }
    else {
        $.ajax({
            type: "POST",
            url: $('input#getDetailUrl').val(),
            data: {
                TransportId: transportId
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if (json.code = 1) {
                    var data = json.data;
                    $('#divTranspostJson').append('<span id="spanTranspost_' + transportId + '">' + JSON.stringify(data) + '</span>');
                    genPanelTransport(transportId, data);
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    }
}

function genPanelTransport(transportId, data){
    var productImagePath = $('input#productImagePath').val();
    var product = "";
    $.each(data.Products, function (key, value) {
        product += '<div class="medium-box-n boxst-1 primary-gradient" style="margin-bottom: 5px; font-size:13px">'+
                    '<img class="pull-left" width="40px" src="' + productImagePath + value.ProductImage + '" style="margin-right: 5px;">'+
                    '<div class="right-s">'+
                    '<p style="margin: 0 0 0px"><a href="javascript:void(0)">'+value.ProductName+'</a></p>'+
                    '<p style="margin: 0 0 0px">'+value.Quantity+' x '+formatDecimal(value.Price.toString())+' đ</p>'+
                    '</div>'+
                    '<div class="clearfix"></div>'+
                    '</div>';
    });

    var status="", shipCost = "Cập nhật", tracking = "Cập nhật", customerName = "", phoneNumber = "", email = "", address = "", countryName = "Việt Nam", comment = "", detailComment = "";
    var customerAddress = data.customerAddress;
    if (customerAddress.length > 0) {
        customerName = customerAddress.CustomerName;
        phoneNumber = customerAddress.PhoneNumber;
        email = customerAddress.Email;
    }
    if (customerAddress.ZipCode == "" || customerAddress.ZipCode == '0') {
        address = '<i class="fa fa-map-marker" aria-hidden="true"></i>'+
                '<span class="i-ward">'+customerAddress.Address+' '+customerAddress.WardName+'</span>'+
                '<span class="br-line i-district">'+customerAddress.DistrictName+'</span>'+
                '<span class="br-line i-province">'+customerAddress.ProvinceName+'</span>';
    }
    else {
        address = '<i class="fa fa-list-alt" aria-hidden="true"></i><span class="i-province">ZipCode: '+customerAddress.ZipCode+'</span>';
    }
    if (customerAddress.CountryName != "") countryName = customerAddress.CountryName;

    if(data.transport.TransportStatusId != 4 && data.transport.TransportStatusId != 5 && data.transport.TransportStatusId != 7){
        status = '<p><a href="javascript:void(0)" transport-id="'+transportId+'" transport-status-id="'+data.transport.TransportStatusId+'" class="btn-updaten aTransportStatus" style="color:#fff"><i class="fa fa-truck fa-flip-horizontal"></i> Cập nhật vận chuyển</a><span hidden="hidden" class="jsonTransport">'+JSON.stringify(data.transport)+'</span></p>';
    }else{ 
        status = '<p><a href="javascript:void(0)" transport-id="'+transportId+'" transport-status-id="'+data.transport.TransportStatusId+'" class="btn-updaten aTransportStatus" style="color:#bbb"><i class="fa fa-truck fa-flip-horizontal"></i> Cập nhật vận chuyển</a><span hidden="hidden" class="jsonTransport">'+JSON.stringify(data.transport)+'</span></p>';
    }

    var i = 0;
    var userImagePath = $('input#userImagePath').val();
    $.each(data.listTransportComments, function (k, v) {
        i++;
        if (i < 3) {
            comment = '<div class="box-customer mb10">'+
                '<table><tbody><tr>'+
                '<th rowspan="2" valign="top" style="width: 50px;"><img src="'+userImagePath+''+ v.Avatar+'" alt=""></th>'+
                '<th><a href="javascript:void(0)" class="name">'+v.FullName+'</a></th><th class="time">'+v.CrDateTime+'</th></tr>'+
                '<tr><td colspan="2"><p class="pComment">'+v.Comment+'</p></td></tr></tbody></table></div>';
        }
    });
    if (data.listTransportComments.length > 2) detailComment = `<div class="text-right light-dark"><span hidden="hidden" class="jsonComment">${JSON.stringify(data.listTransportComments)}</span><a href="javascript:void(0)" class="aShowComment" data-id="1">Xem tất cả &gt;&gt;</a></div>`;

    if(data.transport.Tracking != "") tracking = data.transport.Tracking;
    if(parseInt(data.transport.ShipCost) > 0) shipCost = '<span>'+formatDecimal(data.transport.ShipCost.toString())+ '</span> đ';  
    var html = ''+
        '<div class="row">'+
        '<div class="col-sm-3" style="padding-left: 33px;">'+product+'</div>'+
        '<div class="col-sm-3"><label class="light-blue">Ghi chú</label><div class="box-transprt clearfix mb10">'+
        '<button type="button" class="btn-updaten save btnInsertComment" transport-id="'+transportId+'">Lưu</button>'+
        '<input type="text" class="add-text" id="comment_'+transportId+'" value="">'+
        '</div>'+
        '<div class="listComment" id="listComment_'+transportId+'">'+comment+'</div>'+detailComment+
        '</div>'+
        '<div class="col-sm-3">'+
        '<div class=""><div><h4 class="mgbt-20 light-blue">Thông tin giao hàng</h4></div>'+
        '<div class="row" style="margin-top: -10px;">'+
        '<div class="col-sm-12 mh-wrap-customer"><div class="item"><i class="fa fa-user" aria-hidden="true"></i>'+
        '<span class="i-name">'+customerAddress.CustomerName+'</span></div><div class="item">'+
        '<i class="fa fa-phone" aria-hidden="true"></i><span class="i-phone">'+customerAddress.PhoneNumber+'</span></div>'+
        '<div class="item"><i class="fa fa-envelope" aria-hidden="true"></i><span class="i-email">'+customerAddress.Email+'</span>'+
        '</div><div class="item i-address">'+address+'</div><div class="item">'+
        '<i class="fa fa-id-card" aria-hidden="true"></i>'+
        '<span class="i-country" >'+countryName+'</span></div></div></div></div></div><div class="col-sm-3 " style="text-align:  right;">'+
        '<div class="row"><div class="col-sm-12 form-group">'+status+'</div><div class="col-sm-12 form-group">'+
        '<div class="row"><div class="col-sm-7"><span>Mã vận đơn</span></div>'+
        '<div class="col-sm-5"><a href="javascript:void(0)" transport-id="'+transportId+'" transport-status-id="'+data.transport.TransportStatusId+'" class="light-dark aTracking">'+tracking+'</a></div>'+
        '</div></div><div class="col-sm-12 form-group"><div class="row"><div class="col-sm-7"><span>Phí ship thực tế</span></div>'+
        '<div class="col-sm-5"><a href="javascript:void(0)" transport-id="'+transportId+'" transport-status-id="'+data.transport.TransportStatusId+'" class="light-dark aShipCost">'+shipCost+'</a></div>'+
        '</div></div><div class="col-sm-12 form-group"><div class="row"><div class="col-sm-7"><span>Tổng cần thu (COD)</span></div>'+
        '<div class="col-sm-5"><span>'+formatDecimal(data.transport.CODCost)+' đ</span></div></div>'+
        '</div> </div> </div></div>';
    
    $('#content_data_detail_' + transportId).html(html);
    $('span#jsonProduct_' + transportId).text(JSON.stringify(data.Products));
    $('#child_' + transportId).show();
    $(this).removeClass('treetable_open fa fa-angle-right').addClass('treetable_close fa fa-angle-down');
}

function updateTransportField(transportId, fieldName, fieldValue, fnSuccess, fnError){
    $.ajax({
        type: "POST",
        url: $('input#updateFieldUrl').val(),
        data: {
            TransportId: transportId,
            FieldName: fieldName,
            FieldValue: fieldValue,
            OrderId: $('input#orderId').val()
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1){
                $('#ulActionLogs').prepend('<li>' + json.data.Comment + '<span class="time">' + getCurrentDateTime(2) + '</span></li>');
                fnSuccess(json.data);
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            fnError();
        }
    });
}

app.filter = function(){
    $("body").on('click', '.btnShowModalListFilter', function(){
        resetTableFilter();
        $("#modalListFilter").modal('show');
    }).on('click', '#btnShowModalAddFilter', function(){
        $("#btn-remove-ilter").css("display", "none");
        $("#titleAddFilter").html('Thêm mới bộ lọc');
        $("#btn-save-filter-add").html('Thêm');
        $("#modalAddFilter").modal('show');
    });
    var data_filter_add = {
        itemAddFilters: [],
        filterId: 0,
        tagAddFilters: []
    };
    // $('.tttt').click(function(){
    //     $(this).next().toggle();
    // });
    $('a.btn-filter-data-add').on("click", function(e){
        $(this).next('ul').toggle();
        e.stopPropagation();
        var $this = $(this);
        var id = parseInt($this.attr('value-id'));
        if(id > 0){
            // tạo biến gen code html cho field
            //khởi tạo 1 đối tượng item filer
            var filter_ob = {};
            // lấy nhãn lọc
            var filed_name = $this.attr('field-select');
            var text_field_name = $this.parent().parent().parent().parent().find('a span').eq(0).text();
            // lấy tất cả các điều kiện lọc và giá trị của điều kiện lọc theo nhãn này
            var conds = [];
            //text_opertor  ví dụ : là ,trong khoảng ,trước, sau bằng ,.....
            var text_operator = '';
            var ob_text_operator = $this.attr('text-opertor');
            if (typeof ob_text_operator != 'undefined' && ob_text_operator.length > 0) {
                text_operator = " " + ob_text_operator;
            }
            else{
                ob_text_operator = $('select.' + filed_name);
                if (typeof ob_text_operator != 'undefined' && ob_text_operator.length > 0) {
                  
                    text_operator = " " + $(ob_text_operator).find('option:selected').text();
                }
            }
            //giá trị của value_operator khi có text operator ví dụ : là tương úng với is,= | trong khoảng tương ứng với betweet , in tùy ý rồi sang bên server xử lý sau
            var ob_value_operator = $this.attr('value-operator');
            if (typeof ob_value_operator != 'undefined' && ob_value_operator.length > 0) {
                conds.push(ob_value_operator);
            }
            var text_cond_name = '';
            //push điều kiện lọc và giá trị vào mảng đồng thơi gen html cho nhãn lọc
            if($('.' + filed_name).length > 0 && parseInt($this.attr('check-length')) > 0){
                $('.' + filed_name).each(function (key, val) {
                    if (val.tagName == 'INPUT') {
                        conds.push($(val).val());
                        if($(val).attr('id') == 'timeEnd') text_cond_name += " đến " + $(val).val();
                        else text_cond_name += " " + $(val).val();
                    }
                    else if (val.tagName == 'SELECT') {
                        conds.push($(val).val());
                        text_cond_name += " " + $(val).find('option:selected').text();
                    }

                });
            }else{
                conds.push($this.attr('value-id'));
                text_cond_name += " " + $this.text();
            }
            
            //gắn các thuộc tính vào đối tượng item filter
            filter_ob.field_name = filed_name;
            filter_ob.conds = conds;
            filter_ob.tag = text_field_name + text_operator + text_cond_name;

            var replace = false;
            var matches = true;
            var val;
            var key;
            //kiểm tra item lọc này có trùng nhau không
            if (data_filter_add.itemAddFilters != null) {
                for (var i = 0; i < data_filter_add.itemAddFilters.length; i++) {
                    val = data_filter_add.itemAddFilters[i];
                    //nếu 2 nhãn lọc giống nhau
                    if (filter_ob.field_name == val.field_name) {
                        var cond_matches = 0;
                        for (var cond = 0; cond < val.conds.length; cond++) {
                            if (val.conds[cond] == filter_ob.conds[cond]) cond_matches++;
                            else break;
                        }
                        if (cond_matches == val.conds.length) {
                            //showNotification('Điều kiện lọc đã có', 0);
                            return;
                        }
                        // nếu điều kiện lọc mà giống nhau thì nhãn này sẽ bị thay thế
                        if (val.conds[0] == filter_ob.conds[0]) {
                            replace = true;
                            key = i;
                        }
                    }
                }
                if(replace){
                    for (var i = 1; i < val.conds.length; i++) data_filter_add.itemAddFilters[key].conds[i] = filter_ob.conds[i]; //thay thế đối tượng tronmang itemAddFilters
                    data_filter_add.tagAddFilters[key] = filter_ob.tag;
                renderTagFilterAdd( data_filter_add.tagAddFilters)
                   // updateStatusBtnSaveFilterAdd()
                   // updateStatusBtnRemoFilterAdd()
                }
                else {
                    //thêm 1 đối item filter vào itemAddFilters
                    data_filter_add.itemAddFilters.push(filter_ob);
                    data_filter_add.tagAddFilters.push(filter_ob.tag);
                    renderTagFilterAdd( data_filter_add.tagAddFilters)
                }
            }
        }
    });

    // Đăng ký sự kiện khi remove nhãn lọc
    $('#container-filters-add').on('click', '.item  a.remove', function () {
        //lấy đối tượng li
        var parent_item = $(this).parents('.item');
        var index = $(parent_item).attr('data-index');
        data_filter_add.itemAddFilters.splice(index - 1, 1);
        data_filter_add.tagAddFilters.splice(index - 1, 1);
        resetIndexAdd();
        $(this).parent().remove();

    });

    $("body").on('click', '#btn-save-filter-add', function(){
        var filterId = $("input#filterId").val();
        var filterName = '';
        var displayOrder = 0;
        filterName = $("input#nameFilter").val().trim();
        displayOrder = ($("#ulFilter .liFilter").length) +1;
        if (filterName == '') {
            showNotification('Vui lòng nhập tên bộ lọc', 0);
            return false;
        }
        var btn = $('#btn-save-filter-add');
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: btn.attr('data-href'),
            //async: false,
            data: {
                FilterId: filterId,
                FilterName: filterName,
                FilterData: JSON.stringify(data_filter_add.itemAddFilters),
                TagFilter: JSON.stringify(data_filter_add.tagAddFilters),
                ItemTypeId: $('#itemTypeId').val(),
                DisplayOrder: displayOrder,
                FilterNote : $("input#noteFilter").val(),
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                $("input#nameFilter").val('');
                $("input#filterId").val(0);
                $('#container-filters-add').html('');
                $("input#noteFilter").val('');
                if(json.code == 1){
                   resetTableTranport();
                   resetTableFilter();
                    $("#modalAddFilter").modal('hide')
                }
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
    }).on('click', 'a.btn-show-modal-edit-filter', function(){
        $("#btn-remove-ilter").css("display", "block");
        $("#titleAddFilter").html('Cập nhật bộ lọc');
        $("#btn-save-filter-add").html('Cập nhật');
        $("input#nameFilter").val('');
        $("input#filterId").val(0);
        $("input#noteFilter").val('');
        $('#container-filters-add').html('');
        var filterId = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url: $("input#urlGetFilter").val(),
            //async: false,
            data: {
                FilterId: filterId,
            },
            success: function (response) {
                console.log(response)
                var json = $.parseJSON(response);
                if(json.code == 1){
                    var data = json.data;
                    $("input#filterId").val(filterId);
                   $("input#nameFilter").val(data.FilterName);
                   renderTagFilterAdd($.parseJSON(data.TagFilter));
                   $("input#noteFilter").val(data.FilterNote);
                   $("#modalAddFilter").modal('show')
                }
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
    }).on('click', '#btn-remove-ilter', function(){
        $.ajax({
            type: "POST",
            url: $('#remove-filter').attr('data-href'),
            //async: false,
            data: {
                FilterId: $("input#filterId").val(),
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1){
                   redirect(true, '');
                }
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
    
    $('.input-group-btn > ul > li > a.btn-filter-data-add').click(function(){
        $(this).parents('.dropdown-submenu').siblings().children('.dropdown-menu').hide();
        $(this).next().show();
    });


    $('.input-group-btn ul li ul li .btn-filter-data-add').click(function(){
        $(this).parents('.dropdown-menu').hide();
    });

}

app.configTable = function(totalColumnTable){
    var arrIdShowAndHideTable = [];
    var pinNumber = -1;
    $("body").on('click', '#btnShowModalConfigTable', function(){
        var html = '';
        $('#table-data .dnd-moved th').each(function (k,v) {
            html += '<tr>';
            html += '<td>'+k+'</td>';
            html += '<td>'+$(this).text()+'</td>';
            html += '<td><input type="checkbox" value="" class="show_and_hide" id="show_and_hide_'+k+'" data-value="OFF" data-id="'+k+'"/></td>';
            html += '<td><input type="checkbox" value="" class="pin" data-value="OFF" id="pin'+k+'" data-id="'+k+'"/></td>';
            html += '</tr>';
        });
        $("#tbodyConfig").html(html);
        $.each(arrIdShowAndHideTable, function(k,v){
            $("#show_and_hide_"+v).prop('checked', true);
            $("#show_and_hide_"+v).attr('data-value', 'ON')
        })
        if(pinNumber > -1){
            $("#pin"+pinNumber).prop('checked', true);
            $("#pin"+pinNumber).bootstrapSwitch({size: 'mini', 'disabled': false});
            $("#pin"+pinNumber).attr('data-value', 'ON');
        }
        $('#tbodyConfig tr').each(function () {
            var $this = $(this).find('td');
            var td_2 = $this.eq(3).find('input').attr('data-id');
            if(pinNumber > -1 && $this.eq(3).find('input').attr('data-value') == 'OFF'){
                $this.eq(3).find('input').bootstrapSwitch({size: 'mini', 'disabled': true});
                $this.eq(3).find('input').prop('checked', true);
            }

        });

        $('.show_and_hide').bootstrapSwitch({size: 'mini'}).on('switchChange.bootstrapSwitch', function(event, state) {
            var check = state ? 'ON':'OFF';
            $(this).attr('data-value', check);
        });
        $('.pin').bootstrapSwitch({size: 'mini'}).on('switchChange.bootstrapSwitch', function(event, state) {
            var check = state ? 'ON':'OFF';
            $(this).attr('data-value', check);
            if(state){
                $('#tbodyConfig tr').each(function () {
                    var $this = $(this).find('td');
                    var td_2 = $this.eq(3).find('input').attr('data-id');

                    if($this.eq(3).find('input').attr('data-value') == 'OFF'){
                       $this.eq(3).find('input').bootstrapSwitch('disabled',true);
                    }else{
                        pinNumber = parseInt(td_2);
                    }
                    
                    
                });
            }else{
                $('#tbodyConfig tr').each(function () {
                    var $this = $(this).find('td');
                    var td_2 = $this.eq(3).find('input').attr('data-id');
                    if($this.eq(3).find('input').attr('data-value') == 'OFF'){
                       $this.eq(3).find('input').bootstrapSwitch('disabled',false);
                    }
                    pinNumber = -1;
                });
            }
        });
        $("#modalConfigTable").modal('show');
    }).on('click', '#bnAddConfigTable', function(){
        $("#table-data").tableHeadFixer({"left": -1});
        var countColumnTable = 0;
        arrIdShowAndHideTable = [];
        $('#tbodyConfig tr').each(function () {
            var $this = $(this).find('td');
            var td_1 = $this.eq(2).find('input').attr('data-id');
            if($this.eq(2).find('input').attr('data-value') == 'ON'){
                $(".th_"+td_1).hide();
                arrIdShowAndHideTable.push(td_1);
                $('#table-data tr').each(function () {
                    $(this).find('td').eq(td_1).hide();
                    
                });
            }else{
                countColumnTable ++;
                $(".th_"+td_1).show();
                $('#table-data tr').each(function () {
                    $(this).find('td').eq(td_1).show();

                });
            }
            
        });
        if(pinNumber > -1){
            $("#table-data").tableHeadFixer({"left": pinNumber + 1});
            for(var i = 0; i <= pinNumber; i++){
                $(".th_"+i).removeClass('th_move');
            }
        }
        $("input#txtCountColTable").val('Hiển thị '+countColumnTable+'/'+totalColumnTable);
        $("#modalConfigTable").modal('hide');
    });

}

function renderTagFilterAdd(data) {
    var html = '';
    for (var i = 0; i < data.length; i++) {
        html += '<tr class="item" data-index="' + (i + 1) + '">';
        html += '<td>' + data[i] + '</td>';
        html += '<td><a href="javascript:void(0);" class="remove"><i class="fa fa-fw fa-trash font-size-12px"></i></a ></td>';
        html += '</tr>';
    }
    
    $('#container-filters-add').html(html);

}
function resetIndexAdd() {
    $('#container-filters-add').find('.item').each(function (key, val) {
        $(this).attr('data-index', key + 1);
    });
}

    
function renderContentFilters(data){
     var html = '';
    if(data!=null) {
        var trClass = '';
        for (var item = 0; item < data.length; item++) {
            html += '<tr id="trItem_' + data[item].FilterId + '" class="trItem">';
            html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
            html += '<td><a href="javascript:void(0);" class="btn-show-modal-edit-filter" data-id="'+ data[item].FilterId +'">' + data[item].FilterName + '</a></td>';
            html += '<td>' + data[item].UserCreate + '</td>';
            html += '<td>' + data[item].FilterNote + '</td>';
            html += '</tr>';
        }

        html += '<tr><td colspan="4" class="paginate_table"></td></tr>';

        $('#tbodyFilter').html(html);
    }
   
}
