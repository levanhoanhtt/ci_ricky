$(document).ready(function(){
    $("#tbodyTransportReason").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#transportReasonId').val(id);
        $('input#transportReasonName').val($('td#transportReasonName_' + id).text());
        $('select#transactionTypeId').val($('input#transactionTypeId_' + id).val());
        scrollTo('input#transportReasonName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteTransportReasonUrl').val(),
                data: {
                    TransportReasonId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#transportReason_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#transportReasonForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#transportReasonForm')) {
            var form = $('#transportReasonForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="transportReason_' + data.TransportReasonId + '">';
                            html += '<td id="transportReasonName_' + data.TransportReasonId + '">' + data.TransportReasonName + '</td>';
                            html += '<td id="transactionTypeName_' + data.TransportReasonId + '">' + data.TransportTypeName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.TransportReasonId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.TransportReasonId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="transactionTypeId_' + data.TransportReasonId + '" value="' + data.TransactionTypeId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyTransportReason').prepend(html);
                        }
                        else{
                            $('td#transportReasonName_' + data.TransportReasonId).text(data.TransportReasonName);
                            $('td#transactionTypeName_' + data.TransportReasonId).html(data.TransportTypeName);
                            $('input#transactionTypeId_' + data.TransportReasonId).val(data.TransactionTypeId);
                        }
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});