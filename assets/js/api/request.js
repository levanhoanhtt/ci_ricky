function requestApi(options) {
    let url = "http://localhost/work/ceo_brain_cctc/"+options.url;
    return new Promise((resolve, reject) => {
        axios(options).then(function(data) {
            if (data.error) {
                reject(data.error);
                return;
            }
            resolve(data.data);
        }).catch(function(error) {
            let response = error.response;
            if (response && response.data && response.data.error) {
                reject(response.data.error);
                return;
            }
            reject({
                code: 0,
                message: 'Có lỗi xảy ra, bạn vui lòng thử lại'
            });
        });
    });
}

