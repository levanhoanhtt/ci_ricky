var app = app || {};

var paginateConsultObject = null;

app.init = function(customerConsultId) {
    app.initLibrary();
    app.customer();
    // app.product();
    app.addComment(customerConsultId);
    // app.submit(customerConsultId);
    // app.updateFacebook(customerConsultId);
    // if(customerConsultId > 0) app.updatePercentSell(customerConsultId);
    // app.success();
    // app.dataError(customerConsultId);
    app.submit();
    if(parseInt($("input#customerId").val()) > 0) getListConsult($("input#customerId").val());
};

app.initLibrary = function() {
	province();
	$('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
	$('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}

app.customer = function(){
    chooseCustomer(function (li) {
        var customerId = parseInt(li.attr('data-id'));
        $('input#customerId').val('0');
        showInfoCustomer(customerId);
    });
    province('provinceId', 'districtId', 'wardId');
    province('customerProvinceId', 'customerDistrictId', 'customerWardId');
    $('#ulCustomerKindId').on('click', 'a', function(){
        var id = $(this).attr('data-id');
        $('input#customerKindId').val(id);
        if(id == '2') $('#divWholesale').show();
        else{
            $('input#debtCost, select#paymentTimeId').val('0');
            $('#divWholesale').hide();
        }
        $('select#customerGroupId .op').hide();
        $('select#customerGroupId .op_' + id).show();
        $('select#customerGroupId').val('0');
    });
    $('input.iCheckCustomerType').on('ifToggled', function (e) {
        if (e.currentTarget.checked) {
            if (e.currentTarget.value == '2') $('#divCompany').show();
            else $('#divCompany').hide();
        }
    });
    $('select#countryId, select#customerCountryId').change(function () {
        if ($(this).val() == '232' || $(this).val() == '0') {
            $('.VNoff').hide();
            $('.VNon').fadeIn();
        }
        else {
            $('.VNon').hide();
            $('.VNoff').fadeIn();
        }
    });
    //END add customer===================
    if($('input#customerId').val() != '0') showInfoCustomer($('input#customerId').val());
    $('#btnCloseBoxCustomer').click(function (){
        $('#divCustomer').hide();
        $('.boxChooseCustomer').show();
        $('input#customerId').val('0');
        return false;
    });
    $('#btnAddCustomer').click(function () {
        $('#customerForm').trigger('reset');
        if(parseInt($("input#customerId").val()) == 0 && parseInt($('input#customerConsultId').val()) > 0){
            var fullName = $("h4.n-name").text();
            var phone = $("div.n-phone").text();
            var firstName = fullName.split(' ').slice(0, -1).join(' ');
            var lastName = fullName.split(' ').slice(-1).join(' ');
            $("input#firstName").val(firstName);
            $("input#lastName").val(lastName);
            $("input#phoneNumber").val(phone);
        }
        
        $('#modalAddCustomer').modal('show');
    });
    $("#modalAddCustomer").on('click', '#btnUpdateCustomer', function(){
        var btn = $(this);
        if (validateEmpty('#customerForm')) {
            var password = $('input#password').val().trim();
            if(password != ''){
                if(password != $('input#rePass').val().trim()){
                    showNotification('Mật khẩu không trùng', 0);
                    return false;
                }
            }
            btn.prop('disabled', true);
            var isReceiveAd = 1;
            if ($('input#isReceiveAd').parent('div').hasClass('checked')) isReceiveAd = 2;
            $.ajax({
                type: "POST",
                url: $('#customerForm').attr('action'),
                data: {
                    CustomerId: 0,
                    FirstName: $('input#firstName').val().trim(),
                    LastName: $('input#lastName').val().trim(),
                    //FullName: $('input#fullName').val().trim(),
                    Email: $('input#email').val().trim(),
                    PhoneNumber: $('input#phoneNumber').val().trim(),
                    //PhoneNumber2: $('input#phoneNumber2').val().trim(),
                    GenderId: $('input[name="GenderId"]:checked').val(),
                    StatusId: 2,
                    BirthDay: $('input#birthDay').val(),
                    CustomerTypeId: $('input[name="CustomerTypeId"]:checked').val(),
                    CustomerKindId: $('input#customerKindId').val(),
                    CountryId: $('select#countryId').val(),
                    ProvinceId: $('select#provinceId').val(),
                    DistrictId: $('select#districtId').val(),
                    WardId: $('select#wardId').val(),
                    ZipCode: $('input#zipCode').val(),
                    Address: $('input#address').val().trim(),
                    CustomerGroupId: $('select#customerGroupId').val(),
                    FaceBook: $('input#facebook').val().trim(),
                    Comment: $('#customerComment').val().trim(),
                    CareStaffId: $('select#careStaffId').val(),
                    DiscountTypeId: $('select#discountTypeId').val(),
                    PaymentTimeId: $('select#paymentTimeId').val(),
                    CTVId: 0,
                    PositionName: $('input#positionName').val().trim(),
                    CompanyName: $('input#companyName').val().trim(),
                    TaxCode: $('input#taxCode').val().trim(),
                    DebtCost: replaceCost($('input#debtCost').val().trim(), true),
                    Password: password,
                    //TagNames: JSON.stringify(tags),
                    IsReceiveAd: isReceiveAd,
                    IsUpdateCustomerConsult : 1,
                    CustomerConsultId: parseInt($('input#customerConsultId').val())
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) {
                        showInfoCustomer(json.data);
                        $('#modalAddCustomer').modal('hide');
                        redirect(true, '');
                    }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        return false;
    });
};

app.submit = function(){
    $("#customerFormUpdate").on('click', '#updateDataConsult', function(){
        var btn = $(this);
        if (validateEmpty('#customerFormUpdate')) {
            var password = $('input#password').val().trim();
            if(password != ''){
                if(password != $('input#rePass').val().trim()){
                    showNotification('Mật khẩu không trùng', 0);
                    return false;
                }
            }
            btn.prop('disabled', true);
            var isReceiveAd = 1;
            if ($('input#isReceiveAd').parent('div').hasClass('checked')) isReceiveAd = 2;
             $.ajax({
                type: "POST",
                url: $('#customerFormUpdate').attr('action'),
                data: {
                    CustomerId: $("input#customerId").val(),
                    FirstName: $('input#firstNamec').val().trim(),
                    LastName: $('input#lastNamec').val().trim(),
                    //FullName: $('input#fullName').val().trim(),
                    Email: $('input#emailc').val().trim(),
                    PhoneNumber: $('input#phoneNumberc').val().trim(),
                    //PhoneNumber2: $('input#phoneNumber2').val().trim(),
                    GenderId: $('input[name="GenderId"]:checked').val(),
                    StatusId: 2,
                    BirthDay: $('input#birthDay').val(),
                    CustomerTypeId: $('input[name="CustomerTypeId"]:checked').val(),
                    CustomerKindId: $('input#customerKindId').val(),
                    CountryId: $('select#countryId').val(),
                    ProvinceId: $('select#provinceId').val(),
                    DistrictId: $('select#districtId').val(),
                    WardId: $('select#wardId').val(),
                    ZipCode: $('input#zipCode').val(),
                    Address: $('input#address').val().trim(),
                    CustomerGroupId: $('select#customerGroupId').val(),
                    FaceBook: $('input#facebook').val().trim(),
                    Comment: $('#customerComment').val().trim(),
                    CareStaffId: $('select#careStaffId').val(),
                    DiscountTypeId: $('select#discountTypeId').val(),
                    PaymentTimeId: $('select#paymentTimeId').val(),
                    CTVId: 0,
                    PositionName: $('input#positionName').val().trim(),
                    CompanyName: $('input#companyName').val().trim(),
                    TaxCode: $('input#taxCode').val().trim(),
                    DebtCost: replaceCost($('input#debtCost').val().trim(), true),
                    Password: password,
                    //TagNames: JSON.stringify(tags),
                    IsReceiveAd: isReceiveAd,
                    IsUpdateCustomerConsult : 1,
                    CustomerConsultId: parseInt($('input#customerConsultId').val())
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    // if (json.code == 1) {
                    //     showInfoCustomer(json.data);
                    //     $('#modalAddCustomer').modal('hide');
                    // }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        return false;
    });
}

app.addComment = function(customerConsultId){
    $('#btnInsertComment').click(function(){
        var comment = $('input#comment').val().trim();
        if(comment != ''){
            var commentTypeId = parseInt($('input[name="CommentTypeId"]:checked').val());
            if(customerConsultId > 0){
                var btn = $(this);
                btn.prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('input#insertCommentUrl').val(),
                    data: {
                        CustomerConsultId: customerConsultId,
                        Comment: comment,
                        CommentTypeId: 0
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1) {
                            $('div#listComment').prepend(genConsultComment(comment, commentTypeId, json.data.ConsultCommentId));
                            $('input#comment').val('');
                            var userLoginId = $('input#userLoginId').val();
                            if($('#tdSellUser_' + userLoginId).length == 0){
                                var html = '<tr id="tdSellUser_' + userLoginId + '" style="display: none;">';
                                html += '<td>' + $('input#fullNameLoginId').val() + '</td>';
                                html += '<td class="text-center"><span class="spanPercent">0</span> %</td>';
                                html += '<td class="actions text-center"><a href="javascript:void(0)" class="link_edit" data-id="' + json.data.CustomerConsultUserId + '" data-consult="' + json.data.ConsultCommentId + '"><i class="fa fa-pencil"></i></a>';
                                html += '<input type="text" hidden="hidden" class="comment" value=""></td></tr>';
                                $('#tbodySell').prepend(html);
                                //$('#divSell').show();
                            }
                        }
                        else showNotification(json.message, json.code);
                        btn.prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop('disabled', false);
                    }
                });
            }
            else{
                $('div#listComment').prepend(genConsultComment(comment, commentTypeId, 0));
                $('input#comment').val('');
            }
        }
        else{
            showNotification('Vui lòng nhập ghi chú', 0);
            $('input#comment').focus();
        }
    });
    $('#aShowComment').click(function(){
        $('#modalItemComment').modal('show');
        return false;
    });
};

$(document).ready(function () {customerConsultId
    var customerConsultId = parseInt($('input#customerConsultId').val());
    app.init(customerConsultId);
});

function showInfoCustomer(customerId) {
    $.ajax({
        type: "POST",
        url: $('input#getCustomerDetailUrl').val(),
        data: {
            CustomerId: customerId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if (json.code == 1) {
                var data = json.data;
                $('input#customerId').val(customerId);
                if (data.PhoneNumber != '' && data.PhoneNumber2 != ''){
                    $("h3.PhoneNumber").html(data.PhoneNumber);
                    data.PhoneNumber = data.PhoneNumber + ' - ' + data.PhoneNumber2;
                }
                var address = '';
                if (data.CountryId == '232') {
                    address += '<i class="fa fa-map-marker" aria-hidden="true"></i>';
                    //if (data.WardName != '')
                    address += '<span class="i-ward"><span class="spanAddress">' + data.Address + '</span> ' + data.WardName + '</span>';
                    if (data.DistrictName != '') address += '<span class="br-line i-district">' + data.DistrictName + '</span>';
                    if (data.ProvinceName != '') address += '<span class="br-line i-province">' + data.ProvinceName + '</span>';
                }
                else {
                    address += '<i class="fa fa-list-alt" aria-hidden="true"></i>';
                    address += '<span class="i-province">ZipCode: ' + data.ZipCode + '</span>';
                }
                var editCustomerUrl = $('input#editCustomerUrl').val() + '/' + customerId + '/';
                $('h4.i-name').html('<a href="' + editCustomerUrl + '3" target="_blank">' + data.FullName + '</a>');
                $('span.i-name').text(data.FullName);
                $('.i-phone').text(data.PhoneNumber);
                $('.n-phone').text(data.PhoneNumber);
                $('.i-total-orders').html('<a href="' + editCustomerUrl + '1" target="_blank">' + data.TotalOrders + '</a>');
                $('span#customerBalance').html('<a href="' + editCustomerUrl + '2" target="_blank">' + formatDecimal(data.Balance) + '</a>');
                if(data.CustomerGroupName != '') $('.i-cusType').html('<span class="label label-success">' + data.CustomerGroupName + '</span>').show();
                else $('.i-cusType').hide();
                // $('.i-email').text(data.Email);
                $('#spanFacebook').text(data.Facebook);
                $('input#consultFacebook').val(data.Facebook);
                $('.i-country').text(data.CountryName);
                $('.i-address').html(address);
                $('.boxChooseCustomer').hide();
                $('#divCustomer').show();
            }
            else showNotification(json.message, json.code);
        },
        error: function (response) {
            showNotification('Có  lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

function genConsultComment(comment, commentTypeId, consultCommentId){
    var html = '<div class="box-customer mb10" data-id="' + $('input#userLoginId').val() + '" data-consult="' + consultCommentId + '"><table><tbody><tr><th rowspan="2" class="user-wrapper" valign="top" style="width: 50px;"><div class="user-link"><img class="user-img" width="29" src="' + $('input#userImagePath').val() + $('input#avatarLoginId').val() + '" alt=""></div></th>';
    html +='<th><a href="javascript:void(0)" class="name">' + $('input#fullNameLoginId').val() + '</a></th>';
    html += '<th class="time">' + getCurrentDateTime(6) + '</th></tr><tr><td><p class="pComment" data-id="' + commentTypeId + '">' + comment + '</p></td>';
    html += '<td class="tdIcon"><span class="label ' + (commentTypeId == 1 ? 'label-success' : 'label-default') + '">' + (commentTypeId == 1 ? 'Gọi điện' : 'Facebook') + '</span></td></tr></tbody></table></div>';
    return html;
}

function getListConsult(customerId){
    var url = $('input#getListConsultUrl').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            CustomerId: customerId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            renderContentCustomerConsults(json.dataTables);
            var dataConsultPaging = {
                page: json.page,
                pageSize: json.pageSize,
                totalRow: json.totalRow,
                itemName: "Tư vấn lại",
                urlPaging: url,
                classPaging: 'paginate_table_consult',
                TransactionTypeId: 0
            };
            if(paginateConsultObject == null) paginateConsultObject = $('#table-data-consult').PaginateCustom(dataConsultPaging);
            else paginateConsultObject.PaginateCustom(dataConsultPaging);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

function renderContentCustomerConsults(data){
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditConsult = $('#urlEditConsult').val() + '/';
        var trClass = '';
        for(var item in data) {
            trClass = '';
            if(data[item].RemindDayDiff > 0 && data[item].RemindStatusId == 1) trClass = ' trItemCancel';
            html += '<tr class="trItem' + trClass + '">';
            html += '<td><a href="' + urlEditConsult + data[item].CustomerConsultId  + '" target="_blank">' + data[item].ConsultTitle + '</a></td>';
            html += '<td class="text-center">' + data[item].TimeProcessed + '</td>';
            if(data[item].RemindDayDiff > 0){
                if(data[item].ConsultDate != '') html += '<td>Quá hạn ' + data[item].RemindDayDiff + ' ngày</td>';
                else html += '<td></td>';
            }
            else html += '<td>'+ getDayText(data[item].RemindDayDiff) + data[item].ConsultDate +'</td>';
            html += '<td>'+ getDayText(data[item].CrDayDiff) +  data[item].CrDateTime +'</td>';
            html += '<td>' + (data[item].CrFullName != null ? data[item].CrFullName : '') + '</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].RemindStatusId] + '">' + data[item].RemindStatus + '</span></td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].OrderChannelId] + '">' + data[item].OrderChannel + '</span></td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="7" class="paginate_table_consult paginate_table"></td></tr>';
        $('#tbodyCustomerConsult').html(html);
    }
}

$.fn.PaginateCustom = function (opt) {
  var root = this;
  var conf = $.extend({pageShow: 11, page: 1, pageSize: 1, totalRow: 11,registerEvent: true}, opt);
  var actions = {
      init: function () {},
      render: function () {},
      event: function () {}
  };

  actions.init = function () {
      conf.page = parseInt(conf.page);
      conf.pageShow = parseInt(conf.pageShow);
      conf.pageSize = parseInt(conf.pageSize);
      conf.totalRow = parseInt(conf.totalRow);
  };

  actions.render = function () {
      if (conf.pageSize == 1) {
          html = '<div class="up-n-pager"><div class="total-row" style="padding-left:0 !important;">' + conf.totalRow + ' ' + conf.itemName + '</div></div>';
          $(root).find('.'+conf.classPaging).html(html);
          return false;
      }
      var html = "{total_rows}<ul>{first_page}{prev_page}{pages}{next_page}{last_page}</ul>";
      var total_rows = '<div class="total-row">' + conf.totalRow + ' ' + conf.itemName + '</div>';
      var first_page = '<li><a class="{option} first-page" data-page="1" data-transaction="'+conf.TransactionTypeId+'" href="javascript:;">Trang đầu</a></li>';
      var last_page = '<li><a class="{option} last-page" data-page="' + conf.pageSize + '" data-transaction="'+conf.TransactionTypeId+'" href="javascript:;">Trang Cuối</a></li>';
      var prev_page = '<li><a class="{option}" data-page="' + (conf.page - 1 > 0 ? conf.page - 1 : 1 ) + '" data-transaction="'+conf.TransactionTypeId+'" href="javascript:;"><<</a></li>';
      var next_page = '<li><a class="{option}" data-page="' + (conf.page + 1 < conf.pageSize ? conf.page + 1 : conf.pageSize ) + '" data-transaction="'+conf.TransactionTypeId+'" href="javascript:;">>></a></li>';
      var start_page = 1;
      var end_page = 1;
      var offset = Math.floor(conf.pageShow / 2);

      if (conf.pageSize <= conf.pageShow) {
          start_page = 1;
          end_page = conf.pageSize;
          html = html.replace(/\{[a-z_]{6,}\}|/img, '');
      }
      else {
          // page ở khoảng giữa
          if (conf.page > offset && conf.pageSize > conf.page + offset) {
              start_page = conf.page - offset;
              end_page = conf.page + offset;
              // page ở cuối
          }
          else if (conf.page == conf.pageSize) {
              start_page = conf.pageSize - conf.pageShow + 1;
              end_page = conf.pageSize;
              next_page = next_page.replace('{option}', ' disable');
              last_page = last_page.replace('{option}', ' disable');
              //page ở đầu
          }
          else if (conf.page === 1) {
              start_page = 1;
              end_page = conf.pageShow;
              first_page = first_page.replace('{option}', ' disable');
              prev_page = prev_page.replace('{option}', ' disable');

              //page nhỏ hơn số hiển thị
          }
          else if (conf.page <= conf.pageShow) {
              start_page = 1;
              end_page = conf.pageShow;

              //page lớn hơn số hiển thị conf.page > conf.pageShow
          }
          else {
              start_page = conf.pageSize - conf.pageShow + 1;
              end_page = conf.pageSize;
          }
          next_page = next_page.replace('{option}', '');
          last_page = last_page.replace('{option}', '');
          prev_page = prev_page.replace('{option}', '');
          first_page = first_page.replace('{option}', '');
      }
      var pages = "";
      for (var page = start_page; page <= end_page; page++) {
          var row = '<li>';
          row += '<a class="{option}" data-page="' + page + '" data-transaction="'+conf.TransactionTypeId+'" href="javascript:;">' + page + '</a>';
          row += '</li>';
          if (page == conf.page) row = row.replace('{option}', 'active disable none-event');
          else row = row.replace('{option}', '');
          pages += row;
      }
      html = '<div class="up-n-pager">' + html;
      html = html.replace('{total_rows}', total_rows).replace('{pages}', pages).replace('{first_page}', first_page).replace('{prev_page}', prev_page).replace('{next_page}', next_page).replace('{last_page}', last_page);
      html += '</div>';
      $(root).find('.'+conf.classPaging).html(html);
  };
  actions.event = function () {
      $(root).on('click', '.'+conf.classPaging+' a', function (e) {
          if(!$(this).hasClass('disable')){
              var customerId = parseInt($('input#customerId').val());
              var data = {
                  page : $(this).attr('data-page'),
                  CustomerId : customerId,
                  TransactionTypeId : $(this).attr('data-transaction')
              };
              $.ajax({
                  type: "POST",
                  url: conf.urlPaging,
                  data: data,
                  success: function (response) {
                      var dataPaging = $.parseJSON(response);
                      render(dataPaging.callBackTable, dataPaging.dataTables);
                      conf.page = parseInt(dataPaging.page);
                      conf.pageSize = parseInt(dataPaging.pageSize);
                      conf.totalRow = parseInt(dataPaging.totalRow);
                      conf.CustomerId = customerId;
                      actions.render();
                  },
                  error: function (response) {
                      showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                  }
              });
          }
          return false;
      });

  };
  actions.init();
  actions.render();
  if(conf.registerEvent) actions.event();
  return root;
};

function render(call_back, data) {
    window[call_back](data);
}