var app = app || {};

app.init = function(customerConsultId) {
    app.initLibrary();
    app.customer();
    app.product();
    app.addComment(customerConsultId);
    app.submit(customerConsultId);
    app.updateFacebook(customerConsultId);
    if(customerConsultId > 0) app.updatePercentSell(customerConsultId);
    app.success();
};
app.initLibrary = function() {
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('input#consultDate').datetimepicker({
        format: 'dd/mm/yyyy hh:ii',
        startDate: new Date()
    }).on('changeDate', function(e){
        $(this).datetimepicker('hide');
    });
    $('input.iCheckCommentType').on('ifToggled', function (e) {
        if (e.currentTarget.checked) {
            if (e.currentTarget.value == '2') $('#divCustomerFacebook').show();
            else $('#divCustomerFacebook').hide();
        }
    });
    $('#spanCopyFb').click(function(){
        var copyText = document.getElementById('consultFacebook');
        copyText.select();
        document.execCommand("Copy");
    });
    $('select#remindStatusId option[value="1"]').hide();
    $('select#remindStatusId').change(function(){
        $("input#commentStatus, input#orderCode").val('');
        if($(this).val() == '1') $('#divCommentStatus').hide();
        else{
            var txt = $(this).val() == '5' ? 'thành công' : 'thất bại';
            $("#modalConfirmStatus h4").text('Xác nhận trạng thái ' + txt);
            $('.lbCommentStatus').text('Ghi chú ' + txt);
            $('#divCommentStatus').show();
            if($(this).val() == '5'){
                $.ajax({
                    type: "POST",
                    url: $('input#getListOrderUrl').val(),
                    data: {
                        CustomerId: $('input#customerId').val(),
                        OrderStatusIds: '1,2,4,6'
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        var html = '<option value="0">--Đơn đã mua--</option>';
                        $.each(json.data, function(k,v){
                            html += "<option value="+v.OrderId+">"+v.OrderCode+"</option>";
                        });
                        $("select#order_Id").html(html);
                        return false;
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
                $('.divOrderCode').show();
            }
            else $('.divOrderCode').hide();
            $("#modalConfirmStatus").modal('show');
        }
    });
    $('input#orderCode').blur(function(){
         var orderCode = $(this).val().trim();
        orderCode = orderCode.replace('DH-', '');
        orderCode = parseInt(orderCode);
        if(orderCode > 0) $('input#orderId').val(orderCode);
        else $('input#orderId').val('0');
    });
};

app.customer = function(){
    chooseCustomer(function (li) {
        var customerId = parseInt(li.attr('data-id'));
        var customerConsultId = parseInt(li.attr('data-customer_consult_id'));
        $('input#customerId').val('0');
        showInfoCustomer(customerId, customerConsultId);
    });
    province('provinceId', 'districtId', 'wardId');
    province('customerProvinceId', 'customerDistrictId', 'customerWardId');
    $('#ulCustomerKindId').on('click', 'a', function(){
        var id = $(this).attr('data-id');
        $('input#customerKindId').val(id);
        if(id == '2') $('#divWholesale').show();
        else{
            $('input#debtCost, select#paymentTimeId').val('0');
            $('#divWholesale').hide();
        }
        $('select#customerGroupId .op').hide();
        $('select#customerGroupId .op_' + id).show();
        $('select#customerGroupId').val('0');
    });
    $('input.iCheckCustomerType').on('ifToggled', function (e) {
        if (e.currentTarget.checked) {
            if (e.currentTarget.value == '2') $('#divCompany').show();
            else $('#divCompany').hide();
        }
    });
    $('select#countryId, select#customerCountryId').change(function () {
        if ($(this).val() == '232' || $(this).val() == '0') {
            $('.VNoff').hide();
            $('.VNon').fadeIn();
        }
        else {
            $('.VNon').hide();
            $('.VNoff').fadeIn();
        }
    });
    //END add customer===================
    if($('input#customerId').val() != '0') showInfoCustomer($('input#customerId').val());
    $('#btnCloseBoxCustomer').click(function (){
        $('#divCustomer').hide();
        $('.boxChooseCustomer').show();
        $('input#customerId').val('0');
        return false;
    });
    $('#btnAddCustomer').click(function () {
        $('#customerForm').trigger('reset');
        if(parseInt($("input#customerId").val()) == 0 && parseInt($('input#customerConsultId').val()) > 0){
            var fullName = $("h4.n-name").text();
            var phone = $("div.n-phone").text();
            var firstName = fullName.split(' ').slice(0, -1).join(' ');
            var lastName = fullName.split(' ').slice(-1).join(' ');
            $("input#firstName").val(firstName);
            $("input#lastName").val(lastName);
            $("input#phoneNumber").val(phone);
        }
        $('#modalAddCustomer').modal('show');
    });
    $('#btnUpdateCustomer').click(function () {
        var btn = $(this);
        if (validateEmpty('#customerForm')) {
            var password = $('input#password').val().trim();
            if(password != ''){
                if(password != $('input#rePass').val().trim()){
                    showNotification('Mật khẩu không trùng', 0);
                    return false;
                }
            }
            btn.prop('disabled', true);
            var isReceiveAd = 1;
            if ($('input#isReceiveAd').parent('div').hasClass('checked')) isReceiveAd = 2;
            $.ajax({
                type: "POST",
                url: $('#customerForm').attr('action'),
                data: {
                    CustomerId: 0,
                    FirstName: $('input#firstName').val().trim(),
                    LastName: $('input#lastName').val().trim(),
                    //FullName: $('input#fullName').val().trim(),
                    Email: $('input#email').val().trim(),
                    PhoneNumber: $('input#phoneNumber').val().trim(),
                    //PhoneNumber2: $('input#phoneNumber2').val().trim(),
                    GenderId: $('input[name="GenderId"]:checked').val(),
                    StatusId: 2,
                    BirthDay: $('input#birthDay').val(),
                    CustomerTypeId: $('input[name="CustomerTypeId"]:checked').val(),
                    CustomerKindId: $('input#customerKindId').val(),
                    CountryId: $('select#countryId').val(),
                    ProvinceId: $('select#provinceId').val(),
                    DistrictId: $('select#districtId').val(),
                    WardId: $('select#wardId').val(),
                    ZipCode: $('input#zipCode').val(),
                    Address: $('input#address').val().trim(),
                    CustomerGroupId: $('select#customerGroupId').val(),
                    FaceBook: $('input#facebook').val().trim(),
                    Comment: $('#customerComment').val().trim(),
                    CareStaffId: $('select#careStaffId').val(),
                    DiscountTypeId: $('select#discountTypeId').val(),
                    PaymentTimeId: $('select#paymentTimeId').val(),
                    CTVId: 0,
                    PositionName: $('input#positionName').val().trim(),
                    CompanyName: $('input#companyName').val().trim(),
                    TaxCode: $('input#taxCode').val().trim(),
                    DebtCost: replaceCost($('input#debtCost').val().trim(), true),
                    Password: password,
                    //TagNames: JSON.stringify(tags),
                    IsReceiveAd: isReceiveAd
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) {
                        showInfoCustomer(json.data);
                        $('#modalAddCustomer').modal('hide');
                    }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        return false;
    });
};

app.product = function(){
    chooseProduct(function(tr){
        var id = tr.attr('data-id');
        var childId = tr.attr('data-child');
        var flag = false;
        $('#tbodyProduct tr').each(function(){
            if($(this).attr('data-id') == id && $(this).attr('data-child') == childId){
                flag = true;
                return false;
            }
        });
        if(!flag) {
            var html = '<tr data-id="' + id + '" data-child="' + childId + '">';
            html += '<td><img src="' + tr.find('.productImg').attr('src') + '" class="productImg"><a href="javascript:void(0)" class="light-dark" target="_blank">' + tr.find('td.productName').text() + '</a></td>';
            html += '<td class="tdPrice text-right">' + tr.find('td.text-right').text() + '</td>';
            html += '<td><input type="text" class="form-control quantity" value="1"></td>';
            html += '<td><input type="text" class="form-control comment" value=""></td>';
            html += '<td><a href="javascript:void(0)" class="link_delete"><i class="fa fa-times" title="Xóa"></i></a></td></tr>';
            $('#tbodyProduct').append(html);
        }
    });
    $('#tbodyProduct').on('click', '.link_delete', function () {
        $(this).parent().parent().remove();
        return false;
    });
};

app.addComment = function(customerConsultId){
    $('#btnInsertComment').click(function(){
        var comment = $('input#comment').val().trim();
        if(comment != ''){
            var commentTypeId = parseInt($('input[name="CommentTypeId"]:checked').val());
            if(customerConsultId > 0){
                var btn = $(this);
                btn.prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('input#insertCommentUrl').val(),
                    data: {
                        CustomerConsultId: customerConsultId,
                        Comment: comment,
                        CommentTypeId: commentTypeId
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1) {
                            $('div#listComment').prepend(genConsultComment(comment, commentTypeId, json.data.ConsultCommentId));
                            $('input#comment').val('');
                            var userLoginId = $('input#userLoginId').val();
                            if($('#tdSellUser_' + userLoginId).length == 0){
                                var html = '<tr id="tdSellUser_' + userLoginId + '" style="display: none;">';
                                html += '<td>' + $('input#fullNameLoginId').val() + '</td>';
                                html += '<td class="text-center"><span class="spanPercent">0</span> %</td>';
                                html += '<td class="actions text-center"><a href="javascript:void(0)" class="link_edit" data-id="' + json.data.CustomerConsultUserId + '" data-consult="' + json.data.ConsultCommentId + '"><i class="fa fa-pencil"></i></a>';
                                html += '<input type="text" hidden="hidden" class="comment" value=""></td></tr>';
                                $('#tbodySell').prepend(html);
                                //$('#divSell').show();
                            }
                        }
                        else showNotification(json.message, json.code);
                        btn.prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop('disabled', false);
                    }
                });
            }
            else{
                $('div#listComment').prepend(genConsultComment(comment, commentTypeId, 0));
                $('input#comment').val('');
            }
        }
        else{
            showNotification('Vui lòng nhập ghi chú', 0);
            $('input#comment').focus();
        }
    });
    $('#aShowComment').click(function(){
        $('#modalItemComment').modal('show');
        return false;
    });
};

app.success = function(){
    $('body').on('change', 'select#order_Id', function(){
        if(parseInt($(this).val()) > 0){
            $("input.check_order").show();
            $("div#createOrder").hide();
        }
        else $("input.check_order").hide();
    }).on('click','button.btnCreateOrder', function(){
        $("div#createOrder").show();
        $("input.check_order").hide();
        $("select#order_Id").val(0).trigger('change');
    });
};

app.submit = function(customerConsutId){
    $("#customerConsultForm").submit(function(e){
        e.preventDefault();
    });
    $('.submit').click(function() {
        if(validateEmpty('#customerConsultForm')){
            var checkCreateOrder = $(this).attr('data-order');
            var customerId = parseInt($('input#customerId').val());
            var phoneNumber = $('div.i-phone').first().text().trim();
            if(phoneNumber.length < 10){
                showNotification('Khách hàng không có số điện thoại', 0);
                return false;
            }
            var commentStatus = $('input#commentStatus').val().trim();
            var remindStatusId = $('select#remindStatusId').val();

            if(customerConsutId > 0 && remindStatusId != '1' && commentStatus.length < 20){
                showNotification('Ghi chú phải ít nhất 20 ký tự', 0);
                $('#modalConfirmStatus').modal('show');
                return false;
            }
            var orderId = parseInt($('select#order_Id').val());
            // if(remindStatusId == '5' && orderId == 0){
            //     showNotification('Vui lòng chọn đơn hàng', 0);
            //     $('#modalConfirmStatus').modal('show');
            //     return false;
            // }
            $('.submit').prop('disabled', true);
            var products = [];
            var comments = [];
            $('#tbodyProduct tr').each(function () {
                products.push({
                    ProductId: parseInt($(this).attr('data-id')),
                    ProductChildId: parseInt($(this).attr('data-child')),
                    Quantity: replaceCost($(this).find('input.quantity').val(), true),
                    Price: replaceCost($(this).find('.tdPrice').text(), true),
                    Comment: $(this).find('input.comment').val().trim()
                });
            });
            var timeProcessed = 0;
            var consultDate = $('input#consultDate').val().trim();
            if(customerConsutId == 0){
                $('#listComment .pComment').each(function () {
                    comments.push({
                        Comment: $(this).text(),
                        CommentTypeId: $(this).attr('data-id')
                    });
                });
                var comment = $('input#comment').val().trim();
                if(comment != ''){
                    comments.push({
                        Comment: comment,
                        CommentTypeId: $('input[name="CommentTypeId"]:checked').val()
                    });
                }
            }
            else{
                if($("input#remindDateOld").val() != consultDate) timeProcessed = parseInt($('input#timeProcessed').val()) + 1;
                else timeProcessed = $('input#timeProcessed').val();
            }
            $.ajax({
                type: "POST",
                url: $('#customerConsultForm').attr('action'),
                data: {
                    CustomerConsultId: customerConsutId,
                    CustomerId: customerId,
                    ConsultTitle: $('input#consultTitle').val().trim(),
                    FullName: $('h4.i-name').first().text(),
                    PhoneNumber: phoneNumber,
                    Facebook: $('input#consultFacebook').val().trim(),
                    ConsultDate: consultDate,
                    RemindStatusId: remindStatusId,
                    UserId: $("input#userId").val(),
                    PartId: $("input#partId").val(),
                    OrderChannelId: 3,
                    CTVId: $('input#ctvId').val(),
                    OrderId: orderId,
                    TimeProcessed : timeProcessed,
                    CommentStatus: commentStatus,

                    Products: JSON.stringify(products),
                    Comments: JSON.stringify(comments),
                    IsFront: 0
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        if(customerConsutId == 0 || customerConsutId != 0){
                            if(checkCreateOrder == 'POS')redirect(false, $('input#urlOrderAddPos').val() + '/'+ customerId+ '/' + json.data);
                            else if(checkCreateOrder == 'ship')redirect(false, $('input#urlOrderAddShip').val() + '/'+ customerId+ '/' + json.data);
                            else redirect(false, $('input#ediCustomerConsultUrl').val() + '/' + json.data);
                        } 
                        else{
                            $('.submit').prop('disabled', false);
                            $('#modalConfirmStatus').modal('hide');
                        }
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            });
        }
    });
};

app.updateFacebook = function(customerConsultId){
    $('#spanEditFb').click(function(){
        var customerId = $('input#customerId').val();
        if(customerId == '0'){
            showNotification('Vui lòng chọn khách hàng', 0);
            return false;
        }
        var facebook = $('input#consultFacebook').val().trim();
        if(facebook == ''){
            showNotification('Facebook không được bỏ trống', 0);
            return false;
        }
        $.ajax({
            type: 'POST',
            url: $('input#updateCustomerFacebookUrl').val(),
            data: {
                CustomerConsultId: customerConsultId,
                CustomerId: customerId,
                Facebook: facebook
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1){
                    $('#spanFacebook').text(facebook);
                    //$('#ulActionLogs').prepend('<li>' + json.data.Comment + '<span class="time">' + getCurrentDateTime(2) + '</span></li>');
                }
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
};

app.updatePercentSell = function(customerConsultId){
    var countConsultUser = $('input#countConsultUser').val();
    if(countConsultUser == '0' || countConsultUser == $('input#countConsultUserHasPercent').val()) $('#divSell').hide();
    $('.listComment').on('click', '.box-customer', function(){
        var userLoginId = $('input#userLoginId').val();
        if(userLoginId == $(this).attr('data-id')){
            var tr = $('#tdSellUser_' + userLoginId);
            if(tr.length > 0) {
                var a = tr.find('a.link_edit');
                $('input#customerConsultUserId').val(a.attr('data-id'));
                $('input#consultCommentId').val(a.attr('data-consult'));
                openModalPercent(tr);
            }
        }
    });
    $('#tbodySell').on('click', 'a.link_edit', function(){
        $('input#customerConsultUserId').val($(this).attr('data-id'));
        $('input#consultCommentId').val($(this).attr('data-consult'));
        openModalPercent($(this).parent().parent());
        return false;
    });
    $('#tbodyUserPercent').on('change', 'input.slider', function(obj){
        $(this).parent().find('.spanPercent').text(obj.value.newValue);
    });
    $('#btnUpdatePercent').click(function(){
        var tr = $('#tbodyUserPercent tr').first();
        var percent = parseInt(tr.find('.spanPercent').text());
        var comment = tr.find('input.comment').val();
        if(percent > 0 && comment != ''){
            var consultCommentId = $('input#consultCommentId').val();
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateCustomerConsultUserUrl').val(),
                data: {
                    CustomerConsultUserId: $('input#customerConsultUserId').val(),
                    ConsultSellId: 0,
                    ConsultCommentId: consultCommentId,
                    Percent: percent,
                    Comment: comment
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        tr = $('#tdSellUser_' + $('input#userLoginId').val());
                        tr.find('.spanPercent').text(percent);
                        tr.find('input.comment').val(comment);
                        tr.show();
                        $('#divSell').show();
                        tr = $('div.listComment .box-customer[data-consult="' + consultCommentId + '"]');
                        if(tr.find('i.fa-flag').length == 0) tr.find('.tdIcon').prepend('<i class="fa fa-flag" style="margin-right: 5px;color: #357ebd;"></i>');
                        $('#modalSell').modal('hide');
                    }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else showNotification('Phần trăm phải lớn hơn 0 và phần trăm không được bỏ trống', 0);
    });
};

$(document).ready(function () {
    var customerConsultId = parseInt($('input#customerConsultId').val());
    app.init(customerConsultId);
});

function showInfoCustomer(customerId, customerConsultId) {
    
    $.ajax({
        type: "POST",
        url: $('input#getCustomerDetailUrl').val(),
        data: {
            CustomerId: customerId,
            CustomerConsultId: customerConsultId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if (json.code == 1) {
                var data = json.data;
                $('input#customerId').val(customerId);
                if (data.PhoneNumber != '' && data.PhoneNumber2 != ''){
                    $("h3.PhoneNumber").html(data.PhoneNumber);
                    $("input#phone-input").val(data.PhoneNumber);
                    data.PhoneNumber = data.PhoneNumber + ' - ' + data.PhoneNumber2;
                }
                var address = '';
                if (data.CountryId == '232') {
                    address += '<i class="fa fa-map-marker" aria-hidden="true"></i>';
                    //if (data.WardName != '')
                    address += '<span class="i-ward"><span class="spanAddress">' + data.Address + '</span> ' + data.WardName + '</span>';
                    if (data.DistrictName != '') address += '<span class="br-line i-district">' + data.DistrictName + '</span>';
                    if (data.ProvinceName != '') address += '<span class="br-line i-province">' + data.ProvinceName + '</span>';
                }
                else {
                    address += '<i class="fa fa-list-alt" aria-hidden="true"></i>';
                    address += '<span class="i-province">ZipCode: ' + data.ZipCode + '</span>';
                }
                var editCustomerUrl = $('input#editCustomerUrl').val() + '/' + customerId + '/';
                var cssName = 'name_info_blue';
                $('h4.i-name').removeClass('name_info_green').addClass(cssName);
                if(parseInt(data.CustomerId) == 0){
                    $('h4.i-name').removeClass('name_info_blue');
                    cssName = 'name_info_green';
                }
                $('h4.i-name').addClass(cssName).html('<a href="' + editCustomerUrl + '3" target="_blank">' + data.FullName + '</a>');
                
                $('span.i-name').text(data.FullName);
                $('.i-phone').text(data.PhoneNumber);
                $('.n-phone').text(data.PhoneNumber);
                if(customerId != '0'){
                    $('.i-total-orders').html('<a href="' + editCustomerUrl + '1" target="_blank">' + data.TotalOrders + '</a>');
                    $('span#customerBalance').html('<a href="' + editCustomerUrl + '2" target="_blank">' + formatDecimal(data.Balance) + '</a>');
                    $('#spanFacebook').text(data.Facebook);
                    $('input#consultFacebook').val(data.Facebook);
                }
                if(data.CustomerGroupName != '') $('.i-cusType').html('<span class="label label-success">' + data.CustomerGroupName + '</span>').show();
                else $('.i-cusType').hide();
                // $('.i-email').text(data.Email);
                $('.i-country').text(data.CountryName);
                $('.i-address').html(address);
                $('.boxChooseCustomer').hide();
                $('#divCustomer').show();
            }
            else showNotification(json.message, json.code);
        },
        error: function (response) {
            showNotification('Có  lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

function genConsultComment(comment, commentTypeId, consultCommentId){
    var html = '<div class="box-customer mb10" data-id="' + $('input#userLoginId').val() + '" data-consult="' + consultCommentId + '"><table><tbody><tr><th rowspan="2" class="user-wrapper" valign="top" style="width: 50px;"><div class="user-link"><img class="user-img" width="29" src="' + $('input#userImagePath').val() + $('input#avatarLoginId').val() + '" alt=""></div></th>';
    html +='<th><a href="javascript:void(0)" class="name">' + $('input#fullNameLoginId').val() + '</a></th>';
    html += '<th class="time">' + getCurrentDateTime(6) + '</th></tr><tr><td><p class="pComment" data-id="' + commentTypeId + '">' + comment + '</p></td>';
    html += '<td class="tdIcon"><span class="label ' + (commentTypeId == 1 ? 'label-success' : 'label-default') + '">' + (commentTypeId == 1 ? 'Gọi điện' : 'Facebook') + '</span></td></tr></tbody></table></div>';
    return html;
}

function openModalPercent(tr) {
    var html = '<tr><td>' + $('input#fullNameLoginId').val() + '</td><td class="text-center">';
    var percent = tr.find('.spanPercent').text();
    html += '<input type="text" data-slider-enabled="true" value="' + percent + '" class="slider" data-slider-min="0" data-slider-max="100" data-slider-step="5" data-slider-value="' + percent + '" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="blue"> <span class="spanPercent">' + percent + '</span> %</td>';
    html += '<td><input type="text" class="form-control comment" value="' + tr.find('input.comment').val() + '"></td></tr>';
    $('#tbodyUserPercent').html(html);
    $('input.slider').slider();
    $('#modalSell').modal('show');
}