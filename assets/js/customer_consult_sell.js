$(document).ready(function() {
    dateRangePicker();
    $('#tbodyCustomerConsultSell').on('click', 'a.link_edit', function(){
        var tr = $(this).parent().parent();
        var html = '<tr><td>' + tr.find('.tdOrderCode').html() + '</td><td>' + tr.find('.tdCrDateTime').text() + '</td><td>' + tr.find('.tdCustomerName').html() + '</td><td class="text-right">' + tr.find('.tdSumCost').text() + '</td><td>' + tr.find('.tdCrUserName').text() + '</td></tr>';
        $('#tbodyOrderInfo').html(html);
        html = '';
        var userLoginId = $('input#userLoginId').val();
        var json = $.parseJSON(tr.find('.spanJson').text());
        var i = 0, maxPercent = 0, totalPercent = 0;
        for(i = 0; i < json.length; i++){
            if(json[i].UserId == userLoginId) maxPercent += parseInt(json[i].Percent);
            totalPercent += parseInt(json[i].Percent);
        }
        maxPercent += 100 - totalPercent;
        for(i = 0; i < json.length; i++){
            html += '<tr><td>' + $('select#userId option[value="' + json[i].UserId + '"]').text() + '</td><td class="text-center">';
            html += '<input type="text" data-slider-enabled="' + (json[i].UserId == userLoginId ? 'true' : 'false') + '" value="' + json[i].Percent + '" class="slider" data-slider-min="0" data-slider-max="' + (json[i].UserId == userLoginId ? maxPercent : '100') + '" data-slider-step="5" data-slider-value="' + json[i].Percent + '" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="blue"> <span class="spanPercent">'+ json[i].Percent +'</span> %</td><td>';
            if(json[i].UserId == userLoginId) html += '<input type="text" class="form-control comment" value="' + json[i].Comment + '">';
            else html += json[i].Comment;
            html +=  '</td><td class="text-center">';
            if(json[i].UserId == userLoginId) html += '<a href="javascript:void(0)" class="link_update" title="Cập nhật" data-id="' + json[i].CustomerConsultUserId + '" data-sell="' + json[i].ConsultSellId + '"><i class="fa fa-save"></i></a>';
            html += '</td></tr>';
        }
        $('#tbodyUserPercent').html(html);
        $('input.slider').slider();
        $('#modalSell').modal('show');
        return false;
    });
    $('#tbodyUserPercent').on('change', 'input.slider', function(obj){
        $(this).parent().find('.spanPercent').text(obj.value.newValue);
    }).on('click', 'a.link_update', function(){
        var tr = $(this).parent().parent();
        var id = $(this).attr('data-id');
        var sellId = $(this).attr('data-sell');
        $.ajax({
            type: "POST",
            url: $('input#updateCustomerConsultUserUrl').val(),
            data: {
                CustomerConsultUserId: id,
                ConsultSellId: sellId,
                ConsultCommentId: 0,
                Percent: tr.find('.spanPercent').text(),
                Comment: tr.find('input.comment').val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1){
                    var tr = $('#trSell_' + sellId);
                    var data = json.data;
                    tr.find('.spanJson').text(JSON.stringify(data));
                    var html = '';
                    for(var i = 0; i < data.length; i++){
                        if(parseInt(data[i].Percent) > 0) html += $('select#userId option[value="' + data[i].UserId + '"]').text() + ': ' + data[i].Percent + '% | ';
                    }
                    if(html != '') html = html.substr(0, html.length - 3);
                    tr.find('.tdPercent').text(html);
                    $('#modalSell').modal('hide');
                }
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
        return false;
    });
});