$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('input#iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('input#iCheck').on('ifToggled', function(e){
        if(e.currentTarget.checked) $('input#isShare').val('2');
        else $('input#isShare').val('1');
    });        
    $("#tbodyProductType").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#productTypeId').val(id);
        $('input#productTypeName').val($('td#productTypeName_' + id).text());
        $('input#activeDate').val($('td#activeDate_' + id).text());
        var isShare = $('input#isShare_' + id).val();
        $('input#isShare').val(isShare);
        if(isShare == '2') $('input#iCheck').iCheck('check');
        scrollTo('input#productTypeName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')){
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteProductTypeUrl').val(),
                data: {
                    ProductTypeId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) $('tr#productType_' + id).remove();
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#productTypeForm').trigger("reset");
        $('input#iCheck').iCheck('uncheck');
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#productTypeForm')) {
            var form = $('#productTypeForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        $('input#iCheck').iCheck('uncheck');
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="part_' + data.ProductTypeId + '">';
                            html += '<td id="productTypeName_' + data.ProductTypeId + '">' + data.ProductTypeName + '</td>';
                            if(data.IsShare == 1) html += '<td id="isShareName_' + data.ProductTypeId + '"><label class="label label-default">Không</label></td>';
                            else html += '<td id="isShareName_' + data.ProductTypeId + '"><label class="label label-success">Có</label></td>';
                            html += '<td id="activeDate_' + data.ProductTypeId + '">' + data.ActiveDate + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.ProductTypeId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.ProductTypeId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="isShare_' + data.ProductTypeId + '" value="' + data.IsShare + '">' +
                                 '</td>';
                            html += '</tr>';
                            $('#tbodyProductType').prepend(html);
                        }
                        else{
                            $('td#productTypeName_' + data.ProductTypeId).text(data.ProductTypeName);
                            if(data.IsShare == 1) $('td#isShareName_' + data.ProductTypeId).html('<label class="label label-default">Không</label>');
                            else $('td#isShareName_' + data.ProductTypeId).html('<label class="label label-success">Có</label>');
                            $('td#activeDate_' + data.ProductTypeId).text(data.ActiveDate);
                            $('input#isShare_' + data.ProductTypeId).val(data.IsShare);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});