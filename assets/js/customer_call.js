var app = app || {};
var interval = null;
app.init = function () {
	app.remind();
	app.clickActiveLi();
}

app.remind = function(){
	$('input.datetimepicker').datetimepicker({
        format: 'dd/mm/yyyy hh:ii',
        startDate: new Date()
    }).on('changeDate', function(e){
        $(this).datetimepicker('hide');
    });
	var body = $("body");
	body.on('click', 'a.aRemind', function(){
        $('#modalRemind').modal('show');
        return false;
    });
    $('#btnAddRemind').click(function(){
        if(validateEmpty('#modalRemind')){
            var btn = $(this);
            // btn.prop('disabled', true);
            var remindComment = $('input#remindComment').val().trim();
            var CustomerId = 0
            $(".listchat-each").each(function() {
			    if($(this).hasClass('ul_active') == true){
			    	CustomerId = parseInt($(this).attr('custommerid'));
			    }
			});
			if(CustomerId > 0){
				$.ajax({
	                type: "POST",
	                url: $('input#insertRemindUrl').val(),
	                data: {
	                    CustomerId: CustomerId,
	                    Comment: remindComment,
	                    RemindDate: $("input#remindDate").val().trim(),
	                },
	                success: function (response) {
	                    var json = $.parseJSON(response);
	                    showNotification(json.message, json.code);
	                    if(json.code == 1) {
	                        $("input#remindDate").val('');
	                        $("input#remindComment").val('');
	                        $('#modalRemind').modal('hide');
	                    }
	                    // btn.prop('disabled', false);
	                },
	                error: function (response) {
	                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
	                    // btn.prop('disabled', false);
	                }
	            });
			}else showNotification("Có lỗi xảy ra, vui lòng thử lại.", 0);
            
        }
        return false;
    });
};

app.clickActiveLi = function(){
	$("body").on('click', '.listchat-each', function(e){
		$('.listchat-each').removeClass('ul_active');
   		$('.listchat-each').removeClass('active');
        $(this).addClass('ul_active');
        e.preventDefault();
	})
}

$(document).ready(function(){
	$('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
	app.init();

	$('input[name="number"]').keyup(function(e){
		if (!isNumeric(this.value)){
			this.value = this.value.replace(/\D/g, '');
		}
	});
	$('input[name="number"]').keydown(function(e){
		if (!isNumeric(this.value)){
			this.value = this.value.replace(/\D/g, '');
		}
	});

    $('input.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    province('provinceId', 'districtId', 'wardId');

	var commentArrGlobal = [];

    getCallLog(1,"#custommer-call", 0);

	$("#nav-custommer-call").click(function() {
		var here = $(this).attr('href');
		clearRender();
		getCallLog(1,here, 0);
	});
	
	$("#nav-custommer-called").click(function() {
		var here = $(this).attr('href');
		clearRender();
		getCallLog(2,here, 0);
	});
	
	$("#nav-custommer-miss-called").click(function() {
        var here = $(this).attr('href');
        clearRender();
        getCallLog(3,here, 0);
    });

	$('body').on('click', '.save-tag-custommer', function() {
		var arrTag = $('.tag-input-custommer').tagsinput('items');
		if(arrTag[0].length > 0) arrTag = arrTag[0];
		else arrTag = arrTag[1];
		var itemsId = [commentArrGlobal[0].Information.CustomerId];
		$.ajax({
		    type: "POST",
		    url: "api/tag/updateItem",
		    data: {
		    	ItemIds: JSON.stringify(itemsId),
		        TagNames: JSON.stringify(arrTag),
		        ItemTypeId: 6,
                ChangeTagTypeId: 1
		    },
		    dataType: 'json',
		    success: function (response) {
                showNotification(response.message, response.code);
		    },
		    error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
		    }
		});
	})

	$("body").on('keyup', 'input.txt-input-comment', function(){
		$("input.txt-input-comment-2").val($(this).val())
	})


	$('body').on('click', '.add-comment', function() {
		var id = commentArrGlobal[0].Information.CustomerId;
		var commentText = $('body input.txt-input-comment-2').val();
		$.ajax({
		    type: "POST",
		    url: "api/customer/insertComment",
		    dataType: 'json',
		    data: {
		    	Comment: commentText,
		        CustomerId: id
		    },
		    success: function (response) {
                showNotification(response.message, response.code);
                $('div.person-info-comment').prepend(genItemCommentCall(commentText));
                $('input.txt-input-comment').val('');
                $("input.txt-input-comment-2").val('')
		    },
		    error: function (response) {
		        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
		    }
		});
	})

	$('body').on('click', '.views-more-comment', function() {
		var customerId = commentArrGlobal[0].Information.CustomerId;
		$.ajax({
		    type: "POST",
		    url: "api/customer/showComment",
		    dataType: 'json',
		    data: {
		        CustomerId: customerId
		    },
		    success: function (response) {
		    	if(response.code == 1){
		    		var ele = renderCustommerAllComment(response.datas);
		    		$('body .list-custommer-comment').html(ele);
		    		$('#modalComments').modal('show');
		    	}else showNotification(response.message, response.code);
		    },
		    error: function (response) {
		        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
		    }
		});
		// $('#modalComments').modal('show');
		// $('body .list-custommer-comment').empty();
		// var ele = renderCustommerAllComment(commentArrGlobal[0].Comments);
		// $('body .list-custommer-comment').append(ele);
	});

	$('input#customerActiveId').on('ifToggled', function(e){
        if(e.currentTarget.checked) $('input#customerActiveId').val('2');
        else $('input#customerActiveId').val('1');
    });

	$('body').on('click', '.btn-edit', function (e) {
		var infoCustomer = commentArrGlobal[0].Information;
		console.log(infoCustomer)
        $("input#fullName").val(infoCustomer['FullName']);
        $("select#customerTypeId").val(infoCustomer['CustomerTypeId']).trigger('change');
        $("input#phoneNumber").val(infoCustomer['PhoneNumber']);
        $("select#roleId").val(infoCustomer['RoleId']).trigger('change');
        $("input#email").val(infoCustomer['Email']);
        $("select#genderId").val(infoCustomer['GenderId']).trigger('change');
        $("input#birthDay").val(infoCustomer['Birthday']);
        // $("input#facebook").val(infoCustomer['Facebook']);
        $("input#customerComment").val(infoCustomer['CustomerComment']);
        // $("input#address").val(infoCustomer['Address']);
        // $("select#countryId").val(infoCustomer['CountryId']).trigger('change');
        // $("select#provinceId").val(infoCustomer['ProvinceId']).trigger('change');
        // $("select#districtId").val(infoCustomer['DistrictId']).trigger('change');
        // $("select#wardId").val(infoCustomer['WardId']).trigger('change');
        // $("input#zipCode").val(infoCustomer['ZipCode']);
        if(infoCustomer['customerOrder'] != undefined){
        	if(infoCustomer['customerOrder'].length > 0){
	        	var order = infoCustomer['customerOrder'][0]
	        	$("select#companyId").val(order['CompanyId']).trigger('change');
	        	$("select#product").val(order['Product']).trigger('change');
	        	$("select#landingPageId").val(order['LandingPageId']).trigger('change');
	        	$("select#campaignId").val(order['CampaignId']).trigger('change');
	        	$("input#comment").val(order['Comment']);

	        }
        }
        
        if(infoCustomer['CustomerActiveId'] == '2') $('input#customerActiveId').iCheck('check');
        
        $("#modalAddCustomer").modal('show');

    });

    $('select#countryId').change(function () {
        if ($(this).val() == '232' || $(this).val() == '0') {
            $('.VNoff').hide();
            $('.VNon').fadeIn();
        }
        else {
            $('.VNon').hide();
            $('.VNoff').fadeIn();
        }
    });

	$('body').on('click', '.listchat-each', function(e) {
		var CustomerId = parseInt($(this).attr('custommerid'));
		var PhoneNumber = $(this).attr('data-phone');
		$('#phone-input').val(PhoneNumber);
        // $('#call-button-number').show();
        // $('#call-end-number').hide();
		$.ajax({
		    type: "POST",
		    url: "api/customer/getCallLog",
		    dataType: 'json',
		    data: {
		        CustomerId: CustomerId,
				PhoneNumber:PhoneNumber,
                UserId: $('input#userLoginId').val()
		    },
		    success: function (response) {
		    	commentArrGlobal = [];
		    	commentArrGlobal.push(response.data);
				var idx = setDataId(CustomerId,PhoneNumber);
		    	$(idx).find('.block-desc').empty();
				$(idx).find('.block-enterchat').empty();
		    	$(".detail-chat-other").empty();
		        var ele = renderCustommerInfo(response.data);
		        var eleDtl = renderListRecordCustommer(response.data);
		    	$(idx).find('.official-title').text(response.data.Information.FullName);
				$(idx).find('.block-desc').html(ele);
				$(idx).find('.block-enterchat').html(renderButtonCall());
		    	$(".detail-chat-other").html(eleDtl);
		    	$('.tagsinput').tagsinput('refresh');
				callScroll_Tab2(idx);
				if(CustomerId > 0) $("div.check_customer_id").show();
				else $("div.check_customer_id").hide();
				$(".content_comment_call").html('');
				showCommentCall(CustomerId);
		    },
		    error: function (response) {
		        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
		    }
		});
		
	});

    $('body').on('click', '#btnUpdateCustomer', function (e) {
    	if (validateEmpty('#customerForm')) {
    		var checkId = "";
    		if($("ul.custommer-info").hasClass('ul_active') == true) checkId = $("ul.custommer-info").attr('id');
    		var form = $('#customerForm');
            var btn = $(this);
            var datas = form.serializeArray();
            datas.push({ name: "CustomerId", value: commentArrGlobal[0].Information.CustomerId });
             datas.push({ name: "PhoneNumber", value: $('input#phoneNumber').val().trim() });
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: datas,
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                    	var here = "";
                    	var num = 0;
                    	// clearRender();
                    	if($("#nav-custommer-call").hasClass('active') == true){
                    	  here = $("#nav-custommer-call").attr('href');
                    	  num = 1;
                    	}else if($("#nav-custommer-called").hasClass('active') == true){
                    		here = $("#nav-custommer-called").attr('href');
                    	  	num = 2;
                    	}
                    	
                    	if(here != "" && num > 0){
                    		$('body').find('.listchat-info').empty();
                    		getCallLog(num,here, json.customerId);
                    		$('#'+checkId+'').attr('custommerid', json.customerId);
                			$(".detail_full_name").html($("input#fullName").val());
                    		if(json.customerId > 0) $("div.check_customer_id").show();
							else $("div.check_customer_id").hide();
                    	} 
                    }
                    $('#modalAddCustomer').modal('hide');
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
    	}
    	return false;
    
    });

});

function showCommentCall(customerId){
	$.ajax({
	    type: "POST",
	    url: "api/customer/showComment",
	    dataType: 'json',
	    data: {
	        CustomerId: customerId
	    },
	    success: function (response) {
	    	if(response.code == 1){
	    		console.log(response.datas)
	    		if(response.datas.length > 0){
	    			var html = '';
	    			$.each(response.datas, function(k,v){
	    				html += '<div class="person-info-comment1">';
                        html +=     '<ul class="comment-block">';
                        html +=         '<li class="media-header"><a href="javascript:void(0)"><h5 class="name">'+v.FullName+'</h5></a></li>';
                        html +=         '<li><p class="comment-time text-secondary">'+ getDayText(v.DayDiff) + v.CrDateTime+'</p></li>';
                        html +=         '<li><p class="comment">'+v.Comment+'</p></li>';
                        html +=      '</ul>';
                        html +=  '</div>';
	    			});
	    			$(".content_comment_call").html(html);
	    		}
	    		// var ele = renderCustommerAllComment(response.datas);
	    		// $('body .list-custommer-comment').html(ele);
	    		// $('#modalComments').modal('show');
	    	}else showNotification(response.message, response.code);
	    },
	    error: function (response) {
	        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
	    }
	});
}

function getCallLog(tab,here, customerId) {
	if(!(tab) && !(here)){
		showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
		return;
	}
	$.ajax({
		type: "POST",
		url: 'api/customer/getListCallLog',
		dataType: 'json',
		data: {
			tab: tab
		},
		success: function (result) {
			if(result.code == 1){
				callRender(here,result.data, customerId);
				
			}
		},
		error: function (response) {
			showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
		}
	});
}

function clearRender(){
	$('body').find('.listchat-info').empty();
	$('body').find('.detail-chat-block').empty();
	$('body').find('.detail-chat-other').empty();
}

function callRender(here,data, customerId){
	var ele = renderListCustommerAll(data);
	$(here).find(".listchat-info").html(ele);
	var ala = renderDefaultDetail(data);
	$(here).find(".detail-chat-block").html(ala);
	whatsThis();
	callScroll_Tab1(here);
	$('ul#custommer-info-'+customerId).addClass('ul_active');
}

function renderCustommerInfo(data) {
	return renderRecordDetail(data.ListCalls);
}

function renderListCustommerAll(data) {
	var html = '';
	data.map(function(v) {
		var CustomerId = setDataTarget(v);
		html+= '<ul class="media listchat-each custommer-info" custommerId='+v.CustomerId+' id="custommer-info-'+CustomerId+'" data-target="#contentchat'+CustomerId+'" data-phone="'+v.PhoneNumber+'">';
			html+= '<li class="media-body">';
				html+= '<div class="row">';
					html+= '<div class="col-9 no-padding-right bboxchat-left">';
						html+= '<h2 class="title">'+v.FullName.split('(')[0]+'</h2>';
						html+= '<p class="desc">'+v.PhoneNumberHidden+'</p>';
					html+= '</div>';
					//getDayText(data[item].DayDiff) + data[item].CrDateTime
					if(v.dateTime){
						html+= '<div class="col-3 no-padding-left bboxchat-right">';
							html+= '<span class="time">'+getDayText(v.DayDiff) +v.dateTime+'</span>';
						html+= '</div>';
					}
				html+= '</div>';
			html+= '</li>';
		html+= '</ul>';
	});
	return html;
}


function renderDefaultDetail(data) {
	var html = '';
	data.map(function(v) {
		var CustomerId = setDataTarget(v);
		html+= '<div class="detail-chat-info" id="contentchat'+CustomerId+'">';
			html+=renderNameCustommer();
			html+='<div class="block-desc"></div>';
			html+='<div class="col-12 block-enterchat"></div>';
		html+= '</div>';
	});
	return html;
}

function renderNameCustommer() {
	var html = '';
	html += '<div class="d-flex flex-row block-user">';
		html += '<ul class="media">';
			html += '<li class="media-body align-self-center">';
				html += '<h2 class="title official-title"></h2>';
			html += '</li>';
		html += '</ul>';
	html += '</div>';
	return html;
}

function renderRecordDetail(listCalls) {
	var html = '';
	listCalls.map(function(v) {
		if(v.FileUrl != 'null') {
			var style = v.IsFromCustomer == 1 ? "justify-content-end" : "";
			html += '<div class="d-flex flex-row clearfix '+style+'">';
				html += '<div class="media myuser-chat align-self-start">';
					html += '<div class="media-body">';
						html += '<audio controls="controls">';
							html += '<source src="'+v.FileUrl+'" type="audio/mpeg">';
						html += '</audio>';
						html += '<div class="block-infox">';
							html += '<span class="span text-secondary user-info">'+v.FullName+'</span>';
							html += '<span class="span text-secondary date-record">'+v.BeginTime+'</span>';
						html += '</div>';
					html += '</div>';
				html += '</div>';
			html += '</div>';
		}
	});
	return html;
}

function renderButtonCall() {
	var html = '';
	//html += '<div class="col-12 block-enterchat">';
		// html += '<form>';
		// 	html += '<div class="form-row">';
		// 		html += '<div class="col-12 text-center button-control box-call-end"  style="display: none;">';
		// 			html += '<button type="button" class="btn btn-danger" id="call-end" >';
		// 				html += '<i class="fa fa-phone"></i>';
		// 			html += '</button>';
		// 		html += '</div>';
		// 		html += '<div class="col-12 text-center button-control box-call-start">';
		// 			html += '<button id="call-button" type="button" value="Call" class="btn btn-success">';
		// 				html += '<i class="fa fa-phone"></i>';
		// 			html += '</button>';
		// 		html += '</div>';
		// 	html += '</div>';
	 //   html += ' </form>';
	 html += '<form>';
			html += '<div class="form-row">';
				html += '<div class="col-12 text-center button-control box-call-start">';
					html += '<button id="detail-call" type="button" class="btn btn-success detail-call">';
						html += '<i class="fa fa-phone"></i>';
					html += '</button>';
				html += '</div>';
			html += '</div>';
	   html += ' </form>';
	//html += '</div>';

	return html;
}

function renderListRecordCustommer(data) {
	var html = '';
	html += '<div class="tab-content tab-other-content" id="pills-tabContent">';
		html += '<div class="tab-pane fade show active" id="pills-home" role="tabpanel">';
			html += '<div class="person-info-more">';
				html += '<div class="media">';
					html += '<div class="media-header">';
						html += '<h3 class="name"><a href="javascript:void(0)" class="detail_full_name">'+data.Information.FullName+'</a></h3>';
					html += '</div>';
					html += '<div class="media-body">';
						html += '<button type="button" class="btn btn-edit"> Thông tin khách hàng <i class="fa fa-pencil"></i></button>';
						html += '<div class="d-flex flex-row">';
							html += '<ul>';
								html += '<li><i class="fa fa-user"></i><span class="detail_full_name">'+data.Information.FullName+'</span></li>';
								html += '<li><i class="fa fa-phone"></i><span class="detail_phone_number">'+data.Information.PhoneNumber+'</span></li>';
								if(data.Information.Email) html += '<li><i class="fa fa-envelope-o"></i></span class="detail_email">'+data.Information.Email+'</span></li>';
								html += '<li><i class="fa fa-map-marker"></i>"Lấy theo info trên fb"</li>';
							html += '</ul>';
						html += '</div>';
					html += '</div>';
				html += '</div>';
			html += '</div>';
			html += '<div class="check_customer_id">'
				html += renderTagDefault(data.TagNames);

				html += '<div class="person-info-tag">';
					html += '<a href="javascript:void(0)" class="aRemind"><img src="assets/vendor/dist/img/icon08.png"> Tạo chương trình nhắc nhở</a>';
				html += '</div>';

				html += '<div class="person-info-note">';
					html += '<h3 class="title"><i class="fa fa-thumb-tack"></i>Thêm ghi chú</h3>';
					html += '<form>';
						html += '<div class="input-group">';
						html += '<input text="hidden" hidden="hidden" class="form-control txt-input-comment-2">'
							html += '<input type="text" class="form-control txt-input-comment"  placeholder="Thêm ghi chú...">';
							html += '<div class="input-group-append">';
								html += '<button type="button" class="btn btn-primary add-comment">Lưu</button>';
							html += '</div>';
						html += '</div>';
					html += '</form>';
				html += '</div>';
				html += '<div class="person-info-comment">';
					html += renderCustommerComment(data.Comments);
					html += '<div class="text-center"><a class="btn btn-primary views-more-comment" href="javascript:void(0);">Xem tất cả >></a></div>';
				html += '</div>';
			html += '</div>';
		html += '</div>';
	html += '</div>';
	return html;
}

function renderTagDefault(data) {
	var html = '';
	html += '<div class="person-info-tag">';
		html += '<h3 class="title"><i class="fa fa-tag"></i>Thêm tags khách hàng</h3>';
		html += '<input type="text" class="tagsinput tag-input-custommer" value="'+data.toString()+'" data-role="tagsinput"/>';
		html += '<div class="text-center">';
		html += '<button class="btn btn-primary save-tag-custommer">Save</button>';
		html += '</div>';
	html += '</div>';
	return html;
}

function renderCustommerComment(data) {
	var html = '';
	data.splice(2);
	data.map(function(v) {
		html += '<ul class="comment-block">';
			html += '<li class="media-header"><a href="#"><h5 class="name">'+v.FullName+'</h5></a></li>';
			html += '<li><p class="comment-time text-secondary">'+ getDayText(v.DayDiff) + v.CrDateTime+'</p></li>';
			html += '<li><p class="comment">'+v.Comment+'</p></li>';
		html += '</ul>';
	});
	return html;
}

function renderCustommerAllComment(data) {
	var html = '';
	data.map(function(v) {
		html += '<div class="person-info-comment">';
			html += '<ul class="comment-block">';
			html += '<li class="media-header"><a href="#"><h5 class="name">'+v.FullName+'</h5></a></li>';
			html += '<li><p class="comment-time text-secondary">'+ getDayText(v.DayDiff) + v.CrDateTime+'</p></li>';
			html += '<li><p class="comment">'+v.Comment+'</p></li>';
			html += '</ul>';
		html += '</div>';
	});
	return html;
}

function setDataId(CustomerId,PhoneNumber){
	var id;
	if (CustomerId == 0){
		id = PhoneNumber;
	}else{
		id = CustomerId;
	}
	return '#contentchat'+id;
}
function setDataTarget(v){
	var newData=0;
	if(v.CustomerId==0){
		newData = v.PhoneNumber;
	}else{
		newData=v.CustomerId;
	}
	return newData;
}

function genItemCommentCall(comment){
	var html = '<ul class="comment-block">';
		html += '<li class="media-header"><a href="#"><h5 class="name">'+$('input#fullNameLoginId').val()+'</h5></a></li>';
		html += '<li><p class="comment-time text-secondary">'+getCurrentDateTime(6)+'</p></li>';
		html += '<li><p class="comment">'+comment+'</p></li>';
		html += '</ul>';
    return html;
}
