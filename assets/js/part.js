$(document).ready(function(){
    $("#tbodyPart").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#partId').val(id);
        $('input#partCode').val($('td#partCode_' + id).text());
        $('input#partName').val($('td#partName_' + id).text());
        $('select#parentPartId').val($('input#parentPartId_' + id).val());
        scrollTo('input#partCode');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deletePartUrl').val(),
                data: {
                    PartId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#part_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#partForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#partForm')) {
            var form = $('#partForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        var parentPartName = '';
                        if(data.ParentPartId != '0') parentPartName = $('td#partName_' + data.ParentPartId).text();
                        if(data.IsAdd == 1){
                            var html = '<tr id="part_' + data.PartId + '">';
                            html += '<td id="partCode_' + data.PartId + '">' + data.PartCode + '</td>';
                            html += '<td id="partName_' + data.PartId + '">' + data.PartName + '</td>';
                            html += '<td id="parentPartName_' + data.PartId + '">' + parentPartName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.PartId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.PartId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="parentPartId_' + data.PartId + '" value="' + data.ParentPartId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyPart').prepend(html);
                            if(data.ParentPartId ==  '0') $('select#parentPartId').append('<option value="' + data.PartId + '">' + data.PartName + '</option>');
                        }
                        else{
                            $('td#partCode_' + data.PartId).text(data.PartCode);
                            $('td#partName_' + data.PartId).text(data.PartName);
                            $('td#parentPartName_' + data.PartId).text(parentPartName);
                            $('input#parentPartId_' + data.PartId).val(data.ParentPartId);
                            var option = $('select#parentPartId option[value="' + data.PartId + '"]');
                            if(data.ParentPartId == '0'){
                                if(option.length > 0) option.text(data.PartName);
                                else $('select#parentPartId').append('<option value="' + data.PartId + '">' + data.PartName + '</option>');
                            }
                            else if(option.length > 0) option.remove();
                        } 
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});