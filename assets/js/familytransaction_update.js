var app = app || {};

app.init = function(familyTransactionId, canEdit) {
    app.initLibrary();
    app.chooseBank(familyTransactionId);
    app.updateVerifyLevel(familyTransactionId);
    if(familyTransactionId > 0 && canEdit == 1) app.cancelTransaction(familyTransactionId);
};

app.initLibrary = function(){
    $('input.iCheckRadio').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('select#transactionKindId1').change(function(){
        $('select#transactionKindId2 option').hide();
        $('select#transactionKindId2 option[value="0"]').show();
        $('select#transactionKindId2 option[data-id="' + $(this).val() + '"]').show();
        $('select#transactionKindId2').val('0');
    });
    //$('#moneySourceId option[value="5"]').hide();
    var moneySourceId = $('#moneySourceId').val();
    $('.divMoneySource').hide();
    $('.divMoneySource_' + moneySourceId).show();
    $('#moneySourceId').change(function(e) {
        moneySourceId = $(this).val();
        $('.divMoneySource select').val('0');
        $('input#bankId').val('0');
        $('#bankInfo').html('');
        $('.divMoneySource').hide();
        $('.divMoneySource_' + moneySourceId).show();
        if(moneySourceId == '2'){
            $('#listBank li').removeClass('active');
            $('input#bankIdTmp').val('0');
            //$('#modalChooseBank').modal('show');
            $('#selectBank').show();
        }
    });

    $('input#paidCost').keydown(function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).keyup(function (e) {
        var value = formatDecimal($(this).val());
        $(this).val(value);
    });
};

app.updateVerifyLevel = function(familyTransactionId){
    $('#btnUpdateVerifyLevel').click(function(){
        var btn = $(this);
        var verifyLevelId = parseInt($('input[name="VerifyLevelId"]:checked').val());
        if(verifyLevelId > 0){
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateVerifyLevelUrl').val(),
                data: {
                    FamilyTransactionId: familyTransactionId,
                    VerifyLevelId: verifyLevelId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(true, '');
                    else btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else showNotification('Vui lòng chọn trạng thái duyệt', 0);
    });
};

app.chooseBank = function(familyTransactionId){
    var bankId = $('input#bankId').val();
    if(bankId != '0' && familyTransactionId > 0){
        $('#bankInfo').html($('#listBank li[data-id="' + bankId + '"]').html());
        $('#selectBank').hide();
    }
    else $('#selectBank').show();
    $('#iBank, #selectBank').click(function(){
        bankId = $('input#bankId').val();
        $('input#bankIdTmp').val(bankId);
        if(bankId != '0'){
            $('#listBank li').removeClass('active');
            $('#listBank li[data-id="' + bankId + '"]').addClass('active');
        }
        $('#modalChooseBank').modal('show');
    });
    $('input.iCheckBankType').on('ifToggled', function(e){
        if(e.currentTarget.checked){
            $('#listBank li').hide();
            $('.liBank_' + e.currentTarget.value).show();
        }
    });
    $('#listBank').on('click', 'li', function(){
        $('#listBank li').removeClass('active');
        $(this).addClass('active');
        $('input#bankIdTmp').val($(this).attr('data-id'));
    });
    $('#btnChooseBank').click(function(){
        bankId = $('input#bankIdTmp').val();
        $('input#bankId').val(bankId);
        if(bankId != '0'){
            $('#bankInfo').html($('#listBank li[data-id="' + bankId + '"]').html());
            $('#selectBank').hide();
        }
        else $('#bankInfo').html('');
        $('#modalChooseBank').modal('hide');
    });
};

app.cancelTransaction = function(familyTransactionId){
    $('.btnCancel').click(function(){
        $('.btnCancel').prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#cancelTransactionUrl').val(),
            data: {
                FamilyTransactionId: familyTransactionId,
                TransactionTypeId: $('input#transactionTypeId').val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) redirect(true, '');
                else $('.btnCancel').prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                $('.btnCancel').prop('disabled', false);
            }
        });
    });
};

$(document).ready(function () {
    var familyTransactionId = parseInt($('input#familyTransactionId').val());
    var canEdit = parseInt($('input#canEdit').val());
    app.init(familyTransactionId, canEdit);

    var tags = [];
    var inputTag = $('input#tags');
    inputTag.tagsInput({
        'width': '100%',
        'height': '50px',
        'interactive': true,
        'defaultText': '',
        'onAddTag': function(tag){
            tags.push(tag);
        },
        'onRemoveTag': function(tag){
            var index = tags.indexOf(tag);
            if(index >= 0) tags.splice(index, 1);
        },
        'delimiter': [',', ';'],
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 0
    });
    $('input.tagName').each(function(){
        inputTag.addTag($(this).val());
    });
    $('#ulTagExist').on('click', 'a', function(){
        var tag = $(this).text();
        if(!inputTag.tagExist(tag)) inputTag.addTag(tag);
    });

    $('#familyTransactionForm').submit(function(e) {
        e.preventDefault();
    });
    if(canEdit){
        $('.submit').click(function() {
            var paidCost = replaceCost($('input#paidCost').val().trim(), true);
            if(paidCost <= 0){
                showNotification('Số tiền phải lớn hơn 0', 0);
                $('input#paidCost').focus();
                return false;
            }
            var moneySourceId = parseInt($('select#moneySourceId').val());
            var moneyPhoneId = parseInt($('select#moneyPhoneId').val());
            var bankId = $('input#bankId').val();
            var bankToText = '';
            if(moneySourceId == 2){
                if(bankId == '0'){
                    showNotification('Vui lòng chọn số tài khoản', 0);
                    return false;
                }
                bankToText = $("input#bankToText").val().trim();
                if(bankToText == ""){
                    showNotification('Vui lòng nhập STK nhận tiền', 0);
                    return false;
                }
            }
            else if(moneySourceId == 3){
                if(moneyPhoneId == '0'){
                    showNotification('Vui lòng chọn thẻ điện thoại', 0);
                    return false;
                }
            }
            $('.submit').prop('disabled', true);
            var transactionStatusId = 1;
            var verifyLevelId = 1;
            if($(this).attr('data-id') == '1'){ //xac thuc va hoan thanh
                if($('input#canEditLevel').val() == '2'){ //quan ly
                    verifyLevelId = 3;
                    transactionStatusId = 2;
                }
                else{ //nhan vien
                    verifyLevelId = 2;
                    transactionStatusId = 2;
                }
            }
            var transactionKindId = $('select#transactionKindId2').val();
            if(transactionKindId == '0') transactionKindId = $('select#transactionKindId1').val();
            $.ajax({
                type: 'POST',
                url: $('#familyTransactionForm').attr('action'),
                data : {
                    FamilyTransactionId: familyTransactionId,
                    TransactionTypeId: $('input#transactionTypeId').val(),
                    TransactionStatusId: transactionStatusId,
                    VerifyLevelId: verifyLevelId,
                    MoneySourceId: moneySourceId,
                    MoneyPhoneId: moneyPhoneId,
                    BankId: bankId,
                    BankToText: bankToText,
                    PaidCost: paidCost,
                    Comment: $('#comment').val().trim(),
                    TransactionKindId: transactionKindId,
                    PayerName: $('input#payerName').val(),
                    PayerPhone: $('input#payerPhone').val(),

                    TagNames: JSON.stringify(tags)
                },
                success: function(response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        if(familyTransactionId == 0) redirect(false, $('#aFamilyTransactionList').attr('href'));
                        else redirect(true, '');
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function(er) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            });
            return false;
        })

    }
});