$(document).ready(function () {
    // dateRangePicker();
    actionItemAndSearch({
        ItemName: 'Tư vấn lại',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
});

function renderContentCustomerConsults(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditCustomerConsult = $('#urlEditCustomerConsult').val() + '/';
        var urlInfoCustomerConsult = $("#urlInfoCustomerConsult").val() + '/';
        var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        var trClass = '';
        for (var item = 0; item < data.length; item++) {
            trClass = '';
            if(data[item].RemindDayDiff > 0 && data[item].RemindStatusId == 1) trClass = ' trItemCancel';
            html += '<tr id="trItem_'+data[item].CustomerConsultId+'" class="trItem' + trClass + '">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].CustomerConsultId + '"></td>';
            html += '<td><a href="' + urlEditCustomerConsult + data[item].CustomerConsultId  + '">' + data[item].CustomerConsultCode + '</a></td>';
            if(data[item].RemindDayDiff > 0 && data[item].RemindStatusId != '5' && data[item].RemindStatusId != '6'){
                if(data[item].ConsultDate != '') html += '<td>Quá hạn ' + data[item].RemindDayDiff + ' ngày</td>';
                else html += '<td></td>';
            }
            else html += '<td>'+ getDayText(data[item].RemindDayDiff) + data[item].ConsultDate +'</td>';
            html += '<td><a href="' + urlEditCustomerConsult + data[item].CustomerConsultId  + '">' + data[item].ConsultTitle + '</a></td>';
            html += '<td class="text-center">' + data[item].TimeProcessed + '</td>';
            
            html += '<td>'+ getDayText(data[item].CrDayDiff) +  data[item].CrDateTime +'</td>';
            // if(data[item].CustomerId != '0') html += '<td><a href="' + urlEditCustomer + data[item].CustomerId + '">'+ data[item].FullName +'</a></td>';
            // else html += '<td>'+ data[item].FullName +'</td>';
            html += '<td> <a href="' + urlInfoCustomerConsult + data[item].CustomerConsultId  + '">'+ data[item].FullName +'</a></td>';
            html += '<td>'+ data[item].PhoneNumber +'</td>';
            html += '<td>' + (data[item].CrFullName != null ? data[item].CrFullName : 'Khách hàng') + '</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].RemindStatusId] + '">' + data[item].RemindStatus + '</span></td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].OrderChannelId] + '">' + data[item].OrderChannel + '</span></td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="10" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}