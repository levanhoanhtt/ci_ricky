$(document).ready(function () {
    var searchUrl = $('input#searchFilterUrl').val();
    getListProductQuantity(0, 0, searchUrl);
    $('input.history').click(function () {
        getListProductQuantity(parseInt($(this).val()), parseInt($('select#storeId').val()), searchUrl);
    });
    $('select#storeId').change(function () {
        getListProductQuantity(parseInt($('input[name="InventoryStatusId"]:checked').val()), parseInt($(this).val()), searchUrl);
    });
    $('#btnCloseQuantityInfo').click(function () {
        $('div#quantityInfo').removeClass('active');
    });
    $(document).on('click', 'body', function () {
        $('div#quantityInfo').removeClass('active');
    }).on('click', 'div#quantityInfo', function (e) {
        e.stopPropagation();
    });

    $('#tbodyProduct').on('keydown', 'input.cost', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input.cost', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    }).on('click', 'a.link_update', function (e) {
        var price = replaceCost($(this).parent().parent().find('input.productPrice').val(), true);
        if(price > 0) {
            $.ajax({
                type: "POST",
                url: $('input#updateProductPriceUrl').val(),
                data: {
                    Price: price,
                    ProductId: $(this).attr('data-id'),
                    ProductChildId: $(this).attr('data-child')
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    //if(json.code == 1) parent.eq(7).find("input.productChangeId").val(json.data);
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        else showNotification('Giá vốn sản phẩm không được bé hơn 0', 0);
        return false;
    })
});
function getListProductQuantity(inventoryStatusId, storeId, searchUrl) {
    if(inventoryStatusId > 0 && storeId > 0) $('#btn-filter').attr('data-href', searchUrl + '?InventoryStatusId=' + inventoryStatusId + '&StoreId=' + storeId);
    else if(inventoryStatusId > 0) $('#btn-filter').attr('data-href', searchUrl + '?InventoryStatusId=' + inventoryStatusId);
    else if(storeId > 0) $('#btn-filter').attr('data-href', searchUrl + '?StoreId=' + storeId);
    else $('#btn-filter').attr('data-href', searchUrl);
    actionItemAndSearch({
        ItemName: 'Sản phẩm',
        IsRenderFirst: 1,
        extendFunction: function (itemIds, actionCode) {}
    });
}

function renderContentProducts(data) {
    var html = '';
    if (data != null) {
        var labelCss = [];
        if (data.length > 0) labelCss = data[0].labelCss;
        var urlEditProduct = $('#urlEditProduct').val() + '/';
        var urlEditProductCombo = $('#urlEditProductCombo').val() + '/';
        var canViewPrice = $('input#canViewPrice').val() == '1';
        var trClass = '';
        for (var item = 0; item < data.length; item++) {
            trClass = '';
            if(data[item].ProductStatusId != 2) trClass = ' trItemCancel';
            if(data[item].ProductChildId > 0) trClass += ' cProduct';
            html += '<tr class="trItem' + trClass + '">';
            html += '<td class="productName"><a href="' + ((data[item].ProductKindId == 3) ? urlEditProductCombo + data[item].ProductId : urlEditProduct + data[item].ProductId) + '">' + data[item].ProductName + '</a></td>';
            html += '<td class="text-center">';
            if (data[item].ProductKindId != 2) {
                html += '<span class="spanQuantity" data-id="' + data[item].ProductId + '" data-child="' + data[item].ProductChildId + '">';
                html += data[item].ProductQuantity != null ? formatDecimal(data[item].ProductQuantity.toString()) : '0';
                html += '</span>';
            }
            html += '</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].ProductKindId] + '">' + data[item].ProductKind + '</span></td>';
            if (canViewPrice){
                var priceCapital = 0;
                if(data[item].PriceCapital != null) priceCapital = formatDecimal(data[item].PriceCapital.toString());
                else priceCapital = 0;
                html += '<td class="text-right"><input type="text" class="form-control cost productPrice" value="'+priceCapital+'"></td>';
            }
            html += '<td>' + data[item].BarCode + '</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].ProductStatusId] + '">' + data[item].ProductStatus + '</span></td>';
            html += '<td class="text-center"><a href="javascript:void(0)" class="link_update" data-id="'+data[item].ProductId+'" data-child = "'+data[item].ProductChildId+'"><i class="fa fa-save"></i></a></td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="7" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);

        $('.spanQuantity').hover(function () {
            var span = $(this);
            $('div#quantityInfo').css('top', ($(this).offset().top - 220) + 'px').addClass('active');
            $('div#quantityInfo .imgLoading').show();
            $('div#quantityInfo table').hide();
            var productId = span.attr('data-id');
            var productChildId = span.attr('data-child');
            var inputs = $('input.productQuantity_' + productId + '_' + productChildId);
            if (inputs.length > 0) {
                var html = '';
                inputs.each(function () {
                    html += '<tr><td>' + $(this).attr('data-store') + '</td>';
                    html += '<td>' + $(this).val() + '</td></tr>';
                });
                $("div#quantityInfo table tbody").html(html);
                $('div#quantityInfo .imgLoading').hide();
                $('div#quantityInfo table').show();
                $('#pProductName').text(span.parent().parent().find('td.productName').text());
            }
            else {
                $.ajax({
                    type: "POST",
                    url: $('input#getQuantityUrl').val(),
                    data: {
                        ProductId: productId,
                        ProductChildId: productChildId
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) {
                            var html = '';
                            var htmlInput = '';
                            var data = json.data;
                            for (var i = 0; i < data.length; i++) {
                                html += '<tr><td>' + data[i].StoreName + '</td>';
                                html += '<td>' + data[i].Quantity + '</td></tr>';
                                htmlInput += '<input type="text" hidden="hidden" class="productQuantity_' + productId + '_' + productChildId + '" data-store="' + data[i].StoreName + '" value="' + data[i].Quantity + '">';
                            }
                            $("div#quantityInfo table tbody").html(html);
                            $('div#quantityInfo .imgLoading').hide();
                            $('div#quantityInfo table').show();
                            $('#divProductQuantity').append(htmlInput);
                            $('#pProductName').text(span.parent().parent().find('td.productName').text());
                        }
                        else showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }, function () {
        });
    }
}