$(document).ready(function(){
    $("#tbodyProductUsageStatus").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#productUsageStatusId').val(id);
        $('input#productUsageStatusName').val($('td#productUsageStatusName_' + id).text());
        scrollTo('input#productUsageStatusName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteProductUsageStatusUrl').val(),
                data: {
                    ProductUsageStatusId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#productUsageStatus_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#productUsageStatusForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#productUsageStatusForm')) {
            var form = $('#productUsageStatusForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="productUsageStatus_' + data.ProductUsageStatusId + '">';
                            html += '<td id="productUsageStatusName_' + data.ProductUsageStatusId + '">' + data.ProductUsageStatusName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.ProductUsageStatusId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.ProductUsageStatusId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyProductUsageStatus').prepend(html);
                        }
                        else{
                            $('td#productUsageStatusName_' + data.ProductUsageStatusId).text(data.ProductUsageStatusName);
                            $('td#itemTypeName_' + data.ProductUsageStatusId).html(data.ItemTypeName);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});