var countUpTime;
var elements = {
    sipLabel: document.getElementById('sip-account'),
    uaStatus: document.getElementById('ua-status'),
    phoneInput: document.getElementById('phone-input'),
    callStatus: document.getElementById('call-status'),
    callButton: document.getElementById('call-button'),
    player: document.getElementById('player'),
    logger: document.getElementById('logger'),

    configForm: document.getElementById('config-form'),
    clickCall: '0',
    callCustomer: 'callWeb',
    registerButton: document.getElementById('ua-register'),
    newSessionForm: document.getElementById('new-session-form'),
    inviteButton: document.getElementById('ua-invite-submit'),
    messageButton: document.getElementById('ua-message-submit'),

    uaURI: document.getElementById('ua-uri'),
    statusClose: 'closePhone',
    sessionList: document.getElementById('session-list'),
    sessionTemplate: document.getElementById('session-template'),
    messageTemplate: document.getElementById('message-template')
};

$('body').on('click', '.detail-call', function () {
    var phoneInput = $('#phone-input').val();
    console.log(phoneInput)
    console.log(status)
    callModal(phoneInput);
    $.ajax({
        type: "POST",
        url: $("#getUpdateIsCallAgainUrl").val(),
        data: {
            phoneNumber:phoneInput
        },
        success: function (response) {
        },
        error: function (response) {
        }
    });
    switch (status) {
        case "idle": {
            call();
            break;
        }
        case "outbound-ringing":
        case "outbound-awsering": {
            hangup();
            break;
        }
        case "inbound-ringing": {
            answer();
            break;
        }
        default:
            break;
    }
});

$('#call-customer-number').click(function() {
    var val = $('#call-number').val();
    if(val == '') {
        showNotification('Vui lòng nhập số điện thoại', 0);
        $('#call-number').focus();
        return;
    }

    if(!isNumeric(val)){
        showNotification('Số điện thoại chỉ có thể nhập vào số', 0);
        return;
    }

    if(val.length>11){
        showNotification('Số điện thoại không vượt quá 11 số', 0);
        return;
    }
    
    if(val.length<10){
        showNotification('Số điện thoại không nhỏ quá 10 số', 0);
        return;
    }
    
    elements.phoneInput.value = val;
    callModal(val);
    call();
    
});

function callModal(param){
    var callModal = $('#modalAnswer');
    callModal.modal('show');
    $('.PhoneNumber').text(param);
    $('.note-call').addClass('show');
    startTimer();
    callModal.find('.box-call-end').toggleClass('col-6 col-12');
    callModal.find('.box-call-answer').hide();
}

$('#call-answer').click(function(){
    $(this).closest('.form-row').find('.box-call-end').toggleClass('col-6 col-12');
    $(this).closest('.form-row').find('.box-call-answer').hide();
    answer();
    startTimer();
});

function startTimer(){
    var timer = $('#timer');
    if(timer.find('.status').length>0){
        clearInterval(countUpTime);
        timer.empty();
    }
    var startTimestamp = moment().startOf("day");
    countUpTime = setInterval(function() {
        startTimestamp.add(1, 'second');
        var time = startTimestamp.format('HH:mm:ss');
        timer.html('<p class="status">'+time+'</p>');
    }, 1000);
}

$('#call-end').click(function(){
    var boxCallEnd = $(this).closest('.form-row').find('.box-call-end');
    if(boxCallEnd.hasClass('col-12')){
        boxCallEnd.toggleClass('col-12 col-6');
        $(this).closest('.form-row').find('.box-call-answer').show();
    }
    $('.note-call').removeClass('show');
    stopTimer();
    $('#modalAnswer').modal('hide');
    hangup();
});

function stopTimer(){
    var timer = $('#timer');
    if(timer.find('.status').length!=1){
        return;
    }
    clearInterval(countUpTime);
    timer.empty();
}

var config0 = {
    userAgentString: 'SIP.js/0.7.0 BB',
    traceSip: false,
    register: true,
    displayName: $('#userName').val(),
    uri: $('#uri').val(),
    authorizationUser: $('#userName').val(),
    password: $('#passWord').val(),
    wsServers: "wss://pbx01.cmctelecom.vn:7443"
};

var config = config0;

var status = "idle";
var ua;
var session;
var sessionUIs = {};

/* var interval = setInterval(function(){
  let html = "";
  if(session){
    html = "<br/>accept:"+ (session.accept !== undefined).toString();
    html += "<br/>reject:"+ (session.reject !== undefined).toString();
    html += "<br/>cancel:"+ (session.cancel !== undefined).toString();
    html += "<br/>startTime:"+ session.startTime;
    html += "<br/>status:"+ status.toString();
  }
  elements.logger.innerHTML = html;
},1000); */
function hangup() {
    // updateCallLog();
    elements.clickCall = '0';
    elements.statusClose = 'closeWeb';
    if (session) {
        if (session.startTime) { // Connected
            session.bye();
        } else if (session.reject) { // Incoming
            session.reject();
        } else if (session.cancel) { // Outbound
            session.cancel();
        }
    }
}

function pageLoaded() {
    // updateCallLog();
    elements.sipLabel.innerHTML = config.authorizationUser;

    elements.uaStatus.innerHTML = 'Connecting...';

    ua = new SIP.UA(config);

    ua.on('connected', function () {
      elements.uaStatus.innerHTML = 'Connected (Unregistered)';
    });

    ua.on('registered', function () {
      elements.uaStatus.innerHTML = 'Connected (Registered)';
    });

    ua.on('unregistered', function () {
      elements.uaStatus.innerHTML = 'Connected (Unregistered)';
    });

    ua.on('invite', function (s) {
       
        session = s;
        var uri = s ? s.remoteIdentity.uri : SIP.Utils.normalizeTarget(uri, ua.configuration.hostport_params);
        var displayName = (s && s.remoteIdentity.displayName) || uri.user;
        $('.PhoneNumber').text(displayName);
        elements.clickCall = '1';
        elements.callCustomer = 'callMoblie';
        status = "inbound-ringing";
        updateUI();
    });
    // updateCallLog();
    ua.on('message', function (message) {
        //console.log("MESSAGE---", message);
    });
}

function updateUI() {
    switch (status) {
        case "idle": {
            elements.callButton.value = "Call";
            break;
        }
        case "outbound-ringing":
        case "outbound-awsering":
        case "inbound-awsering": {
            elements.callButton.value = "Hangup";
            break;
        }
        case "inbound-ringing": {
            elements.callButton.value = "Answer";
            $('#modalAnswer').modal('show');
            break;
        }
        default:
            break;
    }
    elements.callStatus.innerHTML = status;
}

function call() {
    var phoneNumber = elements.phoneInput.value;
    // updateCallLog();
    elements.clickCall = '1';
    elements.statusClose = 'closeWeb';
    elements.callCustomer = 'callWeb'; console.log(phoneNumber)
    session = ua.invite(phoneNumber, {
        media: {
            constraints: {
                audio: true,
                video: false
            }
        }
    });
    status = "outbound-ringing";
    session.on('bye', function () {
        status = "idle";
        updateUI();
    });
    session.on('failed', function () {
        status = "idle";
        updateUI();
    });
    session.mediaHandler.on('addStream', function () {
        status = "outbound-answering";
        updateUI();
        session.mediaHandler.render({
            remote: elements.player
        });
    });
    updateUI();
}

function answer() {
    elements.callCustomer = 'callWeb';
    elements.clickCall = '0';
    // updateCallLog();
    if(session.accept && !session.startTime){
        session.accept({
            media: {
                constraints: {
                    audio: true,
                    video: false
                }
            }
        });
        session.on('accepted',function(){
            status = "inbound-aswering";
            updateUI();
            session.mediaHandler.render({
                remote: elements.player
            });
            console.log("ANSWERED");
        });
        session.on('bye',function(){
            status = "idle";
            updateUI();
            console.log("HANGUP");
        })
        session.on('failed',function(){
            status = "idle";
            updateUI();
            console.log("FAILED");
        })
    }
}

setInterval(function(){
    if(elements.clickCall == '1') {
        document.getElementById("callPlay").play();
    }
}, 1000);

// setInterval(updateCallLog, 60000 * 30); //30p

function updateCallLog() {
    $.ajax({
        type: "POST",
        url: $("#getCallLogUpdateUrl").val(),
        data: {
        },
        success: function (response) {
        },
        error: function (response) {
        }
    });
}

$('.search-listchat input').keyup(function () {
    var value = $(this).val();
    if(value == '') {
        $(".listchat-each.custommer-info").show();
    } else {
        value = convertToEnglish(value.toLowerCase());
        $(".listchat-each.custommer-info").each(function( index ) {
            var text = $(this).find('.title').text();
            var desc = $(this).find('.desc').text();
            text = convertToEnglish(text.toLowerCase());
            text = '@'+text+'@'+desc+'@';
            var n = text.indexOf(value);
            $(this).hide();
            if(n > 0) {
                $(this).show();
            }
        });
    }
})

function convertToEnglish(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}
