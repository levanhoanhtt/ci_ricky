$('#targetType').change(function () {
    var targetType = $(this).val();
    listTagetType(targetType, 0);
})

var targetIdValue = $('#targetIdValue').val();
var targetType = $('#targetType').val();
listTagetType(targetType, targetIdValue);
function listTagetType(targetType, targetId) {
    $.ajax({
        type: "POST",
        url: $('#apiTargetIdList').val(),
        data: {
            'targetType' : targetType,
            'targetId' : targetId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            $('#targetId').html(json.html);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}