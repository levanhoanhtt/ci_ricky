$(document).ready(function() {
	dateRangePicker();
	var reportTypeId = parseInt($('input#reportTypeId').val());
	var beginDate = '';
	var flag = false;
	var dates = $("#dateRangePicker").val().trim().split('-');
	if(dates.length == 2){
		beginDate =  dates[0].trim();
		if(beginDate != '') getReport(beginDate, dates[1].trim(), 0, 0, 0, 0, reportTypeId);
		else flag = true;
	}
	else if(dates.length == 1){
		beginDate =  dates[0].trim();
		if(beginDate != '') getReport(beginDate, '', 0, 0, 0, 0, reportTypeId);
		else flag = true;
	}
	else flag = true;
	if(flag) showNotification("Vui lòng nhập ngày bắt đầu", 0);

	$('#btnGetReport').click(function(){
		flag = false;
		dates = $("#dateRangePicker").val().trim().split('-');
		if(dates.length == 2){
			beginDate =  dates[0].trim();
			if(beginDate != '') getReport(beginDate, dates[1].trim(), $("select#storeId").val(), $("select#productTypeId").val(), $("select#deliveryTypeId").val(), $("select#crUserId").val(), reportTypeId);
			else flag = true;
		}
		else if(dates.length == 1){
			beginDate =  dates[0].trim();
			if(beginDate != '') getReport(beginDate, '', $("select#storeId").val(), $("select#productTypeId").val(), $("select#deliveryTypeId").val(), $("select#crUserId").val(), reportTypeId);
			else flag = true;
		}
		else flag = true;
		if(flag) showNotification("Vui lòng nhập ngày bắt đầu", 0);
	});

    $("#tbodyByDate").on('click', 'a.list_order', function () {
        var date = $(this).parent().parent().find('td:eq(0)').text();
        var orderStatusIds = $(this).attr('order-status-id');
        $.ajax({
            type: "POST",
            url: $('input#searchByOrderStatusUrl').val(),
            data: {
                BeginDate: date,
                EndDate: date,
                OrderStatusIds: orderStatusIds
            },
            success: function (response) {
                var json = $.parseJSON(response);
                var data = json.data;
                var html = '';
                if(data!=null) {
                    var urlEditOrder = $('#urlEditOrder').val() + '/';
                    var urlEditCustomer = $('#urlEditCustomer').val() + '/';
                    var sumTotalCost = 0, discount = 0, totalSuccessfulSales = 0 , totalGrossProfit = 0, totalPriceChange = 0;

                    for (var item = 0; item < data.length; item++) {
                        sumTotalCost += parseInt(data[item].TotalCost);
                        discount += data[item].Discount;
                        totalPriceChange += parseInt(data[item].PriceChange);
                        var successfulSales = parseInt(data[item].TotalCost) -  parseInt(data[item].Discount);
                        totalSuccessfulSales += parseInt(successfulSales);

						var grossProfit = parseInt(successfulSales) -  parseInt(data[item].PriceChange);
						totalGrossProfit += parseInt(grossProfit);

                        html += '<tr id="trItem_'+data[item].OrderId+'" class="orderStatus_'+data[item].OrderStatusId+'">';
                        if(data[item].VerifyStatusId == 2) html += '<td id="orderCode_'+data[item].OrderId+'"><a href="' + urlEditOrder + data[item].OrderId + '" target="_blank">' + data[item].OrderCode + '</a><i class="fa fa-check tooltip1 active" title="Đã xác thực"></i></td>';
                        else html += '<td id="orderCode_'+data[item].OrderId+'"><a href="' + urlEditOrder + data[item].OrderId + '" target="_blank">' + data[item].OrderCode + '</a></td>';
                        html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
                        html += '<td><a href="' + urlEditCustomer + data[item].CustomerId + '" target="_blank">' + data[item].FullName + '</a></td>';
                        html += '<td class="text-right">' + formatDecimal(data[item].TotalCost.toString()) + '</td>';
                        html += '<td class="text-right">' + formatDecimal(data[item].Discount.toString()) + '</td>'; 
                        html += '<td class="text-right">' + formatDecimal(successfulSales.toString()) + '</td>';
                        html += '<td class="text-right">' + formatDecimal(data[item].PriceChange.toString()) + '</td>'; 
                        html += '<td class="text-right">' + formatDecimal(grossProfit.toString()) + '</td>';
                        html += '</tr>';
                    }
                    if(html != ''){
                    	html += '<tr><td colspan="3"></td><td class="text-right">' + formatDecimal(sumTotalCost.toString()) + '</td>';
                    	html += '<td class="text-right">' + formatDecimal(discount.toString()) + '</td>';
                    	html += '<td class="text-right">' + formatDecimal(totalSuccessfulSales.toString()) + '</td>';
                    	html += '<td class="text-right">' + formatDecimal(totalPriceChange.toString()) + '</td>';
                    	html += '<td class="text-right">' + formatDecimal(totalGrossProfit.toString()) + '</td></tr>';
                    } 
                    html += '<tr><td colspan="9" class="paginate_table"></td></tr>';
                    $('#tbodyOrder').html(html);
                }
                $("#modalListOrder").modal('show');
            }
        });
    });
});

function getReport(beginDate, endDate, storeId, productTypeId, deliveryTypeId, crUserId, reportTypeId){
	$('.text-null').text('');
	$('#tbodyByDate').html('');
	$.ajax({
        type: "POST",
        url: $('input#getReportUrl').val(),
        data: {
            BeginDate: beginDate,
            EndDate: endDate,
            StoreId: storeId,
            ProductTypeId: productTypeId,
            DeliveryTypeId: deliveryTypeId,
			CrUserId: crUserId
        },
        success: function (response) {
        	var json = $.parseJSON(response);
        	var data = json.data;
        	
			var totalRevence = 0, totaldhSuccess = 0, totaldhFailure = 0, totaldhCOD = 0,  totaldhRevence = 0, totalShip = 0, totalPos = 0, total = 0;
			var html = '';
			if(reportTypeId == 1){ //doanh thu
				// totalRevence 	= parseInt(data['Success'].TotalCost - data['Success'].DiscountCost) + parseInt(data['Failure'].TotalCost - data['Failure'].DiscountCost) + parseInt(data['COD'].TotalCost - data['COD'].DiscountCost);
				// totaldhSuccess 	= parseInt(data['Success'].ShipCount) + parseInt(data['Success'].POSCount);
				// totaldhFailure 	= parseInt(data['Failure'].ShipCount) + parseInt(data['Failure'].POSCount);
				// totaldhCOD 		= parseInt(data['COD'].ShipCount) + parseInt(data['COD'].POSCount);
				// totaldhRevence 	= totaldhSuccess + totaldhFailure + totaldhCOD;
				// totalShip 		= parseInt(data['Success'].ShipCount) + parseInt(data['Failure'].ShipCount) + parseInt(data['COD'].ShipCount);
				// totalPos 		= parseInt(data['Success'].POSCount) + parseInt(data['Failure'].POSCount) + parseInt(data['COD'].POSCount);

				// $(".total-revence").text(formatDecimal(totalRevence.toString())+' đ');
				// $(".total-dh-revence").text('ĐH: '+formatDecimal(totaldhRevence.toString()));
				// $(".ship-post-revence").text(totalShip+ ' ship + '+ totalPos + ' pos');

				// $(".total-success").text(formatDecimal((parseInt(data['Success'].TotalCost - data['Success'].DiscountCost)).toString())+ ' đ');
				// $(".total-dh-success").text('ĐH: '+formatDecimal(totaldhSuccess.toString()));
				// $(".ship-post-success").text(data['Success'].ShipCount+ ' ship + '+ data['Success'].POSCount + ' pos');

				// $(".total-cod").text(formatDecimal((parseInt(data['COD'].TotalCost - data['COD'].DiscountCost)).toString())+ ' đ');
				// $(".total-dh-cod").text('ĐH: '+formatDecimal(totaldhCOD.toString()));
				// $(".ship-post-cod").text(data['COD'].ShipCount+ ' ship + '+ data['COD'].POSCount + ' pos');

				// $(".total-failure").text(formatDecimal((parseInt(data['Failure'].TotalCost - data['Failure'].DiscountCost)).toString())+ ' đ');
				// $(".total-dh-failure").text('ĐH: '+formatDecimal(totaldhFailure.toString()));
				// $(".ship-post-failure").text(data['Failure'].ShipCount+ ' ship + '+ data['Failure'].POSCount + ' pos');

				// html = '';

				// $.each(data.ByDate,function(key, value){
				// 	total = parseInt(value['Success'].TotalCost - value['Success'].DiscountCost) + parseInt(value['Failure'].TotalCost - value['Failure'].DiscountCost) + parseInt(value['COD'].TotalCost - value['COD'].DiscountCost);
				// 	html += '<tr><td>' + key + '</td><td><a href="javascript:void(0);" order-status-id = "(6)" class="list_order"> ' + formatDecimal((parseInt(value['Success'].TotalCost - value['Success'].DiscountCost)).toString()) + '</a></td>';
				// 	html += '<td><a href="javascript:void(0);" order-status-id = "(2)" class="list_order"> ' + formatDecimal((parseInt(value['COD'].TotalCost - value['COD'].DiscountCost)).toString()) + '</a></td>';
				// 	html += '<td><a href="javascript:void(0);" order-status-id = "(5)" class="list_order"> ' + formatDecimal((parseInt(value['Failure'].TotalCost - value['Failure'].DiscountCost)).toString()) + '</a></td>';
				// 	html += '<td><a href="javascript:void(0);" order-status-id = "(2,5,6)" class="list_order"> ' + formatDecimal(total.toString()) + '</a></td></tr>';
				// });
				// $("#tbodyByDate").html(html);
			}
			else if(reportTypeId == 2){ //doanh so
				totalRevence 	= parseInt(data['Success'].TotalCost) + parseInt(data['Failure'].TotalCost) + parseInt(data['COD'].TotalCost);
				totaldhSuccess 	= parseInt(data['Success'].ShipCount) + parseInt(data['Success'].POSCount);
				totaldhFailure 	= parseInt(data['Failure'].ShipCount) + parseInt(data['Failure'].POSCount);
				totaldhCOD 		= parseInt(data['COD'].ShipCount) + parseInt(data['COD'].POSCount);
				totaldhRevence 	= totaldhSuccess + totaldhFailure + totaldhCOD;
				totalShip 		= parseInt(data['Success'].ShipCount) + parseInt(data['Failure'].ShipCount) + parseInt(data['COD'].ShipCount);
				totalPos 		= parseInt(data['Success'].POSCount) + parseInt(data['Failure'].POSCount) + parseInt(data['COD'].POSCount);

				$(".total-revence").text(formatDecimal(totalRevence.toString())+' đ');
				$(".total-dh-revence").text('ĐH: '+formatDecimal(totaldhRevence.toString()));
				$(".ship-post-revence").text(totalShip+ ' ship + '+ totalPos + ' pos');

				$(".total-success").text(formatDecimal(data['Success'].TotalCost.toString())+ ' đ');
				$(".total-dh-success").text('ĐH: '+formatDecimal(totaldhSuccess.toString()));
				$(".ship-post-success").text(data['Success'].ShipCount+ ' ship + '+ data['Success'].POSCount + ' pos');

				$(".total-cod").text(formatDecimal(data['COD'].TotalCost.toString())+ ' đ');
				$(".total-dh-cod").text('ĐH: '+formatDecimal(totaldhCOD.toString()));
				$(".ship-post-cod").text(data['COD'].ShipCount+ ' ship + '+ data['COD'].POSCount + ' pos');

				$(".total-failure").text(formatDecimal(data['Failure'].TotalCost.toString())+ ' đ');
				$(".total-dh-failure").text('ĐH: '+formatDecimal(totaldhFailure.toString()));
				$(".ship-post-failure").text(data['Failure'].ShipCount+ ' ship + '+ data['Failure'].POSCount + ' pos');

				html = '';
				var totalSuccess = 0, totalDiscount = 0, totalSuccessfulSales = 0, totalPriceChange = 0, totalGrossProfit = 0;
				$.each(data.ByDate,function(key, value){
					total = parseInt(value['Success'].TotalCost);
					var successfulSales = parseInt(value['Success'].TotalCost) -  parseInt(value['Success'].DiscountCost);
					var grossProfit = parseInt(successfulSales) -  parseInt(value['Success'].PriceChange);

					totalSuccess += total;
					totalDiscount += value['Success'].DiscountCost;
					totalSuccessfulSales += successfulSales;
					totalPriceChange += value['Success'].PriceChange;
					totalGrossProfit += grossProfit;

					html += '<tr><td>' + key + '</td><td><a href="javascript:void(0)" order-status-id = "(6)" class="list_order">' + formatDecimal(value['Success'].TotalCost.toString()) + '</a></td>';
					html += '<td><a href="javascript:void(0)" order-status-id = "(6)" class="list_order">' + formatDecimal(value['Success'].DiscountCost.toString()) + '</a></td>';
					html += '<td><a href="javascript:void(0)" order-status-id = "(6)" class="list_order">' + formatDecimal(successfulSales.toString()) + '</a></td>';
					html += '<td><a href="javascript:void(0)" order-status-id = "(6)" class="list_order">' + formatDecimal(value['Success'].PriceChange.toString()) + '</a></td>';
					html += '<td><a href="javascript:void(0)" order-status-id = "(6)" class="list_order">' + formatDecimal(grossProfit.toString()) + '</a></td></tr>';
				});
				$("#totalSuccess").html(formatDecimal(totalSuccess.toString())+ ' VNĐ');
				$("#totalDiscount").html(formatDecimal(totalDiscount.toString())+ ' VNĐ');
				$("#totalSuccessfulSales").html(formatDecimal(totalSuccessfulSales.toString())+ ' VNĐ');
				$("#totalPriceChange").html(formatDecimal(totalPriceChange.toString())+ ' VNĐ');
				$("#totalGrossProfit").html(formatDecimal(totalGrossProfit.toString())+ ' VNĐ');
				
				$('#tbodyByDate').html(html);
			}
        },
		error: function (response) {
			showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
		}
    });
}