$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Phiếu',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
});

function renderContentTransactions(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        var urlEditTransaction = $('#urlEditTransaction').val() + '/';
        var sumPaidCost = 0;
        var trClass = '';
        for (var item = 0; item < data.length; item++) {
            sumPaidCost += parseInt(data[item].PaidCost);
            trClass = '';
            if(data[item].TransactionStatusId == '3') trClass = ' trItemCancel';
            html += '<tr id="trItem_'+data[item].TransactionId+'" class="trItem' + trClass + '">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].TransactionId + '"></td>';
            if(data[item].TransactionStatusId == 2) html += '<td><a href="' + urlEditTransaction + data[item].TransactionId + '">' + data[item].TransactionCode + ' <i class="fa fa-check"></i></a></td>';
            else html += '<td><a href="' + urlEditTransaction + data[item].TransactionId + '">' + data[item].TransactionCode + '</a></td>';
            html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
            html += '<td><a href="' + urlEditCustomer + data[item].CustomerId + '">' + data[item].FullName + '</a></td>';
            html += '<td class="text-right">' + formatDecimal(data[item].PaidCost) + '</td>';
            html += '<td>' + data[item].MoneySourceName + '</td>';
            html += '<td>' + (data[item].TransactionReasonName != null ? data[item].TransactionReasonName : '') + '</td>';
            html += '<td>' + (data[item].StoreName != null ? data[item].StoreName : '--') + '</td>';
            html += '<td class="text-center"><span class="' + labelCss.TransactionStatusCss[data[item].TransactionStatusId] + '">' + data[item].TransactionStatusName + '</span></td>';
            html += '<td class="text-center"><span class="' + labelCss.VerifyLevelCss[data[item].VerifyLevelId] + '">' + data[item].VerifyLevelName + '</span></td>';
            html += '</tr>';
        }
        if(html != '') html += '<tr><td colspan="4"></td><td class="text-right">' + formatDecimal(sumPaidCost.toString()) + '</td><td colspan="5"></td></tr>';
        html += '<tr><td colspan="10" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}