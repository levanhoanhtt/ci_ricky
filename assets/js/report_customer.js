$(document).ready(function(){
   dateRangePicker();
   var keepingDateOld = '';
    var keepingDate = '';
    var rowspan = 1;
    $('#tbodyCustomers tr').each(function(){
        keepingDate = $(this).attr('data-id');
        if(keepingDate != keepingDateOld){
            keepingDateOld = keepingDate;
            rowspan = $('#tbodyCustomers tr[data-id="' + keepingDate + '"]').length;
            if(rowspan > 1){
                var trFirst = $('#tbodyCustomers tr[data-id="' + keepingDate + '"]').first();
                trFirst.find('.tdMerge').attr('rowspan', rowspan).css('vertical-align', 'middle');
                $('#tbodyCustomers tr[data-id="' + keepingDate + '"] .tdMerge[rowspan="1"]').remove();
                // trFirst.find('.tdTotalTime').text($('input#duration_' + keepingDate.replace(/\//g, '_')).val());
            }
        }
    });
    var cellThis, cellPrev, spanning;
    $('#tbodyCustomers .col-x').each(function () {
        cellThis = $(this);
        spanning = 0;
        if(cellPrev) {
            if($(cellPrev).find('span.spanMergeName').html() == $(cellThis).find('span.spanMergeName').html()){
                $(cellThis).remove();
                $(cellPrev).prop("rowspan", parseInt($(cellPrev).prop("rowspan")) + 1);
                spanning = 1;
            }
        }
        if(spanning == 0) cellPrev = $(this);
    });
});



