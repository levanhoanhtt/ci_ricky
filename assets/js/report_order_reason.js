$(document).ready(function() {
    dateRangePicker(function(dates){
        dates = dates.trim().split('-');
        if(dates.length == 2) getReport(dates[0].trim(), dates[1].trim());
        else if(dates.length == 1) getReport(dates[0].trim(), '');
        else getReport('', '');
    });
    var dates = $("#dateRangePicker").val().trim().split('-');
    if(dates.length == 2) getReport(dates[0].trim(), dates[1].trim());
    else if(dates.length == 1) getReport(dates[0].trim(), '');
    else getReport('', '');
});

function getReport(beginDate, endDate){
    $('#tbodyOrderReason .trItem').remove();
    $('#tbodyOrderReason .tdCount').text('-');
    $.ajax({
        type: "POST",
        url: $('input#getReportUrl').val(),
        data: {
            BeginDate: beginDate,
            EndDate: endDate
        },
        success: function (response) {
            var json = $.parseJSON(response);
            var data = json.data;
            var orderReasonIds = [];
            var sumOrderReasonIds = [];
            var n = 0;
            $('input.orderReasonId').each(function(){
                orderReasonIds.push($(this).val());
                sumOrderReasonIds[$(this).val()] = 0;
                n++;
            });
            var j, k, flag, countOrder, totalOrder, sumOrder = 0;
            //console.log(data);
            var html = '';
            for(var i = 0; i < data.length; i++){
                totalOrder = parseInt(data[i].TotalOrder);
                sumOrder += totalOrder;
                html += '<tr class="trItem">';
                html += '<td>' + data[i].CrDateTime + '</td>';
                html += '<td class="text-center">' + data[i].TotalOrder + '</td>';
                for(j = 0; j < n; j++){
                    flag = false;
                    for(k = 0; k < data[i].Data.length; k++){
                        if(orderReasonIds[j] == data[i].Data[k].OrderReasonId){
                            countOrder = parseInt(data[i].Data[k].CountOrder);
                            sumOrderReasonIds[orderReasonIds[j]] += countOrder;
                            html += '<td class="text-center">' + data[i].Data[k].CountOrder;
                            if(totalOrder > 0){
                                countOrder = Math.ceil(countOrder / totalOrder * 100);
                                html += ' (' + countOrder + ' %)';
                            }
                            html += '</td>';
                            flag = true;
                            break;
                        }
                    }
                    if(!flag) html += '<td class="text-center">-</td>';
                }
                html += '</tr>';
            }
            $('#tbodyOrderReason').append(html);
            $('#tdTotalOrder').text(formatDecimal(sumOrder.toString()));
            for(j in sumOrderReasonIds){
                html = formatDecimal(sumOrderReasonIds[j].toString());
                if(sumOrder > 0){
                    countOrder = Math.ceil(sumOrderReasonIds[j] / sumOrder * 100);
                    html += ' (' + countOrder + ' %)';
                }
                $('#tdCountOrder_' + j).text(html);
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}