var pageAccessToken = '';
var userToken = "EAActZC3KEBCwBAJ1MMVJRXhce86XybZCx3bl1dfdmgYEH0sTrnmtJXN78nZBiXgUtxLhr9JSOVGZBjoWYCbpGliHI8eGJT4mCCLUWIjShoYOEPmGt5R7DIdbVfFMHy6KIFljCOvNlggqGIyYIN3HgNbC01K0annZCAenIKzkdcwZDZD";
var pageId = '1759192184381061';
var userLogin = "";
var ws;


var access_token = '2020899998139436|dvRUN1GCgvUv2P3aP3EXQqSrLlk';

var userId = '';

//init data
$(document).ready(function () {
    $.ajaxSetup({cache: true});
    initWebsocket();

    $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
        FB.init({
            appId: '2020899998139436',
            version: 'v2.10'
        });
        $('#list-conversation').html('');
        var listConversationHtml = "";
        getAccessToken();
        FB.api(
            '/' + pageId + '/feed',
            'GET',
            {
                access_token: access_token,
                fields: 'message,id,object_id,created_time,call_to_action,status_type,type,from{id,picture,name},comments{from,created_time,message}'
            },
            function (response) {
                console.log(response);
                var listFeed = response;
                for (var i = 0; i < listFeed.data.length; i++) {
                    listConversationHtml += '<li class="border-bottom is-unreaded"  onclick = "displayBox(\'' + listFeed.data[i].id + '\')">' +
                        '<div class="conversation-icon">' +
                        '<img src="' + listFeed.data[i].from.picture.data.url + '">' +
                        '</div>' +
                        '<div class="conversation-content">' +
                        '<p class="text-no-bold conversation-snippet">' + listFeed.data[i].from.name + '</p>' +
                        '<div class="conversation-snippet">' +
                        '<span>' + listFeed.data[i].message + '</span>' +
                        '</div>' +
                        '</div>' +
                        '<div class="conversation-info">' +
                        '<div>' +
                        '<i class="fa fa-clock-o"></i>' +
                        '<span>' + listFeed.data[i].created_time + '</span>' +
                        '</div>' +
                        '<div class="user-in-obj">' +
                        '<ul class="letter-avatar"></ul>' +
                        '</div>' +
                        '<div>' +
                        '<i class="type-icon fa fa-envelope-o fa-1-5"></i>' +
                        '</div>' +
                        '<div>' +
                        '<a class="read-status inline_block_i is-unreaded"></a>' +
                        '</div>' +
                        '</div>' +
                        '</li>';
                }
                $('#list-conversation').html(listConversationHtml);
            }
        );
    });

    $('.fb-chat-list .conversation .tab-content:not(:first-child)').hide();
    var window_height = $(window).height();
    $("#fb-chat").css('min-height', window_height - $('.main-footer').outerHeight());
    $('.fb-chat-menu .menu-icon').click(function () {
        $('.fb-chat-menu .menu-icon').removeClass('active');
        $(this).addClass('active');
        var _id = $(this).attr('data-original');
        $(this).parents('.fb-chat-list').find('.tab-content').hide();
        $(_id).show();
    });

});


function displayBox(id) {
    FB.api(
        '/' + id,
        'GET',
        {
            access_token: access_token,
            fields: 'message,id,object_id,created_time,call_to_action,status_type,type,from{id,picture,name},comments'
        },
        function (response) {
            console.log(response);
            $('#currName').html(response.from.name);
            $('#selected-user-avatar').attr('src', response.from.picture.data.url)
            $('#message-type').val(response.status_type)
            $('#customer-info-detail').html('');
            var detailCustommer = '';
            var location = response.from.location == undefined ? 'Chưa cập nhật thông tin' : response.from.location;
            var phoneNo = response.from.phone == undefined ? 'Chưa cập nhật thông tin' : response.from.phone;
            detailCustommer = '<ul class="participant-info-detail-customer"><li class="overflow-ellipsis"><i class="fa fa-user mr5"></i>' +
                '<a href="https://facebook.com/' + response.from.id + '" target="_blank">' + response.from.name + '</a></li><li class="overflow-ellipsis"><i class="fa fa-envelope mr5"></i><span>' + response.from.id + '@facebook.com</span></li>' +
                '<li class="overflow-ellipsis"><i class="fa fa-phone-square mr5"></i>' +
                '<span>' + phoneNo + '</span></li>' +
                '<li class="overflow' +
                '-ellipsis"><i class="fa fa-home mr5"></i>' +
                '<span>' + location + '</span></li></ul>';

            $('#customer-info-detail').html(detailCustommer);

            $('#customer-info-detail').show();

            if (response.type == 'status') {
                $('#private-id-message').val(id);
                var listComment = '';

                if (response.comments) {
                    for (comment of response.comments.data) {
                        listComment += '<li>' +
                            '<div>' +
                            '<div>' + comment.from.name + '</div>' +
                            '<p class="message">' +
                            '<span>' + comment.message + '</span>' +
                            '<div><span><button title="Thích" style = "border:none" class = "fa fa-thumbs-o-up" onclick="reactionComment(\'' + comment.id + '\')"  ></button><button title="Ẩn comment" style = "border:none" class = "fa fa-eye-slash" onclick="hideComment(\'' + comment.id + '\')"  ></button><button  title="Gửi tin nhắn nhanh" data-toggle="modal" data-target="#dialog-reply" style = "border:none" class = "fa fa-commenting-o" onclick="sendPrivateReply(\'' + comment.id + '\')"  ></button><button  title="Trả lời" data-toggle="modal" data-target="#dialog-comment" style = "border:none" class = "fa fa-commenting-o" onclick="createTextboxQuickReply(\'' + comment.id + '\')"  ></button><a title="Xem trên facebook" class = "fa fa-facebook-square"  href="https://facebook.com/' + comment.id + '" target ="_blank"></a></span></div>' +
                            '</p>' +
                            '</div></li>';
                    }
                }
                var htmlMessage =
                    '<div style= "background: cornsilk, padding-bottom: 30px;"> ' +
                    '<p>' +
                    '<span>' + response.message + '</span>' +
                    '</p>' +
                    '</div>';
                $('#conversation-message-picture').html(htmlMessage + listComment);
                $('.list-message').show();
            }
        }
    );
}

//action send post data to api
$('#send-button').click(function () {
    var id = $('#private-id-message').val();
    var datapost = $('#textarea-auto-height-harapage').val();
    if (datapost == null || datapost == undefined || datapost.trim() == '') {
        return;
    } else {
        //check type reply
        if ($('#message-type').val() == 'message') {
            // reply a comment, post comment for activity
            sendMessage(datapost);
        } else {
            // reply a message, post private message to user
            replyACommnent(id, datapost);
        }

        //clear input text
        $('#textarea-auto-height-harapage').val('');
    }

});


function sendMessage(message) {
    var id = $('#private-id-message').val();
    console.log(id);
    FB.api(
        "/" + id + "/messages",
        "POST",
        {
            message: message,
            access_token: pageAccessToken
        },
        function (response) {
            console.log(response);
            bindConversationData(id);
            if (response && !response.error) {
                console.log(response);
                bindConversationData(id);
            } else {

            }
        }
    );

}

function replyACommnent(idComment, messageReply) {

    FB.api(
        "/" + idComment + "/comments",
        "POST",
        {
            message: messageReply,
            access_token: pageAccessToken
        },
        function (response) {
            console.log(response);
            if (response && !response.error) {
                alertify.success('Đã trả lời bình luận này');
            } else {
                //gui tin nhan ko thnah cong
                alertify.error('Trả lời gửi không thành công.');

            }
        }
    );
}

//Get access_token
function getAccessToken() {
    if (FB.getAccessToken() == null || FB.getAccessToken() == undefined || FB.getAccessToken().trim() == '') {

        FB.api(
            '/' + pageId,
            'GET',
            {
                'fields': 'access_token',
                'access_token': userToken
            },
            function (response) {
                if (!response.error) {
                    pageAccessToken = response.access_token;
                }
            }
        );
        /*
         FB.login(function (response) {
         //console.log(response);
         userToken = FB.getAccessToken();
         userId = FB.getUserID();
         FB.api('/me?access_token=' + userToken + '&fields=name,picture,id', function (responseResult) {
         if (!responseResult.error) {
         userLogin = responseResult;
         }
         });

         FB.api(
         '/' + pageId,
         'GET',
         {
         'fields': 'access_token',
         'access_token': userToken
         },
         function (response) {
         // console.log(response);
         if (!response.error) {
         pageAccessToken = response.access_token;
         }
         }
         );
         }); */
    }

}


$('#data-messenger').click(function () {
    getListMessage();
});


function getListMessage() {
    var dataJson = {};
    FB.api(
        pageId + '?fields=conversations{id,snippet,updated_time,senders}',
        'GET',
        {
            'access_token': pageAccessToken
            //EAActZC3KEBCwBACUgnk0DQyrwGVhR024apZCVtqlZBDz3cKAZAj5vZB9sejv0ZBdpEcezxCmStqdHZAJ1QQyY7NwGmGfQgknaP0zODZCGd7y8IsQ5EPymxf7UU4upO3lQIIkHhcJ587Jk37dcLgqPKssYNLxZAdXWVeZC6WBUXsq0YxAZDZD
            //EAActZC3KEBCwBAJGD03G9M3zNwno8oCxwW98ifSx9EI0jqepBkVGrRhcyVNtOQCrRU7uZC9D5dZCcZCSG6Fy5YTigAxqTCQWTWQBLJftZC9bQn3mFqZA7TFctg01VX1mk2GZBAn5kFgDR7hso2ZABRCPcEq5L4sx6gUGfR2xam5aVAZDZD
        },
        function (response) {
            if (!response.error) {
                dataJson = response;
                bindMessageData(dataJson);
            } else {
                dataJson = {error: 'Không lấy được danh sách tin nhắn'};
            }
        }
    );
}

function bindMessageData(data) {
    console.log(data);
    var listConversationHtml = '';
    $('#inbox-message').html(listConversationHtml);

    var listFeed = data;
    for (var i = 0; i < listFeed.conversations.data.length; i++) {
        listConversationHtml += '<li id = "' + listFeed.conversations.data[i].id + '"  class="border-bottom is-unreaded"  onclick = "displayConversation(\'' + listFeed.conversations.data[i].id + '\',\'' + listFeed.conversations.data[i].senders.data[0].name + '\',\'' + listFeed.conversations.data[i].senders.data[0].id + '\')">' +
            '<div class="conversation-icon">' +
            '<img src="https://graph.facebook.com/' + listFeed.conversations.data[i].senders.data[0].id + '/picture?type=square">' +
            '</div>' +
            '<div class="conversation-content">' +
            '<p class="text-no-bold conversation-snippet">' + listFeed.conversations.data[i].senders.data[0].name + '</p>' +
            '<div class="conversation-snippet">' +
            '<span>' + listFeed.conversations.data[i].snippet + '</span>' +
            '</div>' +
            '</div>' +
            '<div class="conversation-info">' +
            '<div>' +
            '<i class="fa fa-clock-o"></i>' +
            '<span>' + listFeed.conversations.data[i].updated_time + '</span>' +
            '</div>' +
            '<div class="user-in-obj">' +
            '<ul class="letter-avatar"></ul>' +
            '</div>' +
            '<div>' +
            '<i class="type-icon fa fa-envelope-o fa-1-5"></i>' +
            '</div>' +
            '<div>' +
            '<a class="read-status inline_block_i is-unreaded"></a>' +
            '</div>' +
            '</div>' +
            '</li>';
    }
    $('#inbox-message').html(listConversationHtml);
}


function displayConversation(idConversation, name, id) {
    //fill data of client
    $('#customer-info-detail').html('');
    var detailCustommer = '';

    $('#selected-user-avatar').attr('src', "https://graph.facebook.com/" + id + "/picture?type=square");
    detailCustommer = '<ul class="participant-info-detail-customer"><li class="overflow-ellipsis"><i class="fa fa-user mr5"></i>' +
        '<a href="https://facebook.com/' + id + '" target="_blank">' + name + '</a></li><li class="overflow-ellipsis"><i class="fa fa-envelope mr5"></i><span>' + id + '@facebook.com</span></li>' +
        '<li class="overflow-ellipsis"><i class="fa fa-phone-square mr5"></i>' +
        '<span> </span></li>' +
        '<li class="overflow-ellipsis"><i class="fa fa-home mr5"></i>' +
        '<span> </span></li></ul>';

    $('#customer-info-detail').html(detailCustommer);
    $('#customer-info-detail').show();
    bindConversationData(idConversation);
}


//bind list message
function bindConversationData(idConversation) {
    var dataJson = {};
    FB.api(
        '/' + idConversation + '/messages?fields=senders,created_time,from,to,message',
        'GET',
        {
            'access_token': pageAccessToken
        },
        function (response) {
            console.log(response);
            if (!response.error) {
                dataJson = response;
                var response = dataJson;
                var htmlMessage = '';
                for (var i = response.data.length - 1; i >= 0; i--) {
                    if (response.data[i].from.id == userId) {
                        htmlMessage += '<li class="is-from-page">' +
                            '<div>' +
                            '<p class="message">' +
                            '<span>' + response.data[i].message + '</span>' +
                            '</p>' +
                            '</div>' +
                            '<span class="time note" data-bind="timeshort: LastUpdatedDate">' + moment(new Date(response.data[i].created_time)).format('DD/MM h:mm: A') + '</span>' +
                            '<br><span class="note">Được gửi bởi <b data-bind="text: UserName">' + response.data[i].from.name + '</b></span>' +
                            '</li>';
                    } else {
                        htmlMessage += '<li >' +
                            '<div>' +
                            '<p class="message">' +
                            '<span>' + response.data[i].message + '</span>' +
                            '</p>' +
                            '</div>' +
                            '<span class="time note" data-bind="timeshort: LastUpdatedDate">' + moment(new Date(response.data[i].created_time)).format('DD/MM h:mm: A') + '</span>' +
                            '<br><span class="note">Được gửi bởi <b data-bind="text: UserName">' + response.data[i].from.name + '</b></span>' +
                            '</li>';
                    }
                }

                $('#conversation-message-picture').html('');
                $('#conversation-message-picture').html(htmlMessage);
                var lastmessage = $('#conversation-message-picture');
                $('#message-type').val('message');
                $('#private-id-message').val(idConversation);
                $('.list-message').show();
                //scroll xuong cuoi trang chat
                var wtf = $(".list-message-container");
                var height = wtf[0].scrollHeight;
                $(".list-message-container").scrollTop(height);
            } else {
                dataJson = {error: 'Không lấy được danh sách tin nhắn'};
            }
        }
    );
}


function initWebsocket() {
    if ("WebSocket" in window) {
        console.log("browser support web socket");
        // Let us open a web socket
        ws = new WebSocket("wss://hoanmuada.cf:8444");

        ws.onopen = function () {
            console.log('create socket connection');
        };

        ws.onmessage = function (evt) {
            var received_msg = evt.data;
            console.log("receive message from websocket :" + JSON.stringify(received_msg))
            var changes = JSON.parse(received_msg);
            //tin nhan den page
            if (changes.messaging && changes.messaging[0].message) {
                getDetailMessageChange(changes.messaging[0].message.mid);
            }

            //thay doi tren conversation
            if (changes.changes && changes.changes[0].field == 'conversations' && changes.changes[0].value != null && changes.changes[0].value != undefined && changes.changes[0].value.thread_id) {
                //reload lai danh sach tin nhan
                getListMessage();
            }


        };
        ws.onerror = function (evt) {
            var received_msg = evt.data;
            console.log("receive error from websocket :" + JSON.stringify(received_msg))
        };
        ws.onclose = function () {
            // websocket is closed.
            console.log('close websocket');
        };

        window.onbeforeunload = function (event) {
            ws.close();
        };
    }

    else {
        // The browser doesn't support WebSocket
        alertify.error("Trình duyệt không hỗ trợ tính năng cập nhật trạng thái thời gian thực, vui lòng thử lại với trình duyệt khác!", 100);
    }
}


function hideComment(commentId) {
    FB.api(
        commentId,
        'POST',
        {
            'is_hidden': true,
            'access_token': pageAccessToken
        },
        function (response) {
            if (!response.error && response.success == true) {
                alertify.success('Đã ẩn tin nhắn');
            } else {
                alertify.error('Không ẩn được tin nhắn này');
            }
        }
    );
}

function privateReply(commentId, message) {
    FB.api(
        commentId + '/private_replies',
        'POST',
        {
            'message': message,
            'access_token': pageAccessToken
        },
        function (response) {
            if (!response.error) {
                alertify.success('Đã inbox cho người dùng');
            } else {
                alertify.error(response.error.message);
            }
        }
    );

}

//reaction comment
function reactionComment(commentId, reactions) {
    if (reactions == null || reactions == undefined || reactions == '') {
        reactions = 'LIKE';//reactions constants
    }
    FB.api(
        commentId,
        'POST',
        {
            'reactions': reactions,
            'access_token': pageAccessToken
        },
        function (response) {
            if (!response.error && response.success == true) {
                alertify.success('Đã thể hiện cảm xúc với comment trên');
            } else {
                alertify.error('Không thể biểu lộ cảm xúc với comment này');
            }
        }
    );
}


//lay thong tin chi tiet thay doi cua tin nhan
function getDetailMessageChange(messageId) {
    FB.api(
        'm_' + messageId,
        'GET',
        {
            'fields': 'message,id,created_time,from{id,picture,name},to{id,picture,name}',
            'access_token': pageAccessToken
        },
        function (response) {
            if (!response.error && response.success == true) {
                console.log(response);
            } else {
                //console.log('ko lấy được thông tin message mới');
            }
        }
    );

}

//lay thong tin chi tiet thay doi cua comment
function getDetailNewComment(commentId) {
    FB.api(
        commentId,
        'GET',
        {
            'fields': 'comment,id,created_time,from{id,picture,name},to{id,picture,name}',
            'access_token': pageAccessToken
        },
        function (response) {
            if (!response.error && response.success == true) {
                appendNewComment(response);
            } else {
                //console.log('ko lấy được thông tin message mới');
            }
        }
    );

}


function sendPrivateReply(commentId) {
    $('#comment-id-private-message').val(commentId);
    //$('#dialog-reply').modal().show();
}

function createTextboxQuickReply(commentId) {
    $('#comment-id-quick-reply').val(commentId);
}

function sendMessagePrivately() {
    var commentId = $('#comment-id-private-message').val();
    var message = $('#quick-private-reply-box').val();
    if (message == null || message == undefined || message.trim() == '') {
        alertify.error('Chưa nhập nội dung trước khi gửi');
    } else {
        privateReply(commentId, message);
    }
}


function sendQuickReply() {
    var commentId = $('#comment-id-quick-reply').val();
    var message = $('#quick-reply-box').val();
    if (message == null || message == undefined || message.trim() == '') {
        alertify.error('Chưa nhập nội dung trước khi gửi');
    } else {
        replyACommnent(commentId, message);
    }
}
