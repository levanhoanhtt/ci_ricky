var baseUrl = $('#baseUrl').attr('href');
$('.btn-add-form').click(function() {
    var clone = $('.box1').clone();
    $('.box-menus').append(clone);
});

$('body').on('change','.targetType', function () {
    var targetType = $(this).val();
    var formHtml = $(this);
    $.ajax({
        type: "POST",
        url: $('#apiTargetIdList').val(),
        data: {
            'targetType' : targetType,
            'targetId' : 0
        },
        success: function (response) {
            var json = $.parseJSON(response);
            $(formHtml).closest('.menu-form').find('#targetId').html(json.html);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
})

$('#phoneNumber').change(function () {
    var ivrId = $(this).val();
    $('#chart-container .orgchart').remove();
    $.ajax({
        type: "POST",
        url: $("#getListIvrUrl").val(),
        data: {
            ivrId: ivrId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            renderChart(json.data);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
})

function updateJsonChart(fullData, filterData) {
    for (var i = 0; i < fullData.length; i++) {
        fullData[i].children = [];
        fullData[i].value = 0;
    }
    for(var i = 0; i < filterData.length; i++) {
        var objIndex = fullData.findIndex((obj => obj.name == filterData[i].name));
        fullData[objIndex].value = parseInt(filterData[i].value);
    }

    var jsonRecuv = getNestedChildren(fullData, 0);
    var updateArrayValue = convert_value(convert_parent_value(jsonRecuv));
    renderChartKey(updateArrayValue)
}


$('body').on('keydown', '.numberValue', function (e) {
    if(checkKeyCodeNumber(e)) e.preventDefault();
}).on('keyup', '.numberValue', function(){
    var value = $(this).val();
    $(this).val(formatDecimal(value));
});

function renderChart(dataJson) {
    var dataRaw = getNestedChildren(dataJson, 0);
    var getId = function () {
        return (new Date().getTime()) * 1000 + Math.floor(Math.random() * 1001);
    };
    var diagrams = $('#chart-container').orgchart({
        'data'        : dataRaw[0],
        'nodeContent' : 'value',
        'createNode': function($node, data) {
            var inputEditName = '<input type="text" class="label-edit-name" autocomplete="off" name="name" />';
            var updateName    = '<i class="fa fa-check update-name"></i>';
            var ids           = '<input type="hidden" value="'+data.ids+'" class="id-node" data-type="'+data.type+'" data-name="'+data.name+'"/>'
            var parents       = '<input type="hidden" value="'+data.parents+'" class="parent-node"/>'
            var countChildren = '<input type="hidden" value="'+data.CountChildren+'" class="count-children"/>';
            $node.append(inputEditName).append(updateName);
            $node.append(ids);
            $node.append(parents);
            $node.append(countChildren);
        }
    });

    diagrams.$chart.on('nodedrop.orgchart', function(event, extraParams) {
        var dataSender = {
            ids            : extraParams.draggedNode.find('.id-node').val(),
            parents        : extraParams.dropZone.find('.id-node').val(),
            parameter_code : $('input#parameterCode').val(),
            order          : 1
        }
        $.ajax({
            type: "POST",
            url: $("#changeParentParameterUrl").val(),
            data: dataSender,
            success: function (response) {
                var json = $.parseJSON(response);
                if (json.code == 1) {
                    showNotification(json.message, json.code);
                    var data = json.dataChange;
                    renderVertical(data, 1);
                    var maxLevel = json.level;
                    var arrValue = setRowSpan(data, maxLevel);
                    $("#table-chart").html(Parametervalues_table_gop(arrValue, maxLevel));
                    $(".table_gop , .table_tach").css('width',maxLevel * 200);
                    $('#spanValueJson').html(JSON.stringify(arrValue));
                    return;
                }
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });

    });


    // Show input edit name node
    diagrams.$chartContainer.on('dblclick', '.title', function (e) {
        $(this).closest(".node").find(".label-edit-name").css("display", "block");
        $(this).closest(".node").find(".update-name").css("display", "block");
        $(this).closest(".node").find(".label-edit-name").val($(this).text());
    });

    function getValueNode(e) {
        let parentNew = $("#id-parameter").val();
        let id = $(e.target).closest(".node").find(".id-node").val();
        let parent = $(e.target).closest(".node").find(".parent-node").val();
        let countChildren = $(e.target).closest(".node").find(".count-children").val();
        let name = $(e.target).closest(".node").find(".label-edit-name").val();
        return {
            id: id,
            name: name,
            parent: parent,
            parentNew: parentNew,
            countChildren: countChildren,
        }
    }

    // Update name node event click button
    diagrams.$chartContainer.on('click', '.update-name', function (e) {
        handleUpdateNode(e)
    })

    // Update name node event enter
    diagrams.$chartContainer.on('keydown', '.label-edit-name', function (e) {
        if (e.keyCode == 13) {
            handleUpdateNode(e);
        }
    })

    function handleUpdateNode(e) {
        var selfs = $(e.target);
        let value = getValueNode(e);
        var ids   = value.id.length > 8 ? 0 : value.id;
        var parent = $('.orgchart').find('.focused .parent-node').val();
        var name = $('.orgchart').find('.focused .label-edit-name').val();
        var dataSender = {
            name: value.name,
            ids: ids,
            parameter_id: $('input#parameterId').val(),
            parents: parent,
            parameter_code: $('input#parameterCode').val(),
            order: 1
        }

        if (!name.trim()) {
            showNotification('Giá trị không được để trống', 0);
            return false;
        }

        if(parent == 'undefined') {
            showNotification('Bộ phận này không thể thêm', 0);
            return false;
        }

        $.ajax({
            type: "POST",
            url: $("#updateParameterUrl").val(),
            data: dataSender,
            success: function (response) {
                var json = $.parseJSON(response);
                selfs.closest(".node").find(".label-edit-name").css("display", "none");
                selfs.closest(".node").find(".update-name").css("display", "none");
                selfs.closest(".node").find(".title").text(value.name);
                var data = json.dataUpdate;
                renderVertical(data, 1);
                var maxLevel = json.level;
                var arrValue = setRowSpan(data, maxLevel);
                $("#table-chart").html(Parametervalues_table_gop(arrValue, maxLevel));
                $(".table_gop , .table_tach").css('width',maxLevel * 200);
                $('#spanValueJson').html(JSON.stringify(arrValue));
                $('#chart-container .focused').find('.id-node').val(json.data);
                $('.orgchart').find('.focused').closest('table').find('.lines .parent-node, .nodes .parent-node').val(json.data);
                showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    }

    // Xóa node
    diagrams.$chartContainer.on('keydown', '.node', function (e) {
        if (e.keyCode == 46) {
            var $node = $('#selected-node').data('node');
            var value = getValueNode(e);
            if (!window.confirm('Bạn có chắc chắn muốn xóa bộ phận này')) {
                return;
            }
            if (!value.id) {
                diagrams.removeNodes($node);
                return;
            }
            var params = {
                ids: value.id,
                parameter_code: $('input#parameterCode').val()
            }
            $.ajax({
                type: "POST",
                url: $("#deleteParameterUrl").val(),
                data: params,
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) {
                        diagrams.removeNodes($node);
                        var data = json.data;
                        renderVertical(data, 1  );
                        var maxLevel = json.level;
                        var arrValue = setRowSpan(data, maxLevel);
                        $("#table-chart").html(Parametervalues_table_gop(arrValue, maxLevel));
                        $(".table_gop , .table_tach").css('width',maxLevel * 200);
                        $('#spanValueJson').html(JSON.stringify(arrValue));
                    }
                    $('#selected-node').val('').data('node', null);
                    $('.popup-department-detail').css("display", "none");
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
    });

    // Gán value khi click node
    diagrams.$chartContainer.on('click', '.node', function () {
        var $this = $(this);
        $('#selected-node').val($this.find('.title').text()).data('node', $this);
    });

    diagrams.$chartContainer.on('click', '.orgchart', function (event) {
        if (!$(event.target).closest('.node').length) {
            $('#selected-node').val('');
            $('#check-children').val('')
        }
    });

    // Add new node
    diagrams.$chartContainer.on('keydown', '.node', function (e) {
        var parent = $(this).closest(".node").find(".id-node").val();
        var checkHidden = false;
        var i = 1;
        $(this).closest('table').find('tr:nth-child(2)').each(function () {
            if ($(this).hasClass("hidden") && i == 1) {
                checkHidden = true;
            }
            i++;
        });
        if (checkHidden) {
            return false;
        }
        var parent = $(this).closest(".node").find(".id-node").val();
        if (e.keyCode == 9) {
            var nodeVals = [" "];
            var $node = $('#selected-node').data('node');
            var hasChild = $node.parent().attr('colspan') > 0 ? true : false;
            var id = getId();
            if (!hasChild) {
                var rel = nodeVals.length > 1 ? '110' : '100';
                diagrams.addChildren($node, nodeVals.map(function(item) {
                    $('#name-department').val(' ');
                    $('.popup-department-detail').css("display", "none");
                    $('#id-parameter').val(parent);
                    $node.find()
                    return {
                        'name': item,
                        'relationship': rel,
                        'id': id,
                    };
                }));
            } else {
                diagrams.addSiblings($node.closest('tr').siblings('.nodes').find('.node:first'), nodeVals.map(function(item) {

                    $('#name-department').val('');
                    $('.popup-department-detail').css("display", "none");
                    $('#id-parameter').val(parent);
                    return {
                        'name': item,
                        'relationship': '110',
                        'id': id,
                    };
                }));
            }
        }
        $('.orgchart').find('.focused').closest('table').find('.lines .parent-node, .nodes .parent-node').val(parent);
    })

    // Filter node theo tên
    $('.content').on('click', '#filter-node', function (e) {
        e.preventDefault();
        var keyword = $("#text-filter").val();
        filterNodes(keyword);
    })

    // Reset chart khi value bằng null
    $('.content').on('click', '#text-filter', function (e) {
        if ($(this).val() == '') {
            clearFilterResult();
        }
    })

    // Reset chart khi click button
    $('.content').on('click', '#reset-chart', function (e) {
        e.preventDefault();
        $("#filter-path").removeAttr('disabled');
        clearFilterResult();
    })


    // Hàm xử lý reset chart
    function clearFilterResult() {
        $('.orgchart').removeClass('noncollapsable')
            .find('.node').removeClass('matched retained')
            .end().find('.hidden').removeClass('hidden')
            .end().find('.slide-up, .slide-left, .slide-right').removeClass('slide-up slide-right slide-left');
    }

    // convert json
    function getNestedChildren(arr, parents) {
        var out = [];
        for (var i in arr) {
            if (arr[i].parents == parents) {
                var children = getNestedChildren(arr, arr[i].ids)
                if (children.length) {
                    arr[i].children = children;
                }
                out.push(arr[i]);
            }
        }
        return out;
    }
}

$('input.iCheck').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue',
    increaseArea: '20%'
});

$('body').on('click', '.deleteFrom', function () {
    var length = $('.menu-form').length;
    if(length == 1) {
        return false;
    }
    $(this).closest('.menu-form').remove();
})

$('body').on('click', '.node.focused .diagramContent', function () {
    var queueId = $(this).closest('.node.focused').find('.id-node').val();
    var check = $(this).closest('.node.focused').find('.id-node').attr('data-type');
    $('#modalSelect').modal('show');
    // if(check == 'group') {
    //     $.ajax({
    //         type: "POST",
    //         url: $("#getListAgentUrl").val(),
    //         data: {
    //             queueId: queueId
    //         },
    //         success: function (response) {
    //             var json = $.parseJSON(response);
    //             $('#tbodyAgent').html(json.data);
    //             $('#modalAgent').modal('show');
    //         },
    //         error: function (response) {
    //             showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
    //         }
    //     });
    // }
})

$('body').on('click', '.tabAddIvr', function () {
    $('#modalAddIvr').modal('show');
})

$('body').on('click', '.tabAddGroup', function () {
    $('#modalAddGroup').modal('show');
})

$('body').on('click', '.tabAddExt', function () {
    $('#modalSelectExt').modal('show');
})

$('body').on('click', '.addStaffExt', function () {
    var length = $('body #modalAddExt').find('tbody tr').length;
    if(length > 0) {
        showNotification('Máy lẻ này đã có nhân viên được chọn', 0);
        return false;
    }
    $.ajax({
        type: "POST",
        url: $("#urlGetStaffList").val(),
        success: function (response) {
            var json = $.parseJSON(response);
            if(json.code == 1) {
                viewStaffList(json.data);
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
    $('#modalStaffList').modal('show');
})

// Handling delele staff extension
$('body').on('click', '#modalAddExt tbody .delete', function () {
    var parent = $(this);
    var id = $(parent).attr('data-id');
    swal({
        title: "Bạn có muốn xóa không?",
        text: "",
        icon: "warning",
        buttons: [
            'Không đồng ý',
            'Đồng ý'
        ],
        dangerMode: true,
    }).then(function (willDelete) {
        if (willDelete) {
            swal('Xóa thành công', {
                icon: "success",
                button: "Thoát"
            });
            $(parent).closest('.trItem').remove();
        }
    });
})

// Handling delele staff from staff list
$('body').on('click', '#modalStaffList tbody .delete', function () {
    var parent = $(this);
    var userId = $(this).attr('data-id');
    swal({
        title: "Bạn có muốn xóa không?",
        text: "",
        icon: "warning",
        buttons: [
            'Không đồng ý',
            'Đồng ý'
        ],
        dangerMode: true,
    }).then(function (willDelete) {
        $.ajax({
            type: "POST",
            url: $('#urlDeleteUser').val(),
            data: {
                userId: userId
            },
            success: function(result) {
                var json = $.parseJSON(result);
                viewStaffList(json.data);
                swal(json.message, {
                    icon: "success",
                    button: "Thoát"
                });
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
})

//Display modal form create extension
$('body').on('click', '.tabCreateExt', function () {
    $('#modalAddExt').modal('show');
})

//Display modal extension list
$('body').on('click', '.tabExtList', function () {
    $('#modalExtList').modal('show');
})

//Display modal add extesion group
$('body').on('click', '.addExtGroup', function () {
    $('#modalSelectExt').modal('show');
})

//Handling submit add staff for extension
$('body').on('click', '#modalStaffList .submit', function () {
    var length = $('body #modalStaffList').find('.checked').length;
    if(length > 1) {
        showNotification('Chỉ được phép chọn một nhân viên', 0);
        return false;
    } else if(length == 0) {
        showNotification('Bạn chưa chọn nhân viên', 0);
        return false;
    } else {
        var userId = $('body #modalStaffList').find('.checked input').val();
        var html = $('body #modalStaffList').find('.checked input').closest('.trItem').clone();
        html.find('.staffCheck').remove();
        $('#modalAddExt table tbody').html(html);
        $('#modalStaffList').modal('hide');
    }
})

//Handling submit add extension
$('body').on('click', '#modalAddExt .submit', function () {
    var length = $('body #modalAddExt').find('.trItem').length;
    if(length == 0) {
        showNotification('Bạn chưa chọn nhân viên', 0);
        return false;
    } else {

    }
})

//Handling view staff list ajax
function viewStaffList(staffList) {
    var ele = '';
    $.each( staffList, function( key, value ) {
        ele += '<tr class="trItem">';
        ele += '<td class="staffCheck"><input class="iCheck" type="checkbox" value="'+value.UserId+'"></td>';
        ele += '<td>'+value.UserName+'</td>';
        ele += '<td>'+value.FullName+'</td>';
        ele += '<td>'+value.PhoneNumber+'</td>';
        ele += '<td><a href="javascript:void(0)" data-id="'+value.UserId+'" class="btn btn-danger delete">Xóa</a></td>';
        ele += '</tr>';
    });
    $('#modalStaffList tbody').html(ele);
    icheck();
}

function icheck() {
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}



