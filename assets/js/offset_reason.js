$(document).ready(function(){
    $("#tbodyOffsetReason").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#offsetReasonId').val(id);
        $('input#offsetReasonName').val($('td#offsetReasonName_' + id).text());
        scrollTo('input#offsetReasonName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteOffsetReasonUrl').val(),
                data: {
                    OffsetReasonId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#offsetReason_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#offsetReasonForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#offsetReasonForm')) {
            var form = $('#offsetReasonForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    console.log(json)
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="offsetReason_' + data.OffsetReasonId + '">';
                            html += '<td id="offsetReasonName_' + data.OffsetReasonId + '">' + data.OffsetReasonName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.OffsetReasonId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.OffsetReasonId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyOffsetReason').prepend(html);
                        }
                        else{
                            $('td#offsetReasonName_' + data.OffsetReasonId).text(data.OffsetReasonName);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});