function chooseProduct(fnChooseProduct){
    
    $(document).on('click','.open-search',function(){
        $('.panelProduct').removeClass('active');
        $('.wrapper').removeClass('open-search');
    }).on('click', '.panel-default.active', function(e) {
        e.stopPropagation();
    });
    var statusSearch = null;

    // key f3 search product
    jwerty.key('f3', function () { 
        var idcheck = $("#idcheck").val();
        if($('.panelProduct').hasClass('active')){
            $('#panelProduct_'+idcheck).removeClass('active');
            $('#panelProduct_'+idcheck).find('.panel-body').css("width", "99%");
        }
        else{
            $('#panelProduct_'+idcheck).addClass('active');
            $('.wrapper').removeClass('open-search');
            setTimeout(function (){
                $('#panelProduct_'+idcheck).find('.panel-body').css("width", "100%");
                $('.wrapper').addClass('open-search');
            }, 100);
            $('input#pageIdProduct').val('1');
            getListProducts();
        }
    })

    $("body").on('click', '.txtSearchProduct', function(){
        var idcheck = $("#idcheck").val();
        if(idcheck == $(this).attr('id-check')){
            if($('.panelProduct').hasClass('active')){
                $('#panelProduct_'+idcheck).removeClass('active');
                $('#panelProduct_'+idcheck).find('.panel-body').css("width", "99%");
            }
            else{
                $('#panelProduct_'+idcheck).addClass('active');
                setTimeout(function (){
                    $('#panelProduct_'+idcheck).find('.panel-body').css("width", "100%");
                    $('.wrapper').addClass('open-search');
                }, 100);
                $('input#pageIdProduct').val('1');
                getListProducts();
            }
        }
        
    }).keydown(function () {
        if (statusSearch != null) {
            clearTimeout(statusSearch);
            statusSearch = null;
        }
    }).keyup(function () {
        if (statusSearch == null) {
            statusSearch = setTimeout(function () {
                if(!$('#panelProduct').hasClass('active')){
                    $('#panelProduct').addClass('active');
                    setTimeout(function (){
                        $('#panelProduct').find('.panel-body').css("width", "100%");
                        $('.wrapper').addClass('open-search');
                    }, 100);
                }
                $('input#pageIdProduct').val('1');
                getListProducts();
            }, 500);
        }
    });
    $("body").on('click', 'select.categoryId', function(){
        $('input#pageIdProduct').val('1');
        getListProducts();
    });
    $("body").on('click', '.btnPrevProduct', function(){
        if($(this).attr('id-check') == $("#idcheck").val()){
            var pageId = parseInt($('input#pageIdProduct').val());
            if($('input#pageIdProduct') > 1){
                $('input#pageIdProduct').val(pageId - 1);
                getListProducts();
            }
        }
        
    });
    $("body").on('click', '.btnNextProduct', function(){
        if($(this).attr('id-check') == $("#idcheck").val()){
            var pageId = parseInt($('input#pageIdProduct').val());
            $('input#pageIdProduct').val(pageId + 1);
            getListProducts();
        }
        
    });
    $("body").on('click', '.tbodyProductSearch tr', function(){
        $('.panelProduct').removeClass('active');
        $('.panelProduct').find('.panel-body').css("width", "99%");
        $('.txtSearchProduct').val('');
        $('select.categoryId').val('0');
        $('input#pageIdProduct').val('1');
        fnChooseProduct($(this));
    });
}

function getListProducts(){
    var idcheck = $("#idcheck").val();
    var loading = $('#panelProduct .search-loading');
    loading.show();
    $('.tbodyProductSearch').html('');
    $.ajax({
        type: "POST",
        url: $('input#getListProductUrl').val(),
        data: {
            SearchText: $('input.txtSearchProduct').val().trim(),
            CategoryId: $('select.categoryId').val(),
            PageId: parseInt($('input#pageIdProduct').val()),
            Limit: 10
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if (json.code == 1){
                loading.hide();
                var data = json.data;
                var html = '';
                var i, j;
                var productPath = $('input#productPath').val();
                var noImage = 'logo.png';
                for(i = 0; i < data.length; i++){
                    html += '<tr id-check="'+idcheck+'" class="pProduct" data-id="' + data[i].ProductId + '" data-child="0">';
                    html += '<td><img src="' + productPath + (data[i].ProductImage == '' ? noImage : data[i].ProductImage) + '" class="productImg"></td>';
                    html += '<td class="productName">' + data[i].ProductName + '</td>';
                    html += '<td>' + data[i].BarCode + '</td>';
                    html += '<td class="text-right">' + formatDecimal(data[i].Price.toString()) + '</td>';
                    html += '<td>' + data[i].GuaranteeMonth + ' tháng</td></tr>';
                    if(data[i].Childs.length > 0){
                        for(j = 0; j < data[i].Childs.length; j++){
                            html += '<tr class="cProduct" data-id="' + data[i].ProductId + '" data-child="' + data[i].Childs[j].ProductChildId + '">';
                            html += '<td><img src="' + productPath + (data[i].Childs[j].ProductImage == '' ? noImage : data[i].Childs[j].ProductImage) + '" class="productImg"></td>';
                            html += '<td class="productName">' + data[i].ProductName + ' (' + data[i].Childs[j].ProductName + ')</td>';
                            html += '<td>' + data[i].Childs[j].BarCode + '</td>';
                            html += '<td class="text-right">' + formatDecimal(data[i].Childs[j].Price.toString()) + '</td>';
                            html += '<td>' + data[i].GuaranteeMonth + ' tháng</td></tr>';
                        }
                    }
                }
                $('.tbodyProductSearch').html(html);
                $('.panelProduct .panel-body').slimScroll({
                    height: '300px',
                    alwaysVisible: true,
                    wheelStep: 20,
                    touchScrollStep: 500
                });
            }
            else loading.text('Có lỗi xảy ra').show();
        },
        error: function (response) {
            //showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            loading.text('Có lỗi xảy ra').show();
        }
    });
}

function chooseCustomer(fnChooseCustomer){
    $(document).on('click','.open-search-customer',function(){
        var idcheck = $("#idcheck").val();
        $('#panelCustomer_'+idcheck).removeClass('active');
        $('.wrapper').removeClass('open-search-customer');

    }).on('click', '.panel-default.active', function(e) {
        e.stopPropagation();
    });

    // key f4 search customer
    jwerty.key('f4', function () { 
        var idcheck = $("#idcheck").val();
        if ($('#panelCustomer_'+idcheck).hasClass('active')) {
            $('#panelCustomer_'+idcheck).removeClass('active');
            $('#panelCustomer_'+idcheck).find('panel-body').css("width", "99%");
        }
        else {
            $('#panelCustomer_'+idcheck).addClass('active');
            setTimeout(function () {
                $('#panelCustomer_'+idcheck).find('panel-body').css("width", "100%");
                $('.wrapper').addClass('open-search-customer');
            }, 100);
            $('input#pageIdCustomer').val('1');
            getListCustomers();
        }
    })

    var statusSearch = null;
    $("body").on('click', '.txtSearchCustomer', function(){
        var idcheck = $("#idcheck").val();
        if(idcheck == $(this).attr("id-check")){
            if ($('#panelCustomer_'+idcheck).hasClass('active')) {
                $('#panelCustomer_'+idcheck).removeClass('active');
                //$("#panelCustomer .panel-body").css("width", "99%");
                $('#panelCustomer_'+idcheck).find('panel-body').css("width", "99%");
            }
            else {
                $('#panelCustomer_'+idcheck).addClass('active');
                setTimeout(function () {
                    //$("#panelCustomer .panel-body").css("width", "100%");
                    $('#panelCustomer_'+idcheck).find('panel-body').css("width", "100%");
                    $('.wrapper').addClass('open-search-customer');
                }, 100);
                $('input#pageIdCustomer').val('1');
                getListCustomers();
            }
        }
        
    }).keydown(function () {
        if (statusSearch != null) {
            clearTimeout(statusSearch);
            statusSearch = null;
        }
    }).keyup(function () {
        if (statusSearch == null) {
            var idcheck = $("#idcheck").val();
            if(idcheck == $(this).attr("id-check")){
                statusSearch = setTimeout(function () {
                    if(!$('#panelCustomer_'+idcheck).hasClass('active')){
                        $('#panelCustomer_'+idcheck).addClass('active');
                        setTimeout(function (){
                            //$("#panelProduct .panel-body").css("width", "100%");
                            $('#panelCustomer_'+idcheck).find('.panel-body').css("width", "100%");
                            $('.wrapper').addClass('open-search');
                        }, 100);
                    }
                    $('input#pageIdCustomer').val('1');
                    getListCustomers();
                }, 500);
            }
        }
    });
    $("body").on('click', '.btnPrevCustomer', function(){
        var idcheck = $("#idcheck").val();
        if(idcheck == $(this).attr("id-check")){
            var pageId = parseInt($('input#pageIdCustomer').val());
            if (pageId > 1) {
                $('input#pageIdCustomer').val(pageId - 1);
                getListCustomers();
            }
        }
    });
    $("body").on('click', '.btnNextCustomer', function(){
        var idcheck = $("#idcheck").val();
        if(idcheck == $(this).attr("id-check")){
            var pageId = parseInt($('input#pageIdCustomer').val());
            $('input#pageIdCustomer').val(pageId + 1);
            getListCustomers();
        }
    });
    $("body").on('click', '.ulListCustomers li.row', function(){
        var idcheck = $("#idcheck").val();

        if($(this).attr('id-check') == idcheck){
            $('#panelCustomer_'+idcheck).removeClass('active');
            //$("#panelCustomer .panel-body").css("width", "99%");
            $('#panelCustomer_'+idcheck).find('panel-body').css("width", "99%");
            $('.txtSearchCustomer').val('');
            $('input#pageIdCustomer').val('1');
            fnChooseCustomer($(this));
        }
        
    });
}

function getListCustomers(){
    var loading = $('.panelCustomer .search-loading');
    var idcheck = $("#idcheck").val();
    loading.show();
    $('#ulListCustomers_'+idcheck).html('');
    $.ajax({
        type: "POST",
        url: $('input#getListCustomerUrl').val(),
        data: {
            SearchText: $('input.txtSearchCustomer').val().trim(),
            PageId: parseInt($('input#pageIdCustomer').val()),
            Limit: 10
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if (json.code == 1){
                loading.hide();
                var data = json.data;
                var html = '';
                for(var i = 0; i < data.length; i++){
                    html += '<li class="row" id-check="'+idcheck+'" data-id="' + data[i].CustomerId + '"><div class="wrap-img inline_block vertical-align-t radius-cycle"><img class="thumb-image radius-cycle" src="assets/vendor/dist/img/users.png" title="" /></div><div class="inline_block ml10">';
                    html += '<p class="pCustomerName">' + data[i].FullName + '</p><p>' + data[i].PhoneNumber;
                    if(data[i].PhoneNumber2 != '') html += ' - ' + data[i].PhoneNumber2;
                    html += '</p></div></li>';
                }
                
                $('#ulListCustomers_'+idcheck).html(html).slimScroll({
                    height: '250px',
                    alwaysVisible: true,
                    wheelStep: 20,
                    touchScrollStep: 500,
                    color: '#3c8dbc'
                });
            }
            else loading.text('Có lỗi xảy ra').show();
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            loading.hide();
        }
    });
}