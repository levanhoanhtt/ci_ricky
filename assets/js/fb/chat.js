(function($) {
    "use strict";
    $(document).ready(function() {
        var _height = $(window).height();
        var _list_height = _height - $('.top-left').height() - $('#header').height();
        $('.equal-height').css('height', _list_height);
        var mes_height = _list_height - $('.message-container .action').height() -50;
        $('.post-mes').css('max-height', mes_height);

        $('.slimScroll').slimScroll({
            height: '720px',
            alwaysVisible: true,
            wheelStep: 20,
            touchScrollStep: 500,
            start: 'bottom'
            //scrollTo : $('#listChat').prop('scrollHeight') + 'px'
        });

        /**
            TODO:
            - @author DatNguyen
            - Ajax Call Load More Inbox
        */
         fb.init();
         var fb = {
            init: function() {
                this.getLoadMore(),
            },
            getLoadMore : function(){
                //Ajax Call Load More Inbox
                $("body").on('click', '.btn-load-more', function(event) {
                    event.preventDefault();
                    $.ajax({
                        url: '/path/to/file',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            param1: 'value1'
                        },
                    })
                    .done(function(data) {
                        console.log(data);
                    })
                    .fail(function(error) {
                        console.log(error);
                    });
                });
            }
         };
        
    });
})(jQuery);