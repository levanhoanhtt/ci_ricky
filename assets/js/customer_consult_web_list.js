$(document).ready(function () {
    // dateRangePicker();
    actionItemAndSearch({
        ItemName: 'Khách hàng quan tâm',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });

    $("#tbodyCustomerConsult").on('click', 'a.change_status', function(){
        var customerConsultId = parseInt($(this).attr('data-id'));
        if(customerConsultId > 0){
            if(confirm('Chuyển sang bên tư vấn lại?')){
                $.ajax({
                    type: "POST",
                    url: $('input#changeStatusUrl').val(),
                    data: {
                        CustomerConsultId: customerConsultId
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 1){

                        }
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }else showNotification("Có lỗi xảy ra, vui lòng thử lại", 0);
        return false;
    })
});

function renderContentCustomerConsults(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditCustomerConsult = $('#urlEditCustomerConsult').val() + '/';
        var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        var trClass = '';
        for (var item = 0; item < data.length; item++) {
            trClass = '';
            var email = "", product = "";
            $.each(data[item].Products, function(k,v){
                product += '<p>'+v.ProductName+'</p>';
            });
            var orderChannel = "RICKY.VN";
            if(data[item].OrderChannelId != 3) orderChannel = data[item].OrderChannel;
          
            if(data[item].CustomerInfo != "") email = $.parseJSON(data[item].CustomerInfo).CustomerEmail;
            // if(data[item].RemindDayDiff > 0 && data[item].RemindStatusId == 1) trClass = ' trItemCancel';
            html += '<tr id="trItem_'+data[item].CustomerConsultId+'" class="trItem' + trClass + '">';
            html += '<td>'+ getDayText(data[item].CrDayDiff) +  data[item].CrDateTime +'</td>';
            html += '<td>' + data[item].FullName + '</td>';
            html += '<td>' + data[item].PhoneNumber + '</td>'; 
            html += '<td>' + email + '</td>';
            html += '<td>' + product + '</td>';
            html += '<td>' + (data[item].Consultcomment != undefined ? data[item].Consultcomment : '') + '</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].OrderChannelId] + '">' + orderChannel + '</span></td>';
            if(data[item].RemindStatusId == '1') html += '<td class="text-center tdStatusName"><a href="javascript:void(0)" class="change_status" data-id="'+data[item].CustomerConsultId+'"><i class="fa fa-mail-forward"></i> <span class="' + labelCss[data[item].RemindStatusId] + '">' + data[item].RemindStatus + '</span></a></td>';
            else html += '<td class="text-center tdStatusName"><span class="' + labelCss[data[item].RemindStatusId] + '">' + (data[item].RemindStatusId == '1' ? 'Chờ xử lý' : 'Đã xử lý') + '</span></td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="8" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}