$(document).ready(function () {
    // dateRangePicker();
    actionItemAndSearch({
        ItemName: 'Tư vấn lại',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });

    $("body").on('click', '.showComment', function(){
    	var customerConsultId = parseInt($(this).data('id'));
    	if(customerConsultId > 0){
    		$.ajax({
                type: "POST",
                url: $('#urlCommentConsult').val(),
                data: {
                	CustomerConsultId: customerConsultId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        showInfoComment(json.data);
                        $("#modalItemComment").modal('show');
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
    	}
    	return false;
    })
});

function showInfoComment(data){
	var html = '';
	for (var item = 0; item < data.length; item++) {
		html += '<div class="box-customer mb10" data-id="'+data[item].UserId+'" data-consult="'+data[item].ConsultCommentId+'">';
		html += '<table><tbody>';
		html += '<tr>';
		html += '<th rowspan="2" valign="top" style="width: 50px;"><img src="'+data[item].Avatar+'>" alt=""></th>';
		html += '<th><a href="javascript:void(0)" class="name">'+data[item].FullName+'</a></th>';
		html += '<th class="time">'+data[item].CrDateTime+'</th>';
		html += '</tr>';
		html += '<tr>';
		html += '<td><p class="pComment">'+data[item].Comment+'</p></td>';
		var icon = '';
		if(data[item].Flag) icon = '<i class="fa fa-flag" style="margin-right: 5px;color: #357ebd;"></i>';
		var label = 'label-default';
		var labelName = 'Facebook';
		if(data[item].CommentTypeId == 1){
			labelName = 'Gọi điện';
			label = 'label-success';
		} 
		html += '<td class="tdIcon">'+icon+'<span class="label '+label+'">'+labelName+'</span></td>';
		html += '</tr>';
		html += '</tbody></table>';
		html += '</div>';
	}
	$('.listComment').html(html);
}

function renderContentCustomerConsults(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditCustomerConsult = $('#urlEditCustomerConsult').val() + '/';
        var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        var urlInfoCustomer = $('#urlInfoCustomer').val() + '/';
        var trClass = '';
        for (var item = 0; item < data.length; item++) {
            trClass = '';
            if(data[item].RemindDayDiff > 0 && data[item].RemindStatusId == 1) trClass = ' trItemCancel';
            html += '<tr id="trItem_'+data[item].CustomerConsultId+'" class="trItem' + trClass + '">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].CustomerConsultId + '"></td>';
            html += '<td><a href="' + urlEditCustomerConsult + data[item].CustomerConsultId  + '">DT-' + (10000 + parseInt(data[item].CustomerConsultId)) + '</a></td>';
            html += '<td>'+ getDayText(data[item].CrDayDiff) + data[item].CrDateTime +'</td>';
            if(parseInt(data[item].RemindStatusId) == 2){ // if(data[item].RemindDayDiff > 0 && parseInt(data[item].RemindStatusId) != 5 && parseInt(data[item].RemindStatusId) != 6){
                if(data[item].RemindDayDiff > 0){
                    if(data[item].ConsultDate != '') html += '<td> Quá hạn ' + data[item].RemindDayDiff + ' ngày</td>';
                    else html += '<td></td>';
                }else{
                    if(data[item].ConsultDate != '') html += '<td> '+ getDayText(data[item].RemindDayDiff) + data[item].ConsultDate +'</td>';
                    else html += '<td></td>';
                }
                
            }
            else html += '<td> -- </td>';
            // else html += '<td> '+ getDayText(data[item].RemindDayDiff) + data[item].ConsultDate +'</td>';
            html += '<td  class="text-center"><a href="javascript:void(0)" class="showComment" data-id="'+data[item].CustomerConsultId+'">' + data[item].TimeProcessed + ' lần</a></td>';
            if(data[item].CustomerId != '0') html += '<td class="'+data[item].Color+'"><a href="' + urlEditCustomer + data[item].CustomerConsultId + '">'+ data[item].FullName +'</a></td>';
            else html += '<td class="'+data[item].Color+'"><a href="' + urlInfoCustomer + data[item].CustomerConsultId + '">'+ data[item].FullName +'</a></td>';
            html += '<td>'+ getDayText(data[item].UpdateDateTime) +  data[item].UpdateDateTime +'</td>';
            html += '<td data-start="'+data[item].ConsultStar+'" data-id="'+data[item].CustomerConsultId+'"><div id="consultStar_'+data[item].CustomerConsultId+'"></div></td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].RemindStatusId] + '">' + data[item].RemindStatus + '</span></td>';
            html += '</tr>';

            
        }
        html += '<tr><td colspan="10" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
        $(".trItem").each(function(){
        	var numberStart = $(this).find('td').eq(7).data('start');
        	var id = $(this).find('td').eq(7).data('id');
        	$("#consultStar_"+id).starrr({
		        rating: parseInt(numberStart),
                change: function(e, value){
                    console.log('new rating is ' + value);
                    $.ajax({
                        type: "POST",
                        url: $('#updateStarUrl').val(),
                        data: {
                            CustomerConsultId: id,
                            ConsultStar: value
                        },
                        success: function (response) {
                            var json = $.parseJSON(response);
                            showNotification(json.message, json.code);
                        },
                        error: function (response) {
                            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        }
                    });
                }
		    });
        });
        
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}