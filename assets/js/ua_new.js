var countUpTime;
var elements = {
    phoneInput: document.getElementById('phone-input'),
    callStatus: document.getElementById('call-status'),
    callButton: document.getElementById('call-button'),
    player: document.getElementById('player'),
    logger: document.getElementById('logger'),

    configForm: document.getElementById('config-form'),
    clickCall: '0',
    callCustomer: 'callWeb',
    registerButton: document.getElementById('ua-register'),
    newSessionForm: document.getElementById('new-session-form'),
    inviteButton: document.getElementById('ua-invite-submit'),
    messageButton: document.getElementById('ua-message-submit'),

    uaURI: document.getElementById('ua-uri'),
    statusClose: 'closePhone',
    sessionList: document.getElementById('session-list'),
    sessionTemplate: document.getElementById('session-template'),
    messageTemplate: document.getElementById('message-template')
};

$('body').on('click', '.detail-call', function () {
    var phoneInput = $('#phone-input').val();
    $.ajax({
        type: "POST",
        url: $("#getUpdateIsCallAgainUrl").val(),
        data: {
            phoneNumber:phoneInput
        },
        success: function (response) {
        },
        error: function (response) {
        }
    });
    callModal(phoneInput);
    call();
});

$('#call-customer-number').click(function() {
    var val = $('#call-number').val();
    if(val == '') {
        showNotification('Vui lòng nhập số điện thoại', 0);
        $('#call-number').focus();
        return;
    }

    if(!isNumeric(val)){
        showNotification('Số điện thoại chỉ có thể nhập vào số', 0);
        return;
    }

    if(val.length>11){
        showNotification('Số điện thoại không vượt quá 11 số', 0);
        return;
    }
    
    if(val.length<10){
        showNotification('Số điện thoại không nhỏ quá 10 số', 0);
        return;
    }
    
    elements.phoneInput.value = val;
    callModal(val);
    call();
    
});

function callModal(param){
    var callModal = $('#modalAnswer');
    callModal.modal('show');
    $('.PhoneNumber').text(param);
    $('.note-call').addClass('show');
    startTimer();
    callModal.find('.box-call-end').toggleClass('col-6 col-12');
    callModal.find('.box-call-answer').hide();
}

$('#call-answer').click(function(){
    $(this).closest('.form-row').find('.box-call-end').toggleClass('col-6 col-12');
    $(this).closest('.form-row').find('.box-call-answer').hide();
    answer();
    startTimer();
});

function startTimer(){
    var timer = $('#timer');
    if(timer.find('.status').length>0){
        clearInterval(countUpTime);
        timer.empty();
    }
    var startTimestamp = moment().startOf("day");
    countUpTime = setInterval(function() {
        startTimestamp.add(1, 'second');
        var time = startTimestamp.format('HH:mm:ss');
        timer.html('<p class="status">'+time+'</p>');
    }, 1000);
}

$('#call-end').click(function(){
    var boxCallEnd = $(this).closest('.form-row').find('.box-call-end');
    if(boxCallEnd.hasClass('col-12')){
        boxCallEnd.toggleClass('col-12 col-6');
        $(this).closest('.form-row').find('.box-call-answer').show();
    }
    $('.note-call').removeClass('show');
    stopTimer();
    $('#modalAnswer').modal('hide');
    hangup();
});

function stopTimer(){
    var timer = $('#timer');
    if(timer.find('.status').length!=1){
        return;
    }
    clearInterval(countUpTime);
    timer.empty();
}

var config0 = {
    userAgentString: 'SIP.js/0.7.0 BB',
    traceSip: false,
    register: true,
    displayName: $('#userName').val(),
    uri: $('#uri').val(),
    authorizationUser: $('#userName').val(),
    password: $('#passWord').val(),
    wsServers: "wss://pbx01.cmctelecom.vn:7443"
};

var config = config0;

var status = "idle";
var ua;
var session;
var sessionUIs = {};

/* var interval = setInterval(function(){
  let html = "";
  if(session){
    html = "<br/>accept:"+ (session.accept !== undefined).toString();
    html += "<br/>reject:"+ (session.reject !== undefined).toString();
    html += "<br/>cancel:"+ (session.cancel !== undefined).toString();
    html += "<br/>startTime:"+ session.startTime;
    html += "<br/>status:"+ status.toString();
  }
  elements.logger.innerHTML = html;
},1000); */
function hangup() {
    sipHangUp();
}
function call() {
    elements.statusClose = 'closeWeb';
    elements.callCustomer = 'callWeb';
    sipCall();
}

setInterval(function(){
    if(elements.clickCall == '1') {
        document.getElementById("callPlay").play();
    }
}, 1000);

// setInterval(updateCallLog, 60000 * 30); //30p

function updateCallLog() {
    $.ajax({
        type: "POST",
        url: $("#getCallLogUpdateUrl").val(),
        data: {
        },
        success: function (response) {
        },
        error: function (response) {
        }
    });
}

$('.search-listchat input').keyup(function () {
    var value = $(this).val();
    if(value == '') {
        $(".listchat-each.custommer-info").show();
    } else {
        value = convertToEnglish(value.toLowerCase());
        $(".listchat-each.custommer-info").each(function( index ) {
            var text = $(this).find('.title').text();
            var desc = $(this).find('.desc').text();
            text = convertToEnglish(text.toLowerCase());
            text = '@'+text+'@'+desc+'@';
            var n = text.indexOf(value);
            $(this).hide();
            if(n > 0) {
                $(this).show();
            }
        });
    }
})

function convertToEnglish(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}


var sTransferNumber;
var oRingTone, oRingbackTone;
var oSipStack, oSipSessionRegister, oSipSessionCall, oSipSessionTransferCall;
var videoRemote, videoLocal, audioRemote;
// var bFullScreen = false;
var oNotifICall;
var bDisableVideo = false;
var viewVideoLocal, viewVideoRemote, viewLocalScreencast; // <video> (webrtc) or <div> (webrtc4all)
var oConfigCall;
var oReadyStateTimer;

/**
 * Step 1: init stack
 */
function pageLoaded() {
    // videoLocal = document.getElementById("video_local");
    // videoRemote = document.getElementById("video_remote");
    audioRemote = document.getElementById("audio_remote");

    oConfigCall = {
        audio_remote: audioRemote,
        video_local: viewVideoLocal,
        video_remote: viewVideoRemote,
        // screencast_window_id: 0x00000000, // entire desktop
        // bandwidth: { audio: undefined, video: undefined },
        // video_size: { minWidth: undefined, minHeight: undefined, maxWidth: undefined, maxHeight: undefined },
        events_listener: { events: '*', listener: onSipEventSession },
        sip_caps: [
                        { name: '+g.oma.sip-im' },
                        { name: 'language', value: '\"en,fr\"' }
        ]
    };

    // initialize SIPML5
    SIPml.init(readyCallback, errorCallback);
}

var readyCallback = function(e){
    createSipStack(); // see next section
};
var errorCallback = function(e){
    console.error('Failed to initialize the engine: ' + e.message);
}

function createSipStack(){
    oSipStack = new SIPml.Stack({
            realm: 'pbx01.cmctelecom.vn', // mandatory: domain name
            impi: $('#userName').val(), // mandatory: authorization name (IMS Private Identity)
            impu: $('#uri').val(), // mandatory: valid SIP Uri (IMS Public Identity)
            password: $('#passWord').val(), // optional
            display_name: $('#userName').val(), // optional
            websocket_proxy_url: $('#wsServers').val(), // optional
            outbound_proxy_url: null, // optional
            enable_rtcweb_breaker: false, // optional
            events_listener: { events: '*', listener: onSipEventStack }, // optional: '*' means all events
            sip_headers: [ // optional
                    { name: 'User-Agent', value: 'IM-client/OMA1.0 sipML5-v1.0.0.0' },
                    { name: 'Organization', value: 'Doubango Telecom' }
            ]
        }
    );
    oSipStack.start();
}

// Callback function for SIP Stacks
function onSipEventStack(e /*SIPml.Stack.Event*/) {
    tsk_utils_log_info('==stack event = ' + e.type);
    switch (e.type) {
        case 'started':
            {
                // catch exception for IE (DOM not ready)
                try {
                    // LogIn (REGISTER) as soon as the stack finish starting
                    oSipSessionRegister = this.newSession('register', {
                        expires: 200,
                        events_listener: { events: '*', listener: onSipEventSession },
                        sip_caps: [
                                    { name: '+g.oma.sip-im', value: null },
                                    //{ name: '+sip.ice' }, // rfc5768: FIXME doesn't work with Polycom TelePresence
                                    { name: '+audio', value: null },
                                    { name: 'language', value: '\"en,fr\"' }
                        ]
                    });
                    oSipSessionRegister.register();
                }
                catch (e) {
                    // txtRegStatus.value = txtRegStatus.innerHTML = "<b>1:" + e + "</b>";
                    btnRegister.disabled = false;
                }
                break;
            }
        case 'stopping': case 'stopped': case 'failed_to_start': case 'failed_to_stop':
            {
                var bFailure = (e.type == 'failed_to_start') || (e.type == 'failed_to_stop');
                oSipStack = null;
                oSipSessionRegister = null;
                oSipSessionCall = null;

                // txtCallStatus.innerHTML = '';
                // txtRegStatus.innerHTML = bFailure ? "<i>Disconnected: <b>" + e.description + "</b></i>" : "<i>Disconnected</i>";
                break;
            }

        case 'i_new_call':
            {
                if (oSipSessionCall) {
                    // do not accept the incoming call if we're already 'in call'
                    e.newSession.hangup(); // comment this line for multi-line support
                }
                else {
                    oSipSessionCall = e.newSession;
                    // start listening for events
                    oSipSessionCall.setConfiguration(oConfigCall);
                    openAnswerModal();
                }
                break;
            }

        case 'm_permission_requested':
            {
                break;
            }
        case 'm_permission_accepted':
        case 'm_permission_refused':
            {
                break;
            }

        case 'starting': default: break;
    }
};

/**
 * Step 2: make actions
 */
// makes a call (SIP INVITE)
function sipCall() {
    var phoneNumber = elements.phoneInput.value;
    if (oSipStack && !oSipSessionCall && !tsk_string_is_null_or_empty(phoneNumber)) {
        // create call session
        oSipSessionCall = oSipStack.newSession('call-audio', oConfigCall);
        // make call
        if (oSipSessionCall.call(phoneNumber) != 0) {
            oSipSessionCall = null;
            // txtCallStatus.value = 'Failed to make call'; (text cho element)
            return;
        }
    }
    else if (oSipSessionCall) {
        // txtCallStatus.innerHTML = '<i>Connecting...</i>';
        oSipSessionCall.accept(oConfigCall);
    }
}

// terminates the call (SIP BYE or CANCEL)
function sipHangUp() {
    if (oSipSessionCall) {
        // txtCallStatus.innerHTML = '<i>Terminating the call...</i>';
        oSipSessionCall.hangup({ events_listener: { events: '*', listener: onSipEventSession } });
    }
}

// Callback function for SIP sessions (INVITE, REGISTER, MESSAGE...)
function onSipEventSession(e /* SIPml.Session.Event */) {
    tsk_utils_log_info('==session event = ' + e.type);

    switch (e.type) {
        case 'connecting': case 'connected':
            {
                break;
            } // 'connecting' | 'connected'
        case 'terminating': case 'terminated':
            {
                elements.clickCall = '0';
                if (e.session == oSipSessionRegister) {
                    oSipSessionCall = null;
                    oSipSessionRegister = null;

                    // txtRegStatus.innerHTML = "<i>" + e.description + "</i>";
                }
                else if (e.session == oSipSessionCall) {
                    uiCallTerminated(e.description);
                }
                break;
            } // 'terminating' | 'terminated'

        case 'm_stream_video_local_added':
            {
                break;
            }
        case 'm_stream_video_local_removed':
            {
                break;
            }
        case 'm_stream_video_remote_added':
            {
                break;
            }
        case 'm_stream_video_remote_removed':
            {
                break;
            }

        case 'm_stream_audio_local_added':
        case 'm_stream_audio_local_removed':
        case 'm_stream_audio_remote_added':
        case 'm_stream_audio_remote_removed':
            {
                break;
            }

        case 'i_ect_new_call':
            {
                break;
            }

        case 'i_ao_request':
            {
                break;
            }

        case 'm_early_media':
            {
                break;
            }

        case 'm_local_hold_ok':
            {
                break;
            }
        case 'm_local_hold_nok':
            {
                break;
            }
        case 'm_local_resume_ok':
            {
                break;
            }
        case 'm_local_resume_nok':
            {
                break;
            }
        case 'm_remote_hold':
            {
                break;
            }
        case 'm_remote_resume':
            {
                break;
            }
        case 'm_bfcp_info':
            {
                break;
            }

        case 'o_ect_trying':
            {
                break;
            }
        case 'o_ect_accepted':
            {
                break;
            }
        case 'o_ect_completed':
        case 'i_ect_completed':
            {
                break;
            }
        case 'o_ect_failed':
        case 'i_ect_failed':
            {
                break;
            }
        case 'o_ect_notify':
        case 'i_ect_notify':
            {
                break;
            }
        case 'i_ect_requested':
            {
                break;
            }
    }
}

function uiCallTerminated(s_description) {
    oSipSessionCall = null;
    closeCallModal();
    // txtCallStatus.innerHTML = "<i>" + s_description + "</i>";

    // setTimeout(function () { if (!oSipSessionCall) txtCallStatus.innerHTML = ''; }, 2500);
}

function answer() {
    elements.callCustomer = 'callWeb';
    elements.clickCall = '0';
    
    $('#modalAnswer').find('.box-call-end').show();
    oSipSessionCall.accept(oConfigCall);
}

function closeCallModal()
{
    var boxCallEnd = $('#call-end').closest('.form-row').find('.box-call-end');
    if(boxCallEnd.hasClass('col-12')){
        boxCallEnd.toggleClass('col-12 col-6');
        $('#call-end').closest('.form-row').find('.box-call-answer').show();
    }
    $('.note-call').removeClass('show');
    stopTimer();
    $('#modalAnswer').modal('hide');
}

function openAnswerModal()
{
    elements.clickCall = '1';
    var callModal = $('#modalAnswer');
    callModal.modal('show');
    $('.PhoneNumber').text(elements.phoneInput.value);
    $('.note-call').addClass('show');
    // startTimer();
    callModal.find('.box-call-end').hide();
    callModal.find('.box-call-answer').removeClass('col-6').addClass('col-12');
}