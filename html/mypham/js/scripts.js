var app = app || {};

app.init = function () {

  app.smooth_scroll();
  app.smooth_scroll2();
  app.sample();
  app.button_menu();
  app.openmenu();
  app.count_header();
  app.handle();
  app.zoom_eff();
  app.tabs();
  app.select_rating();
  app.add_act();


}
app.smooth_scroll = function(){
	$('a.scroll').click(function() {
   var speed = 500;
   var href= $(this).attr("href");
   var target = $(href == "#" || href == "" ? 'html' : href);
   var position = target.offset().top;
   $('body,html').animate({scrollTop:position}, speed, 'swing');
   
   return false;
   });
}
app.smooth_scroll2 = function(){
	$('a.scroll-argency').click(function() {
	var speed = 500;
	var href= $(this).attr("href");
	var target = $(href == "#" || href == "" ? 'html' : href);
	var position = target.offset().top;
	$('body,html').animate({scrollTop:position}, speed, 'swing');
	$('#navigation').toggleClass("active");
	$('.overlay').toggleClass("active");
	$('#btn-close').toggleClass("active");
   	return false;
   });
}

app.sample = function() {

  $('.bxslider').bxSlider({
    mode: 'fade',
    auto: true,
    pause : 6000,
    speed : 1200
 
  });

}
app.button_menu = function(){
	$('.nav li a.has-arrow').click(function(){
		$(this).toggleClass("active");
		$(this).next().stop().slideToggle();
	})
}
app.openmenu = function(){
	$('#bt-menu').click(function(){
		$('#navigation').toggleClass("active");
		$('.overlay').toggleClass("active");
		$('#btn-close').toggleClass("active");
	})
	$('#btn-close').click(function(){
		$('#navigation').toggleClass("active");
		$('.overlay').toggleClass("active");
		$(this).toggleClass("active");
	})
}
app.count_header = function(){
	
	$(window).on('load scroll resize', function () {
		var height_hd = $('header').outerHeight();
		$('body').css("padding-top",height_hd)
	});

	$(window).scroll(function () {
		if ($(this).scrollTop() > 300) {
			$('header').addClass("stuck");
		} else {
			$('header').removeClass("stuck");
		}
	});
 
	
}

app.handle = function (){
	
	$(".click-down").click(function() {
		var val = parseInt($(this).next().val());
		if(val <=0){
			val = 0;
			$(this).next().val(val);
		}
		else{
			val = val - 1;
			$(this).next().val(val);
		}
		return false;
	});
	$(".click-up").click(function() {
		var val = parseInt($(this).prev().val());
			val = val + 1;
			$(this).prev().val(val);
		return false;
	});
}
app.zoom_eff = function(){
	$('#zoom-eff').zoom();
}

app.tabs = function(){
	$(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
}

app.select_rating = function(){
	$(".stars.selected a").click(function(){
		$(this).siblings().removeClass("active");
		$(this).addClass("active");
		return false;
	})
}

app.add_act = function(){
	$(".add-ctv-par").click(function(){
		$(this).parent().toggleClass("active");
		return false;
	});
}
$(function() {

  app.init();

});