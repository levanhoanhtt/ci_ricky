<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 200px;">Tên trang FB</th>
                                <th>Thư viện</th>
                                <th style="width: 85px;">Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyLibraryPage">
                            <?php
                            foreach($listFbPages as $p){
                            	$librarySentenceIds = $this->Mlibrarypages->getListFieldValue(array('FbPageId' => $p['FbPageId']), 'LibrarySentenceId'); ?>
                                <tr>
                                    <td><?php echo $p['FbPageName']; ?></td>
                                    <td>
                                    	<select class="form-control select2" id="librarySentence_<?php echo $p['FbPageId']; ?>" multiple="multiple" data-placeholder="Chọn thư viện">
	                                        <?php foreach($listLibrarySentences as $c){?>
	                                            <option value="<?php echo $c['LibrarySentenceId']; ?>" data-type="<?php echo $c['LibrarySentenceId']; ?>"<?php if(in_array($c['LibrarySentenceId'], $librarySentenceIds)) echo ' selected="selected"'; ?>><?php echo $c['LibraryName']; ?></option>
	                                        <?php } ?>
	                                    </select>
                                    </td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $p['FbPageId']; ?>" title="Sửa"><i class="fa fa-save"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="updateLibraryPageUrl" value="<?php echo base_url('librarypage/update') ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>