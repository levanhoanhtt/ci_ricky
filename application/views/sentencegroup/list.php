<?php $this->load->view('includes/header'); ?>
	<div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('sentencegroup/update', array('id' => 'sentencegroupsForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Tên nhóm câu</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodySentencegroups">
                            <?php
                            foreach($listSentencegroups as $bt){ ?>
                                <tr id="sentencegroup_<?php echo $bt['SentenceGroupId']; ?>">
                                    <td id="sentenceGroupName_<?php echo $bt['SentenceGroupId']; ?>"><?php echo $bt['SentenceGroupName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $bt['SentenceGroupId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $bt['SentenceGroupId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="sentenceGroupName" name="SentenceGroupName" value="" data-field="Tên thư viện"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="SentenceGroupId" id="sentenceGroupId" value="0" hidden="hidden">
                                    <input type="text" id="deleteSentencegroupUrl" value="<?php echo base_url('sentencegroup/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>