<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php if ($customerId > 0) { ?>
                <section class="content-header">
                    <h1><?php echo $title; ?></h1>
                    <ul class="list-inline">
                        <li><a href="<?php echo base_url('customer'); ?>" class="btn btn-default">Đóng</a></li>
                    </ul>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-sm-8 no-padding">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li<?php if($tabId == 1) echo ' class="active"'; ?>><a href="#tab_1" data-toggle="tab" data-id="1">Đơn hàng</a></li>
                                    <li<?php if($tabId == 2) echo ' class="active"'; ?>><a href="#tab_2" data-toggle="tab" data-id="2">Tài chính</a></li>
                                    <li<?php if($tabId == 4) echo ' class="active"'; ?>><a href="#tab_4" data-toggle="tab" data-id="4">Tư vấn lại</a></li>
                                    <li<?php if($tabId == 5) echo ' class="active"'; ?>><a href="#tab_5" data-toggle="tab" data-id="5">CSKH</a></li>
                                    <li<?php if($tabId == 3) echo ' class="active"'; ?>><a href="#tab_3" data-toggle="tab" data-id="3">Thông tin khách hàng</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane<?php if($tabId == 1) echo ' active'; ?>" id="tab_1">
                                        <div class="box-body">
                                            <p>
                                                Số đơn hàng thành công: &nbsp;&nbsp;&nbsp;&nbsp;
                                                <span class="i-order-status">
                                                    <?php echo $successOrder . " / " . $totalOrder; ?>
                                                </span>
                                            </p>
                                            <p>
                                                Tổng tiền hàng thành công: &nbsp;<span class="i-total-order-customer"><?php echo priceFormat($successCost); ?></span> ₫
                                            </p>
                                        </div>
                                        <div class="box-body table-responsive divTable">
                                            <table class="table new-style table-hover table-bordered table-data" id="table-data-order">
                                                <thead>
                                                <tr>
                                                    <th>Mã đơn hàng <a href="<?php echo base_url('order/add/'.$customer['CustomerKindId'].'/'.$customerId); ?>" title="Thêm đơn hàng"><i class="fa fa-plus"></i></a></th>
                                                    <th>Ngày tạo</th>
                                                    <th class="text-center">TT đơn hàng</th>
                                                    <th class="text-center">TT Thanh toán</th>
                                                    <th class="text-center">TT giao hàng</th>
                                                    <th class="text-right">Tổng tiền</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbodyOrderCustomer"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane<?php if($tabId == 2) echo ' active'; ?>" id="tab_2">
                                        <div class="box-body">
                                            <div class="">
                                                Số dư tài khoản (công nợ): &nbsp;&nbsp;&nbsp;&nbsp;
                                                <span class="i-order-status">
                                                    0
                                                </span> ₫
                                            </div>
                                            <div class="">
                                                Tổng thu thực: &nbsp;
                                                <span class="i-total-order-customer">
                                                    0
                                                </span> ₫
                                            </div>
                                            <div class="">
                                                Tổng chi thực: &nbsp;
                                                <span class="i-total-order-customer">
                                                    0
                                                </span> ₫
                                            </div>
                                        </div>
                                        <div class="box-body table-responsive divTable">
                                            <table class="table new-style table-hover table-bordered table-data" id="table-data-transaction-type1">
                                                <thead>
                                                <tr>
                                                    <th>Mã phiếu thu <a href="<?php echo base_url('transaction/add/1/'.$customerId); ?>" title="Thêm phiếu thu"><i class="fa fa-plus"></i></a></th>
                                                    <th>Ngày tạo</th>
                                                    <th>Lý do</th>
                                                    <th>Loại tiền</th>
                                                    <th class="text-right">Số tiền</th>
                                                    <th class="text-center">Trạng thái</th>
                                                    <th>Ghi chú</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbodyTransactionType1"></tbody>
                                            </table>
                                        </div>
                                        <hr>
                                        <div class="box-body table-responsive divTable">
                                            <table class="table new-style table-hover table-bordered table-data" id="table-data-transaction-type2">
                                                <thead>
                                                <tr>
                                                    <th>Mã phiếu chi <a href="<?php echo base_url('transaction/add/2/'.$customerId); ?>" title="Thêm phiếu chi"><i class="fa fa-plus"></i></a></th>
                                                    <th>Ngày tạo</th>
                                                    <th>Lý do</th>
                                                    <th>Loại tiền</th>
                                                    <th class="text-right">Số tiền</th>
                                                    <th class="text-center">Trạng thái</th>
                                                    <th>Ghi chú</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbodyTransactionType2"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane<?php if($tabId == 4) echo ' active'; ?>" id="tab_4">
                                        <div class="box-body table-responsive divTable">
                                            <table class="table new-style table-hover table-bordered table-data" id="table-data-consult">
                                                <thead>
                                                <tr>
                                                    <th>Tiêu đề tư vấn</th>
                                                    <th class="text-center">Số lần TVL</th>
                                                    <th>Thời điểm cần xử lý</th>
                                                    <th>Ngày tạo</th>
                                                    <th>Người xem</th>
                                                    <th class="text-center">Trạng thái</th>
                                                    <th class="text-center">Kênh</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbodyCustomerConsult"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane<?php if($tabId == 5) echo ' active'; ?>" id="tab_5">
                                        <div class="box-body table-responsive divTable">
                                            <table class="table new-style table-hover table-bordered table-data" id="table-data-sms-log">
                                                <thead>
                                                <tr>
                                                    <th>Ngày</th>
                                                    <th>Loại</th>
                                                    <th>Bởi</th>
                                                    <th>Nội dung</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbodyLogSms">
                                                <?php if(count($listSmsLog) > 0){
                                                    foreach ($listSmsLog as $s){
                                                    ?>
                                                        <tr>
                                                            <td><?php echo ddMMyyyy($s['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                                            <td><?php echo $this->Mconstants->statusSMS['statusSms'][$s['SMSTypeId']]; ?></td>
                                                            <td><?php echo $s['FullName']; ?></td>
                                                            <td><?php echo $s['SendContent']; ?></td>
                                                        </tr>
                                                <?php }} ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane<?php if($tabId == 3) echo ' active'; ?>" id="tab_3">
                                        <?php echo form_open('api/customer/update', array('id' => 'customerForm')); ?>
                                        <div class="flexbox-annotated-section">
                                            <div class="flexbox-annotated-section-annotation">
                                                <div class="annotated-section-title pd-all-20"><h2>Thông tin chung</h2></div>
                                                <div class="annotated-section-description pd-all-20 p-none-t">
                                                    <p class="color-note tabcustomer in privateCus">Một số thông tin cơ bản của khách hàng.</p>
                                                    <p class="color-note tabcustomer corporateCus">Một số thông tin cơ bản của người đại diện doanh nghiệp.</p>
                                                </div>
                                            </div>
                                            <div class="flexbox-annotated-section-content">
                                                <div class="box box-default padding15">
                                                    <div class="box-header with-border">
                                                        <div class="row">
                                                            <div class="col-sm-12" style="margin-bottom: 10px;">
                                                                <ul class="nav nav-tabs" id="ulCustomerKindId">
                                                                    <?php foreach($this->Mconstants->customerKinds as $i => $v ){ ?>
                                                                        <li class="<?php echo $i == $customer['CustomerKindId'] ? 'active':''; ?>">
                                                                            <a href="javascript:void(0)" data-toggle="tab" data-id="<?php echo $i; ?>"><?php echo $v; ?></a>
                                                                        </li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="radio-group">
                                                                    <span class="item"><input type="radio" name="CustomerTypeId" class="iCheck iCheckCustomerType" value="1"<?php if($customer['CustomerTypeId'] == 1) echo ' checked' ?>> Cá nhân</span>
                                                                    <span class="item"><input type="radio" name="CustomerTypeId" class="iCheck iCheckCustomerType" value="2"<?php if($customer['CustomerTypeId'] == 2) echo ' checked' ?>> Công ty</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <span class="item"><input type="checkbox" class="iCheck" id="isReceiveAd"<?php if($customer['IsReceiveAd'] == 2) echo ' checked' ?>> Có Nhận Quảng Cáo</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-body">
                                                        <div class="row">

                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Họ <span class="required">*</span></label>
                                                                    <input type="text" id="firstName" class="form-control hmdrequired" value="<?php echo $customer['FirstName']; ?>" data-field="Họ">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Tên <span class="required">*</span></label>
                                                                    <input type="text" id="lastName" class="form-control hmdrequired" value="<?php echo $customer['LastName']; ?>" data-field="Tên">
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Số điện thoại</label>
                                                                    <input type="text" id="phoneNumber" class="form-control" value="<?php echo $customer['PhoneNumber']; ?>" data-field="Số điện thoại">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Email </label>
                                                                    <input type="text" id="email" class="form-control" value="<?php echo $customer['Email']; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Giới tính</label>
                                                                    <div class="radio-group">
                                                                        <span class="item"><input type="radio" name="GenderId" class="iCheck" value="1"<?php if($customer['GenderId'] == 1) echo ' checked' ?>> Nam</span>
                                                                        <span class="item"><input type="radio" name="GenderId" class="iCheck" value="2"<?php if($customer['GenderId'] == 2) echo ' checked' ?>> Nữ</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Ngày sinh </label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </span>
                                                                        <input type="text" class="form-control datepicker" id="birthDay" value="<?php echo ddMMyyyy($customer['BirthDay']); ?>" autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label class="control-label">Mã FB Hara</label>
                                                                    <input type="text" id="facebook" class="form-control" value="<?php echo $customer['Facebook'] ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row tabcustomer corporateCus">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Chức vụ </label>
                                                                    <input type="text" id="positionName" class="form-control" value="<?php echo $customer['PositionName']; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="corporateCus tabcustomer">
                                            <hr class="hr-new">
                                            <div class="flexbox-annotated-section">
                                                <div class="flexbox-annotated-section-annotation">
                                                    <div class="annotated-section-title pd-all-20">
                                                        <h2>Thông tin công ty</h2>
                                                    </div>
                                                    <div class="annotated-section-description pd-all-20 p-none-t">
                                                        <p class="color-note">Nhập thông tin về công ty.</p>
                                                    </div>
                                                </div>
                                                <div class="flexbox-annotated-section-content">
                                                    <div class="box box-default padding15">
                                                        <div class="box-body">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Tên công ty </label>
                                                                        <input type="text" id="companyName" class="form-control" value=""<?php echo $customer['CompanyName']; ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Mã số thuế </label>
                                                                        <input type="text" id="taxCode" class="form-control" value=""<?php echo $customer['TaxCode']; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="hr-new">
                                        <div class="flexbox-annotated-section">
                                            <div class="flexbox-annotated-section-annotation">
                                                <div class="annotated-section-title pd-all-20">
                                                    <h2>Địa chỉ</h2>
                                                </div>
                                                <div class="annotated-section-description pd-all-20 p-none-t">
                                                    <p class="color-note tabcustomer in privateCus">Địa chỉ chính của khách hàng này.</p>
                                                    <p class="color-note tabcustomer corporateCus">Địa chỉ của công ty.</p>
                                                </div>
                                            </div>
                                            <div class="flexbox-annotated-section-content">
                                                <div class="box box-default padding15">
                                                    <div class="box-body">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Quốc gia </label>
                                                                    <?php $this->Mconstants->selectObject($listCountries, 'CountryId', 'CountryName', 'CountryId', $customer['CountryId'], false, '', ' select2'); ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 VNon">
                                                                <div class="form-group">
                                                                    <label class="control-label">Tỉnh / Thành </label>
                                                                    <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', $customer['ProvinceId'], true, '--Tỉnh / Thành--', ' select2'); ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 VNoff" style="display: none">
                                                                <div class="form-group">
                                                                    <label class="control-label">Postal / Zip Code</label>
                                                                    <input type="text" id="zipCode" class="form-control" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row VNon">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Quận/ Huyện</label>
                                                                    <?php echo $this->Mdistricts->selectHtml($customer['DistrictId'], 'DistrictId', $listDistricts); ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Phường / Xã </label>
                                                                    <?php echo $this->Mwards->selectHtml($customer['WardId']); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Địa chỉ </label>
                                                            <input type="text" id="address" class="form-control" value="<?php echo $customer['Address']; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="privateCus tabcustomer in">
                                            <hr class="hr-new">
                                            <div class="flexbox-annotated-section">
                                                <div class="flexbox-annotated-section-annotation">
                                                    <div class="annotated-section-title pd-all-20">
                                                        <h2>Ghi chú</h2>
                                                    </div>
                                                    <div class="annotated-section-description pd-all-20 p-none-t">
                                                        <p class="color-note">Nhập ghi chú về khách hàng.</p>
                                                    </div>
                                                </div>
                                                <div class="flexbox-annotated-section-content">
                                                    <div class="box box-default padding15">
                                                        <div class="box-body">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Nhóm khách hàng</label>
                                                                        <select class="form-control" name="CustomerGroupId" id="customerGroupId">
                                                                            <option value="0">--Chọn--</option>
                                                                            <?php foreach($listCustomerGroups as $cg){ ?>
                                                                                <option value="<?php echo $cg['CustomerGroupId']; ?>" class="op op_<?php echo $cg['CustomerKindId']; ?>"<?php if($cg['CustomerKindId'] != $customer['CustomerKindId']) echo ' style="display: none;"'; if($cg['CustomerGroupId'] == $customer['CustomerGroupId']) echo ' selected="selected"'; ?>><?php echo $cg['CustomerGroupName']; ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6"></div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Ghi chú </label>
                                                                <textarea class="form-control" id="customerComment" rows="2"><?php echo $customer['Comment']; ?></textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Nhãn (cách nhau bởi dấu phẩy)</label>
                                                                <input type="text" class="form-control" id="tags">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Thêm các nhãn đã có</label>
                                                                <ul class="list-inline" id="ulTagExist">
                                                                    <?php foreach($listTags as $t) { ?>
                                                                        <li><a href="javascript:void(0)"><?php echo $t['TagName']; ?></a></li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="hr-new">
                                            <div class="flexbox-annotated-section">
                                                <div class="flexbox-annotated-section-annotation">
                                                    <div class="annotated-section-title pd-all-20">
                                                        <h2>Cài đặt nâng cao</h2>
                                                    </div>
                                                    <div class="annotated-section-description pd-all-20 p-none-t">
                                                        <p class="color-note">Cài đặt nâng cao</p>
                                                    </div>
                                                </div>
                                                <div class="flexbox-annotated-section-content">
                                                    <button class="advance-btn"><i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                                    <div class="box box-default padding15 advance-tab" style="display: none">
                                                        <div class="box-body">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Nhân viên chăm sóc chính</label>
                                                                        <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'CareStaffId', $customer['CareStaffId'], true, '--Nhân viên chăm sóc chính--', ' select2'); ?>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Chiết khấu giá</label>
                                                                        <?php $this->Mconstants->selectConstants('discountType', 'DiscountTypeId', $customer['DiscountTypeId'], true, '--Chiết khấu giá--'); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="divWholesale" style="display: none;">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Cấu hình nợ </label>
                                                                        <input type="text" id="debtCost" class="form-control" value="<?php echo priceFormat($customer['DebtCost']); ?>" data-field="Cấu hình nợ">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Kì hạn thanh toán</label>
                                                                        <?php $this->Mconstants->selectConstants('paymentTime', 'PaymentTimeId', $customer['PaymentTimeId'], true, '--Kì hạn thanh toán--'); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="divCTV">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">CTV</label>
                                                                        <?php $this->Mconstants->selectObject($listCTVs, 'CustomerId', 'FullName', 'CTVId', $customer['CTVId'], true, '--Chọn--', ' select2'); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Mật khẩu</label>
                                                                        <input type="text" id="password" class="form-control" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Nhắc lại mật khẩu</label>
                                                                        <input type="text" id="rePass" class="form-control" value="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="list-inline pull-right margin-right-10">
                                            <li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
                                            <li><a href="<?php echo base_url('customer'); ?>" id="customerListUrl" class="btn btn-default">Đóng</a></li>
                                            <input type="text" hidden="hidden" id="customerId" value="<?php echo $customerId; ?>">
                                            <input type="text" hidden="hidden" id="getListOrderUrl" value="<?php echo base_url('api/order/searchByFilter');?>">
                                            <input type="text" hidden="hidden" id="getListTransactionUrl" value="<?php echo base_url('api/transaction/searchByFilter');?>">
                                            <input type="text" hidden="hidden" id="getListConsultUrl" value="<?php echo base_url('customerconsult/searchByFilter');?>">
                                            <input type="text" hidden="hidden" id="getListSmsLogUrl" value="<?php echo base_url('smslog/listSmsLogCustomer');?>">
                                            <input type="text" hidden="hidden" id="insertCustomerCommentUrl" value="<?php echo base_url('api/customer/insertComment'); ?>">
                                            <input type="hidden" id="urlEditOrder" value="<?php echo base_url('order/edit'); ?>">
                                            <input type="hidden" id="urlEditTransaction" value="<?php echo base_url('transaction/edit'); ?>">
                                            <input type="hidden" id="urlEditConsult" value="<?php echo base_url('customerconsult/edit'); ?>">
                                            <input type="text" hidden="hidden" id="statusId" value="<?php echo $customer['StatusId']; ?>">
                                            <input type="text" hidden="hidden" id="customerKindId" value="<?php echo $customer['CustomerKindId'] ?>">
                                            <?php foreach($tagNames as $tagName){ ?>
                                                <input type="text" hidden="hidden" class="tagName" value="<?php echo $tagName; ?>">
                                            <?php } ?>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <?php echo form_close(); ?>
                                        <?php $this->load->view('includes/action_logs_new'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="box box-default mh-wrap-customer" id="divCustomer">
                                <div class="with-border">
                                    <h3 class="box-title">Thông tin khách hàng</h3>
                                    <div class="mh-info-customer">
                                        <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                                        <div class="name-info">
                                            <h4 class="i-name"><?php echo $customer['FullName']; ?></h4>
                                            <div class="phones i-phone"><?php echo $customer['PhoneNumber']; if(!empty($customer['PhoneNumber2'])) echo ' - '.$customer['PhoneNumber2']; ?></div>
                                        </div>
                                        <div class="i-cusType">
                                            <?php $customerGroupName = $this->Mconstants->getObjectValue($listCustomerGroups, 'CustomerGroupId', $customer['CustomerGroupId'], 'CustomerGroupName');
                                            if(!empty($customerGroupName)) echo '<span class="label label-success">'.$customerGroupName.'</span>'; ?>
                                        </div>
                                        <div class="bank">
                                            <i class="fa fa-money" aria-hidden="true"></i><span>Số dư tài khoản :&nbsp;&nbsp;<span id="customerBalance"><?php echo priceFormat($customer['Balance']); ?></span> ₫</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div>
                                        <h4 class="mgbt-20 light-blue">Địa chỉ</h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="item">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                <span class="i-name"><?php echo $customer['FullName']; ?></span>
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-phone" aria-hidden="true"></i>
                                                <span class="i-phone"><?php echo $customer['PhoneNumber']; ?></span>
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                                <span class="i-email"><?php echo $customer['Email']; ?></span>
                                            </div>
                                            <div class="item i-address">
                                                <?php if(empty($customer['ZipCode']) || $customer['ZipCode'] == '0'){ ?>
                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                <span class="i-ward"><?php echo $customer['Address'] . ' ' . $this->Mwards->getWardName($customer['WardId']); ?></span>
                                                <span class="br-line i-district"><?php echo $this->Mconstants->getObjectValue($listDistricts, 'DistrictId', $customer['DistrictId'], 'DistrictName'); ?></span>
                                                <span class="br-line i-province"><?php echo $this->Mconstants->getObjectValue($listProvinces, 'ProvinceId', $customer['ProvinceId'], 'ProvinceName'); ?></span>
                                                <?php } else{ ?>
                                                    <i class="fa fa-list-alt" aria-hidden="true"></i>
                                                    <span class="i-province">ZipCode: <?php echo $customer['ZipCode']; ?></span>
                                                <?php } ?>
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-id-card" aria-hidden="true"></i>
                                                <span class="i-country">
                                                    <?php $countryName = $this->Mconstants->getObjectValue($listCountries, 'CountryId', $customer['CountryId'], 'CountryName');
                                                    if(empty($countryName)) $countryName = 'Việt Nam';
                                                    echo $countryName; ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box box-default classify padding20">
                                <div class="form-group">
                                    <label class="control-label">Ghi chú</label>
                                    <div class="box-transprt clearfix mb10">
                                        <button type="button" class="btn-updaten save" id="btnInsertComment">Lưu</button>
                                        <input type="text" class="add-text" id="comment" value="" placeholder="Thêm nội dung ghi chú...">
                                    </div>
                                </div>
                                <div id="listComment">
                                    <?php $i = 0;
                                    foreach($listCustomerComments as $oc){
                                        $avatar = (empty($oc['Avatar']) ? NO_IMAGE : $oc['Avatar']);
                                        $i++;
                                        if($i < 3){ ?>
                                            <div class="box-customer mb10">
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <th rowspan="2" valign="top" style="width: 50px;"><img src="<?php echo USER_PATH.$avatar; ?>" alt=""></th>
                                                        <th><a href="javascript:void(0)" class="name"><?php echo $oc['FullName']; ?></a></th>
                                                        <th class="time"><?php echo ddMMyyyy($oc['CrDateTime'], 'H:i d/m/Y'); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <p class="pComment"><?php echo $oc['Comment']; ?></p>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php }
                                    } ?>
                                </div>
                                <?php if(count($listCustomerComments) > 2){ ?>
                                    <div class="text-right light-dark">
                                        <a href="javascript:void(0)" id="aShowComment">Xem tất cả &gt;&gt;</a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('includes/modal/comment', array('itemName' => 'khách hàng', 'listItemComments' => $listCustomerComments)); ?>
                </section>
            <?php } else { ?>
                <section class="content"><?php $this->load->view('includes/notice'); ?></section>
            <?php } ?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>