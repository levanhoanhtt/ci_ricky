<div class="modal fade" id="modalProduct" tabindex="-1" role="dialog" aria-labelledby="modalProduct">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-product-hunt"></i> Thêm sản phẩm Bảo hành Đổi trả</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Tìm kiếm">
                </div>
                <div class="box-body table-responsive divTable no-padding">
                    <table class="table new-style table-hover table-bordered">
                        <thead>
                        <tr>
                            <th style="width: 80px;">ĐH</th>
                            <th>Sản phẩm</th>
                            <th class="text-center" style="width: 60px;">Đơn vị</th>
                            <th style="width: 50px;">SL</th>
                            <th style="width: 100px;">Ngày mua</th>
                            <th style="width: 30px;"></th>
                        </tr>
                        </thead>
                        <tbody id="tbodyProductSearch"></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="modalVideo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-youtube-play"></i> Video Bảo hành Đổi trả</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" class="form-control" id="vìdeoUrl" value="" placeholder="Link Youtube Url">
                </div>
                <div class="form-group">
                    <iframe id="iframeVideo" src="" frameborder="0" allowfullscreen style="width: 100%;height: 490px;"></iframe>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" id="btnUpdateVideo">Cập nhật</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
