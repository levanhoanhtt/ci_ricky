<div class="table-responsive no-padding divTable divTableGuarantee" id="divTableGuarantee_1"<?php if($guarantee['GuaranteeStep'] > 2) echo ' style="display: none;"'; ?>>
    <table class="table table-hover table-bordered-bottom">
        <thead class="theadNormal">
        <tr>
            <th style="width: 80px;">ĐH</th>
            <th>Sản phẩm</th>
            <th style="width: 60px;">Đơn vị</th>
            <th style="width: 50px;">SL</th>
            <th style="width: 90px;">Ngày mua</th>
            <th style="width: 140px;">Hình thức</th>
            <th style="width: 200px;">Phụ kiện</th>
            <th class="text-center" style="width: 60px;">Ảnh</th>
            <?php if($canEditAll) echo '<th style="width: 30px;"></th>'; ?>
        </tr>
        </thead>
        <tbody class="tbodyProduct" id="tbodyProduct_1">
        <?php $i = 0;
        foreach($listGuaranteeProducts as $op){
            $i++;
            $productAccessoryIds = array();
            if(!empty($op['ProductAccessoryIds'])) $productAccessoryIds = json_decode($op['ProductAccessoryIds'], true);
            $productName = $products[$op['ProductId']]['ProductName'];
            //$productImage = $products[$op['ProductId']]['ProductImage'];
            $productUnitId = $products[$op['ProductId']]['ProductUnitId'];
            $guaranteeMonth = $products[$op['ProductId']]['GuaranteeMonth'];
            $productChildName = '';
            if($op['ProductChildId'] > 0){
                $productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
                //$productImage = $productChilds[$op['ProductChildId']]['ProductImage'];
                $guaranteeMonth = $productChilds[$op['ProductChildId']]['GuaranteeMonth'];
            }
            //if(empty($productImage)) $productImage = NO_IMAGE; ?>
            <tr data-index="<?php echo $i; ?>" class="trProductMain trProduct_1_<?php echo $i; ?>" data-id="<?php echo $op['GuaranteeProductId']; ?>" data-product="<?php echo $op['ProductId']; ?>" data-child="<?php echo $op['ProductChildId']; ?>" data-order="<?php echo $op['OrderId']; ?>">
                <td><a href="<?php echo base_url('order/edit/'.$op['OrderId']); ?>" target="_blank"><?php echo $this->Morders->genOrderCode($op['OrderId']); ?></a></td>
                <td>
                    <a href="<?php echo base_url('product/edit/'.$op['ProductId']); ?>" class="light-dark" target="_blank">
                        <?php echo $productName;
                        if(!empty($productChildName)) echo ' ('.$productChildName.')'; ?>
                    </a>
                </td>
                <td class="text-center"><?php echo $this->Mconstants->getObjectValue($listProductUnits, 'ProductUnitId', $productUnitId, 'ProductUnitName'); ?></td>
                <td class="quantity">1</td>
                <td><?php echo ddMMyyyy($orders[$op['OrderId']]['CrDateTime']); ?></td>
                <td class="trPadding">
                    <select class="form-control usageStatusId1">
                        <option value="0">--Chọn--</option>
                        <?php foreach($listUsageStatus as $us){
                            if($us['UsageStatusTypeId'] == 1){ ?>
                            <option value="<?php echo $us['UsageStatusId']; ?>"<?php if($us['UsageStatusId'] == $op['UsageStatusId1']) echo ' selected="selected"'; ?>><?php echo $us['UsageStatusName']; ?></option>
                        <?php } } ?>
                    </select>
                </td>
                <td class="trPadding">
                    <?php foreach($productAccessories[$op['ProductId']] as $pa){ ?>
                        <p class="item"><input type="checkbox" class="iCheck iCheckAccessory accessoryId_<?php echo $pa['ProductId']; ?>" value="<?php echo $pa['ProductAccessoryId']; ?>"<?php if(in_array($pa['ProductAccessoryId'], $productAccessoryIds)) echo ' checked="checked"'; if(!$canEditAll) echo ' disabled="disabled"'; ?>><span class="spanCheck"><?php echo $pa['AccessoryName']; ?></span></p>
                    <?php } ?>
                </td>
                <td class="text-center">
                    <a href="javascript:void(0)" class="aProductImage">
                        <?php if(!empty($op['ProductImage1'])) echo '<img src="'.GUARANTEE_PATH.$op['ProductImage1'].'" style="height: 50px;">';
                        else echo '<i class="fa fa-file-image-o"></i>'; ?>
                    </a>
                </td>
                <?php if($canEditAll){ ?><td class="text-right"><a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a></td><?php } ?>
            </tr>
            <tr class="trPadding trProduct_1_<?php echo $i; ?>">
                <td colspan="<?php echo $canEditAll ? 9 : 8; ?>">
                    <div class="input-group" style="width: 100%;">
                        <input type="text" class="form-control comment1_<?php echo $op['GuaranteeProductId']; ?>" id="productComment_1_<?php echo $i; ?>" value="<?php echo $op['Comment1']; ?>" placeholder="Ghi chú mô tả">
                        <span class="input-group-addon spanComment" data-id="<?php echo $op['GuaranteeProductId']; ?>" data-no="1"><i class="fa fa-paper-plane"></i></span>
                    </div>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>