<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo" style="margin-bottom: 0 !important;"><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a id="aGuaranteeList" href="<?php echo base_url('guarantee'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content new-box-stl ft-seogeo">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($guaranteeId > 0){ ?>
                <?php echo form_open('api/guarantee/update', array('id' => 'guaranteeForm')); ?>
                <div class="row">
                    <div class="col-sm-9 no-padding">
                        <style>
                            .nav-tabs-custom>.nav-tabs>li>a.active{color: #3c8dbc !important;}
                            .tbodyProduct .trPadding, #tbodyProductMoney .trPadding{padding: 0 2px;}
                            .tbodyProduct .spanComment{width: 37px;cursor: pointer;color: #3c8dbc}
                            #tbodyProductMoney .spanPrice{color: #357ebd;}
                            #tbodyProductMoney .spanDiscount{color: red;}
                            #tbodyProductMoney i.fa-info{cursor: pointer;color: #357ebd;}
                            /*#tbodyProductMoney .spanControl{cursor: pointer;}
                            #tbodyProductMoney .spanControl.has-border{border-right: 1px solid #d2d6de;}
                            #tbodyProductMoney .spanControl.active{background-color: #357ebd;color: #fff;}*/
                            #divGuaranteeInfo p{padding: 0 20px 0;color: #357ebd;}
                            #divGuaranteeInfo ul{padding: 5px 10px 10px;border: 1px solid #357ebd;margin: 0 10px 10px;border-radius: 10px;}
                        </style>
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="ulGuaranteeStep">
                                <?php foreach($this->Mconstants->guaranteeSteps as $i => $v){ ?>
                                    <li<?php if($i == $guarantee['GuaranteeStep']) echo ' class="active"'; ?>>
                                        <a href="javascript:void(0)" data-id="<?php echo $i; ?>" data-toggle="tab"<?php if($i == $guarantee['GuaranteeStep']) echo ' class="active"'; ?>><?php echo $v; ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <p>Ngày nhận: <?php echo ddMMyyyy($guarantee['CrDateTime'], 'd/m/Y H:i') ?></p>
                                <p>Bộ phận: Kho vận</p>
                                <p>Người xử lý: <?php $this->Musers->getFieldValue(array('UserId' => $guarantee['CrUserId']), 'FullName'); ?></p>
                                <hr class="hr-ths">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>Danh sách sản phẩm đã nhận</p>
                                        <p><button type="button" class="btn btn-default" id="btnAddProduct">Thêm sản phẩm BH</button></p>
                                        <p>Danh sách sản phẩm đã nhận</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p><a href="javascript:void(0)" id="aGuaranteeImage"><i class="fa fa-file-image-o"></i> Ảnh chụp toàn bộ kiện hàng</a></p>
                                        <p><a href="" id="aGuaranteeImage1" target="_blank"><img src="<?php echo GUARANTEE_PATH.$guarantee['GuaranteeImage']; ?>" id="imgGuaranteeImage" style="height: 70px;<?php if(empty($guarantee['GuaranteeImage'])) echo 'display: none;'; ?>"></a></p>
                                    </div>
                                </div>
                                <?php $products = array();
                                $productChilds = array();
                                $productAccessories = array();
                                $orders = array();
                                foreach($listGuaranteeProducts as $op) {
                                    if(!isset($products[$op['ProductId']])) $products[$op['ProductId']] = $this->Mproducts->get($op['ProductId'], true, '', 'ProductName, ProductImage, Price, ProductUnitId, GuaranteeMonth');
                                    if(!isset($productAccessories[$op['ProductId']])) $productAccessories[$op['ProductId']] = $this->Mproductaccessories->getBy(array('ProductId' => $op['ProductId'], 'StatusId' => STATUS_ACTIVED));
                                    if(!isset($orders[$op['OrderId']])) $orders[$op['OrderId']] = $this->Morders->get($op['OrderId'], true, '', 'Discount, CrDateTime');
                                    if($op['ProductChildId'] > 0) if(!isset($productChilds[$op['ProductChildId']])) $productChilds[$op['ProductChildId']] = $this->Mproductchilds->get($op['ProductChildId'], true, '', 'ProductName, ProductImage, Price, GuaranteeMonth');
                                }
                                $this->load->view('guarantee/includes/product_1', array('products' => $products, 'productChilds' => $productChilds, 'productAccessories' => $productAccessories, 'orders' => $orders));
                                $this->load->view('guarantee/includes/product_3', array('products' => $products, 'productChilds' => $productChilds, 'productAccessories' => $productAccessories, 'orders' => $orders));
                                $this->load->view('guarantee/includes/product_4', array('products' => $products, 'productChilds' => $productChilds, 'productAccessories' => $productAccessories, 'orders' => $orders)); ?>
                            </div>
                        </div>
                        <?php $this->load->view('includes/action_logs_new'); ?>
                    </div>
                    <div class="col-sm-3">
                        <div class="box box-default" id="boxChooseCustomer">
                            <div class="box-header with-border">
                                <h3 class="box-title">Khách hàng</h3>
                            </div>
                            <div class="box-body">
                                <div class="box-search-advance customer">
                                    <div>
                                        <input type="text" class="form-control textbox-advancesearch" id="txtSearchCustomer" placeholder="Tìm khách hàng (F4)">
                                    </div>
                                    <div class="panel panel-default" id="panelCustomer">
                                        <div class="panel-body">
                                            <div class="list-search-data">
                                                <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                <ul id="ulListCustomers"></ul>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default" id="btnPrevCustomer"><i class="fa fa-chevron-left"></i></button>
                                                <button type="button" class="btn btn-default" id="btnNextCustomer"><i class="fa fa-chevron-right"></i></button>
                                                <input type="text" hidden="hidden" id="pageIdCustomer" value="1">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default mh-wrap-customer" id="divCustomer" style="display: none;">
                            <div class="with-border">
                                <h3 class="box-title">Thông tin khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnCloseBoxCustomer"><i class="fa fa-times" style="font-size: 18px;color:#777"></i></button>
                                </div>
                                <div class="mh-info-customer">
                                    <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                                    <div class="name-info">
                                        <h4 class="i-name"></h4>
                                        <div class="phones i-phone"></div>
                                    </div>
                                    <div class="i-cusType">
                                        <span class="label label-success"></span>
                                    </div>
                                    <div class="total-orders">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>Số đơn hàng &nbsp;:&nbsp;&nbsp;<span class="i-total-orders"></span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-money" aria-hidden="true"></i><span>Số dư tài khoản :&nbsp;&nbsp;<span id="customerBalance">0</span> ₫</span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-question-circle" aria-hidden="true"></i><span>BH gần nhất &nbsp;:&nbsp;&nbsp;<span id="spanLastGuarantee"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default more-task" id="divGuaranteeInfo"<?php if($guarantee['GuaranteeStep'] != 4) echo 'style="display: none;"'; ?>>
                            <a href="javascript:void(0)" target="_blank"><i class="fa fa-bars "></i> Chính sách của công ty hiện tại</a>
                            <p>Tổng hợp xử lý</p>
                            <ul class="list-group"></ul>
                        </div>
                        <div class="box box-default classify padding20" id="divStore"<?php if($guarantee['GuaranteeStep'] > 2) echo 'style="display: none;"'; ?>>
                            <div class="form-group">
                                <label class="control-label light-blue">CS xử lý</label>
                                <?php $this->Mconstants->selectObject($listStores, 'StoreId', 'StoreName', 'StoreId', $guarantee['StoreId'], true, '--Chọn cơ sở--', ' hmdrequiredNumber', ' data-field="Cơ sở"'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label light-blue">Phương thức nhận hàng</label>
                                <?php $this->Mconstants->selectObject($listReceiptTypes, 'ReceiptTypeId', 'ReceiptTypeName', 'ReceiptTypeId', $guarantee['ReceiptTypeId'], true, '--Loại phương thức--'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label light-blue">Phương thức thanh toán</label>
                                <?php $this->Mconstants->selectObject($listPaymentTypes, 'PaymentTypeId', 'PaymentTypeName', 'PaymentTypeId', $guarantee['PaymentTypeId'], true, '--Chọn phương thức--'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label light-blue">Phí ship thực tế (nếu có)</label>
                                <input type="text" class="form-control cost text-right" id="shipCost" value="<?php echo priceFormat($guarantee['ShipCost']); ?>">
                            </div>
                        </div>
                        <div class="box box-default classify padding20" id="divCustomerComment"<?php if($guarantee['GuaranteeStep'] < 3) echo 'style="display: none;"'; ?>>
                            <label class="light-blue">Lịch sử trao đổi với KH</label>
                            <div class="box-transprt clearfix mb10">
                                <button type="button" class="btn-updaten save btnInsertComment" data-id="2">
                                    Lưu
                                </button>
                                <input type="text" class="add-text" id="comment_2" value="">
                            </div>
                            <div class="listComment" id="listComment_2">
                                <?php $i = 0;
                                $now = new DateTime(date('Y-m-d'));
                                foreach($listGuaranteeComments2 as $oc){
                                    $avatar = (empty($oc['Avatar']) ? NO_IMAGE : $oc['Avatar']);
                                    $i++;
                                    if($i < 3){ ?>
                                        <div class="box-customer mb10">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <th rowspan="2" valign="top" style="width: 50px;"><img src="<?php echo USER_PATH.$avatar; ?>" alt=""></th>
                                                    <th><a href="javascript:void(0)" class="name"><?php echo $oc['FullName']; ?></a></th>
                                                    <th class="time">
                                                        <?php $dayDiff = getDayDiff($oc['CrDateTime'], $now);
                                                        echo getDayDiffText($dayDiff).ddMMyyyy($oc['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <p class="pComment"><?php echo $oc['Comment']; ?></p>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php }
                                } ?>
                            </div>
                            <?php if(count($listGuaranteeComments2) > 2){ ?>
                                <div class="text-right light-dark">
                                    <a href="javascript:void(0)" class="aShowComment" data-id="2">Xem tất cả &gt;&gt;</a>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="box box-default classify padding20">
                            <label class="light-blue">Ghi chú nội bộ</label>
                            <div class="box-transprt clearfix mb10">
                                <button type="button" class="btn-updaten save btnInsertComment" data-id="1">
                                    Lưu
                                </button>
                                <input type="text" class="add-text" id="comment_1" value="">
                            </div>
                            <div class="listComment" id="listComment_1">
                                <?php $i = 0;
                                $now = new DateTime(date('Y-m-d'));
                                foreach($listGuaranteeComments1 as $oc){
                                    $avatar = (empty($oc['Avatar']) ? NO_IMAGE : $oc['Avatar']);
                                    $i++;
                                    if($i < 3){ ?>
                                        <div class="box-customer mb10">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <th rowspan="2" valign="top" style="width: 50px;"><img src="<?php echo USER_PATH.$avatar; ?>" alt=""></th>
                                                    <th><a href="javascript:void(0)" class="name"><?php echo $oc['FullName']; ?></a></th>
                                                    <th class="time">
                                                        <?php $dayDiff = getDayDiff($oc['CrDateTime'], $now);
                                                        echo getDayDiffText($dayDiff).ddMMyyyy($oc['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <p class="pComment"><?php echo $oc['Comment']; ?></p>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php }
                                } ?>
                            </div>
                            <?php if(count($listGuaranteeComments1) > 2){ ?>
                                <div class="text-right light-dark">
                                    <a href="javascript:void(0)" class="aShowComment" data-id="1">Xem tất cả &gt;&gt;</a>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="box box-default more-task">
                            <!--<a href="javascript:void(0)" class="task1 aRemind"><img src="assets/vendor/dist/img/icon07.png">Thêm task chăm sóc khách hàng</a>-->
                            <a href="javascript:void(0)" class="aRemind"><img src="assets/vendor/dist/img/icon08.png">Tạo chương trình nhắc nhở</a>
                        </div>
                        <div class="box box-default padding20" id="divSubmit">
                            <ul class="list-group text-center">
                                <li style="margin-bottom: 10px;"><button type="button" class="btn btn-primary" id="btnSaveGuarantee">Lưu thay đổi</button></li>
                                <li><button type="button" class="btn btn-primary" id="btnNextStep">Chuyển sang CSKH</button></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <input type="text" hidden="hidden" id="getCustomerDetailUrl" value="<?php echo base_url('api/customer/get'); ?>">
                <input type="text" hidden="hidden" id="getListCustomerUrl" value="<?php echo base_url('api/customer/getList'); ?>">
                <input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getListByCustomerGuarantee'); ?>">
                <input type="text" hidden="hidden" id="getListAccessoryUrl" value="<?php echo base_url('api/product/getListAccessories'); ?>">
                <input type="text" hidden="hidden" id="editCustomerUrl" value="<?php echo base_url('customer/edit'); ?>">
                <input type="text" hidden="hidden" id="editOrderUrl" value="<?php echo base_url('order/edit'); ?>">
                <input type="text" hidden="hidden" id="updateFieldUrl" value="<?php echo base_url('api/guarantee/updateField'); ?>">
                <input type="text" hidden="hidden" id="updateProductUrl" value="<?php echo base_url('api/guarantee/updateProduct'); ?>">
                <input type="text" hidden="hidden" id="insertGuaranteeCommentUrl" value="<?php echo base_url('api/guarantee/insertComment'); ?>">
                <input type="text" hidden="hidden" id="updateProductCommentUrl" value="<?php echo base_url('api/guarantee/updateProductComment'); ?>">
                <input type="text" hidden="hidden" id="guaranteeId" value="<?php echo $guaranteeId; ?>">
                <input type="text" hidden="hidden" id="customerId" value="<?php echo $guarantee['CustomerId']; ?>">
                <input type="text" hidden="hidden" id="guaranteeStep" value="<?php echo $guarantee['GuaranteeStep']; ?>">
                <input type="text" hidden="hidden" id="guaranteeStatusId" value="<?php echo $guarantee['GuaranteeStatusId']; ?>">
                <input type="text" hidden="hidden" id="currentGuaranteeStep" value="<?php echo $guarantee['GuaranteeStep']; ?>">
                <input type="text" hidden="hidden" id="productIndex" value="<?php echo count($listGuaranteeProducts); ?>">
                <input type="text" hidden="hidden" id="canEditAll" value="<?php echo $canEditAll ? 1 : 0 ?>">
                <input type="text" hidden="hidden" id="curentProductIndex" value="0">
                <input type="text" hidden="hidden" id="currentGuaranteeStep4" value="1">
                <?php echo $this->Mconstants->selectObject($listUsageStatus, 'UsageStatusId', 'UsageStatusName', 'UsageStatusIdOriginal', 0, false, '', '', ' style="display:none;"');
                echo $this->Mconstants->selectConstants('guaranteeSolutions', 'GuaranteeSolutionIdOriginal', 0, false, '', '', ' style="display:none;"'); ?>
                <?php echo form_close(); ?>
                <?php $this->load->view('includes/modal/remind'); ?>
                <?php $this->load->view('includes/modal/comment_type', array('listItemComments' => $listGuaranteeComments)); ?>
                <?php $this->load->view('guarantee/modal'); ?>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>