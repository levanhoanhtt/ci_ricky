<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl">
                    <li><a href="<?php echo base_url('guarantee/add'); ?>" class="btn btn-primary">Tạo bảo hành</a></li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả bảo hành</a></li>
                        <?php foreach($listFilters as $f){ ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="input-group margin ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả bảo hành theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="guarantee_store">Cơ sở xử lý</option>
                                        <option value="guarantee_status">Trạng thái</option>
                                        <option value="guarantee_customer">Khách hàng</option>
                                        <option value="guarantee_receipttypes">Phương thức nhận hàng</option>
                                        <option value="guarantee_paymenttypes">Phương thức thanh toán</option>
                                        <option value="guarantee_create">Thời gian tạo phiếu</option>
                                        <option value="guarantee_tag">Tag</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 guarantee_store none-display">
                                    <div class="text_opertor">ở</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group mb10 guarantee_status guarantee_customer guarantee_receipttypes guarantee_paymenttypes">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <select class="form-control guarantee_store">
                                        <?php foreach($listStores as $s) :?>
                                            <option value="<?php echo $s['StoreId']; ?>"><?php echo $s['StoreName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <select class="form-control guarantee_status none-display">
                                        <?php foreach($this->Mconstants->guaranteeStatus as $i => $v) :?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <select class="form-control guarantee_customer none-display">
                                        <?php foreach($listCustomers as $s) :?>
                                            <option value="<?php echo $s['CustomerId']; ?>"><?php echo $s['FullName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <select class="form-control guarantee_receipttypes none-display">
                                        <?php foreach($listReceiptTypes as $s) :?>
                                            <option value="<?php echo $s['ReceiptTypeId']; ?>"><?php echo $s['ReceiptTypeName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <select class="form-control guarantee_paymenttypes none-display">
                                        <?php foreach($listPaymentTypes as $s) :?>
                                            <option value="<?php echo $s['PaymentTypeId']; ?>"><?php echo $s['PaymentTypeName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <select class="form-control guarantee_create none-display" id="select_operator_date">
                                        <option value="between">trong khoảng</option>
                                        <option value="<">trước</option>
                                        <option value="=">bằng</option>
                                        <option value=">">sau</option>
                                    </select>
                                    <select class="form-control guarantee_tag none-display mb10">
                                        <option value="in">chứa</option>
                                        <option value="not in">không chứa</option>
                                    </select>
                                    <div class="guarantee_tag none-display">
                                        <select class="form-control select2 guarantee_tag ">
                                            <?php foreach($listTags as $key => $v){ ?>
                                                <option value="<?php echo $v['TagId']; ?>"><?php echo $v['TagName']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group block-display mb10">
                                    <input class="form-control datepicker guarantee_create none-display" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart">
                                    <input class="form-control datepicker guarantee_create none-display" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd">
                                </div>
                                <div class="form-group block-display widthauto">
                                    <button id="btn-filter" data-href="<?php echo base_url('api/guarantee/searchByFilter'); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-sm-3 fix-width-200">
                                <select class="form-control" id="selectAction" style="display: none;">
                                    <option value="">Chọn hành động</option>
                                    <!-- <option value="add_tags">Thêm nhãn</option>
                                    <option value="delete_tags">Bỏ nhãn</option> -->
                                </select>
                            </div>
                            <div class="col-sm-2 fix-width-200">
                                <select class="form-control" id="selectData" style="display: none;"></select>
                            </div>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Mã bảo hành</th>
                                <th>Ngày tạo</th>
                                <th>Khách hàng</th>
                                <th>CS xử lý</th>
                                <th class="text-center">Trạng thái xử lý</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyGuarantee"></tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="itemTypeId" value="27">
                    <input type="hidden" value="<?php echo base_url('customer/edit')?>" id="urlEditCustomer">
                    <input type="hidden" value="<?php echo base_url('guarantee/edit')?>" id="urlEditGuarantee">
                    <?php $this->load->view('includes/modal/filter'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>