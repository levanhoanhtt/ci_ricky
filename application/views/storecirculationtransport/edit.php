<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content-header no-pd-lr">
            <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            <ul class="list-inline">
                <li><a id="aTransportList" href="<?php echo base_url('storecirculationtransport'); ?>" class="btn btn-default">Đóng</a></li>
                <?php if($storeCirculationTransportId > 0){ ?><li><a href="<?php echo base_url('storecirculationtransport/printPdf/'.$storeCirculationTransportId); ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> In</a></li><?php } ?>
            </ul>
        </section>
        <section class="content new-box-stl ft-seogeo section-transport">
            <?php $this->load->view('includes/notice'); ?>
            <?php if($storeCirculationTransportId > 0){ ?>
            <?php if($storeCirculationTransport['TransportStatusId'] == 5){ ?>
                <div class="alert alert-danger" style="margin-left: -15px;margin-bottom: 15px;">
                    <p>
                        <i class="fa fa-exclamation"></i> Phiếu giao hàng đã bị hủy lúc <?php echo ddMMyyyy($storeCirculationTransport['UpdateDateTime'], 'H:i d/m/Y'); ?>.
                        <?php $cancelReasonName = '';
                        if($storeCirculationTransport['CancelReasonId'] > 0) $cancelReasonName = $this->Mconstants->getObjectValue($listCancelReasons, 'CancelReasonId', $storeCirculationTransport['CancelReasonId'], 'CancelReasonName');
                        if(!empty($cancelReasonName)){
                            echo ' Lý do: '.$cancelReasonName;
                            if(!empty($storeCirculationTransport['CancelComment'])) echo ' ('.$storeCirculationTransport['CancelComment'].')';
                        }
                        elseif(!empty($storeCirculationTransport['CancelComment'])) echo ' Lý do: '.$storeCirculationTransport['CancelComment']; ?>
                    </p>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-sm-8 no-padding ">
                    <div class="box box-default padding20 same">
                        <div class="box-step has-slider" >
                            <ul class="clearfix bxslider">
                                <?php foreach($this->Mconstants->transportStatus as $i => $v){ ?>
                                    <li class="liTransportStatus<?php if($storeCirculationTransport['TransportStatusId'] == $i) echo ' active'; ?>" id="liTransportStatus_<?php echo $i; ?>">
                                        <a href="javascript:void(0);"><?php echo $v; ?> <div class="icon"><img src="assets/vendor/dist/img//transport/<?php echo $i.'.'; if($storeCirculationTransport['TransportStatusId'] == $i) echo 2; else echo 1; ?>.png" alt="<?php echo $v; ?>"></div></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="box box-default padding15">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover stl2">
                                        <thead class="theadNormal">
                                            <tr>
                                                <th>Thông tin Sản phẩm</th>
                                                <th class="text-right" style="width: 134px;">Đơn giá * SL</th>
                                                <th class="text-right" style="width: 134px;">Thành tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbodyProduct">
                                            <?php $products = array();
                                            $productChilds = array();
                                            foreach($listStoreCirculationProducts as $op){
                                                if(!isset($products[$op['ProductId']])) $products[$op['ProductId']] = $this->Mproducts->get($op['ProductId'], true, '', 'ProductName, ProductImage, ProductKindId, Price, BarCode');
                                                $productName = $products[$op['ProductId']]['ProductName'];
                                                $productImage = $products[$op['ProductId']]['ProductImage'];
                                                $barCode = $products[$op['ProductId']]['BarCode'];
                                                $productChildName = '';
                                                $price = $products[$op['ProductId']]['Price'];
                                                if($op['ProductChildId'] > 0){
                                                    if(!isset($productChilds[$op['ProductChildId']])) $productChilds[$op['ProductChildId']] = $this->Mproductchilds->get($op['ProductChildId'], true, '', 'ProductName, ProductImage, Price, BarCode');
                                                    $productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
                                                    $price = $productChilds[$op['ProductChildId']]['Price'];
                                                    $productImage = $productChilds[$op['ProductChildId']]['ProductImage'];
                                                    $barCode = $productChilds[$op['ProductChildId']]['BarCode'];
                                                }
                                                if(empty($productImage)) $productImage = NO_IMAGE;
                                            ?>
                                            <tr data-id="<?php echo $op['ProductId']; ?>" data-child="<?php echo $op['ProductChildId']; ?>">
                                                <td>
                                                    <img src="<?php echo PRODUCT_PATH.$productImage; ?>" class="productImg">
                                                    <a href="<?php echo base_url('product/edit/'.$op['ProductId']); ?>" class="light-dark" target="_blank">
                                                        <?php echo $productName;
                                                        if(!empty($productChildName)) echo ' | '.$productChildName; ?>
                                                    </a>
                                                    <br />
                                                    SKU: <?php echo $barCode; ?>
                                                </td>
                                                <td class="text-right"><?php echo priceFormat($price). ' x '.priceFormat($op['Quantity']); ?></td>
                                                <td class="text-right"><?php echo priceFormat($op['Quantity'] * $price); ?> đ</td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group stl2">
                                    <label class="control-label light-blue">Nhắn nhủ giao hàng</label>
                                    <input type="text" class="form-control" disabled value="<?php echo $storeCirculationTransport['Comment']; ?>">
                                </div>
                               
                            </div>
                        </div>
                    </div>
                    <div class="box box-default padding15">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="light-blue">Ghi chú</label>
                                <div class="box-transprt clearfix mb10">
                                    <button class="btn-updaten save" type="button" id="btnInsertComment">Lưu</button>
                                    <input type="text" class="add-text" id="comment">
                                </div>
                                <div id="listComment">
                                    <?php $i = 0;
                                    $now = new DateTime(date('Y-m-d'));
                                    foreach($listStoreCirculationTransportComments as $oc){
                                        $i++;
                                        if($i < 3){ ?>
                                            <div class="box-customer mb10">
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <th rowspan="2" valign="top" style="width: 50px;"><img src="assets/vendor/dist/img/users2.png" alt=""></th>
                                                        <th><a href="javascript:void(0)" class="name"><?php echo $oc['FullName']; ?></a></th>
                                                        <th class="time">
                                                            <?php $dayDiff = getDayDiff($oc['CrDateTime'], $now);
                                                            echo getDayDiffText($dayDiff).ddMMyyyy($oc['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <p class="pComment"><?php echo $oc['Comment']; ?></p>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php }
                                    } ?>
                                </div>
                                <?php if(count($listStoreCirculationTransportComments) > 2){ ?>
                                    <div class="text-right light-dark">
                                        <a href="javascript:void(0)" id="aShowComment">Xem tất cả &gt;&gt;</a>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-sm-6"></div>
                        </div>
                    </div>
                    <?php $this->load->view('includes/action_logs_new'); ?>
                </div>
               <div class="col-sm-4">
                    <div class="box box-primary">
                        <h3 class="box-title">
                            <span class="dropdown-toggle" style="cursor: pointer;" type="button" data-toggle="dropdown">
                                <span class="light-blue pos-re" style="color: #fff;">CS gửi hàng : <?php echo $storeCirculation['StoreSourceId'] > 0 ? $this->Mconstants->getObjectValue($listStores, 'StoreId', $storeCirculation['StoreSourceId'], 'StoreName') : '' ?></span><br><br>
                                <span class="light-blue pos-re" style="color: #fff;">CS nhận hàng : <?php echo $storeCirculation['StoreDestinationId'] > 0 ? $this->Mconstants->getObjectValue($listStores, 'StoreId', $storeCirculation['StoreDestinationId'], 'StoreName') : '' ?></span>
                            </span>
                        </h3>

                        <?php if($storeCirculationTransport['TransportStatusId'] != 4 && $storeCirculationTransport['TransportStatusId'] != 5 && $storeCirculationTransport['TransportStatusId'] != 7){ ?>
                            <p><a href="javascript:void(0)" id="aTransportStatus"><i class="fa fa-truck fa-flip-horizontal"></i> Cập nhật trạng thái giao hàng LCK</a></p>
                        <?php } ?>
                    </div>
                    <div class="box box-default">
                        <table class="tbl-cod">
                            <tr>
                                <th>Trạng thái giao hàng</th>
                                <td class="text-right" id="tdTransportStatus">
                                    <?php if($storeCirculationTransport['TransportStatusId'] > 0){ ?>
                                        <span class="<?php echo $this->Mtransports->labelCss['TransportStatusCss'][$storeCirculationTransport['TransportStatusId']]; ?>">
                                            <?php echo $this->Mconstants->transportStatus[$storeCirculationTransport['TransportStatusId']];
                                            if($storeCirculationTransport['PendingStatusId'] > 0) echo ' ('.$this->Mconstants->getObjectValue($listPendingStatus, 'PendingStatusId', $storeCirculationTransport['PendingStatusId'], 'PendingStatusName').')'; ?>
                                        </span>
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Mã vận đơn</th>
                                <td class="text-right"><a href="javascript:void(0)" id="aTracking" class="light-dark"><?php echo empty($storeCirculationTransport['Tracking']) ? 'Cập nhật' : $storeCirculationTransport['Tracking']; ?></a></td>
                            </tr>
                            <tr>
                                <th>Phí ship thực tế</th>
                                <td class="text-right"><a href="javascript:void(0)" id="aShipCost" class="light-dark"><?php echo $storeCirculationTransport['ShipCost'] > 0 ? '<span class="spanShipCost">'.priceFormat($storeCirculationTransport['ShipCost']).'</span> ₫' : '<span class="spanShipCost">Cập nhật</span>'; ?></a></td>
                            </tr>
                            <tr>
                                <th>Nhà vận chuyển</th>
                                <td class="text-right">
                                    <a href="javascript:void(0)" id="aTransporter" class="light-dark">
                                        <?php $value = $this->Mconstants->getObjectValue($listTransporters, 'TransporterId', $storeCirculationTransport['TransporterId'], 'TransporterName');
                                        echo empty($value) ? 'Cập nhật' : $value; ?>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="box box-default ">
                        <table class="tbl-cod">
                            <tr>
                                <th>Mã giao hàng LCK</th>
                                <td class="mid"><?php echo ddMMyyyy($storeCirculationTransport['CrDateTime'], 'H:i | d/m/Y'); ?></td>
                                <td class="text-right"><a href="javascript:void(0)" class="light-dark" id="aTransportCode"><?php echo $storeCirculationTransport['TransportCode']; ?></a></td>
                            </tr>
                            <tr>
                                <th>Mã lưu chuyển kho</th>
                                <td class="mid"><?php echo ddMMyyyy($storeCirculation['CrDateTime'], 'H:i | d/m/Y'); ?></td>
                                <td class="text-right"><a href="<?php echo base_url('storecirculation/edit/'.$storeCirculation['StoreCirculationId']); ?>" target="_blank" class="light-dark"><?php echo $storeCirculation['StoreCirculationCode']; ?></a></td>
                            </tr>
                        </table>
                    </div>
                     <div class="box box-default mh-wrap-customer" id="divCustomer">
                        <div class="with-border">
                            <div class="mh-info-customer">
                                <div class="clearfix">
                                    <img class="avatar-user" src="assets/vendor/dist/img/users2.png">
                                    <div class="name-info">
                                        <h4 class="i-name"><?php echo $customerAddress ? $customerAddress['CustomerName'] : ''; ?></h4>
                                        <div class="phones i-phone"><?php echo $customerAddress ? $customerAddress['PhoneNumber'] : ''; ?></div>
                                    </div>
                                </div>
                                <!--<div class="i-cusType"><span class="label label-success"><?php //echo $this->Mcustomergroups->getFieldValue(array('CustomerGroupId' => $customer['CustomerGroupId']), 'CustomerGroupName'); ?></span></div>-->
                            </div>
                        </div>
                        <div class="box-body">
                            <div>
                                <h4 class="mgbt-20 light-blue">Thông tin giao hàng &nbsp;&nbsp;
                                <!-- <a href="javascript:void(0)" id="aCustomerAddress"><i class="fa fa-pencil"></i></a> -->
                            </h4>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="item">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        <span class="i-name"><?php echo $customerAddress['CustomerName']; ?></span>
                                    </div>
                                    <div class="item">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <span class="i-phone"><?php echo $customerAddress['PhoneNumber']; ?></span>
                                    </div>
                                    <div class="item">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <span class="i-email"><?php echo $customerAddress['Email']; ?></span>
                                    </div>
                                    <div class="item i-address">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span class="i-ward"><spam class="spanAddress"><?php echo $customerAddress['Address'] . '</span> ' . $customerAddress['WardName']; ?></span>
                                        <span class="br-line i-district"><?php echo $customerAddress['DistrictName']; ?></span>
                                        <span class="br-line i-province"><?php echo $customerAddress['ProvinceName']; ?></span>
                                    </div>
                                    <div class="item">
                                        <i class="fa fa-id-card" aria-hidden="true"></i>
                                        <span class="i-country" data-id="<?php echo $customerAddress['CountryId']; ?>" data-province="<?php echo $customerAddress['ProvinceId']; ?>" data-district="<?php echo $customerAddress['DistrictId']; ?>" data-ward="<?php echo $customerAddress['WardId']; ?>" data-zip="<?php echo $customerAddress['ZipCode']; ?>">
                                            Việt Nam
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-default more-task">
                        <!--<a href="javascript:void(0)" class="task1 aRemind"><img src="assets/vendor/dist/img/icon07.png">Thêm task chăm sóc khách hàng</a>-->
                        <a href="javascript:void(0)" class="aRemind"><img src="assets/vendor/dist/img/icon08.png">Tạo chương trình nhắc nhở</a>
                    </div>
                    <div class="box box-default more-tabs padding20">
                        <div class="form-group">
                            <label class="control-label light-blue" style="width: 100%;line-height: 28px;">Nhãn (cách nhau bởi dấu phẩy) <button class="btn-updaten save btn-sm pull-right" type="button" id="btnUpdateTag">Lưu</button></label>
                            <input type="text" class="form-control" id="tags">
                        </div>
                        <p class="light-gray">Bạn có thể chọn những tag đã được sử dụng</p>
                        <div class="clearfix">
                            <?php foreach($listTags as $t){ ?>
                                <a href="javascript:void(0)" class="ntags"><?php echo $t['TagName']; ?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <input type="text" hidden="hidden" id="storeCirculationTransportId" value="<?php echo $storeCirculationTransportId; ?>">
            <input type="text" hidden="hidden" id="storeCirculationId" value="<?php echo $storeCirculation['StoreCirculationId']; ?>">
            <input type="text" hidden="hidden" id="storeSourceId" value="<?php echo $storeCirculation['StoreSourceId']; ?>">
            <input type="text" hidden="hidden" id="storeDestinationId" value="<?php echo $storeCirculation['StoreDestinationId']; ?>">
            <input type="text" hidden="hidden" id="transportCode" value="<?php echo $storeCirculationTransport['TransportCode']; ?>">
            <input type="text" hidden="hidden" id="transportStatusIdOriginal" value="<?php echo $storeCirculationTransport['TransportStatusId']; ?>">
            <input type="text" hidden="hidden" id="pendingStatusIdOriginal" value="<?php echo $storeCirculationTransport['PendingStatusId']; ?>">
            <input type="text" hidden="hidden" id="updateFieldUrl" value="<?php echo base_url('storecirculationtransport/updateField'); ?>">
            <input type="text" hidden="hidden" id="transporterIdOriginal" value="<?php echo $storeCirculationTransport['TransporterId']; ?>">
            <input type="text" hidden="hidden" id="updateItemTagUrl" value="<?php echo base_url('api/tag/updateItem'); ?>">
            <input type="text" hidden="hidden" id="insertTransportCommentUrl" value="<?php echo base_url('storecirculationtransport/insertComment'); ?>">
            <?php foreach($tagNames as $tagName){ ?>
                <input type="text" hidden="hidden" class="tagName" value="<?php echo $tagName; ?>">
            <?php } ?>
            <?php $this->load->view('includes/modal/comment', array('itemName' => 'LCK', 'listItemComments' => $listStoreCirculationTransportComments)); ?>
            <?php $this->load->view('includes/modal/remind'); ?>
            <div class="modal fade" id="modalTracking" tabindex="-1" role="dialog" aria-labelledby="modalTracking">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Cập nhật mã vận đơn</h4>
                        </div>
                        <div class="modal-body">
                            <p class="light-blue">Mã vận đơn</p>
                            <div class="form-group">
                                <input type="text" class="form-control" id="tracking" placeholder="Mã vận đơn">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" id="btnUpdateTracking">Hoàn thành</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalShipCost" tabindex="-1" role="dialog" aria-labelledby="modalShipCost">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Cập nhật phí giao hàng</h4>
                        </div>
                        <div class="modal-body">
                            <p class="light-blue">Phí ship thực tế</p>
                            <div class="form-group">
                                <input type="text" class="form-control cost" id="shipCost" value="0">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" id="btnUpdateShipCost">Hoàn thành</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalTransporter" tabindex="-1" role="dialog" aria-labelledby="modalTransporter">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Cập nhật nhà vận chuyển</h4>
                        </div>
                        <div class="modal-body">
                            <p class="light-blue">Nhà vận chuyển</p>
                            <div class="form-group">
                                <?php $this->Mconstants->selectObject($listTransporters, 'TransporterId', 'TransporterName', 'TransporterId', $storeCirculationTransport['TransporterId']); ?>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" id="btnUpdateTransporter">Hoàn thành</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalTransportStatus" tabindex="-1" role="dialog" aria-labelledby="modalTransportStatus">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="box-title"><i class="fa fa-truck" aria-hidden="true"></i> Cập nhật trạng thái vận chuyển</h4>
                        </div>
                        <div class="modal-body">
                            <p class="light-blue">Trạng thái vận chuyển</p>
                            <div class="form-group">
                                <?php $this->Mconstants->selectConstants('transportStatus', 'TransportStatusId', $storeCirculationTransport['TransportStatusId']); ?>
                            </div>
                            <div id="divCancelReason" style="display: none;">
                                <div class="form-group">
                                    <label class="control-label">Lý do hủy:</label>
                                    <?php $this->Mconstants->selectObject($listCancelReasons, 'CancelReasonId', 'CancelReasonName', 'CancelReasonId', 0, true, '--Chọn lý do--'); ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Ghi chú:</label>
                                    <input class="form-control" type="text" id="cancelComment" value="">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" id="btnUpdateTransportStatus">Hoàn thành</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalUpdatePending" tabindex="-1" role="dialog" aria-labelledby="modalUpdatePending">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="box-title"><i class="fa fa-wrench"></i> Cập nhật trạng thái chờ xử lý</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="radio-group">
                                    <?php foreach($listPendingStatus as $ps){ ?>
                                        <p class="item"><input type="radio" name="PendingStatusId" class="iCheck" value="<?php echo $ps['PendingStatusId']; ?>"<?php if($storeCirculationTransport['PendingStatusId'] == $ps['PendingStatusId']) echo ' checked'; ?>> <?php echo $ps['PendingStatusName']; ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" id="btnUpdatePending">Cập nhật</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>