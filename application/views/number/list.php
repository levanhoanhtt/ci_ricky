<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>TargetId</th>
                                <th>Phone</th>
                                <th>Domain</th>
                                <th>TargetType</th>
                                <th>CallbackUrl</th>
                                <th>State</th>
                                <th>CreatedAt</th>
                                <th>UpdatedAt</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyExt">
                            <?php if(!empty($numberList)) { foreach ($numberList as $number) { ?>
                                <tr>
                                    <td><?=$number['NumberId']; ?></td>
                                    <td><?=$number['TargetId']; ?></td>
                                    <td><?=$number['Phone']; ?></td>
                                    <td><?=$number['Domain']; ?></td>
                                    <td><?=$number['TargetType']; ?></td>
                                    <td><?=$number['CallbackUrl']; ?></td>
                                    <td><?=$number['State']; ?></td>
                                    <td><?=$number['CreatedAt']; ?></td>
                                    <td><?=$number['UpdatedAt']; ?></td>
                                    <td><a href="<?php echo base_url('number/edit/'.$number['NumberId'].''); ?>" class="btn btn-info">Sửa</a></td>
                                </tr>
                            <?php } }  ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="itemTypeId" value="1">
                    <input type="text" hidden="hidden" id="editDepartmentUrl" value="<?php echo base_url('number/edit'); ?>">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>