<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <?php echo form_open('', array('id' => 'number')); ?>
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button type="submit" class="btn btn-primary submit">Cập nhật</button></li>
                    <li><a href="<?php echo base_url('number'); ?>" id="departmentsUrl" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php if(!empty($numberDetail)) { ?>
                <div class="box box-default">
                    <div class="box-body">
                        <div class="row">
                            <input type="hidden" value="<?= $numberDetail['Phone'] ?>" name="Phone">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label normal">Target Type<span class="required">*</span></label>
                                    <select class="form-control" name="targetType" id="targetType">
                                        <?php if(isset($targetTypes) && !empty($targetTypes)) { foreach ($targetTypes as $key=>$val) { ?>
                                            <option <?= $numberDetail['TargetType'] == $val ? 'selected' : ''; ?> value="<?= $val ?>"><?= $val ?></option>
                                        <?php } }?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label normal">Target Id</label>
                                    <select class="form-control" name="targetId" id="targetId">

                                    </select>
                                    <input id="targetIdValue" type="hidden" value="<?=$numberDetail['TargetId'] ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }  else { ?>
                    <div class="box box-default">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label normal">Target Type<span class="required">*</span></label>
                                        <input value="<?= $this->input->post('targetType') ? $this->input->post('targetId') : '' ?>" type="text"
                                               required name="targetType" class="form-control hmdrequired" value="" data-field="Target Type">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label normal">Target Id</label>
                                        <input type="number" value="<?= $this->input->post('targetId') ? $this->input->post('targetId') : '' ?>"
                                               required name="targetId" class="form-control hmdrequired" value="" data-field="Target Id">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </section>
        </div>
        <input type="hidden" id="apiTargetIdList" value="<?php echo base_url('api/taget/listBy'); ?>" />
        <?php echo form_close(); ?>
    </div>
<?php $this->load->view('includes/footer'); ?>