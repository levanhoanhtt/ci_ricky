<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content-header">
            <h1><?php echo $title; ?></h1>
            <ul class="list-inline">
                <?php if($contributorId > 0){ ?><li><button class="btn btn-primary submit">Lưu</button></li><?php } ?>
                <li><a href="<?php echo base_url('contributor'); ?>" class="btn btn-default">Đóng</a></li>
            </ul>
        </section>
        <section class="content">
            <?php $this->load->view('includes/notice'); ?>
            <?php if($contributorId > 0){ ?>
                <?php echo form_open('contributor/update', array('id' => 'contributorForm')); ?>
                <div class="box box-default padding15">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thông tin cổ đông</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="form-label normal">Tên cổ đông <span class="required">*</span></label>
                                    <input type="text" name="ContributorName" class="form-control hmdrequired" value="<?php echo $contributor['ContributorName']; ?>" data-field="Tên cổ đông">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="form-label normal">Điện thoại <span class="required">*</span></label>
                                    <input type="text" name="ContributorPhone" class="form-control hmdrequired" value="<?php echo $contributor['ContributorPhone']; ?>" data-field="Điện thoại">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="form-label normal">Ngày sinh <span class="required">*</span></label>
                                    <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                                        <input type="text" class="form-control hmdrequired datepicker" name="BirthDay" value="<?php echo ddMMyyyy($contributor['BirthDay']); ?>" autocomplete="off" data-field="Ngày sinh">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label normal">Trạng thái</label>
                                    <?php $this->Mconstants->selectConstants('itemStatus', 'ItemStatusId', $contributor['ItemStatusId']); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label normal">Tỉnh/ Thành phố</label>
                                    <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', $contributor['ProvinceId'], false, '', ' select2'); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label normal">Quận huyện</label>
                                    <?php echo $this->Mdistricts->selectHtml($contributor['DistrictId']); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Phường xã</label>
                                    <?php echo $this->Mwards->selectHtml($contributor['WardId']); ?>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label class="control-label normal">Địa chỉ</label>
                                    <input type="text" name="Address" class="form-control hmdrequired" value="<?php echo $contributor['Address']; ?>" data-field="Địa chỉ">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-default padding15">
                    <div class="box-header with-border">
                        <h3 class="box-title">Các lần đóng góp</h3>
                    </div>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead class="theadNormal">
                            <tr>
                                <th>Mảng kinh doanh</th>
                                <th>Số tiền</th>
                                <th>Ngày đóng góp</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyContributor">
                            <?php $i = 0;
                            foreach($listContributorProductTypes as $cpt){
                                $i++; ?>
                                <tr id="contributor_<?php echo $cpt['ContributorProductTypeId']; ?>">
                                    <td id="productTypeName_<?php echo $cpt['ContributorProductTypeId']; ?>"><?php echo $this->Mconstants->getObjectValue($listProductTypes, 'ProductTypeId', $cpt['ProductTypeId'], 'ProductTypeName'); ?></td>
                                    <td id="paidVN_<?php echo $cpt['ContributorProductTypeId']; ?>"><?php echo priceFormat($cpt['PaidVN']); ?></td>
                                    <td id="paidDateTime_<?php echo $cpt['ContributorProductTypeId']; ?>"><?php echo ddMMyyyy($cpt['PaidDateTime']); ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $cpt['ContributorProductTypeId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $cpt['ContributorProductTypeId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="productTypeId_<?php echo $cpt['ContributorProductTypeId']; ?>" value="<?php echo $cpt['ProductTypeId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><?php echo $this->Mconstants->selectObject($listProductTypes, 'ProductTypeId', 'ProductTypeName', 'ProductTypeId', 0, false, '', ' select2'); ?></td>
                                <td><input type="text" class="form-control hmdrequiredNumber" id="paidVN" value="0" data-field="Số tiền"></td>
                                <td><input type="text" class="form-control hmdrequired datepicker" id="paidDateTime" value="" autocomplete="off" data-field="Ngày đóng góp"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" id="contributorProductTypeId" value="0" hidden="hidden">
                                    <input type="text" id="updateContributeUrl" value="<?php echo base_url('contributor/updateContribute'); ?>" hidden="hidden">
                                    <input type="text" id="deleteContributeUrl" value="<?php echo base_url('contributor/deleteContribute'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
                    <li><a href="<?php echo base_url('contributor'); ?>" id="contributorListUrl" class="btn btn-default">Đóng</a></li>
                    <input type="text" id="contributorId" name="ContributorId" hidden="hidden" value="<?php echo $contributorId; ?>">
                </ul>
                <?php echo form_close(); ?>
            <?php } ?>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>
