<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content-header">
            <h1><?php echo $title; ?></h1>
            <ul class="list-inline">
                <li><button class="btn btn-primary submit">Lưu</button></li>
                <li><a href="<?php echo base_url('contributor'); ?>" class="btn btn-default">Đóng</a></li>
            </ul>
        </section>
        <section class="content">
            <?php echo form_open('contributor/update', array('id' => 'contributorForm')); ?>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="form-label normal">Tên cổ đông <span class="required">*</span></label>
                        <input type="text" name="ContributorName" class="form-control hmdrequired" value="" data-field="Tên cổ đông">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="form-label normal">Điện thoại <span class="required">*</span></label>
                        <input type="text" name="ContributorPhone" class="form-control hmdrequired" value="" data-field="Điện thoại">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="form-label normal">Ngày sinh <span class="required">*</span></label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input type="text" class="form-control hmdrequired datepicker" name="BirthDay" autocomplete="off" data-field="Ngày sinh">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label normal">Trạng thái</label>
                        <?php $this->Mconstants->selectConstants('itemStatus', 'ItemStatusId'); ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label normal">Tỉnh/ Thành phố</label>
                        <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', 0, false, '', ' select2'); ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label normal">Quận huyện</label>
                        <?php echo $this->Mdistricts->selectHtml(); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Phường xã</label>
                        <?php echo $this->Mwards->selectHtml(); ?>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <label class="control-label normal">Địa chỉ</label>
                        <input type="text" name="Address" class="form-control hmdrequired" value="" data-field="Địa chỉ">
                    </div>
                </div>
            </div>
            <ul class="list-inline pull-right margin-right-10">
                <li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
                <li><a href="<?php echo base_url('contributor'); ?>" id="contributorListUrl" class="btn btn-default">Đóng</a></li>
                <input type="text" id="contributorId" name="ContributorId" hidden="hidden" value="0">
            </ul>
            <?php echo form_close(); ?>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>
