<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button type="button" class="btn btn-primary submit">Cập nhật</button></li>
                    <li><a href="<?php echo base_url('transactionbusiness/'.$transactionTypeId); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($transactionTypeId > 0){ ?>
                <div class="box box-default">
                    <div class="box-body row-margin">
                        <?php //echo form_open('transactionbusiness/edit/'.$transactionBusinessId); ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <select class="form-control" id="transactionKindIdSearch1" name="TransactionKindId1">
                                    <option value="0">--Loại <?php echo $transactionTypeId == 1 ? 'thu' : 'chi'; ?> cha--</option>
                                    <?php foreach($listTransactionKinds as $tk){
                                        if($tk['ParentTransactionKindId'] == 0){ ?>
                                            <option value="<?php echo $tk['TransactionKindId']; ?>"><?php echo $tk['TransactionKindName']; ?></option>
                                        <?php } } ?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control" id="transactionKindIdSearch2" name="TransactionKindId2">
                                    <option value="0">--Loại <?php echo $transactionTypeId == 1 ? 'thu' : 'chi'; ?>--</option>
                                    <?php foreach($listTransactionKinds as $tk){
                                        if($tk['ParentTransactionKindId'] > 0){ ?>
                                            <option value="<?php echo $tk['TransactionKindId']; ?>" data-id="<?php echo $tk['ParentTransactionKindId']; ?>" style="display: none;"><?php echo $tk['TransactionKindName']; ?></option>
                                        <?php } } ?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <!--<input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">-->
                                <button type="button" class="btn btn-primary" id="btnSearch">Tìm kiếm</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <p>Tổng <?php echo $transactionTypeId == 1 ? 'thu' : 'chi'; ?> trong tháng: <span class="label label-primary" id="spanSumCost" style="font-size: 100%;">0</span></p>
                            </div>
                            <div class="col-sm-6">
                                <p>Trung bình trên ngày: <span class="label label-primary" id="spanDayCost" style="font-size: 100%;">0</span></p>
                            </div>
                        </div>
                        <?php //echo form_close(); ?>
                    </div>
                </div>
                <?php } ?>
            </section>
            <?php if($transactionTypeId > 0){ ?>
            <section class="content upn ft-seogeo">
                <div class="box-body divTable">
                    <div class="table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center" colspan="2">Tên loại chi phí</th>
                                <th rowspan="2" class="text-right" style="vertical-align: middle;width: 130px;">Số tiền</th>
                                <th rowspan="2" style="vertical-align: middle;">Ghi chú</th>
                            </tr>
                            <tr>
                                <th>Nhóm mẹ</th>
                                <th>Tên chi phí</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTransactionBusiness">
                            <?php $sumCost = 0;
                            foreach($listTransactionKinds as $tk) {
                                if($tk['ParentTransactionKindId'] == 0){
                                    $flag = false;
                                    foreach($listTransactionKinds as $tk1){
                                        if($tk1['ParentTransactionKindId'] == $tk['TransactionKindId']){
                                            $flag = true;
                                            $item = false;
                                            foreach($listTransactionBusinessItems as $tbi){
                                                if($tbi['TransactionKindId'] == $tk1['TransactionKindId']){
                                                    $item = $tbi;
                                                    $sumCost += $tbi['PaidCost'];
                                                    break;
                                                }
                                            } ?>
                                            <tr data-id="<?php echo $item ? $item['TransactionBusinessItemId'] : 0; ?>" id="trItem_<?php echo $tk['TransactionKindId']; ?>_<?php echo $tk1['TransactionKindId']; ?>" class="trItem_<?php echo $tk['TransactionKindId']; ?>">
                                                <td><?php echo $tk['TransactionKindName']; ?></td>
                                                <td><?php echo $tk1['TransactionKindName']; ?></td>
                                                <td><input type="text" class="form-control text-right paidCost" value="<?php echo $item ? priceFormat($item['PaidCost']) : 0; ?>"></td>
                                                <td>
                                                    <input type="text" class="form-control comment" value="<?php echo $item ? $item['Comment'] : ''; ?>">
                                                    <input type="text" hidden="hidden" class="transactionKindId" value="<?php echo $tk1['TransactionKindId']; ?>">
                                                </td>
                                            </tr>
                                        <?php }
                                    }
                                    if(!$flag){
                                        $item = false;
                                        foreach($listTransactionBusinessItems as $tbi){
                                            if($tbi['TransactionKindId'] == $tk['TransactionKindId']){
                                                $item = $tbi;
                                                $sumCost += $tbi['PaidCost'];
                                                break;
                                            }
                                        } ?>
                                        <tr data-id="<?php echo $item ? $item['TransactionBusinessItemId'] : 0; ?>" id="trItem_<?php echo $tk['TransactionKindId']; ?>_0" class="trItem_<?php echo $tk['TransactionKindId']; ?>">
                                            <td><?php echo $tk['TransactionKindName']; ?></td>
                                            <td></td>
                                            <td><input type="text" class="form-control text-right paidCost" value="<?php echo $item ? priceFormat($item['PaidCost']) : 0; ?>"></td>
                                            <td>
                                                <input type="text" class="form-control comment" value="<?php echo $item ? $item['Comment'] : ''; ?>">
                                                <input type="text" hidden="hidden" class="transactionKindId" value="<?php echo $tk['TransactionKindId']; ?>">
                                            </td>
                                        </tr>
                                    <?php }
                                }
                            } ?>
                            </tbody>
                        </table>
                    </div>
                    <ul class="list-inline pull-right margin-right-10">
                        <li><button type="button" class="btn btn-primary submit">Cập nhật</button></li>
                        <li><a href="<?php echo base_url('transactionbusiness/'.$transactionTypeId); ?>" class="btn btn-default">Đóng</a></li>
                        <input type="text" hidden="hidden" id="transactionBusinessId" value="<?php echo $transactionBusinessId; ?>">
                        <input type="text" hidden="hidden" id="sumCost" value="<?php echo $sumCost; ?>">
                        <input type="text" hidden="hidden" id="updateTransactionBusinessUrl" value="<?php echo base_url('transactionbusiness/update'); ?>">
                    </ul>
                </div>
            </section>
            <?php } ?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>