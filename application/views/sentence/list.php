<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/notice'); ?>
            <?php if($librarySentenceId > 0){ ?>
        	<section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl">
                    <li><button type="button" class="btn btn-default" id="btnAddSentence">Thêm mới</button></li>
                </ul>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('sentence/'.$librarySentenceId); ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" name="SearchText" class="form-control" value="<?php echo set_value('SearchText'); ?>" placeholder="Mẫu câu">
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listSentenceGroups, 'SentenceGroupId', 'SentenceGroupName', 'SentenceGroupId', set_value('SentenceGroupId'), true, 'Nhóm', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tiêu đề câu</th>
                                <th>Nhóm câu</th>
                                <th>Nội dung câu</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodySentence">
                            <?php $i = 0;
                            foreach($listSentences as $bt){
                                $i++; ?>
                                <tr id="sentence_<?php echo $bt['SentenceId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td id="sentenceTitle_<?php echo $bt['SentenceId']; ?>"><?php echo $bt['SentenceTitle']; ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listSentenceGroups, 'SentenceGroupId', $bt['SentenceGroupId'], 'SentenceGroupName'); ?></td>
                                    <td id="sentenceContent_<?php echo $bt['SentenceId']; ?>">
                                        <?php if($bt['FileTypeId'] == 1) echo '<a href="'.base_url(IMAGE_PATH.$bt['SentenceContent']).'" target="_blank">'.$bt['SentenceContent'].'</a>';
                                        else echo $bt['SentenceContent']; ?>
                                    </td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $bt['SentenceId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $bt['SentenceId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="sentenceGroupId_<?php echo $bt['SentenceId']; ?>" value="<?php echo $bt['SentenceGroupId']; ?>">
                                        <input type="text" hidden="hidden" id="fileTypeId_<?php echo $bt['SentenceId']; ?>" value="<?php echo $bt['FileTypeId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="deleteSentenceUrl" value="<?php echo base_url('sentence/delete'); ?>">
                    <input type="text" hidden="hidden" id="imagePath" value="<?php echo IMAGE_PATH; ?>">
                    <div class="modal fade" id="modalUpdateSentence" tabindex="-1" role="dialog" aria-labelledby="modalUpdateSentence">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Cập nhật mẫu câu</h4>
                                </div>
                                <?php echo form_open('sentence/update', array('id' => 'sentenceForm')); ?>
                                <div class="modal-body">
                                    <div class="box-body table-responsive no-padding divTable">
                                        <div class="form-group">
                                            <label class="control-label">Tiêu đề câu</label>
                                            <input type="text" class="form-control" id="sentenceTitle" value="">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Nhóm câu</label>
                                            <?php $this->Mconstants->selectObject($listSentenceGroups, 'SentenceGroupId', 'SentenceGroupName', 'SentenceGroupId1', 0, true, 'Nhóm câu',' select2'); ?>
                                        </div>
                                        <div class="form-group">
                                            <div class="radio-group">
                                                <span class="item"><input type="radio" name="FileTypeId" class="iCheck iCheckFileTypeId" id="iCheckFileTypeId_3" value="3" checked> Text</span>
                                                <span class="item"><input type="radio" name="FileTypeId" class="iCheck iCheckFileTypeId" id="iCheckFileTypeId_1" value="1"> Ảnh</span>
                                            </div>
                                        </div>
                                        <div class="form-group" id="divFileTypeId_3">
                                            <label class="control-label">Nội dung câu</label>
                                            <textarea id="sentenceContent" class="form-control" rows="4"></textarea>
                                        </div>
                                        <div class="form-group" id="divFileTypeId_1" style="display: none;">
                                            <label class="control-label">Hình ảnh <button type="button" class="btn btn-sm" id="btnUpImage"><i class="fa fa-upload"></i> Chọn hình</button></label>
                                            <img src="" style="width: 100%;display: none;" id="sentenceImage">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" id="btnUpdateSentence">Lưu</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy bỏ</button>
                                    <input type="text" hidden="hidden" id="librarySentenceId" value="<?php echo $librarySentenceId ?>">
                                    <input type="text"  hidden="hidden" id="sentenceId" value="0">
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php } ?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>