<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content">
                <div class="content-menu">
                    <ul class="nav nav-tabs" id="tabs">
                        <?php foreach ($menuParentItems as $key=>$val) {?>
                            <?php if (isset($groupChildrenItems[$val['MenuItemId']])):?>
                                <li>
                                    <a class="btn dropdown-toggle <?php echo $val['CSSClass'];?>" 
                                        data-toggle="dropdown" href="#" icon-font="<?php echo $val['IconFont'];?>" 
                                        target="<?php echo $val['Target'];?>">
                                        <?php echo $val['Title'];?> <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" id="collapsed">
                                        <?php foreach ($groupChildrenItems[$val['MenuItemId']] as $kch=>$vch) {?>
                                        <li><a href="#<?php echo $vch['Title'];?>" data-toggle="tab" 
                                            class="<?php echo $val['CSSClass'];?>"  icon-font="<?php echo $val['IconFont'];?>" 
                                            target="<?php echo $val['Target'];?>"><?php echo $vch['Title'];?></a></li>
                                        <?php }?>
                                    </ul>
                                </li>
                            <?php else:?>
                                <li><a href="#<?php $val['Title']?>" data-toggle="tab" 
                                    class="<?php echo $val['Title'];?>" icon-font="<?php echo $val['IconFont'];?>" 
                                    target="<?php echo $val['Target'];?>"><?php echo $val['Title'];?></a></li>
                            <?php endif;?>
                        
                        <?php }?>
                    </ul>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>