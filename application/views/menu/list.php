<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl">
                    <li><a href="<?php echo base_url('menu/create'); ?>" class="btn btn-primary">Thêm mới</a></li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th class="text-center">Tên menu</th>
                                <th class="text-center">Ngày tạo</th>
                                <th class="text-center">Người tạo</th>
                                <th class="text-center">Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyMenu">
                            <?php foreach ($listMenus as $k=>$val) {?>
                                <tr class="text-center">
                                    <td><?php echo $val['Name'];?></td>
                                    <td><?php echo $val['CrDateTime'];?></td>
                                    <td><?php echo $val['CrUserId'];?></td>
                                    <td><a href="<?php echo base_url('/dragmenu?id=').$val['MenuId'];?>" class="fa fa-edit">Sửa</a></td>
                                </tr>
                            <?php }?>
                            </table>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>