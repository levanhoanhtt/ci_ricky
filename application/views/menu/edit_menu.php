<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit">Lưu</button></li>
                    <li><a href="<?php echo base_url('menu'); ?>" id="menuListUrl" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php echo form_open('api/menu/create', array('id' => 'createMenuForm')); ?>
                
                <div class="row">
                    <div class="no-padding">
                        <div class="box box-default padding15">
                            <div class="form-group">
                                <label class="control-label">Tên menu <span class="required">*</span></label>
                                <input type="text" id="menuName" name="menuName" class="form-control" value="<?php echo $menuName;?>" data-field="Tên menu">
                                <input type="text" hidden="hidden" id="menuId" value="<?php echo $menuId ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>