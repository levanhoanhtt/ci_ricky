<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl">
                    <li><button class="btn btn-default">Xuất dữ liệu</button></li>
                </ul>

            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả vận chuyển</a></li>
                        <?php foreach($listFilters as $f){ ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="">
                    <?php $this->load->view('transport/_search'); ?>
                </div>
                <div class="">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-sm-3 fix-width-200">
                                <select class="form-control" id="selectAction" style="display: none;">
                                    <option value="">Chọn hành động</option>
                                    <option value="add_tags">Thêm nhãn</option>
                                    <option value="delete_tags">Bỏ nhãn</option>
                                    <option value="print_order">In phiếu bán hàng</option>
                                    <option value="print_transport">In phiếu giao hàng</option>
                                    <option value="export_transport">Xuất gửi bưu điện</option>
                                    <option value="change_status">Cập nhật trạng thái</option>
                                    <option value="delete_item">Xóa vận chuyển đã chọn</option>
                                </select>
                            </div>
                            <div class="col-sm-2 fix-width-200">
                                <select class="form-control" id="selectData" style="display: none;"></select>
                            </div>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding divTable">

                            <!-- <div class="box-tools pull-right paginate_table">
                                -->
                           <!--  </div>
                        </div> -->
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead id="html-thead">
                            <!-- <tr  class="dnd-moved">
                                <th class="th_0 th_move"><input type="checkbox" class="iCheckTable" class="checkAll"></th>
                                <th class="th_1 th_move">Mã vận chuyển</th>
                                <th class="th_2 th_move">Mã đơn hàng</th>
                                <th class="th_3 th_move">Ngày tạo VC</th>
                                <th class="th_4 th_move">Khách hàng</th>
                                <th class="th_5 th_move">Nhà vận chuyển</th>
                                <th class="th_6 th_move">TT giao hàng</th>
                                <th class="th_7 th_move">Tình trạng thu hộ (COD)</th>
                                <th class="th_8 th_move">Tiền thu (COD)</th>
                                <th class="th_9 th_move">Cơ sở xử lý</th>
                            </tr> -->
                            </thead>
                            <tbody id="tbodyTransport"></tbody>
                            <!-- <tfoot id="tfootPaginate"></tfoot> -->
                        </table>
                        
                       
                    </div>
                    <div class="box-footer"></div>
                    <input type="text" hidden="hidden" id="printOrderMultiple" value="<?php echo base_url('order/printPdfMultipleByTransport'); ?>">
                    <input type="text" hidden="hidden" id="printTransportMultiple" value="<?php echo base_url('transport/printPdfMultiple'); ?>">
                    <input type="text" hidden="hidden" id="exportTransport" value="<?php echo base_url('transport/exportTransport'); ?>">
                    <input type="text" hidden="hidden" id="changeStatusBatchUrl" value="<?php echo base_url('api/transport/changeStatusBatch'); ?>">
                    <input type="text" hidden="hidden" id="productImagePath" value="<?php echo PRODUCT_PATH; ?>">
                    <input type="hidden" value="<?php echo base_url('api/transport/getDetail')?>" id="getDetailUrl">
                    <input type="text" hidden="hidden" id="itemTypeId" value="9">
                    <input type="hidden" value="<?php echo base_url('order/edit')?>" id="urlEditOrder">
                    <input type="hidden" value="<?php echo base_url('customer/edit')?>" id="urlEditCustomer">
                    <input type="hidden" value="<?php echo base_url('transport/edit')?>" id="urlEditTransport">
                    <input type="text" hidden="hidden" id="insertTransportCommentUrl" value="<?php echo base_url('api/transport/insertComment'); ?>">
                    <input type="text" hidden="hidden" id="updateFieldUrl" value="<?php echo base_url('api/transport/updateField'); ?>">
                    <input type="text" hidden="hidden" id="transportId" value="0"> 
                    <input type="text" hidden="hidden" id="transportStatusId" value="0">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                    <div id="divTranspostJson" style="display: none;"></div>
                    <div class="modal fade" id="modalTransportStatus" tabindex="-1" role="dialog" aria-labelledby="modalTransportStatus">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-truck" aria-hidden="true"></i> Cập nhật trạng thái vận chuyển</h4>
                                </div>
                                <div class="modal-body">
                                    <p class="light-blue">Trạng thái vận chuyển</p>
                                    <div class="form-group">
                                        <?php $this->Mconstants->selectConstants('transportStatus', 'TransportStatusId'); ?>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <span hidden="hidden" id="jsonTransport"></span>
                                    <button class="btn btn-primary" type="button" id="btnUpdateTransportStatus">Hoàn thành</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="<?php echo base_url('transport/printPdfMultiple'); ?>" method="POST" target="_blank" id="printForm" style="display: none;">
                        <input type="hidden" name="TransportIds" value="" id="transportIds"/>
                        <input type="submit" value="Submit">
                    </form>

                    <div class="modal fade" id="modalItemComment" tabindex="-1" role="dialog" aria-labelledby="modalItemComment">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-comments-o"></i> Ghi chú</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="listCommentAll"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalTracking" tabindex="-1" role="dialog" aria-labelledby="modalTracking">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Cập nhật mã vận đơn</h4>
                                </div>
                                <div class="modal-body">
                                    <p class="light-blue">Mã vận đơn</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="tracking" placeholder="Mã vận đơn">
                                    </div>
                                </div>
                                <div class="modal-footer">

                                    <button class="btn btn-primary" type="button" id="btnUpdateTracking">Hoàn thành</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalShipCost" tabindex="-1" role="dialog" aria-labelledby="modalShipCost">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Cập nhật phí giao hàng</h4>
                                </div>
                                <div class="modal-body">
                                    <p class="light-blue">Phí ship thực tế</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control cost" id="shipCost" value="0">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="button" id="btnUpdateShipCost">Hoàn thành</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalTransportStatus" tabindex="-1" role="dialog" aria-labelledby="modalTransportStatus">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-truck" aria-hidden="true"></i> Cập nhật trạng thái vận chuyển</h4>
                                </div>
                                <div class="modal-body">
                                    <p class="light-blue">Trạng thái vận chuyển</p>
                                    <div class="form-group">
                                        <?php $this->Mconstants->selectConstants('transportStatus', 'TransportStatusId', $transport['TransportStatusId']); ?>
                                    </div>
                                    <div id="divCancelReason" style="display: none;">
                                        <div class="form-group">
                                            <label class="control-label">Lý do hủy:</label>
                                            <?php $this->Mconstants->selectObject($listCancelReasons, 'CancelReasonId', 'CancelReasonName', 'CancelReasonId', 0, false, '--Chọn lý do--'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Ghi chú <span class="required">*</span></label>
                                            <input class="form-control" type="text" id="cancelComment" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="button" id="btnUpdateTransportStatus">Hoàn thành</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalListFilter" tabindex="-1" role="dialog" aria-labelledby="modalListFilter">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Danh sách bộ lộc</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                              <input type="text" class="form-control" id="itemSearchNameFilter" placeholder="Nhập thông tin tìm kiếm">
                                            </div>
                                        </div>
                                        <div class="col-sm-3 text-right">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary" id="btnShowModalAddFilter">Thêm mới bộ lộc <i class="fa fa-fw fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="box-body table-responsive divTable">
                                            <table class="table new-style table-hover table-bordered table" id="table-data-filter">
                                                <thead>
                                                <tr>
                                                    <th>Thời điểm tạo</th>
                                                    <th>Tên bộ lọc</th>
                                                    <th>Người tạo</th>
                                                    <th>Mô tả</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbodyFilter"></tbody>
                                            </table>
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="text" hidden="hidden" value="<?php echo base_url('api/transport/searchByDataFilter'); ?>" id="btn-data-filter">
                                    <input type="text" hidden="hidden" value="<?php echo base_url('filter/get') ?>" id="urlGetFilter">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalAddFilter" tabindex="-1" role="dialog" aria-labelledby="modalAddFilter">
                        <div class="modal-dialog" >
                            <div class="modal-content" style="width: 500px;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title" id="titleAddFilter">Thêm mới bộ lọc</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                  <label class="control-label">Tên bộ lọc *</label>
                                                  <input type="text" class="form-control" id="nameFilter">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                  <label class="control-label">Mô tả</label>
                                                  <input type="text" class="form-control" id="noteFilter">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="input-group margin">
                                                <div class="input-group-btn">
                                                    <button type="button" class="btn dropdown-toggle transform tttt" data-toggle="dropdown" aria-expanded="false">
                                                        Chọn loại tiêu chí lọc <span class="fa fa-caret-down"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li class="dropdown-submenu">
                                                            <a class="btn-filter-data-add" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Cơ sở giao hàng</span> <span class="caret"></span></a>
                                                            <ul class="dropdown-menu">
                                                                <?php foreach($listStores as $s) :?>
                                                                    <li>
                                                                        <div><a class="btn-filter-data-add" tabindex="-1" href="javascript:void(0);" text-opertor="là" value-operator="=" field-select="transport_store" check-length="0" value-id="<?php echo $s['StoreId']; ?>"><?php echo $s['StoreName']; ?></a></div>
                                                                    </li>
                                                                <?php endforeach;?>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu">
                                                            <a class="btn-filter-data-add" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Trạng thái giao hàng</span> <span class="caret"></span></a>
                                                            <ul class="dropdown-menu">
                                                                <?php foreach($this->Mconstants->transportStatus as $i => $v){ ?>
                                                                    <li>
                                                                        <div><a class="btn-filter-data-add" tabindex="-1" href="javascript:void(0);" text-opertor="là" value-operator="=" field-select="transport_status" check-length="0" value-id="<?php echo $i; ?>"><?php echo $v; ?></a></div>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu">
                                                            <a class="btn-filter-data-add" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Trạng thái COD</span> <span class="caret"></span></a>
                                                            <ul class="dropdown-menu">
                                                                <?php foreach($this->Mconstants->CODStatus as $i => $v){ ?>
                                                                    <li>
                                                                        <div><a class="btn-filter-data-add" tabindex="-1" href="javascript:void(0);" text-opertor="là" value-operator="=" field-select="transport_status_cod" check-length="0" value-id="<?php echo $i; ?>"><?php echo $v; ?></a></div>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu">
                                                            <a class="btn-filter-data-add" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Thời điểm tạo giao hàng</span> <span class="caret"></span></a>
                                                            <ul class="dropdown-menu">
                                                                <li>
                                                                    <div class="col-sm-12 form-group">
                                                                        <select class="form-control block-display transport_create" id="select_operator_date">
                                                                            <option value="between">trong khoảng</option>
                                                                            <option value="<">trước</option>
                                                                            <option value="=">bằng</option>
                                                                            <option value=">">sau</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-sm-12 form-group">
                                                                        <input class="form-control datepicker transport_create" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart" value="27/02/2018">
                                                                    </div>
                                                                    
                                                                    <div class="col-sm-12 form-group">
                                                                        <input class="form-control datepicker transport_create" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd" value="09/10/2018">
                                                                    </div>
                                                                    <div class="col-sm-12 form-group">
                                                                        <a class="btn btn-primary btn-filter-data-add " tabindex="-1" href="javascript:void(0);" field-select="transport_create" check-length="1" value-id="1">Tìm kiếm</a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu">
                                                            <a class="btn-filter-data-add" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Thời gian xử lý</span> <span class="caret"></span></a>
                                                            <ul class="dropdown-menu">
                                                                <li>
                                                                    <div class="col-sm-12 form-group">
                                                                        <select class="transport_day_process form-control">
                                                                            <option value="=">bằng</option>
                                                                            <option value="!=">khác</option>
                                                                            <option value="<">nhỏ hơn</option>
                                                                            <option value=">">lớn hơn</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-sm-12 form-group">
                                                                        <input class="form-control transport_day_process input-number" type="text">
                                                                    </div>
                                                                    <div class="col-sm-12 form-group">
                                                                        <a class="btn btn-primary btn-filter-data-add " tabindex="-1" href="javascript:void(0);" field-select="transport_day_process" check-length="1" value-id="1">Tìm kiếm</a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu">
                                                            <a class="btn-filter-data-add" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Nhà vận chuyển</span> <span class="caret"></span></a>
                                                            <ul class="dropdown-menu">
                                                                <?php foreach($listTransporters as $t) :?>
                                                                    <li>
                                                                        <div><a class="btn-filter-data-add" tabindex="-1" href="javascript:void(0);" text-opertor="là" value-operator="=" field-select="transport_transporter" check-length="0" value-id="<?php echo $t['TransporterId']; ?>"><?php echo $t['TransporterName']; ?></a></div>
                                                                    </li>
                                                                <?php endforeach;?>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu">
                                                            <a class="btn-filter-data-add" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Phương thức vận chuyển</span> <span class="caret"></span></a>
                                                            <ul class="dropdown-menu">
                                                                <?php foreach($listTransportTypes as $tt) :?>
                                                                    <li>
                                                                        <div><a class="btn-filter-data-add" tabindex="-1" href="javascript:void(0);" text-opertor="là" value-operator="=" field-select="transport_transport_type" check-length="0" value-id="<?php echo $tt['TransportTypeId']; ?>"><?php echo $tt['TransportTypeName']; ?></a></div>
                                                                    </li>
                                                                <?php endforeach;?>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu">
                                                            <a class="btn-filter-data-add" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Tag</span> <span class="caret"></span></a>
                                                            <ul class="dropdown-menu">
                                                                <li>
                                                                    <div class="col-sm-12 form-group">
                                                                        <select class="form-control transport_tag">
                                                                            <option value="in">chứa</option>
                                                                            <option value="not in">không chứa</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-sm-12 form-group">
                                                                        <select class="form-control select2 transport_tag">
                                                                            <?php foreach($listTags as $key => $v){ ?>
                                                                                <option value="<?php echo $v['TagId']; ?>"><?php echo $v['TagName']; ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-sm-12 form-group">
                                                                        <a class="btn btn-primary btn-filter-data-add " tabindex="-1" href="javascript:void(0);" field-select="transport_tag" check-length="1" value-id="1">Tìm kiếm</a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                              
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="box-body table-responsive divTable">
                                            <label class="control-label">Danh sách các bộ lọc đang chọn</label>
                                            <table class="table new-style table-hover table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Loại tiêu chí - Giá trị lọc</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody id="container-filters-add"></tbody>
                                            </table>
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" id="filterId" value="0">

                                    <button type="button" class="btn btn-danger" id="btn-remove-ilter" style="float: left; display: none">Xóa</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy bỏ</button>
                                    <button type="button" class="btn btn-primary"  data-href="<?php echo base_url('filter/save'); ?>" id="btn-save-filter-add" display-order="1">Thêm</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalConfigTable" tabindex="-1" role="dialog" aria-labelledby="modalConfigTable">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Cấu hình cột thông tin</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                        <table class="table new-style table-hover table-bordered" id="table-data-filter">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Tên cột dữ liệu</th>
                                                    <th>Ẩn/Hiện cột</th>
                                                    <th>Ghim cột</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyConfig">
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    <button type="button" class="btn btn-primary" id="bnAddConfigTable">Thêm</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>