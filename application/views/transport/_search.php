<div class="row">
	<div class="col-sm-7">
		<div class="input-group margin">
			<div class="input-group-btn">
			    <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
			        Chọn loại tiêu chí lọc <span class="fa fa-caret-down"></span>
			    </button>
			    <ul class="dropdown-menu">
			        <li class="dropdown-submenu">
			            <a class="btn-filter-data" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Cơ sở giao hàng</span> <span class="caret"></span></a>
			            <ul class="dropdown-menu">
			                <?php foreach($listStores as $s) :?>
			                    <li>
			                    	<div><a class="btn-filter-data" tabindex="-1" href="javascript:void(0);" text-opertor="là" value-operator="=" field-select="transport_store" check-length="0" value-id="<?php echo $s['StoreId']; ?>"><?php echo $s['StoreName']; ?></a></div>
			                    </li>
			                <?php endforeach;?>
			            </ul>
			        </li>
			        <li class="dropdown-submenu">
			            <a class="btn-filter-data" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Trạng thái giao hàng</span> <span class="caret"></span></a>
			            <ul class="dropdown-menu">
			                <?php foreach($this->Mconstants->transportStatus as $i => $v){ ?>
			                    <li>
			                    	<div><a class="btn-filter-data" tabindex="-1" href="javascript:void(0);" text-opertor="là" value-operator="=" field-select="transport_status" check-length="0" value-id="<?php echo $i; ?>"><?php echo $v; ?></a></div>
			                    </li>
			                <?php } ?>
			            </ul>
			        </li>
			        <li class="dropdown-submenu">
			            <a class="btn-filter-data" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Trạng thái COD</span> <span class="caret"></span></a>
			            <ul class="dropdown-menu">
			                <?php foreach($this->Mconstants->CODStatus as $i => $v){ ?>
			                    <li>
			                    	<div><a class="btn-filter-data" tabindex="-1" href="javascript:void(0);" text-opertor="là" value-operator="=" field-select="transport_status_cod" check-length="0" value-id="<?php echo $i; ?>"><?php echo $v; ?></a></div>
			                    </li>
			                <?php } ?>
			            </ul>
			        </li>
			        <li class="dropdown-submenu">
			            <a class="btn-filter-data" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Thời điểm tạo giao hàng</span> <span class="caret"></span></a>
			            <ul class="dropdown-menu">
			                <li>
			                    <div class="col-sm-12 form-group">
			                        <select class="form-control block-display transport_create" id="select_operator_date">
			                            <option value="between">trong khoảng</option>
			                            <option value="<">trước</option>
			                            <option value="=">bằng</option>
			                            <option value=">">sau</option>
			                        </select>
			                    </div>
			                    <div class="col-sm-12 form-group">
			                        <input class="form-control datepicker transport_create" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart" value="27/02/2018">
			                    </div>
			                    
			                    <div class="col-sm-12 form-group">
			                        <input class="form-control datepicker transport_create" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd" value="09/10/2018">
			                    </div>
			                    <div class="col-sm-12 form-group">
			                        <a class="btn btn-primary btn-filter-data " tabindex="-1" href="javascript:void(0);" field-select="transport_create" check-length="1" value-id="1">Tìm kiếm</a>
			                    </div>
			                </li>
			            </ul>
			        </li>
			        <li class="dropdown-submenu">
			            <a class="btn-filter-data" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Thời gian xử lý</span> <span class="caret"></span></a>
			            <ul class="dropdown-menu">
			                <li>
			                    <div class="col-sm-12 form-group">
			                        <select class="transport_day_process form-control">
			                            <option value="=">bằng</option>
			                            <option value="!=">khác</option>
			                            <option value="<">nhỏ hơn</option>
			                            <option value=">">lớn hơn</option>
			                        </select>
			                    </div>
			                    <div class="col-sm-12 form-group">
			                        <input class="form-control transport_day_process input-number" type="text">
			                    </div>
			                    <div class="col-sm-12 form-group">
			                        <a class="btn btn-primary btn-filter-data " tabindex="-1" href="javascript:void(0);" field-select="transport_day_process" check-length="1" value-id="1">Tìm kiếm</a>
			                    </div>
			                </li>
			            </ul>
			        </li>
			        <li class="dropdown-submenu">
			            <a class="btn-filter-data" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Nhà vận chuyển</span> <span class="caret"></span></a>
			            <ul class="dropdown-menu">
			                <?php foreach($listTransporters as $t) :?>
			                    <li>
			                    	<div><a class="btn-filter-data" tabindex="-1" href="javascript:void(0);" text-opertor="là" value-operator="=" field-select="transport_transporter" check-length="0" value-id="<?php echo $t['TransporterId']; ?>"><?php echo $t['TransporterName']; ?></a></div>
			                    </li>
			                <?php endforeach;?>
			            </ul>
			        </li>
			        <li class="dropdown-submenu">
			            <a class="btn-filter-data" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Phương thức vận chuyển</span> <span class="caret"></span></a>
			            <ul class="dropdown-menu">
			                <?php foreach($listTransportTypes as $tt) :?>
			                    <li>
			                    	<div><a class="btn-filter-data" tabindex="-1" href="javascript:void(0);" text-opertor="là" value-operator="=" field-select="transport_transport_type" check-length="0" value-id="<?php echo $tt['TransportTypeId']; ?>"><?php echo $tt['TransportTypeName']; ?></a></div>
			                    </li>
			                <?php endforeach;?>
			            </ul>
			        </li>
			        <li class="dropdown-submenu">
			            <a class="btn-filter-data" aria-expanded="false" data-toggle="dropdown" tabindex="-1" href="javascript:void(0);" value-id="0"><span>Tag</span> <span class="caret"></span></a>
			            <ul class="dropdown-menu">
			                <li>
			                    <div class="col-sm-12 form-group">
			                        <select class="form-control transport_tag">
			                            <option value="in">chứa</option>
			                            <option value="not in">không chứa</option>
			                        </select>
			                    </div>
			                    <div class="col-sm-12 form-group">
			                        <select class="form-control select2 transport_tag">
			                            <?php foreach($listTags as $key => $v){ ?>
			                                <option value="<?php echo $v['TagId']; ?>"><?php echo $v['TagName']; ?></option>
			                            <?php } ?>
			                        </select>
			                    </div>
			                    <div class="col-sm-12 form-group">
			                        <a class="btn btn-primary btn-filter-data " tabindex="-1" href="javascript:void(0);" field-select="transport_tag" check-length="1" value-id="1">Tìm kiếm</a>
			                    </div>
			                </li>
			            </ul>
			        </li>
			    </ul>
			</div>
			<input type="text" hidden="hidden" value="<?php echo base_url('api/transport/searchByFilter'); ?>" id="btn-filter">
			<input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
			<span class="input-group-btn">
			    <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
			</span>
			<span class="input-group-btn">
			    <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
			</span>
		</div>
	</div>
	<div class="col-sm-3">
		 <div class="input-group margin"> 
            <select id="btn_add_ilter" class="form-control" >
                <option value="0">Danh sách bộ lọc</option>
                	<?php foreach($listFilters as $f){ ?>
                <option value="<?php echo $f['FilterId'] ?>"><?php echo $f['FilterName']; ?></option>
                 <?php } ?>
            </select> 
            <span class="input-group-btn">
                <button class="btn btn-default btnShowModalListFilter" type="button"><i class="fa fa-fw fa-cog"></i></button>
            </span>
        </div>
	</div>
	<div class="col-sm-2">
		<div class="input-group margin">
        	<input type="text" class="form-control" disabled="true" id="txtCountColTable" value="hiển thị 0/0 cột">
            <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-flat" id="btnShowModalConfigTable"><i class="fa fa-fw fa-cog"></i></button>
            </span>
      </div>
	</div>
</div>

<div class="mb10 mgt-10">
    <ul id="container-filters"></ul>
</div>