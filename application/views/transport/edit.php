<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content-header no-pd-lr">
            <h1 class="ttl-list-order ft-seogeo" style="margin-bottom: 0 !important;">
                <?php if($transportId > 0){ ?>
                    <a href=" <?php echo base_url('transport'); ?>">Danh sách vận chuyển</a> / <?php echo $transport['TransportCode']; ?>
                <?php }
                else echo $title; ?>
            </h1>
            <ul class="list-inline">
                <li><a id="aTransportList" href="<?php echo base_url('transport'); ?>" class="btn btn-default">Đóng</a></li>
                <?php if($transportId > 0){ ?><li><a href="<?php echo base_url('transport/printPdf/'.$transportId); ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> In</a></li><?php } ?>
            </ul>
        </section>
        <section class="content new-box-stl ft-seogeo section-transport">
            <?php $this->load->view('includes/notice'); ?>
            <?php if($transportId > 0){ ?>
            <?php if(in_array($transport['TransportStatusId'], array(5, 6, 7))){
                    $statusName = 'đã bị hủy';
                    if($transport['TransportStatusId'] == 6) $statusName = 'đang chuyển hoàn';
                    elseif($transport['TransportStatusId'] == 7) $statusName = 'đã chuyển hoàn'; ?>
                <div class="alert alert-danger" style="margin-left: -15px;margin-bottom: 15px;">
                    <p>
                        <i class="fa fa-exclamation"></i> Phiếu giao hàng <?php echo $statusName; ?> lúc <?php echo ddMMyyyy($transport['UpdateDateTime'], 'H:i d/m/Y'); ?>.
                        <?php $cancelReasonName = '';
                        if($transport['CancelReasonId'] > 0) $cancelReasonName = $this->Mconstants->getObjectValue($listCancelReasons, 'CancelReasonId', $transport['CancelReasonId'], 'CancelReasonName');
                        if(!empty($cancelReasonName)){
                            echo ' Lý do: '.$cancelReasonName;
                            if(!empty($transport['CancelComment'])) echo ' ('.$transport['CancelComment'].')';
                        }
                        elseif(!empty($transport['CancelComment'])) echo ' Lý do: '.$transport['CancelComment']; ?>
                    </p>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-sm-8 no-padding">
                    <div class="box box-default padding20 same" id="divSlider">
                        <div class="box-step has-slider">
                            <ul class="clearfix bxslider">
                                <?php foreach($this->Mconstants->transportStatus as $i => $v){ ?>
                                    <li class="<?php if(in_array($i, $transportStatusLogIds)) echo 'block-display'; else echo 'none-display'; ?> liTransportStatus<?php if($transport['TransportStatusId'] == $i) echo ' active'; ?>" id="liTransportStatus_<?php echo $i; ?>">
                                        <a href="javascript:void(0);"><?php echo $v; ?> <div class="icon"><img src="assets/vendor/dist/img//transport/<?php echo $i.'.'; if($transport['TransportStatusId'] == $i) echo 2; else echo 1; ?>.png" alt="<?php echo $v; ?>"></div></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="box box-default padding15">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover stl2">
                                        <thead class="theadNormal">
                                            <tr>
                                                <th>Thông tin Sản phẩm</th>
                                                <th class="text-right" style="width: 134px;">Đơn giá * SL</th>
                                                <th class="text-right" style="width: 134px;">Thành tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbodyProduct">
                                            <?php $products = array();
                                            $productChilds = array();
                                            foreach($listOrderProducts as $op){
                                                if(!isset($products[$op['ProductId']])) $products[$op['ProductId']] = $this->Mproducts->get($op['ProductId'], true, '', 'ProductName, ProductImage, BarCode');
                                                $productName = $products[$op['ProductId']]['ProductName'];
                                                $productImage = $products[$op['ProductId']]['ProductImage'];
                                                $barCode = $products[$op['ProductId']]['BarCode'];
                                                $productChildName = '';
                                                if($op['ProductChildId'] > 0){
                                                    if(!isset($productChilds[$op['ProductChildId']])) $productChilds[$op['ProductChildId']] = $this->Mproductchilds->get($op['ProductChildId'], true, '', 'ProductName, ProductImage, BarCode');
                                                    $productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
                                                    $productImage = $productChilds[$op['ProductChildId']]['ProductImage'];
                                                    $barCode = $productChilds[$op['ProductChildId']]['BarCode'];
                                                }
                                                if(empty($productImage)) $productImage = NO_IMAGE;
                                            ?>
                                            <tr data-id="<?php echo $op['ProductId']; ?>" data-child="<?php echo $op['ProductChildId']; ?>">
                                                <td>
                                                    <img src="<?php echo PRODUCT_PATH.$productImage; ?>" class="productImg">
                                                    <a href="<?php echo base_url('product/edit/'.$op['ProductId']); ?>" class="light-dark" target="_blank">
                                                        <?php echo $productName;
                                                        if(!empty($productChildName)) echo ' | '.$productChildName; ?>
                                                    </a>
                                                    <br />
                                                    SKU: <?php echo $barCode; ?>
                                                </td>
                                                <td class="text-right"><?php echo priceFormat($op['Price']). ' x '.priceFormat($op['Quantity']); ?></td>
                                                <td class="text-right"><?php echo priceFormat($op['Quantity'] * $op['Price']); ?> đ</td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group stl2">
                                    <label class="control-label light-blue">Nhắn nhủ giao hàng</label>
                                    <input type="text" class="form-control" disabled value="<?php echo $transport['Comment']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <table class="tb-totaly">
                                    <thead>
                                        <tr>
                                            <th>Tổng cần thu (COD)</th>
                                            <td><span id="spanCODCost"><?php echo priceFormat($transport['CODCost']); ?></span> ₫</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>Tổng giá trị đơn hàng</th>
                                            <td><?php echo priceFormat($order['TotalCost']); ?> ₫</td>
                                        </tr>
                                        <tr>
                                            <th>Phí ship thu khách</th>
                                            <td><?php echo priceFormat($order['TransportCost']); ?> ₫</td>
                                        </tr>
                                        <tr>
                                            <th>Phí ship thực tế</th>
                                            <td><span class="spanShipCost"><?php echo priceFormat($transport['ShipCost']); ?></span> ₫</td>
                                        </tr>
                                        <tr>
                                            <th>Khối lượng (gam)</th>
                                            <td><?php echo priceFormat($transport['Weight']); ?> g</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <a href="javascript:void(0)" class="btn btn-updaten giao<?php if($transport['CODCost'] == 0 || $transport['CODStatusId'] == 4) echo ' actived'; ?>" id="aCODStatus" style="width: 100%;padding: 5px 0;"><?php echo $transport['CODCost'] == 0 ? 'Không có COD' : 'Cập nhật trạng thái thu hộ COD'; ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="box box-default padding15">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="light-blue">Ghi chú</label>
                                <div class="box-transprt clearfix mb10">
                                    <button class="btn-updaten save" type="button" id="btnInsertComment">Lưu</button>
                                    <input type="text" class="add-text" id="comment">
                                </div>
                                <div id="listComment">
                                    <?php $i = 0;
                                    $now = new DateTime(date('Y-m-d'));
                                    foreach($listTransportComments as $oc){
                                        $avatar = (empty($oc['Avatar']) ? NO_IMAGE : $oc['Avatar']);
                                        $i++;
                                        if($i < 3){ ?>
                                            <div class="box-customer mb10">
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <th rowspan="2" valign="top" style="width: 50px;"><img src="<?php echo USER_PATH.$avatar; ?>" alt=""></th>
                                                        <th><a href="javascript:void(0)" class="name"><?php echo $oc['FullName']; ?></a></th>
                                                        <th class="time">
                                                            <?php $dayDiff = getDayDiff($oc['CrDateTime'], $now);
                                                            echo getDayDiffText($dayDiff).ddMMyyyy($oc['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <p class="pComment"><?php echo $oc['Comment']; ?></p>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php }
                                    } ?>
                                </div>
                                <?php if(count($listTransportComments) > 2){ ?>
                                    <div class="text-right light-dark">
                                        <a href="javascript:void(0)" id="aShowComment">Xem tất cả &gt;&gt;</a>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-sm-6"></div>
                        </div>
                    </div>
                    <?php $this->load->view('includes/action_logs_new'); ?>
                </div>
                <div class="col-sm-4">
                    <div class="box box-primary">
                        <h3 class="box-title">
                            <span class="dropdown">
                                <span class="dropdown-toggle" style="cursor: pointer;" type="button" data-toggle="dropdown">
                                    <span class="light-blue pos-re" id="spanStoreName" style="color: #fff;">CS : <?php echo $transport['StoreId'] > 0 ? $this->Mconstants->getObjectValue($listStores, 'StoreId', $transport['StoreId'], 'StoreName') : '' ?></span>
                                    <i class="fa fa-align-justify" aria-hidden="true" style="margin-left: 10px;"></i>
                                </span>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)" id="aUpdatePending"><img src="assets/vendor/dist/img/pop01.png">&nbsp;&nbsp;Cập nhật chờ xử lý</a></li>
                                    <li><a href="javascript:void(0)" id="aChooseStore"><img src="assets/vendor/dist/img/pop03.png">&nbsp;&nbsp;Chọn cơ sở xử lý</a></li>
                                </ul>
                            </span>
                        </h3>
                        <?php if($transport['TransportStatusId'] != 4 && $transport['TransportStatusId'] != 5 && $transport['TransportStatusId'] != 7){ ?>
                            <p><a href="javascript:void(0)" id="aTransportStatus"><i class="fa fa-truck fa-flip-horizontal"></i> Cập nhật trạng thái vận chuyển</a></p>
                        <?php } else{ ?>
                            <p><a href="javascript:void(0)" id="aTransportStatus" style="color: #bbb;"><i class="fa fa-truck fa-flip-horizontal"></i> Cập nhật trạng thái vận chuyển</a></p>
                        <?php } ?>
                    </div>
                    <div class="box box-default">
                        <table class="tbl-cod">
                            <tr>
                                <th>Trạng thái giao hàng</th>
                                <td class="text-right" id="tdTransportStatus">
                                    <?php if($transport['TransportStatusId'] > 0){ ?>
                                        <span class="<?php echo $this->Mtransports->labelCss['TransportStatusCss'][$transport['TransportStatusId']]; ?>">
                                            <?php echo $this->Mconstants->transportStatus[$transport['TransportStatusId']];
                                            if($transport['PendingStatusId'] > 0) echo ' ('.$this->Mconstants->getObjectValue($listPendingStatus, 'PendingStatusId', $transport['PendingStatusId'], 'PendingStatusName').')'; ?>
                                        </span>
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <th class="border-bottom">Trạng thái thu hộ (COD)</th>
                                <td class="text-right border-bottom" id="tdCODStatus"><?php if($transport['CODStatusId'] > 0){ ?><span class="<?php echo $this->Mtransports->labelCss['CODStatusCss'][$transport['CODStatusId']]; ?>"><?php echo $this->Mconstants->CODStatus[$transport['CODStatusId']]; ?></span><?php } ?></td>
                            </tr>
                            <tr>
                                <th>Mã vận đơn</th>
                                <td class="text-right"><a href="javascript:void(0)" id="aTracking" class="light-dark"><?php echo empty($transport['Tracking']) ? 'Cập nhật' : $transport['Tracking']; ?></a></td>
                            </tr>
                            <tr>
                                <th>Phí ship thực tế</th>
                                <td class="text-right"><a href="javascript:void(0)" id="aShipCost" class="light-dark"><?php echo $transport['ShipCost'] > 0 ? '<span class="spanShipCost">'.priceFormat($transport['ShipCost']).'</span> ₫' : '<span class="spanShipCost">Cập nhật</span>'; ?></a></td>
                            </tr>
                            <tr>
                                <th>Nhà vận chuyển</th>
                                <td class="text-right">
                                    <a href="javascript:void(0)" id="aTransporter" class="light-dark">
                                        <?php $value = $this->Mconstants->getObjectValue($listTransporters, 'TransporterId', $transport['TransporterId'], 'TransporterName');
                                        echo empty($value) ? 'Cập nhật' : $value; ?>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="box box-default ">
                        <table class="tbl-cod">
                            <tr>
                                <th>Mã vận chuyển</th>
                                <td class="mid"><?php echo ddMMyyyy($transport['CrDateTime'], 'H:i | d/m/Y'); ?></td>
                                <td class="text-right"><a href="javascript:void(0)" class="light-dark" id="aTransportCode"><?php echo $transport['TransportCode']; ?></a></td>
                            </tr>
                            <tr>
                                <th>Mã đơn hàng</th>
                                <td class="mid"><?php echo ddMMyyyy($order['CrDateTime'], 'H:i | d/m/Y'); ?></td>
                                <td class="text-right"><a href="<?php echo base_url('order/edit/'.$transport['OrderId']); ?>" target="_blank" class="light-dark"><?php echo $order['OrderCode']; ?></a></td>
                            </tr>
                        </table>
                    </div>
                    <div class="box box-default mh-wrap-customer" id="divCustomer">
                        <div class="with-border">
                            <div class="mh-info-customer">
                                <div class="clearfix">
                                    <img class="avatar-user" src="assets/vendor/dist/img/users2.png">
                                    <div class="name-info">
                                        <h4 class="i-name"><a href="<?php echo base_url('customer/edit/'.$transport['CustomerId']); ?>" target="_blank"><?php echo $customerAddress ? $customerAddress['CustomerName'] : ''; ?></a></h4>
                                        <div class="phones i-phone"><?php echo $customerAddress ? $customerAddress['PhoneNumber'] : ''; ?></div>
                                    </div>
                                </div>
                                <!--<div class="i-cusType"><span class="label label-success"><?php //echo $this->Mcustomergroups->getFieldValue(array('CustomerGroupId' => $customer['CustomerGroupId']), 'CustomerGroupName'); ?></span></div>-->
                            </div>
                        </div>
                        <div class="box-body">
                            <div><h4 class="mgbt-20 light-blue">Thông tin giao hàng &nbsp;&nbsp;<a href="javascript:void(0)" id="aCustomerAddress"><i class="fa fa-pencil"></i></a></h4></div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php if($customerAddress){ ?>
                                        <div class="item">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            <span class="i-name"><?php echo $customerAddress['CustomerName']; ?></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span class="i-phone"><?php echo $customerAddress['PhoneNumber']; ?></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span class="i-email"><?php echo $customerAddress['Email']; ?></span>
                                        </div>
                                        <div class="item i-address">
                                            <?php $zipCode = $customerAddress['ZipCode'];
                                            if(empty($zipCode) || $zipCode == '0'){ ?>
                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                <span class="i-ward"><spam class="spanAddress"><?php echo $customerAddress['Address'] . '</span> ' . $this->Mwards->getWardName($customerAddress['WardId']); ?></span>
                                                <span class="br-line i-district"><?php echo $this->Mconstants->getObjectValue($listDistricts, 'DistrictId', $customerAddress['DistrictId'], 'DistrictName'); ?></span>
                                                <span class="br-line i-province"><?php echo $this->Mconstants->getObjectValue($listProvinces, 'ProvinceId', $customerAddress['ProvinceId'], 'ProvinceName'); ?></span>
                                            <?php } else{ ?>
                                                <i class="fa fa-list-alt" aria-hidden="true"></i>
                                                <span class="i-province">ZipCode: <?php echo $zipCode; ?></span>
                                            <?php } ?>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-id-card" aria-hidden="true"></i>
                                                <span class="i-country" data-id="<?php echo $customerAddress['CountryId']; ?>" data-province="<?php echo $customerAddress['ProvinceId']; ?>" data-district="<?php echo $customerAddress['DistrictId']; ?>" data-ward="<?php echo $customerAddress['WardId']; ?>" data-zip="<?php echo $zipCode; ?>">
                                                    <?php $countryName = $this->Mconstants->getObjectValue($listCountries, 'CountryId', $customerAddress['CountryId'], 'CountryName');
                                                    if(empty($countryName)) $countryName = 'Việt Nam';
                                                    echo $countryName; ?>
                                                </span>
                                        </div>
                                    <?php } else{ ?>
                                        <div class="item">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            <span class="i-name"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span class="i-phone"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span class="i-email"></span>
                                        </div>
                                        <div class="item i-address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <span class="i-ward"></span>
                                            <span class="br-line i-district"></span>
                                            <span class="br-line i-province"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-id-card" aria-hidden="true"></i>
                                            <span class="i-country" data-id="232" data-province="0" data-district="0" data-ward="0" data-zip="">Việt Nam</span>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-default more-task">
                        <!--<a href="javascript:void(0)" class="task1 aRemind"><img src="assets/vendor/dist/img/icon07.png">Thêm task chăm sóc khách hàng</a>-->
                        <a href="javascript:void(0)" class="aRemind"><img src="assets/vendor/dist/img/icon08.png">Tạo chương trình nhắc nhở</a>
                    </div>
                    <div class="box box-default classify padding20">
                        <label class="light-blue">Chăm sóc giao hàng (CSKH + VC)</label>
                        <div class="box-transprt clearfix mb10">
                            <button type="button" class="btn-updaten save" id="btnInsertComment1">
                                Lưu
                            </button>
                            <input type="text" class="add-text" id="comment1" value="">
                        </div>
                        <div class="listComment" id="listComment1">
                            <?php $i = 0;
                            $now = new DateTime(date('Y-m-d'));
                            foreach($listOrderComments as $oc){
                                $avatar = (empty($oc['Avatar']) ? NO_IMAGE : $oc['Avatar']);
                                $i++;
                                if($i < 3){ ?>
                                    <div class="box-customer mb10">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <th rowspan="2" valign="top" style="width: 50px;"><img src="<?php echo USER_PATH.$avatar; ?>" alt=""></th>
                                                <th><a href="javascript:void(0)" class="name"><?php echo $oc['FullName']; ?></a></th>
                                                <th class="time">
                                                    <?php $dayDiff = getDayDiff($oc['CrDateTime'], $now);
                                                    echo getDayDiffText($dayDiff).ddMMyyyy($oc['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <p class="pComment"><?php echo $oc['Comment']; ?></p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php }
                            } ?>
                        </div>
                        <?php if(count($listOrderComments) > 2){ ?>
                            <div class="text-right light-dark">
                                <a href="javascript:void(0)" id="aShowComment1">Xem tất cả &gt;&gt;</a>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="box box-default more-tabs padding20">
                        <div class="form-group">
                            <label class="control-label light-blue" style="width: 100%;line-height: 28px;">Nhãn (cách nhau bởi dấu phẩy) <button class="btn-updaten save btn-sm pull-right" type="button" id="btnUpdateTag">Lưu</button></label>
                            <input type="text" class="form-control" id="tags">
                        </div>
                        <p class="light-gray">Bạn có thể chọn những tag đã được sử dụng</p>
                        <div class="clearfix">
                            <?php foreach($listTags as $t){ ?>
                                <a href="javascript:void(0)" class="ntags"><?php echo $t['TagName']; ?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <input type="text" hidden="hidden" id="transportId" value="<?php echo $transportId; ?>">
            <input type="text" hidden="hidden" id="orderId" value="<?php echo $transport['OrderId']; ?>">
            <input type="text" hidden="hidden" id="transportCode" value="<?php echo $transport['TransportCode']; ?>">
            <input type="text" hidden="hidden" id="customerId" value="<?php echo $transport['CustomerId']; ?>">
            <input type="text" hidden="hidden" id="transportStatusIdOriginal" value="<?php echo $transport['TransportStatusId']; ?>">
            <input type="text" hidden="hidden" id="pendingStatusIdOriginal" value="<?php echo $transport['PendingStatusId']; ?>">
            <input type="text" hidden="hidden" id="cODStatusIdOriginal" value="<?php echo $transport['CODStatusId']; ?>">
            <input type="text" hidden="hidden" id="transporterIdOriginal" value="<?php echo $transport['TransporterId']; ?>">
            <input type="text" hidden="hidden" id="storeIdOriginal" value="<?php echo $transport['StoreId']; ?>">
            <input type="text" hidden="hidden" id="customerAddressId" value="<?php echo $transport['CustomerAddressId']; ?>">
            <input type="text" hidden="hidden" id="insertTransportCommentUrl" value="<?php echo base_url('api/transport/insertComment'); ?>">
            <input type="text" hidden="hidden" id="insertOrderCommentUrl" value="<?php echo base_url('api/order/insertComment'); ?>">
            <input type="text" hidden="hidden" id="updateFieldUrl" value="<?php echo base_url('api/transport/updateField'); ?>">
            <input type="text" hidden="hidden" id="updateItemTagUrl" value="<?php echo base_url('api/tag/updateItem'); ?>">
            <input type="text" hidden="hidden" id="transportStatusIconPath" value="assets/vendor/dist/img//transport/">
            <?php foreach($tagNames as $tagName){ ?>
                <input type="text" hidden="hidden" class="tagName" value="<?php echo $tagName; ?>">
            <?php } ?>
            <?php $this->load->view('includes/modal/customer_address', array('listCountries' => $listCountries, 'listProvinces' => $listProvinces, 'listDistricts' => $listDistricts)); ?>
            <?php $this->load->view('includes/modal/comment', array('itemName' => 'vận chuyển', 'listItemComments' => $listTransportComments)); ?>
            <?php $this->load->view('includes/modal/remind'); ?>
            <div class="modal fade" id="modalTracking" tabindex="-1" role="dialog" aria-labelledby="modalTracking">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Cập nhật mã vận đơn</h4>
                        </div>
                        <div class="modal-body">
                            <p class="light-blue">Mã vận đơn</p>
                            <div class="form-group">
                                <input type="text" class="form-control" id="tracking" placeholder="Mã vận đơn">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" id="btnUpdateTracking">Hoàn thành</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalShipCost" tabindex="-1" role="dialog" aria-labelledby="modalShipCost">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Cập nhật phí giao hàng</h4>
                        </div>
                        <div class="modal-body">
                            <p class="light-blue">Phí ship thực tế</p>
                            <div class="form-group">
                                <input type="text" class="form-control cost" id="shipCost" value="0">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" id="btnUpdateShipCost">Hoàn thành</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalTransporter" tabindex="-1" role="dialog" aria-labelledby="modalTransporter">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Cập nhật nhà vận chuyển</h4>
                        </div>
                        <div class="modal-body">
                            <p class="light-blue">Nhà vận chuyển</p>
                            <div class="form-group">
                                <?php $this->Mconstants->selectObject($listTransporters, 'TransporterId', 'TransporterName', 'TransporterId', $transport['TransporterId']); ?>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" id="btnUpdateTransporter">Hoàn thành</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalTransportStatus" tabindex="-1" role="dialog" aria-labelledby="modalTransportStatus">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="box-title"><i class="fa fa-truck" aria-hidden="true"></i> Cập nhật trạng thái vận chuyển</h4>
                        </div>
                        <div class="modal-body">
                            <p class="light-blue">Trạng thái vận chuyển</p>
                            <div class="form-group">
                                <?php $this->Mconstants->selectConstants('transportStatus', 'TransportStatusId', $transport['TransportStatusId']); ?>
                            </div>
                            <div id="divCancelReason" style="display: none;">
                                <div class="form-group">
                                    <label class="control-label">Lý do hủy:</label>
                                    <?php $this->Mconstants->selectObject($listCancelReasons, 'CancelReasonId', 'CancelReasonName', 'CancelReasonId', 0, false, '--Chọn lý do--'); ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Ghi chú <span class="required">*</span></label>
                                    <input class="form-control" type="text" id="cancelComment" value="">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" id="btnUpdateTransportStatus">Hoàn thành</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalUpdatePending" tabindex="-1" role="dialog" aria-labelledby="modalUpdatePending">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="box-title"><i class="fa fa-wrench"></i> Cập nhật trạng thái chờ xử lý</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="radio-group">
                                    <?php foreach($listPendingStatus as $ps){ ?>
                                        <p class="item"><input type="radio" name="PendingStatusId" class="iCheck" value="<?php echo $ps['PendingStatusId']; ?>"<?php if($transport['PendingStatusId'] == $ps['PendingStatusId']) echo ' checked'; ?>> <?php echo $ps['PendingStatusName']; ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" id="btnUpdatePending">Cập nhật</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalCODStatus" tabindex="-1" role="dialog" aria-labelledby="modalCODStatus">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Cập nhật trạng thái COD</h4>
                        </div>
                        <div class="modal-body">
                            <p class="light-blue">Hãy cân nhắc kỹ lưỡng !</p>
                            <div class="form-group">
                                <?php $this->Mconstants->selectConstants('CODStatus', 'CODStatusId', $transport['CODStatusId']); ?>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" id="btnUpdateCODStatus">Hoàn thành</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalChooseStore" tabindex="-1" role="dialog" aria-labelledby="modalChooseStore">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="box-title"><i class="fa fa-wrench"></i> Lựa chọn cơ sở xử lý thủ công</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="radio-group">
                                    <?php foreach($listStores as $s){ ?>
                                        <p class="item"><input type="radio" name="StoreId" class="iCheck" value="<?php echo $s['StoreId']; ?>"<?php if($transport['StoreId'] == $s['StoreId']) echo ' checked'; ?>> <?php echo $s['StoreName']; ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" id="btnUpdateStore">Cập nhật</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalOrderComment" tabindex="-1" role="dialog" aria-labelledby="modalOrderComment">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="box-title"><i class="fa fa-comments-o"></i> Chăm sóc giao hàng (CSKH + VC)</h4>
                        </div>
                        <div class="modal-body">
                            <div class="listComment">
                                <?php $now = new DateTime(date('Y-m-d'));
                                foreach($listOrderComments as $ic){
                                    $avatar = (empty($oc['Avatar']) ? NO_IMAGE : $oc['Avatar']); ?>
                                    <div class="box-customer mb10">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <th rowspan="2" valign="top" style="width: 50px;"><img src="<?php echo USER_PATH.$avatar; ?>" alt=""></th>
                                                <th><a href="javascript:void(0)" class="name"><?php echo $ic['FullName']; ?></a></th>
                                                <th class="time">
                                                    <?php $dayDiff = getDayDiff($ic['CrDateTime'], $now);
                                                    echo getDayDiffText($dayDiff).ddMMyyyy($ic['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <p class="pComment"><?php echo $ic['Comment']; ?></p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>