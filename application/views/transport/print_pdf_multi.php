<!DOCTYPE html>
<html>
<head>
	<base href="<?php echo base_url() ?>">
	<link rel="stylesheet" type="text/css" href="assets/css/transportprint.css?6">
	<title>In vận chuyển</title>
</head>
<body>
<?php $products = array();
$productChilds = array();
foreach ($printData as $data): ?>
	<div class="content_border_mutil">
		<div class="top">
			<div class="content-img">
				<img  src="assets/vendor/dist/img/logo_in.png" style="height: 120px;width: 160px;margin-top: 5px;">
			</div>
			<div class="wrap-left-t">
				<h1><?php echo $configs["COMPANY_NAME_REG"]; ?></h1>
				<p>Địa chỉ: <?php echo $configs["ADDRESS"]; ?></p>
				<p>Hotline vận chuyển :  097.147.7007</p>
				<p>Website: <?php echo base_url(); ?> </p>
				<p>CS xử lý : <?php echo $data['storeName']; ?></p>
			</div>
			<div class="wrap-right-t">
				<img src="<?php echo $data['barcodeSrc']; ?>" width="120px;">
				<p class="text-center"><b><?php echo  $data['transport']['TransportCode']; ?></b></p>
			</div>
		</div>
		<div class="mid">
			<h1 class="text-center mt-10"><?php echo $configs["TRANSPORT_PRINT_TEXT"]; ?></h1>
			<div class="content">
				<div class="w33">
					<p class="mbt-10"><b>Thông tin người nhận</b></p>
					<div class="wrap-box-last bdl-0 bdr">
						<p><b><?php echo $data['customerAddress'] ? $data['customerAddress']['CustomerName'] : ''; ?></b></p>
						<p>SĐT: <?php echo $data['customerAddress'] ? $data['customerAddress']['PhoneNumber'] : ''; ?></p>
						<p><?php echo $data['customerAddress'] ? $data['customerAddress']['Address'] : ''; ?></p>
						<!-- <div class="loinhan">
							<p><b>Lời nhắn</b></p>
							<p><?php // echo $data['transport']['Comment']; ?></p>
						</div> -->
						<div class="signature text-center">
							<div class="w22">
								<p><b>Người giao</b></p>
								<p class="c-ddd">( Kí, họ tên )</p>
							</div>
							<div class="w22">
								<p><b>Người nhận</b></p>
								<p class="c-ddd">( Kí, họ tên )</p>
							</div>
						</div>
							<div class="clearfix"></div>
					</div>
				</div>
				<div class="w33">
					<p class="mbt-10"><b>Thanh toán</b></p>
					<div class="wrap-box-last bdl-0">
						<h2 class="mt-5">COD : <span class="pull-r mgr-0" id="total_code"><?php echo priceFormat($data['transport']['CODCost']); ?> đ</span></h2>
						<div class="title-line">Ghi chú giao hàng</div>
						<div class="note">
							<p>- Báo lại bên gửi nếu <b>không</b> liên hệ được người nhận</p>
							<p>- Hàng <b>không</b> chứa PIN và NAM CHÂM</p>
							<p>- Hàng <b>dễ hỏng</b>, xin nhẹ tay !</p>
							<p>- Cân năng: <?php echo priceFormat($data['transport']['Weight']); ?>g</p>
						</div>
						<!-- <h2 class="mt-15 text-center">CHO XEM HÀNG</h2> -->
					</div>
				</div>
				<div class="w33">
					<p class="mbt-10"><b>Tên sản phẩm</b> <b class="pull-r">Số Lượng</b></p>
					<div class="wrap-box-last">
						<?php foreach($listOrderProducts as $op){
							if($op['ProductKindId'] == 3){
								$listProductChilds = $this->Mproductchilds->getByProductId($op['ProductId']);
								foreach ($listProductChilds as $pc){
									if(!isset($products[$pc['ProductPartId']])) $products[$pc['ProductPartId']] = $this->Mproducts->get($pc['ProductPartId'], true, '', 'ProductName, ProductImage, ProductUnitId, Price');
									$productName = $products[$pc['ProductPartId']]['ProductName'];
									$productChildName = '';
									if($pc['ProductPartChildId'] > 0){
										if(!isset($productChilds[$pc['ProductPartChildId']])) $productChilds[$pc['ProductPartChildId']] = $this->Mproductchilds->get($pc['ProductPartChildId'], true, '', 'ProductName, ProductImage, Price');
										$productChildName = $productChilds[$pc['ProductPartChildId']]['ProductName'];
									} ?>
									<div class="item">
										<?php echo $productName; ?><?php if(!empty($productChildName)) echo ' | '.$productChildName; ?>
										<span class="pull-r"><?php echo priceFormat($op['Quantity'] * $pc['Quantity']); ?></span>
									</div>
								<?php }
							}
							else{
								if(!isset($products[$op['ProductId']])) $products[$op['ProductId']] = $this->Mproducts->get($op['ProductId'], true, '', 'ProductName, ProductImage, BarCode');
								$productName = $products[$op['ProductId']]['ProductName'];
								$productChildName = '';
								if($op['ProductChildId'] > 0){
									if(!isset($productChilds[$op['ProductChildId']])) $productChilds[$op['ProductChildId']] = $this->Mproductchilds->get($op['ProductChildId'], true, '', 'ProductName, ProductImage, BarCode');
									$productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
								}  ?>
								<div class="item">
									<?php echo $productName; ?><?php if(!empty($productChildName)) echo ' | '.$productChildName; ?>
									<span class="pull-r"><?php echo priceFormat($op['Quantity']); ?></span>
								</div>
							<?php }
						} ?>
					</div>
				</div>
			</div>
			<div class="clearfix" style="page-break-inside: always;"></div>
		</div>
	</div>
	<div class="clearfix" style="
    margin-top: 15px;"></div>
<?php endforeach; ?>
</body>
<script type="text/javascript">
	window.onload = function() { window.print(); }
</script>
</html>