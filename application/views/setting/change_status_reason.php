<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('changestatusreason/update', array('id' => 'changeStatusReasonForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Lý do thay đổi trạng thái</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyChangeStatusReason">
                            <?php
                            foreach($listChangeStatusReasons as $cr){ ?>
                                <tr id="changeStatusReason_<?php echo $cr['ChangeStatusReasonId']; ?>">
                                    <td id="changeStatusReasonName_<?php echo $cr['ChangeStatusReasonId']; ?>"><?php echo $cr['ChangeStatusReasonName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $cr['ChangeStatusReasonId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $cr['ChangeStatusReasonId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="changeStatusReasonName" name="ChangeStatusReasonName" value="" data-field="Lý do thay đổi trạng thái"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="ChangeStatusReasonId" id="changeStatusReasonId" value="0" hidden="hidden">
                                    <input type="text" id="deleteChangeStatusReasonUrl" value="<?php echo base_url('changestatusreason/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>