<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('otherservice/update', array('id' => 'otherServiceForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Dịch vụ</th>
                                <th>Mô tả</th>
                                <th>Chi phí</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOtherService">
                            <?php
                            foreach($listOtherServices as $os){ ?>
                                <tr id="otherService_<?php echo $os['OtherServiceId']; ?>">
                                    <td id="otherServiceName_<?php echo $os['OtherServiceId']; ?>"><?php echo $os['OtherServiceName']; ?></td>
                                    <td id="otherServiceDesc_<?php echo $os['OtherServiceId']; ?>"><?php echo $os['OtherServiceDesc']; ?></td>
                                    <td id="serviceCost_<?php echo $os['OtherServiceId']; ?>"><?php echo priceFormat($os['ServiceCost']); ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $os['OtherServiceId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $os['OtherServiceId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="otherServiceName" name="OtherServiceName" value="" data-field="Dịch vụ"></td>
                                <td><input type="text" class="form-control" id="otherServiceDesc" name="OtherServiceDesc" value=""></td>
                                <td><input type="text" class="form-control" id="serviceCost" name="ServiceCost" value="0"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="OtherServiceId" id="otherServiceId" value="0" hidden="hidden">
                                    <input type="text" id="deleteOtherServiceUrl" value="<?php echo base_url('otherservice/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>