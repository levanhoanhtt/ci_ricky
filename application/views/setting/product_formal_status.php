<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('productformalstatus/update', array('id' => 'productFormalStatusForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Hình thức sản phẩm</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProductFormalStatus">
                            <?php foreach($listProductFormalStatus as $p){ ?>
                                <tr id="productFormalStatus_<?php echo $p['ProductFormalStatusId']; ?>">
                                    <td id="productFormalStatusName_<?php echo $p['ProductFormalStatusId']; ?>"><?php echo $p['ProductFormalStatusName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $p['ProductFormalStatusId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $p['ProductFormalStatusId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="productFormalStatusName" name="ProductFormalStatusName" value="" data-field="Hình thức sản phẩm"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="ProductFormalStatusId" id="productFormalStatusId" value="0" hidden="hidden">
                                    <input type="text" id="deleteProductFormalStatusUrl" value="<?php echo base_url('productformalstatus/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>