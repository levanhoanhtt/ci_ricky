<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('offsettype/update', array('id' => 'offsetTypeForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Loại gửi bù</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOffsetType">
                            <?php
                            foreach($listOffsetTypes as $bt){ ?>
                                <tr id="offsetType_<?php echo $bt['OffsetTypeId']; ?>">
                                    <td id="offsetTypeName_<?php echo $bt['OffsetTypeId']; ?>"><?php echo $bt['OffsetTypeName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $bt['BankTypeId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $bt['BankTypeId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="offsetTypeName" name="OffsetTypeName" value="" data-field="Loại gửi bù"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="OffsetTypeId" id="offsetTypeId" value="0" hidden="hidden">
                                    <input type="text" id="deleteOffsetTypeUrl" value="<?php echo base_url('offsettype/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>