<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('guaranteeimagegroup/update', array('id' => 'guaranteeImageGroupForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Nhóm ảnh bảo hành</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyGuaranteeImageGroup">
                            <?php
                            foreach($listGuaranteeImageGroups as $cr){ ?>
                                <tr id="guaranteeImageGroup_<?php echo $cr['GuaranteeImageGroupId']; ?>">
                                    <td id="guaranteeImageGroupName_<?php echo $cr['GuaranteeImageGroupId']; ?>"><?php echo $cr['GuaranteeImageGroupName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $cr['GuaranteeImageGroupId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $cr['GuaranteeImageGroupId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="guaranteeImageGroupName" name="GuaranteeImageGroupName" value="" data-field="Nhóm ảnh bảo hành"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="GuaranteeImageGroupId" id="guaranteeImageGroupId" value="0" hidden="hidden">
                                    <input type="text" id="deleteGuaranteeImageGroupUrl" value="<?php echo base_url('guaranteeimagegroup/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>