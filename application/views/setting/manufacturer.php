<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('manufacturer/update', array('id' => 'manufacturerForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Hãng sản xuất</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyManufacturer">
                            <?php
                            foreach($listManufacturers as $p){ ?>
                                <tr id="manufacturer_<?php echo $p['ManufacturerId']; ?>">
                                    <td id="manufacturerName_<?php echo $p['ManufacturerId']; ?>"><?php echo $p['ManufacturerName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $p['ManufacturerId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $p['ManufacturerId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="manufacturerName" name="ManufacturerName" value="" data-field="Hãng sản xuất"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="ManufacturerId" id="manufacturerId" value="0" hidden="hidden">
                                    <input type="text" hidden="hidden" id="deleteManufacturerUrl" value="<?php echo base_url('manufacturer/delete'); ?>">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>