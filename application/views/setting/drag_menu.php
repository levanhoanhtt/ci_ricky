<?php $this->load->view('includes/header'); ?>

<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

    <div class="content-wrapper">
        <div class="container-fluid">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Edit menu</li>
            </ol>
            <?php echo form_open('dragmenu/update', 
                array('id' => 'dragMenuForm', 'method'=>"POST", 'class'=>"form-save-menu clearfix")); ?>
                <input type="hidden" name="deleted_nodes">
                <input type="hidden" name="menuId" id="menuId" value="<?php echo $menuId;?>">
                <a href="<?php echo base_url('menu'); ?>" class="hide" id="menuListUrl" class="btn btn-default"></a>
                <textarea name="menu_nodes" id="nestable-output"
                    class="form-control hide"> </textarea>
                <div class="row">
                    <div class="col-sm-9">
                        <div class="widget">
                            <div class="widget-body" style="min-height: 100px;">
                                <div class="form-group ">
                                    <label for="name"
                                        class="control-label required"> Menu
                                        name (key: <?php echo $menuEdit['SlugName'];?>)</label> 
                                    <input type="text" name="menuName" id="menuName"
                                        class="form-control" value="<?php echo $menuEdit['Name'];?>"
                                        autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row widget-menu">
                            <div class="col-sm-4">
                                <div class="panel-group" id="accordion">
                                    <div class="widget panel">
                                        <div class="widget-heading">
                                            <a data-toggle="collapse"
                                                data-parent="#accordion"
                                                href="#collapseCategories">
                                                <h4 class="widget-title">
                                                    <i class="box_img_sale"> </i>
                                                    <span>Chuyên mục sản phẩm</span> <i
                                                        class="fa fa-angle-down narrow-icon">
                                                    </i>
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseCategories"
                                            class="panel-collapse collapse">
                                            <div class="widget-body">
                                                <div class="box-links-for-menu">
                                                    <div class="the-box">
                                                        <ul class="list-item">
                                                        <?php foreach ($menuProduct as $key=>$val) {?>
                                                            <li><input
                                                                class="styled"
                                                                id="menu_id_<?php echo $val['CategoryId'];?>"
                                                                name="menu_id"
                                                                type="checkbox"
                                                                value="<?php echo $val['CategoryId'];?>"> <label
                                                                for="menu_id_<?php echo $val['CategoryId'];?>"
                                                                data-title="<?php echo $val['CategoryName'];?>"
                                                                data-related-id="<?php echo $val['CategoryId']?>"
                                                                data-type="product"><?php echo $val['CategoryName'];?></label>
                                                            </li>
                                                        <?php }?>
                                                        </ul>
    
                                                        <div class="text-right">
                                                            <div
                                                                class="btn-group btn-group-devided">
                                                                <a href="#"
                                                                    class="btn-add-to-menu btn btn-primary">
                                                                    <span
                                                                    class="text">
                                                                        <i
                                                                        class="fa fa-plus">
                                                                    </i> Add to
                                                                        menu
                                                                </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget panel">
                                        <div class="widget-heading">
                                            <a data-toggle="collapse"
                                                data-parent="#accordion"
                                                href="#collapseTags">
                                                <h4 class="widget-title">
                                                    <i class="box_img_sale"> </i>
                                                    <span>Chuyên mục tin tức</span> <i
                                                        class="fa fa-angle-down narrow-icon">
                                                    </i>
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseTags"
                                            class="panel-collapse collapse">
                                            <div class="widget-body">
                                                <div class="box-links-for-menu">
                                                    <div class="the-box">
                                                        <ul class="list-item">
                                                        <?php foreach ($menuNews as $key=>$val) {?>
                                                            <li><input
                                                                class="styled"
                                                                id="menu_id_<?php echo $val['CategoryId'];?>"
                                                                name="menu_id"
                                                                type="checkbox"
                                                                value="<?php echo $val['CategoryId'];?>"> <label
                                                                for="menu_id_<?php echo $val['CategoryId'];?>"
                                                                data-title="<?php echo $val['CategoryName'];?>"
                                                                data-related-id="<?php echo $val['CategoryId']?>"
                                                                data-type="news"><?php echo $val['CategoryName'];?></label>
                                                            </li>
                                                        <?php }?>
                                                        </ul>
    
                                                        <div class="text-right">
                                                            <div
                                                                class="btn-group btn-group-devided">
                                                                <a href="#"
                                                                    class="btn-add-to-menu btn btn-primary">
                                                                    <span
                                                                    class="text">
                                                                        <i
                                                                        class="fa fa-plus">
                                                                    </i> Add to
                                                                        menu
                                                                </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget panel">
                                        <div class="widget-heading">
                                            <a data-toggle="collapse"
                                                data-parent="#accordion"
                                                href="#collapsePages">
                                                <h4 class="widget-title"
                                                    style="">
                                                    <i class="box_img_sale"> </i>
                                                    <span>Pages</span> <i
                                                        class="fa fa-angle-down narrow-icon">
                                                    </i>
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapsePages"
                                            class="panel-collapse collapse">
                                            <div class="widget-body">
                                                <div class="box-links-for-menu">
                                                    <div class="the-box">
                                                        <ul class="list-item">
                                                        <?php foreach ($menuArticles as $key=>$val) {?>
                                                            <li><input
                                                                class="styled"
                                                                id="menu_id_<?php echo $val['ArticleId'];?>"
                                                                name="menu_id"
                                                                type="checkbox"
                                                                value="<?php echo $val['ArticleId'];?>"> <label
                                                                for="menu_id_<?php echo $val['ArticleId'];?>"
                                                                data-title="<?php echo $val['ArticleTitle'];?>"
                                                                data-related-id="<?php echo $val['ArticleId']?>"
                                                                data-type="page"><?php echo $val['ArticleTitle'];?></label>
                                                            </li>
                                                        <?php }?>
                                                        </ul>
    
                                                        <div class="text-right">
                                                            <div
                                                                class="btn-group btn-group-devided">
                                                                <a href="#"
                                                                    class="btn-add-to-menu btn btn-primary">
                                                                    <span
                                                                    class="text">
                                                                        <i
                                                                        class="fa fa-plus">
                                                                    </i> Add to
                                                                        menu
                                                                </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
    
                                    <div class="widget panel">
                                        <div class="widget-heading">
                                            <a data-toggle="collapse"
                                                data-parent="#accordion"
                                                href="#collapseCustomLink">
                                                <h4 class="widget-title">
                                                    <i class="box_img_sale"> </i>
                                                    <span>Add link</span> <i
                                                        class="fa fa-angle-down narrow-icon">
                                                    </i>
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseCustomLink"
                                            class="panel-collapse collapse">
                                            <div class="widget-body">
                                                <div class="box-links-for-menu">
                                                    <div id="external_link"
                                                        class="the-box">
                                                        <div
                                                            class="node-content">
                                                            <div
                                                                class="form-group">
                                                                <label
                                                                    for="node-title">
                                                                    Title</label>
                                                                <input
                                                                    type="text"
                                                                    required="required"
                                                                    class="form-control"
                                                                    id="node-title"
                                                                    autocomplete="false">
                                                            </div>
                                                            <div
                                                                class="form-group">
                                                                <label
                                                                    for="node-url">
                                                                    URL</label>
                                                                <input
                                                                    type="text"
                                                                    required="required"
                                                                    class="form-control"
                                                                    id="node-url"
                                                                    placeholder="http://"
                                                                    autocomplete="false">
                                                            </div>
                                                            <div
                                                                class="form-group">
                                                                <label
                                                                    for="node-icon">
                                                                    Icon</label>
                                                                <input
                                                                    type="text"
                                                                    required="required"
                                                                    class="form-control"
                                                                    id="node-icon"
                                                                    placeholder="fa fa-home"
                                                                    autocomplete="false">
                                                            </div>
                                                            <div
                                                                class="form-group">
                                                                <label
                                                                    for="node-css">
                                                                    CSS class</label>
                                                                <input
                                                                    type="text"
                                                                    required="required"
                                                                    class="form-control"
                                                                    id="node-css"
                                                                    autocomplete="false">
                                                            </div>
                                                            <div
                                                                class="form-group">
                                                                <label
                                                                    for="target">
                                                                    Target</label>
                                                                <select
                                                                    name="target"
                                                                    class="form-control select-full"
                                                                    id="target">
                                                                    <option
                                                                        value="_self">
                                                                        Open
                                                                        link
                                                                        directly</option>
                                                                    <option
                                                                        value="_blank">
                                                                        Open
                                                                        link in
                                                                        new tab</option>
                                                                </select>
                                                            </div>
    
                                                            <div
                                                                class="text-right form-group node-actions hide">
                                                                <a
                                                                    class="btn red btn-remove"
                                                                    href="#">
                                                                    Remove</a> <a
                                                                    class="btn blue btn-cancel"
                                                                    href="#">
                                                                    Cancel</a>
                                                            </div>
    
                                                            <div
                                                                class="form-group">
                                                                <div
                                                                    class="text-right add-button">
                                                                    <div
                                                                        class="btn-group">
                                                                        <a
                                                                            href="#"
                                                                            class="btn-add-to-menu btn btn-primary">
                                                                            <span
                                                                            class="text">
                                                                                <i
                                                                                class="fa fa-plus">
                                                                            </i>
                                                                                Add
                                                                                to
                                                                                menu
                                                                        </span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
    
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="widget">
                                    <div class="widget-title">
                                        <h4>
                                            <i class="box_img_sale"> </i><span>Menu
                                                structure</span>
                                        </h4>
                                    </div>
                                    <div class="widget-body">
                                        <div class="dd nestable-menu"
                                            id="nestable" data-depth="2">
                                            <ol class="dd-list">
                                                <?php foreach ($menuParentItems as $key=>$val) {?>
                                                <!-- Menu cha -->
                                                <li data-type="news" data-related-id="16" 
                                                data-title="<?php echo $val['Title']?>" data-id="0" 
                                                data-target="_self" class="dd-item dd3-item">
                                                    <div class="dd-handle dd3-handle"></div>
                                                    <div class="dd3-content">
                                                        <span class="text pull-left" data-update="title"><?php echo $val['Title']?></span>
                                                        <span class="text pull-right"><?php echo $val['Type'];?></span>
                                                        <a href="#" class="show-item-details">
                                                            <i class="fa fa-angle-down"></i>
                                                        </a>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="item-details">
                                                        <label class="pad-bot-5">
                                                            <span class="text pad-top-5 dis-inline-block" data-update="title">Title</span>
                                                            <input type="text" data-old="<?php echo $val['Title'];?>" 
                                                            value="<?php echo $val['Title'];?>" name="title" class="form-control">
                                                        </label>
                                                        <label class="pad-bot-5 dis-inline-block">
                                                            <span class="text pad-top-5" data-update="icon-font">Icon - font</span>
                                                            <input type="text" name="icon-font" class="form-control" value="<?php echo $val['IconFont'];?>">
                                                        </label>
                                                        <label class="pad-bot-10">
                                                            <span class="text pad-top-5 dis-inline-block" data-update="class">CSS class</span>
                                                            <input type="text" name="class" class="form-control" value="<?php echo $val['CSSClass'];?>">
                                                        </label>
                                                        <label class="pad-bot-10">
                                                            <span class="text pad-top-5 dis-inline-block" data-update="target">Target</span>
                                                            <div style="width: 228px; display: inline-block">
                                                                <select name="target" id="target" class="form-control select-full select2-hidden-accessible" 
                                                                tabindex="-1" aria-hidden="true">
                                                                    <option value="_self" <?php if($val['Target']=='_self') {echo "selected";}?>>Open link directly</option>
                                                                    <option value="_blank" <?php if($val['Target']=='_blank') {echo "selected";}?>>Open link in new tab</option>
                                                                </select>
                                                                <span class="select2 select2-container select2-container--default hide" 
                                                                dir="ltr" style="width: 100%;">
                                                                    <span class="selection">
                                                                        <span class="select2-selection select2-selection--single" 
                                                                        role="combobox" aria-haspopup="true" aria-expanded="false" 
                                                                        tabindex="0" aria-labelledby="select2-target-container">
                                                                            <span class="select2-selection__rendered" id="select2-target-container" 
                                                                            title="Open link directly">Open link directly</span>
                                                                            <span class="select2-selection__arrow" role="presentation">
                                                                                <b role="presentation"></b>
                                                                            </span>
                                                                        </span>
                                                                    </span>
                                                                    <span class="dropdown-wrapper" aria-hidden="true"></span>
                                                                </span>
                                                            </div>
                                                        </label>
                                                        <div class="text-right">
                                                            <a class="btn red btn-remove" href="#">Remove</a>
                                                            <a class="btn blue btn-cancel" href="#">Cancel</a>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <!-- Menu con -->
                                                    <?php if (isset($groupChildrenItems[$val['MenuItemId']])) :?>
                                                    <ol class="dd-list">
                                                        <?php foreach ($groupChildrenItems[$val['MenuItemId']] as $kch=>$vch) {?>
                                                        <li data-type="<?php echo $vch['Type'];?>" data-related-id="1" data-title="<?php echo $vch['Title'];?>" 
                                                        data-id="0" data-target="_self" class="dd-item dd3-item">
                                                            <div class="dd-handle dd3-handle"></div>
                                                            <div class="dd3-content">
                                                                <span class="text pull-left" data-update="title"><?php echo $vch['Title'];?></span>
                                                                <span class="text pull-right"><?php echo $vch['Type'];?></span>
                                                                <a href="#" class="show-item-details">
                                                                    <i class="fa fa-angle-down"></i>
                                                                </a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="item-details">
                                                                <label class="pad-bot-5">
                                                                    <span class="text pad-top-5 dis-inline-block" data-update="title">Title</span>
                                                                    <input type="text" data-old="<?php echo $vch['Title'];?>" value="<?php echo $vch['Title'];?>" name="title" class="form-control">
                                                                </label>
                                                                <label class="pad-bot-5 dis-inline-block">
                                                                    <span class="text pad-top-5" data-update="icon-font">Icon - font</span>
                                                                    <input type="text" name="icon-font" class="form-control" data-old="<?php echo $vch['IconFont'];?>" value="<?php echo $vch['IconFont'];?>">
                                                                </label>
                                                                <label class="pad-bot-10">
                                                                    <span class="text pad-top-5 dis-inline-block" data-update="class">CSS class</span>
                                                                    <input type="text" name="class" class="form-control" data-old="<?php echo $vch['CSSClass'];?>" value="<?php echo $vch['CSSClass'];?>">
                                                                </label>
                                                                <label class="pad-bot-10">
                                                                    <span class="text pad-top-5 dis-inline-block" data-update="target">Target</span>
                                                                    <div style="width: 228px; display: inline-block">
                                                                        <select name="target" id="target" 
                                                                        class="form-control select-full select2-hidden-accessible" 
                                                                        tabindex="-1" aria-hidden="true">
                                                                            <option value="_self" <?php if ($vch['Target']=="_self") {echo "selected";}?>>Open link directly</option>
                                                                            <option value="_blank" <?php if ($vch['Target']=="_blank") {echo "selected";}?>>Open link in new tab</option>
                                                                        </select>
                                                                    </div>
                                                                </label>
                                                                <div class="text-right">
                                                                    <a class="btn red btn-remove" href="#">Remove</a>
                                                                    <a class="btn blue btn-cancel" href="#">Cancel</a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                        <?php }?>
                                                    </ol>
                                                    <?php endif;?>
                                                </li>
                                                <?php }?>
                                            </ol>
    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 right-sidebar">
                        <div
                            class="widget meta-boxes form-actions form-actions-default action-horizontal">
                            <div class="widget-title">
                                <h4>
                                    <span>Publish</span>
                                </h4>
                            </div>
                            <div class="widget-body">
                                <div class="btn-set">
                                    <span class="btn btn-info submit-menu">
                                        <i class="fa fa-save"> </i> Save
                                    </span>
                                    <span class="btn btn-success submit-menu-edit">
                                        <i class="fa fa-check-circle"> </i> Save
                                        &amp; Edit
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div id="waypoint"></div>
                        <div class="form-actions form-actions-fixed-top hidden" style="margin-left: 30px;">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a
                                    href="#">
                                        Dashboard</a></li>
                                <li class="breadcrumb-item active">Edit menu</li>
                            </ol>
    
    
                            <div class="btn-set">
                                <span class="btn btn-info submit-menu">
                                    <i class="fa fa-save"> </i> Save
                                </span>
                                <span class="btn btn-success submit-menu-edit">
                                    <i class="fa fa-check-circle"> </i> Save
                                    &amp; Edit
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /page content -->
    <div class="clearfix"></div>
    <!-- /page container -->

<?php $this->load->view('includes/footer'); ?>
