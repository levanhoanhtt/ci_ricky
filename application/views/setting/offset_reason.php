<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('offsetreason/update', array('id' => 'offsetReasonForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Lý do gửi bù</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOffsetReason">
                            <?php
                            foreach($listOffsetReasons as $bt){ ?>
                                <tr id="offsetReason_<?php echo $bt['OffsetReasonId']; ?>">
                                    <td id="offsetReasonName_<?php echo $bt['OffsetReasonId']; ?>"><?php echo $bt['OffsetReasonName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $bt['OffsetReasonId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $bt['OffsetReasonId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="offsetReasonName" name="OffsetReasonName" value="" data-field="Lý do gửi bù"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="OffsetReasonId" id="offsetReasonId" value="0" hidden="hidden">
                                    <input type="text" id="deleteOffsetReasonUrl" value="<?php echo base_url('offsetreason/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>