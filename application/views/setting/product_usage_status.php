<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('productusagestatus/update', array('id' => 'productUsageStatusForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Tình trạng sử dụng</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProductUsageStatus">
                            <?php foreach($listProductUsageStatus as $p){ ?>
                                <tr id="productUsageStatus_<?php echo $p['ProductUsageStatusId']; ?>">
                                    <td id="productUsageStatusName_<?php echo $p['ProductUsageStatusId']; ?>"><?php echo $p['ProductUsageStatusName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $p['ProductUsageStatusId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $p['ProductUsageStatusId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="productUsageStatusName" name="ProductUsageStatusName" value="" data-field="Tình trạng sử dụng"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="ProductUsageStatusId" id="productUsageStatusId" value="0" hidden="hidden">
                                    <input type="text" id="deleteProductUsageStatusUrl" value="<?php echo base_url('productusagestatus/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>