<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('paymenttype/update', array('id' => 'paymentTypeForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Phương thức thanh toán</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyPaymentType">
                            <?php
                            foreach($listPaymentTypes as $bt){ ?>
                                <tr id="paymentType_<?php echo $bt['PaymentTypeId']; ?>">
                                    <td id="paymentTypeName_<?php echo $bt['PaymentTypeId']; ?>"><?php echo $bt['PaymentTypeName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $bt['PaymentTypeId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $bt['PaymentTypeId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="paymentTypeName" name="PaymentTypeName" value="" data-field="Phương thức thanh toán"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="PaymentTypeId" id="paymentTypeId" value="0" hidden="hidden">
                                    <input type="text" id="deletePaymentTypeUrl" value="<?php echo base_url('paymenttype/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>