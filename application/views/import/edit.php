<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <?php if($canEdit){ ?>
                        <li><button class="btn btn-primary submit" type="button">Lưu</button></li>
                        <li><button class="btn btn-danger btnCancel" type="button">Hủy phiếu</button></li>
                    <?php } ?>
                    <li><a href="<?php echo base_url('import'); ?>" id="importListUrl" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($importId > 0){ ?>
                    <?php echo form_open('import/update', array('id' => 'importForm')); ?>
                    <div class="row">
                        <div class="col-sm-8 no-padding">
                            <?php $products = array();
                            $productChilds = array(); ?>
                            <div class="box box-default padding15">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Sảm phẩm mới</h3>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive no-padding divTable">
                                        <table class="table table-hover table-bordered">
                                            <thead class="theadNormal">
                                            <tr>
                                                <th>Sản phẩm</th>
                                                <th class="text-center" style="width: 60px;">Đơn vị</th>
                                                <th class="text-center" style="width: 130px;">SKU</th>
                                                <th class="text-center" style="width: 120px;">Bảo hành</th>
                                                <th class="text-center" style="width: 120px;">Bảo hành NCC</th>
                                                <th style="width: 85px;">Số lượng</th>
                                                <?php if($canActive){ ?>
                                                <th style="width: 150px;">ĐG chưa thuế</th>
                                                <th style="width: 85px;">VAT (%)</th>
                                                <th style="width: 150px;">Thành tiền</th>
                                                <?php } ?>
                                                <?php if($canEdit){ ?><th style="width: 30px;"></th><?php } ?>
                                            </tr>
                                            </thead>
                                            <tbody id="tbodyProduct">
                                            <?php foreach ($listImportProduct as $ip){
                                                    if(!isset($products[$ip['ProductId']])) $products[$ip['ProductId']] = $this->Mproducts->get($ip['ProductId'], true, '', 'ProductName, ProductImage, BarCode, ProductUnitId, GuaranteeMonth');
                                                    $productName = $products[$ip['ProductId']]['ProductName'];
                                                    $productImage = $products[$ip['ProductId']]['ProductImage'];
                                                    $barCode = $products[$ip['ProductId']]['BarCode'];
                                                    $productUnitId = $products[$ip['ProductId']]['ProductUnitId'];
                                                    $guaranteeMonth = $products[$ip['ProductId']]['GuaranteeMonth'];
                                                    $productChildName = '';
                                                    if($ip['ProductChildId'] > 0){
                                                        if(!isset($productChilds[$ip['ProductChildId']])) $productChilds[$ip['ProductChildId']] = $this->Mproductchilds->get($ip['ProductChildId'], true, '', 'ProductName, ProductImage, BarCode, GuaranteeMonth');
                                                        $productChildName = $productChilds[$ip['ProductChildId']]['ProductName'];
                                                        $productImage = $productChilds[$ip['ProductChildId']]['ProductImage'];
                                                        $barCode = $productChilds[$ip['ProductChildId']]['BarCode'];
                                                        $guaranteeMonth = $productChilds[$ip['ProductChildId']]['GuaranteeMonth'];
                                                    }
                                                    if(empty($productImage)) $productImage = NO_IMAGE; ?>
                                                    <tr data-id="<?php echo $ip['ProductId']; ?>" data-child="<?php echo $ip['ProductChildId']; ?>">
                                                        <td>
                                                            <img src="<?php echo PRODUCT_PATH.$productImage; ?>" class="productImg">
                                                            <a href="<?php echo base_url('product/edit/'.$ip['ProductId']); ?>" class="light-dark" target="_blank">
                                                                <?php echo $productName;
                                                                if(!empty($productChildName)) echo ' ('.$productChildName.')'; ?>
                                                            </a>
                                                        </td>
                                                        <td class="text-center"><?php echo $this->Mconstants->getObjectValue($listProductUnits, 'ProductUnitId', $productUnitId, 'ProductUnitName'); ?></td>
                                                        <td class="text-center"><?php echo $barCode; ?></td>
                                                        <td class="text-center"><?php echo $guaranteeMonth; ?> tháng</td>
                                                        <td class="text-center"><input class="form-control guarantee_month" value="<?php echo $ip['GuaranteeMonth']; ?>"></td>
                                                        <td><input class="form-control quantity cost" value="<?php echo priceFormat($ip['Quantity']); ?>"></td>
                                                        <?php if($canActive){ ?>
                                                        <td><input class="form-control price cost" value="<?php echo priceFormat($ip['Price']); ?>"></td>
                                                        <td><input class="form-control percent cost" value="<?php echo $ip['VAT']; ?>"></td>
                                                        <td>
                                                            <?php $sumCost = $ip['Quantity'] * $ip['Price'];
                                                            $sumTax = ceil($sumCost * $ip['VAT'] / 100); ?>
                                                            <input class="form-control sumPrice text-right" disabled value="<?php echo priceFormat($sumCost + $sumTax); ?>">
                                                            <input type="text" hidden="hidden" class="sumCost" value="<?php echo priceFormat($sumCost); ?>">
                                                            <input type="text" hidden="hidden" class="sumTax" value="<?php echo priceFormat($sumTax); ?>">
                                                        </td>
                                                        <?php } ?>
                                                        <?php if($canEdit){ ?><td class="text-right"><a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a></td><?php } ?>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <?php if($canEdit){ ?>
                                    <div class="border-top-title-main">
                                        <div class="clearfix">
                                            <div class="box-search-advance product">
                                                <div>
                                                    <input type="text" class="form-control textbox-advancesearch" id="txtSearchProduct" placeholder="Tìm kiếm sản phẩm">
                                                </div>
                                                <div class="panel panel-default" id="panelProduct">
                                                    <div class="panel-body" style="width:100%;">
                                                        <div class="list-search-data">
                                                            <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                            <div>
                                                                <div class="form-group pull-right" style="width: 300px;">
                                                                    <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', 0, true, 'Nhóm sản phẩm', ' select2'); ?>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="table-responsive no-padding divTable">
                                                                <table class="table table-hover table-bordered">
                                                                    <thead class="theadNormal">
                                                                    <tr>
                                                                        <th style="width: 100px;">Ảnh</th>
                                                                        <th>Sản phẩm</th>
                                                                        <th style="width: 100px;">Mã sản phẩm</th>
                                                                        <th style="width: 100px;">Giá</th>
                                                                        <th style="width: 100px;">Bảo hành</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody id="tbodyProductSearch"></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel-footer">
                                                        <div class="btn-group pull-right">
                                                            <button type="button" class="btn btn-default" id="btnPrevProduct"><i class="fa fa-chevron-left"></i></button>
                                                            <button type="button" class="btn btn-default" id="btnNextProduct"><i class="fa fa-chevron-right"></i></button>
                                                            <input type="text" hidden="hidden" id="pageIdProduct" value="1">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Ghi chú phiếu nhập kho</label>
                                                <textarea class="form-control" rows="2" id="comment"><?php echo $import['Comment']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-6"<?php if(!$canActive) echo ' style="display: none;";'; ?>>
                                            <div class="row tb-sead">
                                                <div class="col-sm-6">Tổng chưa thuế</div>
                                                <div class="col-sm-6 text-right"><span id="totalPrice">0</span> đ</div>
                                                <div class="col-sm-6">Tiền thuế (VAT)</div>
                                                <div class="col-sm-6 text-right"><span id="totalTax">0</span> đ</div>
                                                <div class="col-sm-6" style="line-height: 34px;">Cấu hình vận chuyển</div>
                                                <div class="col-sm-6 text-right">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control cost1" id="transportCost" value="<?php echo priceFormat($import['TransportCost']); ?>">
                                                        <span class="input-group-addon"> đ</span>
                                                    </div>
                                                </div>
                                                <div class="divConfigExpand" id="divService">
                                                    <?php foreach($listImportServices as $is){ ?>
                                                        <div class="item" data-id="<?php echo $is['ImportServiceId']; ?>">
                                                            <div class="col-sm-6" style="clear: both">
                                                                <a href="javascript:void(0)" class="aExpand"><?php echo $this->Mconstants->getObjectValue($listOtherServices, 'OtherServiceId', $is['ImportServiceId'], 'OtherServiceName'); ?></a>
                                                            </div>
                                                            <div class="col-sm-6 text-right"><span id="spanOtherCost_<?php echo $is['ImportServiceId']; ?>" class="spanOtherCost"><?php echo priceFormat($is['ServiceCost']); ?></span> đ</div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <hr class="hr-ths" style="margin-top: 9px;">
                                            <div class="row mb10">
                                                <div class="col-sm-6">Tổng tiền</div>
                                                <div class="col-sm-6 text-right"><span id="orderCost"></span> đ</div>
                                            </div>
                                            <div class="row mb10">
                                                <div class="col-sm-6">Triết khấu</div>
                                                <div class="col-sm-6 text-right">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control cost1" id="discountCost" value="<?php echo priceFormat($import['DiscountCost']); ?>">
                                                        <span class="input-group-addon"> đ</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb10">
                                                <div class="col-sm-6">Tổng cần thanh toán</div>
                                                <div class="col-sm-6 text-right"><span id="totalCost"></span> đ</div>
                                            </div>
                                            <div class="row mb10">
                                                <div class="col-sm-6" style="line-height: 34px;">Đã thanh toán</div>
                                                <div class="col-sm-6 text-right">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control cost1" id="paymentCost" value="<?php echo priceFormat($import['PaymentCost']); ?>">
                                                        <span class="input-group-addon"> đ</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">Nợ lại</div>
                                                <div class="col-sm-6 text-right bold light-dark2 fs-16">
                                                    <span id="ownCost"></span> đ
                                                </div>
                                                <div class="col-sm-12" style="margin-top: 10px;">
                                                    <p class="pull-right"><a href="javascript:void(0)" class="aExpand">Mở rộng &gt;&gt;</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box box-default padding15">
                                <style>#tbodyProductErrors tr{cursor: pointer;}</style>
                                <div class="box-header with-border">
                                    <h3 class="box-title">Sản phẩm có vấn đề</h3>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive no-padding divTable">
                                        <table class="table table-hover table-bordered">
                                            <thead class="theadNormal">
                                            <tr>
                                                <th>Sản phẩm</th>
                                                <th class="text-center" style="width: 60px;">Đơn vị</th>
                                                <th class="text-center" style="width: 130px;">SKU</th>
                                                <th style="width: 85px;">Số lượng</th>
                                                <th style="width: 150px;">Ghi chú</th>
                                                <th style="width: 5px;"></th>
                                            </tr>
                                            </thead>
                                            <tbody id="tbodyProductErrors">
                                                <?php foreach ($listImportProductErrors as $ip){
                                                        if(!isset($products[$ip['ProductId']])) $products[$ip['ProductId']] = $this->Mproducts->get($ip['ProductId'], true, '', 'ProductName, ProductImage, BarCode, ProductUnitId, GuaranteeMonth');
                                                        $productName = $products[$ip['ProductId']]['ProductName'];
                                                        $productImage = $products[$ip['ProductId']]['ProductImage'];
                                                        $productChildName = '';
                                                        if($ip['ProductChildId'] > 0){
                                                            if(!isset($productChilds[$ip['ProductChildId']])) $productChilds[$ip['ProductChildId']] = $this->Mproductchilds->get($ip['ProductChildId'], true, '', 'ProductName, ProductImage, BarCode, GuaranteeMonth');
                                                            $productChildName = $productChilds[$ip['ProductChildId']]['ProductName'];
                                                            $productImage = $productChilds[$ip['ProductChildId']]['ProductImage'];
                                                        }
                                                        if(empty($productImage)) $productImage = NO_IMAGE; ?>
                                                        <tr data-id="<?php echo $ip['ProductId']; ?>" data-child="<?php echo $ip['ProductChildId']; ?>">
                                                            <td>
                                                                <img src="<?php echo PRODUCT_PATH.$productImage; ?>" class="productImg">
                                                                <a href="<?php echo base_url('product/edit/'.$ip['ProductId']); ?>" class="light-dark" target="_blank">
                                                                    <?php echo $productName;
                                                                    if(!empty($productChildName)) echo ' ('.$productChildName.')'; ?>
                                                                </a>
                                                            </td>
                                                            <td class="text-center"><?php echo $this->Mconstants->getObjectValue($listProductUnits, 'ProductUnitId', $productUnitId, 'ProductUnitName'); ?></td>
                                                            <td class="text-center"><?php echo $barCode; ?></td>
                                                            <td><input class="form-control quantity cost" value="<?php echo priceFormat($ip['Quantity']); ?>"></td>
                                                            <td><input class="form-control comment" value="<?php echo $ip['Comment']; ?>"></td>
                                                            <?php if($canEdit){ ?><td class="text-right"><a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a></td><?php } ?>
                                                        </tr>
                                                    <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <?php if($canEdit){ ?>
                                    <div class="border-top-title-main">
                                        <div class="clearfix">
                                            <div class="box-search-advance product">
                                                <div>
                                                    <input type="text" class="form-control textbox-advancesearch" id="txtSearchProductError" placeholder="Tìm kiếm sản phẩm">
                                                </div>
                                                <div class="panel panel-default" id="panelProductError">
                                                    <div class="panel-body" style="width:100%;">
                                                        <div class="list-search-data">
                                                            <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                            <div>
                                                                <div class="form-group pull-right" style="width: 300px;">
                                                                    <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryIdError', 0, true, 'Nhóm sản phẩm', ' select2'); ?>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="table-responsive no-padding divTable">
                                                                <table class="table table-hover table-bordered">
                                                                    <thead class="theadNormal">
                                                                    <tr>
                                                                        <th style="width: 100px;">Ảnh</th>
                                                                        <th>Sản phẩm</th>
                                                                        <th style="width: 100px;">SKU</th>
                                                                        <th style="width: 100px;">Giá</th>
                                                                        <th style="width: 100px;">Bảo hành</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody id="tbodyProductSearchError"></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel-footer">
                                                        <div class="btn-group pull-right">
                                                            <button type="button" class="btn btn-default" id="btnPrevProductError"><i class="fa fa-chevron-left"></i></button>
                                                            <button type="button" class="btn btn-default" id="btnNextProductError"><i class="fa fa-chevron-right"></i></button>
                                                            <input type="text" hidden="hidden" id="pageIdProductError" value="1">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php $this->load->view('includes/action_logs_new'); ?>
                        </div>
                        <div class="col-sm-4">
                            <div class="box box-default padding20">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Người giao hàng</h3>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label class="control-label">Họ tên</label>
                                        <input type="text" class="form-control" id="deliverName" value="<?php echo $import['DeliverName']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Số điện thoại</label>
                                        <input type="text" class="form-control" id="deliverPhone" value="<?php echo $import['DeliverPhone']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="box box-default classify padding20">
                                <div class="form-group">
                                    <label class="control-label light-blue">Nhà cung cấp</label>
                                    <?php $this->Mconstants->selectObject($listSuppliers, 'SupplierId', 'SupplierName', 'SupplierId', $import['SupplierId'], false, '--Chọn nhà cung cấp--', ' select2 hmdrequiredNumber', ' data-field="Nhà cung cấp"'); ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label light-blue">Cơ sở nhập</label>
                                    <?php $this->Mconstants->selectObject($listStores, 'StoreId', 'StoreName', 'StoreId',  $import['StoreId'], false, '--Chọn cơ sở--', ' select2 hmdrequiredNumber', ' data-field="Cơ sở nhập"'); ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label light-blue">Trạng thái</label>
                                    <?php if($canActive) $this->Mconstants->selectConstants('importStatus', 'ImportStatusId', $import['ImportStatusId'], false, '', '', $canEdit ? '' : ' disabled');
                                    elseif(!$canEdit)  $this->Mconstants->selectConstants('importStatus', 'ImportStatusId', $import['ImportStatusId'], false, '', '', ' disabled');
                                    else{ ?>
                                        <select class="form-control" name="ImportStatusId" id="importStatusId">
                                            <option value="5"<?php if($import['ImportStatusId'] == 5) echo ' selected="selected"'; ?>>Chờ xử lý</option>
                                            <option value="1"<?php if($import['ImportStatusId'] == 1) echo ' selected="selected"'; ?>>Chờ duyệt</option>
                                        </select>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="box box-default more-tabs padding20">
                                <div class="form-group">
                                    <label class="control-label">Nhãn (cách nhau bởi dấu phẩy)</label>
                                    <input type="text" class="form-control" id="tags">
                                </div>
                                <p class="light-gray">Bạn có thể chọn những tag đã được sử dụng</p>
                                <div class="clearfix">
                                    <?php foreach ($listTags as $t) { ?>
                                        <a href="javascript:void(0)" class="ntags"><?php echo $t['TagName']; ?></a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-inline pull-right margin-right-10">
                        <?php if($canEdit){ ?>
                            <li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
                            <li><button class="btn btn-danger btnCancel" type="button">Hủy phiếu</button></li>
                        <?php } ?>
                        <li><a href="<?php echo base_url('import'); ?>" class="btn btn-default">Đóng</a></li>
                        <input type="text" hidden="hidden" id="getProductDetailUrl" value="<?php echo base_url('api/product/get'); ?>">
                        <input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getList'); ?>">
                        <input type="text" hidden="hidden" id="productKindIdDifferent" value="3">
                        <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('import/changeStatus'); ?>">
                        <input type="text" hidden="hidden" id="importId" value="<?php echo $importId; ?>">
                        <input type="text" hidden="hidden" id="canEdit" value="<?php echo $canEdit ? 1 : 0; ?>">
                        <input type="text" hidden="hidden" id="canActive" value="<?php echo $canActive ? 1 : 0; ?>">
                        <input type="text" hidden="hidden" id="debitOtherTypeId" value="<?php echo DEBIT_OTHER_TYPE_ID; ?>">
                        <input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
                        <?php foreach($tagNames as $tagName){ ?>
                            <input type="text" hidden="hidden" class="tagName" value="<?php echo $tagName; ?>">
                        <?php } ?>
                    </ul>
                    <?php echo form_close(); ?>
                    <?php $this->load->view('import/modal'); ?>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>