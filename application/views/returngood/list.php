<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl">
                    <li><a href="<?php echo base_url('returngood/add'); ?>" class="btn btn-primary">Tạo Đơn hoàn hàng về</a></li>
                    <li><button class="btn btn-default">Xuất dữ liệu</button></li>
                </ul>
            </section>
            <section class="content  upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả Đơn hoàn hàng về</a></li>
                        <?php foreach($listFilters as $f){ ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php $orderStatus = $this->Mconstants->orderStatus; ?>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả đơn hàng theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="order_status">Trạng thái đơn hàng</option>
                                        <option value="order_status_transport">Trạng thái giao hàng</option>
                                        <option value="order_create">Thời điểm tạo đơn</option>
                                        <option value="order_store">Cơ sở nhập hàng</option>
                                        <option value="order_tag">Tag</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 order_status order_status_transport order_store">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <!-- group_money group_order field đây là các filter được sử dụng tùy chọn này-->
                                    <select class="form-control order_status block-display">
                                        <?php foreach($orderStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_status_transport none-display">
                                        <?php foreach($this->Mconstants->returnGoodTypes as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>

                                    <select class="form-control order_create none-display" id="select_operator_date">
                                        <option value="between">trong khoảng</option>
                                        <option value="<">trước</option>
                                        <option value="=">bằng</option>
                                        <option value=">">sau</option>
                                    </select>
                                    <select class="form-control order_store none-display">
                                        <?php foreach($listStores as $s) :?>
                                            <option value="<?php echo $s['StoreId']; ?>"><?php echo $s['StoreName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <select class="form-control order_tag none-display mb10">
                                        <option value="in">chứa</option>
                                        <option value="not in">không chứa</option>
                                    </select>
                                    <div class="order_tag none-display">
                                        <select class="form-control select2 order_tag ">
                                            <?php foreach($listTags as $key => $v){ ?>
                                                <option value="<?php echo $v['TagId']; ?>"><?php echo $v['TagName']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group block-display mb10">
                                    <input class="form-control datepicker order_create none-display" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart">
                                    <input class="form-control datepicker order_create none-display" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd">
                                </div>
                                <div class="form-group block-display widthauto">
                                    <!-- data-href : Đây là link gọi để filter mỗi trang sẽ có 1 link khác nhau -->
                                    <button id="btn-filter" data-href="<?php echo base_url('returngood/searchByFilter'); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-sm-3 fix-width-200">
                                <select class="form-control" id="selectAction" style="display: none;">
                                <option value="">Chọn hành động</option>
                                <option value="add_tags">Thêm nhãn</option>
                                <option value="delete_tags">Bỏ nhãn</option>
                            </select>
                            </div>
                            <div class="col-sm-2 fix-width-200">
                                <select class="form-control" id="selectData" style="display: none;"></select>
                            </div>
                            <?php if(isset($paggingHtml)){ ?>
                                <div class="box-tools pull-right">
                                    <?php echo $paggingHtml; ?>
                                </div>
                            <?php } ?>
                        </div>
                        
                        <div class="box-body table-responsive no-padding divTable">
                            <table class="table new-style table-hover table-bordered" id="table-data">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                    <th>Mã đơn</th>
                                    <th>Khách hàng</th>
                                    <th>Cơ sở nhập hàng</th>
                                    <th class="text-center">Trạng thái</th>
                                    <th class="text-center">Loại</th>
                                    <th>Ngày tạo</th>
                                </tr>
                                </thead>
                                <tbody id="tbodyReturnGoods">
                                </tbody>
                            </table>
                        </div>
                        <input type="text" hidden="hidden" id="urlEditCustomer" value="<?php echo base_url('customer/edit')?>" >
                        <input type="text" hidden="hidden" id="urlEditReturnGood" value="<?php echo base_url('returngood/edit')?>" >
                        <input type="text" hidden="hidden" id="itemTypeId" value="14">
                    </div>
                </div>
                <?php $this->load->view('includes/modal/tag'); ?>
                <?php $this->load->view('includes/modal/filter'); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>