<div class="modal fade" id="modalCustomerAddress" role="dialog" aria-labelledby="modalCustomerAddress">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Địa chỉ giao hàng</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Họ tên <span class="required">*</span></label>
                            <input type="text" class="form-control hmdrequired" id="customerName" data-field="Họ Tên">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="text" class="form-control" id="customerEmail">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Số điện thoại <span class="required">*</span></label>
                            <input type="text" class="form-control hmdrequired" id="customerPhone" data-field="Số điện thoại">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group ">
                            <label class="control-label">Quốc gia </label>
                            <?php $this->Mconstants->selectObject($listCountries, 'CountryId', 'CountryName', 'CustomerCountryId', 232, false, '', ' select2'); ?>
                        </div>
                    </div>
                </div>
                <div class="row VNon">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Tỉnh/ Thành phố</label>
                            <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'CustomerProvinceId', 0, false, '', ' select2'); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Quận huyện</label>
                            <?php echo $this->Mdistricts->selectHtml(0, 'CustomerDistrictId', $listDistricts); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 VNon">
                        <div class="form-group">
                            <label class="control-label">Phường / Xã </label>
                            <?php echo $this->Mwards->selectHtml(0, 'CustomerWardId'); ?>
                        </div>
                    </div>
                    <div class="col-sm-6 VNoff" style="display: none;">
                        <div class="form-group">
                            <label class="control-label">ZipCode</label>
                            <input type="text" id="customerZipCode" class="form-control" value="">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Địa chỉ <span class="required">*</span></label>
                    <input id="customerAddress" class="form-control hmdrequired" value="" data-field="Địa chỉ">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary" id="btnUpdateCustomerAddress">Thay đổi</button>
                <input type="text" hidden="hidden" id="insertCustomerAddressUrl" value="<?php echo base_url('api/customer/insertAddress'); ?>">
            </div>
        </div>
    </div>
</div>