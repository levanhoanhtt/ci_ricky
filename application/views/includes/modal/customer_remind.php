<div class="modal fade" id="modalRemind" tabindex="-1" role="dialog" aria-labelledby="modalRemind">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-clock-o"></i> Thêm nhắc nhở</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-lable">Cập nhật tình hình hiện tại <span class="required">*</span></label>
                    <input type="text" class="form-control hmdrequired" id="remindComment"   placeholder="Cập nhật tình hình hiện tại" data-field="Cập nhật tình hình hiện tại">
                </div>
                <div class="form-group">
                    <label class="control-lable">Thời điểm cần xử lý <span class="required">*</span></label>
                    <div class="input-group">
                        <div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-calendar"></i></span>
						</div>
	                    <input type="text" class="form-control datetimepicker hmdrequired" id="remindDate" name="RemindDate" value="" autocomplete="off" data-field="Thời điểm cần xử lý">
	                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" id="btnAddRemind">Tạo nhắc nhở</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <input type="text" hidden="hidden" id="insertRemindUrl" value="<?php echo base_url('customer/updateRemind'); ?>">
            </div>
        </div>
    </div>
</div>