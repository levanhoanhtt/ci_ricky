<div class="modal fade" id="modalAddPosition" tabindex="-1" role="dialog" aria-labelledby="modalAddPosition">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 968px!important; left: 0;right: 0;margin: auto;">
            <div class="modal-header">
                <h4 class="modal-title">Cập nhật vị trí</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="radio-group col-sm-12">
                        <span class="item"><input type="radio" name="radioaddedit" class="iCheckAddPosition iCheck" value="1" checked> Nhập tên</span>
                        <br><br>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <input type="text" name="name" class="form-control hmdrequired" id="text-update-add-name" placeholder="Nhập tên vị trí" data-field="Nhập tên vị trí" value="">
                            </div>
                        </div>
                        <br>
                        <span class="item"><input type="radio" name="radioaddedit" class="iCheckAddPosition iCheck" value="2"> Nhập từ vị trí đã có</span>
                        <br>
                        <br>
                    </div>
                </div>
                <div class="row">
                    <!--<div class="col-sm-12">-->
                        <div id="chart-container4" class="col-sm-12 disabled">
                            <input type="hidden" id="selected-node"/>
                            <input type="hidden" id="check-children"/>
                        </div>
                    <!--</div>-->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-update-add-name" class="btn btn-border btn-gradient-bluesky waves-effect waves-light">Cập nhật</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAdddDepartment" tabindex="-1" role="dialog" aria-labelledby="modalAdddDepartment">
    <div class="modal-dialog">
        <div class="modal-content" style="max-width: 1200px!important; left: 0;right: 0;margin: auto;">
            <div class="modal-header">
                <h4 class="modal-title">Xem cơ cấu tổ chức</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-position-to-department">
                            <section class="col-sm-12">
                                <div class="row">
                                    <div class="col-12">
                                        <div id="chart-container3" class="col-md-12">
                                            <input type="hidden" id="selected-node"/>
                                            <input type="hidden" id="check-children"/>
                                            <input type="hidden" id="id-department"/>
                                            <input type="text" hidden="hidden" id="departmentId" value="">
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" value="<?= base_url('api/department/updateDepartment')?>" id="updateDepartmentUrl">
            <input type="hidden" value="<?= base_url('api/ceo/showFloor')?>" id="showFloorUrl">
            <input type="hidden" value="<?= base_url('api/ceo/saveLeadership')?>" id="saveLeadershipUrl">
            <input type="hidden" value="<?= base_url('api/ceo/removeLeadership')?>" id="removeLeadershipUrl">
        </div>
    </div>
</div>

<!--<div class="modal CEO-modal fade" id="modalAddDepartmentType" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="modalAddDepartmentType" aria-hidden="true">
    <div class="modal-dialog">
        <div class="mblock-content">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h5 class="modal-title">Thêm tầng</h5>
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group-border">
                            <input class="form-control" value="" id="departmentTypeName" placeholder="Nhập vào tên tầng">
                        </div>
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <button type="button" class="btn btn-border button btn-gradient-bluesky waves-effect waves-light" id="btnUpdateDepartmentType">Lưu</button>
                <input type="hidden" value="<?php /*echo base_url('api/departmenttype/update')*/?>" id="updateDepartmentTypeUrl">
            </div>
        </div>
    </div>
</div>-->

<div class="modal fade" id="modalAddUser" tabindex="-1" role="dialog" aria-labelledby="modalAddUser">
    <div class="modal-dialog">
        <div class="modal-content" style="max-width: 1200px!important; left: 0;right: 0;margin: auto;">
            <div class="modal-header">
                <h5 class="modal-title">Cập nhật nhân viên vào vị trí</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="userByPosition">
                    <div class="form-group-border">
                        <label class="control-label normal">
                            Danh sách nhân viên đang đảm nhiệm (<span class="count_users" data-num="">0</span>)
                        </label>
                    </div>
                    <table class="table new-style table-hover table-bordered" id="table-data-user-by-position">
                        <thead>
                            <tr>
                                <th>Mã nhân viên</th>
                                <th>Tên nhân viên</th>
                                <th>SĐT</th>
                                <th class="text-center">Xóa</th>
                            </tr>
                        </thead>
                        <tbody id="tbodyListUserPosition">
                        </tbody>
                    </table>
                </div>
                <hr style="background-color: #0babfe">
                <div class="position-users">
                    <div class="form-group-border">
                        <label class="control-label normal">
                            Thêm nhân viên vào vị trí từ danh sách nhân viên
                            <a id="btn-modal-add-user" href="javascript:void(0)">Thêm nhân viên mới <i class="fa fa-plus"></i></a>
                        </label>
                    </div>
                    <div id="divTableUser" style="height: 300px;overflow-y: scroll;">
                        <table class="table new-style table-hover table-bordered" id="table-data-user">
                            <thead>
                                <tr>
                                    <th>Mã nhân viên</th>
                                    <th>Tên nhân viên</th>
                                    <th>SĐT</th>
                                    <td class="text-center">Thêm</td>
                                </tr>
                            </thead>
                            <tbody id="tbodyListUser">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-border btn-default" id="btnCloseModal" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-border btn-gradient-bluesky" id="btnAddUserPosition">Thêm</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAddUserNews" tabindex="-1" role="dialog" aria-labelledby="modalAddUserNews">
    <div class="modal-dialog">
        <div class="modal-content" style="max-width: 1200px!important; left: 0;right: 0;margin: auto;">
            <div class="modal-header">
                <h5 class="modal-title">Thêm nhân viên vào vị trí</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <?php echo form_open('api/user/addUsers', array('id' => 'ceoUserForm')); ?>
                <div class="row">
                    <div class="col-12">
                        <div class="form-user-to-position">
                            <div class="form-row">
                                <div class="form-group-border col-sm-6">
                                    <label class="control-label">Họ tên <span class="required">*</span></label>
                                    <input type="text" id="FullName" name="FullName" class="form-control hmdrequired" value="" data-field="Họ tên" autocomplete="off">
                                </div>
                                <div class="form-group-border col-md-6">
                                    <label class="control-label">Giới tính</label>
                                    <select id="inputState" name="GenderId" class="form-control">
                                        <option value="1">Nam</option>
                                        <option value="2">Nữ</option>
                                    </select>
                                </div>
                                <div class="form-group-border col-sm-6">
                                    <label class="control-label">Số điện thoại <span class="required">*</span></label>
                                    <input type="text" id="PhoneNumber" name="PhoneNumber" class="form-control hmdrequired" value="" autocomplete="off">
                                </div>
                                <div class="form-group-border col-sm-6">
                                    <label class="control-label">Email </label>
                                    <input type="text" id="Email" name="Email" class="form-control" value="" autocomplete="off">
                                </div>

                                <div class="col-sm-6 form-group-border">
                                    <label class="control-label">Ngày sinh</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" class="form-control datepicker" name="BirthDay" id="birthDay" value="" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group-border col-sm-6">
                                    <label class="control-label">Password </label>
                                    <input type="password" id="UserPass" name="UserPass" class="form-control" value="" autocomplete="off">
                                </div>
                                <input type="text" hidden class="form-control" name="IsOld" value="<?= isset($isOld) ? $isOld : 2; ?>">
                                <input type="text" hidden class="form-control" id="PositionId" name="PositionId" value="0">
                                <input type="text" hidden class="form-control" id="userId" value="0">
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-save-user">Cập nhật</button>
            </div>
        </div>
    </div>
</div>

<!--Thêm khu vực-->
<div class="modal fade" id="modalAddProvinces" tabindex="-1" role="dialog" aria-labelledby="modalAddProvinces">
    <div class="modal-dialog">
        <div class="modal-content" style="max-width: 1200px!important; left: 0;right: 0;margin: auto;">
            <div class="modal-header">
                <h4 class="modal-title">Thêm mới khu vực</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group-border">
                            <div class="row">
                                <div class="form-check col-md-6">
                                    <input type="radio" name="AreaTypeId" class="iCheckRadioArea" id="icheckCountry" value="1" checked>
                                    <label class="form-check-label" for="">
                                        Toàn quốc
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-check col-md-6">
                                    <input type="radio" name="AreaTypeId" class="iCheckRadioArea" id="icheckPartOfCountry" value="2">
                                    <label class="form-check-label" for="">
                                        Theo miền
                                    </label>
                                </div>
                                <div class="form-group-border col-md-6" id="addPartOfCountry" style="display: none;">
                                    <?php $this->Mconstants->selectConstants('partOfCountry', 'PartOfCountryId'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-check col-md-6">
                                    <input type="radio" name="AreaTypeId" class="iCheckRadioArea" id="icheckRegion" value="3">
                                    <label class="form-check-label" for="">
                                        Theo khu vực
                                    </label>
                                </div>
                                <div class="form-group-border col-md-6" id="addRegion" style="display: none;">
                                    <?php $this->Mconstants->selectConstants('region', 'RegionId'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-check col-md-6">
                                    <input type="radio" name="AreaTypeId" class="iCheckRadioArea" id="icheckProvince" value="4">
                                    <label class="form-check-label" for="">
                                        Theo tỉnh
                                    </label>
                                </div>
                                <div class="form-group-border col-md-6" id="addProvince" style="display: none;">
                                    <?php $this->Mconstants->selectObject($provinceList, 'ProvinceId', 'ProvinceName', 'ProvinceId', 0, false, '', ' select2', ' style="width: 100%;"'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-check col-md-6">
                                    <input type="radio" name="AreaTypeId" class="iCheckRadioArea" id="icheckManual" value="5">
                                    <label class="form-check-label" for="">
                                        Tự thêm
                                    </label>
                                </div>
                                <div class="col-md-6" id="manual" style="display: none">
                                    <div class="form-group-border">
                                        <input type="text" name="ManualArea" id="manualArea" class="form-control" value="" placeholder="Nhập khu vực">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit">Cập nhật</button>
            </div>
        </div>
    </div>
</div>

<!--Thêm text ý thức-->
<!--<div class="modal CEO-modal fade" id="modalAddAwareness" style="z-index: 9999999" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="mblock-content">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title"> Thêm mới text</h4>
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group-border">
                            <input type="text" class="form-control" name="awarenessText" id="awarenessText">
                        </div>
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <button type="button" class="btn btn-border btn-gradient-bluesky waves-effect waves-light submit">Cập nhật</button>
            </div>
        </div>
    </div>
</div>-->

<!--Cập nhật text ý thức-->
<!--<div class="modal CEO-modal fade" id="modalUpdateAwareness" style="z-index: 9999999" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="mblock-content">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title"> Cập nhật text</h4>
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group-border">
                            <input type="text" class="form-control" name="awarenessText" id="awarenessText">
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <button type="button" class="btn btn-danger btn-border waves-effect waves-light btnDeleteRow">Xóa</button>
            </div>
            <div class="mblock-footer">
                <button type="button" class="btn btn-border btn-gradient-bluesky waves-effect waves-light submit">Cập nhật</button>
            </div>
        </div>
    </div>
</div>-->

<!--Thêm text hiệu quả công việc-->
<!--<div class="modal CEO-modal fade" id="modalAddJobPerformance" style="z-index: 9999999" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="mblock-content">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title"> Thêm mới text</h4>
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <input type="text" class="form-control" name="jobPerformanceText" id="jobPerformanceText">
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <button type="button" class="btn btn-border btn-gradient-bluesky waves-effect waves-light submit" >Cập nhật</button>
            </div>
        </div>
    </div>
</div>-->

<!--Cập nhật text hiệu quả công việc-->
<!--<div class="modal CEO-modal fade" id="modalUpdateJobPerformance" style="z-index: 9999999" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="mblock-content">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title"> Cập nhật text</h4>
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <input type="text" class="form-control" name="jobPerformanceText" id="jobPerformanceText">
                    </div>
                </div>
            </div>
            <div>
                <button type="button" class="btn btn-danger btn-border waves-effect waves-light btnDeleteRow">Xóa</button>
            </div>
            <div class="mblock-footer">
                <button type="button" class="btn btn-border btn-gradient-bluesky waves-effect waves-light submit" >Cập nhật</button>
            </div>
        </div>
    </div>
</div>-->

<!--Thông tin mở rộng-->
<div class="modal fade" id="modalInfor" tabindex="-1" role="dialog" aria-labelledby="modalInfor">
    <div class="modal-dialog">
        <div class="modal-content" style="max-width: 1200px!important; left: 0;right: 0;margin: auto;">
            <div class="modal-header">
                <h5 class="modal-title">Cấu hình mở rộng</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <table class="table">
                            <tbody>
                            <!--<tr>
                                <td style="padding-top: 20px;">1. Tên nhóm sản phẩm</td>
                                <td>
                                    <input class="form-control" value="" id="nameGroupProduct">
                                    <a href="javascript:void(0)" class="saveProduct"><i class="fa fa-check"></i></a>
                                </td>
                            </tr>-->
                            <tr>
                                <td valign="center">1. Mô tả công việc</td>
                                <td><a href="javascript:void(0)" class="add-job-description tab2">Thêm chi tiết</a></td>
                            </tr>
                            <tr>
                                <td valign="center">2. Nội quy</td>
                                <td><a href="javascript:void(0)" class="add-rules tab5">Thêm chi tiết</a></td>
                            </tr>
                            <tr>
                                <td valign="center">3. Quy chế KPI</td>
                                <td><a href="javascript:void(0)" class="add-kpi tab3">Thêm chi tiết</a></td>
                            </tr>
                            <tr>
                                <td valign="center">4. Quy chế lương thưởng</td>
                                <td><a href="javascript:void(0)" class="add-table-salary tab4">Thêm chi tiết</a></td>
                            </tr>

                            <tr>
                                <td valign="center">5. Chính sách phúc lợi</td>
                                <td><a href="javascript:void(0)" class="add-welfare-policy tab9">Thêm chi tiết</a></td>
                            </tr>
                            <tr>
                                <td valign="center">6. Tài liệu training - đào tạo</td>
                                <td><a href="javascript:void(0)" class="add-training-materials tab6">Thêm chi tiết</a></td>
                            </tr>
                            <tr>
                                <td valign="center">7. Ngân hàng biểu mẫu</td>
                                <td><a href="javascript:void(0)" class="add-tables-forms tab7">Thêm chi tiết</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Thêm mô tả công việc-->
<!--<div class="modal CEO-modal fade" id="modalAddJobDescription" style="z-index: 1051;" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-width: 968px;">
        <div class="mblock-content" style="top: -10px;">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title"> Mô tả công việc</h4>
                <button type="button" class="btn btn-info btnWriteJobDescription"><i class="fa fa-pencil"></i> Soạn văn bản</button>
                <button type="button" class="btn btn-info btnUploadFileJobDescription"><i class="fa fa-upload"></i> Tải lên file</button>
                <input type="file" style="display: none" id="inputFileJobDescription">
                <input type="text" hidden="hidden" id="inputFileExtensionJobDescription">
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <div class="box-body" id="showFile_modalAddJobDescription">
                        </div>
                        <div class="show-file" style="display: none">
                            <div class="content">
                                <canvas id="the-canvas-job"></canvas>
                            </div>
                            <div class="page">
                                <a id="prev" href="javascript:void(0)" class="fa fa-chevron-left"></a>
                                <span id="page_num"></span> of <span id="page_count"></span>
                                <a id="next" href="javascript:void(0)" class="fa fa-chevron-right"></a>
                                <a id="full-screen" data-url="" href="javascript:void(0)" class="glyphicon glyphicon-fullscreen right"></a>
                            </div>
                        </div>
                        <div class="form-group-border" id="divJobDescription">
                            <textarea name="JobDescription" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <input type="text" hidden="hidden" id="fileUrl_AddJobDescription" value="">
                <button type="button" class="btn submit btn-border btn-gradient-bluesky waves-effect waves-light">Cập nhật</button>
            </div>
        </div>
    </div>
</div>-->

<!--Nội quy-->
<!--<div class="modal CEO-modal fade" id="modalAddRules" style="z-index: 1051;" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-width: 968px;">
        <div class="mblock-content" style="top: -10px;">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title">Nội quy</h4>
                <button type="button" class="btn btn-info btnWriteRule"><i class="fa fa-pencil"></i> Soạn văn bản</button>
                <button type="button" class="btn btn-info btnUploadFileRule" ><i class="fa fa-upload"></i> Tải lên file</button>
                <input type="file" style="display: none" id="inputFileRule">
                <input type="text" hidden="hidden" id="inputFileExtensionRule">
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <div class="box-body" id="showFile_modalAddRules"></div>
                        <div class="show-file" style="display: none">
                            <div class="content">
                                <canvas id="the-canvas-rule"></canvas>
                            </div>
                            <div class="page">
                                <a id="prev" href="javascript:void(0)" class="fa fa-chevron-left"></a>
                                <span id="page_num"></span> of <span id="page_count"></span>
                                <a id="next" href="javascript:void(0)" class="fa fa-chevron-right"></a>
                                <a id="full-screen" data-url="" href="javascript:void(0)" class="glyphicon glyphicon-fullscreen right"></a>
                            </div>
                        </div>
                        <div class="form-group-border" id="divJobDescription3">
                            <textarea name="JobDescription3" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <input type="text" hidden="hidden" id="fileUrl_AddRules" value="">
                <button type="button" class="btn btn-border btn-gradient-bluesky waves-effect waves-light submit" data-id="5">Cập nhật</button>
            </div>
        </div>
    </div>
</div>-->

<!--Thêm KPI-->
<!--<div class="modal CEO-modal fade" id="modalAddKPI" style="z-index: 1051;" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-width: 968px;">
        <div class="mblock-content" style="top: -10px;">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title"> Thêm mới KPI</h4>
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <?php /*echo form_open('api/ceo/saveKpi', array('id' => 'ceoApiForm')); */?>
                        <table class="table-default table-bordered table-hover" id="ceoApiTable" style="overflow: auto">
                            <thead>
                            <tr>
                                <th style="min-width: 200px"></th>
                                <th style="min-width: 200px">Tiêu chí</th>
                                <th>Khối lượng tiêu chuẩn</th>
                                <th>Điểm tiêu chuẩn</th>
                                <th>Điểm tối đa</th>
                                <th>Khối lượng thực tế</th>
                                <th style="min-width: 100px">Điểm thực tế</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <input type="hidden" name="kpi[PositionId]" id="PositionId">
                        </table>
                        <?php /*echo form_close(); */?>
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <p style="color: red;">Ở phiên bản V1.0, Bảng KPI này chỉ mang tính chất mô phỏng minh họa</p>
                <button type="button" class="btn btn-border btn-gradient-bluesky waves-effect waves-light submit">Cập nhật</button>
            </div>
        </div>
    </div>
</div>-->

<!--Quy chế lương thưởng-->
<!--<div class="modal CEO-modal fade" id="modalAddSalary" style="z-index: 1051;" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-width: 968px;">
        <div class="mblock-content" style="top: -10px">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title"> Quy chế lương thưởng</h4>
                <button type="button" class="btn btn-info btnWriteSalary"><i class="fa fa-pencil"></i> Soạn văn bản</button>
                <button type="button" class="btn btn-info btnUploadFileSalary" ><i class="fa fa-upload"></i> Tải lên file</button>
                <input type="file" style="display: none" id="inputFileSalary">
                <input type="text" hidden="hidden" id="inputFileExtensionSalary">
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <div class="box-body" id="showFile_modalAddSalary">
                        </div>
                        <div class="show-file" style="display: none">
                            <div class="content">
                                <canvas id="the-canvas-salary"></canvas>
                            </div>
                            <div class="page">
                                <a id="prev" href="javascript:void(0)" class="fa fa-chevron-left"></a>
                                <span id="page_num"></span> of <span id="page_count"></span>
                                <a id="next" href="javascript:void(0)" class="fa fa-chevron-right"></a>
                                <a id="full-screen" data-url="" href="javascript:void(0)" class="glyphicon glyphicon-fullscreen right"></a>
                            </div>
                        </div>
                        <div class="form-group-border" id="divJobDescription2">
                            <textarea name="JobDescription2" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <input type="text" hidden="hidden" id="fileUrl_AddSalary" value="">
                <button type="button" class="btn btn-border btn-gradient-bluesky waves-effect waves-light submit" data-id="4">Cập nhật</button>
            </div>
        </div>
    </div>
</div>-->

<!--Chính sách phúc lợi-->
<!--<div class="modal CEO-modal fade" id="modalAddWelfarePolicy" style="z-index: 1051;" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-width: 968px;">
        <div class="mblock-content" style="top: -10px">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title"> Chính sách phúc lợi</h4>
                <button type="button" class="btn btn-info btnWriteWelfarePolicy"><i class="fa fa-pencil"></i> Soạn văn bản</button>
                <button type="button" class="btn btn-info btnUploadFileWelfarePolicy"><i class="fa fa-upload"></i> Tải lên file</button>
                <input type="file" style="display: none" id="inputFileWelfarePolicy">
                <input type="text" hidden="hidden" id="inputFileExtensionWelfarePolicy">
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <div class="box-body" id="showFile_modalAddWelfarePolicy">
                        </div>
                        <div class="show-file" style="display: none">
                            <div class="content">
                                <canvas id="the-canvas-job"></canvas>
                            </div>
                            <div class="page">
                                <a id="prev" href="javascript:void(0)" class="fa fa-chevron-left"></a>
                                <span id="page_num"></span> of <span id="page_count"></span>
                                <a id="next" href="javascript:void(0)" class="fa fa-chevron-right"></a>
                                <a id="full-screen" data-url="" href="javascript:void(0)" class="glyphicon glyphicon-fullscreen right"></a>
                            </div>
                        </div>
                        <div class="form-group-border" id="divJobDescription4">
                            <textarea name="JobDescription4" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <input type="text" hidden="hidden" id="fileUrl_AddWelfarePolicy" value="">
                <button type="button" class="btn btn-border btn-gradient-bluesky waves-effect waves-light submit">Cập nhật</button>
            </div>
        </div>
    </div>
</div>-->

<!--Tài liệu training - đào tạo-->
<div class="modal CEO-modal fade" id="modalDocumentTraining" style="z-index: 1051;" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-width: 968px;">
        <div class="mblock-content" style="top: -10px">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title"> Tài liệu training - đào tạo</h4>
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body" style="min-height: 500px; max-height: 600px;">
                <div class="row">
                    <div class="col-12">
                        <a href="javascript:void(0)" class="add-lesson">
                            <i class="fa fa-plus"></i>Thêm bài học
                        </a>
                        <div class="row row-fix">
                            <div class="col-3">
                                <div class="list-group-circle">
                                    <ol class="activity-feed">
                                        <li class="feed-item"  style="display: none">
                                            <a href="javascript:void(0);" class="item-execute active" data-target="#contentfeed1">
                                                <span class="number">1</span>
                                                <span class="text">Tab1</span>
                                            </a>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                            <div class="col-9">
                                <div class="block-feed form-updateLesson" id="updateLesson">
                                    <div class="form-group-border">
                                        <label class="control-label">Tên bài học <span class="required">*</span></label>
                                        <input type="text" class="form-control hmdrequired" id="lessonName" value="" data-field="Tên bài học">
                                    </div>
                                    <div class="form-group-border">
                                        <label class="control-label">Loại tài liệu
                                            <div class="radio-group" style="display: inline;padding-left: 10px;">
                                                <?php foreach($this->Mconstants->lessonTypes as $i => $v){ ?>
                                                    <span class="item"><input type="radio" name="LessonTypeId" class="iCheckRadio" value="<?php echo $i; ?>"<?php if($i == 1) echo ' checked'; ?>> <?php echo $v; ?></span>
                                                <?php } ?>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="form-group-border divLessonType" id="divLessonType_2" style="display: none;">
                                        <button type="button" class="btn btn-info btnUploadMedia" data-id="2"><i class="fa fa-upload"></i> Upload</button>
                                        <div class="col-sm-12 no-padding">
                                            <!--<style>
                                                .video-js {
                                                    width: 100%;
                                                }
                                            </style>
                                            <video id="my-audio"
                                                   class="video-js"
                                                   controls
                                                   preload="auto"
                                                   poster=""
                                                   data-setup='{}'>
                                                <source id="audioWrap" src="" type="video/mp4">
                                            </video>-->
                                        </div>
                                    </div>
                                    <div class="form-group-border divLessonType" id="divLessonType_3" style="display: none;">
                                        <!--button type="button" class="btn btn-info btnUploadMedia" data-id="3"><i class="fa fa-upload"></i> <span class="title-download"><span></button>-->
                                        <div class="form-group-border">
                                            <label>Link video youtube <span class="required">*</span></label>
                                            <input type="text" class="form-control" id="youtubeEditUrl" value="">
                                        </div>
                                        <div class="col-sm-12 no-padding" id="divVideoEdit" style="display: none;">
                                            <iframe id="iframeVideoEdit" src="" frameborder="0" allowfullscreen style="width: 100%;height: 500px;"></iframe>
                                            <!--<video id="my-video-update"
                                                   class="video-js"
                                                   controls
                                                   preload="auto"
                                                   poster=""
                                                   data-setup='{}'>
                                                <source id="playVideo" src="" type="video/mp4">
                                            </video>-->
                                        </div>
                                    </div>
                                    <div class="form-group-border divLessonType" id="divLessonType_4" style="display: none;">
                                        <button type="button" class="btn btn-info btnUploadMedia" data-id="4"><i class="fa fa-upload"></i> Tải lên file</button>
                                        <input type="file" style="display: none" id="inputFileDocumentTraining">
                                        <input type="text" hidden="hidden" id="inputFileExtensionDocumentTraining">
                                        <div class="box-body" id="showFile"></div>
                                        <div class="show-file" style="display: none">
                                            <div class="content">
                                                <canvas id="the-canvas-updateLesson"></canvas>
                                            </div>
                                            <div class="page">
                                                <a id="prev" href="javascript:void(0)" class="fa fa-chevron-left"></a>
                                                <span id="page_num"></span> of <span id="page_count"></span>
                                                <a id="next" href="javascript:void(0)" class="fa fa-chevron-right"></a>
                                                <a id="full-screen" data-url="" href="javascript:void(0)" class="glyphicon glyphicon-fullscreen right"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group-border divLessonType" style="margin-top: 20px;" id="divLessonType_1">
                                        <label class="control-label">Nội dung</label>
                                        <textarea name="lessonContent" id="lessonContentUpdate" class="form-control"></textarea>
                                    </div>
                                    <input type="text" hidden="hidden" id="fileUrl" value="">
                                    <input type="text" hidden="hidden" id="PositionLessionId" value="0">
                                    <input type="text" hidden="hidden" id="LessonPositionId" value="0">
                                    <div class="btn-lesson">
                                        <button type="button" class="btn btn-border btn-gradient-bluesky waves-effect waves-light submitLesson">Cập nhật</button>
                                        <button type="button" class="btn btn-border waves-effect waves-light delete">Xóa</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Danh sách ngân hàng biểu mẫu-->
<!--<div class="modal CEO-modal fade" id="modalFormBank" style="z-index: 1051;" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-keyboard="false">
    <div class="modal-dialog" style="max-width: 968px;">
        <div class="mblock-content" style="height: 700px; top: -10px">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title"> Ngân hàng biểu mẫu</h4>
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body form-updateFormBank" style="min-height: 300px;">
                <div class="row">
                    <div class="col-12">
                        <a href="javascript:void(0)" class="add-form-bank">
                            <i class="fa fa-plus"></i> Thêm Ngân hàng biểu mẫu
                        </a>
                        <div class="row row-fix">
                            <div class="col-3">
                                <div class="list-group-circle">
                                    <ol class="activity-feed">
                                        <li class="feed-item"  style="display: none">
                                            <a href="javascript:void(0);" class="item-execute active" data-target="#contentfeed1">
                                                <span class="number">1</span>
                                                <span class="text">Tab1</span>
                                            </a>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                            <div class="col-9">
                                <div class="block-feed form-updateLesson" id="updateLesson">
                                    <div class="form-group-border">
                                        <label class="control-label">Tên ngân hàng <span class="required">*</span></label>
                                        <input type="text" class="form-control hmdrequired" id="FormBankName" value="" data-field="Tên ngân hàng">
                                    </div>
                                    <div class="form-group-border">
                                        <div class="radio-group">
                                            <span class="item"><input type="radio" name="LessonTypeId" class="iCheckRadio" value="1" checked>Text</span>
                                            <span class="item"><input type="radio" name="LessonTypeId" class="iCheckRadio" value="2" > File</span>
                                        </div>
                                    </div>
                                    <div class="form-group-border divLessonType" style="margin-top: 20px;" id="divLessonType_1">
                                        <label class="control-label">Nội dung</label>
                                        <textarea name="FormBankDescriptionUpdate" id="FormBankDescriptionUpdate" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group-border divLessonType" id="divLessonType_2" style="display: none;">
                                        <button type="button" class="btn btn-info btnUploadFileUpdateFormBank" data-id="4"><i class="fa fa-upload"></i> Tải lên file</button>
                                        <input type="file" style="display: none" id="inputFileFormBank">
                                        <input type="text" hidden="hidden" id="inputFileExtensionFormBank">
                                        <div class="box-body" id="showFileFormBankDescriptionUpdate"></div>
                                    </div>
                                    <input type="text" hidden="hidden" id="fileUrl" value="">
                                    <input type="hidden" id="PositionFormId">
                                    <input type="hidden" id="FormPositionId">
                                    <div class="btn-lesson">
                                        <button type="button" class="btn submitFormBank btn-border btn-gradient-bluesky waves-effect waves-light">Cập nhật</button>
                                        <button type="button" class="btn btn-danger delete btn-border waves-effect waves-light">Xóa</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->

<!--Thêm ngân hàng bảng biểu - biểu mẫu-->
<div class="modal CEO-modal fade" id="modalAddFormBank" style="z-index: 1052;" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-keyboard="false">
    <div class="modal-dialog" style="max-width: 1000px;">
        <div class="mblock-content" style="height: 800px; top: -20px;">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title"> Ngân hàng biểu mẫu</h4>
                <!--                <button type="button" class="btn btn-info btnUploadFileFormBank"><i class="fa fa-upload"></i> Tải lên file</button>-->
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group-border">
                            <label class="control-label">Tên ngân hàng <span class="required">*</span></label>
                            <input type="text" class="form-control hmdrequired" id="FormBankName" value="" data-field="Tên ngân hàng">
                        </div>
                        <div class="form-group-border">
                            <div class="radio-group">
                                <span class="item"><input type="radio" name="LessonTypeId" class="iCheckRadio" value="1" checked>Text</span>
                                <span class="item"><input type="radio" name="LessonTypeId" class="iCheckRadio" value="2" > File</span>
                            </div>
                        </div>
                        <div class="form-group-border divLessonType" style="margin-top: 20px;" id="divLessonType_1">
                            <label class="control-label">Nội dung</label>
                            <textarea name="FormBankDescription" id="FormBankDescription" class="form-control"></textarea>
                        </div>
                        <div class="form-group-border divLessonType" id="divLessonType_2" style="display: none;">
                            <button type="button" class="btn btn-info btnUploadFileFormBank" data-id="4"><i class="fa fa-upload"></i> Tải lên file</button>
                            <input type="file" style="display: none" id="inputFileAddFormBank">
                            <input type="text" hidden="hidden" id="inputFileExtensionAddFormBank">
                            <div class="box-body" id="showFileFormBankDescription"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <input type="text" hidden="hidden" id="fileUrl_FormBank" value="">
                <button type="button" class="btn btn-border btn-gradient-bluesky waves-effect waves-light submit">Cập nhật</button>
            </div>
        </div>
    </div>
</div>

<!--Hướng dẫn ở modal Infor-->
<div class="modal CEO-modal fade" id="modalVideoGuide" style="z-index: 1052;" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" style="max-width: 968px;">
        <div class="mblock-content">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title">Hướng dẫn</h4>
            </div>
            <button type="button" class="mbtn-close close close-video" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <iframe id="video-intro" width="100%" height="415" src="https://www.youtube.com/embed/Pi4XLhkIXyc?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Thêm bài học-->
<div class="modal CEO-modal fade" id="modalAddLesson" style="z-index: 1052;" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="min-width: 1010px;">
        <div class="mblock-content" style="height:700px; top: -20px;">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title"> Thêm bài học</h4>
            </div>
            <button type="button" class="mbtn-close close btnClose" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body" id="addLesson" style="min-height: 300px">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group-border">
                            <label>Tên bài học <span class="required">*</span></label>
                            <input type="text" class="form-control" name="lessonName" id="lessonName">
                        </div>
                        <div class="form-group-border">
                            <label class="control-label">Loại tài liệu
                                <div class="radio-group" style="display: inline;padding-left: 10px;">
                                    <?php foreach($this->Mconstants->lessonTypes as $i => $v){ ?>
                                        <span class="item"><input type="radio" name="LessonTypeId" class="iCheckRadio" value="<?php echo $i; ?>"<?php if($i == 1) echo ' checked'; ?>> <?php echo $v; ?></span>
                                    <?php } ?>
                                </div>
                            </label>
                        </div>
                        <div class="form-group-border divLessonType" id="divLessonType_2" style="display: none;">
                            <button type="button" class="btn btn-info btnUploadMedia" data-id="2"><i class="fa fa-upload"></i> Tải lên audio</button>
                            <div class="col-sm-12 no-padding">
                                <!--<style>
                                    .video-js {
                                        width: 100%;
                                    }
                                </style>
                                <video id="my-audio"
                                       class="video-js"
                                       controls
                                       preload="auto"
                                       poster=""
                                       data-setup='{}'>
                                    <source id="audioWrap" src="" type="video/mp4">
                                </video>-->
                            </div>
                        </div>
                        <div class="form-group-border divLessonType" id="divLessonType_3" style="display: none;">
                            <!--<button type="button" class="btn btn-info btnUploadMedia" data-id="3"><i class="fa fa-upload"></i> <span class="title-download">Tải lên video<span></span></button>-->
                            <div class="form-group-border">
                                <label>Link video youtube <span class="required">*</span></label>
                                <input type="text" class="form-control" id="youtubeAddUrl" value="">
                            </div>
                            <div class="col-sm-12 no-padding" id="divVideoAdd" style="display: none;">
                                <iframe id="iframeVideoAdd" src="" frameborder="0" allowfullscreen style="width: 100%;height: 500px;"></iframe>
                                <!--<video id="my-video"
                                       class="video-js"
                                       controls
                                       preload="auto"
                                       poster=""
                                       data-setup='{}'>
                                    <source id="playVideo" src="" type="video/mp4">
                                </video>-->
                            </div>
                        </div>

                        <div class="form-group-border divLessonType" id="divLessonType_4" style="display: none;">
                            <button type="button" class="btn btn-info btnUploadMedia" data-id="4"><i class="fa fa-upload"></i> Tải lên file</button>
                            <input type="file" style="display: none" id="inputFileAddLesson">
                            <input type="text" hidden="hidden" id="inputFileExtensionAddLesson">
                            <div class="box-body" id="showFile"></div>
                            <div class="show-file" style="display: none">
                                <div class="content">
                                    <canvas id="the-canvas-addLesson"></canvas>
                                </div>
                                <div class="page">
                                    <a id="prev" href="javascript:void(0)" class="fa fa-chevron-left"></a>
                                    <span id="page_num"></span> of <span id="page_count"></span>
                                    <a id="next" href="javascript:void(0)" class="fa fa-chevron-right"></a>
                                    <a id="full-screen" data-url="" href="javascript:void(0)" class="glyphicon glyphicon-fullscreen right"></a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group-border divLessonType" style="margin-top: 20px;" id="divLessonType_1">
                            <label class="control-label">Nội dung</label>
                            <textarea name="lessonContent" id="lessonContent" class="form-control"></textarea>
                        </div>
                        <input type="text" hidden="hidden" id="fileUrl" value="">
                        <input type="text" hidden="hidden" id="PositionLessionId" value="0">
                        <input type="text" hidden="hidden" id="LessonPositionId" value="0">
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <button type="button" class="btn btn-border btn-gradient-bluesky waves-effect waves-light submitLesson" id="submitLesson_modalAddLesson" data-submit="addLesson">Cập nhật</button>
            </div>
        </div>
    </div>
</div>

<div class="modal CEO-modal fade" id="modalAddPointStart" style="z-index: 999999" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="mblock-content">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title">Thông báo xác nhận</h4>
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <center>Bạn có đồng ý chọn điểm đầu không?</center>
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <button type="button" class="btn btn-border button btn-gradient-bluesky waves-effect waves-light savePointStart">Đồng ý</button>
                <button type="button" class="btn btn-border button btn-gradient-bluesky waves-effect waves-light" data-dismiss="modal">Hủy</button>
            </div>
        </div>
    </div>
</div>

<div class="modal CEO-modal fade" id="modalAddPointEnd" style="z-index: 999999" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="mblock-content">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title">Thông báo xác nhận</h4>
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <center>Bạn có đồng ý chọn điểm cuối không?</center>
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <button type="button" class="btn btn-border button btn-gradient-bluesky waves-effect waves-light savePointEnd">Đồng ý</button>
                <button type="button" class="btn btn-border button btn-gradient-bluesky waves-effect waves-light" data-dismiss="modal">Hủy</button>
            </div>
        </div>
    </div>
</div>

<div class="modal CEO-modal fade" id="modalDeleteRelationship" style="z-index: 999999" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="mblock-content">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title">Thông báo xóa liên kết</h4>
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <center>Bạn có đồng ý xóa liên kết này không?</center>
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <button type="button" class="btn btn-border button btn-gradient-bluesky waves-effect waves-light delete">Đồng ý</button>
                <button type="button" class="btn btn-border button btn-gradient-bluesky waves-effect waves-light" data-dismiss="modal">Hủy</button>
            </div>
        </div>
    </div>
</div>

<div class="modal CEO-modal fade" id="modalResearch" style="z-index: 999999" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="mblock-content">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title">Thông báo thêm bản nghiên cứu</h4>
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label">Tên bản CCTC</label>
                            <input type="text" class="form-control nameCCTC" placeholder="Nhập tên tại đây">
                        </div>
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <button type="button" class="btn btn-border btn-default" data-dismiss="modal" style="margin-left: 25px;">Hủy</button>
                <button type="button" class="btn btn-border button btn-gradient-bluesky waves-effect waves-light btnAddResearch">Lưu nghiên cứu</button>
            </div>
        </div>
    </div>
</div>

<div class="modal CEO-modal fade" id="modalAddApply" style="z-index: 999999" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" style="max-width: 520px;">
        <div class="mblock-content">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title">Thông báo thay đổi bản áp dụng</h4>
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label">Tên bản CCTC</label>
                            <input type="text" class="form-control nameCCTC" placeholder="Nhập tên tại đây">
                        </div>
                        <p>Đề xuất áp dụng của bạn cần chờ Admin duyệt ở trang lịch sử áp dụng !</p>
                    </div>
                </div>
            </div>
            <div class="mblock-footer">
                <button type="button" class="btn btn-border btn-default" data-dismiss="modal" style="margin-left: 25px;">Hủy</button>
                <button type="button" class="btn btn-border button btn-gradient-bluesky waves-effect waves-light btnAddApply">Tạo và gửi đề xuất áp dụng</button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="countkpi1" value="1">
<input type="hidden" id="countkpi2" value="1">
<input type="hidden" id="countkpi3" value="1">
<input type="hidden" id="countkpi4" value="1">

<input type="hidden" id="savePositionRelateUrl" value="<?=base_url('api/position/savePositionRelate') ?>">
<input type="hidden" id="deletePositionRelateUrl" value="<?=base_url('api/position/deletePositionRelate') ?>">
<input type="hidden" value="<?=base_url('api/ceo/checkListInfor') ?>" id="checkListInforUrl">
<input type="hidden" value="<?=base_url('api/Userposition/addUser') ?>" id="addPositionUrl">
<input type="hidden" value="<?=base_url('api/ceo/getlistUser') ?>" id="getlistCeoUserUrl">
<input type="hidden" value="<?=base_url('api/ceo/viewLesson') ?>" id="viewLessonUrl">
<input type="hidden" value="<?=base_url('api/ceo/viewForm') ?>" id="viewFormUrl">
<input type="hidden" value="<?=base_url('api/ceo/deleteLesson') ?>" id="deleteLessonUrl">
<input type="hidden" value="<?=base_url('api/ceo/saveExtendedConfiguration') ?>" id="saveExtendedConfigurationUrl">
<input type="hidden" value="<?=base_url('api/ceo/saveLesson') ?>" id="saveLessonUrl">
<input type="hidden" value="<?=base_url('api/ceo/saveFormBank') ?>" id="saveFormBankUrl">
<input type="hidden" value="<?=base_url('api/ceo/getFormBank') ?>" id="getFormBankUrl">
<input type="hidden" value="<?=base_url('api/ceo/deleteFormBank') ?>" id="deleteFormBankUrl">
<input type="hidden" value="<?=base_url('api/ceo/getLesson') ?>" id="getLessonUrl">
<input type="hidden" value="<?=base_url('api/ceo/getKpi') ?>" id="getKpiUrl">
<input type="hidden" value="<?=base_url('api/ceo/getValueExtendedConfiguration') ?>" id="getValueExtendedConfigurationUrl">
<input type="hidden" value="<?=base_url('api/ceo/getFileUrl') ?>" id="getFileUrl">
