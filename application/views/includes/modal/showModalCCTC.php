<div class="modal CEO-modal fade" id="modalShowDemo" style="z-index: 999999" tabindex="-1" role="dialog" data-modal="1" aria-labelledby="test" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog"  style="max-width: 100%;margin: 50px;">
        <div class="mblock-content">
            <div class="mblock-icon">
                <img src="assets/vendor2/images/modal-icon.gif">
            </div>
            <div class="mblock-header">
                <h4 class="modal-title">Xem CCTC</h4>
            </div>
            <button type="button" class="mbtn-close close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <div class="mblock-body">
                <div class="card-body">
                    <div class="tab-content">
                        <div class="title" style="text-align: center">
                            <h3>CƠ CẤU TỔ CHỨC</h3>
                        </div>
                        <div class="col-lg-12 wow slideInRight">
                            <div class="card">
                                <div class="card-body">
                                    <div id="chart-container-demo">
                                        <input type="hidden" id="selected-node"/>
                                        <input type="hidden" id="check-children"/>
                                        <input type="hidden" id="id-department"/>
                                    </div>
                                    <div class="boxLine" style="display:none;">
                                        <?php if(isset($listPositionrelates) && !empty($listPositionrelates)) {
                                            foreach ($listPositionrelates as $Positionrelate) { ?>
                                                <span class="line tab<?=$Positionrelate['BeginPositionId'].$Positionrelate['EndPositionId']?>" data-idstart="<?=$Positionrelate['BeginPositionId']?>" data-idend="<?=$Positionrelate['EndPositionId']?>" data-start="connect<?=$Positionrelate['BeginPositionId']?>" data-end="connect<?=$Positionrelate['EndPositionId']?>" id="tab<?=$Positionrelate['BeginPositionId'].$Positionrelate['EndPositionId']?>"></span>
                                        <?php }}?>
                                    </div>
                                    <div class="context-diagrams" style="display: none;">
                                        <p id="delete-node-diagrams-icon-department"><i class="fa fa-trash" aria-hidden="true"></i></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="ccsdUrl" value="">
            </div>
        </div>
    </div>
</div>