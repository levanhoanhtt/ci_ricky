<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả sản phẩm</a></li>
                        <?php foreach($listFilters as $f){ ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php $productStatus = $this->Mconstants->productStatus; ?>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả sản phẩm theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="product_store">Cơ sở</option>
                                        <option value="product_status_trade">Tình trạng kinh doanh</option>
                                        <option value="product_status_display">Tình trạng hiển thị</option>
                                        <option value="inventory_status">Trạng thái duyệt</option>
                                        <option value="inventory_manual">Thay đổi tồn kho</option>
                                        <option value="product_type">Ngành kinh doanh</option>
                                        <option value="product_kind">Loại sản phẩm</option>
                                        <option value="product_suppliers">Nhà cung cấp</option>
                                        <option value="product_manufacturer">Hãng sản xuất</option>
                                        <!--<option value="product_tags">Được tag với</option>-->
                                        <option value="product_group_1">Nhóm sản phẩm</option>
                                        <option value="product_group_2">Loại hàng hóa</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 product_store product_status_trade product_status_display inventory_status inventory_manual product_type product_kind product_suppliers product_manufacturer product_group_1 product_group_2 display-display">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <select class="form-control product_store block-display">
                                        <?php foreach($listStores as $s){ ?>
                                            <option value="<?php echo $s['StoreId']; ?>"><?php echo $s['StoreName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_status_trade none-display">
                                        <?php foreach($productStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_status_display none-display">
                                        <?php foreach($this->Mconstants->productDisplayTypes as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control inventory_status none-display">
                                        <option value="2">Đã duyệt</option>
                                        <option value="1">Chưa duyệt</option>
                                        <option value="3">Không duyệt</option>
                                    </select>
                                    <select class="form-control inventory_manual none-display">
                                        <option value="2">Thủ công</option>
                                        <option value="1">Tự động</option>
                                    </select>
                                    <select class="form-control product_type none-display">
                                        <?php foreach($listProductTypes as $pt){ ?>
                                            <option value="<?php echo $pt['ProductTypeId']; ?>"><?php echo $pt['ProductTypeName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_kind none-display">
                                        <?php foreach($this->Mconstants->productKinds as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_suppliers none-display">
                                        <?php foreach($listSuppliers as $s){ ?>
                                            <option value="<?php echo $s['SupplierId']; ?>"><?php echo $s['SupplierName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_manufacturer none-display">
                                        <?php foreach($listManufacturers as $m){ ?>
                                            <option value="<?php echo $m['ManufacturerId']; ?>"><?php echo $m['ManufacturerName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_group_1 none-display">
                                        <?php foreach($listCategories as $c){
                                            if($c['ItemTypeId'] == 1){ ?>
                                                <option value="<?php echo $c['CategoryId']; ?>"><?php echo $c['CategoryName']; ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                    <select class="form-control product_group_2 none-display">
                                        <?php foreach($listCategories as $c){
                                            if($c['ItemTypeId'] == 2){ ?>
                                                <option value="<?php echo $c['CategoryId']; ?>"><?php echo $c['CategoryName']; ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                                <div class="form-group block-display widthauto">
                                    <!-- data-href : Đây là link gọi để filter mỗi trang sẽ có 1 link khác nhau -->
                                    <button id="btn-filter" data-href="<?php echo base_url('inventory/searchByFilter'); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <style>
                        #tbodyProduct i{color: #1782db;}
                        #tbodyProduct i.fa-arrow-down{color: red;}
                        #tbodyProduct i.fa-info{cursor: pointer;}
                    </style>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Ngày duyệt</th>
                                <th>Sản phẩm</th>
                                <th>Barcode</th>
                                <th class="text-center">SL ban đầu</th>
                                <th class="text-center">SL thay đổi</th>
                                <th class="text-center">SL còn</th>
                                <th>Ghi chú</th>
                                <th>Cơ sở</th>
                                <th class="text-center">Trạng thái duyệt</th>
                                <th>Người duyệt</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProduct"></tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="itemTypeId" value="15">
                    <input type="hidden" value="<?php echo base_url('product/edit')?>" id="urlEditProduct">
                    <input type="hidden" value="<?php echo base_url('product/editCombo')?>" id="urlEditProductCombo">
                    <?php $this->load->view('includes/modal/filter'); ?>
                    <?php $this->load->view('includes/modal/tag'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>