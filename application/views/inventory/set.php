<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <style>
                .list-his{font-size: 14px !important;}
                .list-his li{line-height: 34px;}
                #tbodyProduct .tdQuantity{padding-left: 0;padding-right: 0;}
                #tbodyProduct .spanQuantity, #tbodyProduct button{margin-right: 5px;}
                #tbodyProduct input.quantity{width: 50px;margin-right: 8px;}
                #tbodyProduct input.comment{width: 200px;margin-right: 8px;}
                #tbodyProduct select{height: 26px;}
            </style>
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl list-his mgt-10">
                    <li><label><input type="radio" value="0" name="InventoryStatusId" checked class="history"><span>Tất cả</span></label></li>
                    <li><label><input type="radio" value="2" name="InventoryStatusId" class="history"><span>Còn hàng</span></label></li>
                    <li><label><input type="radio" value="1" name="InventoryStatusId" class="history"><span>Hết hàng</span></label></li>
                    <li>
                        <?php $this->Mconstants->selectObject($listStores, 'StoreId', 'StoreName', 'StoreId', 0, true, "--Chọn cơ sở--"); ?>
                    </li>
                    <?php if($canActive){ ?>
                        <li><a href="<?php echo base_url('inventory/active'); ?>" class="btn btn-primary">Duyệt tốn kho</a></li>
                    <?php } ?>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" data-id="1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Sản phẩm một phiên bản</a></li>
                        <li data-id="2"><a href="#tab_2" data-toggle="tab" aria-expanded="true">Sản phẩm nhiều phiên bản</a></li>
                        <!--<li data-id="3"><a href="#tab_3" data-toggle="tab" aria-expanded="true">Combo</a></li>-->
                    </ul>
                </div>
                <div class="input-group ctrl-filter updaten" style="width: 98%;">
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                </div>
                <div class="">
                    <style>#tbodyProduct tr.cProduct td.productName{padding-left: 30px;}</style>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Sản phẩm</th>
                                <th class="text-center">Tồn kho</th>
                                <th class="text-center">Đang về</th>
                                <th class="text-center">Loại</th>
                                <th>Barcode</th>
                                <th class="text-center">Trạng thái</th>
                                <th class="text-center" style="width: 425px;">Cập nhật tồn kho mới</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProduct"></tbody>
                        </table>
                    </div>
                    <input type="hidden" value="<?php echo base_url('product/edit'); ?>" id="urlEditProduct">
                    <input type="hidden" value="<?php echo base_url('product/editCombo'); ?>" id="urlEditProductCombo">
                    <input type="hidden" value="<?php echo base_url('api/product/searchByFilter/1'); ?>" id="searchProductUrl">
                    <input type="hidden" value="<?php echo base_url('inventory/update'); ?>" id="updateInventoryUrl">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>