<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url(); ?>" id="baseUrl"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php $this->load->view('includes/favicon'); ?>
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/vendor/plugins/pnotify/pnotify.custom.min.css"/>
    <link rel="stylesheet" href="assets/vendor/plugins/select2/select2.min.css"/>
    <link rel="stylesheet" href="assets/vendor/plugins/iCheck/all.css">
    <?php if (isset($scriptHeader)) outputScript($scriptHeader); ?>
    <link rel="stylesheet" href="assets/vendor/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="assets/vendor/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="assets/vendor/plugins/pace/pace.min.css">
    <!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="assets/vendor/dist/fb/style.css" />
    <link rel="stylesheet" type="text/css" href="assets/vendor/dist/fb/css/responsive.css" />
</head>

<body>
<div id="wrapper">
    <header id="header" class="site-header clearfix">
        <div class="nav-bar">
            <i class="fa fa-navicon"></i>
        </div>
        <div class="site-logo">
            <a href="#"><img src="assets/vendor/dist/fb/images/logo.png" alt="Logo"/></a>
        </div>
        <div class="header-right">
            <div class="user">
                <div class="avatar">
                    <span><?=substr($pages['FbPageName'], 0, 1);?></span>
                </div>
                <div class="name">
                    <a href="#"><?=$pages['FbPageName']?></a>
                </div>
            </div>
        </div>
    </header><!-- .site-header -->

    <main id="main" class="site-main">
        <div class="bar-left">
            <ul>
                <li><a href="#"><i class="fa fa-eye-slash"></i></a></li>
                <li class="active"><a href="#"><img src="assets/vendor/dist/fb/images/tick.png" alt=""/></a></li>
                <li><a href="#"><img src="assets/vendor/dist/fb/images/not.png" alt=""/></a></li>
            </ul>
        </div>
        <div class="main-content clearfix">
            <div class="content-left">
                <div class="menu-top">
                    <ul>
                        <li class="active"><a href="#" title="Inbox"><img src="assets/vendor/dist/fb/images/all.png" alt=""/></a></li>
                        <li><a href="#" title="Inbox"><img src="assets/vendor/dist/fb/images/envelop.png" alt=""/></a></li>
                        <li><a href="#" title="Inbox"><img src="assets/vendor/dist/fb/images/talk.png" alt=""/></a></li>
                    </ul>
                </div>
                <div class="top-left">
                    <div class="search-form">
                        <form action="">
                            <input type="search" placeholder="Tìm kiếm" class="txtSearch"/>
                            <input type="submit" class="hidden" value="Search"/>
                        </form>
                    </div>
                    <div class="tags">
                        <i class="fa fa-tags"></i>
                    </div>
                </div>
                <div class="mes-lists equal-height">
                    <ul class="slimScroll">
                        <li>
                            <div class="mes-item type-comment clearfix">
                                <div class="mes-avatar">
                                    <img src="assets/vendor/dist/fb/images/avt.jpg" alt="avatar"/>
                                </div>
                                <div class="mes-excerpt">
                                    <p class="mes-name">Xấu. Trai</p>
                                    <span title="Co loai bm900 ko ak">Co loai bm900 ko ak</span>
                                    <div class="mes-time"><span>Hôm Nay 11:10</span></div>
                                    <div class="mes-tag">
                                        <ul>
                                            <li><a href="#">Khách Vip</a></li>
                                            <li><a class="custom-tag" href="#">Chó</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </div><!-- .content-left -->
            <div class="content-center">
                <div class="top-info">
                    <span><img src="assets/vendor/dist/fb/images/dog.png" alt="Dog" class="small-avatar"/> Lê Vũ Linh</span>
                </div>
                <div class="message-container equal-height">
                    <div class="post-time ">
                        <div class="mes-tag">
                            <span class="nhan">Nhãn hội thoại</span>
                            <ul>
                                <li><a href="#">Khách Vip</a></li>
                                <li><a class="custom-tag" href="#">Chó</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="post-mes slimScroll">
                        <div class="post-content">
                            <div class="post-box clearfix">
                                <div class="post-thumb">
                                    <img src="assets/vendor/dist/fb/images/avt.jpg" alt=""/>
                                </div>
                                <div class="post-detail">
                                    <h3>Hình ảnh minh Họa</h3>
                                    <p>Combo HOME Studio đẳng cấp trong tầm giá !</p>
                                </div>
                                <a href="#" class="share-post"><i class="fa fa-external-link"></i></a>
                            </div>
                        </div>
                        <div class="list-message">
                            <ul>
                                <li>
                                    <div class="message-icon">
                                        <img src="assets/vendor/dist/fb/images/small-icon.jpg" alt=""/>
                                        <div class="message-user"><a href="#">Văn Cường</a></div>
                                    </div>
                                    <p class="message">.</p><br/>
                                    <p class="message">Giá thế nào ad?</p>
                                </li>
                                <li class="admin">
                                    <p class="message">Chào bạn, ad đã ib cho bạn rồi đó ạ  Bạn check ib giúp ad nha. Cảm ơn bạn nhiều ạ <3</p>
                                </li>
                                <li>
                                    <div class="message-icon">
                                        <img src="assets/vendor/dist/fb/images/small-icon.jpg" alt=""/>
                                        <div class="message-user"><a href="#">Văn Cường</a></div>
                                    </div>
                                    <p class="message">.</p><br/>
                                    <p class="message">Giá thế nào ad?</p>
                                </li>
                                <li class="admin">
                                    <p class="message">Chào bạn, ad đã ib cho bạn rồi đó ạ  Bạn check ib giúp ad nha. Cảm ơn bạn nhiều ạ <3</p>
                                </li>
                                <li>
                                    <div class="message-icon">
                                        <img src="assets/vendor/dist/fb/images/small-icon.jpg" alt=""/>
                                        <div class="message-user"><a href="#">Văn Cường</a></div>
                                    </div>
                                    <p class="message">.</p><br/>
                                    <p class="message">Giá thế nào ad?</p>
                                </li>
                                <li class="admin">
                                    <p class="message">Chào bạn, ad đã ib cho bạn rồi đó ạ  Bạn check ib giúp ad nha. Cảm ơn bạn nhiều ạ <3</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="action ">
                        <form action="" class="">
                            <div class="clearfix">
                                <div class="form-text">
                                    <textarea name="" id="" cols="30" rows="3" placeholder="Nhập nội dung tại đây"></textarea>
                                </div>
                                <div class="form-action">
                                    <div class="list-action">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-camera"></i></a></li>
                                            <li><a href="#"><i class="fa fa-comments"></i></a></li>
                                            <li><a href="#"><i class="fa fa-tags"></i></a></li>
                                        </ul>
                                    </div>
                                    <input type="submit" value="Gửi" class="btn btn-primary"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- .content-center -->
            <div class="content-right">
                <div class="tab-user equal-height">
                    <div class="tab-heading">
                        <ul class="clearfix">
                            <li><a href="#info"><img src="assets/vendor/dist/fb/images/people-512.png" alt="" width="40"/></a></li>
                            <li><a href="#cart"><img src="assets/vendor/dist/fb/images/cart.png" alt="" width="40"/></a></li>
                            <li><a href="#suppoet"><img src="assets/vendor/dist/fb/images/sp.png" alt="" width="40"/></a></li>
                        </ul>
                    </div>
                    <div class="tab-container">
                        <div class="tab-content" id="info">
                            <div class="clearfix">
                                <div class="col-md-4 col-xs-12 customer pull-left">
                                    <div class="participant-avatar-customer text-center">
                                        <img class="img-max-width-70" alt="//hstatic.net/0/0/global/noDefaultImage6_large.gif" src="//graph.facebook.com/132550450842931/picture?width=70&amp;height=70">
                                    </div>
                                    <a  href="#" target="_blank" class="view-detail-customer text-center"><strong>Hải's Lắc's</strong></a>
                                    <div class="action-btn">
                                        <a href="#">Báo cáo</a>
                                        <a href="#">Chặn</a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-12 p-l10 p-r10 pull-left">
                                    <ul class="participant-info-detail-customer">
                                        <li class="overflow-ellipsis">
                                            <i class="fa fa-user"></i>
                                            <span data-bind="text: CustomerNameTmp">Hải's Lắc's</span>
                                        </li>
                                        <li class="overflow-ellipsis">
                                            <i class="fa fa-phone-square mr5"></i>
                                            <span data-bind="text: CustomerPhoneTmp">Số điện thoại</span>
                                        </li>
                                        <li class="overflow-ellipsis">
                                            <i class="fa fa-map-marker"></i>
                                            <span data-bind="text: CustomerEmailTmp">Địa chỉ</span>
                                        </li>
                                        <li class="overflow-ellipsis">
                                            <i class="fa fa-envelope"></i>
                                            <span data-bind="text: CustomerNameTmp">Email</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="info-user clearfix">
                                <a href="#" class="pull-right color"><i class="fa fa-pencil"></i>Chỉnh sửa thông tin</a>

                                <form action="">
                                    <textarea name="note" id="note" cols="30" rows="3" placeholder="Nhập nội dung ghi chú"></textarea>
                                </form>
                                <a href="#" class="pull-right">Xem tất cả ghi chú</a>
                            </div>
                            <div class="order text-center clearfix">
                                <p class="text-left"><i class="fa fa-shopping-cart color"></i>Khách hàng chưa có đơn hàng</p>
                                <a href="#" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>Tạo đơn hàng</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .content-right -->
        </div><!-- .main-content -->
    </main><!-- .site-main -->

    <footer id="footer" class="site-footer">
        <div class="container">

        </div><!-- .container -->
    </footer><!-- site-footer -->
</div>
<input type="text" hidden="hidden" id="rootPath" value="<?php echo ROOT_PATH; ?>">
<input type="text" hidden="hidden" id="siteName" value="Ricky">
<input type="text" hidden="hidden" id="userImagePath" value="<?php echo USER_PATH; ?>">
<?php if(!$user) $user = $this->session->userdata('user');
if($user){ ?>
    <input type="text" hidden="hidden" id="userLoginId" value="<?php echo $user['UserId']; ?>">
    <input type="text" hidden="hidden" id="fullNameLoginId" value="<?php echo $user['FullName'] ?>">
    <input type="text" hidden="hidden" id="avatarLoginId" value="<?php echo empty($user['Avatar']) ? NO_IMAGE : $user['Avatar']; ?>">
<?php } else { ?>
    <input type="text" hidden="hidden" id="userLoginId" value="0">
    <input type="text" hidden="hidden" id="fullNameLoginId" value="">
    <input type="text" hidden="hidden" id="avatarLoginId" value="<?php echo NO_IMAGE; ?>">
<?php } ?>
<noscript><meta http-equiv="refresh" content="0; url=<?php echo base_url('user/permission'); ?>" /></noscript>
<script src="assets/vendor/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/plugins/pace/pace.min.js"></script>
<script src="assets/vendor/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="assets/vendor/plugins/fastclick/fastclick.js"></script>
<script src="assets/vendor/dist/js/app.min.js"></script>
<script src="assets/vendor/plugins/pnotify/pnotify.custom.min.js"></script>
<script src="assets/vendor/plugins/select2/select2.full.min.js"></script>
<script src="assets/vendor/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="assets/js/common.js?20172210"></script>
<?php if(isset($scriptFooter)) outputScript($scriptFooter); ?>
</body>
</html>