<?php $this->load->view('site/includes/header'); ?>
    <div id="mango-collection">
        <div class="breadcrumb-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ">
                        <ol class="breadcrumb breadcrumb-arrow hidden-sm hidden-xs">
                            <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                            <li><a href="<?php echo base_url('tin-tuc.html'); ?>"><?php echo $pageTitle; ?></a></li>
                            <li class="active"><span><?php echo $article['ArticleTitle']; ?></span></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="box_collection">
            <div class="container">
                <div class="information-blocks" >
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 col-sm-8 col-sm-push-4">
                            <div class="page-selector">
                                <div class="shop-grid-controls">
                                    <div class="entry collection_title">
                                        <h1><?php echo $article['ArticleTitle']; ?></h1>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="row shop-grid grid-view product-list filter">
                                <div class="col-md-12 col-sm-12 col-xs-12 shop-grid-item blog-posts">
                                    <div class="entry-meta-data">
                                        <!--<span class="author"><i class="fa fa-user"></i> by: <a href="#"><?php //echo $author['FullName']; ?></a></span>-->
                                        <span class="cat"><i class="fa fa-folder-o"></i><a href="<?php echo base_url('tin-tuc.html'); ?>"><?php echo $pageTitle; ?>, </a></span>
                                        <span class="date"><i class="fa fa-calendar"></i> <?php echo ddMMyyyy($article['PublishDateTime'], 'd/m/Y H:i'); ?></span>
                                    </div>
                                    <div class="content-text clearfix news-content">
                                        <?php echo $article['ArticleContent']; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9 col-sm-4 col-sm-pull-8 blog-sidebar">
                            <div class="information-blocks categories-border-wrapper">
                                <div class="block-title size-3">TIN TỨC MỚI NHẤT</div>
                                <div class="accordeon" id="menu-left" >
                                    <div class="block-content">
                                        <ul class="blog-list-sidebar clearfix">
                                            <?php foreach ($listArticlesNews  as $key =>  $a) {
                                                $url = $this->Mconstants->getUrl($a['ArticleSlug'], $a['ArticleId'], 5); ?>
                                                <li>
                                                    <div class="post-thumb">
                                                        <?php $image = IMAGE_PATH.(empty($a['ArticleImage']) ? NO_IMAGE : $a['ArticleImage']); ?>
                                                        <a href="<?php echo $url; ?>"><img width="70" height="49" src="<?php echo $image; ?>" alt="<?php echo $a['ArticleTitle']; ?>"></a>
                                                    </div>
                                                    <div class="post-info">
                                                        <h5 class="entry_title"><a href="<?php echo $url; ?>"><?php echo $a['ArticleTitle']; ?></a></h5>
                                                        <div class="post-meta">
                                                            <span class="date"><i class="fa fa-calendar"></i> <?php echo ddMMyyyy($a['PublishDateTime'], 'd/m/Y H:i'); ?></span>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('site/includes/footer'); ?>