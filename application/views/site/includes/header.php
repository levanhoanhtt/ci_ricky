<!doctype html>
<html lang="vi">
<head>
    <meta charset="utf-8"/>
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <![endif]-->
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url(); ?>" id="baseUrl"/>
    <?php $this->load->view("includes/favicon"); ?>
    <meta name="description" content="<?php echo $configSites['META_DESC']; ?>"/>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=0" name="viewport"/>
    <!--------------CSS----------->
    <link rel="stylesheet" href="assets/front/v1/js/plugins/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/front/v1/js/plugins/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="assets/front/v1/js/haravan/h_font/brandonnew/brandontext-font.min.css" rel="stylesheet" type="text/css" media="all"/>
    <!--<link href="assets/front/v1/css/sanfrancisco-font.css" rel="stylesheet" type="text/css" media="all"/>-->
    <link href="assets/front/v1/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="assets/front/v1/css/owl.theme.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="assets/front/v1/css/styles.css?20171109" rel="stylesheet" type="text/css" media="all"/>
    <link href="assets/front/v1/css/idangerous.swiper.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="assets/front/v1/css/customer-styles.css?20171031_1" rel="stylesheet" type="text/css" media="all"/>
    <link href="assets/front/v1/css/jquery.fancybox.css?20171023" rel="stylesheet" type="text/css" media="all"/>
    <link href="assets/front/v1/css/footer.css?20171023" rel="stylesheet" type="text/css" media="all"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="<?php echo $configSites['COMPANY_NAME']; ?>"/>
    <meta property="og:description" content="<?php echo $configSites['META_DESC']; ?>"/>

    <meta property="og:url" content="<?php echo $configSites['pageUrl']; ?>"/>
    <meta property="og:site_name" content="<?php echo $configSites['COMPANY_NAME']; ?>"/>

    <meta property="fb:app_id" content="233317707092594"/>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '625090514338697');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=625090514338697&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body class="style-18 <?php echo $bodyClass; ?>">
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!-- Your customer chat code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="377251992433126">
</div>
<div id="content-block">
    <div class="position-center">
        <div class="header-wrapper style-5 style-18">
            <header class="type-2">
                <div class="navigation-vertical-align">
                    <div class="cell-view logo-container">
                        <h1 style="display: none;">
                            <?php echo $configSites['COMPANY_NAME']; ?>
                        </h1>
                        <a id="logo" href="<?php echo base_url(); ?>"><img src="<?php echo empty($configSites['LOGO_IMAGE']) ? 'assets/front/v1/images/logoc466.png' : IMAGE_PATH.$configSites['LOGO_IMAGE']; ?>" alt="Logo"/></a>
                        <h1 style="display:none">
                            <a href="<?php echo base_url(); ?>"><?php echo $configSites['COMPANY_NAME']; ?></a>
                        </h1>
                    </div>
                    <div class="cell-view nav-container">
                        <div class="navigation">
                            <div class="navigation-header responsive-menu-toggle-class">
                                <div class="title">Menu</div>
                                <div class="close-menu"></div>
                            </div>
                            <div class="nav-overflow">
                                <nav>
                                    <ul>
                                        <li class="active"><a class=" active" href="<?php echo base_url(); ?>" title="TRANG CHỦ">TRANG CHỦ</a></li>
                                        <li class=" has-mega full-width-columns">
                                            <a href="<?php echo base_url('san-pham.html'); ?>">SẢN PHẨM</a>
                                            <i class="fa fa-chevron-down"></i>
                                            <div class="submenu">
                                                <div class="product-column-entry">
                                                    <div class="grid">
                                                        <a href="<?php echo base_url('combo-c1.html'); ?>">
                                                            <figure class="effect-bubba">
                                                                <img alt="COMBO" src="assets/front/v1/images/megamenu_2_image_2c466.jpg">
                                                                <div class="span1"></div>
                                                                <div class="span2"></div>
                                                                <div class="span3"></div>
                                                                <div class="span4"></div>
                                                            </figure>
                                                        </a>
                                                    </div>
                                                    <div class="submenu-list-title">
                                                        <a href="<?php echo base_url('combo-c1.html'); ?>">COMBO</a><span class="toggle-list-button"></span>
                                                    </div>
                                                </div>
                                                <div class="product-column-entry">
                                                    <div class="grid">
                                                        <a href="<?php echo base_url('micro-c2.html'); ?>">
                                                            <figure class="effect-bubba">
                                                                <img alt="MICRO" src="assets/front/v1/images/megamenu_2_image_3c466.jpg">
                                                                <div class="span1"></div>
                                                                <div class="span2"></div>
                                                                <div class="span3"></div>
                                                                <div class="span4"></div>
                                                            </figure>
                                                        </a>
                                                    </div>
                                                    <div class="submenu-list-title">
                                                        <a href="<?php echo base_url('micro-c2.html'); ?>">MICRO</a><span class="toggle-list-button"></span>
                                                    </div>
                                                </div>
                                                <div class="product-column-entry">
                                                    <div class="grid">
                                                        <a href="<?php echo base_url('sound-card-c3.html'); ?>">
                                                            <figure class="effect-bubba">
                                                                <img alt="SOUNDCARD" src="assets/front/v1/images/megamenu_2_image_4c466.jpg">
                                                                <div class="span1"></div>
                                                                <div class="span2"></div>
                                                                <div class="span3"></div>
                                                                <div class="span4"></div>
                                                            </figure>
                                                        </a>
                                                    </div>
                                                    <div class="submenu-list-title">
                                                        <a href="<?php echo base_url('sound-card-c3.html'); ?>">SOUNDCARD</a><span class="toggle-list-button"></span>
                                                    </div>
                                                </div>
                                                <div class="product-column-entry">
                                                    <div class="grid">
                                                        <a href="<?php echo base_url('phu-kien-c4.html'); ?>">
                                                            <figure class="effect-bubba">
                                                                <img alt="PHỤ KIỆN" src="assets/front/v1/images/megamenu_2_image_5c466.jpg">
                                                                <div class="span1"></div>
                                                                <div class="span2"></div>
                                                                <div class="span3"></div>
                                                                <div class="span4"></div>
                                                            </figure>
                                                        </a>
                                                    </div>
                                                    <div class="submenu-list-title">
                                                        <a href="<?php echo base_url('phu-kien-c4.html'); ?>">PHỤ KIỆN</a><span class="toggle-list-button"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <!--<li class=" has-mega full-width">
                                            <a href="#">FORUM</a>
                                            <i class="fa fa-chevron-down"></i>
                                            <div class="submenu">
                                                <div class="product-column-entry">
                                                    <div class="grid">
                                                        <a href="#">
                                                            <figure class="effect-bubba">
                                                                <img alt="Review" src="assets/front/v1/images/megamenu_1_image_2c466.jpg">
                                                            </figure>
                                                        </a>
                                                    </div>
                                                    <div class="submenu-list-title">
                                                        <a href="#">Review</a><span class="toggle-list-button"></span>
                                                    </div>
                                                    <div class="description toggle-list-container">
                                                        <ul class="list-type-1">

                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="product-column-entry">
                                                    <div class="grid">
                                                        <a href="#">
                                                            <figure class="effect-bubba">
                                                                <img alt="Tin tức" src="assets/front/v1/images/megamenu_1_image_3c466.jpg">
                                                            </figure>
                                                        </a>
                                                    </div>
                                                    <div class="submenu-list-title">
                                                        <a href="#">Tin tức</a><span class="toggle-list-button"></span>
                                                    </div>
                                                    <div class="description toggle-list-container">
                                                        <ul class="list-type-1">

                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="product-column-entry">
                                                    <div class="grid">
                                                        <a href="#">
                                                            <figure class="effect-bubba">
                                                                <img alt="Video" src="assets/front/v1/images/megamenu_1_image_4c466.jpg">
                                                            </figure>
                                                        </a>
                                                    </div>
                                                    <div class="submenu-list-title">
                                                        <a href="#">Video</a><span class="toggle-list-button"></span>
                                                    </div>
                                                    <div class="description toggle-list-container">
                                                        <ul class="list-type-1">

                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="product-column-entry">
                                                    <div class="grid">
                                                        <a href="#">
                                                            <figure class="effect-bubba">
                                                                <img alt="Hướng dẫn" src="assets/front/v1/images/megamenu_1_image_5c466.jpg">
                                                            </figure>
                                                        </a>
                                                    </div>
                                                    <div class="submenu-list-title">
                                                        <a href="#">Hướng dẫn</a><span class="toggle-list-button"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>-->
                                        <li><a href="<?php echo base_url('ho-tro'); ?>" title="HỖ TRỢ">HỖ TRỢ</a></li>
                                        <li><a href="<?php echo base_url('pages/lien-he'); ?>" title="LIÊN HỆ">LIÊN HỆ</a></li>
                                        <li><a href="<?php echo base_url('tin-tuc.html'); ?>" title="TIN TỨC">TIN TỨC</a></li>
                                        <li class="fixed-header-visible">
                                            <a href="javascript:void(0)" class="fixed-header-square-button open-search-popup"><i class="fa fa-search"></i></a>
                                            <!--<a href="account/login.html" class="fixed-header-square-button"><i class="fa fa-user"></i></a>-->
                                            <a href="<?php echo base_url('gio-hang.html'); ?>" class="fixed-header-square-button open-cart-popup"><i class="fa fa-shopping-cart"></i><span class="count_cart_page CartCount"><?php echo $totalItemCart; ?></span></a>
                                        </li>
                                    </ul>
                                    <div class="clear"></div>
                                    <a href="<?php echo base_url(); ?>" class="fixed-header-visible additional-header-logo"><img src="<?php echo empty($configSites['LOGO_IMAGE']) ? 'assets/front/v1/images/logoc466.png' : IMAGE_PATH.$configSites['LOGO_IMAGE']; ?>" alt="Logo"/></a>
                                </nav>
                            </div>
                        </div>
                        <div class="responsive-menu-toggle-class">
                            <a href="#" class="header-functionality-entry menu-button"><i class="fa fa-reorder"></i></a>
                            <a href="#" class="header-functionality-entry open-cart-popup"><i class="fa fa-shopping-cart"></i></a>
                            <a href="#" class="header-functionality-entry open-search-popup"><i class="fa fa-search"></i></a>
                        </div>
                    </div>
                </div>
                <div class="close-header-layer"></div>
            </header>
            <div class="clear"></div>
        </div>
    </div>