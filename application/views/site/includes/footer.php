<div class="footer-top">
            <div class="box-banner">
            </div>
            <div class="container">
               <div class="row">
                  <div class="col-md-2 col-12">
                     <div class="title">
                        <h2>Về chúng tôi </h2>
                     </div>
                     <div class="demo">
                        <ul id="demo">
                           <li><a href="<?php echo base_url() ?>/pages/gioi-thieu">Giới thiệu</a></li>
                           <li><a href="<?php echo base_url() ?>/pages/nguyen-tac-quy-dinh">Nguyên tắc - quy định</a></li>
                           <li><a href="<?php echo base_url() ?>/pages/thanh-toan-va-bao-mat-thong-tin">Thanh toán -bảo mật thông tin</a></li>
                           <li><a href="<?php echo base_url() ?>/pages/dieu-khoan-cam-ket">Điều khoản cam kết</a></li>
                           <li><a href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=43878"> <img
                              src="assets/front/v1/images/footerimg/dathongbao.png" alt=""></a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-2  col-12">
                     <div class="title">
                        <h2>CHÍNH SÁCH</h2>
                     </div>
                     <div class="demo">
                        <ul id="demo">
                           <li><a href="<?php echo base_url() ?>/pages/quyen-va-nghia-vu-cua-ban-quan-ly">Quản lí ricky</a></li>
                           <li><a href="<?php echo base_url() ?>/pages/khieu-nai">Góp ý kiến -khiếu nại</a></li>
                           <li><a href="<?php echo base_url() ?>/pages/quy-trinh-bao-hanh">Quy định và bảo hành</a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-4 col-12">
                     <div class="title logo " style="padding-left: 4rem;margin-bottom: 20px">
                        <div class="footer-logo">
                        </div>
                        <h2 style="font-size: 1.3rem; color: #ffed6e; padding-left: 30px">
                           <p>
                              RICKY VIỆT NAM
                           <p style="font-size: .8rem; font-family: Arial;font-style: italic; font-weight: 550; text-transform: initial; color: #3d8680">
                              Trao uy tín, nhận niềm tin
                           </p>
                           </p>
                        </h2>
                     </div>
                     <div class="demo">
                        <ul>
                           <li class="i">
                              <i class="fa fa-phone"></i>
                              <p style="font-size: 16px;font-weight: bold;padding-left: 20px">
                                 Hotline:<span style="color: #e9d966; font-size: 18px"> 0971.477.007</span>
                                 <br> <span style="color: #e1e1e1; font-style: italic;
                                    font-size: 12px; font-weight: 400">(Chăm sóc 24/7)</span>
                              </p>
                           </li>
                           <li class="i">
                              <i> <span class="clock"></span></i>
                              <p style="font-size: 16px;font-weight: bold;padding-left: 20px ;line-height: 20px">
                                 Thời gian làm việc
                                 <br>
                                 <span style="font-size: 12px;font-weight: 400">
                                 từ thứ 2 đến thứ 6: 8:00 - 18 :00
                                 </span>
                                 <br>
                                 <span style="font-size: 12px;font-weight: 400">Thứ bảy ,chủ nhật :Vui lòng hẹn trước</span>
                              </p>
                           </li>
                           <li >
                              <div class="mxh"> <a href="https://www.youtube.com/channel/UCK1L90-84fSNuFPbWDsjRIA"> <i class="fa fa-youtube-play"></i></i></a>
                                 <a href="https://www.facebook.com/rickystudiovn/"><i class="fa fa-facebook-f"></i></a>
                              </div>
                              <iframe class="facbooke" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Frickystudiovn%2F&tabs&width=350&height=250&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=true&appId" data-hiden-cover="false"  width="100%" height="241" style="border:none;overflow:hidden"    scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-4 col-12">
                     <div class="title map" style="padding-left: 42px">
                        <h2>GOOGLE MAP</h2>
                     </div>
                     <div class="demo">
                        <ul id="demo">
                           <li class="i" style="padding-left: 10px">
                              <i class="fa fa-map-marker" style="left: 0"></i></i>
                              <p style="font-size: 16px;font-weight: bold;padding-left: 34px;padding-right: 80px">
                                 Địa chỉ:
                                 <br>
                                 <span style="font-size: 12px;font-weight: 400">
                                 Nhà số 24/2, Ngõ 56 Trần Vỹ, Mai Dịch, Cầu Giấy, Hà Nội
                                 </span>
                              </p>
                           </li>
                           <li style="text-align: right" class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3723.801338885689!2d105.77092291445479!3d21.040633492749812!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1zTmjDoCBz4buRIDI0LzIsIE5nw7UgNTYgVHLhuqduIFbhu7ksIE1haSBE4buLY2gsIEPhuqd1IEdp4bqleSwgSMOgIE7hu5k!5e0!3m2!1svi!2s!4v1555838979419!5m2!1svi!2s" width="90%" height="325" frameborder="0" style="border:0" allowfullscreen></iframe></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div class="coppyright text-center" style="padding: 10px 0">
               <p style="font-size: 14px">CÔNG TY TNHH RICKY VIỆT NAM - GPĐKKD số 0107788880 do Sở KHĐT Hà Nội cấp ngày 04/04/2017 - MST: 0107788880 - Địa chỉ: Số 24/2 ngõ 56 Trần Vỹ, Mai Dịch, Cầu Giấy, Hà Nội</p>
            </div>
</div>
<div class="search-box popup">
    <form action="<?php echo base_url('tim-kiem.html'); ?>">
        <div class="search-button">
            <i class="fa fa-search"></i>
            <input type="submit"/>
        </div>
        <div class="search-field">
            <input type="hidden" name="type" value="product"/>
            <input type="text" value="" name="q" placeholder="Tìm kiếm..."/>
        </div>
    </form>
</div>
<div id="cartModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <div class="cart-box">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <div class="row">
                                            <div class="col-xs-5 col-sm-6">
                                                <h5 class="mgt-5"><span class="glyphicon glyphicon-shopping-cart"></span> Giỏ hàng</h5>
                                            </div>
                                            <div class="col-xs-7 col-sm-6">
                                                <a href="<?php echo base_url('san-pham.html'); ?>" class="btn btn-ricky btn btn-block">
                                                    <span class="glyphicon glyphicon-share-alt"></span> Tiếp tục mua hàng
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">

                                </div>
                                <hr>
                                <div class="row pd-15">
                                    <div>
                                        <div class="col-sm-6 col-sm-6">
                                            <input type="text" class="coupon-code text-center" placeholder="Mã giảm giá">
                                        </div>
                                        <div class="col-sm-3 col-sm-6 col-sm-offset-3 text-right">
                                            <button type="button" class="btn btn-default btn btn-block update-cart">
                                                Cập nhật lại giỏ hàng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer border-full">
                                <div class="well mg-15 open-form">
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <label>Họ tên</label>
                                            <input class="form-control focus-i" id="name" type="text">
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <label>Email</label>
                                            <input class="form-control focus-i" id="email" type="email">
                                        </div>
                                        <div class="col-sm-6" form-group>
                                            <label>Số điện thoại <span class="required">*</span></label>
                                            <input class="form-control focus-i" id="phone" type="text">
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <label>Địa chỉ</label>
                                            <input class="form-control focus-i" id="address" type="text">
                                        </div>
                                        <!-- <div class="col-sm-6 wrap-contries-select form-group">
                                            <label>Quốc gia</label>
                                        </div>
                                        <div class="col-sm-6 wrap-provinces-select form-group VNon">
                                            <label>Tỉnh/ Thành phố</label>
                                        </div>
                                        <div class="col-sm-6 form-group wrap-districts-select form-group VNon">
                                            <label>Quận huyện</label>
                                        </div>
                                        <div class="col-sm-6 form-group wrap-wards-select form-group VNon">
                                            <label>Phường / Xã</label>
                                        </div> -->
                                        <div class="col-sm-6 form-group VNoff" style="display: none">
                                            <label class="control-label">ZipCode</label>
                                            <input type="text" id="zipCode" class="form-control" value="">
                                        </div>
                                        <div class="col-sm-12 form-group">
                                            <label>Nhắn nhủ cho RICKY</label>
                                            <input class="form-control focus-i" id="note" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 text-center">
                                        <p class="checkout-info text-danger"></p>
                                    </div>
                                </div>
                                <div class="row text-center">
                                    <div class="col-xs-7 col-sm-9">
                                        <h4 class="text-right mgt-11">Tổng tiền:
                                            <strong class="totalMoney">0</strong></h4>
                                    </div>
                                    <div class="col-xs-5 col-sm-3">
                                        <button type="button" class="btn btn-ricky btn-block checkout">Mua hàng</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="text" hidden="hidden" id="getListWardUrl" value="<?php echo base_url('api/config/getListWard'); ?>">
<script src="assets/front/v1/js/plugins/jquery/jquery-1.11.3.min.js"></script>
<script src="assets/front/v1/js/plugins/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="assets/front/v1/js/js/option_selection.min.js"></script>
<script src="assets/front/v1/js/js/api.jquery.min.js"></script>
<script src="assets/front/v1/js/modernizr.min.js" type="text/javascript"></script>
<script src="assets/front/v1/js/theme-default/html5shiv.js"></script>
<script src="assets/front/v1/js/theme-default/jquery-migrate-1.2.0.min.js"></script>
<script src="assets/front/v1/js/theme-default/jquery.touchSwipe.min.js" type="text/javascript"></script>
<script src="assets/front/v1/js/js/haravan.plugin.1.0.min.js"></script>
<script src="assets/front/v1/js/idangerous.swiper.min.js" type="text/javascript"></script>
<script src="assets/front/v1/js/owl.carousel.js" type="text/javascript"></script>
<script src="assets/front/v1/js/global.js" type="text/javascript"></script>
<script src="assets/front/v1/js/jquery.mousewheel.js" type="text/javascript"></script>
<script src="assets/front/v1/js/jquery.jscrollpane.min.js" type="text/javascript"></script>
<script src="assets/front/v1/js/ajax-cart.js?20190118" type="text/javascript"></script>
<script src="assets/front/v1/js/scripts.js?20171031" type="text/javascript"></script>
<script src="assets/front/v1/js/jquery.fancybox.js" type="text/javascript"></script>
<?php if (isset($scriptFooter)) outputScript($scriptFooter); ?>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-89018846-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-89018846-1');
</script>
</body>
</html>