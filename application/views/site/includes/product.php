<?php $url = $this->Mconstants->getUrl($product['ProductSlug'], $product['ProductId'], 3); ?>
<div class="swiper-slide fix_box">
    <div class="paddings-container">
        <div class="product-slide-entry shift-image">
            <div class="product-image">
                <a href="<?php echo $url; ?>">
                    <?php $image = PRODUCT_PATH.(empty($product['ProductImage']) ? NO_IMAGE : $product['ProductImage']); ?>
                    <img src="<?php echo $image; ?>" alt="<?php echo $product['ProductName']; ?>" class="product-image">
                    <img src="<?php echo $image; ?>" alt="<?php echo $product['ProductName']; ?>" class="product-image-hover">
                </a>
                <a href="javascript:void(0)" title="Xem nhanh" class="top-line-a left btn-quickview-1" data-handle="<?php echo $url; ?>"><i class="fa fa-retweet"></i></a>
                <a href="<?php echo $url; ?>" title="Xem chi tiết" class="top-line-a right"><i class="fa fa-eye"></i></a>
                <div class="bottom-line">
                    <a href="javascript:void(0)" data-id="<?php echo $product['ProductId']; ?>" class="bottom-line-a Addcart"><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</a>
                </div>
            </div>
            <div class="product-info">
                <a class="title_product" href="<?php echo $url; ?>"><?php echo $product['ProductName']; ?></a>
                <div class="price">
                    <?php if($product['IsContactPrice'] == 2) { ?>
                        <div class="current">Giá liên hệ</div>
                    <?php } else { ?>
                        <?php if($product['OldPrice'] > 0){ ?><div class="prev"><?php echo priceFormat($product['OldPrice']); ?>₫</div><?php } ?>
                        <div class="current"><?php echo $product['Price'] > 0 ? priceFormat($product['Price']).'₫' : 'Giá liên hệ'; ?></div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>