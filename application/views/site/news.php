<?php $this->load->view('site/includes/header'); ?>
    <div id="mango-collection">
        <div class="breadcrumb-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ">
                        <ol class="breadcrumb breadcrumb-arrow hidden-sm hidden-xs">
                            <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                            <li class="active"><a href="<?php echo base_url('tin-tuc.html'); ?>"><?php echo $pageTitle; ?></a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="box_collection">
            <div class="container">
                <div class="information-blocks" >
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 col-sm-8 col-sm-push-4">
                            <div class="page-selector">
                                <?php if($pageCount > 1){ ?>
                                    <div class="pages-box hidden-xs hidden-sm">
                                        <div class="content_sortPagiBar pagi">
                                            <div id="pagination" class="clearfix">
                                                <ul class="pagination">
                                                    <?php if($pageCurrent > 1) echo '<li class="pagination_previous"><a href="'.str_replace('{$1}', $pageCurrent - 1, $categoryUrlPage).'"><i class="fa fa-chevron-left"></i></a></li>';
                                                    for($i = 1; $i <= $pageCount; $i++){
                                                        if($i == $pageCurrent) echo '<li class="active"><span>'.$i.'</span></li>';
                                                        else echo '<li><a href="'.str_replace('{$1}', $i, $categoryUrlPage).'">'.$i.'</a></li>';
                                                    }
                                                    if($pageCurrent < $pageCount) echo '<li class="pagination_next"><a href="'.str_replace('{$1}', $pageCurrent + 1, $categoryUrlPage).'"><i class="fa fa-chevron-right"></i></a></li>'; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="shop-grid-controls">
                                    <div class="entry collection_title">
                                        <h1><?php echo $pageTitle; ?></h1>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="row shop-grid grid-view product-list filter">
                                <div class="col-md-12 col-sm-12 col-xs-12 shop-grid-item blog-posts">
                                    <?php foreach ($listArticles as $a) {
                                    $url = $this->Mconstants->getUrl($a['ArticleSlug'], $a['ArticleId'], 5); ?>
                                        <div class="row">
                                        <div class="col-sm-5">
                                            <div class="entry-thumb image-hover2">
                                                <a href="<?php echo $url; ?>">
                                                    <?php $image = IMAGE_PATH.(empty($a['ArticleImage']) ? NO_IMAGE : $a['ArticleImage']); ?>
                                                    <img width="270" height="257" src="<?php echo $image; ?>" alt="<?php echo $a['ArticleTitle']; ?>">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="entry-ci">
                                                <h3 class="entry-title">
                                                <a href="<?php echo $url; ?>"><?php echo $a['ArticleTitle']; ?></a>
                                                </h3>
                                                <div class="entry-meta-data">
                                                    <span class="date"><i class="fa fa-calendar"></i> <?php echo ddMMyyyy($a['PublishDateTime'], 'd/m/Y H:i'); ?></span>
                                                </div>
                                                <div class="entry-excerpt">
                                                    <?php echo $a['ArticleLead']; ?>
                                                </div>
                                                <div class="entry-more">
                                                    <a href="<?php echo $url; ?>">Đọc thêm</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-pull-9 col-sm-4 col-sm-pull-8 blog-sidebar">
                            <div class="information-blocks categories-border-wrapper">
                                <div class="block-title size-3">TIN TỨC MỚI NHẤT</div>
                                <div class="accordeon" id="menu-left" >
                                    <div class="block-content">
                                        <ul class="blog-list-sidebar clearfix">
                                        <?php foreach ($listArticlesNews  as $key =>  $a) {
                                            $url = $this->Mconstants->getUrl($a['ArticleSlug'], $a['ArticleId'], 5); ?>
                                        <li>
                                            <div class="post-thumb">
                                                <?php $image = IMAGE_PATH.(empty($a['ArticleImage']) ? NO_IMAGE : $a['ArticleImage']); ?>
                                                <a href="<?php echo $url; ?>"><img width="70" height="49" src="<?php echo $image; ?>" alt="<?php echo $a['ArticleTitle']; ?>"></a>
                                            </div>
                                            <div class="post-info">
                                                <h5 class="entry_title"><a href="<?php echo $url; ?>"><?php echo $a['ArticleTitle']; ?></a></h5>
                                                <div class="post-meta">
                                                    <span class="date"><i class="fa fa-calendar"></i> <?php echo ddMMyyyy($a['PublishDateTime'], 'd/m/Y H:i'); ?></span>
                                                </div>
                                            </div>
                                        </li>
                                        <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('site/includes/footer'); ?>