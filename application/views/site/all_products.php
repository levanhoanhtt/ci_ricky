<?php $this->load->view('site/includes/header'); ?>
    <div id="mango-collection">
        <div class="breadcrumb-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ">
                        <ol class="breadcrumb breadcrumb-arrow hidden-sm hidden-xs">
                            <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                            <li><a href="<?php echo base_url('san-pham.html'); ?>">Danh mục</a></li>
                            <li class="active"><span><?php echo $pageTitle; ?></span></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="box_collection">
            <div class="container">
                <div class="information-blocks" style="border-top:1px solid #e6e6e6;">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 col-sm-8 col-sm-push-4">
                            <div class="row">
                                <div class="menu_horizontal">
                                    <ul id="menu-top">
                                        <?php $i = 0;
                                        foreach($listCategories as $c){
                                            $i++;
                                            if ($i < 5) { ?>
                                                <li<?php if($categoryId == $c['CategoryId']) echo ' class="active"'; ?>>
                                                    <a href="<?php echo $this->Mconstants->getUrl($c['CategorySlug'], $c['CategoryId'], 1); ?>">
                                                        <i class="fa fa-bars"></i> <?php echo $c['CategoryName']; ?>
                                                    </a>
                                                    <?php if(!empty($c['Childs'])){ ?>
                                                        <ul>
                                                            <?php foreach($c['Childs'] as $c1){ ?>
                                                            <li<?php if($categoryId == $c1['CategoryId']) echo ' class="active"'; ?>>
                                                                <a href="<?php echo $this->Mconstants->getUrl($c1['CategorySlug'], $c1['CategoryId'], 1); ?>">
                                                                    <?php echo $c1['CategoryName']; ?>
                                                                </a>
                                                            </li>
                                                            <?php } ?>
                                                        </ul>
                                                    <?php } ?>
                                                </li>
                                            <?php }
                                        } ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="page-selector">
                                <?php if($pageCount > 1){ ?>
                                <div class="pages-box hidden-xs hidden-sm">
                                    <div class="content_sortPagiBar pagi">
                                        <div id="pagination" class="clearfix">
                                            <ul class="pagination">
                                                <?php if($pageCurrent > 1) echo '<li class="pagination_previous"><a href="'.str_replace('{$1}', $pageCurrent - 1, $categoryUrlPage).'"><i class="fa fa-chevron-left"></i></a></li>';
                                                for($i = 1; $i <= $pageCount; $i++){
                                                    if($i == $pageCurrent) echo '<li class="active"><span>'.$i.'</span></li>';
                                                    else echo '<li><a href="'.str_replace('{$1}', $i, $categoryUrlPage).'">'.$i.'</a></li>';
                                                }
                                                if($pageCurrent < $pageCount) echo '<li class="pagination_next"><a href="'.str_replace('{$1}', $pageCurrent + 1, $categoryUrlPage).'"><i class="fa fa-chevron-right"></i></a></li>'; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="shop-grid-controls">
                                    <div class="entry collection_title">
                                        <h1><?php echo $pageTitle; ?></h1>
                                    </div>
                                    <!--<div class="entry">
                                        <div class="inline-text">Sắp xếp theo:</div>
                                        <div class="simple-drop-down">
                                            <select class="sort-by custom-dropdown__select custom-dropdown__select--white">
                                                <option value="manual">Sản phẩm nổi bật</option>
                                                <option value="price-ascending">Giá: Tăng dần</option>
                                                <option value="price-descending">Giá: Giảm dần</option>
                                                <option value="title-ascending">Tên: A-Z</option>
                                                <option value="title-descending">Tên: Z-A</option>
                                                <option value="created-ascending">Cũ nhất</option>
                                                <option value="created-descending">Mới nhất</option>
                                                <option value="best-selling">Bán chạy nhất</option>
                                            </select>
                                        </div>
                                    </div>-->
                                    <div class="entry">
                                        <div class="view-button active grid"><i class="fa fa-th"></i></div>
                                        <div class="view-button list"><i class="fa fa-list"></i></div>
                                    </div>
                                    <div class="entry form_collection hidden-sm hidden-xs">
                                        <form action="<?php echo base_url('tim-kiem.html'); ?>">
                                            <input type="hidden" name="type" value="product"/>
                                            <input type="text" name="q" placeholder="Tìm kiếm..." value="<?php echo isset($_GET['q']) ? $_GET['q'] : ''; ?>"/>
                                            <button type="submit" class="btn_s">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </form>
                                    </div>
                                    <div class="entry form_collection_2 hidden-md hidden-lg">
                                        <form action="<?php echo base_url('tim-kiem.html'); ?>">
                                            <input type="hidden" name="type" value="product"/>
                                            <input type="text" name="q" placeholder="Tìm kiếm..." value="<?php echo isset($_GET['q']) ? $_GET['q'] : ''; ?>"/>
                                            <input type="submit" class="btn_s">
                                        </form>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="row shop-grid grid-view product-list filter">
                                <?php foreach ($listProducts as $product) {
                                    $url = $this->Mconstants->getUrl($product['ProductSlug'], $product['ProductId'], 3); ?>
                                    <div class="col-md-3 col-sm-4 col-xs-6 shop-grid-item">
                                        <div class="product-slide-entry shift-image fix_box">
                                            <div class="paddings-container">
                                                <div class="product-slide-entry shift-image">
                                                    <div class="product-image">
                                                        <a href="<?php echo $url; ?>" title="<?php echo $product['ProductName'];?>">
                                                            <?php $image = PRODUCT_PATH.(empty($product['ProductImage']) ? NO_IMAGE : $product['ProductImage']); ?>
                                                            <img src="<?php echo $image; ?>" alt="<?php echo $product['ProductName']; ?>" class="product-image">
                                                            <img src="<?php echo $image; ?>" alt="<?php echo $product['ProductName']; ?>" class="product-image-hover">
                                                        </a>
                                                        <a href="javascript:void(0)" title="Xem nhanh" class="top-line-a left btn-quickview-1" data-handle="<?php echo $url; ?>"><i class="fa fa-retweet"></i></a>
                                                        <a href="<?php echo $url; ?>" title="Xem chi tiết" class="top-line-a right"><i class="fa fa-eye"></i></a>
                                                        <div class="bottom-line">
                                                            <a href="javascript:void(0)" data-id="<?php echo $product['ProductId']; ?>" class="bottom-line-a Addcart"><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</a>
                                                        </div>
                                                    </div>
                                                    <div class="product-info">
                                                        <a class="title_product" href="<?php echo $url; ?>" title="<?php echo $product['ProductName'];?>"><?php echo $product['ProductName'];?></a>
                                                        <div class="price">
                                                            <?php if($product['IsContactPrice'] == 2) { ?>
                                                                <div class="current">Giá liên hệ</div>
                                                            <?php } else { ?>
                                                                <?php if($product['OldPrice'] > 0){ ?><div class="prev"><?php echo priceFormat($product['OldPrice']); ?>₫</div><?php } ?>
                                                                <div class="current"><?php echo $product['Price'] > 0 ? priceFormat($product['Price']).'₫' : 'Giá liên hệ'; ?></div>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="product-listting">
                                                            <span class="vendor"><strong>Nhà cung cấp:</strong>
                                                                <?php echo $this->Mconstants->getObjectValue($listSuppliers, 'SupplierId', $product['SupplierId'], 'SupplierName'); ?>
                                                            </span>
                                                            <span class="sku"><strong>Mã sản phẩm:</strong> <?php echo $product['Sku'];?></span>
                                                            <div class="short-desc">
                                                                <?php echo $product['ProductShortDesc']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php if($pageCount > 1){ ?>
                            <div class="page-selector content_sortPagiBar pagi">
                                <div id="pagination" class="clearfix">
                                    <ul class="pagination">
                                        <?php if($pageCurrent > 1) echo '<li class="pagination_previous"><a href="'.str_replace('{$1}', $pageCurrent - 1, $categoryUrlPage).'"><i class="fa fa-chevron-left"></i></a></li>';
                                        for($i = 1; $i <= $pageCount; $i++){
                                            if($i == $pageCurrent) echo '<li class="active"><span>'.$i.'</span></li>';
                                            else echo '<li><a href="'.str_replace('{$1}', $i, $categoryUrlPage).'">'.$i.'</a></li>';
                                        }
                                        if($pageCurrent < $pageCount) echo '<li class="pagination_next"><a href="'.str_replace('{$1}', $pageCurrent + 1, $categoryUrlPage).'"><i class="fa fa-chevron-right"></i></a></li>'; ?>
                                    </ul>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="col-md-3 col-md-pull-9 col-sm-4 col-sm-pull-8 blog-sidebar">
                            <div class="information-blocks categories-border-wrapper">
                                <div class="block-title size-3">DANH MỤC SẢN PHẨM</div>
                                <div class="accordeon" id="menu-left" style="padding-left:20px;">
                                    <?php foreach($listCategories as $c){
                                        $class = '';
                                        if($categoryId == $c['CategoryId']) $class = ' active';
                                        if(empty($c['Childs'])) $class .= ' no-child'; ?>
                                        <div class="accordeon-title<?php echo $class; ?>">
                                            <a href="<?php echo $this->Mconstants->getUrl($c['CategorySlug'], $c['CategoryId'], 1); ?>"><?php echo $c['CategoryName']; ?></a>
                                            <?php if(!empty($c['Childs'])){ ?>
                                                <div class="accordeon-entry">
                                                    <div class="article-container style-1">
                                                        <ul>
                                                            <?php foreach($c['Childs'] as $c1){ ?>
                                                                <li<?php if($categoryId == $c1['CategoryId']) echo ' class="active"'; ?>>
                                                                    <a href="<?php echo $this->Mconstants->getUrl($c1['CategorySlug'], $c1['CategoryId'], 1); ?>">
                                                                        <?php echo $c1['CategoryName']; ?>
                                                                    </a>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('site/includes/footer'); ?>