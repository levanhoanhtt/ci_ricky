<?php $this->load->view('site/includes/header'); ?>
    <div id="mango-product">
        <div class="breadcrumb-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ">
                        <ol class="breadcrumb breadcrumb-arrow hidden-sm hidden-xs">
                            <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                            <li><a href="<?php echo base_url('tin-tuc.html'); ?>">Bài viết</a></li>
                            <li class="active"><span> <?php echo $article['ArticleTitle']; ?></span></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="box_collection">
            <div class="container">
                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 information-entry">
                            <div class="product-detail-box article-box">
                                <h1 class="product-title"><?php echo $article['ArticleTitle']; ?></h1>
                                <hr>
                                <div class="product-description detail-info-entry frame_des"><?php echo $article['ArticleContent']; ?></div>
                                <div class="product_socaial">
                                    <div class="box_social">
                                        <div class="fb">
                                            <div class="fb-like" data-href="<?php echo $articleUrl; ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                        </div>
                                        <div class="gg">
                                            <div class="g-plus" data-action="share" data-annotation="none" data-href="<?php echo $articleUrl; ?>"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('site/includes/footer'); ?>