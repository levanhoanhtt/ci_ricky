<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content-header">
            <h1><?php echo $title; ?></h1>
            <ul class="list-inline">
                <li><button class="btn btn-primary submit">Lưu</button></li>
                <li><a href="<?php echo base_url('speedsms'); ?>" class="btn btn-default">Đóng</a></li>
            </ul>
        </section>
        <section class="content new-box-stl ft-seogeo">
        	<div class="row">
        		<div class="col-sm-12">
        			<div class="box box-default">
                        <div class="box-body">
	                    	<div class="panel-body">
	                    		<div class="form-group">
	                                <label class="control-label">Gửi:</label>
	                                <input type="text" class="form-control" id="phoneNumber" placeholder="Nhập số điện thoại người nhận">
	                            </div>
	                            <div class="form-group">
	                                <label class="control-label">Hoặc chọn nhóm bên dưới</label>
	                                <?php $this->Mconstants->selectObject($listCustomerGroups, 'CustomerGroupId', 'CustomerGroupName', 'CustomerGroupId', 0, true, 'Nhóm khách hàng', ' select2'); ?>
	                            </div>
	                            <div class="form-group">
	                                <label class="control-label">Nội dung: (<i>Bạn có thể soạn nội dung tiếng việt có dấu</i>)</label>

	                            </div>
	                            <div class="form-group pull-right">
	                            	<label class="control-label">Cá nhân hóa nội dung: </label>
	                            	<div class="btn-group">
									  <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-phone"></span> Thêm số DĐ</button>
									  <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-user"></span> Thêm tên</button>
									  <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-envelope"></span> Thêm email</button>
									  <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-calendar"></span> Thêm birthday</button>
									  <button type="button" class="btn btn-primary"><span class=" glyphicon glyphicon-plus"></span> Thông tin khác</button>
									</div>

	                            </div>
	                            <div class="form-group">
	                            	<textarea class="form-control" rows="5" id="message"></textarea>
	                            	<label class="control-label"><span id="character_number">0</span>/160 ký tự - <span id="number_sms">0</span> SMS</label>
	                            </div>
	                            <div class="form-group" style="text-align: center">
	                            	<div class="btn-group ">
	                            		<?php $this->Mconstants->selectObject($listSmscampaigns, 'SMSCampaignId', 'SMSCampaignName', 'SMSCampaignId', 0, true, 'Chọn chiến dịch đã có', ' select2'); ?>
									</div>
									<div class="btn-group">
										<button type="button" class="btn btn-default" id="sample_sentences"><span class="glyphicon glyphicon-bullhorn"></span><br/>Chọn mẫu câu có sẵn</button>
									</div>
	                            </div>
	                            <div class="hide_campaign">
	                            	<div class="form-group">
		                            	<label>Tên chiến dịch</label>
		                            	<input type="text" id="sMSCampaignName" class="form-control hmdrequired" value="" placeholder="Tên chiến dịch">
		                            </div>
		                            <div class="form-group">
		                            	<label class="control-label">Mô tả chiến dịch</label>
		                            	<textarea class="form-control" rows="5" id="sMSCampaignDesc"></textarea>
		                            </div>
	                            </div>
	                            
	                    	</div>
	                    </div>
                    </div>
                    
        		</div>
        	</div>
        	<ul class="list-inline pull-right margin-right-10">
	            <li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
	            <li><a href="<?php echo base_url('speedsms'); ?>" id="smsListUrl" class="btn btn-default">Đóng</a></li>
	            <input type="text" hidden="hidden" value="<?php echo base_url('speedsms/sendSms') ?>" id="sendSmsUrl">
	            <input type="hidden" id="getUrlContentSentences" value="<?php echo base_url('speedsms/getContentSentences') ?>">
	        </ul>
	        <?php $this->load->view('speedsms/modal'); ?>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>