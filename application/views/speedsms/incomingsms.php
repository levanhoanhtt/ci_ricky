<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('speedsms/listIncoming'); ?>
                        <div class="row">
                            <div class="col-sm-4">
                        		<div class="form-group">
                                    <div class='input-group' >
                                        <input type='text' class="form-control daterangepicker" name="DateRangePicker" value="<?php echo set_value('DateRangePicker');?>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                            	<input type="text" class="form-control" name="SearchText" value="<?php echo set_value('SearchText'); ?>" placeholder="Số điện thoại">
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content upn ft-seogeo">
                 <div class="">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Ngày giờ nhận</th>
                                <th>Số điện thoại</th>
                                <th>Nội dung tin nhắn</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyIncomingsms">
                            <?php
                            foreach($listIncomingsms as $a){ ?>
                                <tr>
                                    <td><?php echo getDayDiffText($a['DayDiff']) .' '. $a['CrDateTime']; ?></td> 
                                    <td><?php echo $a['PhoneNumber']; ?></td>
                                    <td><?php echo $a['SMSContent']; ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(isset($paggingHtml)){ ?>
                            <tr>
                                <td colspan="5"><?php echo $paggingHtml; ?></td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>