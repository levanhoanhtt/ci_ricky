<div class="modal fade" id="modalSampleSentences" tabindex="-1" role="dialog" aria-labelledby="modalSampleSentences" data-backdrop="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
                <div class="form-horizontal">
                
                	<div class="form-group">
                		<h4 class="modal-title col-sm-4">Chọn ngôi theo mẫu câu</h4>
					    <label for="select1" class="col-sm-1 control-label" > Mình </label>
					    <div class="col-sm-2">
					      <select class="form-control" id="customer_care">
                            <option value="">--Chọn--</option>
					      	<option value="Tôi">Tôi</option>
                            <option value="Anh">Anh</option>
                            <option value="Em">Em</option>
                            <option value="Chế">Chế</option>
                            <option value="Cửa hàng">Cửa hàng</option>
                            <option value="Shop">Shop</option>
					      </select>
					    </div>
					    <label for="select2" class="col-sm-2 control-label" > Khách hàng </label>
					    <div class="col-sm-2">
					       <select class="form-control" id="customers_need_care">
                            <option value="">--Chọn--</option>
					      	<option value="Anh">Anh</option>
                            <option value="Chị">Chị</option>
                            <option value="Chế">Chế</option>
                            <option value="Cửa hàng">Cửa hàng</option>
					      </select>
					    </div>
					  </div>
                </div>
                
            </div>
            <div class="modal-body">
                <div class="row">
                	<div class="col-sm-6">
                		<div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" class="form-control search-entences">
                            <span class="input-group-addon"><i class="fa fa-tags"></i></span>
                        </div>
                	</div>
                    

                	<div class="col-sm-6 form-horizontal">
                		<div class="form-group">
                			<label for="select1" class="col-sm-6 control-label"> Lọc theo nhóm: </label>
						    <div class="col-sm-6">
						      <select class="form-control" id="sentenceGroupId">
						      	<option value="0">Tất cả</option>
                                <?php foreach ($listSentenceGroups as $key => $value) { ?>
                                <option value="<?php echo $value["SentenceGroupId"]; ?>"><?php echo $value["SentenceGroupName"]; ?></option>
                                <?php } ?>
						      </select>
						    </div>
                		</div>
                	</div>
                	<div class="col-sm-12">
                		<table class="table table-hover " width="100%">
	                		<tbody id="tbodyContent">
	                			<!-- <tr>
	                				<td><label>test</label><p>aaa</p></td>
	                			</tr>
	                			<tr>
	                				<td><label>test</label><p>aaa</p></td>
	                			</tr> -->
	                		</tbody>
	                	</table>
                	</div>
                	
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="">Quản lý</button>
              <!--   <button type="button" class="btn btn-default" data-dismiss="modal">Hủy bỏ</button>
                <input type="text" hidden="hidden" id="productChildId" value="0">
                <input type="text" hidden="hidden" id="productChildIndex" value="0"> -->
            </div>
        </div>
    </div>
</div>