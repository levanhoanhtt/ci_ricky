<?php $url = $this->Mconstants->getUrl($product['ProductSlug'], $product['ProductId'], 3, 2); ?>
<div class="product-top">
    <div class="image">
        <a href="<?php echo $url; ?>"><img src="<?php echo PRODUCT_PATH.$product['ProductImage']; ?>" alt="<?php echo $product['ProductName']; ?>"/></a>
        <a data-fancybox="modal" data-src="#modal<?php echo $product['ProductId']; ?>" href="javascript:;" class="tool">Xem nhanh</a>
    </div>
    <div class="box-text">
        <p><a href="<?php echo $url; ?>"><?php echo $product['ProductName']; ?></a></p>
        <div class="star-rating"><span style="width: 90%;"></span></div>
        <span class="price">
            <?php if($product['IsContactPrice'] == 2) { ?>
                <div class="current">Giá liên hệ</div>
            <?php } else { ?>
                <?php if($product['OldPrice'] > 0){ ?><div class="prev"><?php echo priceFormat($product['OldPrice']); ?><span class="dv">₫</span></div><?php } ?>
                <div class="current"><?php echo $product['Price'] > 0 ? priceFormat($product['Price']).'<span class="dv">₫</span>' : 'Giá liên hệ'; ?></div>
            <?php } ?>
        </span>
    </div>
</div>
<div class="content-pop display-none" id="modal<?php echo $product['ProductId']; ?>">
    <div class="row pd15 top-product mb15 ">
        <div class="col-md-6 left-pro mb15">
            <div class="img-pro">
                <img src="<?php echo PRODUCT_PATH.$product['ProductImage']; ?>" alt="<?php echo $product['ProductName']; ?>"/>
            </div>
        </div>
        <div class="col-md-6 right-product mb15">
            <h2 class="ttl-detail"><?php echo $product['ProductName']; ?></h2>
            <div class="clearfix inner-left">
                <div class="star-rating"><span style="width: 90%;"></span></div>
            </div>
            <div class="price-detail">
                <?php if($product['IsContactPrice'] == 2) { ?>
                    <div class="current">Giá liên hệ</div>
                <?php } else { ?>
                    <?php if($product['OldPrice'] > 0){ ?><div class="prev"><?php echo priceFormat($product['OldPrice']); ?><span>₫</span></div><?php } ?>
                    <div class="current"><?php echo $product['Price'] > 0 ? priceFormat($product['Price']).'<span>₫</span>' : 'Giá liên hệ'; ?></div>
                <?php } ?>
            </div>
            <?php echo $product['ProductShortDesc']; ?>
            <input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
            <input type="text" hidden="hidden" id="productChildChosen" value="0">

            <div class="clearfix qual-bt">

                <div class="qualty">
                    <a href="#" class="des click-down">-</a>
                    
                    <input type="text" class="number" value="1" >

                    <a href="#" class="inc click-up">+</a>

                </div>
                <button type="button" class="btn-cmn btn-add-cart detail-p" data-id="<?php echo $product['ProductId'];?>">Thêm vào giỏ</button>
            </div>
        </div>
    </div>
</div>