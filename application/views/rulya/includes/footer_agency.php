<footer>
    <div class="container">
        <div class="row pd15">
            <div class="col-footer col-sm-6 col-md-3">
                <h3>VỀ CHÚNG TÔI – RULYA</h3>
                <ul>
                    <li><a class="trans" href="#">Giới thiệu chung</a></li>
                    <li><a class="trans" href="#">Tầm nhìn RULYA</a></li>
                    <li><a class="trans" href="#">Sứ mệnh RULYA</a></li>
                    <li><a class="trans" href="#">Giá trị cốt lõi RULYA</a></li>
                </ul>
            </div>
            <div class="col-footer col-sm-6 col-md-3">
                <h3>THAM GIA CÙNG CHÚNG TÔI</h3>
                <ul>
                    <li><a class="trans" href="#">Giới thiệu chung về hệ thống</a></li>
                    <li><a class="trans" href="#">Chính sách dành cho Đại lý</a></li>
                    <li><a class="trans" href="#">Đào tạo kinh doanh cho Đại lý</a></li>
                    <li><a class="trans" href="#">Danh sách các Đai lý</a></li>
                </ul>
            </div>
            <div class="col-footer col-sm-6 col-md-3">
                <h3>CTY TNHH RULYA BEAUTY VIỆT NAM</h3>
                <ul>
                    <li><p>Địa chỉ: 125 Trần Phú, Hà Đông, Hà Nội</p></li>
                    <li><p>Hotline: 0966.999.316</p></li>
                    <li><p>Email: nguyendinhtruongceo@gmail.com</p></li>
                </ul>
            </div>
            <div class="col-footer col-sm-6 col-md-3">
                <h3>CHÚNG TÔI TRÊN FACEBOOK</h3>
                <div class="textwidget">
                    <div class="fb-page" data-href="https://www.facebook.com/myphamrulya/" data-tabs="timeline"
                         data-height="170" data-small-header="true" data-adapt-container-width="true"
                         data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/myphamrulya/" class="fb-xfbml-parse-ignore"><a
                                href="https://www.facebook.com/myphamrulya/">RULYA</a></blockquote>
                    </div>
                    <div id="fb-root"></div>
                    <script>(function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=784703888255915';
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">Copyright 2018 © <span class="bold">CÔNG TY TNHH RULYA BEAUTY VIỆT NAM</span></div>
    </div>
</footer>
<script src="assets/front/rulya/js/jquery-3.2.1.min.js"></script>
<script src="assets/front/rulya/js/modernizr.js"></script>
<script src="assets/front/rulya/js/bootstrap.js"></script>
<script src="assets/front/rulya/js/wow.min.js"></script>
<script src="assets/front/rulya/js/jquery.bxslider.min.js"></script>
<script src="assets/front/rulya/js/jquery-ui.min.js"></script>
<script src="assets/front/rulya/js/jquery.zoom.js"></script>
<script src="assets/front/rulya/js/jquery.fancybox.js"></script>
<script src="assets/front/rulya/js/scripts.js"></script>
<?php if (isset($scriptFooter)) outputScript($scriptFooter); ?>
<script>
    new WOW().init();
</script>
</body>
</html>