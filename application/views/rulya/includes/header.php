<!DOCTYPE html>
<html lang="vi" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url(); ?>" id="baseUrl"/>
    <?php $this->load->view("includes/favicon"); ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&amp;subset=vietnamese" rel="stylesheet">
    <link rel="stylesheet" href="assets/front/rulya/css/bootstrap.css">
    <link rel="stylesheet" href="assets/front/rulya/css/animate.css">
    <link rel="stylesheet" href="assets/front/rulya/css/jquery-ui.css">
    <link rel="stylesheet" href="assets/front/rulya/css/jquery.fancybox.css">
    <link rel="stylesheet" href="assets/front/rulya/css/style.css">
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="<?php echo $configSites['COMPANY_NAME']; ?>"/>
    <meta property="og:description" content="<?php echo $configSites['META_DESC']; ?>"/>
    <meta property="og:url" content="<?php echo $configSites['pageUrl']; ?>"/>
    <meta property="og:site_name" content="<?php echo $configSites['COMPANY_NAME']; ?>"/>
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cleartype" content="on">
    <![endif]-->
</head>
<body>
<?php 
    $totalMoney = 0;
    foreach ($totalPriceCart as $key => $item) {
        $totalMoney += $item['price'] * $item['qty'];
} ?>
<header>
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-xs-12"></div>
                <div class="col-sm-8 col-xs-12">
                    <ul class="clearfix">
                        <li><a href="<?php echo base_url('tai-khoan'); ?>">Đăng nhập / Đăng ký</a></li>
                        <li class="line">&nbsp;</li>
                        <li><a href="<?php echo base_url('gio-hang'); ?>">
                             <span  hidden="hidden" class="span_total_price"><?php echo $totalMoney; ?></span>
                            <span class="count_cart_page CartCount"><?php echo $totalItemCart; ?> Giỏ hàng / <?php echo priceFormat($totalMoney); ?>₫</span><i class="icon-shopping-basket" data-icon-label="0"></i></a></li>
                        <li class="line">&nbsp;</li>
                        <li class="header-search"><a href="javascript:;" class="is-small"><i class="icon-search"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="overlay"></div>
    <div class="main-header">
        <div class="container">
            <div class="bt-menu sm" id="bt-menu"><i class="icon-menu"></i></div>

            <div class="btn-close" id="btn-close">×</div>
            <h1 class="logo">
                <a href="<?php echo base_url(); ?>"><img src="assets/front/rulya/img/logo.png" alt="Rulya"></a>
            </h1>
            <ul class="nav" id="navigation">
                <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                <li>
                    <a href="<?php echo base_url('gioi-thieu-chung'); ?>" class="has-arrow">Về chúng tôi<i class="icon-angle-down"></i></a>
                    <ul class="nav-dropdown">
                        <li><a href="<?php echo base_url('gioi-thieu-chung'); ?>">Giới thiệu chung</a></li>
                        <li><a href="<?php echo base_url('tam-nhin-su-menh'); ?>">Tầm nhìn - Sứ mệnh</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo base_url('shop'); ?>" class="has-arrow">Sản phẩm<i class="icon-angle-down"></i></a>
                    <ul class="nav-dropdown">
                        <?php foreach($listProductCategories as $c){ ?>
                            <li><a href="<?php echo $this->Mconstants->getUrl($c['CategorySlug'], $c['CategoryId'], 1, 2); ?>"><?php echo $c['CategoryName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li><a href="<?php echo base_url('academy'); ?>">Học viện RULYA</a></li>
                <li>
                    <a href="<?php echo base_url('dai-ly'); ?>" class="has-arrow">Đại lý<i class="icon-angle-down"></i></a>
                    <ul class="nav-dropdown">
                        <li><a href="<?php echo base_url('dai-ly/dang-ky-lam-dai-ly'); ?>">Đăng ký làm đại lý</a></li>
                        <li><a href="<?php echo base_url('dai-ly/gioi-thieu-chung-ve-he-thong'); ?>">Giới thiệu chung về hệ thống</a></li>
                        <li><a href="<?php echo base_url('dai-ly/chinh-sach-danh-cho-dai-ly'); ?>">Chính sách dành cho đại lý</a></li>
                        <li><a href="<?php echo base_url('dai-ly/dao-tao-kinh-doanh-cho-dai-ly'); ?>">Đào tạo kinh doanh cho đại lý</a></li>
                        <li><a href="<?php echo base_url('dai-ly/danh-sach-cac-dai-ly'); ?>">Danh sách các đại lý</a></li>
                        <li><a href="<?php echo base_url('dai-ly/leader'); ?>">LEADER DIAMOND</a></li>
                        <li><a href="<?php echo base_url('dai-ly/star'); ?>">RULYA STAR</a></li>
                    </ul>
                </li>
                <li class="blog"><a href="<?php echo base_url('blog'); ?>">Blog</a></li>
                <li><a href="<?php echo base_url('lien-he'); ?>">Liên hệ</a></li>
            </ul>
        </div>
    </div>
</header>