<?php $this->load->view('rulya/includes/header'); ?>
<div class="main">
    <div class="wrapper">
        <div class="container">
            <div class="content-vision">
                <div class="row">
                    <div class="col-md-9 col-sm-9 border-right">
                        <div class="page-inner">
                            <?php echo $articleContent; ?>
                            <!--<h2 class="ttl-vision">1. TẦM NHÌN</h2>
                            <p>RULYA trở thành tập đoàn kinh tế hàng đầu Việt Nam trong lĩnh vực mỹ phẩm vào năm
                                2022 và vươn ra quốc tế.</p>
                            <h2 class="ttl-vision">2. SỨ MỆNH</h2>
                            <ul>
                                <li>Đối với khách hàng: <strong>PHỤNG SỰ KHÁCH HÀNG</strong> trên cả sự mong đợi,
                                    lấy khách hàng làm trung tâm, mang đến cho khách hàng vẻ đẹp tự nhiên rạng ngời
                                </li>
                                <li>Đối với hệ thống đại lý: <strong>“SỨ MỆNH 10.000”</strong> người thành công,
                                    Thành công của đại lý là Nguồn động lực của RULYA
                                </li>
                                <li>Đối với đội nhóm:<strong> ĐỘI NHÓM VÔ ĐỊCH</strong>, Đoàn kết, coi nhau là anh
                                    em một nhà trong gia đình chung RULYA
                                </li>
                                <li>Đối với nhân viên: <strong>TINH THẦN CHIẾN BINH</strong>, không ngừng nghiên cứu
                                    nâng cao trình độ chuyên môn, tôi luyện Giá trị cốt lõi, luôn vươn lên trở thành
                                    nhân tố xuất sắc nhất, quan trọng nhất của tổ chức.
                                </li>
                                <li>Đối với cổ đông: <strong>Gia tăng lợi ích bền vững</strong></li>
                                <li>Đối với đối tác: Hợp tác win win</li>
                                <li>Đối với xã hội: Đóng góp tích cực trong các hoạt động cộng đồng để góp phần xây
                                    dựng một xã hội văn minh, một cộng đồng phát triển.
                                </li>
                            </ul>
                            <h2 class="ttl-vision">3. GIÁ TRỊ CỐT LÕI</h2>
                            <ul>
                                <li><strong>HẠNH</strong>: Mang đến hạnh phúc cho khách hàng, đồng đội, mọi người
                                    xung quanh và bản thân
                                </li>
                                <li><strong>TÂM:</strong> Trái tim và Lẽ phải</li>
                                <li><strong>TRÍ:</strong> Dám nghĩ dám làm, nói được làm được, sáng tạo là đòn bẩy
                                    thành công tuyệt đỉnh
                                </li>
                                <li><strong>TÍN:</strong> Chữ tín làm vũ khí cạnh tranh sắc nhọn, giữ chữ tín như
                                    giữ mạng
                                </li>
                                <li><strong>TỐC: </strong>RULYA&nbsp; đề cao 2 chữ “Tốc độ” – khả năng “Quyết định
                                    nhanh – Đầu tư nhanh – Triển khai nhanh – Bán hàng nhanh – Thay đổi và thích ứng
                                    nhanh “. RULYA coi trọng tốc độ nhưng “nhanh không ẩu”
                                </li>
                                <li><strong>TINH:</strong> “Dải Ngân Hà ”. RULYA coi nhân sự phù hợp là tài sản quý
                                    giá nhất, tạo điều kiện cho nhân sự phát triển hết khả năng của mình và luôn có
                                    mục tiêu tập hợp nhân tài về tổ chức để xây dựng nên 1 đế chế kinh doanh hùng
                                    mạnh vươn ra quốc tế năm châu.
                                </li>
                                <li><strong>QUANG</strong>: Quang là ánh sáng, chiếu sáng. HẠNH TÂM TRÍ TÍN TỐC TINH
                                    QUANG là giá trị cốt lõi trong mỗi con người RULYA. Giá trị này thực đẹp và lớn
                                    lao khi nó lan tỏa Giá trị tới mọi khách hàng ở muôn nơi.
                                </li>
                            </ul>-->
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <?php $this->load->view('rulya/includes/news'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('rulya/includes/footer'); ?>