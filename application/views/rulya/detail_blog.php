<?php $this->load->view('rulya/includes/header'); ?>
<div class="main">
    <div class="box-page-detail-blog">
        <div class="container">
            <ul>
                <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                <li><span>»</span></li>
                <li><a href="<?php echo base_url('blog'); ?>">Blog</a></li>
                <li><span>»</span></li>
                <li><?php echo $article['ArticleTitle']; ?></li>
            </ul>
            <h2 class="ttl-detail-blog"><?php echo $article['ArticleTitle']; ?></h2>
            
        </div>
    </div>
    <div class="wrapper">
        <div class="container">
            <div class="content-blog">
                <div class="row">
                    <div class="col-md-9 col-sm-9 border-right">
                        <div class="entry-content">
                            <?php echo $article['ArticleContent']; ?>

                        </div>
                        <div class="social-icons text-center blo">
                            <a href="#" class="trans face"><i class="icon-facebook"></i></a>
                            <a href="#" class="trans twitter"><i class="icon-twitter"></i></a>
                            <a href="#" class="trans envelop"><i class="icon-envelop"></i></a>
                            <a href="#" class="trans pinterest"><i class="icon-pinterest"></i></a>
                            <a href="#" class="trans google"><i class="icon-google-plus"></i></a>
                            <a href="#" class="trans linkedin"><i class="icon-linkedin"></i></a>
                        </div>
                        <div class="text-center text-borkmark">
                            This entry was posted in <a class="link" href="<?php echo base_url('blog'); ?>">Blog</a>. Bookmark the <a href="#" class="link">permalink</a>.
                        </div>
                        <!--<div class="next-prev-nav">
                            <a class="prev trans" href="#"><i class="icon-angle-left"></i>Ngạc nhiên với công dụng làm
                                đẹp tuyệt vời của tinh dầu bưởi</a>
                            <a class="next trans" href="#"><i class="icon-angle-right"></i>Sạch bay mụn ẩn với phương
                                thức làm đẹp từ sữa ong chúa</a>
                        </div>-->
                        <div class="comment-blog">
                            <h3 class="ttl-comment-blog">Trả lời </h3>
                            <p>Thư điện tử của bạn sẽ không được hiển thị công khai. Các trường bắt buộc được đánh dấu *</p>
                            <p>Bình luận</p>
                            <textarea class="text-area" id="comment"></textarea>
                            <div class="row pd15">
                                <div class="col-md-4"><p>Tên *</p><input type="text" class="input-text" id="customerName"></div>
                                <div class="col-md-4"><p>Thư điện tử *</p><input type="text" class="input-text" id="customerEmail"></div>
                                <div class="col-md-4"><p>Trang web</p><input type="text" class="input-text" id="customerWeb"></div>
                            </div>
                            <br/>
                            <button class="btn-cmn" type="button">phản hồi</button>
                            <div class="alert alert-danger" id="commentAlert" style="margin-top: 10px;display: none;"></div>
                            <input type="text" hidden="hidden" name="" id="id_article" value=" <?php echo $article['ArticleId']; ?>">
                            <input type="hidden" id="insertCommentUrl" value="<?php echo base_url('comment/update'); ?>">
                        </div>

                    </div>
                    <div class="col-md-3 col-sm-3">
                        <?php $this->load->view('rulya/includes/news'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('rulya/includes/footer'); ?>
