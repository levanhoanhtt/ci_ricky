<?php $this->load->view('rulya/includes/header'); ?>
<div class="main">

  <div class="wrapper">
    <div class="container">
      <div class="content-account">
        <h2 class="ttl-account">Giỏ hàng</h2>
        <div class="row pd15">
          <div class="col-md-9  mb20">
            <div class="over-auto">
            <table class="tb-cart mb20">
              <thead>
                <tr>
                  <th colspan="3">Sản phẩm</th>
                  <th>Giá</th>
                  <th>Số lượng</th>
                  <th>Tổng cộng</th>
                </tr>
              </thead>
              <tbody class="tbody_cart_product">
                  <?php $totalMoney = 0;
                    foreach ($carts as $item): ?>
                    <?php $totalMoney += $item['price'] * $item['qty']; ?>
               <tr class="product-item" data-child="<?php echo $item['product_child'] ?>" data-id="<?php echo $item['id'] ?>">
                      <td><a href="#" class="remove delcart" data-id="<?php echo $item['id']; ?>">x</a></td>
                      <td class="col-product-thumbnail"><a href="#" class="trans"><img src="<?php echo PRODUCT_PATH . $item['image_link']; ?>" alt=" " ></a></td>
                      <td><a href="#" class="link"><?php echo $item['name'] ?></a></td>
                      <td class="text-right">
                        <?php if ($item['old_price'] > 0): ?> 
                          <span class="link"><?php echo priceFormat($item['old_price']); ?>đ</span>
                          <span class="link"><?php echo priceFormat($item['price']) ?>đ</span>
                           <?php else: ?>
                            <span class="link"><?php echo priceFormat($item['price']) ?>đ</span>
                        <?php endif ?>
                      </td>
                      <td class="col-qualty">
                        <div class="qualty cart">
                          <a href="javascript:void(0)" class="des click-down">-</a>
                          <input type="text" class="number" min="1" value="<?php echo $item['qty'] ?>" >
                          <a href="javascript:void(0)" class="inc click-up">+</a>
                        </div>
                      </td>
                      <td class="text-right">
                       
                          <span class="link"><?php echo priceFormat($item['price'] * $item['qty']); ?>đ</span></td>
                    </tr>
                <?php endforeach ?>
                <?php if (count($carts) == 0) echo 'Giỏ hàng rỗng !' ?>
              </tbody>
            </table>
            </div>
            <div class="text-left">
              <button class="btn-cmn has-border mr20 mb10">← Tiếp tục mua hàng</button>
              <button class="btn-cmn mb10 update-cart" type="button">Cập nhật giỏ hàng</button>
            </div>
            
            
          </div>
          <div class="col-md-3  border-left">
            <div class="mb30">
              <table class="tb-total mb20">
                <thead>
                  <tr>
                    <th colspan="2">Tổng giỏ hàng</th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="row01">
                    <td class="bold">Thành tiền</td>
                    <td class="text-right totalPrice" ><?php echo priceFormat($totalMoney) ?> đ</td>
                  </tr>
                  <tr>
                    <td class="bold">Tổng cộng</td>
                    <td class="text-right totalMoney" ><?php echo priceFormat($totalMoney) ?> đ</td>
                  </tr>
                </tbody>
              </table>
              <div class="text-left">
                <button class="btn-cmn full mr20">Thanh toán đơn hàng</button>
              </div>
            </div>
            <h3 class="ttl-col-cart mb10"><i class="icon-tag"></i>Phiếu ưu đãi</h3>
            <input type="text" class="mb20 input-text" placeholder="Mã ưu đãi">
            <div class="text-left">
              <button class="btn-cmn full mr20">Áp dụng ưu đãi</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
</div>
<?php $this->load->view('rulya/includes/footer'); ?>