<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            
            <section class="content">
                <div class="box box-default">
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-3 form-group">
                                <input type="text" class="form-control startDate" value="">
                            </div>
                            <div class="col-sm-3 form-group">
                                <input type="text" class="form-control showTime" disabled>
                            </div>
                            <div class="col-sm-2">
                                <button type="button" class="btn btn-primary btnShowDate">Show</button>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </section>
            <input type="text" hidden="hidden" id="urlShowTime" value="<?php echo base_url('formatdate/showDate') ?>">
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>