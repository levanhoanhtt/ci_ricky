<div class="col-sm-12">
    <div class="">
        <div class="table-responsive no-padding divTable">
            <table class="table table-hover table-bordered-bottom">
                <thead class="theadNormal">
                <tr>
                    <th>Sản phẩm</th>
                    <th class="text-center" style="width: 60px;">Đơn vị</th>
                    <th class="text-center" style="width: 130px;">SKU</th>
                    <th class="text-center" style="width: 120px;">Bảo hành</th>
                    <th class="text-center" style="width: 150px;">Giá</th>
                    <th style="width: 30px;"></th>
                </tr>
                </thead>
                <tbody id="tbodyProduct">
               
                </tbody>
            </table>
        </div>
        <div class="border-top-title-main">
            <div class="clearfix">
                <div class="box-search-advance product">
                    <div>
                        <input type="text" class="form-control textbox-advancesearch updated" id="txtSearchProduct" placeholder="Tìm kiếm sản phẩm (F3)">
                    </div>
                    <div class="panel panel-default" id="panelProduct">
                        <div class="panel-body" style="width:100%;">
                            <div class="list-search-data">
                                <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                <div>
                                    <div class="form-group pull-right" style="width: 300px;">
                                        <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', 0, true, 'Nhóm sản phẩm', ' select2'); ?>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered">
                                        <thead class="theadNormal">
                                        <tr>
                                            <th style="width: 100px;">Ảnh</th>
                                            <th>Sản phẩm</th>
                                            <th style="width: 100px;">SKU</th>
                                            <th style="width: 100px;">Giá</th>
                                            <th style="width: 100px;">Bảo hành</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyProductSearch"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default" id="btnPrevProduct"><i class="fa fa-chevron-left"></i></button>
                                <button type="button" class="btn btn-default" id="btnNextProduct"><i class="fa fa-chevron-right"></i></button>
                                <input type="text" hidden="hidden" id="pageIdProduct" value="1">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive no-padding divTable" id='viewOldProductNew' style="display: none">
            <table class="table table-hover table-bordered">
                <thead class="theadNormal">
                <tr>
                    <th>IMEI</th>
                    <th>Hình thức</th>
                    <th>Sử dụng</th>
                    <th>Phụ kiện</th>
                    <th>Ghi chú</th>
                </tr>
                </thead>
                <tbody id="tbodyOldProductAdd">
                    <tr>
                        <td><input type="text" class="form-control hmdrequired" id="IMEI" value="" data-field="IMEI"></td>
                        <td><?php echo $this->Mconstants->selectObject($listProductFormalStatus, 'ProductFormalStatusId', 'ProductFormalStatusName', 'ProductFormalStatusId', 0, true, '--Chọn--'); ?></td>
                        <td><?php echo $this->Mconstants->selectObject($listProductUsageStatus, 'ProductUsageStatusId', 'ProductUsageStatusName', 'ProductUsageStatusId', 0, true, '--Chọn--'); ?></td>
                        <td><?php echo $this->Mconstants->selectObject($listProductAccessoryStatus, 'ProductAccessoryStatusId', 'ProductAccessoryStatusName', 'ProductAccessoryStatusId', 0, true, '--Chọn--'); ?></td>
                        <td><input type="text" class="form-control" id="comment" value=""></td>
                    </tr>
                    <tr >
                        <td colspan="5" >
                            <a href="javascript:void(0)" style="float: right;" class="btn btn-primary" id="link_add_product_old" data-id="0">Thêm sản phẩm cũ</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="" id="viewOldProduct" style="display: none">
         <h4>Các sản phẩm cũ đã có</h4>
        <div class="table-responsive no-padding divTable">
            <table class="table table-hover table-bordered">
                <thead class="theadNormal">
                <tr>
                    <th>IMEI</th>
                    <th>Hình thức</th>
                    <th>Sử dụng</th>
                    <th>Phụ kiện</th>
                    <th>Ghi chú</th>
                </tr>
                </thead>
                <tbody id="tbodyOldProduct">
                </tbody>
            </table>
        </div>
    </div>
</div>
<input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getList'); ?>">
<input type="text" hidden="hidden" id="getProductDetailUrl" value="<?php echo base_url('api/product/get'); ?>">
<input type="text" hidden="hidden" id="getListOldProductUrl" value="<?php echo base_url('api/product/getListOldProduct'); ?>">
<input type="text" hidden="hidden" id="changeProductStatusUrl" value="<?php echo base_url('api/product/changeStatusBatch'); ?>">
<input type="text" hidden="hidden" id="updateOldProductUrl" value="<?php echo base_url('api/product/updateOldProduct'); ?>">
<input type="text" hidden="hidden" id="deleteProductChildUrl" value="<?php echo base_url('api/product/deleteProductChild'); ?>">
<input type="text" hidden="hidden" id="updateOldProductChildUrl" value="<?php echo base_url('api/product/updateOldProductChild'); ?>">
<input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
<input type="text" hidden="hidden" id="parentProductId" value="<?php echo $productId; ?>">
<input type="text" hidden="hidden" id="parentProductChildId" value="<?php echo $productChildId; ?>">
<input type="text" hidden="hidden" id="productOrChild" value="<?php echo $productChildId > 0 ? 2 : 1; ?>">