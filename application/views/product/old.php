<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl">
                    <li><a href="javascript:void(0)" class="btn btn-primary" id="id_add_old_product">Thêm sản phẩm cũ</a></li> 
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả sản phẩm</a></li>
                        <?php foreach($listFilters as $f){ ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả sản phẩm theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="product_status_trade">Tình trạng kinh doanh</option>
                                        <option value="product_formal_status">Hình thức sản phẩm</option>
                                        <option value="product_usage_status">Tình trạng sử dụng</option>
                                        <option value="product_accessory_status">Tình trạng phụ kiện</option>
                                        <option value="product_status_display">Tình trạng hiển thị</option>
                                        <option value="product_type">Ngành kinh doanh</option>
                                        <option value="product_kind">Loại sản phẩm</option>
                                        <!--<option value="product_suppliers">Nhà cung cấp</option>-->
                                        <option value="product_manufacturer">Hãng sản xuất</option>
                                        <option value="product_group_1">Nhóm sản phẩm</option>
                                        <option value="product_group_2">Loại hàng hóa</option>
                                        <option value="product_unit">Đơn vị</option>
                                        <option value="product_image">Ảnh sản phẩm</option>
                                        <option value="product_tag">Tag</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 product_status_trade product_formal_status product_usage_status product_accessory_status product_status_display product_type product_kind product_suppliers product_manufacturer product_group_1 product_group_2 product_unit block-display">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="product_image none-display">
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <select class="form-control product_status_trade block-display">
                                        <?php foreach($this->Mconstants->productStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_formal_status none-display">
                                        <?php foreach($listProductFormalStatus as $pt){ ?>
                                            <option value="<?php echo $pt['ProductFormalStatusId']; ?>"><?php echo $pt['ProductFormalStatusName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_usage_status none-display">
                                        <?php foreach($listProductUsageStatus as $pt){ ?>
                                            <option value="<?php echo $pt['ProductUsageStatusId']; ?>"><?php echo $pt['ProductUsageStatusName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_accessory_status none-display">
                                        <?php foreach($listProductAccessoryStatus as $pt){ ?>
                                            <option value="<?php echo $pt['ProductAccessoryStatusId']; ?>"><?php echo $pt['ProductAccessoryStatusName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_status_display none-display">
                                        <?php foreach($this->Mconstants->productDisplayTypes as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_type none-display">
                                        <?php foreach($listProductTypes as $pt){ ?>
                                            <option value="<?php echo $pt['ProductTypeId']; ?>"><?php echo $pt['ProductTypeName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_kind none-display">
                                        <?php foreach($this->Mconstants->productKinds as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <!--<select class="form-control product_suppliers none-display">
                                        <?php //foreach($listSuppliers as $s){ ?>
                                            <option value="<?php //echo $s['SupplierId']; ?>"><?php //echo $s['SupplierName']; ?></option>
                                        <?php //} ?>
                                    </select>-->
                                    <select class="form-control product_manufacturer none-display">
                                        <?php foreach($listManufacturers as $m){ ?>
                                            <option value="<?php echo $m['ManufacturerId']; ?>"><?php echo $m['ManufacturerName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_group_1 none-display">
                                        <?php foreach($listCategories as $c){
                                            if($c['ItemTypeId'] == 1){ ?>
                                                <option value="<?php echo $c['CategoryId']; ?>"><?php echo $c['CategoryName']; ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                    <select class="form-control product_group_2 none-display">
                                        <?php foreach($listCategories as $c){
                                            if($c['ItemTypeId'] == 2){ ?>
                                                <option value="<?php echo $c['CategoryId']; ?>"><?php echo $c['CategoryName']; ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                    <select class="form-control product_unit none-display">
                                        <?php foreach($listProductUnits as $pu){ ?>
                                            <option value="<?php echo $pu['ProductUnitId']; ?>"><?php echo $pu['ProductUnitName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_image none-display">
                                        <option value="2">Có</option>
                                        <option value="1">Không</option>
                                    </select>
                                    <select class="form-control product_tag none-display">
                                        <option value="in">chứa</option>
                                        <option value="not in">không chứa</option>
                                    </select>
                                </div>
                                <div class="form-group block-display widthauto">
                                    <input class="form-control product_tag none-display" type="text">
                                    <button id="btn-filter" data-href="<?php echo base_url('api/product/searchProductOldByFilter'); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-sm-3 fix-width-200">
                                <select class="form-control" id="selectAction" style="display: none;">
                                    <option value="">Chọn hành động</option>
                                </select>
                            </div>
                            <div class="col-sm-2 fix-width-200">
                                <select class="form-control" id="selectData" style="display: none;"></select>
                            </div>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Sản phẩm</th>
                                <th>Ngành kinh doanh</th>
                                <th>IMEI</th>
                                <th>Hình thức sản phẩm</th>
                                <th>Tình trạng sử dụng</th>
                                <th>Tình trạng phụ kiện</th>
                                <th>Ghi chú</th>
                                <th class="text-center">Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProductOld"></tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="changeItemStatusUrl" value="<?php echo base_url('api/product/changeStatusBatch'); ?>">
                    <input type="text" hidden="hidden" id="itemTypeId" value="33">
                    <input type="hidden" value="<?php echo base_url('product/addOld')?>" id="urlEditProduct">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                    <div class="modal fade" id="modalProductChild" tabindex="-1" role="dialog" aria-labelledby="modalProductChild">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-comments-o"></i>Thông tin sản phẩm cũ</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table class="table new-style table-hover table-bordered" id="table-data">
                                                <thead>
                                                <tr>
                                                    <th>Sản phẩm</th>
                                                    <th>IMEI</th>
                                                    <th>Hình thức sản phẩm</th>
                                                    <th>Tình trạng sử dụng</th>
                                                    <th>Tình trạng phụ kiện</th>
                                                    <th>Ghi chú</th>
                                                    <th style="width: 50px"></th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbodyModalProductOld">
                                                <tr>
                                                    <td id="productName"></td>
                                                    <td><input type="text" class="form-control hmdrequired" id="IMEI" value="" data-field="IMEI"></td>
                                                    <td><?php echo $this->Mconstants->selectObject($listProductFormalStatus, 'ProductFormalStatusId', 'ProductFormalStatusName', 'ProductFormalStatusId', 0, true, '--Chọn--'); ?></td>
                                                    <td><?php echo $this->Mconstants->selectObject($listProductUsageStatus, 'ProductUsageStatusId', 'ProductUsageStatusName', 'ProductUsageStatusId', 0, true, '--Chọn--'); ?></td>
                                                    <td><?php echo $this->Mconstants->selectObject($listProductAccessoryStatus, 'ProductAccessoryStatusId', 'ProductAccessoryStatusName', 'ProductAccessoryStatusId', 0, true, '--Chọn--'); ?></td>
                                                    <td><input type="text" class="form-control" id="comment" value=""></td>
                                                    <td>
                                                        <a href="javascript:void(0)" id="link_update"><i class="fa fa-save" title="Cập nhật"></i></a>
                                                        <a href="javascript:void(0)" id="link_delete"><i class="fa fa-times" title="Xóa"></i></a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" hidden="hidden" id="productId" value="0">
                                    <input type="hidden" hidden="hidden" id="parentProductId" value="0">
                                    <input type="hidden" hidden="hidden" id="productChildId" value="0">
                                    <input type="hidden" hidden="hidden" id="parentProductChildId" value="0">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    <input type="text" hidden="hidden" id="updateOldProductChildUrl" value="<?php echo base_url('api/product/updateOldProductChild'); ?>">
                                    <input type="text" hidden="hidden" id="updateOldProductUrl" value="<?php echo base_url('api/product/updateOldProduct'); ?>">
                                    <input type="text" hidden="hidden" id="changeProductStatusUrl" value="<?php echo base_url('api/product/changeStatusBatch'); ?>">
                                    <input type="text" hidden="hidden" id="deleteProductChildUrl" value="<?php echo base_url('api/product/deleteProductChild'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalAddOldProduct" tabindex="-1" role="dialog" aria-labelledby="modalAddOldProduct">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-comments-o"></i>Thông tin sản phẩm cũ</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <?php $this->load->view('product/modal_product_old'); ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>