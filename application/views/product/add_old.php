<?php $this->load->view('includes/header');  ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('product/old'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content new-box-stl ft-seogeo">
                <div class="row">
                    <div class="col-sm-12 no-padding">
                        <div class="box box-default padding20">
                            <div class="">
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered-bottom">
                                        <thead class="theadNormal">
                                        <tr>
                                            <th>Sản phẩm</th>
                                            <th class="text-center" style="width: 60px;">Đơn vị</th>
                                            <th class="text-center" style="width: 130px;">SKU</th>
                                            <th class="text-center" style="width: 120px;">Bảo hành</th>
                                            <th class="text-center" style="width: 150px;">Giá</th>
                                            <th style="width: 30px;"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyProduct">
                                        <?php if($productChildId > 0){
                                            $productChild = $this->Mproductchilds->get($productChildId, true, '', 'ProductId, ProductName, ProductImage, BarCode, Price, GuaranteeMonth');
                                            if($productChild){
                                                $product = $this->Mproducts->get($productChild['ProductId'], true, '', 'ProductName, ProductUnitId');
                                                $productImage = empty($productChild['ProductImage']) ? NO_IMAGE : $productChild['ProductImage']; ?>
                                                <tr data-id="<?php echo $productChild['ProductId']; ?>" data-child="<?php echo $productChildId; ?>">
                                                    <td><img src="<?php echo PRODUCT_PATH.$productImage; ?>" class="productImg"><a href="javascript:void(0)" class="light-dark aProductLink" target="_blank"><?php echo $product['ProductName'].' ('.$productChild['ProductName'].')'; ?></a></td>
                                                    <td class="text-center"><?php echo $this->Mproductunits->getFieldValue(array('ProductUnitId' => $product['ProductUnitId']), 'ProductUnitName'); ?></td>
                                                    <td class="text-center"><?php echo $productChild['BarCode']; ?></td>
                                                    <td class="text-center"><?php echo $productChild['GuaranteeMonth']; ?></td>
                                                    <td class="tdPrice text-right"><span class="spanPrice"><?php echo priceFormat($productChild['Price']); ?></span> ₫</td>
                                                    <td class="text-right"><a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a></td>
                                                </tr>
                                            <?php }
                                        }
                                        elseif($productId > 0){
                                            $product = $this->Mproducts->get($productId, true, '', 'ProductName, ProductUnitId, ProductImage, BarCode, Price, GuaranteeMonth');
                                            if($product){
                                                $productImage = empty($product['ProductImage']) ? NO_IMAGE : $product['ProductImage']; ?>
                                                <tr data-id="<?php echo $productId; ?>" data-child="0">
                                                    <td><img src="<?php echo PRODUCT_PATH.$productImage; ?>" class="productImg"><a href="javascript:void(0)" class="light-dark aProductLink" target="_blank"><?php echo $product['ProductName']; ?></a></td>
                                                    <td class="text-center"><?php echo $this->Mproductunits->getFieldValue(array('ProductUnitId' => $product['ProductUnitId']), 'ProductUnitName'); ?></td>
                                                    <td class="text-center"><?php echo $product['BarCode']; ?></td>
                                                    <td class="text-center"><?php echo $product['GuaranteeMonth']; ?></td>
                                                    <td class="tdPrice text-right"><span class="spanPrice"><?php echo priceFormat($product['Price']); ?></span> ₫</td>
                                                    <td class="text-right"><a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a></td>
                                                </tr>
                                            <?php }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="border-top-title-main">
                                    <div class="clearfix">
                                        <div class="box-search-advance product">
                                            <div>
                                                <input type="text" class="form-control textbox-advancesearch updated" id="txtSearchProduct" placeholder="Tìm kiếm sản phẩm (F3)">
                                            </div>
                                            <div class="panel panel-default" id="panelProduct">
                                                <div class="panel-body" style="width:100%;">
                                                    <div class="list-search-data">
                                                        <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                        <div>
                                                            <div class="form-group pull-right" style="width: 300px;">
                                                                <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', 0, true, 'Nhóm sản phẩm', ' select2'); ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="table-responsive no-padding divTable">
                                                            <table class="table table-hover table-bordered">
                                                                <thead class="theadNormal">
                                                                <tr>
                                                                    <th style="width: 100px;">Ảnh</th>
                                                                    <th>Sản phẩm</th>
                                                                    <th style="width: 100px;">SKU</th>
                                                                    <th style="width: 100px;">Giá</th>
                                                                    <th style="width: 100px;">Bảo hành</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="tbodyProductSearch"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="btn-group pull-right">
                                                        <button type="button" class="btn btn-default" id="btnPrevProduct"><i class="fa fa-chevron-left"></i></button>
                                                        <button type="button" class="btn btn-default" id="btnNextProduct"><i class="fa fa-chevron-right"></i></button>
                                                        <input type="text" hidden="hidden" id="pageIdProduct" value="1">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive no-padding divTable" id='viewOldProductNew' style="display: none">
                                    <table class="table table-hover table-bordered">
                                        <thead class="theadNormal">
                                        <tr>
                                            <th>IMEI</th>
                                            <th>Hình thức</th>
                                            <th>Sử dụng</th>
                                            <th>Phụ kiện</th>
                                            <th>Ghi chú</th>
                                        </tr>
                                        </thead>
                                        <tbody  id="tbodyOldProductAdd">
                                            <tr>
                                                <td><input type="text" class="form-control hmdrequired" id="IMEI" value="" data-field="IMEI"></td>
                                                <td><?php echo $this->Mconstants->selectObject($listProductFormalStatus, 'ProductFormalStatusId', 'ProductFormalStatusName', 'ProductFormalStatusId', 0, true, '--Chọn--'); ?></td>
                                                <td><?php echo $this->Mconstants->selectObject($listProductUsageStatus, 'ProductUsageStatusId', 'ProductUsageStatusName', 'ProductUsageStatusId', 0, true, '--Chọn--'); ?></td>
                                                <td><?php echo $this->Mconstants->selectObject($listProductAccessoryStatus, 'ProductAccessoryStatusId', 'ProductAccessoryStatusName', 'ProductAccessoryStatusId', 0, true, '--Chọn--'); ?></td>
                                                <td><input type="text" class="form-control" id="comment" value=""></td>
                                            </tr>
                                            <tr >
                                                <td colspan="5" >
                                                    <a href="javascript:void(0)" style="float: right;" class="btn btn-primary" id="link_add_product_old" data-id="0">Thêm sản phẩm cũ</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default padding20" id="viewOldProduct" style="display: none">
                             <h4>Các sản phẩm cũ đã có</h4>
                            <div class="table-responsive no-padding divTable">
                                <table class="table table-hover table-bordered">
                                    <thead class="theadNormal">
                                    <tr>
                                        <th>IMEI</th>
                                        <th>Hình thức</th>
                                        <th>Sử dụng</th>
                                        <th>Phụ kiện</th>
                                        <th>Ghi chú</th>
                                        <!-- <th style="width: 50px;"></th> -->
                                    </tr>
                                    </thead>
                                    <tbody id="tbodyOldProduct">
                                    <?php foreach($listOldProducts as $p){ ?>
                                        <tr class="trOldProduct" data-id="<?php echo $p['ProductId']; ?>" data-child="<?php echo $p['ProductChildId']; ?>">
                                            <td class="tdIMEI"><?php echo $p['IMEI']; ?></td>
                                            <td class="tdFormalStatus"><?php echo $this->Mconstants->getObjectValue($listProductFormalStatus, 'ProductFormalStatusId', $p['ProductFormalStatusId'], 'ProductFormalStatusName'); ?></td>
                                            <td class="tdUsageStatus"><?php echo $this->Mconstants->getObjectValue($listProductUsageStatus, 'ProductUsageStatusId', $p['ProductUsageStatusId'], 'ProductUsageStatusName'); ?></td>
                                            <td class="tdAccessoryStatus"><?php echo $this->Mconstants->getObjectValue($listProductAccessoryStatus, 'ProductAccessoryStatusId', $p['ProductAccessoryStatusId'], 'ProductAccessoryStatusName'); ?></td>
                                            <td class="tdComment"><?php echo $p['ProductChildId'] > 0 ? $p['Comment'] : $p['ProductShortDesc']; ?></td>
                                            <!-- <td>
                                                <a href="javascript:void(0)" class="link_edit"><i class="fa fa-pencil" title="Sửa"></i></a>
                                                <a href="javascript:void(0)" class="link_delete"><i class="fa fa-times" title="Xóa"></i></a>
                                                <input type="text" hidden="hidden" id="productFormalStatusId_<?php // echo $p['ProductChildId'] > 0 ? $p['ProductChildId'] : $p['ProductId']; ?>" value="<?php // echo $p['ProductFormalStatusId']; ?>">
                                                <input type="text" hidden="hidden" id="productUsageStatusId_<?php // echo $p['ProductChildId'] > 0 ? $p['ProductChildId'] : $p['ProductId']; ?>" value="<?php // echo $p['ProductUsageStatusId']; ?>">
                                                <input type="text" hidden="hidden" id="productAccessoryStatusId_<?php // echo $p['ProductChildId'] > 0 ? $p['ProductChildId'] : $p['ProductId']; ?>" value="<?php // echo $p['ProductAccessoryStatusId']; ?>">
                                            </td> -->
                                        </tr>
                                    <?php } ?>
                                    <!-- <tr>
                                        <td><input type="text" class="form-control hmdrequired" id="IMEI" value="" data-field="IMEI"></td>
                                        <td><?php // echo $this->Mconstants->selectObject($listProductFormalStatus, 'ProductFormalStatusId', 'ProductFormalStatusName', 'ProductFormalStatusId', 0, true, '--Chọn--'); ?></td>
                                        <td><?php // echo $this->Mconstants->selectObject($listProductUsageStatus, 'ProductUsageStatusId', 'ProductUsageStatusName', 'ProductUsageStatusId', 0, true, '--Chọn--'); ?></td>
                                        <td><?php // echo $this->Mconstants->selectObject($listProductAccessoryStatus, 'ProductAccessoryStatusId', 'ProductAccessoryStatusName', 'ProductAccessoryStatusId', 0, true, '--Chọn--'); ?></td>
                                        <td><input type="text" class="form-control" id="comment" value=""></td>
                                        <td>
                                            <a href="javascript:void(0)" id="link_update" data-id="0"><i class="fa fa-save" title="Cập nhật"></i></a>
                                            <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr> -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--<div class="col-sm-4"></div>-->
                </div>
                <input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getList'); ?>">
                <input type="text" hidden="hidden" id="getProductDetailUrl" value="<?php echo base_url('api/product/get'); ?>">
                <input type="text" hidden="hidden" id="getListOldProductUrl" value="<?php echo base_url('api/product/getListOldProduct'); ?>">
                <input type="text" hidden="hidden" id="changeProductStatusUrl" value="<?php echo base_url('api/product/changeStatusBatch'); ?>">
                <input type="text" hidden="hidden" id="updateOldProductUrl" value="<?php echo base_url('api/product/updateOldProduct'); ?>">
                <input type="text" hidden="hidden" id="deleteProductChildUrl" value="<?php echo base_url('api/product/deleteProductChild'); ?>">
                <input type="text" hidden="hidden" id="updateOldProductChildUrl" value="<?php echo base_url('api/product/updateOldProductChild'); ?>">
                <input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
                <input type="text" hidden="hidden" id="parentProductId" value="<?php echo $productId; ?>">
                <input type="text" hidden="hidden" id="parentProductChildId" value="<?php echo $productChildId; ?>">
                <input type="text" hidden="hidden" id="productOrChild" value="<?php echo $productChildId > 0 ? 2 : 1; ?>">
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>