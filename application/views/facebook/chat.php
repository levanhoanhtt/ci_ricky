<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <div id="content" class="main-content">
                <div class="outer">
                    <div class="inner" id="viewareaid">
                        <div>
                            <div id="fb-chat" class="wrapper-content fb-chat">
                                <div class="fb-chat-conversation border-top">
                                    <div class="tool-left-harapage">
                                        <ul>
                                            <li class="active">
                                                <a class="cursor-pointer" data-placement="right" title=""
                                                   data-original-title="RICKY - Thiết bị studio chính hãng">
                                                    <img src="assets/facebookresource/picture">
                                                </a>
                                            </li>
                                            <li class="addmoretab">
                                                <a class="cursor-pointer" data-toggle="modal"
                                                   data-target=".add-more-tab">
                                                    <i class="fa fa-plus fa-2x"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="fb-chat-conversation-container">
                                        <div class="border-right fb-chat-list clearfix">
                                            <div class="fb-wrap-col-bar-col-search">
                                                <div class="col-md-12 fb-chat-action-bar border-bottom">
                                                    <div class="fb-chat-menu">
                                                        <div class="menu-icon active" data-toggle="tooltip"
                                                             data-placement="bottom" data-original="#list-conversation">
                                                            <i class="fa fa-inbox fa-2x"></i>
                                                        </div>
                                                        <div class="menu-icon" id="data-messenger" data-toggle="tooltip"
                                                             data-placement="bottom" data-original="#inbox-message">
                                                            <i class="fa fa-envelope-o fa-2x"></i>
                                                        </div>
                                                        <div class="menu-icon" data-toggle="tooltip"
                                                             data-placement="bottom" data-original="#comment-message">
                                                            <i class="fa fa-comment-o fa-2x"></i>
                                                        </div>
                                                        <div class="menu-icon" data-toggle="tooltip"
                                                             data-placement="bottom" data-original="#unread">
                                                            <i class="fa fa-eye-slash fa-2x"></i>
                                                        </div>
                                                        <div class="menu-icon drop-control dropdown">
                                                            <span class="dropdown-toggle" data-toggle="dropdown"
                                                                  role="button" aria-expanded="false">
                                                                <i class="fa fa-bars fa-2x"></i>
                                                            </span>
                                                            <div class="facebook tag-menu dropdown-menu dropdown-ps-left"
                                                                 role="menu">
                                                                <div class="drop-control">
                                                                    <div class="menu-icon clearfix border-bottom mb15">
                                                                        <div class="item">
                                                                            <i class="fa fa-eye fa-1-5"></i>
                                                                            <span>Đã đọc tất cả</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="menu-icon clearfix">
                                                                        <div class="item tag-color-1">
                                                                            <i class="fa fa-tag fa-1-5"></i>
                                                                            <span data-bind="text: Name">BAO-HANH</span>
                                                                        </div>
                                                                        <a data-bind="click: $parent.RemoveTag"
                                                                           class="remove-tag pull-right"
                                                                           data-toggle="tooltip" data-placement="bottom"
                                                                           data-original-title="Xóa nhãn">
                                                                            <i class="fa fa-times fa-1 note"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="menu-icon clearfix">
                                                                        <div class="item tag-color-23">
                                                                            <i class="fa fa-tag fa-1-5"></i>
                                                                            <span data-bind="text: Name">ĐÃ ĐẶT CỌC</span>
                                                                        </div>
                                                                        <a class="remove-tag pull-right"
                                                                           data-toggle="tooltip" data-placement="bottom"
                                                                           data-original-title="Xóa nhãn">
                                                                            <i class="fa fa-times fa-1 note"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="menu-icon clearfix">
                                                                        <div class="item tag-color-24">
                                                                            <i class="fa fa-tag fa-1-5"></i>
                                                                            <span data-bind="text: Name">ĐỢI K10 VỀ GỌI CHO KHÁCH</span>
                                                                        </div>
                                                                        <a class="remove-tag pull-right"
                                                                           data-toggle="tooltip" data-placement="bottom"
                                                                           data-original-title="Xóa nhãn">
                                                                            <i class="fa fa-times fa-1 note"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 search-conversation border-bottom">
                                                    <div class="">
                                                        <input placeholder="Tìm kiếm" type="text"
                                                               class="form-control form-large txtSearch">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 conversation scroll-conversation event-scroll-visible"
                                                 style="overflow: auto; max-height: 99vh; height: 98%">
                                                <ul id="list-conversation" class="all-message tab-content">
                                                </ul>
                                                <ul id="inbox-message" class="inbox-message tab-content">
                                                </ul>
                                                <ul id="comment-message" class="comment-message tab-content">


                                                </ul>
                                                <ul id="unread" class="unread tab-content">


                                                </ul>
                                            </div>
                                        </div>
                                        <div class="fb-conversation-message ps-relative">
                                            <div class="inline_block border-right chat-box">
                                                <div class="conversation-message-info border-bottom ws-nm"
                                                     title="Lưu Văn Nhất">
                                                    <i class="btn fa fa-arrow-circle-left backlistchat comebacklistchat"
                                                       style="display: none;"></i>
                                                    <span class="name" id="currName"> </span>
                                                </div>
                                                <div class="conversation-tags border-bottom">
                                                    <a id="ga_harapage_11" class="btn btn-default btn-xs">
                                                        <i class="fa fa-tags"></i>
                                                        Chỉnh sửa nhãn
                                                    </a>
                                                </div>
                                                <div class="list-message" style="display: none">
                                                    <div class="list-message-fix-height">
                                                        <div class="list-message-container"
                                                             style="overflow-y: scroll; max-height: 99vh;">
                                                            <ul id="conversation-message-picture">
                                                                <li class="conversation-message-picture hidden">
                                                                    <p class="text-no-bold"></p>
                                                                </li>
                                                                <li class="is-from-page">
                                                                    <div>
                                                                        <p class="message">
                                                                            <span></span>
                                                                        </p>
                                                                    </div>

                                                                </li>

                                                            </ul>
                                                        </div>
                                                        <div id="list-message" class="list-message-container"
                                                             style="display:none">

                                                        </div>
                                                    </div>
                                                    <div class="message-reply border-top clearfix">
                                                        <div class="textarea-reply">
                                                            <textarea id="textarea-auto-height-harapage"
                                                                      style="height: 58px; max-height: 300px; width: 100%;"
                                                                      class="form-control"
                                                                      placeholder="Nhập câu trả lời">		</textarea>

                                                        </div>
                                                        <div class="reply-attachment-form">
                                                            <div class="item">
                                                                <div>
                                                                    <div class="fa fa-smile-o"></div>
                                                                    <div class="p-none-important btn-group drop-select-search drop-control dropup"
                                                                         style="position: relative;">
                                                                        <div id="ga_harapage_6" data-toggle="dropdown"
                                                                             role="button">
                                                                            <i class="fa fa-product"></i>
                                                                            <p data-bind="t   <p>Kho hình ảnh</p>ext: SelectedText">
                                                                                Tìm sản phẩm</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                <div id="ga_harapage_7"
                                                                     data-bind="click: ClickOpenAddImageModal">
                                                                    <i class="fa fa-camera"></i>
                                                                </div>
                                                            </div>
                                                            <div class="item" style="position: relative;">
                                                                <div id="ga_harapage_15"
                                                                     style="position: relative; z-index: 1;">
                                                                    <i class="fa fa-upload"></i>
                                                                    <p>Tải hình mới</p>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                <div class="dropup drop-control"
                                                                     style="position: relative;"
                                                                     id="SampleMessageDropDown">
                                                                    <div data-toggle="dropdown" id="dropdownMenu1">
                                                                        <div id="">
                                                                            <i class="fa fa-list-alt"></i>
                                                                            <p>Tin nhắn mẫu</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="action-group-reply">
                                                            <div class="reply-attachment">
                                                                <i class="fa fa-paperclip"></i>
                                                                <span>Đính kèm</span>
                                                            </div>
                                                            <input type="hidden" id="message-type">
                                                            <a class="btn btn-primary" id="send-button">Gửi</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="inline_block participant-info-container overflow-auto">
                                                <div class="participant-info-customer ws-nm text-left">
                                                    <div class="participant-info-customer-detail">
                                                        <div class="mb10 clearfix">
                                                            <a class="pull-right text-no-bold">Cập nhật thông tin</a>
                                                        </div>
                                                        <div class="clearfix" id="customer-info">
                                                            <div class="col-md-3 col-xs-12 p-none pull-left">
                                                                <div class="participant-avatar-customer text-center">
                                                                    <img class="img-max-width-100"
                                                                         id="selected-user-avatar"/>
                                                                </div>

                                                            </div>
                                                            <div class="col-md-9 col-xs-12 p-l10 p-r10 pull-left"
                                                                 id="customer-info-detail">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearboth"></div>
                                                    <div class="participant-info-customer-tags overflow-auto border-top">
                                                        <div class="clearfix">
                                                            <a class="pull-right text-no-bold"
                                                               data-bind="click:ClickOpenEditTags">Chỉnh sửa nhãn</a>
                                                        </div>
                                                        <div class="clearfix mt5">
                                                            <div class="tags ws-nm">
                                                                <p class="text-center text-no-bold ws-nm mb0">
                                                                    Chưa có nhãn nào
                                                                </p>

                                                                <p class="text-center ws-nm mb0 note">
                                                                    Việc thêm nhãn giúp bạn phân loại hỗ trợ chăm sóc
                                                                    khách hàng
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="participant-info-customer-comment overflow-auto border-top">
                                                        <div class="comment-log-small">
                                                            <div class="comment-log-small-header clearfix">
                                                                <a class="pull-right text-no-bold">
                                                                    Thêm ghi chú
                                                                </a>
                                                            </div>
                                                            <div class="comment-log-small-body clearfix mt5">
                                                                <!--                                                                <ul>-->
                                                                <!--                                                                    <li class="comment-log-small-comment-item">-->
                                                                <!--                                                                        <span class="text-no-bold">CẢNH LEE</span>-->
                                                                <!--                                                                        <span> - </span>-->
                                                                <!--                                                                        <span class="comment-log-small-comment-item-comment" >COMBO AT250-KS108</span>-->
                                                                <!---->
                                                                <!--                                                                        <div class="comment-log-small-comment-info">-->
                                                                <!--                                                                            <span class="time note">Hôm qua 11:19 SA</span>-->
                                                                <!--                                                                            <span> - </span>-->
                                                                <!--                                                                            <a>Xóa</a>-->
                                                                <!--                                                                        </div>-->
                                                                <!--                                                                    </li>-->
                                                                <!---->
                                                                <!--                                                                    <li class="comment-log-small-comment-item">-->
                                                                <!--                                                                        <span data-bind="text: LogUserFullname" class="text-no-bold">CẢNH LEE</span>-->
                                                                <!--                                                                        <span> - </span>-->
                                                                <!--                                                                        <span class="comment-log-small-comment-item-comment">Tham khảo KS108</span>-->
                                                                <!---->
                                                                <!--                                                                        <div class="comment-log-small-comment-info">-->
                                                                <!--                                                                            <span class="time note">Hôm qua 8:48 SA</span>-->
                                                                <!--                                                                            <span> - </span>-->
                                                                <!--                                                                            <a>Xóa</a>-->
                                                                <!--                                                                        </div>-->
                                                                <!--                                                                    </li>-->
                                                                <!---->
                                                                <!--                                                                    <li class="comment-log-small-comment-item">-->
                                                                <!--                                                                        <span class="text-no-bold">Ngô Vân-CSKH</span>-->
                                                                <!--                                                                        <span> - </span>-->
                                                                <!--                                                                        <span class="comment-log-small-comment-item-comment">ship 80k</span>-->
                                                                <!---->
                                                                <!--                                                                        <div class="comment-log-small-comment-info">-->
                                                                <!--                                                                            <span class="time note">20/08/2017 4:27 CH</span>-->
                                                                <!--                                                                            <span> - </span>-->
                                                                <!--                                                                            <a>Xóa</a>-->
                                                                <!--                                                                        </div>-->
                                                                <!--                                                                    </li>-->
                                                                <!--                                                                </ul>-->
                                                                <div class="text-center mb5">
                                                                    <a><i class="fa fa-arrow-circle-down mr5"></i>Xem
                                                                        thêm</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="conversation-message-info border-bottom ws-nm">
                                                    <i class="btn fa fa-arrow-circle-left backlistchat backlistmessage hidden-lg hidden-md hidden-sm"></i>
                                                    <a class="btn" href="#" target="_blank" data-toggle="tooltip"
                                                       data-placement="bottom" data-original-title="Xem trên Facebook">
                                                        <i class="fa fa-external-link"></i>

                                                        <div class="participant-customer-orders ws-nm text-left">
                                                            <div class="clearfix mb10 participant-customer-orders-header ps-relative">
                                                                <span class="pull-left text-no-bold">Đơn hàng <a
                                                                            href="#" target="_blank"><i
                                                                                class="fa fa-question-circle"></i></a></span>
                                                                <a class="pull-right text-no-bold" href="#"
                                                                   target="_blank">Xem tất cả</a>
                                                            </div>
                                                            <div class="clearfix participant-customer-orders-data">
                                                                <ul class="pl15 p-r15">
                                                                    <li class="mt5 mb5 clearfix">
                                                                        <div class="flexbox-grid-default">
                                                                            <div class="flexbox-auto-70">
                                                                                <div class="wrap-img"
                                                                                     style="vertical-align:middle;">
                                                                                    <img class="img-max-width-100"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="flexbox-auto-right">
                                                                                <a href="#"
                                                                                   target="_blank">RB-104076</a>
                                                                                <p title="Sound card XOX KS108 ">
                                                                                    <span class="overflow-ellipsis-2-row"
                                                                                          style="width:calc(100% - 60px);height:35px;">Sound card XOX KS108</span>
                                                                                    <span class="overflow-ellipsis"></span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end .inner -->
                    </div>
                    <!-- end .outer -->
                </div>
                <div class="clear"></div>
            </div>
            <input id="private-id-message" type="hidden">
            <!-- /#wrap -->
        </div>


        <!--
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.0.0
            </div>
            <strong>Copyright &copy; 2017 <a href="http://hoanmuada.com">Hoàn Mưa Đá Team</a>.</strong> All rights reserved.
            - <strong>Email: <a id="aSysEmail" href="mailto:ricky@gmail.com">ricky@gmail.com</a></strong>.
        </footer>
        -->
    </div>


    <!--reply dialog-->


    <div class="modal fade" id="dialog-reply" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Gửi tin nhắn</h4>
                </div>
                <div class="modal-body" style=" max-height: fit-content;">
                    <input type="hidden" id="comment-id-private-message">
                    <input type="text" style="height: 58px; max-height: 300px; width: 100%;" class="form-control"
                           row="4" id="quick-private-reply-box" placeholder="Nhập tin nhắn riêng">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-default" onclick="sendMessagePrivately()">Gửi tin nhắn</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="dialog-comment" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Trả lời</h4>
                </div>
                <div class="modal-body" style=" max-height: fit-content;">
                    <input type="hidden" id="comment-id-quick-reply">
                    <input type="text" style="height: 58px; max-height: 300px; width: 100%;" class="form-control"
                           id="quick-reply-box" row="4" placeholder="Nhập câu trả lời">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-default" onclick="sendQuickReply()">Gửi bình luận</button>
                </div>
            </div>

        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>