<?php $this->load->view('facebook/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid no-padding">
            <section class="content no-padding">
                <div class="box box-default no-padding">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs ul-tab">
                                    <li class="fix-width"></li>
                                    <li class="active fix-width li_post"><a data-toggle="tab" href="javascript:void(0)" class="list-post"><i class="fa fa-comments-o"></i></a></li>
                                    <li class="fix-width li_chat"><a data-toggle="tab" href="javascript:void(0)" class="list-chat"><i class="fa fa-envelope"></i></a></li>
                                    <li class="fix-width li_"><a data-toggle="tab" href="javascript:void(0)"><i class="   fa fa-comment-o"></i></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="row row-offcanvas row-offcanvas-left">
                                        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
                                            <div class="sidebar-nav">
                                                <ul class="nav">
                                                    <li class="">
                                                        <a href="javascript:void(0)" class="a-padding tags-unread">
                                                            <span class="glyphicon glyphicon-eye-close"></span>
                                                        </a>
                                                    </li>
                                                    <li class=" li-answered">
                                                        <a href="javascript:void(0)" class="a-padding tags-answered">
                                                            <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
                                                        </a>
                                                    </li>
                                                    <li class=" li-not-answered">
                                                        <a href="javascript:void(0)" class="a-padding tags-not-answered">
                                                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!--/.well -->
                                        </div>
                                        <!--/span-->

                                        <div class="col-xs-6 col-sm-9">
                                            <div class="tab-pane active">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                    <input type="text" class="form-control search-inbox">
                                                    <span class="input-group-addon"><i class="fa fa-tags"></i></span>
                                                </div>
                                                <div class="box-comments" id="listChatCustomer" style="margin-top: 15px;">
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                        <!--/span-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="direct-chat">
                                <div class="box-header with-border">
                                    <h3 class="box-title title-name" style="height: 25px;line-height: 25px;"></h3>
                                </div>
                                <div class="box-body body_content_detail" style="height: 550px;overflow-y: hidden;">
                                </div>
                                <div class="box-footer">
                                    <form action="#" method="post">
                                       <div class="form-group">
                                            <div class="form-group has-feedback inner-addon left-addon">
                                                <i class="glyphicon-fix-left glyphicon glyphicon-stop"></i>

                                                <input type="text" class="form-control" name="message" id="txt-message"  placeholder="Type Message ..."/>
                                                <input type="hidden" id="message-type">
                                                    
                                               <a href="javascript:void(0)" class="send-data"> <!-- send-messenger -->
                                                <i class="glyphicon glyphicon-send form-control-feedback-send pointer-events" aria-hidden="true"></i>
                                                </a>
                                               <a href="javascript:void(0)" class="send-smile">
                                                <i class="fa fa-smile-o form-control-feedback-smile" aria-hidden="true"></i>
                                                </a>
                                          
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="container-fluid text-center">
                                            <div class="row">
                                                <div class="mix col-sm-3">
                                                    <a href="javascript:void(0);" class="sample_sentences">
                                                        <i class="fa fa-book"></i><p>Mẫu câu sẵn</p>
                                                    </a>
                                                </div>
                                                <div class="mix col-sm-3"><i class="fa fa-tags"></i><p>Gửi link sku</p></div>
                                                <div class="mix col-sm-3"><i class="fa fa-camera"></i><p>Kho ảnh</p></div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs ul-tab">
                                    <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-user"></i></a></li>
                                    <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-shopping-cart"></i></a></li>
                                    <li><a href="#tab_3" data-toggle="tab"><i class="fa fa-life-ring"></i></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1">
                                        <div class="row tag_info_customer" style="display: none;">
                                            <div class="col-sm-12 mh-info-customer form-group">
                                               
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="more-tabs">
                                                    <div class="form-group">
                                                        <label class="control-label">Thêm tags khách hàng <i class="fa fa-pencil"></i></label>
                                                        <input type="text" class="form-control" id="tags">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="light-blue">Ghi chú</label>
                                                <div class="box-transprt clearfix mb10">
                                                    <button type="button" class="btn-updaten save btnInsertComment" data-id="1">Lưu</button>
                                                    <input type="text" class="add-text" id="comment_1" value="">
                                                </div>
                                                <div class="listComment" id="listComment_1">
                                                    <div class="box-customer mb10">
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <th rowspan="2" valign="top" style="width: 50px;"><img src="assets/vendor/dist/img/users2.png" alt=""></th>
                                                                    <th><a href="javascript:void(0)" class="name">Hoàn Mưa Đá</a></th>
                                                                    <th class="time">15:54 | 21/02/2018</th>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <p class="pComment">test</p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="box-customer mb10">
                                                        <table>
                                                            <tbody>
                                                            <tr>
                                                                <th rowspan="2" valign="top" style="width: 50px;"><img src="assets/vendor/dist/img/users2.png" alt=""></th>
                                                                <th><a href="javascript:void(0)" class="name">Hoàn Mưa Đá</a></th>
                                                                <th class="time">15:52 | 21/02/2018</th>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <p class="pComment">test 2</p>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="text-right light-dark">
                                                    <a href="javascript:void(0)" id="aShowComment">Xem tất cả &gt;&gt;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
        </div>
    </div>
    <audio id="ring_store_fb" hidden="hidden">
      <source src="assets/sound/ring_store.MP3" type="audio/mpeg">
    </audio>
    <?php $this->load->view('facebook/modal'); ?>
    <input type="hidden" id="private-id-message">
    <input type="hidden" id="fb-user-id">
    <input type="hidden" id="fb-page-id">
    <input type="hidden" id="fb-post-id">
    <input type="hidden" id="fb-comment-id">
    <input type="hidden" id="fb-fbPage-code">
    <input type="hidden" id="fb-sender-id">
    <input type="hidden" id="check-tags-active" value="1"> <!-- 1: post, 2 inpox -->
    <input type="hidden" id="search-inbox" value="all"> <!-- 1: post, 2 inpox -->
    <input type="hidden" id="prefix">
    <!-- <input type="hidden" id="urlMessage" value="<?php // echo base_url('facebook/chatMessage') ?>"> -->
    <input type="hidden" id="urlFbUsers" value="<?php echo base_url('facebook/getDataFbUser') ?>">
    <input type="hidden" id="urlFbMess" value="<?php echo base_url('facebook/getDataFbMess') ?>">
    <input type="hidden" id="urlSaveMessage" value="<?php echo base_url('facebook/saveMessage') ?>">
    <input type="hidden" id="urlApiFbPost" value="<?php echo base_url('facebook/getFbPostComment') ?>">
    <input type="hidden" id="urlDetailCommentPost" value="<?php echo base_url('facebook/getFbDetailCommentPost') ?>">
    <input type="hidden" id="urlSaveReplyCommnent" value="<?php echo base_url('facebook/fbSaveReplyCommnent') ?>">
    <input type="hidden" id="getContentSentences" value="<?php echo base_url('facebook/getContentSentences') ?>">
    <input type="hidden" id="urlSearchPost" value="<?php echo base_url('facebook/searchPost') ?>">

    <script type="text/javascript">
        var pageId      = <?php echo json_encode($keyFaceBook['pageId']); ?>;
        var appId       = <?php echo json_encode($keyFaceBook['appId']); ?>;
        var appToken    = <?php echo json_encode($keyFaceBook['appToken']); ?>;
        var userToken   = <?php echo json_encode($keyFaceBook['userToken']); ?>;
    </script>
<?php $this->load->view('includes/footer'); ?>