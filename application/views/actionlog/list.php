<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('actionlog'); ?>
                        <div class="row">
                            <div class="col-sm-4">
                        		<div class="form-group">
                                    <div class='input-group' >
                                        <input type='text' class="form-control daterangepicker" name="DateRangePicker" value="<?php echo set_value('DateRangePicker'); ?>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                            	<?php $this->Mconstants->selectConstants('itemTypes', 'ItemTypeId', set_value('ItemTypeId'), true, '--Đối tượng--'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'UserId', set_value('UserId'), true, '--Người tạo--', ' select2'); ?>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content upn ft-seogeo">
                 <div class="">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Ngày giờ thao tác</th>
                                <th>Nhân viên</th>
                                <th>Mô tả hoạt động</th>
                                <th>Loại hành động</th>
                                <th class="text-center">Link</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyActionLog">
                            <?php
                            foreach($listActionLogs as $a){ ?>
                                <tr>
                                    <td><?php echo getDayDiffText($a['DayDiff']) .' '. $a['CrDateTime']; ?></td> 
                                    <td><?php echo $this->Mconstants->getObjectValue($listUsers, 'UserId', $a['CrUserId'], 'FullName'); ?></td>
                                    <td><?php echo $a['Comment']; ?></td>
                                    <td><?php echo $a['itemTypes']; ?></td>
                                    <td class="text-center">
                                        <?php if(($a['ActionTypeId'] == 1 || $a['ActionTypeId'] == 2) && !empty($a['nameTable']) && !empty($a['idTable'])){ ?>
                                            <a target="_blank" href="<?php echo base_url($a['nameTable'].'/edit/'.$a['idTable']); ?>">Xem</a>
                                        <?php } ?>
                                    </td>
                                </tr>

                            <?php } ?>
                            <?php if(isset($paggingHtml)){ ?>
                            <tr>
                                <td colspan="5"><?php echo $paggingHtml; ?></td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
