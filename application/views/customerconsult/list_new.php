<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl">
                    <li><a href="<?php echo base_url('customerconsult/add'); ?>" class="btn btn-primary">Thêm mới</a></li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả TVL</a></li>
                        <?php foreach($listFilters as $f){ ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="remind_status">Trạng thái</option>
                                        <option value="deadline">Quá hạn</option>
                                        <option value="consult_date">Thời gian cần xử lý</option>
                                        <option value="consult_date_period">Thời điểm cần xử lý</option>
                                        <option value="consult_time">Số lần tư vấn lại</option>
                                        <option value="order_channel">Kênh</option>
                                        <option value="customer_consult_tag">Tag</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 consult_time none-display">
                                    <select class="value_operator form-control">
                                        <option value="=">bằng</option>
                                        <option value="!=">khác</option>
                                        <option value="<">nhỏ hơn</option>
                                        <option value=">">lớn hơn</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 remind_status order_channel deadline">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <select class="form-control remind_status block-display">
                                        <?php foreach($this->Mconstants->remindStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_channel none-display">
                                        <?php foreach($this->Mconstants->orderChannels as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control consult_date_period none-display">
                                        <option value="today">trong hôm nay</option>
                                        <option value="yesterday">trong hôm qua</option>
                                        <option value="current_week">trong tuần hiện tại</option>
                                        <option value="current_month">trong tháng hiện tại</option>
                                    </select>
                                    <select class="form-control consult_date none-display" id="select_operator_date">
                                        <option value="between">trong khoảng</option>
                                        <option value="<">trước</option>
                                        <option value="=">bằng</option>
                                        <option value=">">sau</option>
                                    </select>
                                    <select class="form-control customer_consult_tag none-display mb10">
                                        <option value="in">chứa</option>
                                        <option value="not in">không chứa</option>
                                    </select>
                                    <div class="customer_consult_tag none-display">
                                        <select class="form-control select2 customer_consult_tag ">
                                            <?php foreach($listTags as $key => $v){ ?>
                                                <option value="<?php echo $v['TagId']; ?>"><?php echo $v['TagName']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <select class="form-control deadline none-display">
                                        <option value="bigger">Có quá hạn</option>
                                        <option value="smaller_or_equal">Không quá hạn</option>
                                    </select>
                                </div>
                                <div class="form-group block-display mb10">
                                    <input class="form-control consult_time input-number none-display" type="text">
                                    <input class="form-control datepicker consult_date none-display" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart">
                                    <input class="form-control datepicker consult_date none-display" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd">
                                </div>
                                <div class="form-group block-display widthauto">
                                    <button id="btn-filter" data-href="<?php echo base_url('customerconsult/searchByFilterNew'); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-sm-3 fix-width-200">
                                <select class="form-control" id="selectAction" style="display: none;">
                                    <option value="">Chọn hành động</option>
                                </select>
                            </div>
                            <div class="col-sm-2 fix-width-200">
                                <select class="form-control" id="selectData" style="display: none;"></select>
                            </div>
                        </div>
                    </div>
                    <div class="box-body table-responsive divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Mã chiến dịch</th>
                                <th>Ngày tạo</th>
                                <th>Thời điểm xử lý</th>
                                <th>Lịch sử xử lý</th>
                                <th>Họ tên</th>
                                <th class="text-center">Lần cuối CSL</th>
                                <th>Mức độ tìm năng</th>
                                <th class="text-center">Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyCustomerConsult"></tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="itemTypeId" value="41">
                    <input type="hidden" value="<?php echo base_url('customerconsult/edit2'); ?>" id="urlEditCustomerConsult">
                    <input type="hidden" value="<?php echo base_url('customerconsult/info'); ?>" id="urlInfoCustomer">
                    <input type="hidden" value="<?php echo base_url('customerconsult/edit'); ?>" id="urlEditCustomer">
                    <input type="hidden" value="<?php echo base_url('customerconsult/showComment'); ?>" id="urlCommentConsult">
                    <input type="hidden" value="<?php echo base_url('customerconsult/updateStar'); ?>" id="updateStarUrl">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                </div>
            </section>
        </div>
    </div>
    <div class="modal fade" id="modalItemComment" tabindex="-1" role="dialog" aria-labelledby="modalItemComment">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="box-title"><i class="fa fa-comments-o"></i> Lịch sử xử lý</h4>
                </div>
                <div class="modal-body">
                    <div class="listComment">
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>