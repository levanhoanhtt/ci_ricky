<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php if ($customerConsultId > 0) { ?>
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit" data-order="">Lưu</button></li>
                    <li><a href="<?php echo base_url('customerconsult'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <style>.listComment .box-customer{cursor: pointer;}</style>
                <?php echo form_open('customerconsult/update', array('id' => 'customerConsultForm')); ?>
                <div class="row">
                    <div class="col-sm-8 no-padding">
                        <div class="box box-default padding15">
                            <div class="box-header with-border">
                                <h3 class="box-title">Tư vấn lại<?php if($customerConsult['TimeProcessed'] > 0) echo ' ('.$customerConsult['TimeProcessed'].' lần)'; ?></h3>
                                <div class="box-tools pull-right" style="line-height: 35px;">
                                    <?php echo $objName; ?>
                                </div>
                            </div>
                            <div class="box-body">
                                <?php if($customerConsult['OutOfDate'] == 2 && $customerConsult['RemindStatusId'] == 1){ ?>
                                    <div class="alert alert-danger">
                                        <p><i class="fa fa-exclamation"></i> Nhắc nhỡ đã quá hạn</p>
                                    </div>
                                <?php } elseif($customerConsult['RemindStatusId'] == 4){ ?>
                                    <div class="alert alert-danger">
                                        <p><i class="fa fa-exclamation"></i> Nhắc nhỡ đã hủy bỏ</p>
                                    </div>
                                <?php } elseif($customerConsult['RemindStatusId'] == 5){ ?>
                                    <div class="alert alert-success">
                                        <p><i class="fa fa-check"></i> Nhắc nhỡ đã hoàn thành</p>
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <label class="control-label">Tiêu đề nhắc nhở <span class="required">*</span></label>
                                    <input class="form-control hmdrequired" type="text" value="<?php echo $customerConsult['ConsultTitle']; ?>" id="consultTitle" data-field="Tiêu đề nhắc nhở">
                                </div>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label class="light-blue" style="line-height: 30px;margin-bottom: 15px;">
                                                Cập nhật tình hình hiện tại
                                                <div class="radio-group" style="display: inline;margin-left: 10px;">
                                                    <span class="item"><input type="radio" name="CommentTypeId" class="iCheck iCheckCommentType" value="1" checked> Gọi điện</span>
                                                    <span class="item"><input type="radio" name="CommentTypeId" class="iCheck iCheckCommentType" value="2"> Facebook</span>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" id="divCustomerFacebook" style="display: none;">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="consultFacebook" value="<?php echo $customerConsult['Facebook']; ?>" placeholder="Mã FB Hara">
                                            <span class="input-group-addon" id="spanEditFb" style="cursor: pointer;"><i class="fa fa-pencil"></i></span>
                                            <span class="input-group-addon" id="spanCopyFb" style="cursor: pointer;"><i class="fa fa-copy"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="box-transprt clearfix mb10">
                                        <button type="button" class="btn-updaten save" id="btnInsertComment">
                                            Lưu
                                        </button>
                                        <input type="text" class="add-text " id="comment" data-field="Cập nhật tình hình hiện tại">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label normal">Thời điểm cần xử lý <span class="required">*</span></label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control hmdrequired" id="consultDate" value="<?php echo ddMMyyyy($customerConsult['ConsultDate'], 'd/m/Y H:i') ?>" data-field="Thời điểm cần xử lý">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label normal">Tình trạng xử lý</label>
                                            <?php $this->Mconstants->selectConstants('remindStatus', 'RemindStatusId', $customerConsult['RemindStatusId']); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $commentStatus = "";
                                if($customerConsult['RemindStatusId'] == 4 || $customerConsult['RemindStatusId'] == 6) $commentStatus = $customerConsult['CommentCancel'];
                                else if($customerConsult['RemindStatusId'] == 5) $commentStatus = $customerConsult['CommentComplete']; ?>
                                <div class="form-group" id="divCommentStatus"<?php if($customerConsult['RemindStatusId'] == 1) echo ' style="display: none;"'; ?>>
                                    <label class="control-label normal lbCommentStatus">Ghi chú <?php echo $customerConsult['RemindStatusId'] == 5 ? 'thành công' : 'thất bại'; ?></label>
                                    <input type="text" class="form-control" disabled value="<?php echo $commentStatus; ?>">
                                </div>
                                <div class="form-group divOrderCode" id="divCommentStatus"<?php if($customerConsult['RemindStatusId'] == 1) echo ' style="display: none;"'; ?>>
                                    <label class="control-label">Đơn hàng</label>
                                    <input type="text"  disabled class="form-control" value="<?php echo $this->Mconstants->getObjectValue($listOrders, 'OrderId', $customerConsult['OrderId'], 'OrderCode'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="box box-default padding15">
                            <div class="box-header with-border">
                                <h3 class="box-title">Sản phẩm được tư vấn</h3>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered">
                                        <thead class="theadNormal">
                                        <tr>
                                            <th>Sản phẩm</th>
                                            <th class="text-center" style="width: 150px;">Giá</th>
                                            <th style="width: 85px;">Số lượng</th>
                                            <th>Ghi chú</th>
                                            <th style="width: 5px;"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyProduct">
                                            <?php $products = array();
                                            $productChilds = array();
                                            foreach($listConsultProducts as $op){
                                                if(!isset($products[$op['ProductId']])) $products[$op['ProductId']] = $this->Mproducts->get($op['ProductId'], true, '', 'ProductName, ProductImage, ProductKindId');
                                                $productName = $products[$op['ProductId']]['ProductName'];
                                                $productImage = $products[$op['ProductId']]['ProductImage'];
                                                $productChildName = '';
                                                $productKindId = $products[$op['ProductId']]['ProductKindId'];
                                                if($op['ProductChildId'] > 0){
                                                    if(!isset($productChilds[$op['ProductChildId']])) $productChilds[$op['ProductChildId']] = $this->Mproductchilds->get($op['ProductChildId'], true, '', 'ProductName, ProductImage');
                                                    $productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
                                                    $productImage = $productChilds[$op['ProductChildId']]['ProductImage'];
                                                }
                                                if(empty($productImage)) $productImage = NO_IMAGE; ?>
                                                <tr data-id="<?php echo $op['ProductId']; ?>" data-child="<?php echo $op['ProductChildId']; ?>" >
                                                    <td>
                                                        <img src="<?php echo PRODUCT_PATH.$productImage; ?>" class="productImg">
                                                        <a href="<?php echo $productKindId == 3 ? 'javascript:void(0)' : base_url('product/edit/'.$op['ProductId']); ?>" class="light-dark aProductLink" target="_blank">
                                                            <?php echo $productName;
                                                            if(!empty($productChildName)) echo ' ('.$productChildName.')';
                                                            elseif($productKindId == 3) echo ' (Combo)'; ?>
                                                        </a>
                                                    </td>
                                                    <td class="tdPrice text-right"><?php echo priceFormat($op['Price']); ?></td>
                                                    <td><input type="text" class="form-control quantity" value="<?php echo $op['Quantity']; ?>"></td>
                                                    <td><input type="text" class="form-control comment" value="<?php echo $op['Comment']; ?>"></td>
                                                    <td class="text-right">
                                                        <a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="border-top-title-main">
                                    <div class="clearfix">
                                        <div class="box-search-advance product">
                                            <div>
                                                <input type="text" class="form-control textbox-advancesearch" id="txtSearchProduct" placeholder="Tìm kiếm sản phẩm">
                                            </div>
                                            <div class="panel panel-default" id="panelProduct">
                                                <div class="panel-body" style="width:100%;">
                                                    <div class="list-search-data">
                                                        <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                        <div>
                                                            <div class="form-group pull-right" style="width: 300px;">
                                                                <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', 0, true, 'Nhóm sản phẩm', ' select2'); ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="table-responsive no-padding divTable">
                                                            <table class="table table-hover table-bordered">
                                                                <thead class="theadNormal">
                                                                <tr>
                                                                    <th style="width: 100px;">Ảnh</th>
                                                                    <th>Sản phẩm</th>
                                                                    <th style="width: 100px;">Mã sản phẩm</th>
                                                                    <th style="width: 100px;">Giá bán</th>
                                                                    <th style="width: 100px;">Bảo hành</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="tbodyProductSearch"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="btn-group pull-right">
                                                        <button type="button" class="btn btn-default" id="btnPrevProduct"><i class="fa fa-chevron-left"></i></button>
                                                        <button type="button" class="btn btn-default" id="btnNextProduct"><i class="fa fa-chevron-right"></i></button>
                                                        <input type="text" hidden="hidden" id="pageIdProduct" value="1">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $this->load->view('includes/action_logs_new'); ?>
                    </div>
                    <div class="col-sm-4">
                        <div class="box box-default classify" style="padding: 10px;">
                            <label class="light-blue">Lịch sử xử lý</label>
                            <div class="listComment" id="listComment">
                                <?php $i = 0;
                                $now = new DateTime(date('Y-m-d'));
                                foreach($listConsultComments as $oc){
                                    $avatar = (empty($oc['Avatar']) ? NO_IMAGE : $oc['Avatar']);
                                    $i++;
                                    if($i < 3){ ?>
                                    <div class="box-customer mb10" data-id="<?php echo $oc['UserId']; ?>" data-consult="<?php echo $oc['ConsultCommentId']; ?>">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <th rowspan="2" valign="top" style="width: 50px;"><img src="<?php echo USER_PATH.$avatar; ?>" alt=""></th>
                                                <th><a href="javascript:void(0)" class="name"><?php echo $oc['FullName']; ?></a></th>
                                                <th class="time">
                                                    <?php $dayDiff = getDayDiff($oc['CrDateTime'], $now);
                                                    echo getDayDiffText($dayDiff).ddMMyyyy($oc['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td><p class="pComment" data-id="<?php echo $oc['CommentTypeId']; ?>"><?php echo $oc['Comment']; ?></p></td>
                                                <td class="tdIcon">
                                                    <?php $flag = false;
                                                    foreach($consultUsers2 as $cu){
                                                        if($oc['ConsultCommentId'] == $cu['ConsultCommentId']){
                                                            $flag = $cu['Percent'] > 0;
                                                            break;
                                                        }
                                                    }
                                                    if($flag) echo '<i class="fa fa-flag" style="margin-right: 5px;color: #357ebd;"></i>'; ?>
                                                    <span class="label <?php echo $oc['CommentTypeId'] == 1 ? 'label-success' : 'label-default'; ?>"><?php echo $oc['CommentTypeId'] == 1 ? 'Gọi điện' : 'Facebook'; ?></span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php } } ?>
                            </div>
                            <?php if(count($listConsultComments) > 2){ ?>
                                <div class="text-right light-dark">
                                    <a href="javascript:void(0)" id="aShowComment">Xem tất cả &gt;&gt;</a>
                                </div>
                            <?php } ?>
                        </div>
                        <?php if($customerConsult['CustomerId'] == 0){ ?>
                        <div class="box box-default boxChooseCustomer">
                            <div class="box-header with-border">
                                <h3 class="box-title">Thông tin khách hàng</h3>
                            </div>
                            <div class="box-body mh-wrap-customer">
                                <div class="mh-info-customer">
                                <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                                    <div class="name-info" style="margin-top: -10px;">
                                        <h4 class="n-name <?php echo $customerConsult['CustomerId'] == 0 ? 'name_info_green':'name_info_blue' ?>"><?php echo $customerConsult['FullName']; ?></h4>
                                        <div class="phones n-phone"><?php echo $customerConsult['PhoneNumber']; ?></div>
                                        <div class="phones i-phone" style="display: none;"><?php echo $customerConsult['PhoneNumber']; ?></div>
                                        <h4 class="i-name <?php echo $customerConsult['CustomerId'] == 0 ? 'name_info_green':'name_info_blue' ?>" style="display: none;"><?php echo $customerConsult['FullName']; ?></h4>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="item i-address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <span class="n-ward"><?php echo $customerConsult['CustomerAddress'] . ' ' . $this->Mwards->getWardName($customerConsult['WardId']); ?></span>
                                            <span class="br-line n-district"><?php echo $this->Mconstants->getObjectValue($listDistricts, 'DistrictId', $customerConsult['DistrictId'], 'DistrictName'); ?></span>
                                            <span class="br-line n-province"><?php echo $this->Mconstants->getObjectValue($listProvinces, 'ProvinceId', $customerConsult['ProvinceId'], 'ProvinceName'); ?></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-id-card" aria-hidden="true"></i>
                                            <span class="i-country" data-id="232" data-province="0" data-district="0" data-ward="0" data-zip="">Việt Nam</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="box box-default boxChooseCustomer">
                            <div class="box-header with-border">
                                <h3 class="box-title">Khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnAddCustomer"><i class="fa fa-plus"></i> Thêm</button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="box-search-advance customer">
                                    <div>
                                        <input type="text" class="form-control textbox-advancesearch" id="txtSearchCustomer" placeholder="Tìm khách hàng">
                                    </div>
                                    <div class="panel panel-default" id="panelCustomer">
                                        <div class="panel-body">
                                            <div class="list-search-data">
                                                <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                <ul id="ulListCustomers"></ul>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default" id="btnPrevCustomer"><i class="fa fa-chevron-left"></i></button>
                                                <button type="button" class="btn btn-default" id="btnNextCustomer"><i class="fa fa-chevron-right"></i></button>
                                                <input type="text" hidden="hidden" id="pageIdCustomer" value="1">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default mh-wrap-customer" id="divCustomer" style="display: none;">
                            <div class="with-border">
                                <h3 class="box-title">Thông tin khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnCloseBoxCustomer"><i class="fa fa-times" style="font-size: 18px;color:#777"></i></button>
                                </div>
                                <!--<div class="box-call-start  pull-right">
                                    <div style="display: none;">
                                        <a class="nav-item nav-link " href="javascript:void(0)" id="sip-account"></a>
                                        <span id="ua-status"></span><span id="call-status"></span>
                                        <input type="hidden" id="passWord" value="<?php //echo $ext['Password'] ?>">
                                        <input type="hidden" id="userName" value="<?php //echo $ext['UserId'] ?>">
                                        <input type="hidden" id="uri" value="sip:<?php //echo $ext['UserId'] ?>@pbx01.cmctelecom.vn">
                                        <input type="hidden" id="wsServers" value="wss://pbx01.cmctelecom.vn:7443">
                                        <input type="text" hidden="hidden" id="phone-input" value="">
                                    </div>
                                    <button id="detail-call" type="button" class="btn btn-success detail-call"><i class="fa fa-phone"></i></button>
                                </div>-->
                                <div class="mh-info-customer">
                                    <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                                    <div class="name-info">
                                        <h4 class="i-name <?php echo $customerConsult['CustomerId'] == 0 ? 'name_info_green':'name_info_blue' ?>"></h4>
                                        <div class="phones i-phone"></div>
                                    </div>
                                    <div class="i-cusType">
                                        <span class="label label-success"></span>
                                    </div>
                                    <div class="total-orders">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>Số đơn hàng &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<span class="i-total-orders"></span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-money" aria-hidden="true"></i><span>Số dư tài khoản :&nbsp;&nbsp;<span id="customerBalance">0</span>₫</span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-facebook" aria-hidden="true"></i><span id="spanFacebook"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <!-- <div>
                                    <h4 class="mgbt-20 light-blue">Thông tin khách hàng</h4>
                                </div> -->
                                <div class="row">
                                    <div class="col-sm-12">
                                       <!--  <div class="item">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            <span class="i-name"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span class="i-phone"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span class="i-email"></span>
                                        </div> -->
                                        <div class="item i-address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <span class="i-ward"></span>
                                            <span class="br-line i-district"></span>
                                            <span class="br-line i-province"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-id-card" aria-hidden="true"></i>
                                            <span class="i-country" data-id="232" data-province="0" data-district="0" data-ward="0" data-zip="">Việt Nam</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default" id="divSell">
                            <div class="box-header with-border">
                                <h3 class="box-title">Đăng ký chốt</h3>
                            </div>
                            <div class="box-body table-responsive no-padding divTable">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Thời gian</th>
                                        <th>Nhân viên</th>
                                        <th class="text-center">Phần trăm</th>
                                        <th style="width: 50px;"></th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbodySell">
                                    <?php $i = 0;
                                    foreach($consultUsers2 as $cu){
                                        if($cu['Percent'] == 0) $i++; ?>
                                        <tr id="tdSellUser_<?php echo $cu['UserId']; ?>"<?php if($cu['Percent'] == 0) echo ' style="display: none;"'; ?>>
                                            <td><?php echo empty($cu['UpdateDateTime']) ? ddMMyyyy($cu['CrDateTime'], 'd/m/Y H:i') : ddMMyyyy($cu['UpdateDateTime'], 'd/m/Y H:i'); ?></td>
                                            <td><?php echo $cu['FullName']; ?></td>
                                            <td class="text-center"><span class="spanPercent"><?php echo $cu['Percent']; ?></span> %</td>
                                            <td class="actions text-center">
                                                <?php if($cu['UserId'] == $user['UserId']){ ?>
                                                    <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $cu['CustomerConsultUserId']; ?>" data-consult="<?php echo $cu['ConsultCommentId']; ?>"><i class="fa fa-pencil"></i></a>
                                                    <input type="text" hidden="hidden" class="comment" value="<?php echo $cu['Comment']; ?>">
                                               <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                <input type="text" hidden="hidden" id="countConsultUser" value="<?php echo count($consultUsers2); ?>">
                                <input type="text" hidden="hidden" id="countConsultUserHasPercent" value="<?php echo $i; ?>">
                            </div>
                        </div>
                        <?php if(!empty($consultUsers1)){ ?>
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Thực chốt</h3>
                            </div>
                            <div class="box-body table-responsive no-padding divTable">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Nhân viên</th>
                                        <th class="text-center">Vai trò</th>
                                        <th class="text-center">Phần trăm</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($consultUsers1 as $cu){ ?>
                                        <tr>
                                            <td><?php echo $cu['FullName']; ?></td>
                                            <td class="text-center"><?php echo $cu['IsCrOrder'] == 2 ? '<span class="label label-primary">Tạo đơn</span>' : '<span class="label label-default">Tư vấn</span>'; ?></td>
                                            <td class="text-center"><?php echo $cu['Percent']; ?> %</td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <input class="btn btn-primary submit" type="submit" name="submit" value="Lưu" data-order="">
                    <li><a href="<?php echo base_url('customerconsult'); ?>" class="btn btn-default">Đóng</a></li>
                    <input type="text" hidden="hidden" id="getCustomerDetailUrl" value="<?php echo base_url('api/customer/get'); ?>">
                    <input type="text" hidden="hidden" id="getListCustomerUrl" value="<?php echo base_url('api/customer/getList'); ?>">
                    <input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getList'); ?>">
                    <input type="text" hidden="hidden" id="insertCommentUrl" value="<?php echo base_url('customerconsult/insertComment'); ?>">
                    <input type="text" hidden="hidden" id="ediCustomerConsultUrl" value="<?php echo base_url('customerconsult/edit'); ?>">
                    <input type="text" hidden="hidden" id="updateCustomerFacebookUrl" value="<?php echo base_url('customerconsult/updateCustomerFacebook'); ?>">
                    <input type="text" hidden="hidden" id="editCustomerUrl" value="<?php echo base_url('customer/edit'); ?>">
                    <input type="text" hidden="hidden" id="getListOrderUrl" value="<?php echo base_url('api/order/search'); ?>">
                    <input type="text" hidden="hidden" id="customerId" value="<?php echo $customerConsult['CustomerId']  ?>">
                    <input type="text" hidden="hidden" id="ctvId" value="<?php echo $customerConsult['CTVId']  ?>">
                    <input type="text" hidden="hidden" id="userId" value="<?php echo $customerConsult['UserId']; ?>">
                    <input type="text" hidden="hidden" id="partId" value="<?php echo $customerConsult['PartId']; ?>">
                    <input type="text" hidden="hidden" id="orderId" value="<?php echo $customerConsult['OrderId']; ?>">
                    <input type="text" hidden="hidden" id="customerConsultId" value="<?php echo $customerConsultId ?>">
                    <input type="text" hidden="hidden" id="consultDateOld" value="<?php echo ddMMyyyy($customerConsult['ConsultDate'], 'd/m/Y H:i'); ?>">
                    <input type="text" hidden="hidden" id="timeProcessed" value="<?php echo $customerConsult['TimeProcessed']; ?>">
                    <input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
                    <input type="text" hidden="hidden" id="remindStatusId" value="<?php echo $customerConsult['RemindStatusId'] ?>">
                </ul>
                <?php echo form_close(); ?>
                <?php $this->load->view('includes/modal/add_customer', array('listCountries' => $listCountries, 'listProvinces' => $listProvinces, 'listDistricts' => $listDistricts)); ?>
                <?php //$this->load->view('includes/modal/comment', array('itemName' => 'Lịch sử xử lý', 'listItemComments' => $listComments)); ?>
                <div class="modal fade" id="modalItemComment" tabindex="-1" role="dialog" aria-labelledby="modalItemComment">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="box-title"><i class="fa fa-comments-o"></i> Lịch sử xử lý</h4>
                            </div>
                            <div class="modal-body">
                                <div class="listComment">
                                    <?php $now = new DateTime(date('Y-m-d'));
                                    foreach($listConsultComments as $ic){
                                        $avatar = (empty($ic['Avatar']) ? NO_IMAGE : $ic['Avatar']); ?>
                                        <div class="box-customer mb10" data-id="<?php echo $ic['UserId']; ?>" data-consult="<?php echo $ic['ConsultCommentId']; ?>">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <th rowspan="2" valign="top" style="width: 50px;"><img src="<?php echo USER_PATH.$avatar; ?>" alt=""></th>
                                                    <th><a href="javascript:void(0)" class="name"><?php echo $ic['FullName']; ?></a></th>
                                                    <th class="time">
                                                        <?php $dayDiff = getDayDiff($ic['CrDateTime'], $now);
                                                        echo getDayDiffText($dayDiff).ddMMyyyy($ic['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td><p class="pComment" data-id="<?php echo $ic['CommentTypeId']; ?>"><?php echo $ic['Comment']; ?></p></td>
                                                    <td class="tdIcon">
                                                        <?php $flag = false;
                                                        foreach($consultUsers2 as $cu){
                                                            if($ic['ConsultCommentId'] == $cu['ConsultCommentId']){
                                                                $flag = $cu['Percent'] > 0;
                                                                break;
                                                            }
                                                        }
                                                        if($flag) echo '<i class="fa fa-flag" style="margin-right: 5px;color: #357ebd;"></i>'; ?>
                                                        <span class="label <?php echo $ic['CommentTypeId'] == 1 ? 'label-success' : 'label-default'; ?>"><?php echo $ic['CommentTypeId'] == 1 ? 'Gọi điện' : 'Facebook'; ?></span>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modalSell" tabindex="-1" role="dialog" aria-labelledby="modalSell">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"><i class="fa fa-money"></i> Đăng ký doanh số</h4>
                            </div>
                            <div class="modal-body">
                                <div class="no-padding">
                                    <div class="table-responsive divTable">
                                        <table class="table table-hover">
                                            <thead class="theadNormal">
                                            <tr>
                                                <th>Nhân viên</th>
                                                <th class="text-center">Phần trăm</th>
                                                <th>Ghi chú chốt</th>
                                            </tr>
                                            </thead>
                                            <tbody id="tbodyUserPercent"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="button" id="btnUpdatePercent">Hoàn thành</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                <input type="text" hidden="hidden" id="customerConsultUserId" value="0">
                                <input type="text" hidden="hidden" id="consultCommentId" value="0">
                                <input type="text" hidden="hidden" id="updateCustomerConsultUserUrl" value="<?php echo base_url('customerconsult/updateCustomerConsultUser'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modalConfirmStatus" tabindex="-1" role="dialog" aria-labelledby="modalConfirmStatus">
                    <div class="modal-dialog">
                        <div class="modal-content"  style="width: 500px;margin: 0 auto">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Xác nhận trạng thái</h4>
                            </div>
                            <div class="modal-body">
                                <p class="text-center text-info divOrderCode"<?php if($customerConsult['RemindStatusId'] != 5) echo ' style="display: none;"'; ?>>Tư vấn lại thành công chỉ khi khách được tạo đơn hàng</p>
                                <div class="form-group">
                                    <?php $commentStatus = "";
                                    if($customerConsult['RemindStatusId'] == 4 || $customerConsult['RemindStatusId'] == 6) $commentStatus = $customerConsult['CommentCancel'];
                                    else if($customerConsult['RemindStatusId'] == 5) $commentStatus = $customerConsult['CommentComplete']; ?>
                                    <label class="control-label normal lbCommentStatus">Ghi chú <?php echo $customerConsult['RemindStatusId'] == 5 ? 'thành công' : 'thất bại'; ?> <span class="required">*</span></label>
                                    <input type="text" id="commentStatus" class="form-control" value="<?php echo $commentStatus; ?>">
                                </div>
                                <div class="form-group divOrderCode"<?php if($customerConsult['RemindStatusId'] != 5) echo ' style="display: none;"'; ?>>
                                    <!-- <label class="control-label normal">Đơn hàng <span class="required">*</span></label> -->
                                    <!-- <input type="text" id="orderCode" class="form-control" value="<?php // echo if($customerConsult['OrderId'] > 0) echo $this->Morders->genOrderCode($customerConsult['OrderId']); ?>"> -->
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <?php $this->Mconstants->selectObject($listOrders, 'OrderId', 'OrderCode', 'Order_Id', 0, true, 'Đơn đã mua', ' select2'); ?>
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <button type="button" class="btn btn-primary btnCreateOrder" style="width: 100%">Tạo đơn mới</button>
                                        </div>
                                        <div id="createOrder" style="display: none">
                                            <div class="col-sm-6 ">
                                                <a type="button" class="btn btn-primary submit" data-order="POS" style="width: 100%">Tạo đơn (POS)</a>
                                            </div>
                                            <div class="col-sm-6">
                                                <a type="button" class="btn btn-primary submit" data-order="ship" style="width: 100%">Tạo đơn ship</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input class="btn btn-primary submit check_order" data-order="" type="button" value="Hoàn thành" style="display: none">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                <input type="hidden" value="<?php echo base_url('order/addPos/1'); ?>" id="urlOrderAddPos">
                                <input type="hidden" value="<?php echo base_url('order/add/1'); ?>" id="urlOrderAddShip">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php } ?>
        </div>
    </div>
    <audio id="callPlay">
        <source src="<?php echo base_url('assets/audio/call.mp3'); ?>" type="audio/mpeg">
        Your browser does not support the audio element.
    </audio>
    <audio id="closingCall">
        <source src="<?php echo base_url('assets/audio/beep.mp3'); ?>" type="audio/mpeg">
        Your browser does not support the audio element.
    </audio>
    <h3 style="display: none" class="PhoneNumber"></h3>
    <!-- <input type="hidden" id="getCallLogUpdateUrl" value="<?php // echo base_url('call/updateDataTable'); ?>"> -->
    <!-- <input type="hidden" id="getUpdateIsCallAgainUrl" value="<?php // echo base_url('api/call/updateIsCallAgain'); ?>"> -->
    <div style="display: none">
        <video id="player" autoplay="" data-chrome-better-html5-video-type=""></video>
    </div>
    <div class="modal modal-call fade bd-example-modal-md" id="modalAnswer" tabindex="-1" role="dialog" aria-labelledby="modalAnswer" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document" style="width: 800px!important;">
            <div class="modal-content">
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-12 text-center">
                                <h3 class="PhoneNumber">vcvcvcxvcx</h3>
                                <div id="timer"></div>
                            </div>
                        </div>
                        <div class="form-row">
                            <!-- <input id="call-button" type="hidden" value="Answer" class="btn btn-success"> -->
                            <input id="call-button" type="hidden" value="Answer" class="sr-only">
                            <div class="col-6 text-center button-control box-call-end">
                                <button type="button" class="btn btn-danger" id="call-end">
                                    <i class="fa fa-phone"></i>
                                    <!-- <button type="button" class="btn btn-danger" id="call-end" data-dismiss="modal">
                                        <i class="fa fa-phone"></i> -->
                                </button>
                            </div>
                            <div class="col-6 text-center button-control box-call-answer">
                                <button type="button" class="btn btn-success" id="call-answer">
                                    <!-- <button id="answer-button" type="button" value="Answer" class="btn btn-success"> -->
                                    <i class="fa fa-phone"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="note-call">
                        <?php if(!empty($listConsultComments)): ?>
                            <div class="row">
                                <div class="col-12">
                                    <?php foreach ($listConsultComments as $key => $oc): ?>
                                        <div class="person-info-comment">
                                            <ul class="comment-block">
                                                <li class="media-header"><a href="javascript:void(0)"><h5 class="name"><?php echo $oc['FullName']; ?></h5></a></li>
                                                <li><p class="comment-time text-secondary">
                                                        <?php $dayDiff = getDayDiff($oc['CrDateTime'], $now);
                                                        echo getDayDiffText($dayDiff).ddMMyyyy($oc['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                                    </p></li>
                                                <li><p class="comment"><?php echo $oc['Comment']; ?></p></li>
                                            </ul>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>