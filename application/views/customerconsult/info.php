<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php if ($customerConsultId > 0) { ?>
                <section class="content-header">
                    <h1>Thông tin Data "<?= $customerConsult['ConsultTitle'] ?>" chi tiết</h1>
                    <ul class="list-inline">
                        <li><a href="<?php echo base_url('customerconsult'); ?>" class="btn btn-default">Đóng</a></li>
                    </ul>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-sm-8 no-padding">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li ><a href="#tab_4" data-toggle="tab" data-id="4">Tư vấn lại</a></li>
                                    <li><a href="#tab_5" data-toggle="tab" data-id="5">CSKH</a></li>
                                    <li class="active"><a href="#tab_3" data-toggle="tab" data-id="3">Thông tin khách hàng</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane " id="tab_4">
                                        <div class="box-body table-responsive divTable">
                                            <table class="table new-style table-hover table-bordered table-data" id="table-data-consult">
                                                <thead>
                                                <tr>
                                                    <th>Tiêu đề tư vấn</th>
                                                    <th class="text-center">Số lần TVL</th>
                                                    <th>Thời điểm cần xử lý</th>
                                                    <th>Ngày tạo</th>
                                                    <th>Người xem</th>
                                                    <th class="text-center">Trạng thái</th>
                                                    <th class="text-center">Kênh</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbodyCustomerConsult"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_5">
                                        <div class="box-body table-responsive divTable">
                                            <table class="table new-style table-hover table-bordered table-data" id="table-data-sms-log">
                                                <thead>
                                                <tr>
                                                    <th>Ngày</th>
                                                    <th>Loại</th>
                                                    <th>Bởi</th>
                                                    <th>Nội dung</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbodyLogSms">
                                               <?php if($flagCustomerInfo == true && count($listSmsLog) > 0){
                                                    foreach ($listSmsLog as $s){
                                                    ?>
                                                        <tr>
                                                            <td><?php echo ddMMyyyy($s['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                                            <td><?php echo $this->Mconstants->statusSMS['statusSms'][$s['SMSTypeId']]; ?></td>
                                                            <td><?php echo $s['FullName']; ?></td>
                                                            <td><?php echo $s['SendContent']; ?></td>
                                                        </tr>
                                                <?php }} ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane active" id="tab_3">
                                    	<?php if($flagCustomerInfo == true): ?>
                                         <?php echo form_open('api/customer/update', array('id' => 'customerFormUpdate')); ?>
                                        <div class="flexbox-annotated-section">
                                            <div class="flexbox-annotated-section-annotation">
                                                <div class="annotated-section-title pd-all-20"><h2>Thông tin chung</h2></div>
                                                <div class="annotated-section-description pd-all-20 p-none-t">
                                                    <p class="color-note tabcustomer in privateCus">Một số thông tin cơ bản của khách hàng.</p>
                                                </div>
                                            </div>
                                            <div class="flexbox-annotated-section-content">
                                                <div class="box box-default padding15">
                                                    <div class="box-header with-border">
                                                        <div class="row">
                                                            <div class="col-sm-12" style="margin-bottom: 10px;">
                                                                <ul class="nav nav-tabs" id="ulCustomerKindId">
                                                                    <?php foreach($this->Mconstants->customerKinds as $i => $v ){ ?>
                                                                        <li class="<?php echo $i == $customerConsult['CustomerKindId'] ? 'active':''; ?>">
						                                                    <a href="javascript:void(0)" data-toggle="tab" data-id="<?php echo $i; ?>"><?php echo $v; ?></a>
						                                                </li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="radio-group">
                                                                    <span class="item"><input type="radio" name="CustomerTypeId" class="iCheck iCheckCustomerType" value="1"<?php if($customerConsult['CustomerTypeId'] == 1) echo ' checked' ?>> Cá nhân</span>
                                                                    <span class="item"><input type="radio" name="CustomerTypeId" class="iCheck iCheckCustomerType" value="2"<?php if($customerConsult['CustomerTypeId'] == 2) echo ' checked' ?>> Công ty</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                   <span class="item"><input type="checkbox" class="iCheck" id="isReceiveAd"<?php if($customerConsult['IsReceiveAd'] == 2) echo ' checked' ?>> Có Nhận Quảng Cáo</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-body">
                                                        <div class="row">

                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Họ <span class="required">*</span></label>
                                                                    <input type="text" id="firstNamec" class="form-control hmdrequired" value="<?php echo $customerConsult['FirstName']; ?>" data-field="Họ">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Tên <span class="required">*</span></label>
                                                                    <input type="text" id="lastNamec" class="form-control hmdrequired" value="<?php echo $customerConsult['LastName']; ?>" data-field="Tên">
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Số điện thoại</label>
                                                                    <input type="text" id="phoneNumberc" class="form-control" value="<?php echo $customerConsult['PhoneNumber']; ?>" data-field="Số điện thoại">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Email </label>
                                                                    <input type="text" id="emailc" class="form-control" value="<?php echo $customerConsult['CustomerEmail']; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Giới tính</label>
                                                                    <div class="radio-group">
                                                                        <span class="item"><input type="radio" name="GenderId" class="iCheck" value="1" <?php if($customerConsult['GenderId'] == 1) echo ' checked' ?>> Nam</span>
                                                                        <span class="item"><input type="radio" name="GenderId" class="iCheck" value="2" <?php if($customerConsult['GenderId'] == 2) echo ' checked' ?>> Nữ</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Ngày sinh </label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </span>
                                                                        <input type="text" class="form-control datepicker" id="birthDay" value="<?php  echo  ddMMyyyy($customerConsult['BirthDay']); ?>" autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label class="control-label">Mã FB Hara</label>
                                                                    <input type="text" id="facebook" class="form-control" value="<?php  echo  $customerConsult['Facebook'] ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="hr-new">
                                        <div class="flexbox-annotated-section">
                                            <div class="flexbox-annotated-section-annotation">
                                                <div class="annotated-section-title pd-all-20">
                                                    <h2>Địa chỉ</h2>
                                                </div>
                                                <div class="annotated-section-description pd-all-20 p-none-t">
                                                    <p class="color-note tabcustomer in privateCus">Địa chỉ chính của khách hàng này.</p>
                                                </div>
                                            </div>
                                            <div class="flexbox-annotated-section-content">
                                                <div class="box box-default padding15">
                                                    <div class="box-body">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Quốc gia </label>
                                                                    <?php $this->Mconstants->selectObject($listCountries, 'CountryId', 'CountryName', 'CountryId', $customerConsult['CountryId'], false, '', ' select2'); ?> 
                                                                  
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 VNon">
                                                                <div class="form-group">
                                                                    <label class="control-label">Tỉnh / Thành </label>
                                                                    <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', $customerConsult['ProvinceId'], true, '--Tỉnh / Thành--', ' select2'); ?>
                                                                </div>
                                                              
                                                            </div>
                                                        </div>
                                                        <div class="row VNon">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Quận/ Huyện</label>
                                                                  
                                                                    <?php echo $this->Mdistricts->selectHtml($customerConsult['DistrictId'], 'DistrictId', $listDistricts); ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Phường / Xã </label>
                                                                    <?php echo $this->Mwards->selectHtml($customerConsult['WardId']); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Địa chỉ </label>
                                                            <input type="text" id="address" class="form-control" value="<?php  echo $customerConsult['CustomerAddress']; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="hr-new">
                                        <div class="flexbox-annotated-section">
                                        	<div class="flexbox-annotated-section-annotation">
                                                <div class="annotated-section-title pd-all-20">
                                                    <h2>Thông tin thêm</h2>
                                                </div>
                                            </div>
                                            <div class="flexbox-annotated-section-content">
                                            	<div class="box box-default padding15">
							                        <div class="box-body">
							                            <div class="row">
							                                <div class="col-sm-6">
							                                    <div class="form-group">
							                                        <label class="control-label">Nhóm khách hàng</label>
							                                        <select class="form-control" name="CustomerGroupId" id="customerGroupId">
							                                            <option value="0">--Chọn--</option>
							                                            <?php foreach($listCustomerGroups as $cg){ ?>
                                                                                <option value="<?php echo $cg['CustomerGroupId']; ?>" class="op op_<?php echo $cg['CustomerKindId']; ?>"<?php if($cg['CustomerKindId'] != $customerConsult['CustomerKindId']) echo ' style="display: none;"'; if($cg['CustomerGroupId'] == $customerConsult['CustomerGroupId']) echo ' selected="selected"'; ?>><?php echo $cg['CustomerGroupName']; ?></option>
                                                                            <?php } ?>
							                                        </select>
							                                    </div>
							                                </div>
							                                <div class="col-sm-6">
							                                    <div class="form-group">
							                                        <label class="control-label">Nhân viên chăm sóc chính</label>
							                                        <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'CareStaffId',$customerConsult['CareStaffId'], true, '--Chọn--', ' select2'); ?>
							                                    </div>
							                                </div>
							                                <div class="col-sm-6">
							                                    <div class="form-group">
							                                        <label class="control-label">Chiết khấu giá</label>
							                                        <?php $this->Mconstants->selectConstants('discountType', 'DiscountTypeId', $customerConsult['DiscountTypeId'], true, '--Chọn--'); ?>
							                                    </div>
							                                </div>
							                            </div>
							                            
							                            <div class="row">
							                                <div class="col-sm-6">
							                                    <div class="form-group">
							                                        <label class="control-label">Mật khẩu</label>
							                                        <input type="text" id="password" class="form-control" value="">
							                                    </div>
							                                </div>
							                                <div class="col-sm-6">
							                                    <div class="form-group">
							                                        <label class="control-label">Nhắc lại mật khẩu</label>
							                                        <input type="text" id="rePass" class="form-control" value="">
							                                    </div>
							                                </div>
							                            </div>
							                        </div>
							                    </div>
						                    </div>
					                    </div>
                                        <ul class="list-inline pull-right margin-right-10">
                                            <li>
                                                <button type="button" class="btn btn-primary" id="updateDataConsult">Lưu</button>
                                                <!-- <input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"> -->
                                            </li>
                                            <li><a href="<?php echo base_url('customerconsult'); ?>" id="customerListUrl" class="btn btn-default">Đóng</a></li>
                                            
                                            <input type="text" hidden="hidden" id="customerKindId" value="<?= $customerConsult['CustomerKindId'] ?>">
                                        </ul>
                                        <div class="clearfix"></div>
                                        <?php echo form_close(); ?>
                                    	<?php endif; ?>
	                                    <input type="text" hidden="hidden" id="getCustomerDetailUrl" value="<?php echo base_url('api/customer/get'); ?>">
					                    <input type="text" hidden="hidden" id="getListCustomerUrl" value="<?php echo base_url('api/customer/getList'); ?>">
					                    <input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getList'); ?>">
					                    <input type="text" hidden="hidden" id="insertCommentUrl" value="<?php echo base_url('customerconsult/insertComment'); ?>">
					                    <input type="text" hidden="hidden" id="ediCustomerConsultUrl" value="<?php echo base_url('customerconsult/edit'); ?>">
					                    <input type="text" hidden="hidden" id="updateCustomerFacebookUrl" value="<?php echo base_url('customerconsult/updateCustomerFacebook'); ?>">
					                    <input type="text" hidden="hidden" id="editCustomerUrl" value="<?php echo base_url('customer/edit'); ?>">
					                    <input type="text" hidden="hidden" id="getListOrderUrl" value="<?php echo base_url('api/order/search'); ?>">
					                    
					                    <input type="text" hidden="hidden" id="ctvId" value="<?php echo $customerConsult['CTVId']  ?>">
					                    <input type="text" hidden="hidden" id="userId" value="<?php echo $customerConsult['UserId']; ?>">
					                    <input type="text" hidden="hidden" id="partId" value="<?php echo $customerConsult['PartId']; ?>">
					                    <input type="text" hidden="hidden" id="orderId" value="<?php echo $customerConsult['OrderId']; ?>">
					                    <input type="text" hidden="hidden" id="customerConsultId" value="<?php echo $customerConsultId ?>">
					                    <input type="text" hidden="hidden" id="consultDateOld" value="<?php echo ddMMyyyy($customerConsult['ConsultDate'], 'd/m/Y H:i'); ?>">
					                    <input type="text" hidden="hidden" id="timeProcessed" value="<?php echo $customerConsult['TimeProcessed']; ?>">
					                    <input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
					                    <input type="text" hidden="hidden" id="remindStatusId" value="<?php echo $customerConsult['RemindStatusId'] ?>">
					                    <input type="text" hidden="hidden" id="updateDataError" value="<?php echo base_url('customerconsult/updateDataError'); ?>">
                                        <input type="text" hidden="hidden" id="getListConsultUrl" value="<?php echo base_url('customerconsult/searchByFilter');?>">
                                        <input type="hidden" id="urlEditConsult" value="<?php echo base_url('customerconsult/edit'); ?>">
                                        <?php $this->load->view('includes/action_logs_new'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <input type="text" hidden="hidden" id="customerId" value="<?php echo $customerConsult['CustomerId']  ?>">
                        	<?php if($customerConsult['CustomerId'] == 0){ ?>
		                        <div class="box box-default boxChooseCustomer">
		                            <div class="box-header with-border">
		                                <h3 class="box-title">Thông tin khách hàng</h3>
		                                <div class="box-tools pull-right">
		                                    <button type="button" class="btn btn-primary" id="btnAddCustomer">Update thành KH</button>
		                                </div>
		                            </div>
		                            <div class="box-body mh-wrap-customer">
		                                <div class="mh-info-customer">
		                                <img class="avatar-user" src="assets/vendor/dist/img/users.png">
		                                    <div class="name-info" style="margin-top: -10px;">
		                                        <h4 class="n-name <?php echo $customerConsult['CustomerId'] == 0 ? 'name_info_green':'name_info_blue' ?>"><?php echo $customerConsult['FullName']; ?></h4>
		                                        <div class="phones n-phone"><?php echo $customerConsult['PhoneNumber']; ?></div>
		                                        <div class="phones i-phone" style="display: none;"><?php echo $customerConsult['PhoneNumber']; ?></div>
		                                        <h4 class="i-name <?php echo $customerConsult['CustomerId'] == 0 ? 'name_info_green':'name_info_blue' ?>" style="display: none;"><?php echo $customerConsult['FullName']; ?></h4>
		                                    </div>

		                                </div>

		                                <div class="row">
		                                    <div class="col-sm-12">
		                                        <div class="item i-address">
		                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
		                                            <span class="n-ward"><?php echo $customerConsult['CustomerAddress'] . ' ' . $this->Mwards->getWardName($customerConsult['WardId']); ?></span>
		                                            <span class="br-line n-district"><?php echo $this->Mconstants->getObjectValue($listDistricts, 'DistrictId', $customerConsult['DistrictId'], 'DistrictName'); ?></span>
		                                            <span class="br-line n-province"><?php echo $this->Mconstants->getObjectValue($listProvinces, 'ProvinceId', $customerConsult['ProvinceId'], 'ProvinceName'); ?></span>
		                                        </div>
		                                        <div class="item">
		                                            <i class="fa fa-id-card" aria-hidden="true"></i>
		                                            <span class="i-country" data-id="232" data-province="0" data-district="0" data-ward="0" data-zip="">Việt Nam</span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>
		                        <?php } ?>
                            <!-- <div class="box box-default boxChooseCustomer">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Khách hàng</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" id="btnAddCustomer"><i class="fa fa-plus"></i> Thêm</button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="box-search-advance customer">
                                        <div>
                                            <input type="text" class="form-control textbox-advancesearch" id="txtSearchCustomer" placeholder="Tìm khách hàng">
                                        </div>
                                        <div class="panel panel-default" id="panelCustomer">
                                            <div class="panel-body">
                                                <div class="list-search-data">
                                                    <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                    <ul id="ulListCustomers"></ul>
                                                </div>
                                            </div>
                                            <div class="panel-footer">
                                                <div class="btn-group pull-right">
                                                    <button type="button" class="btn btn-default" id="btnPrevCustomer"><i class="fa fa-chevron-left"></i></button>
                                                    <button type="button" class="btn btn-default" id="btnNextCustomer"><i class="fa fa-chevron-right"></i></button>
                                                    <input type="text" hidden="hidden" id="pageIdCustomer" value="1">
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        <div class="box box-default mh-wrap-customer" id="divCustomer" style="display: none;">
                            <div class="with-border">
                                <h3 class="box-title">Thông tin khách hàng</h3>
                                <div class="box-tools pull-right">
                                    
                                </div>
                                <div class="mh-info-customer">
                                    <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                                    <div class="name-info">
                                        <h4 class="i-name <?php echo $customerConsult['CustomerId'] == 0 ? 'name_info_green':'name_info_blue' ?>"></h4>
                                        <div class="phones i-phone"></div>
                                    </div>
                                    <div class="i-cusType">
                                        <span class="label label-success"></span>
                                    </div>
                                    <div class="total-orders">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>Số đơn hàng &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<span class="i-total-orders"></span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-money" aria-hidden="true"></i><span>Số dư tài khoản :&nbsp;&nbsp;<span id="customerBalance">0</span>₫</span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-facebook" aria-hidden="true"></i><span id="spanFacebook"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <!-- <div>
                                    <h4 class="mgbt-20 light-blue">Thông tin khách hàng</h4>
                                </div> -->
                                <div class="row">
                                    <div class="col-sm-12">
                                       <!--  <div class="item">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            <span class="i-name"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span class="i-phone"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span class="i-email"></span>
                                        </div> -->
                                        <div class="item i-address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <span class="i-ward"></span>
                                            <span class="br-line i-district"></span>
                                            <span class="br-line i-province"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-id-card" aria-hidden="true"></i>
                                            <span class="i-country" data-id="232" data-province="0" data-district="0" data-ward="0" data-zip="">Việt Nam</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="box box-default classify padding20">
                                <div class="form-group">
                                    <label class="control-label">Ghi chú</label>
                                    <div class="box-transprt clearfix mb10">
                                        <button type="button" class="btn-updaten save" id="btnInsertComment">Lưu</button>
                                        <input type="text" class="add-text" id="comment" value="" placeholder="Thêm nội dung ghi chú...">
                                    </div>
                                </div>
                                <div id="listComment">
                                    <?php $i = 0;
                                    foreach($listConsultComments as $oc){
                                        $avatar = (empty($oc['Avatar']) ? NO_IMAGE : $oc['Avatar']);
                                        $i++;
                                        if($i < 3){ ?>
                                            <div class="box-customer mb10">
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <th rowspan="2" valign="top" style="width: 50px;"><img src="<?php echo USER_PATH.$avatar; ?>" alt=""></th>
                                                        <th><a href="javascript:void(0)" class="name"><?php echo $oc['FullName']; ?></a></th>
                                                        <th class="time"><?php echo ddMMyyyy($oc['CrDateTime'], 'H:i d/m/Y'); ?></th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <p class="pComment"><?php echo $oc['Comment']; ?></p>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php }
                                    } ?>
                                </div>
                                <?php if(count($listConsultComments) > 2){ ?>
                                    <div class="text-right light-dark">
                                        <a href="javascript:void(0)" id="aShowComment">Xem tất cả &gt;&gt;</a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </section>
                <?php $this->load->view('includes/modal/add_customer', array('listCountries' => $listCountries, 'listProvinces' => $listProvinces, 'listDistricts' => $listDistricts)); ?>
                 <div class="modal fade" id="modalItemComment" tabindex="-1" role="dialog" aria-labelledby="modalItemComment">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="box-title"><i class="fa fa-comments-o"></i> Lịch sử xử lý</h4>
                            </div>
                            <div class="modal-body">
                                <div class="listComment">
                                    <?php $now = new DateTime(date('Y-m-d'));
                                    foreach($listConsultComments as $ic){
                                        $avatar = (empty($ic['Avatar']) ? NO_IMAGE : $ic['Avatar']); ?>
                                        <div class="box-customer mb10" data-id="<?php echo $ic['UserId']; ?>" data-consult="<?php echo $ic['ConsultCommentId']; ?>">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <th rowspan="2" valign="top" style="width: 50px;"><img src="<?php echo USER_PATH.$avatar; ?>" alt=""></th>
                                                    <th><a href="javascript:void(0)" class="name"><?php echo $ic['FullName']; ?></a></th>
                                                    <th class="time">
                                                        <?php $dayDiff = getDayDiff($ic['CrDateTime'], $now);
                                                        echo getDayDiffText($dayDiff).ddMMyyyy($ic['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td><p class="pComment" data-id="<?php echo $ic['CommentTypeId']; ?>"><?php echo $ic['Comment']; ?></p></td>
                                                    <td class="tdIcon">
                                                        <?php $flag = false;
                                                        foreach($consultUsers2 as $cu){
                                                            if($ic['ConsultCommentId'] == $cu['ConsultCommentId']){
                                                                $flag = $cu['Percent'] > 0;
                                                                break;
                                                            }
                                                        }
                                                        if($flag) echo '<i class="fa fa-flag" style="margin-right: 5px;color: #357ebd;"></i>'; ?>
                                                        <span class="label <?php echo $ic['CommentTypeId'] == 1 ? 'label-success' : 'label-default'; ?>"><?php echo $ic['CommentTypeId'] == 1 ? 'Gọi điện' : 'Facebook'; ?></span>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <section class="content"><?php $this->load->view('includes/notice'); ?></section>
            <?php } ?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>