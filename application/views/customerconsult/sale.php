<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('customerconsult/sale'); ?>
                        <div class="row">
                            <div class="col-sm-4">
		                        <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'UserId', set_value('UserId'), true, '--Nhân viên--', ' select2'); ?>
		                    </div>
		                    <div class="col-sm-4">
		                        <div class="form-group">
                                    <div class='input-group' >
                                        <input type='text' class="form-control daterangepicker" name="Date" value="<?php echo set_value('Date') ? set_value('Date') : (date('01/m/Y').' - '.date('d/m/Y')); ?>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
		                    </div>
		                    <div class="col-sm-4">
		                        <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
		                        <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
		                    </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content upn ft-seogeo">
                
                <div class="">
                    <div class="box-body table-responsive divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Đơn hàng</th>
                                <th class="text-center">Ngày chốt</th>
                                <th>Khách hàng</th>
                                <th>Tổng tiền</th>
                                <th>Người tạo</th>
                                <th>Danh sách dang ký chốt</th>
                                <th>Mã TVL</th>
                                <th>Thực chốt</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyCustomerConsultSell">
                            	<?php foreach ($listSells as $s) {
                            		?>
                            		<tr>
                            			<td><a href="<?php echo base_url('order/edit/'.$s['OrderId']); ?>"><?php echo $s['OrderCode']; ?></a></td>
                            			<td><?php echo ddMMyyyy($s['CrDateTime']); ?></td>
                            			<td><a href="<?php echo base_url('customer/edit/'.$s['CustomerId']); ?>"><?php echo $s['FullName']; ?></a></td>
                            			<td><?php echo priceFormat($s['Total']); ?>đ</td>
                            			<td><?php echo $s['FullName1']; ?></td>
                            			<td><?php echo $s['FullName2'].'; '.$s['FullName3'].'; '.$s['FullName4'].'; '; ?></td>
                            			<td><?php echo 'TVL-'.(10000+$s['CustomerConsultId']); ?></td>
                            			<td><?php echo $s['FullName1'].'= '.$s['Percent1'].'; '.$s['FullName2'].'= '.$s['Percent2'].'; '.$s['FullName3'].'= '.$s['Percent3'].'; '.$s['FullName4'].'= '.$s['Percent4']; ?></td>
                                        <td>
                                            <div id="remove_<?php echo $s['CustomerConsultUserId']; ?>">
                                                <span class="jsonDatas" hidden="hidden"><?php echo json_encode($s); ?></span>
                                                <a href="javascript:void(0)" class="showModal"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                            		</tr>
                            	<?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="modal fade" id="modalSale" tabindex="-1" role="dialog" aria-labelledby="modalSale">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-clock-o"></i> Thỏa thuận hoa hồng</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <h4 class="modal-title">Đơn hàng</h4>
                            <table class="table new-style table-hover table-bordered" id="table-data">
                                <thead>
                                <tr>
                                    <th>Đơn hàng</th>
                                    <th class="text-center">Ngày chốt</th>
                                    <th>Khách hàng</th>
                                    <th>Tổng tiền</th>
                                </tr>
                                </thead>
                                <tbody id="tbodyModalOrder">
                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-12 form-group">
                            <h4 class="modal-title">Danh sách phần trăm thỏa thuận</h4>
                            <table class="table new-style table-hover table-bordered" id="table-data">
                                <thead>
                                    <tr class="nameTrALL">
                                     
                                    </tr>
                                </thead>
                                <tbody id="tbodyModalSellAll">
                                    
                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-12 form-group">
                            <h4 class="modal-title">Thêm phần trăm thỏa thuận</h4>
                            <table class="table new-style table-hover table-bordered" id="table-data">
                                <thead>
                                    <tr class="nameTr">
                                     
                                    </tr>
                                </thead>
                                <tbody id="tbodyModalSell">
                                    <tr>
                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" id="btn-save">Hoàn thành</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <input type="text" hidden="hidden" id="customerConsultUserId" value="0">
                    <input type="hidden" hidden="hidden" id="urlUpdateCustomerconsultuser" value="<?php echo base_url('customerconsult/updateCustomerconsultuser') ?>">
                    <input type="hidden" hidden="hidden" id="urlLoadDataCustomerconsultuser" value="<?php echo base_url('customerconsult/loadCustomerconsultuser') ?>">
                </div>
            </div>
        </div>
</div>
<?php $this->load->view('includes/footer'); ?>
