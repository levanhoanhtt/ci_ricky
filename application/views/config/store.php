<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title;?></h1>
            </section>
            <section class="content">
                <div class="box box-default padding15">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tùy chọn cấu hình đơn hàng</h3>
                        <p>Bạn có thể cấu hình đơn hàng, bật/ tắt các chức năng để phù hợp nhất với công việc kinh doanh.</p>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label">Bắt buộc điền Lý do giảm giá</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="checkbox" value="ALLOW_ORDER_DISCOUNT_REASON" class="js-switch"<?php if($listConfigs['ALLOW_ORDER_DISCOUNT_REASON'] == 'ON') echo ' checked'; ?>/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label">Xác thực đơn hàng</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="checkbox" value="ALLOW_VERIFY_ORDER" class="js-switch"<?php if($listConfigs['ALLOW_VERIFY_ORDER'] == 'ON') echo ' checked'; ?>/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label">Tạo phiếu chờ thanh toán</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="checkbox" value="ALLOW_WAIT_PAYMENT_TICKET" class="js-switch"<?php if($listConfigs['ALLOW_WAIT_PAYMENT_TICKET'] == 'ON') echo ' checked'; ?>/>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="text" hidden="hidden" id="autoLoad" value="2">
                <input type="text" hidden="hidden" id="updateConfigUrl" value="<?php echo base_url('api/config/updateItem'); ?>">
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>