<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('librarysentence/update', array('id' => 'librarySentenceForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Tên thư viện</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyLibrarySentence">
                            <?php
                            foreach($listLibrarySentences as $bt){ ?>
                                <tr id="librarySentence_<?php echo $bt['LibrarySentenceId']; ?>">
                                    <td id="libraryName_<?php echo $bt['LibrarySentenceId']; ?>"><?php echo $bt['LibraryName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $bt['LibrarySentenceId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $bt['LibrarySentenceId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <a href="javascript:void(0)" class="link_import" data-id="<?php echo $bt['LibrarySentenceId']; ?>" title="Import"><i class="fa fa-upload"></i></a>
                                        <a href="<?php echo base_url('sentence/'.$bt["LibrarySentenceId"]); ?>" target="_bank" title="Mẫu câu"><i class="fa fa-plus-square"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="libraryName" name="LibraryName" value="" data-field="Tên thư viện"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="LibrarySentenceId" id="librarySentenceId" value="0" hidden="hidden">
                                    <input type="text" id="deleteLibrarySentenceUrl" value="<?php echo base_url('librarysentence/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
    <div class="modal fade" id="modalImportExcel" tabindex="-1" role="dialog" aria-labelledby="modalImportExcel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Import File Excel</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" id="fileExcelUrl" placeholder="File Excel" disabled>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-info btn-flat" id="btnUploadExcel">Upload</button>
                            </span>
                        </div>
                    </div>
                    <img src="assets/vendor/dist/img/loading.gif" class="imgLoading imgCenter" style="display: none;">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="btnImportExcel">Import</button>
                    <input type="text" hidden="hidden" id="importTypeId" value="1">
                </div>
            </div>
        </div>
    </div>
    <input type="text" hidden="hidden" id="importExcelUrl" value="<?php echo base_url('librarysentence/importExcel') ?>">
    <input type="text" hidden="hidden" id="editSentenceUrl" value="<?php echo base_url('sentence') ?>/">
<?php $this->load->view('includes/footer'); ?>