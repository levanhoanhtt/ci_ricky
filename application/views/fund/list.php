<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('fund/add'); ?>" class="btn btn-default">Thêm quỹ</a></li>
                </ul>
            </section>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Mã quỹ</th>
                                <th>Tên quỹ</th>
                                <th>Thủ quỹ</th>
                                <th>Cơ sở</th>
                                <th>Số tiền</th>
                                <th>Trạng thái</th>                                
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyFund">
                            <?php $status = $this->Mconstants->itemStatus;
                            $labelCss = $this->Mconstants->labelCss;
                            foreach($listFunds as $p){
                                $storeIds = $this->Mstorefunds->getListFieldValue(array('FundId' => $p['FundId']), 'StoreId'); ?>
                                <tr id="fund_<?php echo $p['FundId']; ?>">
                                    <td><a href="<?php echo base_url('fund/edit/'.$p['FundId']); ?>"><?php echo $p['FundCode']; ?></a></td>
                                    <td><?php echo $p['FundName']; ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listUsers, 'UserId', $p['TreasureId'], 'FullName'); ?></td>
                                    <td>
                                        <?php foreach ($storeIds as $storeId) echo '<p>'.$this->Mconstants->getObjectValue($listStores, 'StoreId', $storeId, 'StoreName').'</p>'; ?>
                                    </td>
                                    <td><?php echo priceFormat($p['Balance']); ?></td>
                                    <td id="statusName_<?php echo $p['FundId']; ?>"><span class="<?php echo $labelCss[$p['ItemStatusId']]; ?>"><?php echo $status[$p['ItemStatusId']]; ?></span></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $p['FundId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <div class="btn-group" id="btnGroup_<?php echo $p['FundId']; ?>">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                            <ul class="dropdown-menu">
                                                <?php foreach($status as $j => $v){ ?>
                                                    <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $p['FundId']; ?>" data-status="<?php echo $j; ?>"><?php echo $v; ?></a></li>
                                                <?php }  ?>
                                            </ul>
                                        </div>
                                        <input type="text" hidden="hidden" id="statusId_<?php echo $p['FundId']; ?>" value="<?php echo $p['ItemStatusId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('fund/changeStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>