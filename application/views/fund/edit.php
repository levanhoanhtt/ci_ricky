<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <?php if($fundId > 0){ ?><li><button class="btn btn-primary submit">Cập nhật</button></li><?php } ?>
                    <li><a href="<?php echo base_url('fund'); ?>" id="fundListUrl" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($fundId > 0){ ?>
                <?php echo form_open('fund/update', array('id' => 'fundForm')); ?>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Mã sổ quỹ <span class="required">*</span></label>
                            <input type="text" name="FundCode" class="form-control hmdrequired" value="<?php echo $fund['FundCode']; ?>" data-field="Mã sổ quỹ">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Tên sổ quỹ <span class="required">*</span></label>
                            <input type="text" name="FundName" class="form-control hmdrequired" value="<?php echo $fund['FundName']; ?>" data-field="Tên sổ quỹ">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Tình trạng</label>
                            <?php $this->Mconstants->selectConstants('itemStatus', 'ItemStatusId', $fund['ItemStatusId']); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Thủ quỹ</label>
                            <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'TreasureId', $fund['TreasureId'], true, '--Chọn--', ' select2'); ?>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label class="control-label">Cơ sở áp dụng</label>
                            <?php $this->Mconstants->selectObject($listStores, 'StoreId', 'StoreName', 'StoreIds[]', $listStoreIds, false, '', ' select2', ' multiple'); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group text-right">
                    <input type="text" name="FundId" id="fundId" hidden="hidden" value="<?php echo $fundId; ?>">
                    <input class="btn btn-primary submit" type="submit" name="submit" value="Cập nhật">
                </div>
                <?php echo form_close(); ?>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>