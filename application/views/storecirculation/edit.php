<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <?php if($canEdit){ ?><li><button type="button" class="btn btn-primary submit">Lưu</button></li><?php } ?>
                    <?php if($storeCirculationId > 0){ ?><li><a href="<?php echo base_url('storecirculation/printPdf/'.$storeCirculationId); ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> In</a></li><?php } ?>
                    <li><a href="<?php echo base_url('storecirculation'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content new-box-stl ft-seogeo">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($storeCirculationId > 0){
                    $storeCirculationStatusId = $storeCirculation['StoreCirculationStatusId'];
                    if($storeCirculationStatusId == 4){ ?>
                    <div class="alert alert-danger" style="margin-left: -15px;margin-bottom: 15px;">
                        <p><i class="fa fa-exclamation"></i> Lưu chuyển kho đã bị hủy lúc <?php echo ddMMyyyy($storeCirculation['UpdateDateTime'], 'H:i d/m/Y'); ?>.</p>
                    </div>
                <?php } ?>
                <?php echo form_open('api/storecirculation/update', array('id' => 'storeCirculationForm')); ?>
                <div class="row">
                    <div class="col-sm-8 no-padding">
                        <div class="box box-default padding20">
                            <div class="">
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered">
                                        <thead class="theadNormal">
                                        <tr>
                                            <th>Sản phẩm</th>
                                            <th class="text-center" style="width: 60px;">Đơn vị</th>
                                            <th class="text-center" style="width: 130px;">SKU</th>
                                            <th class="text-center" style="width: 120px;">Bảo hành</th>
                                            <th class="text-center" style="width: 150px;">Giá</th>
                                            <th style="width: 85px;">Số lượng</th>
                                            <th class="text-right" style="width: 100px;">Thành tiền</th>
                                            <th style=" <?php if(!$canEdit) echo 'display: none;'; else echo 'width: 30px;'; ?>"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyProduct">
                                        <?php $products = array();
                                        $productChilds = array();
                                        $totalWeight = 0;
                                        foreach($listStoreCirculationProducts as $op){
                                            if(!isset($products[$op['ProductId']])) $products[$op['ProductId']] = $this->Mproducts->get($op['ProductId'], true, '', 'ProductName, ProductImage, ProductKindId, Price, BarCode, Weight, ProductUnitId, GuaranteeMonth');
                                            $productName = $products[$op['ProductId']]['ProductName'];
                                            $productImage = $products[$op['ProductId']]['ProductImage'];
                                            $price = $products[$op['ProductId']]['Price'];
                                            $barCode = $products[$op['ProductId']]['BarCode'];
                                            $weight = $products[$op['ProductId']]['Weight'];
                                            $productUnitId = $products[$op['ProductId']]['ProductUnitId'];
                                            $guaranteeMonth = $products[$op['ProductId']]['GuaranteeMonth'];
                                            $productChildName = '';
                                            if($op['ProductChildId'] > 0){
                                                if(!isset($productChilds[$op['ProductChildId']])) $productChilds[$op['ProductChildId']] = $this->Mproductchilds->get($op['ProductChildId'], true, '', 'ProductName, ProductImage, Price, BarCode, Weight, GuaranteeMonth');
                                                $productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
                                                $productImage = $productChilds[$op['ProductChildId']]['ProductImage'];
                                                $price = $productChilds[$op['ProductChildId']]['Price'];
                                                $barCode = $productChilds[$op['ProductChildId']]['BarCode'];
                                                $weight = $productChilds[$op['ProductChildId']]['Weight'];
                                                $guaranteeMonth = $productChilds[$op['ProductChildId']]['GuaranteeMonth'];
                                            }
                                            $totalWeight += $weight * $op['Quantity'];
                                            if(empty($productImage)) $productImage = NO_IMAGE; ?>
                                            <tr data-id="<?php echo $op['ProductId']; ?>" data-child="<?php echo $op['ProductChildId']; ?>" data-weight="<?php echo $weight; ?>">
                                                <td>
                                                    <img src="<?php echo PRODUCT_PATH.$productImage; ?>" class="productImg">
                                                    <a href="<?php echo base_url('product/edit/'.$op['ProductId']); ?>" class="light-dark aProductLink" target="_blank">
                                                        <?php echo $productName;
                                                        if(!empty($productChildName)) echo ' ('.$productChildName.')'; ?>
                                                    </a>
                                                </td>
                                                <td class="text-center"><?php echo $this->Mconstants->getObjectValue($listProductUnits, 'ProductUnitId', $productUnitId, 'ProductUnitName'); ?></td>
                                                <td class="text-center"><?php echo $barCode; ?></td>
                                                <td class="text-center"><?php echo $guaranteeMonth; ?> tháng</td>
                                                <td class="tdPrice text-right"><span class="spanPrice"><?php echo priceFormat($price); ?></span> ₫</td>
                                                <td><input class="form-control quantity sll" value="<?php echo priceFormat($op['Quantity']); ?>"></td>
                                                <td><input class="form-control sumPrice text-right" disabled value="<?php echo priceFormat($op['Quantity'] * $price); ?>"></td>
                                                <td class="text-right"<?php if(!$canEdit) echo ' style="display: none;"'; ?>>
                                                    <a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php if($canEdit){ ?>
                                <div class="border-top-title-main">
                                    <div class="clearfix">
                                        <div class="box-search-advance product">
                                            <div>
                                                <input type="text" class="form-control textbox-advancesearch" id="txtSearchProduct" placeholder="Tìm kiếm sản phẩm">
                                            </div>
                                            <div class="panel panel-default" id="panelProduct">
                                                <div class="panel-body" style="width:100%;">
                                                    <div class="list-search-data">
                                                        <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                        <div>
                                                            <div class="form-group pull-right" style="width: 300px;">
                                                                <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', 0, true, 'Nhóm sản phẩm', ' select2'); ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="table-responsive no-padding divTable">
                                                            <table class="table table-hover table-bordered">
                                                                <thead class="theadNormal">
                                                                <tr>
                                                                    <th style="width: 100px;">Ảnh</th>
                                                                    <th>Sản phẩm</th>
                                                                    <th style="width: 100px;">Mã sản phẩm</th>
                                                                    <th style="width: 100px;">Giá bán</th>
                                                                    <th style="width: 100px;">Bảo hành</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="tbodyProductSearch"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="btn-group pull-right">
                                                        <button type="button" class="btn btn-default" id="btnPrevProduct"><i class="fa fa-chevron-left"></i></button>
                                                        <button type="button" class="btn btn-default" id="btnNextProduct"><i class="fa fa-chevron-right"></i></button>
                                                        <input type="text" hidden="hidden" id="pageIdProduct" value="1">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="row" style="margin-top: 20px;">
                                    <div class="col-sm-6">
                                        <label class="light-blue">Ghi chú</label>
                                        <div class="box-transprt clearfix mb10">
                                            <button type="button" class="btn-updaten save" id="btnInsertComment">
                                                Lưu
                                            </button>
                                            <input type="text" class="add-text" id="comment" value="">
                                        </div>
                                        <div id="listComment">
                                            <?php $i = 0;
                                            $now = new DateTime(date('Y-m-d'));
                                            foreach($listStoreCirculationComments as $oc){
                                                $avatar = (empty($oc['Avatar']) ? NO_IMAGE : $oc['Avatar']);
                                                $i++;
                                                if($i < 3){ ?>
                                                    <div class="box-customer mb10">
                                                        <table>
                                                            <tbody>
                                                            <tr>
                                                                <th rowspan="2" valign="top" style="width: 50px;"><img src="<?php echo USER_PATH.$avatar; ?>" alt=""></th>
                                                                <th><a href="javascript:void(0)" class="name"><?php echo $oc['FullName']; ?></a></th>
                                                                <th class="time">
                                                                    <?php $dayDiff = getDayDiff($oc['CrDateTime'], $now);
                                                                    echo getDayDiffText($dayDiff).ddMMyyyy($oc['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <p class="pComment"><?php echo $oc['Comment']; ?></p>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                <?php }
                                            } ?>
                                        </div>
                                        <?php if(count($listStoreCirculationComments) > 2){ ?>
                                            <div class="text-right light-dark">
                                                <a href="javascript:void(0)" id="aShowComment">Xem tất cả &gt;&gt;</a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php if($canActive && $canEdit){ ?>
                                            <div class="form-group text-center" id="divActive">
                                                <br>
                                                <button type="button" class="btn btn-primary" id="btnTransport" <?php if(!$canEdit) echo ' style="display: none;"'; ?>>Duyệt giao hàng</button>
                                                <?php if($storeCirculationStatusId == 1){ ?>
                                                    <button type="button" class="btn btn-danger" id="btnCancelOrder">Hủy đơn hàng</button>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php if(count($listTransports) > 0){ ?>
                                <div class="listTransport">
                                    <?php $transportId = 0;
                                    $storeId = 0;
                                   foreach($listTransports as $t){
                                        $transportStatusId = $t['TransportStatusId']; ?>
                                        <div class="box box-default">
                                            <table class="tb-cancel-cart">
                                                <thead>
                                                <tr>
                                                    <td><span class="light-blue">Thông tin giao hàng</span></td>
                                                    <td class="text-center"></td>
                                                    <td class="text-right"><a href="javascript:void(0)" class="btn-cancle-cart"<?php if(!in_array($transportStatusId, array(1, 2, 9)) || ($transportId > 0 && $transportId != $t['TransportId'])) echo ' style="display: none;"'; ?>>Báo hủy giao hàng</a></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr<?php if($transportStatusId == 5) echo ' style="border: 2px solid red;";'; ?>>
                                                    <td class=""><a href="<?php echo base_url('storecirculationtransport/edit/'.$t['StoreCirculationTransportId']); ?>" target="_blank" class="light-dark bold"><img src="assets/vendor/dist/img/icon09.png"><?php echo $t['TransportCode']; ?></a></td>
                                                    <td class="text-center light-blue">Loại vận chuyển: <?php echo $this->Mconstants->getObjectValue($listTransportTypes, 'TransportTypeId', $t['TransportTypeId'], 'TransportTypeName'); ?></td>
                                                    <td class="text-right light-blue">Mã vận đơn: <a href="javascript:void(0)" class="bold light-dark"><?php echo $t['Tracking']; ?></a></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="row no-margin sent-cart">
                                                <div class="col-sm-4">
                                                    <table class="tb-senw">
                                                        <tr>
                                                            <th>Trạng thái giao hàng</th>
                                                            <td><span class="btn-status" id="spanTransactionStatus"><?php echo $this->Mconstants->transportStatus[$t['TransportStatusId']]; ?></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="box-step has-slider">
                                                        <ul class="clearfix bxslider short">
                                                            <?php foreach($this->Mconstants->transportStatus as $i => $v){ ?>
                                                                <li class="liTransportStatus<?php if($transportStatusId == $i) echo ' active'; ?>" id="liTransportStatus_<?php echo $i; ?>">
                                                                    <a href="javascript:void(0);"><?php echo $v; ?> <div class="icon"><img src="assets/vendor/dist/img//transport/<?php echo $i.'.'; if($transportStatusId == $i) echo 2; else echo 1; ?>.png" alt="<?php echo $v; ?>"></div></a>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                                <?php $this->load->view('includes/action_logs_new'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box box-default classify padding20">
                            <div class="form-group">
                                <label class="control-label normal">Cơ sở xuất hàng</label>
                                <?php $this->Mconstants->selectObject($listSourceStores, 'StoreId', 'StoreName', 'StoreSourceId', $storeCirculation['StoreSourceId'], false, '', ' select2'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label normal">Cơ sở nhập hàng</label>
                                <?php $this->Mconstants->selectObject($listDestinationStores, 'StoreId', 'StoreName', 'StoreDestinationId', $storeCirculation['StoreDestinationId'], false, '', ' select2'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Trạng thái</label>
                                <?php $this->Mconstants->selectConstants('storeCirculationStatus', 'StoreCirculationStatusId', $storeCirculationStatusId, false, '', '', ' disabled="disabled"'); ?>
                            </div>
                        </div>
                        <div class="box box-default more-tabs padding20">
                            <div class="form-group">
                                <label class="control-label">Nhãn (cách nhau bởi dấu phẩy)</label>
                                <input type="text" class="form-control" id="tags">
                            </div>
                            <p class="light-gray">Bạn có thể chọn những tag đã được sử dụng</p>
                            <div class="clearfix">
                                <?php foreach ($listTags as $t) { ?>
                                    <a href="javascript:void(0)" class="ntags"><?php echo $t['TagName']; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <?php if($canEdit){ ?><li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li><?php } ?>
                    <li><a href="<?php echo base_url('storecirculation'); ?>" id="storeCirculationListUrl" class="btn btn-default">Đóng</a></li>
                    <input type="text" hidden="hidden" id="getProductDetailUrl" value="<?php echo base_url('api/product/get'); ?>">
                    <input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getList'); ?>">
                    <input type="text" hidden="hidden" id="productKindIdDifferent" value="3">
                    <!--<input type="text" hidden="hidden" id="activeStoreCirculationUrl" value="<?php //echo base_url('api/storecirculation/active'); ?>">-->
                    <input type="text" hidden="hidden" id="insertStoreCirculationCommentUrl" value="<?php echo base_url('api/storecirculation/insertComment'); ?>">
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('api/storecirculation/changeStatus'); ?>">
                    <input type="text" hidden="hidden" id="canEdit" value="<?php echo $canEdit ? 1 : 0; ?>">
                    <input type="text" hidden="hidden" id="canActive" value="<?php echo $canActive ? 1 : 0; ?>">
                    <input type="text" hidden="hidden" id="storeCirculationId" value="<?php echo $storeCirculationId; ?>">
                    <input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
                    <?php foreach($tagNames as $tagName){ ?>
                        <input type="text" hidden="hidden" class="tagName" value="<?php echo $tagName; ?>">
                    <?php } ?>
                </ul>
                <?php echo form_close(); ?>
                <?php $this->load->view('includes/modal/comment', array('itemName' => 'lưu chuyển kho', 'listItemComments' => $listStoreCirculationComments)); ?>
                <div class="modal fade" id="modalTransport" tabindex="-1" role="dialog" aria-labelledby="modalTransport">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Thông tin giao hàng</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label label-nomal">Giao hàng qua</label>
                                            <?php $this->Mconstants->selectObject($listTransportTypes, 'TransportTypeId', 'TransportTypeName', 'TransportTypeId', 0, true, '--Chọn--'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label label-nomal">Nhà vận chuyển</label>
                                            <?php $this->Mconstants->selectObject($listTransporters, 'TransporterId', 'TransporterName', 'TransporterId', 0, true, '--Chọn--'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label label-nomal">Tổng khối lượng (gam)</label>
                                            <input type="text" class="form-control cost" id="transportWeight" disabled value="<?php echo priceFormat($totalWeight); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label label-nomal">Ghi chú cho BPVC</label>
                                    <textarea class="form-control" rows="2" id="transportComment"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                <button type="button" class="btn btn-primary" id="btnUpdateTransport" >Hoàn thành</button>
                                <input type="text" hidden="hidden" id="transportId" value="<?php echo $transport ? $transport['StoreCirculationTransportId'] : 0; ?>">
                                <input type="text" hidden="hidden" id="transportStatusId" value="<?php echo $transport ? $transport['TransportStatusId'] : 9; ?>">
                                <input type="hidden" id="insertTransportUrl" value="<?php echo base_url('storecirculationtransport/update'); ?>">
                                <input type="text" hidden="hidden" id="changeTransportStatusUrl" value="<?php echo base_url('storecirculationtransport/updateField'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modalCancelTransport" tabindex="-1" role="dialog" aria-labelledby="modalCancelTransport">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Báo hủy giao hàng</h4>
                            </div>
                            <div class="modal-body">
                                <p class="text-center light-blue">Bạn có chắc muốn báo hủy giao hàng hàng ?<br/> Hành động này không thể hoàn tác !</p>
                                <div class="form-group">
                                    <label class="control-label">Lý do hủy:</label>
                                    <?php $this->Mconstants->selectObject($listCancelReasons, 'CancelReasonId', 'CancelReasonName', 'CancelReasonId1'); ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Ghi chú:</label>
                                    <input class="form-control" type="text" id="cancelComment1" value="">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="button" id="btnCancelTransport">Xác nhận</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>