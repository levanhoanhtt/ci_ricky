<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button type="button" class="btn btn-primary submit">Lưu</button></li>
                    <li><a href="<?php echo base_url('storecirculation'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content new-box-stl ft-seogeo">
                <?php echo form_open('api/storecirculation/update', array('id' => 'storeCirculationForm')); ?>
                <div class="row">
                    <div class="col-sm-8 no-padding">
                        <div class="box box-default padding20">
                            <div class="">
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered">
                                        <thead class="theadNormal">
                                        <tr>
                                            <th>Sản phẩm</th>
                                            <th class="text-center" style="width: 60px;">Đơn vị</th>
                                            <th class="text-center" style="width: 130px;">SKU</th>
                                            <th class="text-center" style="width: 120px;">Bảo hành</th>
                                            <th class="text-center" style="width: 150px;">Giá</th>
                                            <th style="width: 85px;">Số lượng</th>
                                            <th class="text-right" style="width: 100px;">Thành tiền</th>
                                            <th style="width: 30px;"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyProduct"></tbody>
                                    </table>
                                </div>
                                <div class="border-top-title-main">
                                    <div class="clearfix">
                                        <div class="box-search-advance product">
                                            <div>
                                                <input type="text" class="form-control textbox-advancesearch" id="txtSearchProduct" placeholder="Tìm kiếm sản phẩm">
                                            </div>
                                            <div class="panel panel-default" id="panelProduct">
                                                <div class="panel-body" style="width:100%;">
                                                    <div class="list-search-data">
                                                        <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                        <div>
                                                            <div class="form-group pull-right" style="width: 300px;">
                                                                <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', 0, true, 'Nhóm sản phẩm', ' select2'); ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="table-responsive no-padding divTable">
                                                            <table class="table table-hover table-bordered">
                                                                <thead class="theadNormal">
                                                                <tr>
                                                                    <th style="width: 100px;">Ảnh</th>
                                                                    <th>Sản phẩm</th>
                                                                    <th style="width: 100px;">Mã sản phẩm</th>
                                                                    <th style="width: 100px;">Giá bán</th>
                                                                    <th style="width: 100px;">Bảo hành</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="tbodyProductSearch"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="btn-group pull-right">
                                                        <button type="button" class="btn btn-default" id="btnPrevProduct"><i class="fa fa-chevron-left"></i></button>
                                                        <button type="button" class="btn btn-default" id="btnNextProduct"><i class="fa fa-chevron-right"></i></button>
                                                        <input type="text" hidden="hidden" id="pageIdProduct" value="1">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 20px;">
                                    <div class="col-sm-6">
                                        <label class="light-blue">Ghi chú</label>
                                        <div class="box-transprt clearfix mb10">
                                            <button type="button" class="btn-updaten save" id="btnInsertComment">
                                                Lưu
                                            </button>
                                            <input type="text" class="add-text" id="comment" value="">
                                        </div>
                                        <div class="listComment" id="listComment"></div>
                                    </div>
                                    <div class="col-sm-6">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box box-default classify padding20">
                            <div class="form-group">
                                <label class="control-label">Cơ sở xuất hàng</label>
                                <?php $this->Mconstants->selectObject($listSourceStores, 'StoreId', 'StoreName', 'StoreSourceId', 0, true, '--Chọn cơ sở--', ' select2'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Cơ sở nhập hàng</label>
                                <?php $this->Mconstants->selectObject($listDestinationStores, 'StoreId', 'StoreName', 'StoreDestinationId', 0, true, '--Chọn cơ sở--', ' select2'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Trạng thái</label>
                                <select class="form-control" name="StoreCirculationStatusId" id="storeCirculationStatusId">
                                    <option value="1">Chờ duyệt</option>
                                </select>
                            </div>
                        </div>
                        <div class="box box-default more-tabs padding20">
                            <div class="form-group">
                                <label class="control-label">Nhãn (cách nhau bởi dấu phẩy)</label>
                                <input type="text" class="form-control" id="tags">
                            </div>
                            <p class="light-gray">Bạn có thể chọn những tag đã được sử dụng</p>
                            <div class="clearfix">
                                <?php foreach ($listTags as $t) { ?>
                                    <a href="javascript:void(0)" class="ntags"><?php echo $t['TagName']; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
                    <li><a href="<?php echo base_url('storecirculation'); ?>" id="storeCirculationListUrl" class="btn btn-default">Đóng</a></li>
                    <input type="text" hidden="hidden" id="getProductDetailUrl" value="<?php echo base_url('api/product/get'); ?>">
                    <input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getList'); ?>">
                    <input type="text" hidden="hidden" id="productKindIdDifferent" value="3">
                    <input type="text" hidden="hidden" id="storeCirculationId" value="0">
                    <input type="text" hidden="hidden" id="canEdit" value="<?php echo $canEdit ? 1 : 0; ?>">
                    <input type="text" hidden="hidden" id="canActive" value="0">
                    <input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
                </ul>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>