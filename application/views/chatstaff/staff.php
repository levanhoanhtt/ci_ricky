<?php $this->load->view('chatstaff/header'); ?>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                           <div class="checkbox" style="top:4px">
                                <label style="font-size: 1.2em">
                                    <input type="checkbox" id="cbOnline" checked>
                                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                    Online
                                </label>
                            </div>
                        </li>
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="assets/vendor/dist/img/logo.png" class="user-image" alt="User Image">
                                <span class="hidden-xs" id="spanCustomerName"><?php echo $user['FullName']; ?></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <div class="content-wrapper" id="chatToStaffPage">
        <div class="container-fluid">
            <section class="content">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="box box-primary">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs" id="ulStaffRole">
                                    <li class="active text-center"><a href="#tabCare" data-toggle="tab" data-id="1">Tư vấn - CSKH</a></li>
                                    <li class="text-center"><a href="#tabTech" data-toggle="tab" data-id="2">Kỹ thuật</a></li>
                                </ul>
                                <div class="tab-content">
                                    <?php $customerAvatar = USER_PATH.'customer.png'; ?>
                                    <div class="tab-pane active" id="tabCare">
                                        <div class="box-body box-comments">
                                            <?php foreach($listChatCares as $cs){ ?>
                                                <div class="box-comment" data-id="<?php echo $cs['CustomerId']; ?>" data-name="<?php echo $cs['CustomerName']; ?>" data-phone="<?php echo $cs['PhoneNumber']; ?>">
                                                    <img class="img-circle img-sm" src="<?php echo $customerAvatar; ?>" alt="<?php echo $cs['CustomerName']; ?>">
                                                    <div class="comment-text">
                                                        <span class="username">
                                                            <?php echo $cs['CustomerName']. ' ('.$cs['PhoneNumber'].')'; ?><span class="offline"></span>
                                                            <span class="text-muted pull-right" data-count="0"></span>
                                                        </span>
                                                        <span class="lastMsgChat"><?php echo strip_tags($cs['Message']); ?></span>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tabTech">
                                        <div class="box-body box-comments">
                                            <?php foreach($listChatTechs as $cs){ ?>
                                                <div class="box-comment" data-id="<?php echo $cs['CustomerId']; ?>" data-name="<?php echo $cs['CustomerName']; ?>" data-phone="<?php echo $cs['PhoneNumber']; ?>">
                                                    <img class="img-circle img-sm" src="<?php echo $customerAvatar; ?>" alt="<?php echo $cs['CustomerName']; ?>">
                                                    <div class="comment-text">
                                                        <span class="username">
                                                            <?php echo $cs['CustomerName']. ' ('.$cs['PhoneNumber'].')'; ?><span class="offline"></span>
                                                            <span class="text-muted pull-right" data-count="0"></span>
                                                        </span>
                                                        <span class="lastMsgChat"><?php echo strip_tags($cs['Message']); ?></span>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="box box-primary direct-chat direct-chat-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title" id="h3StaffChat">Chat với khách hàng</h3>
                            </div>
                            <div class="box-body" style="display: block;">
                                <div class="direct-chat-messages" id="listChat"></div>
                                <div class="direct-chat-contacts">
                                    <input type="text" hidden="hidden" id="imagePath" value="<?php echo PRODUCT_PATH; ?>">
                                    <input type="text" hidden="hidden" id="getChatUrl" value="<?php echo base_url('chatstaff/getList'); ?>">
                                    <input type="text" hidden="hidden" id="updateCountChatUnReadUrl" value="<?php echo base_url('chatstaff/updateCountChatUnRead'); ?>">
                                    <input type="text" hidden="hidden" id="startChatPagging" value="0">
                                    <input type="text" hidden="hidden" id="totalChatMsg" value="0">
                                    <input type="text" hidden="hidden" id="staffRoleId" value="1">
                                    <input type="text" hidden="hidden" id="customerId" value="0">
                                    <input type="text" hidden="hidden" id="customerName" value="">
                                    <input type="text" hidden="hidden" id="customerPhone" value="">
                                </div>
                            </div>
                            <div class="box-footer" style="display: block;">
                                <form action="javascript:void(0)" method="post" id="chatForm">
                                    <div class="input-group">
                                        <input placeholder="Tin nhắn..." class="form-control" id="chatMsg">
                                    </div>
                                    <!--<label class="lbChatEnter"><input type="checkbox" id="cbChatEnter" checked> Enter để gửi</label>
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-primary btn-flat">Gửi</button>
                                        <button id="btnChatSendFile" type="button" class="btn btn-default btn-flat"><i class="fa fa-paperclip"></i></button>
                                    </span>-->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>