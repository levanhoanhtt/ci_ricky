<?php 


require_once APPPATH.'third_party/mpdf/vendor/autoload.php';

$mpdf = new \Mpdf\Mpdf(['orientation' => 'L','format'=> [22,105]]);
$mpdf->AddPageByArray([
    'margin-left' => 0,
    'margin-right' => 0,
    'margin-top' => 0,
    'margin-bottom' => 0,
]);
    $listBarcode = array_chunk($listBarcode,3);
    
?>

<?php foreach ($listBarcode as $k => $l){ ?>
<?php
ob_start();
?>
<style>
*{padding: 0;margin: 0}
    h1{font-size: 9px;padding: 0;margin: 0; height: 25px; position: relative; text-align: center;}
    h1 span{
        position: absolute; 
        bottom: 0;
    }
    .item{
        width: 33.33%;
        float: left;
        text-align: center;
    }
    img{width: 90%; height: 30px}
</style>
<body>
    <?php foreach ($l as $n): ?>
        
    <div class="item">
        <h1 style="margin-right: 10px;"><span><br><?php echo $n['name'] ?></span></h1>
        <img style="margin-right: 10px" src="<?php echo $n['link'] ?>">
        <h1 style="margin-right: 10px;"><?php echo $n['barcode'] ?></h1>
    </div>
    <?php endforeach ?>
    
</body>


<?php
$content = ob_get_clean();
// echo $content;
$mpdf->WriteHTML($content);
if($k < (count($listBarcode) - 1 ))
    $mpdf->addPage();
?>

<?php } ?>


<?php



$mpdf->Output();
 ?>