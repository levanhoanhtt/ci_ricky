<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit" type="button">Lưu</button></li>
                    <li><a href="<?php echo base_url('bank'); ?>" id="aBankList" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php echo  form_open('bank/update', array('id' => 'bankForm')); ?>
                    <div class="flexbox-annotated-section">
                        <div class="flexbox-annotated-section-annotation">
                            <div class="annotated-section-title pd-all-20"><h2>Thông tin chung</h2></div>
                            <div class="annotated-section-description pd-all-20 p-none-t">
                                <p class="color-note tabcustomer in privateCus">Một số thông tin cơ bản của ngân hàng.</p>
                            </div>
                        </div>
                        <div class="flexbox-annotated-section-content">
                            <div class="box box-default padding15">
                                <div class="box-header with-border">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="radio-group">
                                                <span class="item"><input type="radio" name="BankTypeId" class="iCheck iCheckBankType" value="1" checked> Cá nhân</span>
                                                <span class="item"><input type="radio" name="BankTypeId" class="iCheck iCheckBankType" value="2"> Công ty</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Tên ngân hàng <span class="required">*</span></label>
                                                <input type="text" class="form-control hmdrequired" name="BankName" id="bankName" data-field="Tên ngân hàng">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Tên viết tắt <span class="required">*</span></label>
                                                <input type="text" class="form-control hmdrequired" name="BankCode" id="bankCode" data-field="Tên viết tắt">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Chủ tài khoản <span class="required">*</span></label>
                                                <input type="text" class="form-control hmdrequired" name="BankHolder" id="bankHolder" value="" data-field="Chủ tài khoản">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Số tài khoản <span class="required">*</span></label>
                                                <input type="text" class="form-control hmdrequired" name="BankNumber" id="bankNumber" value="" data-field="Số tài khoản">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="bankType2" style="display: none;">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Mã số thuế</label>
                                                <input type="text" id="taxCode" name="TaxCode" class="form-control" value="">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Địa chỉ trụ sở công ty</label>
                                                <input type="text" id="address" class="form-control" value="" name="Address">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label">Chi nhánh <span class="required">*</span></label>
                                                <input type="text" class="form-control hmdrequired" name="BranchName" id="branchName" value="" data-field="Chi nhánh">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Trạng thái</label>
                                                <?php $this->Mconstants->selectConstants('status', 'StatusId'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Thứ tự</label>
                                                <?php $this->Mconstants->selectNumber(0, 100, 'DisplayOrder', 1, true); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-inline pull-right margin-right-10 form-group">
                        <li><button class="btn btn-primary submit" type="button">Lưu</button></li>
                        <li><a href="<?php echo base_url('bank'); ?>" class="btn btn-default">Đóng</a></li>
                        <input type="hidden" class="form-control" id="bankId" name="BankId" value="0">
                    </ul>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
	
<?php $this->load->view('includes/footer'); ?>