<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl">
                    <li><button type="button" id="btnGetPage" class="btn btn-primary">Get Page</button></li>
                </ul>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="box box-default box-solid">
                            <div class="box-header with-border text-center">
                                <h3 class="box-title">Chưa kích hoạt</h3>
                            </div>
                            <div class="box-body bodyPageNotActive">
                                <!-- <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="assets/dist/img/user1-128x128.jpg" alt="User Image">
                                        <span class="username"><a href="#">Jonathan Burke Jr.</a></span>
                                        <span class="description">Shared publicly - 7:30 PM Today</span>
                                    </div>
                                    <div class="box-tools">
                                        <button type="button" class="btn btn-primary btn-sm">Kích hoạt</button>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box box-success box-solid">
                            <div class="box-header with-border text-center">
                                <h3 class="box-title">Đã kích hoạt</h3>
                            </div>
                            <div class="box-body bodyPageActive">
                                <!-- <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="assets/dist/img/user1-128x128.jpg" alt="User Image">
                                        <span class="username"><a href="#">Jonathan Burke Jr.</a></span>
                                        <span class="description">Shared publicly - 7:30 PM Today</span>
                                    </div>
                                    <div class="box-tools">
                                        <button type="button" class="btn btn-danger btn-sm">Hủy kích hoạt</button>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <!--<fb:login-button
                    scope="public_profile,email"
                    onlogin="checkLoginState();">
                </fb:login-button>
                <script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>-->
            </section>
            <input type="text" hidden="hidden" id="urlSaveDataPages" value="<?php echo base_url('fbpage/save'); ?>">
            <input type="text" hidden="hidden" id="urlLoadDataPages" value="<?php echo base_url('fbpage/loadFbPage'); ?>">
            <input type="text" hidden="hidden" id="urlChangeStatusPage" value="<?php echo base_url('fbpage/changeStatus'); ?>">
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>