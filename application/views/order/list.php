<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl">
                    <?php if($canExportOrder){ ?><li><a href="javascript:void(0);" class="btn btn-primary btn-export-excel"><i class="fa fa-file-excel-o"></i> Xuất Excel</a></li><?php } ?>
                    <li><a href="<?php echo base_url('order/addPos/'.$customerKindId); ?>" class="btn btn-primary">Tạo đơn (POS)</a></li>
                    <li><a href="<?php echo base_url('order/add/'.$customerKindId); ?>" class="btn btn-primary">Tạo đơn ship</a></li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active liFilter" id="liFilter_0" data-id="0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả</a></li>
                        <?php foreach($listFilters as $f){ ?>
                            <li class="liFilter" id="liFilter_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['DisplayOrder'] ?>"><a  href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php $orderStatus = $this->Mconstants->orderStatus; ?>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả đơn hàng theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="order_status">Trạng thái đơn hàng</option>
                                        <option value="order_status_payment">Trạng thái thanh toán</option>
                                        <option value="order_status_transport">Trạng thái giao hàng</option>
                                        <option value="order_create">Thời điểm đặt hàng</option>
                                        <option value="order_chanels">Kênh bán hàng</option>
                                        <option value="order_type">Loại đơn hàng</option>
                                        <!--<option value="customer_kind">Loại khách hàng</option>-->
                                        <option value="transport_day_process">Thời gian xử lý</option>
                                        <option value="order_status_cod">Trạng thái COD</option>
                                        <option value="order_transporter">Nhà vận chuyển</option>
                                        <option value="order_status_authen">Trạng thái xác thực</option>
                                        <option value="order_method_payment">Phương thức thanh toán</option>
                                        <option value="order_method_transaction">Phương thức vận chuyển</option>
                                        <option value="order_store">Cơ sở xử lý</option>
                                        <option value="order_cost">Tổng tiền đơn hàng</option>
                                        <option value="order_user_create">Người tạo</option>
                                        <option value="order_tag">Tag</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 order_cost transport_day_process none-display">
                                    <select class="value_operator form-control">
                                        <option value="=">bằng</option>
                                        <option value="!=">khác</option>
                                        <option value="<">nhỏ hơn</option>
                                        <option value=">">lớn hơn</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 order_status order_status_payment order_status_transport order_status_cod order_transporter order_transporter order_chanels order_status_authen order_method_payment order_method_transaction order_store order_user_create order_type">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <select class="form-control order_status block-display">
                                        <?php foreach($orderStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_status_payment none-display">
                                        <?php foreach($this->Mconstants->paymentStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_status_transport none-display">
                                        <?php foreach($this->Mconstants->transportStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_chanels none-display">
                                        <?php foreach($this->Mconstants->orderChannels as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_type none-display">
                                        <?php foreach($listOrderTypes as $ot) :?>
                                            <option value="<?php echo $ot['OrderTypeId']; ?>"><?php echo $ot['OrderTypeName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <!--<select class="form-control customer_kind none-display">
                                        <option value="1">Khách lẻ</option>
                                        <option value="2">Khách buôn</option>
                                        <option value="3">CTV</option>
                                    </select>-->
                                    <select class="form-control order_create none-display" id="select_operator_date">
                                        <option value="between">trong khoảng</option>
                                        <option value="<">trước</option>
                                        <option value="=">bằng</option>
                                        <option value=">">sau</option>
                                    </select>
                                    <select class="form-control order_tag none-display mb10">
                                        <option value="in">chứa</option>
                                        <option value="not in">không chứa</option>
                                    </select>
                                    <select class="form-control order_status_cod none-display">
                                        <?php foreach($this->Mconstants->CODStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_transporter none-display">
                                        <?php foreach($listTransporters as $t) :?>
                                            <option value="<?php echo $t['TransporterId']; ?>"><?php echo $t['TransporterName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <select class="form-control order_status_authen none-display">
                                        <?php foreach($this->Mconstants->verifyStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_method_payment none-display">
                                        <?php foreach($this->Mconstants->paymentStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_method_transaction none-display">
                                        <?php foreach($listTransportTypes as $tt) :?>
                                            <option value="<?php echo $tt['TransportTypeId']; ?>"><?php echo $tt['TransportTypeName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <select class="form-control order_store none-display">
                                        <?php foreach($listStores as $s) :?>
                                            <option value="<?php echo $s['StoreId']; ?>"><?php echo $s['StoreName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <select class="form-control order_user_create none-display">
                                        <?php foreach($listUsers as $u) :?>
                                            <option value="<?php echo $u['UserId']; ?>"><?php echo $u['FullName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <div class="order_tag none-display">
                                        <select class="form-control select2 order_tag ">
                                            <?php foreach($listTags as $key => $v){ ?>
                                                <option value="<?php echo $v['TagId']; ?>"><?php echo $v['TagName']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group block-display mb10">
                                    <input class="form-control order_cost transport_day_process input-number none-display" type="text">
                                    <input class="form-control none-display" type="text">
                                    <input class="form-control datepicker order_create none-display" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart">
                                    <input class="form-control datepicker order_create none-display" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd">
                                </div>
                                <div class="form-group block-display widthauto">
                                    <!-- data-href : Đây là link gọi để filter mỗi trang sẽ có 1 link khác nhau -->
                                    <button id="btn-filter" data-href="<?php echo base_url('api/order/searchByFilter/'.$customerKindId); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-sm-3 fix-width-200">
                                <select class="form-control" id="selectAction" style="display: none;">
                                    <option value="">Chọn hành động</option>
                                    <option value="print_order">In phiếu bán hàng</option>
                                    <option value="delete_item">Xóa đơn hàng đã chọn</option>
                                    <option value="verify_order-2">Xác thực đơn hàng</option>
                                    <option value="verify_order-1">Bỏ xác thực đơn hàng</option>
                                    <option value="">----------</option>
                                    <option value="add_tags">Thêm nhãn</option>
                                    <option value="delete_tags">Bỏ nhãn</option>
                                </select>
                            </div>
                            <div class="col-sm-2 fix-width-200">
                                <select class="form-control" id="selectData" style="display: none;"></select>
                            </div>
                        </div>
                    </div>
                    <style>
                        #tbodyOrder i{
                            cursor: pointer;
                            margin-left: 3px;
                        }
                        #tbodyOrder .light-blue, #tbodyOrder .mh-wrap-customer span, #tbodyOrder .aCheckOrder, #tbodyOrder .box-customer table .pComment{
                            font-size: 13px;
                        }
                        #tbodyOrder .box-customer table th.time{
                            font-size: 12px;
                        }
                    </style>
                    <div class="box-body table-responsive divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th style="padding-left: 22px;"><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Mã đơn</th>
                                <th>Ngày tạo đơn</th>
                                <th>Khách hàng</th>
                                <?php if($customerKindId == 3) echo '<th>CTV</th>'; ?>
                                <th class="text-center">TT đơn hàng</th>
                                <th class="text-center">TT giao hàng</th>
                                <th class="text-center">TT Thanh toán</th>
                                <th class="text-right">Tổng tiền</th>
                                <?php if($customerKindId != 3) echo '<th class="text-right">Kênh</th>'; ?>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrder"></tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="itemTypeId" value="6">
                    <input type="text" hidden="hidden" id="customerKindId" value="<?php echo $customerKindId; ?>">
                    <input type="text" hidden="hidden" id="isSearchCustomerPhone" value="0">
                    <input type="hidden" value="<?php echo base_url('customer/edit')?>" id="urlEditCustomer">
                    <input type="hidden" value="<?php echo base_url('order/edit')?>" id="urlEditOrder">
                    <input type="hidden" value="<?php echo base_url('api/order/getDetail')?>" id="getDetailUrl">
                    <input type="text" hidden="hidden" id="checkOrderUrl" value="<?php echo base_url('api/order/checkQuantity'); ?>">
                    <input type="text" hidden="hidden" id="changeVerifyStatusBatchUrl" value="<?php echo base_url('api/order/changeVerifyStatusBatch'); ?>">
                    <input type="text" hidden="hidden" id="printPdfUrl" value="<?php echo base_url('order/printPdf'); ?>">
                    <input type="text" hidden="hidden" id="insertOrderCommentUrl" value="<?php echo base_url('api/order/insertComment'); ?>">
                    <input type="text" hidden="hidden" id="productImagePath" value="<?php echo PRODUCT_PATH; ?>">
                    <input type="text" hidden="hidden" id="urlTransportEdit" value="<?php echo base_url('transport/edit/'); ?>">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                    <form action="<?php echo base_url('order/printPdfMultiple'); ?>" method="POST" target="_blank" id="printForm" style="display: none;">
                        <input type="hidden" name="OrderIds" value="" id="orderIds"/>
                        <input type="submit" value="Submit">
                    </form>
                    <div id="divOrderJson" style="display: none;"></div>
                    <div class="modal fade" id="modalCheckQuantity" tabindex="-1" role="dialog" aria-labelledby="modalCheckQuantity">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Tình trạng sản phẩm trong kho</h4>
                                </div>
                                <div class="modal-body"></div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalItemComment" tabindex="-1" role="dialog" aria-labelledby="modalItemComment">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-comments-o"></i> Ghi chú</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="listCommentAll"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($canExportOrder){ ?>
                    <div class="modal fade" id="modalExportExcel" tabindex="-1" role="dialog" aria-labelledby="modalExportExcel">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-file-excel-o"></i> Xuất Excel</h4>
                                </div>
                                <?php echo form_open('order/exportOrder', array('id' => 'orderFormExport')); ?>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class='input-group' >
                                                    <input type='text' class="form-control daterangepicker" name="DateRangePicker" id='dateRangePicker' value="" autocomplete="off" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    <button type="submit" class="btn btn-primary submit" target="_blank">Xuất</button>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>