<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('order/shipCost'); ?>
                        <div class="row">
                            <div class="col-sm-4">
                        		<div class="form-group">
                                    <div class='input-group' >
                                        <input type='text' class="form-control daterangepicker" name="DateRangePicker" value="<?php echo set_value('DateRangePicker');?>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content upn ft-seogeo">
                 <div class="">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Đơn hàng</th>
                                <th>Phí vận chuyển</th>                                
                                <th>Phí ship thực tế</th>
                                <th>Chênh lệch</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrderStatistic">
                            <?php
                            foreach($listOrderStatistics as $os){ ?>
                                <tr>
                                    <td><a href="<?php echo base_url('order/edit/'.$os['OrderId']); ?>"><?php echo $os['OrderCode']; ?></a></td>
                                    <td><?php echo priceFormat($os['TransportCost']); ?></td>
                                    <td><?php echo priceFormat($os['ShipCost']); ?></td>
                                    <td><?php echo priceFormat($os['ShipCost'] - $os['TransportCost']); ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(isset($paggingHtml)){ ?>
                            <tr>
                                <td colspan="4"><?php echo $paggingHtml; ?></td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>