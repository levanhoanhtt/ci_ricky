<?php $this->load->view('includes/header');  ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit" type="button" data-id="1">Lưu</button></li>
                    <li><a href="<?php echo base_url('order/offset'); ?> " id="orderOffsetListUrl" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content new-box-stl ft-seogeo">
                <?php echo form_open('api/order/updateOffset', array('id' => 'orderFormOffset')); ?>
                <div class="row">
                    <div class="col-sm-8 no-padding">
                        <div class="box box-default padding20">
                            <div class="row clearfix top-aded">
                                <div class="col-sm-12 form-group">
                                    <div class="code">
                                       <label class="control-label">Mã đơn hàng gặp vấn đề</label> <a href="javascript:void(0)" id="aParentOrder" ><i class="fa fa-plus" aria-hidden="true"></i> <span id="orderCode"></span></a>
                                    </div>
                                    <!-- <div class="row col-sm-12">
                                        <label class="control-label">Loại gửi bù</label>
                                        <?php $this->Mconstants->selectObject($listOffsetTypes, 'OffsetTypeId', 'OffsetTypeName', 'OffsetTypeId', 0, true, '-- Loại gửi bù--', ' select2 hmdrequiredNumber', ' data-field="Loại gửi bù"'); ?>
                                    </div> -->
                                </div>
                                <div class="col-sm-12 form-group">
                                    <label class="control-label">Mô tả vấn đề chi tiết</label>
                                    <textarea rows="3" class="form-control" id="commentOffset" placeholder="Mô tả vấn đề chi tiết"></textarea>
                                </div>
                            </div>
                            <div class="">
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered-bottom">
                                        <thead class="theadNormal">
                                        <tr>
                                            <th>Sản phẩm</th>
                                            <th class="text-center" style="width: 60px;">Đơn vị</th>
                                            <th class="text-center" style="width: 130px;">SKU</th>
                                            <th class="text-center" style="width: 120px;">Bảo hành</th>
                                            <th class="text-center" style="width: 150px;">Giá</th>
                                            <th style="width: 85px;">Số lượng</th>
                                            <th class="text-right" style="width: 110px;">Thành tiền</th>
                                            <th style="width: 30px;"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyProduct"></tbody>
                                    </table>
                                </div>
                                <div class="border-top-title-main">
                                    <div class="clearfix">
                                        <div class="box-search-advance product">
                                            <div>
                                                <input type="text" class="form-control textbox-advancesearch updated" id="txtSearchProduct" placeholder="Tìm kiếm sản phẩm (F3)">
                                            </div>
                                            <div class="panel panel-default" id="panelProduct">
                                                <div class="panel-body" style="width:100%;">
                                                    <div class="list-search-data">
                                                        <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                        <div>
                                                            <div class="form-group pull-right" style="width: 300px;">
                                                                <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', 0, true, 'Nhóm sản phẩm', ' select2'); ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="table-responsive no-padding divTable">
                                                            <table class="table table-hover table-bordered">
                                                                <thead class="theadNormal">
                                                                <tr>
                                                                    <th style="width: 100px;">Ảnh</th>
                                                                    <th>Sản phẩm</th>
                                                                    <th style="width: 100px;">SKU</th>
                                                                    <th style="width: 100px;">Giá</th>
                                                                    <th style="width: 100px;">Bảo hành</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="tbodyProductSearch"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="btn-group pull-right">
                                                        <button type="button" class="btn btn-default" id="btnPrevProduct"><i class="fa fa-chevron-left"></i></button>
                                                        <button type="button" class="btn btn-default" id="btnNextProduct"><i class="fa fa-chevron-right"></i></button>
                                                        <input type="text" hidden="hidden" id="pageIdProduct" value="1">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="light-blue">Ghi chú</label>
                                        <div class="box-transprt clearfix mb10">
                                            <button type="button" class="btn-updaten save" id="btnInsertComment">
                                                Lưu
                                            </button>
                                            <input type="text" class="add-text" id="comment" value="">
                                        </div>
                                        <div class="listComment" id="listComment"></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row tb-sead">
                                            <div class="col-sm-6"><a href="javascript:void(0)" id="aTotalPrice">Tổng giá trị sản phẩm</a></div>
                                            <div class="col-sm-6 text-right"><span id="totalPrice">0</span> đ</div>
                                            <div class="clearfix"></div>
                                            <div class="col-sm-6">
                                                <a href="javascript:void(0)">Phí thu khách phát sinh</a>
                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="paymentCost" value="0">
                                                    <span class="input-group-addon"> đ</span>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="hr-ths">
                                        <div class="row">
                                            <div class="col-sm-6">Tổng cần thanh toán (không bao gồm tiền hàng)</div>
                                            <div class="col-sm-6 text-right bold light-dark2 fs-16">
                                                <span id="totalCost">0</span> đ
                                            </div>
                                        </div>
                                        <p class="text-right list-btnn">
                                            <a href="javascript:void(0)" class="btn btn-default check-order" id="aCheckOrder"><span>Check đơn</span></a>
                                            <!--<button class="btn btn-primary giao btnTransport" type="button"><span>Giao hàng</span></button>-->
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box box-default" id="boxChooseCustomer">
                            <div class="box-header with-border">
                                <h3 class="box-title">Khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnAddCustomer"><i class="fa fa-plus"></i> Thêm</button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="box-search-advance customer">
                                    <div>
                                        <input type="text" class="form-control textbox-advancesearch" id="txtSearchCustomer" placeholder="Tìm khách hàng (F4)">
                                    </div>
                                    <div class="panel panel-default" id="panelCustomer">
                                        <div class="panel-body">
                                            <div class="list-search-data">
                                                <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                <ul id="ulListCustomers"></ul>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default" id="btnPrevCustomer"><i class="fa fa-chevron-left"></i></button>
                                                <button type="button" class="btn btn-default" id="btnNextCustomer"><i class="fa fa-chevron-right"></i></button>
                                                <input type="text" hidden="hidden" id="pageIdCustomer" value="1">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default mh-wrap-customer" id="divCustomer" style="display: none;">
                            <div class="with-border">
                                <h3 class="box-title">Thông tin khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnCloseBoxCustomer"><i class="fa fa-times" style="font-size: 18px;color:#777"></i></button>
                                </div>
                                <div class="mh-info-customer">
                                    <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                                    <div class="name-info">
                                        <h4 class="i-name"></h4>
                                        <div class="phones i-phone"></div>
                                    </div>
                                    <div class="i-cusType">
                                        <span class="label label-success"></span>
                                    </div>
                                    <div class="total-orders">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>Số đơn hàng &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<span class="i-total-orders"></span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-money" aria-hidden="true"></i><span>Số dư tài khoản :&nbsp;&nbsp;<span id="customerBalance">0</span> ₫</span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div>
                                    <h4 class="mgbt-20 light-blue">Thông tin giao hàng &nbsp;&nbsp;<a href="javascript:void(0)" id="aCustomerAddress"><i class="fa fa-pencil"></i></a></h4>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="item">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            <span class="i-name"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span class="i-phone"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span class="i-email"></span>
                                        </div>
                                        <div class="item i-address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <span class="i-ward"></span>
                                            <span class="br-line i-district"></span>
                                            <span class="br-line i-province"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-id-card" aria-hidden="true"></i>
                                            <span class="i-country" data-id="232" data-province="0" data-district="0" data-ward="0" data-zip="">Việt Nam</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default classify padding20">
                            <div class="form-group">
                                <label class="control-label light-blue">Lý do gửi bù</label>
                                <?php $this->Mconstants->selectObject($listOffsetReasons, 'OffsetReasonId', 'OffsetReasonName', 'OffsetReasonId', 0, true, '--Lý do gửi bù--', ' hmdrequiredNumber', ' data-field="Lý do gửi bù"'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label light-blue">Lỗi do ai</label>
                                <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'ErrorByUserId', 0, true, '--Lỗi do ai--', ' hmdrequiredNumber', ' data-field="Lỗi do ai"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <li><button class="btn btn-primary submit" type="button" data-id="1">Lưu</button></li>
                    <li><a href="<?php echo base_url('order/offset'); ?> " class="btn btn-default">Đóng</a></li>
                    <input type="text" hidden="hidden" id="orderEditOffsetUrl" value="<?php echo base_url('order/editOffset'); ?>">
                    <input type="text" hidden="hidden" id="getProductDetailUrl" value="<?php echo base_url('api/product/get'); ?>">
                    <input type="text" hidden="hidden" id="getProductChildComboUrl" value="<?php echo base_url('api/product/getProductChildCombo'); ?>">
                    <input type="text" hidden="hidden" id="getCustomerDetailUrl" value="<?php echo base_url('api/customer/get'); ?>">
                    <input type="text" hidden="hidden" id="getListCustomerUrl" value="<?php echo base_url('api/customer/getList'); ?>">
                    <input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getList'); ?>">
                    <input type="text" hidden="hidden" id="checkOrderUrl" value="<?php echo base_url('api/order/checkQuantity'); ?>">
                    <input type="text" hidden="hidden" id="editCustomerUrl" value="<?php echo base_url('customer/edit'); ?>">
                    <input type="text" hidden="hidden" id="searchOrderUrl" value="<?php echo base_url('api/order/search'); ?>">
                    <input type="text" hidden="hidden" id="orderId" value="0">
                    <input type="text" hidden="hidden" id="customerId" value="<?php echo $customerId; ?>">
                    <input type="text" hidden="hidden" id="orderStatusId" value="1">
                    <input type="text" hidden="hidden" id="pendingStatusId" value="0">
                    <input type="text" hidden="hidden" id="paymentStatusId" value="0">
                    <input type="text" hidden="hidden" id="verifyStatusId" value="1">
                    <input type="text" hidden="hidden" id="parentOrderId" value="0">
                    <input type="text" hidden="hidden" id="orderStoreId" value="<?php echo count($listStores) == 1 ? $listStores[0]['StoreId'] : 0; ?>">
                    <input type="text" hidden="hidden" id="deliveryTypeId" value="2">
                    <input type="text" hidden="hidden" id="offsetOrderTypeId" value="<?php echo OFFSET_ORDER_TYPE_ID; ?>">
                    <input type="text" hidden="hidden" id="debitOtherTypeId" value="<?php echo DEBIT_OTHER_TYPE_ID; ?>">
                    <input type="text" hidden="hidden" id="canEdit" value="1">
                    <input type="text" hidden="hidden" id="transportId" value="0">
                    <input type="text" hidden="hidden" id="customerAddressId" value="0">
                    <input type="text" hidden="hidden" id="customerGroupId1" value="0">
                    <input type="text" hidden="hidden" id="customerKindId1" value="<?php echo $customerKindId; ?>">
                    <input type="text" hidden="hidden" id="debtCost1" value="0">
                    <input type="text" hidden="hidden" id="remindOwnCost" value="0">
                    <input type="text" hidden="hidden" id="bankIdTmp" value="0">
                    <input type="text" hidden="hidden" id="moneySourceId" value="0">
                    <input type="text" hidden="hidden" id="realPaymentCost" value="0">
                    <input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
                </ul>
                <?php echo form_close(); ?>
                <?php $this->load->view('includes/modal/add_customer', array('listCountries' => $listCountries, 'listProvinces' => $listProvinces, 'listDistricts' => $listDistricts)); ?>
                <?php $this->load->view('includes/modal/customer_address', array('listCountries' => $listCountries, 'listProvinces' => $listProvinces, 'listDistricts' => $listDistricts)); ?>
                <?php $this->load->view('order/modal'); ?>
                <div class="modal fade" id="modalChooseOrder" tabindex="-1" role="dialog" aria-labelledby="modalChooseOrder">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <style>#tbodyParentOrder tr{cursor: pointer;}</style>
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="box-title">Danh sách đơn hàng</h4>
                            </div>
                            <div class="box-body table-responsive divTable no-padding">
                                <table class="table new-style table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Mã đơn</th>
                                        <th>Ngày tạo đơn</th>
                                        <th class="text-right">Tổng tiền</th>
                                        <th class="text-center">TT đơn hàng</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbodyParentOrder"></tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>