<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả</a></li>
                    </ul>
                </div>
                <?php $orderStatus = $this->Mconstants->orderStatus; ?>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập số điện thoại khách hàng" />
                    <button id="btn-filter" data-href="<?php echo base_url('api/order/searchByFilter/0/1'); ?>" style="display: none;" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-header">
                        <select class="form-control input-sm select-action" id="selectAction" style="display: none;">
                            <option value="">Chọn hành động</option>
                        </select>
                    </div>
                    <style>
                        #tbodyOrder i{
                            cursor: pointer;
                            margin-left: 3px;
                        }
                    </style>
                    <div class="box-body table-responsive divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th style="padding-left: 22px;"><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Mã đơn</th>
                                <th>Ngày tạo đơn</th>
                                <th>Khách hàng</th>
                                <th class="text-center">TT đơn hàng</th>
                                <th class="text-center">TT giao hàng</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrder"></tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="changeVerifyStatusBatchUrl" value="<?php echo base_url('api/order/changeVerifyStatusBatch'); ?>">
                    <input type="text" hidden="hidden" id="itemTypeId" value="6">
                    <input type="text" hidden="hidden" id="customerKindId" value="0">
                    <input type="text" hidden="hidden" id="isSearchCustomerPhone" value="1">
                    <input type="hidden" value="<?php echo base_url('customer/edit')?>" id="urlEditCustomer">
                    <input type="hidden" value="<?php echo base_url('order/viewByTech')?>" id="urlEditOrder">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>