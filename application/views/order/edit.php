<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php if($orderId > 0){
                $orderStatusId = $order['OrderStatusId'];
                $isPOS = $order['DeliveryTypeId'] == 1; ?>
            <section class="content-header">
                <h1  class="ttl-list-order ft-seogeo"><span class="title-order"></span><a href=" <?php echo base_url('order/'.$customerKindId); ?>">Đơn hàng <?php echo $this->Mconstants->customerKinds[$customerKindId]; ?></a> / <?php echo $order['OrderCode']; ?></h1>
                <ul class="list-inline">
                    <?php if($canEdit){ ?><li><button class="btn btn-primary submit" data-id="<?php echo $orderStatusId; ?>">Lưu</button></li><?php } ?>
                    <li><a href="<?php echo base_url('order'); ?>" class="btn btn-default">Đóng</a></li>
                    <li><a href="<?php echo base_url('order/printPdf/'.$orderId); ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> In</a></li>
                </ul>
            </section>
            <section class="content new-box-stl ft-seogeo">
                <style>
                    #aVerifyOrder.verify i{color: #000;}
                </style>
                <?php echo form_open('api/order/update', array('id' => 'orderForm')); ?>
                <div class="row">
                    <div class="col-sm-8 no-padding">
                        <div class="box box-default padding20">
                            <div class="alert alert-danger" id="divCancelOrder"<?php if($orderStatusId != 3) echo ' style="display: none;"'; ?>>
                                <p><i class="fa fa-exclamation"></i> Đơn hàng đã bị hủy lúc <?php echo ddMMyyyy($order['UpdateDateTime'], 'H:i d/m/Y'); ?> bởi <?php echo $this->Musers->getFieldValue(array('UserId' => $order['UpdateUserId']), 'FullName'); ?></p>
                            </div>
                            <div class="clearfix top-aded">
                                <div class="left">
                                    <div class="code light-dark">
                                        <?php $attr = '';
                                        if($order['VerifyStatusId'] == 2){
                                            $attr = ' class="verify" title="Đã được xác thực';
                                            if($order['VerifyUserId'] > 0) $attr .= ' bởi '.$this->Musers->getFieldValue(array('UserId' => $order['VerifyUserId']), 'FullName').' lúc '.ddMMyyyy($order['VerifyDateTime'], 'H:i d/m/Y').'"';
                                            else $attr .= '"';
                                        } ?>
                                        <a href="javascript:void(0)" id="aVerifyOrder"<?php if($order['VerifyStatusId'] == 2) echo $attr; ?>><i class="fa fa-check-circle" aria-hidden="true"></i> <?php echo $order['OrderCode']; ?></a>
                                        <?php if($order['CTVId'] > 0) echo '<a href="javascript:void(0)"><span class="label label-default">CTV</span></a>'; ?>
                                    </div>
                                    <div class="status">
                                        <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                        <span id="spanOrderStatus">
                                            <?php echo $this->Mconstants->orderStatus[$orderStatusId];
                                            if($orderStatusId == 1 && $order['PendingStatusId'] > 0) echo ' ('.$this->Mconstants->getObjectValue($listPendingStatus, 'PendingStatusId', $order['PendingStatusId'], 'PendingStatusName').')'; ?>
                                        </span>
                                    </div>
                                </div>
                                <div class="right" style="margin-right: 0;">
                                    <span class="dropdown">
                                        <span class="dropdown-toggle" style="cursor: pointer;" type="button" data-toggle="dropdown">
                                            <?php $storeId = $transport ? $transport['StoreId'] : $order['StoreId'];  ?>
                                            <span class="light-blue pos-re" id="spanStoreName">CS: <?php echo $storeId > 0 ? $this->Mconstants->getObjectValue($listStores, 'StoreId', $storeId, 'StoreName') : '' ?></span>
                                            <?php if($orderStatusId == 1){ ?><i class="fa fa-align-justify" aria-hidden="true"></i><?php } ?>
                                        </span>
                                        <?php if($orderStatusId == 1){ ?>
                                        <ul class="dropdown-menu">
                                            <li><a href="javascript:void(0)" id="aUpdatePending"><img src="assets/vendor/dist/img/pop01.png">&nbsp;&nbsp;Cập nhật chờ xử lý</a></li>
                                            <li><a href="javascript:void(0)" id="aCreateOrderChild"><img src="assets/vendor/dist/img/pop02.png">&nbsp;&nbsp;Tạo đơn hàng con</a></li>
                                            <li><a href="javascript:void(0)" id="aChooseStore"><img src="assets/vendor/dist/img/pop03.png">&nbsp;&nbsp;Chọn cơ sở xử lý</a></li>
                                            <li><a href="javascript:void(0)" id="aCancelOrder"><img src="assets/vendor/dist/img/pop04.png">&nbsp;&nbsp;Hủy đơn hàng</a></li>
                                        </ul>
                                        <?php } ?>
                                    </span>
                                </div>
                            </div>
                            <div class="">
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered">
                                        <thead class="theadNormal">
                                        <tr>
                                            <th>Sản phẩm</th>
                                            <th class="text-center" style="width: 60px;">Đơn vị</th>
                                            <th class="text-center" style="width: 130px;">SKU</th>
                                            <th class="text-center" style="width: 120px;">Bảo hành</th>
                                            <th class="text-center" style="width: 150px;">Giá</th>
                                            <th style="width: 85px;">Số lượng</th>
                                            <th class="text-right" style="width: 110px;">Thành tiền</th>
                                            <th style="<?php if(!$canEdit) echo 'display: none;'; else echo 'width: 30px;'; ?>"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyProduct">
                                        <?php $products = array();
                                        $productChilds = array();
                                        $productPrices = array();
                                        $totalWeight = 0;
                                        foreach($listOrderProducts as $op){
                                            if(!isset($products[$op['ProductId']])) $products[$op['ProductId']] = $this->Mproducts->get($op['ProductId'], true, '', 'ProductName, ProductImage, ProductKindId, Price, BarCode, Weight, ProductUnitId, GuaranteeMonth');
                                            if(!isset($productPrices[$op['ProductId']])) $productPrices[$op['ProductId']] = $this->Mproductprices->getByProductId($op['ProductId']);
                                            $productName = $products[$op['ProductId']]['ProductName'];
                                            $productImage = $products[$op['ProductId']]['ProductImage'];
                                            $productKindId = $products[$op['ProductId']]['ProductKindId'];
                                            $price = $products[$op['ProductId']]['Price'];
                                            $barCode = $products[$op['ProductId']]['BarCode'];
                                            $weight = $products[$op['ProductId']]['Weight'];
                                            $productUnitId = $products[$op['ProductId']]['ProductUnitId'];
                                            $guaranteeMonth = $products[$op['ProductId']]['GuaranteeMonth'];
                                            $productChildName = '';
                                            if($op['ProductChildId'] > 0){
                                                if(!isset($productChilds[$op['ProductChildId']])) $productChilds[$op['ProductChildId']] = $this->Mproductchilds->get($op['ProductChildId'], true, '', 'ProductName, ProductImage, Price, BarCode, Weight, GuaranteeMonth');
                                                //$productName .= '<br/>(' . $productChilds[$op['ProductChildId']]['ProductName'] .')';
                                                $productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
                                                $productImage = $productChilds[$op['ProductChildId']]['ProductImage'];
                                                $price = $productChilds[$op['ProductChildId']]['Price'];
                                                $barCode = $productChilds[$op['ProductChildId']]['BarCode'];
                                                $weight = $productChilds[$op['ProductChildId']]['Weight'];
                                                $guaranteeMonth = $productChilds[$op['ProductChildId']]['GuaranteeMonth'];
                                            }
                                            $totalWeight += $weight * $op['Quantity'];
                                            if(empty($productImage)) $productImage = NO_IMAGE; ?>
                                            <tr data-id="<?php echo $op['ProductId']; ?>" data-child="<?php echo $op['ProductChildId']; ?>" data-weight="<?php echo $weight ?>" data-kind="<?php echo $productKindId; ?>">
                                                <td>
                                                    <img src="<?php echo PRODUCT_PATH.$productImage; ?>" class="productImg">
                                                    <a href="<?php echo $productKindId == 3 ? 'javascript:void(0)' : base_url('product/edit/'.$op['ProductId']); ?>" class="light-dark aProductLink" target="_blank">
                                                        <?php echo $productName;
                                                        if(!empty($productChildName)) echo ' ('.$productChildName.')';
                                                        elseif($productKindId == 3) echo ' (Combo)'; ?>
                                                    </a>
                                                </td>
                                                <td class="text-center"><?php echo $this->Mconstants->getObjectValue($listProductUnits, 'ProductUnitId', $productUnitId, 'ProductUnitName'); ?></td>
                                                <td class="text-center"><?php echo $barCode; ?></td>
                                                <td class="text-center"><?php echo $guaranteeMonth; ?> tháng</td>
                                                <td class="tdPrice text-right"><span class="spanPrice"><?php echo priceFormat($op['Price']); ?></span> ₫</td>
                                                <td><input class="form-control quantity sll" value="<?php echo priceFormat($op['Quantity']); ?>"></td>
                                                <td><input class="form-control sumPrice text-right" disabled value="<?php echo priceFormat($op['Quantity'] * $op['Price']); ?>"></td>
                                                <td class="text-right"<?php if(!$canEdit) echo ' style="display: none;"'; ?>>
                                                    <a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a>
                                                    <span class="productPrices" style="display: none;"><?php echo json_encode($productPrices[$op['ProductId']]); ?></span>
                                                    <span class="originalPrice" style="display: none;"><?php echo $price; ?></span>
                                                    <span class="priceCapital" style="display: none;"><?php echo $op['PriceCapital']; ?></span>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php if($canEdit){ ?>
                                <div class="border-top-title-main">
                                    <div class="clearfix">
                                        <div class="box-search-advance product">
                                            <div>
                                                <input type="text" class="form-control textbox-advancesearch" id="txtSearchProduct" placeholder="Tìm kiếm sản phẩm (F3)">
                                            </div>
                                            <div class="panel panel-default" id="panelProduct">
                                                <div class="panel-body" style="width:100%;">
                                                    <div class="list-search-data">
                                                        <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                        <div>
                                                            <div class="form-group pull-right" style="width: 300px;">
                                                                <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', 0, true, 'Nhóm sản phẩm', ' select2'); ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="table-responsive no-padding divTable">
                                                            <table class="table table-hover table-bordered">
                                                                <thead class="theadNormal">
                                                                <tr>
                                                                    <th style="width: 100px;">Ảnh</th>
                                                                    <th>Sản phẩm</th>
                                                                    <th style="width: 100px;">SKU</th>
                                                                    <th style="width: 100px;">Giá</th>
                                                                    <th style="width: 100px;">Bảo hành</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="tbodyProductSearch"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="btn-group pull-right">
                                                        <button type="button" class="btn btn-default" id="btnPrevProduct"><i class="fa fa-chevron-left"></i></button>
                                                        <button type="button" class="btn btn-default" id="btnNextProduct"><i class="fa fa-chevron-right"></i></button>
                                                        <input type="text" hidden="hidden" id="pageIdProduct" value="1">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="row" style="margin-top: 20px;margin-bottom: 20px;">
                                    <div class="col-sm-6">
                                        <label class="light-blue">Ghi chú</label>
                                        <div class="box-transprt clearfix mb10">
                                            <button type="button" class="btn-updaten save" id="btnInsertComment">Lưu</button>
                                            <input type="text" class="add-text" id="comment" value="">
                                        </div>
                                        <div class="listComment" id="listComment">
                                            <?php $i = 0;
                                            $now = new DateTime(date('Y-m-d'));
                                            foreach($listOrderComments1 as $oc){
                                                $avatar = (empty($oc['Avatar']) ? NO_IMAGE : $oc['Avatar']);
                                                $i++;
                                                if($i < 3){ ?>
                                                <div class="box-customer mb10">
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <th rowspan="2" valign="top" style="width: 50px;"><img src="<?php echo USER_PATH.$avatar; ?>" alt=""></th>
                                                            <th><a href="javascript:void(0)" class="name"><?php echo $oc['FullName']; ?></a></th>
                                                            <th class="time">
                                                                <?php $dayDiff = getDayDiff($oc['CrDateTime'], $now);
                                                                echo getDayDiffText($dayDiff).ddMMyyyy($oc['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <p class="pComment"><?php echo $oc['Comment']; ?></p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <?php }
                                            } ?>
                                        </div>
                                        <?php if(count($listOrderComments1) > 2){ ?>
                                        <div class="text-right light-dark">
                                            <a href="javascript:void(0)" class="aShowComment" data-id="1">Xem tất cả &gt;&gt;</a>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $debitCost = 0; ?>
                                        <div class="row tb-sead">
                                            <div class="col-sm-6"><a href="javascript:void(0)" id="aTotalPrice">Tổng</a></div>
                                            <div class="col-sm-6 text-right"><span id="totalPrice">0</span> đ</div>
                                            <div class="col-sm-6">
                                                <a href="javascript:void(0)" id="aPromotion">Khuyến mại</a>
                                                <div class="promotion" style="display: none;" id="boxPromotion">
                                                    <div class="content-promotion">
                                                        <div class="form-group">
                                                            <label>Nhập giá trị giảm giá cho đơn hàng</label>
                                                            <div class="input-group promotion-input">
                                                                <input id="reduceNumber" type="text" class="form-control" value="<?php echo priceFormat($order['Discount']); ?>">
                                                                <span class="input-group-addon active" id="spanPromotionCost">đ</span>
                                                                <span class="input-group-addon" id="spanPromotionPercent">%</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Hoặc sử dụng mã khuyến mại</label>
                                                            <input type="text" class="form-control" id="promotionCode" placeholder="Mã khuyến mại" value="<?php echo $orderPromotion ? $orderPromotion['PromotionCode'] : ''; ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Lý do</label>
                                                            <input type="text" class="form-control" placeholder="Lý do giảm giá cho đơn hàng" id="promotionComment" value="<?php echo $orderPromotion ? $orderPromotion['Comment'] : ''; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="footer-promotion">
                                                        <button type="button" class="btn btn-default">Đóng</button>
                                                        <button type="button" class="btn btn-primary" id="btnApplyPromotion">Áp dụng</button>
                                                        <input type="text" hidden="hidden" id="promotionId" value="<?php echo $orderPromotion ? $orderPromotion['PromotionId'] : 0; ?>">
                                                        <input type="text" hidden="hidden" id="reduceTypeId" value="<?php echo $orderPromotion && $orderPromotion['DiscountPercent'] > 0 ? 2 : 1; ?>">
                                                        <input type="text" hidden="hidden" id="discountPercent" value="<?php echo $orderPromotion ? $orderPromotion['DiscountPercent'] : 0; ?>">
                                                        <input type="text" hidden="hidden" id="checkPromotionUrl" value="<?php echo base_url('promotion/checkPromotion'); ?>">
                                                        <input type="text" hidden="hidden" id="updateOrderPromotionUrl" value="<?php echo base_url('promotion/updateOrderPromotion'); ?>">
                                                    </div>
                                                </div>
                                                <br/><i id="iPromotionText">
                                                    <?php if($orderPromotion){
                                                        if(!empty($orderPromotion['PromotionCode'])) echo '('.$orderPromotion['PromotionCode'].')';
                                                        elseif(!empty($orderPromotion['Comment'])) echo '('.$orderPromotion['Comment'].')';
                                                    } ?>
                                                </i>
                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <span id="promotionCost"><?php echo $order['Discount'] > 0 ? priceFormat($order['Discount']) : '-'; ?></span>
                                                <span id="vnd1"<?php if($order['Discount'] == 0) echo ' style="display: none;"'; ?>>đ</span>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-sm-6"<?php if($isPOS) echo ' style="display: none;"'; ?>>
                                                <a href="javascript:void(0)" id="aTransport">Cấu hình vận chuyển</a>
                                                <div class="transport" style="display: none;" id="boxTransport">
                                                    <div class="content-transport">
                                                        <div class="radio-group">
                                                            <p class="item"><input type="radio" name="TransportType" class="iCheck transportType" value="1"<?php if($order['TransportTypeId'] == 0) echo ' checked'; ?>> Miễn phí vận chuyển</p>
                                                            <p class="item"><input type="radio" name="TransportType" class="iCheck transportType" value="2"<?php if($order['TransportTypeId'] > 0) echo ' checked'; ?>> Vận chuyển tùy chọn</p>
                                                        </div>
                                                        <div class="form-group mb10" id="divFreeShip"<?php if($order['TransportTypeId'] > 0) echo ' style="display: none;"'; ?>>
                                                            <label>Lý do miễn phí ship</label>
                                                            <?php echo $this->Mconstants->selectObject($listTransportReasons, 'TransportReasonId', 'TransportReasonName', 'TransportReasonId', $order['TransportReasonId'], true, "--Chọn--"); ?>
                                                        </div>
                                                        <div id="divTransportCost"<?php if($order['TransportTypeId'] == 0) echo ' style="display: none;"'; ?>>
                                                            <div class="form-group mb10">
                                                                <label>Phương thức vận chuyển</label>
                                                                <?php echo $this->Mconstants->selectObject($listTransportTypes, 'TransportTypeId', 'TransportTypeName', 'TransportTypeId1', $order['TransportTypeId'], true, "--Chọn--"); ?>
                                                            </div>
                                                            <div class="form-group mb10">
                                                                <label>Phí vận chuyển</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control cost" id="transportFee" value="<?php echo priceFormat($order['TransportCost']); ?>">
                                                                    <div class="input-group-addon">đ</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer-transport">
                                                        <button type="button" class="btn btn-default">Đóng</button>
                                                        <button type="button" class="btn btn-primary" id="btnApplyConfigTransport">Áp dụng</button>
                                                    </div>
                                                </div>
                                                <br/><i id="iTransportText">(<?php echo $order['TransportTypeId'] == 0 ? 'Miễn phí vận chuyển' : $this->Mconstants->getObjectValue($listTransportTypes, 'TransportTypeId', $order['TransportTypeId'], 'TransportTypeName');; ?>)</i>
                                            </div>
                                            <div class="col-sm-6 text-right"<?php if($isPOS) echo ' style="display: none;"'; ?>>
                                                <span id="transportCost"><?php echo $order['TransportCost'] > 0 ? priceFormat($order['TransportCost']) : '-'; ?></span>
                                                <span id="vnd2"<?php if($order['TransportCost'] == 0) echo ' style="display: none;"'; ?>>đ</span>
                                            </div>
                                            <div class="divConfigExpand" id="divService">
                                                <?php foreach($listOrderServices as $os){
                                                    if($os['OtherServiceId'] == DEBIT_OTHER_TYPE_ID) $debitCost = $os['ServiceCost'];
                                                    else{ ?>
                                                        <div class="item" data-id="<?php echo $os['OtherServiceId']; ?>">
                                                            <div class="col-sm-6" style="clear: both">
                                                                <a href="javascript:void(0)" class="aExpand"><?php echo $this->Mconstants->getObjectValue($listOtherServices, 'OtherServiceId', $os['OtherServiceId'], 'OtherServiceName'); ?></a>
                                                            </div>
                                                            <div class="col-sm-6 text-right"><span id="spanOtherCost_<?php echo $os['OtherServiceId']; ?>" class="spanOtherCost"><?php echo priceFormat($os['ServiceCost']); ?></span> đ</div>
                                                        </div>
                                                    <?php }
                                                } ?>
                                            </div>
                                        </div>
                                        <hr class="hr-ths">
                                        <div class="row mb10">
                                            <div class="col-sm-6">Tổng tiền</div>
                                            <div class="col-sm-6 text-right"><span id="orderCost"><?php echo priceFormat($order['TotalCost']); ?></span> đ</div>
                                        </div>
                                        <div class="row mb10" id="divPaymentCost"<?php if($isPOS || $order['OrderTypeId'] == OFFSET_ORDER_TYPE_ID) echo ' style="display: none;"'; ?>>
                                            <div class="col-sm-6">
                                                <a href="javascript:void(0)" id="aPayment">Thanh toán trước</a><br/>
                                                <i id="iPaymentText"><?php if($order['PaymentStatusId'] > 0) echo '('.$this->Mconstants->paymentStatus[$order['PaymentStatusId']].')'; ?></i>
                                            </div>
                                            <div class="col-sm-6 text-right"<?php if($isPOS) echo ' style="display: none;"'; ?>>
                                                <?php if($canEdit){ ?>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="paymentCost" value="<?php echo priceFormat($order['PaymentCost']); ?>">
                                                        <span class="input-group-addon"> đ</span>
                                                    </div>
                                                <?php } else{ ?>
                                                    <span><?php echo $order['PaymentCost'] > 0 ? priceFormat($order['PaymentCost']) : '-'; ?></span>
                                                    <span id="vnd3"<?php if($order['PaymentCost'] == 0) echo ' style="display: none;"'; ?>>đ</span>
                                                    <input type="text" hidden="hidden" id="paymentCost" value="<?php echo $order['PaymentCost']; ?>">
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="divConfigExpand mb10" id="divOrderOwn"<?php if($debitCost == 0) echo ' style="display: none;"'; ?>>
                                            <div class="row item" data-id="<?php echo DEBIT_OTHER_TYPE_ID; ?>">
                                                <div class="col-sm-6"><a href="javascript:void(0)" class="aExpand">Ghi nợ</a></div>
                                                <div class="col-sm-6 text-right"><span id="spanOtherCost_<?php echo DEBIT_OTHER_TYPE_ID; ?>" class="spanOtherCost"><?php echo priceFormat($debitCost); ?></span> đ</div>
                                            </div>
                                        </div>
                                        <div class="row tb-sead">
                                            <div class="col-sm-6">Tổng cần thanh toán<?php if(!$isPOS) echo ' (COD)'; ?></div>
                                            <div class="col-sm-6 text-right bold light-dark2 fs-16">
                                                <span id="totalCost"><?php echo $order['OrderTypeId'] == OFFSET_ORDER_TYPE_ID ? priceFormat($order['TotalCost']) : 0; ?></span> đ
                                            </div>
                                            <?php if($canEdit && $isPOS){ ?>
                                                <div class="clearfix"></div>
                                                <div class="col-sm-6">
                                                    <a href="javascript:void(0)" id="aBank" style="line-height: 34px;">Loại tiền</a><br/>
                                                </div>
                                                <div class="col-sm-6 text-right light-dark2 fs-16">
                                                    <select class="form-control" id="moneySourceId">
                                                        <option value="1">Tiền mặt</option>
                                                        <option value="2">Chuyển khoản</option>
                                                        <option value="3">Quẹt thẻ</option>
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-sm-6" style="line-height: 34px;">KH thanh toán</div>
                                                <div class="col-sm-6 text-right">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control cost" id="realPaymentCost" value="0">
                                                        <span class="input-group-addon"> đ</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12" style="margin-top: 10px;">
                                                    <p class="pull-right"><a href="javascript:void(0)" class="aExpand">Mở rộng &gt;&gt;</a></p>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <p class="text-right list-btnn">
                                            <?php if($canEdit || $order['OrderStatusId'] == 5){
                                            if($orderStatusId != 6) echo '<a href="javascript:void(0)" class="btn btn-default check-order" id="aCheckOrder"><span>Check đơn</span></a>';
                                            if(!$isPOS){
                                                $flag = !$transport;
                                                if(!$flag) $flag = $transport['TransportStatusId'] == 5;
                                                $class = ' actived';
                                                if(!empty($listOrderProducts)) $class = ''; ?>
                                                <button class="btn btn-primary giao btnTransport<?php echo $class; ?>" type="button"<?php if(!$flag) echo ' style="display: none;"'; ?>><span>Giao hàng</span></button>
                                            <?php } else{
                                                if($orderStatusId == 1) echo '<button class="btn btn-primary" type="button" id="btnVerifyComplete" style="width: 49%;height: 30px;line-height: 17px;">Chốt đơn (F9)</button>'; ?>
                                            <?php }
                                            } if($isPOS){ ?>
                                                <a href="javascript:void(0)" id="aNoneCOD" class="btn btn-default" style="width: 100%;background-color: #798c9c;color: #fff;<?php if($orderStatusId != 6) echo 'display: none;'; ?>"><span>ĐÃ HOÀN THÀNH - POS</span></a>
                                            <?php } ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="listTransport">
                                    <?php $transportId = 0;
                                    $storeId = 0;
                                    if($transport){
                                        $transportId = $transport['TransportId'];
                                        $storeId = $transport['StoreId'];
                                    }
                                    echo '<input type="text" hidden="hidden" id="transportStoreId" value="'.$storeId.'">';
                                    foreach($listTransports as $t){
                                        $transportStatusId = $t['TransportStatusId']; ?>
                                        <div class="box box-default">
                                            <table class="tb-cancel-cart">
                                                <thead>
                                                <tr>
                                                    <td><span class="light-blue">Thông tin giao hàng</span></td>
                                                    <td class="text-center"></td>
                                                    <td class="text-right"><a href="javascript:void(0)" class="btn-cancle-cart"<?php if(!in_array($transportStatusId, array(1, 2, 9)) || ($transportId > 0 && $transportId != $t['TransportId'])) echo ' style="display: none;"'; ?>>Báo hủy giao hàng</a></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr<?php if($transportStatusId == 5) echo ' style="border: 2px solid red;";'; ?>>
                                                    <td class=""><a href="<?php echo base_url('transport/edit/'.$t['TransportId']); ?>" target="_blank" class="light-dark bold"><img src="assets/vendor/dist/img/icon09.png"><?php echo $t['TransportCode']; ?></a></td>
                                                    <td class="text-center light-blue">Loại vận chuyển: <?php echo $this->Mconstants->getObjectValue($listTransportTypes, 'TransportTypeId', $t['TransportTypeId'], 'TransportTypeName'); ?></td>
                                                    <td class="text-right light-blue">Mã vận đơn: <a href="javascript:void(0)" class="bold light-dark"><?php echo $t['Tracking']; ?></a></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="row no-margin sent-cart">
                                                <div class="col-sm-4">
                                                    <table class="tb-senw">
                                                        <tr>
                                                            <th>Trạng thái giao hàng</th>
                                                            <td><span class="btn-status <?php echo $this->Mtransports->labelCss['TransportStatusCss'][$t['TransportStatusId']]; ?>"><?php echo $this->Mconstants->transportStatus[$t['TransportStatusId']]; ?></span></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Trạng thái thu hộ (COD)</th>
                                                            <td><?php if($t['CODStatusId'] > 0){ ?><span class="btn-status <?php echo $this->Mtransports->labelCss['CODStatusCss'][$t['CODStatusId']]; ?>"><?php echo $this->Mconstants->CODStatus[$t['CODStatusId']]; ?></span><?php } ?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="box-step has-slider">
                                                        <ul class="clearfix bxslider short">
                                                            <?php $transportStatusIds = isset($transportStatusLogIds[$t['TransportId']]) ? $transportStatusLogIds[$t['TransportId']] : array(1, $transportStatusId);
                                                            foreach($this->Mconstants->transportStatus as $i => $v){ ?>
                                                                <li class="<?php if(in_array($i, $transportStatusIds)) echo 'block-display'; else echo 'none-display'; ?> liTransportStatus<?php if($transportStatusId == $i) echo ' active'; ?>" id="liTransportStatus_<?php echo $i; ?>"<?php //if(!in_array($i, $transportStatusIds)) echo ' style="display: none;"'; ?>>
                                                                    <a href="javascript:void(0);"><?php echo $v; ?> <div class="icon"><img src="assets/vendor/dist/img/transport/<?php echo $i.'.'; if($transportStatusId == $i) echo 2; else echo 1; ?>.png" alt="<?php echo $v; ?>"></div></a>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php $this->load->view('includes/action_logs_new'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box box-default" id="boxChooseCustomer" style="display: none;">
                            <div class="box-header with-border">
                                <h3 class="box-title">Khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnAddCustomer"><i class="fa fa-plus"></i> Thêm</button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="box-search-advance customer">
                                    <div>
                                        <input type="text" class="form-control textbox-advancesearch" id="txtSearchCustomer" placeholder="Tìm khách hàng (F4)">
                                    </div>
                                    <div class="panel panel-default" id="panelCustomer">
                                        <div class="panel-body">
                                            <div class="list-search-data">
                                                <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                <ul id="ulListCustomers"></ul>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default" id="btnPrevCustomer"><i class="fa fa-chevron-left"></i></button>
                                                <button type="button" class="btn btn-default" id="btnNextCustomer"><i class="fa fa-chevron-right"></i></button>
                                                <input type="text" hidden="hidden" id="pageIdCustomer" value="1">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default mh-wrap-customer" id="divCustomer">
                            <div class="with-border">
                                <h3 class="box-title">Thông tin khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnCloseBoxCustomer"><i
                                            class="fa fa-times" style="font-size: 18px;color:#777"></i>
                                    </button>
                                </div>
                                <div class="mh-info-customer">
                                    <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                                    <div class="name-info">
                                        <h4 class="i-name"></h4>
                                        <div class="phones i-phone"></div>
                                    </div>
                                    <div class="i-cusType">
                                        <span class="label label-success"></span>
                                    </div>
                                    <div class="total-orders">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>Số đơn hàng &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<span class="i-total-orders"></span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-money" aria-hidden="true"></i><span>Số dư tài khoản :&nbsp;&nbsp;<span id="customerBalance">0</span> ₫</span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div>
                                    <h4 class="mgbt-20 light-blue">Thông tin giao hàng &nbsp;&nbsp;<a href="javascript:void(0)" id="aCustomerAddress"><i class="fa fa-pencil"></i></a></h4>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?php if($customerAddress){ ?>
                                            <div class="item">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                <span class="i-name"><?php echo $customerAddress['CustomerName']; ?></span>
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-phone" aria-hidden="true"></i>
                                                <span class="i-phone"><?php echo $customerAddress['PhoneNumber']; ?></span>
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                                <span class="i-email"><?php echo $customerAddress['Email']; ?></span>
                                            </div>
                                            <div class="item i-address">
                                                <?php $zipCode = $customerAddress['ZipCode'];
                                                if(empty($zipCode) || $zipCode == '0'){ ?>
                                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    <span class="i-ward"><spam class="spanAddress"><?php echo $customerAddress['Address'] . '</span> ' . $this->Mwards->getWardName($customerAddress['WardId']); ?></span>
                                                    <span class="br-line i-district"><?php echo $this->Mconstants->getObjectValue($listDistricts, 'DistrictId', $customerAddress['DistrictId'], 'DistrictName'); ?></span>
                                                    <span class="br-line i-province"><?php echo $this->Mconstants->getObjectValue($listProvinces, 'ProvinceId', $customerAddress['ProvinceId'], 'ProvinceName'); ?></span>
                                                <?php } else{ ?>
                                                    <i class="fa fa-list-alt" aria-hidden="true"></i>
                                                    <span class="i-province">ZipCode: <?php echo $zipCode; ?></span>
                                                <?php } ?>
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-id-card" aria-hidden="true"></i>
                                                <span class="i-country" data-id="<?php echo $customerAddress['CountryId']; ?>" data-province="<?php echo $customerAddress['ProvinceId']; ?>" data-district="<?php echo $customerAddress['DistrictId']; ?>" data-ward="<?php echo $customerAddress['WardId']; ?>" data-zip="<?php echo $zipCode; ?>">
                                                    <?php $countryName = $this->Mconstants->getObjectValue($listCountries, 'CountryId', $customerAddress['CountryId'], 'CountryName');
                                                    if(empty($countryName)) $countryName = 'Việt Nam';
                                                    echo $countryName; ?>
                                                </span>
                                            </div>
                                        <?php } else{ ?>
                                            <div class="item">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                <span class="i-name"></span>
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-phone" aria-hidden="true"></i>
                                                <span class="i-phone"></span>
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                                <span class="i-email"></span>
                                            </div>
                                            <div class="item i-address">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                <span class="i-ward"></span>
                                                <span class="br-line i-district"></span>
                                                <span class="br-line i-province"></span>
                                            </div>
                                            <div class="item">
                                                <i class="fa fa-id-card" aria-hidden="true"></i>
                                                <span class="i-country" data-id="232" data-province="0" data-district="0" data-ward="0" data-zip="">Việt Nam</span>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default classify padding20">
                            <div class="form-group">
                                <label class="control-label">Loại đơn hàng</label>
                                <?php $this->Mconstants->selectObject($listOrderTypes, 'OrderTypeId', 'OrderTypeName', 'OrderTypeId', $order['OrderTypeId'], true, '--Loại đơn hàng--'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Lý do mua hàng</label>
                                <?php $this->Mconstants->selectObject($listOrderReasons, 'OrderReasonId', 'OrderReasonName', 'OrderReasonId', $order['OrderReasonId'], true, '--Chọn lý do--'); ?>
                            </div>
                            <div class="form-group"<?php if($order['CTVId'] == 0) echo ' style="display: none;"'; ?>>
                                <label class="control-label light-blue">CTV <span class="required">*</span></label>
                                <?php $this->Mconstants->selectObject($listCTVs, 'CustomerId', 'FullName', 'CTVId', $order['CTVId'], true, '--Chọn CTV--'); ?>
                            </div>
                            <div class="form-group" style="display: none;">
                                <label class="control-label">Kênh bán hàng</label>
                                <?php $this->Mconstants->selectConstants('orderChannels', 'OrderChanelId', $order['OrderChanelId']); ?>
                            </div>
                            <!--<div class="form-group">
                                <label class="control-label light-blue">Cách thức giao hàng</label>
                                <div class="radio-group">
                                    <span class="item"><input type="radio" name="DeliveryTypeId" class="iCheck iCheckDeliveryTypeId" value="1"<?php //if($isPOS) echo ' checked'; ?>> POS</span>
                                    <span class="item"><input type="radio" name="DeliveryTypeId" class="iCheck iCheckDeliveryTypeId" value="2"<?php //if(!$isPOS) echo ' checked'; ?>> Từ xa</span>
                                </div>
                            </div>-->
                        </div>
                        <div class="box box-default more-task">
                            <!--<a href="javascript:void(0)" class="task1 aRemind"><img src="assets/vendor/dist/img/icon07.png">Thêm task chăm sóc khách hàng</a>-->
                            <a href="javascript:void(0)" class="aRemind"><img src="assets/vendor/dist/img/icon08.png">Tạo chương trình nhắc nhở</a>
                        </div>
                        <?php if($transport && $transport['TransportStatusId'] > 1){ ?>
                            <div class="box box-default classify padding20">
                                <label class="light-blue">Chăm sóc giao hàng (CSKH + VC)</label>
                                <div class="box-transprt clearfix mb10">
                                    <button type="button" class="btn-updaten save" id="btnInsertComment1">
                                        Lưu
                                    </button>
                                    <input type="text" class="add-text" id="comment1" value="">
                                </div>
                                <div class="listComment" id="listComment1">
                                    <?php $i = 0;
                                    $now = new DateTime(date('Y-m-d'));
                                    foreach($listOrderComments2 as $oc){
                                        $avatar = (empty($oc['Avatar']) ? NO_IMAGE : $oc['Avatar']);
                                        $i++;
                                        if($i < 3){ ?>
                                            <div class="box-customer mb10">
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <th rowspan="2" valign="top" style="width: 50px;"><img src="<?php echo USER_PATH.$avatar; ?>" alt=""></th>
                                                        <th><a href="javascript:void(0)" class="name"><?php echo $oc['FullName']; ?></a></th>
                                                        <th class="time">
                                                            <?php $dayDiff = getDayDiff($oc['CrDateTime'], $now);
                                                            echo getDayDiffText($dayDiff).ddMMyyyy($oc['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <p class="pComment"><?php echo $oc['Comment']; ?></p>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php }
                                    } ?>
                                </div>
                                <?php if(count($listOrderComments2) > 2){ ?>
                                    <div class="text-right light-dark">
                                        <a href="javascript:void(0)" class="aShowComment" data-id="2">Xem tất cả &gt;&gt;</a>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <div class="box box-default more-tabs padding20">
                            <div class="form-group">
                                <label class="control-label" style="width: 100%;line-height: 28px;">Nhãn (cách nhau bởi dấu phẩy) <button class="btn-updaten save btn-sm pull-right" type="button" id="btnUpdateTag">Lưu</button></label>
                                <input type="text" class="form-control" id="tags">
                            </div>
                            <p class="light-gray">Bạn có thể chọn những tag đã được sử dụng</p>
                            <div class="clearfix">
                                <?php foreach($listTags as $t){ ?>
                                    <a href="javascript:void(0)" class="ntags"><?php echo $t['TagName']; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <?php if($canEdit){ ?><li><button class="btn btn-primary submit" data-id="<?php echo $orderStatusId; ?>">Lưu</button></li><?php } ?>
                    <li><a href="<?php echo base_url('order'); ?>" class="btn btn-default">Đóng</a></li>
                    <input type="text" hidden="hidden" id="orderEditUrl" value="<?php echo base_url('order/edit'); ?>">
                    <input type="text" hidden="hidden" id="getProductDetailUrl" value="<?php echo base_url('api/product/get'); ?>">
                    <input type="text" hidden="hidden" id="getProductChildComboUrl" value="<?php echo base_url('api/product/getProductChildCombo'); ?>">
                    <input type="text" hidden="hidden" id="getCustomerDetailUrl" value="<?php echo base_url('api/customer/get'); ?>">
                    <input type="text" hidden="hidden" id="getListCustomerUrl" value="<?php echo base_url('api/customer/getList'); ?>">
                    <input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getList'); ?>">
                    <input type="text" hidden="hidden" id="insertOrderCommentUrl" value="<?php echo base_url('api/order/insertComment'); ?>">
                    <input type="text" hidden="hidden" id="updateVerifyOrderUrl" value="<?php echo base_url('api/order/changeVerifyStatusBatch'); ?>">
                    <input type="text" hidden="hidden" id="updateFieldUrl" value="<?php echo base_url('api/order/updateField'); ?>">
                    <input type="text" hidden="hidden" id="checkOrderUrl" value="<?php echo base_url('api/order/checkQuantity'); ?>">
                    <input type="text" hidden="hidden" id="editCustomerUrl" value="<?php echo base_url('customer/edit'); ?>">
                    <input type="text" hidden="hidden" id="updateItemTagUrl" value="<?php echo base_url('api/tag/updateItem'); ?>">
                    <input type="text" hidden="hidden" id="orderId" value="<?php echo $orderId; ?>">
                    <input type="text" hidden="hidden" id="customerId" value="<?php echo $order['CustomerId']; ?>">
                    <input type="text" hidden="hidden" id="orderStatusId" value="<?php echo $orderStatusId; ?>">
                    <input type="text" hidden="hidden" id="pendingStatusId" value="<?php echo $order['PendingStatusId']; ?>">
                    <input type="text" hidden="hidden" id="paymentStatusId" value="<?php echo $order['PaymentStatusId']; ?>">
                    <input type="text" hidden="hidden" id="verifyStatusId" value="<?php echo $order['VerifyStatusId']; ?>">
                    <input type="text" hidden="hidden" id="parentOrderId" value="<?php echo $order['ParentOrderId']; ?>">
                    <input type="text" hidden="hidden" id="orderStoreId" value="<?php echo $order['StoreId']; ?>">
                    <input type="text" hidden="hidden" id="deliveryTypeId" value="<?php echo $order['DeliveryTypeId']; ?>">
                    <input type="text" hidden="hidden" id="offsetOrderTypeId" value="<?php echo OFFSET_ORDER_TYPE_ID; ?>">
                    <input type="text" hidden="hidden" id="debitOtherTypeId" value="<?php echo DEBIT_OTHER_TYPE_ID; ?>">
                    <input type="text" hidden="hidden" id="canEdit" value="<?php echo $canEdit ? 1 : 0; ?>">
                    <input type="text" hidden="hidden" id="customerAddressId" value="<?php echo $transport ? $transport['CustomerAddressId'] : $order['CustomerAddressId']; ?>">
                    <input type="text" hidden="hidden" id="customerGroupId1" value="0">
                    <input type="text" hidden="hidden" id="customerKindId1" value="<?php echo $customerKindId; ?>">
                    <input type="text" hidden="hidden" id="debtCost1" value="0">
                    <input type="text" hidden="hidden" id="remindOwnCost" value="<?php echo $remindOwnCost; ?>">
                    <input type="text" hidden="hidden" id="bankIdTmp" value="0">
                    <input type="text" hidden="hidden" id="customerConsultId" value="0">
                    <input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
                    <input type="text" hidden="hidden" id="transportStatusIconPath" value="assets/vendor/dist/img/transport/">
                    <?php if(!($canEdit && $isPOS)){ ?>
                        <input type="text" hidden="hidden" id="moneySourceId" value="0">
                        <input type="text" hidden="hidden" id="realPaymentCost" value="0">
                    <?php } ?>
                    <?php foreach($tagNames as $tagName){ ?>
                        <input type="text" hidden="hidden" class="tagName" value="<?php echo $tagName; ?>">
                    <?php } ?>
                </ul>
                <?php echo form_close(); ?>
                <?php $this->load->view('includes/modal/add_customer', array('listCountries' => $listCountries, 'listProvinces' => $listProvinces, 'listDistricts' => $listDistricts)); ?>
                <?php $this->load->view('includes/modal/customer_address', array('listCountries' => $listCountries, 'listProvinces' => $listProvinces, 'listDistricts' => $listDistricts)); ?>
                <?php $this->load->view('order/modal'); ?>
                <?php $this->load->view('includes/modal/remind'); ?>
                <?php if($canEdit && $isPOS) $this->load->view('transaction/modal_bank'); ?>
                <?php $this->load->view('includes/modal/comment_type', array('listItemComments' => $listOrderComments)); ?>
                <div class="modal fade" id="modalTransport" tabindex="-1" role="dialog" aria-labelledby="modalTransport">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Thông tin giao hàng</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label label-nomal">Cơ sở xử lý đơn hàng</label>
                                            <?php $this->Mconstants->selectObject($listStores, 'StoreId', 'StoreName', 'StoreId', $order['StoreId'], false, '', ' select2'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label label-nomal">Giao hàng qua</label>
                                            <?php $this->Mconstants->selectObject($listTransportTypes, 'TransportTypeId', 'TransportTypeName', 'TransportTypeId', 0, true, '--Chọn--'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label label-nomal">Chế độ chăm sóc đặc biệt</label>
                                            <select class="form-control" name="TransportCareTypeId" id="transportCareTypeId">
                                                <option value="0">--Chọn--</option>
                                                <?php foreach($listTransportCareTypes as $ct){ ?>
                                                    <option value="<?php echo $ct['TransportCareTypeId']; ?>" data-id="<?php echo $ct['TransportTypeId']; ?>" style="display: none;"><?php echo $ct['TransportCareTypeName']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label label-nomal">Tổng khối lượng (gam)</label>
                                            <input type="text" class="form-control cost" id="transportWeight" disabled value="<?php echo priceFormat($totalWeight); ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label label-nomal">Tiền sẽ thu khách (COD)</label>
                                            <input type="text" class="form-control cost" id="transportCODCost" disabled value="<?php echo priceFormat($order['TotalCost']); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label label-nomal">Ghi chú cho BPVC</label>
                                    <textarea class="form-control" rows="2" id="transportComment"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="pull-left">
                                    <input type="checkbox" class="iCheck">
                                    <span>Gửi email tự động cho khách hàng</span>
                                </div>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                <button type="button" class="btn btn-primary" id="btnAddTransport">Hoàn thành</button>
                                <input type="text" hidden="hidden" id="updateTransportUrl" value="<?php echo base_url('api/transport/update'); ?>">
                                <input type="text" hidden="hidden" id="transportId" value="<?php echo $transport ? $transport['TransportId'] : 0; ?>">
                                <input type="text" hidden="hidden" id="transportStatusId" value="<?php echo $transport ? $transport['TransportStatusId'] : 9; ?>">
                                <input type="text" hidden="hidden" id="changeTransportStatusUrl" value="<?php echo base_url('api/transport/updateField'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modalCheckOrder" tabindex="-1" role="dialog" aria-labelledby="modalCheckOrder">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Kiểm tra đơn hàng</h4>
                            </div>
                            <div class="modal-body">
                                <h4>Tình trạng tồn kho</h4>
                                <div id="productExits">
                                    <h5 class="title"></h5>
                                    <div class="table-responsive no-padding divTable">
                                        <table class="table table-hover table-bordered">
                                            <thead class="theadNormal">
                                            <tr>
                                                <th style="width: 70%;">Sản phẩm</th>
                                                <th style="width: 30%;">Số lượng hiện còn</th>
                                            </tr>
                                            </thead>
                                            <tbody id="productExitsTbody"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modalCancelOrder" tabindex="-1" role="dialog" aria-labelledby="modalCancelOrder">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Hủy đơn hàng</h4>
                            </div>
                            <div class="modal-body">
                                <p class="text-center light-blue">Bạn nên hủy đơn hàng khi thấy đơn hàng có vấn đề, khi khách hàng thay đỏi quyết định hoặc khi kho hàng hết sản phẩm. Thao tác này không thể phục hồi.</p>
                                <div class="form-group">
                                    <label class="control-label">Lý do hủy:</label>
                                    <?php $this->Mconstants->selectObject($listCancelReasons, 'CancelReasonId', 'CancelReasonName', 'CancelReasonId'); ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Ghi chú <span class="required">*</span></label>
                                    <input class="form-control" type="text" id="cancelComment" value="">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="button" id="btnCancelOrder">Xác nhận</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modalUpdatePending" tabindex="-1" role="dialog" aria-labelledby="modalUpdatePending">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="box-title"><i class="fa fa-wrench"></i> Cập nhật trạng thái chờ xử lý</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="radio-group">
                                        <?php foreach($listPendingStatus as $ps){ ?>
                                            <p class="item"><input type="radio" name="PendingStatusId" class="iCheck" value="<?php echo $ps['PendingStatusId']; ?>"<?php if($order['PendingStatusId'] == $ps['PendingStatusId']) echo ' checked'; ?>> <?php echo $ps['PendingStatusName']; ?></p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="button" id="btnUpdatePending">Cập nhật</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modalVerifyOrder" tabindex="-1" role="dialog" aria-labelledby="modalVerifyOrder">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Xác thực đơn hàng</h4>
                            </div>
                            <div class="modal-body">
                                <p class="text-center light-blue">Bạn có chắc muốn xác thực cho đơn hàng này?<br/> Thao tác này không thể phục hồi.</p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="button" id="btnVerifyOrder">Xác nhận</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modalCancelTransport" tabindex="-1" role="dialog" aria-labelledby="modalCancelTransport">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Báo hủy giao hàng</h4>
                            </div>
                            <div class="modal-body">
                                <p class="text-center light-blue">Bạn có chắc muốn báo hủy giao hàng hàng ?<br/> Hành động này không thể hoàn tác !</p>
                                <div class="form-group">
                                    <label class="control-label">Lý do hủy:</label>
                                    <?php $this->Mconstants->selectObject($listCancelReasons, 'CancelReasonId', 'CancelReasonName', 'CancelReasonId1'); ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Ghi chú <span class="required">*</span></label>
                                    <input class="form-control" type="text" id="cancelComment1" value="">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="button" id="btnCancelTransport">Xác nhận</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php } else{ ?>
                <section class="content"><?php $this->load->view('includes/notice'); ?></section>
            <?php } ?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>