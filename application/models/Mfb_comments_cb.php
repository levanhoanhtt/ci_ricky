<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfb_comments_cb extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "fb_comments_cb";
        $this->_primary_key = "FbCommentId";
    }
}