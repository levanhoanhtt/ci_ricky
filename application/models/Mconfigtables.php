<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mconfigtables extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "configtables";
        $this->_primary_key = "ConfigTableId";
    }

    public function genHtmlThTable($controller, $userId = 0){
        $query = "SELECT * FROM `configtables` LEFT JOIN configtableusers ON configtableusers.ConfigTableId = configtables.ConfigTableId WHERE configtables.TableName = ?";
        $tableTh = $this->getByQuery($query, array($controller));
        $htmlTableTh = '';
        if($tableTh){
            $configTableUserId = 0;
            if($tableTh[0]["ConfigTableUserId"] != NULL AND $tableTh[0]["UserId"] == $userId) $configTableUserId = $tableTh[0]["ConfigTableUserId"];
            if($tableTh[0]["TableUserJson"] != NULL) $json = json_decode($tableTh[0]["TableUserJson"], true);
            else $json = json_decode($tableTh[0]["ConfigTableJson"], true);
            $htmlTableTh = '<tr class="dnd-moved" data-id="'.$tableTh[0]['ConfigTableId'].'" config-table-user-id="'.$configTableUserId.'">';
            for($y = 0; $y < count($json); $y++){
                $value = "column-name='".$json[$y]['ColumnName']."' name-user='".$json[$y]['ColumnNameUser']."' modals-db='".$json[$y]['ModelsDb']."' status='".$json[$y]['Status']."' edit='".$json[$y]['Edit']."' id-edit='".$json[$y]['IdEdit']."' number='".$json[$y]['Number']."' ";
                if($json[$y]['ColumnName'] == 'Check'){
                    $htmlTableTh .= '<th class="th_'.$y.' th_move th_fix_width" '.$value.'><input type="checkbox" class="iCheckTable" class="checkAll"></th>';
                }else{
                    $htmlTableTh .= '<th class="th_'.$y.' th_move" '.$value.' >'.$json[$y]['ColumnNameUser'].'</th>';
                }
                
            }
            $htmlTableTh .= '</tr>';
        }
        return array('htmlTableTh' => $htmlTableTh, 'tableTh' => $json);
        
    }
}