<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mincomingsms extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "incomingsms";
        $this->_primary_key = "IncomingSmsId";
    }

    public function getCount($postData){
        $query = "1=1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM incomingsms WHERE 1=1" . $this->buildQuery($postData).' ORDER BY CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if (isset($postData['SearchText']) && !empty($postData['SearchText'])) $query .= " AND PhoneNumber LIKE '%{$postData['SearchText']}%'";
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND CrDateTime <= '{$postData['EndDate']}'";
        return $query;
    }
}