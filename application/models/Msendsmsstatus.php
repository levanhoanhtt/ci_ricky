<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msendsmsstatus extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "sendsmsstatus";
        $this->_primary_key = "SendSMSStatusId";
    }

    public function send($sMSLogId, $tranId, $userId){
        if($sMSLogId > 0 && $tranId > 0) {
            require_once APPPATH . "/libraries/SpeedSMSAPI.php";
            $speedSMSAPI = new SpeedSMSAPI();
            $sms = $speedSMSAPI->getSMSStatus($tranId);
            if(!empty($sms)){
                $postData = array(
                    'SMSLogId' => $sMSLogId,
                    'TranId' => $tranId,
                    'CrUserId' => $userId,
                    'CrDateTime' => getCurentDateTime(),
                );
                if($sms['status'] == 'success'){
                    $postData['ResponseJson'] = json_encode($sms['data'], true);
                    $postData['StatusName'] = $sms['status'];
                }
                else $postData['ErrorMessage'] = $sms['message'];
                $this->load->model('Msendsmsstatus');
                $flag = $this->Msendsmsstatus->save($postData);
                if($flag > 0) return $postData;
            }
        }
        return false;
    }

}