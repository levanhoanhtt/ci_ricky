<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfb_comments extends MY_Model {

    private $prefix = '';

    function __construct() {
        parent::__construct();
        $this->_primary_key = "FbCommentId";
    }

    public function setPrefix($prefix){
        $this->prefix = $prefix;
        $this->_table_name = "fb_comments_".$prefix;
    }

    public function getPrefix(){
        return $this->prefix;
    }

    public function getFbCommentId($commentId){
        return $this->getFieldValue(array('CommentId' => $commentId), 'FbCommentId', 0);
    }

    public function deleteCmt($commentId,$fbPageCode,$fbCommentId, $type){
        $this->db->trans_begin();
        if($type == "delete"){
            if($fbCommentId == "")$this->db->delete('fb_comments_'.$fbPageCode, array('CommentId' => $commentId));
            else{
                $this->db->delete('fb_comments_'.$fbPageCode, array('FbCommentId' => $fbCommentId));
                $this->db->delete('fb_comments_'.$fbPageCode, array('ParentCommentId' => $fbCommentId));
            }
        }else{
            if($fbCommentId == ""){
                $this->db->where('CommentId', $commentId);
                $this->db->update('fb_comments_'.$fbPageCode, array('CommentStatusId' => 1));
            }
            else{
                $this->db->where('FbCommentId', $fbCommentId);
                $this->db->update('fb_comments_'.$fbPageCode, array('CommentStatusId' => 1));
                // $this->db->where('ParentCommentId', $fbCommentId);
                // $this->db->update('fb_comments_'.$fbPageCode, array('CommentStatusId' => 1));
            }
        }
        
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }
}