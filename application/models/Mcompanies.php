<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcompanies extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "companies";
        $this->_primary_key = "CompanyId";
    }

    public function getCompany($userId, $isOld){
        $query = "SELECT * FROM companies WHERE CrUserId = ? AND IsOld = ?";
        $datas = $this->getByQuery($query, array($userId, $isOld));
        if(!empty($datas)) return $datas[0];
        else return '';
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $postData = array()){
        $queryCount = "select companies.CompanyId AS totalRow from companies {joins} where {wheres}";
        $query = "select {selects} from companies {joins} where {wheres} ORDER BY companies.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'companies.*',
            'users.FullName',
            'companytypes.CompanyTypeName'
        ];
        $joins = [
            'users' => 'inner join users ON users.UserId = companies.CrUserId',
            'companytypes' => 'left join companytypes ON companytypes.CompanyTypeId = companies.CompanyTypeId'
        ];
        $wheres = array();
        $whereSearch= '';
        $dataBind = [];
        if(isset($postData['IsOld'])){
            if($postData['IsOld'] == 1) $wheres = array('companies.IsOld = 1');
            elseif($postData['IsOld'] == 2) $wheres = array('companies.IsOld = 2');
        }
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'companies.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            else{
                $whereSearch = 'companies.CompanyName like ? or companies.CompanyShort like ? or users.FullName like ?';
                for( $i = 0; $i < 3; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                switch ($filed_name) {
                    case 'company_type':
                        $wheres[] = "companies.CompanyTypeId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'company_scale':
                        $wheres[] = "companies.CompanyScaleId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $listCompanies = $this->getByQuery($query, $dataBind);
        for ($i = 0; $i < count($listCompanies); $i++) {
            $listCompanies[$i]['CompanyScale'] = $listCompanies[$i]['CompanyScaleId'] > 0 ? $this->Mconstants->companyScale[$listCompanies[$i]['CompanyScaleId']] : '';
            $dayDiff = getDayDiff($listCompanies[$i]['CrDateTime'], $now);
            $listCompanies[$i]['CrDateTime'] = ddMMyyyy($listCompanies[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $listCompanies[$i]['DayDiff'] = $dayDiff;
        }
        $data = array();
        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $listCompanies;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentCompanies';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }
}