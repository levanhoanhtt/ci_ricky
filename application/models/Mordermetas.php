<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mordermetas extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "ordermetas";
        $this->_primary_key = "OrderMetaId";
    }

    public function mergeToOrder($order){
        $orderMetas = $this->getBy(array('OrderId' => $order['OrderId']), false, '', 'MetaName, MetaValue');
        $retVal = array();
        foreach ($orderMetas as $om) $retVal[$om['MetaName']] = $om['MetaValue'];
        return array_merge($retVal, $order);
    }
}