<?php
/**
 * Created by PhpStorm.
 * User: MAN - DEV
 * Date: 7/15/2018
 * Time: 9:01 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Moffsetreasons extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->_table_name = "offsetreasons";
        $this->_primary_key = "OffsetReasonId";
    }
}