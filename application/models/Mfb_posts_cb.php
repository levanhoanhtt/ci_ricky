<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfb_posts_cb extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "fb_posts_cb";
        $this->_primary_key = "FbPostId";
    }

    public function getListPost($pageId, $viewStatusId, $answerStatusId){
    	$sql = "SELECT c.*, pa.FbPageCode AS FbPageCode, pa.FbPageName AS FbPageName, p.FbPostId AS FbPostId, p.PostContent AS PostContent, p.PostLink AS PostLink FROM fb_posts_cb p LEFT JOIN fb_comments_cb c ON c.FbPostId = p.FbPostId LEFT JOIN fb_pages pa ON pa.FbPageId = p.FbPageId WHERE p.FbPageId = 1 ".$this->buildQuery($pageId, $viewStatusId, $answerStatusId)." AND c.FbCommentId IN (SELECT Max(FbCommentId) AS FbCommentId FROM fb_comments_cb GROUP BY FbPostId ORDER BY CreatedDateTime DESC ) GROUP BY c.FbPostId ORDER BY  p.CreatedDateTime DESC";
    	return $this->getByQuery($sql);
    }

    private function buildQuery($pageId, $viewStatusId, $answerStatusId){
    	$query = '';
        if($pageId > 0 && !empty($viewStatusId) == "" && !empty($answerStatusId) == "") $query.=""; // tất cả
        if($pageId > 0 && $viewStatusId == 2) $query.=" AND c.ViewStatusId = ".$viewStatusId; // chua đọc
        if($pageId > 0 && $viewStatusId == 1 && $answerStatusId == 1) $query.=" AND c.ViewStatusId = {$viewStatusId} AND c.AnswerStatusId = {$answerStatusId}"; // đa đọc đã trả lời
        if($pageId > 0 && $viewStatusId == 1 && $answerStatusId == 2) $query.=" AND c.ViewStatusId = {$viewStatusId} AND c.AnswerStatusId = {$answerStatusId}"; // doc chua tra lời
    	return $query;
    }
}