<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdeliverysmsstatus extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "deliverysmsstatus";
        $this->_primary_key = "DeliverySMSStatusId";
    }

    public function update($postData = array()){
    	$this->db->trans_begin();
    	if (!empty($postData)) $this->db->insert_batch('deliverysmsstatus', $postData);
    	if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return 1;
        }
    }
}