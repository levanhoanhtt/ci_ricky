<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproductprintproducts extends MY_Model{
	function __construct(){
        parent::__construct();
        $this->_table_name = "productprintproducts";
        $this->_primary_key = "ProductPrintProductId";
    }
}