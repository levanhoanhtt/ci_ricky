<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproductunits extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "productunits";
        $this->_primary_key = "ProductUnitId";
    }
}