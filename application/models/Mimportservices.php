<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mimportservices extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "importservices";
        $this->_primary_key = "ImportServiceId";
    }

    public function getServiceCost($importId, $otherServiceId){
        return $this->getFieldValue(array('ImportId' => $importId, 'OtherServiceId' => $otherServiceId), 'ServiceCost', 0);
    }
}