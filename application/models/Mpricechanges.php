<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpricechanges extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "pricechanges";
        $this->_primary_key = "PriceChangeId";
    }

    public function getPrice($productId, $productChildId, $defaultPrice = 0){
        return $this->getFieldValue(array('ProductId' => $productId, 'ProductChildId' => $productChildId), 'Price', $defaultPrice);
    }
}