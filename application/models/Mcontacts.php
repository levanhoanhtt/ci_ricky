<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcontacts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "contacts";
        $this->_primary_key = "ContactId";
    }

    public function getCount($postData){
        $query = "1=1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM contacts WHERE 1=1" . $this->buildQuery($postData) . ' ORDER BY ContactId ASC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['CustomerId']) && $postData['CustomerId'] > 0) $query.=" AND CustomerId=".$postData['CustomerId'];
        if(isset($postData['GenderId']) && $postData['GenderId'] > 0) $query.=" AND GenderId=".$postData['GenderId'];
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND StatusId=".$postData['StatusId'];
        if(isset($postData['CustomerName']) && !empty($postData['CustomerName'])) $query.=" AND CustomerName LIKE '%{$postData['CustomerName']}%'";
        return $query;
    }
}
