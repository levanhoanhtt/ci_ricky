<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mspeedsms extends CI_Model {

    public function send($phones, $message, $userId){
        if(!empty($phones) && !empty($message)) {
            require_once APPPATH . "/libraries/SpeedSMSAPI.php";
            $speedSMSAPI = new SpeedSMSAPI();
            $sms = $speedSMSAPI->sendSMS($phones, $message, SpeedSMSAPI::SMS_TYPE_BRANDNAME, 'RICKY');
            if(!empty($sms)){
                $postData = array(
                    'SendJson' => json_encode(array('phones' => $phones, 'message' => $message)),
                    'ResponseJson' => json_encode($sms),
                    'SMSCampaignId' => 0,
                    'SendPhones' => json_encode($phones),
                    'SendContent' => $message,
                    'SMSTypeId' => SpeedSMSAPI::SMS_TYPE_BRANDNAME,
                    'SenderName' => 'RICKY',
                    'StatusName' => $sms['status'],
                    'ErrorCode' => $sms['code'],
                    'CrUserId' => $userId,
                    'CrDateTime' => getCurentDateTime()
                );
                if($sms['status'] == 'success'){
                    $postData['TranId'] = $sms['data']['tranId'];
                    $postData['TotalSMS'] = $sms['data']['totalSMS'];
                    $postData['TotalPrice'] = $sms['data']['totalPrice'];
                    $postData['InvalidPhones'] = json_encode($sms['data']['invalidPhone']);
                    $postData['StatusName'] = $sms['status'];

                }
                else $postData['ErrorMessage'] = $sms['message'];
                $this->load->model('Msmslogs');
                $flag = $this->Msmslogs->save($postData);
                if($flag > 0) return true;
            }
        }
        return false;
    }
}