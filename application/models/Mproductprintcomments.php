<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproductprintcomments extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "productprintcomments";
        $this->_primary_key = "ProductPrintsCommentId";
    }

    public function getListByProductPrintId($productPrintId){
        return $this->getByQuery('SELECT productprintcomments.*, users.FullName, users.Avatar FROM productprintcomments INNER JOIN users ON productprintcomments.UserId = users.UserId WHERE productprintcomments.ProductPrintId = ? ORDER BY productprintcomments.CrDateTime DESC', array($productPrintId));
    }
}