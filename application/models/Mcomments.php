<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcomments extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "comments";
        $this->_primary_key = "CommentId";
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM comments WHERE StatusId > 0" . $this->buildQuery($postData) . ' ORDER BY CommentId ASC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['ItemId']) && $postData['ItemId'] > 0) $query.=" AND ItemId=".$postData['ItemId'];
        if(isset($postData['ItemTypeId']) && $postData['ItemTypeId'] > 0) $query.=" AND ItemTypeId=".$postData['ItemTypeId'];
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND StatusId=".$postData['StatusId'];
        if(isset($postData['ParentCommentId']) && $postData['ParentCommentId'] > 0) $query.=" AND ParentCommentId=".$postData['ParentCommentId'];
        if(isset($postData['CommentStarId']) && $postData['CommentStarId'] > 0) $query.=" AND CommentStarId=".$postData['CommentStarId'];
        if(isset($postData['IpAddress']) && !empty($postData['IpAddress'])) $query.=" AND IpAddress LIKE '%{$postData['IpAddress']}%'";
        return $query;
    }
}
