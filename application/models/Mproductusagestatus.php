<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproductusagestatus extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "productusagestatus";
        $this->_primary_key = "ProductUsageStatusId";
    }
}