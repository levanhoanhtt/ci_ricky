<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mstorefunds extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "storefunds";
        $this->_primary_key = "StoreFundId";
    }
}
