<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mimportproducterrors extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "importproducterrors";
        $this->_primary_key = "ImportProductErrorId";
    }
}