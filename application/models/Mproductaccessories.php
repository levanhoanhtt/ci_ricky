<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproductaccessories extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "productaccessories";
        $this->_primary_key = "ProductAccessoryId";
    }
}