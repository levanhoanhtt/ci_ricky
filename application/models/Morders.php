<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Morders extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "orders";
        $this->_primary_key = "OrderId";
    }

    public $labelCss = array(
        'OrderStatusCss' => array(
            1 => 'label lable-grey',
            2 => 'label lable-yellow',
            3 => 'label lable-red',
            4 => 'label lable-yellow',
            5 => 'label lable-grey',
            6 => 'label lable-green'
        ),
        'PaymentStatusCss' => array(
            1 => 'label lable-red',
            2 => 'label lable-yellow',
            3 => 'label lable-green',
            4 => 'label lable-grey',
            5 => 'label lable-grey',
            6 => 'label lable-yellow'
        )
    );

    public function update($postData, $orderId, $products = array(), $tagNames = array(), $services = array(), $promotion = array(), $comments = array(), $remindData = array(), $POSData = array(), $customerConsult = array(), $customerConsultId = 0, $actionLog = array()){
        $this->load->model('Mtags');
        $this->load->model('Mreminds');
        $this->load->model('Morderpromotions');
        $this->load->model('Mtransactions');
        $this->load->model('Minventories');
        $this->load->model('Mconsultsells');
        $this->load->model('Mcustomerconsults');
        $actionLogs = array();
        $itemTypeId = 6;
        $isUpdate = $orderId > 0 ? true : false;
        $crUserId = $isUpdate ? $postData['UpdateUserId'] : $postData['CrUserId'];
        $crDateTime = $isUpdate ? $postData['UpdateDateTime'] : $postData['CrDateTime'];
        $this->db->trans_begin();
        $orderId = $this->save($postData, $orderId, array('UpdateUserId', 'UpdateDateTime'));
        if ($orderId > 0) {
            if(!empty($actionLog)) {
                $actionLog['ItemId'] = $orderId;
                $actionLogs[] = $actionLog;
            }
            if($isUpdate){
                $this->db->delete('orderproducts', array('OrderId' => $orderId));
                $this->db->delete('itemtags', array('ItemId' => $orderId, 'ItemTypeId' => $itemTypeId));
                $this->db->delete('orderservices', array('OrderId' => $orderId));
                $this->db->delete('orderpromotions', array('OrderId' => $orderId));
                if(!empty($promotion)) $this->db->update('orderpromotions', array('StatusId' => 0), array('OrderId' => $orderId));
                if(!empty($remindData)) $this->db->update('reminds', array('RemindStatusId' => 6, 'UpdateUserId' => $postData['UpdateUserId'], 'UpdateDateTime' => $postData['UpdateDateTime']), array('OrderId' => $orderId));
            }
            else {
                $this->db->update('orders', array('OrderCode' => $this->genOrderCode($orderId)), array('OrderId' => $orderId));
                if(!empty($comments)){
                    $orderComments = array();
                    foreach($comments as $comment){
                        $orderComments[] = array(
                            'OrderId' => $orderId,
                            'UserId' => $crUserId,
                            'Comment' => $comment,
                            'CommentTypeId' => 1,
                            'CrDateTime' => $crDateTime
                        );
                    }
                    if (!empty($orderComments)) $this->db->insert_batch('ordercomments', $orderComments);
                }
                if($customerConsult['CustomerConsultId'] > 0) $this->db->update('customerconsults', array('OrderId' => $orderId), array('CustomerConsultId' => $customerConsult['CustomerConsultId']));
                //if(!empty($customerConsult['ConsultUserIds'])) {
                    $consultSellId = $this->Mconsultsells->save(array(
                        'CustomerConsultId' => $customerConsult['CustomerConsultId'],
                        'OrderId' => $orderId,
                        'StatusId' => 1
                    ));
                    $customerConsultUsers = array(
                        array(
                            'ConsultSellId' => $consultSellId,
                            'CustomerConsultId' => $customerConsult['CustomerConsultId'],
                            'UserId' => $crUserId,
                            'IsCrOrder' => 2,
                            'Percent' => 0,
                            'Comment' => '',
                            'ConsultCommentId' => 0,
                            'CrDateTime' => $crDateTime
                        )
                    );
                    foreach ($customerConsult['ConsultUserIds'] as $userId) {
                        if($userId != $crUserId){
                            $customerConsultUsers[] = array(
                                'ConsultSellId' => $consultSellId,
                                'CustomerConsultId' => $customerConsult['CustomerConsultId'],
                                'UserId' => $userId,
                                'IsCrOrder' => 1,
                                'Percent' => 0,
                                'Comment' => '',
                                'ConsultCommentId' => 0,
                                'CrDateTime' => $crDateTime
                            );
                        }
                    }
                    if (!empty($customerConsultUsers)) $this->db->insert_batch('customerconsultusers', $customerConsultUsers);
                //}
            }
            if(!empty($products)) {
                $orderProducts = array();
                foreach($products as $p){
                    $p['OrderId'] = $orderId;
                    $orderProducts[] = $p;
                }
                if (!empty($orderProducts)) $this->db->insert_batch('orderproducts', $orderProducts);
            }
            if(!empty($tagNames)) {
                $itemTags = array();
                foreach ($tagNames as $tagName) {
                    $tagId = $this->Mtags->getTagId($tagName, $itemTypeId);
                    if ($tagId > 0) {
                        $itemTags[] = array(
                            'ItemId' => $orderId,
                            'ItemTypeId' => $itemTypeId,
                            'TagId' => $tagId
                        );
                    }
                }
                if (!empty($itemTags)) $this->db->insert_batch('itemtags', $itemTags);
            }
            if(!empty($services)){
                $orderServices = array();
                foreach ($services as $s) {
                    $s['OrderId'] = $orderId;
                    $orderServices[] = $s;
                }
                if (!empty($orderProducts)) $this->db->insert_batch('orderservices', $orderServices);
            }
            if(!empty($promotion)){
                $promotion['OrderId'] = $orderId;
                $flag = $this->Morderpromotions->save($promotion);
                if($flag && $promotion['PromotionId'] > 0){
                    $promotionCount = $this->Morderpromotions->countRows(array('OrderId' => $orderId, 'PromotionId' => $promotion['PromotionId']));
                    if($promotionCount == 1) $this->db->query('UPDATE promotions SET NumberUse = NumberUse - 1 WHERE PromotionId = ? AND IsUnLimit = 1', array($promotion['PromotionId']));
                }
            }
            if(!empty($remindData)){
                $remindData['RemindTitle'] = 'Nhắc khách thanh toán '.priceFormat($remindData['OwnCost']).' VNĐ tiền đơn hàng '.$this->genOrderCode($orderId);
                $remindData['OrderId'] = $orderId;
                $remindComment = $remindData['RemindComment'];
                unset($remindData['RemindComment']);
                $remindComments = array();
                if(!empty($remindComment)) $remindComments[] = $remindComment;
                $this->Mreminds->update($remindData, 0, $remindComments);
            }
            if(!empty($POSData)){
                $orderCode = $this->genOrderCode($orderId);
                if($postData['PaymentCost'] > 0){
                    $transaction = array(
                        'TransactionTypeId' => 2,
                        'TransactionStatusId' => STATUS_ACTIVED,
                        'MoneySourceId' => $POSData['MoneySourceId'],
                        'MoneyPhoneId' => 0,
                        'VerifyLevelId' => 2,
                        'StoreId' => $postData['StoreId'],
                        'TransactionReasonId' => 0,
                        'CustomerId' => $postData['CustomerId'],
                        'BankId' => $POSData['BankId'],
                        'PaidCost' => $postData['PaymentCost'],
                        'OrderId' => $orderId,
                        'Comment' => 'Thanh toán đơn hàng '.$orderCode,
                        'PrintStatusId' => 1,
                        'HasDebt' => 1,
                        'DebtComment' => '',
                        'TransportId' => 0,
                        'FundId' => 0,
                        'TransactionKindId' => 0,
                        'CrUserId' => $crUserId,
                        'CrDateTime' => $crDateTime
                    );
                    $transactionId = $this->Mtransactions->update($transaction);
                    if($transactionId > 0){
                        $actionLogs[] = array(
                            'ItemId' => $transactionId,
                            'ItemTypeId' => 18,
                            'ActionTypeId' => 1,
                            'Comment' => $POSData['FullName'] . ' thêm phiếu chi thanh toán đơn hàng '.$orderCode,
                            'CrUserId' => $crUserId,
                            'CrDateTime' => $crDateTime
                        );
                        if($POSData['RealPaymentCost'] > 0){
                            $transaction['TransactionTypeId'] = 1;
                            $transaction['PaidCost'] = $POSData['RealPaymentCost'];
                            $transactionId = $this->Mtransactions->update($transaction);
                            if($transactionId > 0){
                                $actionLogs[] = array(
                                    'ItemId' => $transactionId,
                                    'ItemTypeId' => 17,
                                    'ActionTypeId' => 1,
                                    'Comment' => $POSData['FullName'] . ' thêm phiếu thu thanh toán đơn hàng '.$orderCode,
                                    'CrUserId' => $crUserId,
                                    'CrDateTime' => $crDateTime
                                );
                            }
                        }
                    }
                }
                if($POSData['DebitCost'] > 0){
                    $actionLogs[] = array(
                        'ItemId' => $orderId,
                        'ItemTypeId' => 6,
                        'ActionTypeId' => 1,
                        'Comment' => $POSData['FullName'] . ' thêm phiếu ghi nợ đơn hàng '.$orderCode,
                        'CrUserId' => $crUserId,
                        'CrDateTime' => $crDateTime
                    );
                    $transaction = array(
                        'TransactionTypeId' => 2,
                        'TransactionStatusId' => STATUS_ACTIVED,
                        'MoneySourceId' => 5,
                        'MoneyPhoneId' => 0,
                        'VerifyLevelId' => 3,
                        'StoreId' => $postData['StoreId'],
                        'TransactionReasonId' => 0,
                        'CustomerId' => $postData['CustomerId'],
                        'BankId' => 0,
                        'PaidCost' => $POSData['DebitCost'],
                        'OrderId' => $orderId,
                        'Comment' => 'Ghi nợ đơn hàng '.$orderCode,
                        'PrintStatusId' => 1,
                        'HasDebt' => 1,
                        'DebtComment' => '',
                        'TransportId' => 0,
                        'FundId' => 0,
                        'TransactionKindId' => 0,
                        'CrUserId' => $crUserId,
                        'CrDateTime' => $crDateTime
                    );
                    $transactionId = $this->Mtransactions->update($transaction);
                    if($transactionId > 0){
                        $actionLogs[] = array(
                            'ItemId' => $transactionId,
                            'ItemTypeId' => 18,
                            'ActionTypeId' => 1,
                            'Comment' => $POSData['FullName'] . ' thêm phiếu ghi nợ đơn hàng '.$orderCode,
                            'CrUserId' => $crUserId,
                            'CrDateTime' => $crDateTime
                        );
                    }
                }
                //ma Khuyen mai
                if($promotion && $promotion['PromotionId'] > 0){
                    $this->db->query('UPDATE orderpromotions SET StatusId = 2 WHERE PromotionId = ?', array($promotion['PromotionId']));
                    $this->db->query('UPDATE promotions SET NumberUse = NumberUse - 1 WHERE PromotionId = ? AND IsUnLimit = 1', array($promotion['PromotionId']));
                }
                //tru so luong ton kho
                foreach($POSData['InventoryData'] as $id){
                    $id['Comment'] = str_replace('{OrderCode}', $orderCode, $id['Comment']);
                    $this->Minventories->update($id);
                }
            }
            if($customerConsultId > 0) $this->Mcustomerconsults->save(array('OrderId' => $orderId, 'CustomerId' => $postData['CustomerId']), $customerConsultId);
            if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $orderId;
        }
    }

    public  function updateOffset($postData, $orderId, $orderMetas = array(), $products = array(), $comments = array(), $actionLog = array()){
        $isUpdate = $orderId > 0;
        $crUserId = $isUpdate ? $postData['UpdateUserId'] : $postData['CrUserId'];
        $crDateTime = $isUpdate ? $postData['UpdateDateTime'] : $postData['CrDateTime'];
        $this->db->trans_begin();
        $orderId = $this->save($postData, $orderId, array('UpdateUserId', 'UpdateDateTime'));
        if ($orderId > 0) {
            if(!empty($actionLog)) {
                $actionLog['ItemId'] = $orderId;
                $this->Mactionlogs->save($actionLog);
            }
            if($isUpdate){
                $this->db->delete('orderproducts', array('OrderId' => $orderId));
                $this->db->delete('ordermetas', array('OrderId' => $orderId));
            }
            else {
                $this->db->update('orders', array('OrderCode' => $this->genOrderCode($orderId)), array('OrderId' => $orderId));
                if(!empty($comments) ){
                    $orderComments = array();
                    foreach($comments as $comment){
                        $orderComments[] = array(
                            'OrderId' => $orderId,
                            'UserId' => $crUserId,
                            'Comment' => $comment,
                            'CommentTypeId' => 1,
                            'CrDateTime' => $crDateTime
                        );
                    }
                    if (!empty($orderComments)) $this->db->insert_batch('ordercomments', $orderComments);
                }
            }
            if(!empty($products)) {
                $orderProducts = array();
                foreach($products as $p){
                    $p['OrderId'] = $orderId;
                    $orderProducts[] = $p;
                }
                if (!empty($orderProducts)) $this->db->insert_batch('orderproducts', $orderProducts);
            }
            $orderMetaInserts = array();
            foreach($orderMetas as $key => $value){
                $orderMetaInserts[] = array(
                    'OrderId' => $orderId,
                    'MetaName' => $key,
                    'MetaValue' => $value,
                );
            }
            if(!empty($orderMetaInserts)) $this->db->insert_batch('ordermetas', $orderMetaInserts);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $orderId;
        }
    }

    public function changeVerifyStatusBatch($orderIds, $verifyStatusId, $user){
        $crDateTime = getCurentDateTime();
        $comment = $verifyStatusId == 2 ? ($user['FullName'] . ' đã xác thực đơn hàng') : ($user['FullName'] . ' chuyển đơn hàng về trạng thái chưa xác thực');
        $this->db->trans_begin();
        $this->db->query('UPDATE orders SET VerifyStatusId = ?, VerifyUserId = ?, VerifyDateTime = ? WHERE OrderId IN ?', array($verifyStatusId, $user['UserId'], $crDateTime, $orderIds));
        $actionLogs = array();
        foreach ($orderIds as $orderId) {
            $actionLogs[] = array(
                'ItemId' => $orderId,
                'ItemTypeId' => 6,
                'ActionTypeId' => 2,
                'Comment' => $comment,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
        }
        if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function changeStatusBatch($orderIds, $statusId, $metaData){
        $crDateTime = getCurentDateTime();
        $comment = $statusId == 0 ? ($metaData['FullName'] . ' xóa đơn hàng') : ($metaData['FullName'] . ' chuyển đơn hàng về trạng thái '.$this->Mconstants->orderStatus[$statusId]);
        $this->db->trans_begin();
        $actionLogs = array();
        //$tableLogs = array();
        foreach ($orderIds as $orderId) {
            $actionLogs[] = array(
                'ItemId' => $orderId,
                'ItemTypeId' => 6,
                'ActionTypeId' => 2,
                'Comment' => $comment,
                'CrUserId' => $metaData['UserId'],
                'CrDateTime' => $crDateTime
            );
            /*if($statusId == 0){
                $tableLogs[] = array(
                    'TableName' => $this->_table_name,
                    'PrimaryKeyName' => $this->_primary_key,
                    'StatusIdOld' => $this->getFieldValue(array('OrderId' => $orderId), 'OrderStatusId', 0),
                    'IsBack' => 1,
                    'Comment' => $metaData['Comment'],
                    'CrUserId' => $metaData['UserId'],
                    'CrDateTime' => $crDateTime
                );
            }*/
        }
        $this->db->query('UPDATE orders SET OrderStatusId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE OrderId IN ?', array($statusId, $metaData['UserId'], $crDateTime, $orderIds));
        if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        //if(!empty($tableLogs)) $this->db->insert_batch('tablelogs', $tableLogs);
        if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateField($postData, $orderId, $actionLog = array()){
        $this->db->trans_begin();
        $orderId = $this->save($postData, $orderId);
        if($orderId > 0){
            if(!empty($actionLog)) $this->Mactionlogs->save($actionLog);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    /*public function insertChild($data, $orderId, $userId){
        $crDateTime = getCurentDateTime();
        $orderProducts = array();
        $this->db->trans_begin();
        for ($item = 0; $item < count($data); $item++) {
            $dataProduct = $data[$item]['DataProduct'];
            $orderChildId = $this->save(
                array(
                    'OrderCode' => $dataProduct['OrderCode'] . "-" . ($item + 1),
                    'OrderParentId' => $orderId,
                    'StoreId' => $data[$item]['DataProduct']['StoreId'],
                    'CrUserId' => $userId,
                    'CrDateTime' => $crDateTime,
                    'BarCode' => $dataProduct['BarCode'],
                    'CustomerId' => $dataProduct['CustomerId'],
                    'CustomerAddressId' => $dataProduct['CustomerAddressId'],
                    'OrderChanelId' => $dataProduct['OrderChanelId'],
                    'OrderStatusId' => $dataProduct['OrderStatusId'],
                    'TransportCost' => 0,
                    'IsLendBack' => 0,
                    'LendBackCost' => 0,
                    'Discount' => 0,
                    'PreCost' => 0,
                    'VATPercent' =>0,
                    'PaymentStatusId' => $dataProduct['PaymentStatusId'],
                    'VerifyStatusId' => $dataProduct['VerifyStatusId'],
                    'OrderTypeId' => $dataProduct['OrderTypeId'],
                    'DeliveryTypeId' => $dataProduct['DeliveryTypeId'],
                    'OrderReasonId' => $dataProduct['OrderReasonId'],
                    'CODCost' => 0,
                    'CODStatusId' => $dataProduct['CODStatusId'],
                    'UpdateUserId' => $userId,
                    'UpdateDateTime' => $crDateTime,
                    'Comment' => ''
                )
            );
            foreach ($dataProduct as $dProduct) {
                $orderProducts[] = [
                    'OrderId' => $orderChildId,
                    'IsChildren' => 1,
                    'ProductId' => $dProduct['ProductId'],
                    'ProductChildId' => !empty($dProduct['ProductChildId']) ? $dProduct['ProductChildId'] : 0,
                    'Quantity' => $dProduct['order_quantity'],
                    'Price' => $dProduct['Price'],
                    'OriginalPrice' => $dProduct['OriginalPrice'],
                    'DiscountReason' => $dProduct['DiscountReason'],
                ];
            }
        }
        if(!empty($orderProducts)) $this->db->insert_batch('orderproducts', $orderProducts);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function insertOrderWait($dataWithStore, $orderWait, $userId){
        $crDateTime = getCurentDateTime();
        $orderProducts = array();
        $this->db->trans_begin();
        //lưu đơn hàng chuyển trước
        foreach($dataWithStore as $key => $data){
            //lưu đơn hàng con
            $orderIdLast = $this->save([
                'OrderCode' => $data['OrderCode'],
                'OrderParentId' => $data[0]['OrderParentId'],
                'StoreId' => $data[0]['StoreId'],
                'CrUserId' => $userId,
                'CrDateTime' => $crDateTime,
                'BarCode' => $data['BarCode'],
                'CustomerId' => $data['CustomerId'],
                'CustomerAddressId' => $data['CustomerAddressId'],
                'OrderChanelId' => $data['OrderChanelId'],
                'OrderStatusId' => $data['OrderStatusId'],
                'TransportCost' => 0,
                'IsLendBack' => 0,
                'LendBackCost' => 0,
                'Discount' => 0,
                'PreCost' => 0,
                'VATPercent' =>0,
                'PaymentStatusId' => $data['PaymentStatusId'],
                'VerifyStatusId' => $data['VerifyStatusId'],
                'OrderTypeId' => $data['OrderTypeId'],
                'DeliveryTypeId' => $data['DeliveryTypeId'],
                'OrderReasonId' => $data['OrderReasonId'],
                'CODCost' => 0,
                'CODStatusId' => $data['CODStatusId'],
                'UpdateUserId' => $userId,
                'UpdateDateTime' => $crDateTime,
                'Comment' => ''

            ]);
            // lưu các thông tin liên quan đến đơn hàng
            foreach ($data as $keyProduct => $product){
                if(is_numeric($keyProduct)){
                    $orderProducts[] = [
                        'OrderId' => $orderIdLast,
                        'IsChildren' => 1,
                        'ProductId' =>  $product['ProductId'],
                        'ProductChildId' => $product['ProductChildId'],
                        'Quantity' => $product['Quantity'],
                        'Price' => $product['Price'],
                        'OriginalPrice' => $product['OriginalPrice'],
                        'DiscountReason' => $product['DiscountReason']
                    ];
                }
            }
        }
        // lưu đơn hàng chờ
        $orderIdLast = $this->save([
            'OrderParentId' => $orderWait[array_keys($orderWait)[1]]['OrderParentId'],
            'OrderCode' => $orderWait['OrderCode'],
            'OrderStatusId' => 4,
            'CrUserId' => $userId,
            'CrDateTime' => $crDateTime,
        ]);
        foreach ($orderWait as $key => $product){
            if(is_numeric($key)){
                $orderProducts[] = [
                    'OrderId' => $orderIdLast,
                    'IsChildren' => 1,
                    'ProductId' =>  $product['ProductId'],
                    'ProductChildId' => $product['ProductChildId'],
                    'Quantity' => $product['Quantity'],
                    'Price' => $product['Price'],
                    'OriginalPrice' => $product['OriginalPrice'],
                    'DiscountReason' => $product['DiscountReason']
                ];
            }
        }
        if(!empty($orderProducts)) $this->db->insert_batch('orderproducts', $orderProducts);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }*/

    public function revenue($postData){
        $successCost = 0; $successDiscount = 0; $successPos = 0; $successShip = 0;
        $failureCost = 0; $failureDiscount = 0; $failurePos = 0; $failureShip = 0;
        $CODCost = 0; $CODDiscount = 0; $CODPos = 0; $CODShip = 0;
        $byDates = array();
        if(!empty($postData['BeginDate'])){
            if(empty($postData['EndDate'])) $postData['EndDate'] = date('Y-m-d');
            $dates = new DatePeriod(
                new DateTime($postData['BeginDate']),
                new DateInterval('P1D'),
                new DateTime($postData['EndDate'])
            );
            $dateStrs = array();
            foreach($dates as $date) $dateStrs[] = $date->format('d/m/Y');
            $dateStrs = array_reverse($dateStrs);
            foreach($dateStrs as $dateStr){
                $byDates[$dateStr] = array(
                    'Success' => array('TotalCost' => 0, 'DiscountCost' => 0),
                    'Failure' => array('TotalCost' => 0, 'DiscountCost' => 0),
                    'COD' => array('TotalCost' => 0, 'DiscountCost' => 0),
                );
            }
        }
        $CODs = array();
        $deliveryTypes = array();
        $query = 'SELECT orders.OrderId, orders.OrderStatusId, orders.DeliveryTypeId, orders.Discount, orders.CrDateTime, SUM(Quantity * Price) AS SumPrice FROM orderproducts INNER JOIN orders ON orderproducts.OrderId = orders.OrderId WHERE OrderStatusId IN(2, 5, 6)'.$this->buildQuery($postData).' GROUP BY orders.OrderId, orders.OrderStatusId, orders.DeliveryTypeId, orders.Discount, orders.CrDateTime ORDER BY orders.CrDateTime DESC';
        $products = $this->getByQuery($query);
        foreach($products as $p){
            $crDateTime = ddMMyyyy($p['CrDateTime']);
            if($p['OrderStatusId'] == 6){
                $successCost += $p['SumPrice'];
                $successDiscount += $p['Discount'];
                if($p['DeliveryTypeId'] == 1) $successPos++;
                elseif($p['DeliveryTypeId'] == 2) $successShip++;
                if(!isset($byDates[$crDateTime])){
                    $byDates[$crDateTime] = array(
                        'Success' => array('TotalCost' => $p['SumPrice'], 'DiscountCost' => $p['Discount']),
                        'Failure' => array('TotalCost' => 0, 'DiscountCost' => 0),
                        'COD' => array('TotalCost' => 0, 'DiscountCost' => 0),
                       
                    );
                }
                else{
                    $byDates[$crDateTime]['Success']['TotalCost'] += $p['SumPrice'];
                    $byDates[$crDateTime]['Success']['DiscountCost'] += $p['Discount'];
                }
            }
            elseif($p['OrderStatusId'] == 5){
                $failureCost += $p['SumPrice'];
                $failureDiscount += $p['Discount'];
                if($p['DeliveryTypeId'] == 1) $failurePos++;
                elseif($p['DeliveryTypeId'] == 2) $failureShip++;
                if(!isset($byDates[$crDateTime])){
                    $byDates[$crDateTime] = array(
                        'Success' => array('TotalCost' => 0, 'DiscountCost' => 0),
                        'Failure' => array('TotalCost' => $p['SumPrice'], 'DiscountCost' => $p['Discount']),
                        'COD' => array('TotalCost' => 0, 'DiscountCost' => 0)

                    );
                }
                else{
                    $byDates[$crDateTime]['Failure']['TotalCost'] += $p['SumPrice'];
                    $byDates[$crDateTime]['Failure']['DiscountCost'] += $p['Discount'];
                }
            }
            elseif($p['OrderStatusId'] == 2){
                $CODs[$p['OrderId']] = array($crDateTime => array('TotalCost' => $p['SumPrice'], 'DiscountCost' => $p['Discount']));
                $deliveryTypes[$p['OrderId']] = $p['DeliveryTypeId'];
            }
        }
        if(!empty($CODs)){
            $transports = $this->getByQuery('SELECT OrderId FROM transports WHERE CODStatusId = 3 AND OrderId IN ?', array(array_keys($CODs)));
            foreach($transports as $t){
                if(isset($CODs[$t['OrderId']])){
                    foreach($CODs[$t['OrderId']] as $crDateTime => $arrCost){
                        $CODCost += $arrCost['TotalCost'];
                        if(!isset($byDates[$crDateTime])){
                            $byDates[$crDateTime] = array(
                                'Success' => array('TotalCost' => 0, 'DiscountCost' => 0),
                                'Failure' => array('TotalCost' => 0, 'DiscountCost' => 0),
                                'COD' => array('TotalCost' => $arrCost['TotalCost'], 'DiscountCost' => $arrCost['DiscountCost'])
                            );
                        }
                        else{
                            $byDates[$crDateTime]['COD']['TotalCost'] += $arrCost['TotalCost'];
                            $byDates[$crDateTime]['COD']['DiscountCost'] += $arrCost['DiscountCost'];
                        }
                    }
                }
                if(isset($deliveryTypes[$t['OrderId']])){
                    if($deliveryTypes[$t['OrderId']] == 1) $CODPos++;
                    elseif($deliveryTypes[$t['OrderId']] == 2) $CODShip++;
                }
            }
        }
        return array(
            'Success' => array('TotalCost' => $successCost, 'DiscountCost' => $successDiscount, 'POSCount' => $successPos, 'ShipCount' => $successShip),
            'Failure' => array('TotalCost' => $failureCost, 'DiscountCost' => $failureDiscount,'POSCount' => $failurePos, 'ShipCount' => $failureShip),
            'COD' => array('TotalCost' => $CODCost, 'DiscountCost' => $CODDiscount, 'POSCount' => $CODPos, 'ShipCount' => $CODShip),
            'ByDate' => $byDates
        );
    }

    public function revenue_v2($postData){
        $successCost = 0; $successDiscount = 0; $successPos = 0; $successShip = 0;
        $failureCost = 0; $failureDiscount = 0; $failurePos = 0; $failureShip = 0;
        $CODCost = 0; $CODDiscount = 0; $CODPos = 0; $CODShip = 0;
        $byDates = array();
        if(!empty($postData['BeginDate'])){
            if(empty($postData['EndDate'])) $postData['EndDate'] = date('Y-m-d');
            $dates = new DatePeriod(
                new DateTime($postData['BeginDate']),
                new DateInterval('P1D'),
                new DateTime($postData['EndDate'])
            );
            $dateStrs = array();
            foreach($dates as $date) $dateStrs[] = $date->format('d/m/Y');
            $dateStrs = array_reverse($dateStrs);
            foreach($dateStrs as $dateStr){
                $byDates[$dateStr] = array(
                    'Success' => array('TotalCost' => 0, 'DiscountCost' => 0, 'PriceChange' => 0),
                    'Failure' => array('TotalCost' => 0, 'DiscountCost' => 0, 'PriceChange' => 0),
                    'COD' => array('TotalCost' => 0, 'DiscountCost' => 0, 'PriceChange' => 0),
                );
            }
        }
        $CODs = array();
        $deliveryTypes = array();
        $query = 'SELECT orderproducts.ProductId, orderproducts.ProductChildId, orders.OrderId, orders.OrderStatusId, orders.DeliveryTypeId, orders.Discount, orders.CrDateTime, SUM(Quantity * Price) AS SumPrice FROM orderproducts INNER JOIN orders ON orderproducts.OrderId = orders.OrderId WHERE OrderStatusId IN(2, 5, 6)'.$this->buildQuery($postData).' GROUP BY orders.OrderId, orders.OrderStatusId, orders.DeliveryTypeId, orders.Discount, orders.CrDateTime ORDER BY orders.CrDateTime DESC';
        $products = $this->getByQuery($query);
        $this->load->model('Mpricechanges');
        foreach($products as $p){
            $crDateTime = ddMMyyyy($p['CrDateTime']);
            $priceChange = $this->Mpricechanges->getFieldValue(array('ProductId' => $p['ProductId'], 'ProductChildId' => $p['ProductChildId']), 'Price', 0);
            if($p['OrderStatusId'] == 6){
                $successCost += $p['SumPrice'];
                $successDiscount += $p['Discount'];
                if($p['DeliveryTypeId'] == 1) $successPos++;
                elseif($p['DeliveryTypeId'] == 2) $successShip++;
                if(!isset($byDates[$crDateTime])){
                    $byDates[$crDateTime] = array(
                        'Success' => array('TotalCost' => $p['SumPrice'], 'DiscountCost' => $p['Discount'], 'PriceChange' => $priceChange),
                        'Failure' => array('TotalCost' => 0, 'DiscountCost' => 0, 'PriceChange' => 0),
                        'COD' => array('TotalCost' => 0, 'DiscountCost' => 0, 'PriceChange' => 0),
                       
                    );
                }
                else{
                    $byDates[$crDateTime]['Success']['TotalCost'] += $p['SumPrice'];
                    $byDates[$crDateTime]['Success']['DiscountCost'] += $p['Discount'];
                    $byDates[$crDateTime]['Success']['PriceChange'] += $priceChange;
                }
            }
            elseif($p['OrderStatusId'] == 5){
                $failureCost += $p['SumPrice'];
                $failureDiscount += $p['Discount'];
                if($p['DeliveryTypeId'] == 1) $failurePos++;
                elseif($p['DeliveryTypeId'] == 2) $failureShip++;
                if(!isset($byDates[$crDateTime])){
                    $byDates[$crDateTime] = array(
                        'Success' => array('TotalCost' => 0, 'DiscountCost' => 0, 'PriceChange' => 0),
                        'Failure' => array('TotalCost' => $p['SumPrice'], 'DiscountCost' => $p['Discount'], 'PriceChange' => 0),
                        'COD' => array('TotalCost' => 0, 'DiscountCost' => 0, 'PriceChange' => 0)

                    );
                }
                else{
                    $byDates[$crDateTime]['Failure']['TotalCost'] += $p['SumPrice'];
                    $byDates[$crDateTime]['Failure']['DiscountCost'] += $p['Discount'];
                    $byDates[$crDateTime]['Failure']['PriceChange'] += $priceChange;
                }
            }
            elseif($p['OrderStatusId'] == 2){
                $CODs[$p['OrderId']] = array($crDateTime => array('TotalCost' => $p['SumPrice'], 'DiscountCost' => $p['Discount']));
                $deliveryTypes[$p['OrderId']] = $p['DeliveryTypeId'];
            }
        }
        if(!empty($CODs)){
            $transports = $this->getByQuery('SELECT OrderId FROM transports WHERE CODStatusId = 3 AND OrderId IN ?', array(array_keys($CODs)));
            foreach($transports as $t){
                if(isset($CODs[$t['OrderId']])){
                    foreach($CODs[$t['OrderId']] as $crDateTime => $arrCost){
                        $CODCost += $arrCost['TotalCost'];
                        if(!isset($byDates[$crDateTime])){
                            $byDates[$crDateTime] = array(
                                'Success' => array('TotalCost' => 0, 'DiscountCost' => 0, 'PriceChange' => 0),
                                'Failure' => array('TotalCost' => 0, 'DiscountCost' => 0, 'PriceChange' => 0),
                                'COD' => array('TotalCost' => $arrCost['TotalCost'], 'DiscountCost' => $arrCost['DiscountCost'], 'PriceChange' => 0)
                            );
                        }
                        else{
                            $byDates[$crDateTime]['COD']['TotalCost'] += $arrCost['TotalCost'];
                            $byDates[$crDateTime]['COD']['DiscountCost'] += $arrCost['DiscountCost'];
                            $byDates[$crDateTime]['COD']['PriceChange'] += $priceChange;
                        }
                    }
                }
                if(isset($deliveryTypes[$t['OrderId']])){
                    if($deliveryTypes[$t['OrderId']] == 1) $CODPos++;
                    elseif($deliveryTypes[$t['OrderId']] == 2) $CODShip++;
                }
            }
        }
        return array(
            'Success' => array('TotalCost' => $successCost, 'DiscountCost' => $successDiscount, 'POSCount' => $successPos, 'ShipCount' => $successShip),
            'Failure' => array('TotalCost' => $failureCost, 'DiscountCost' => $failureDiscount,'POSCount' => $failurePos, 'ShipCount' => $failureShip),
            'COD' => array('TotalCost' => $CODCost, 'DiscountCost' => $CODDiscount, 'POSCount' => $CODPos, 'ShipCount' => $CODShip),
            'ByDate' => $byDates
        );
    }

    public function productSelling($postData){
        $retVal = array();
        $productFinals = array();
        $productIds = array();
        $query = 'SELECT orderproducts.ProductId, orderproducts.ProductChildId, SUM(Quantity) AS SumQuantity, SUM(Quantity * Price) AS SumPrice
                    FROM orderproducts INNER JOIN orders ON orderproducts.OrderId = orders.OrderId WHERE OrderStatusId = 6'.$this->buildQuery($postData).
                    ' GROUP BY orderproducts.ProductId, orderproducts.ProductChildId';// ORDER BY SumQuantity DESC';
        $listProducts = $this->getByQuery($query);
        foreach($listProducts as $op){
            $op['SumQuantityDeliver'] = 0;
            $op['TotalQuantity'] = $op['SumQuantity'];
            $productFinals[$op['ProductId'].'_'.$op['ProductChildId']] = $op;
            if(!in_array($op['ProductId'], $productIds)) $productIds[] = $op['ProductId'];
        }
        $query = 'SELECT orderproducts.ProductId, orderproducts.ProductChildId, SUM(Quantity) AS SumQuantityDeliver, SUM(Quantity * Price) AS SumPrice
                    FROM orderproducts INNER JOIN orders ON orderproducts.OrderId = orders.OrderId WHERE orders.OrderStatusId > 0 AND orders.OrderId IN(SELECT OrderId FROM transports WHERE TransportStatusId IN(3, 6, 9))'.$this->buildQuery($postData).
            ' GROUP BY orderproducts.ProductId, orderproducts.ProductChildId';
        $listProducts = $this->getByQuery($query);
        foreach($listProducts as $op){
            $key = $op['ProductId'].'_'.$op['ProductChildId'];
            if(isset($productFinals[$key])){
                $productFinals[$key]['SumQuantityDeliver'] = $op['SumQuantityDeliver'];
                $productFinals[$key]['SumPrice'] += $op['SumPrice'];
                $productFinals[$key]['TotalQuantity'] += $op['SumQuantityDeliver'];
            }
            else{
                $op['SumQuantity'] = 0;
                $op['TotalQuantity'] = $op['SumQuantityDeliver'];
                $productFinals[$key] = $op;
            }
            if(!in_array($op['ProductId'], $productIds)) $productIds[] = $op['ProductId'];
        }
        if(!empty($productFinals)){
            usort($productFinals, function ($a, $b) {
                return $a['TotalQuantity'] < $b['TotalQuantity'];
            });
            $listProductQuantity = $this->Mproductquantity->getListQuantity($productIds);
            $products = array();
            $productChilds = array();
            foreach ($productFinals as $key => $op){
                if (!isset($products[$op['ProductId']])) $products[$op['ProductId']] = $this->Mproducts->get($op['ProductId'], true, '', 'ProductName, BarCode, ProductKindId');
                if ($op['ProductChildId'] > 0) {
                    if (!isset($productChilds[$op['ProductChildId']])) $productChilds[$op['ProductChildId']] = $this->Mproductchilds->get($op['ProductChildId'], true, '', 'ProductName, BarCode');
                    $op['ProductName'] = $products[$op['ProductId']]['ProductName'] . ' (' . $productChilds[$op['ProductChildId']]['ProductName'] . ')';
                    $op['BarCode'] = $productChilds[$op['ProductChildId']]['BarCode'];
                }
                else {
                    $op['ProductName'] = $products[$op['ProductId']]['ProductName'];
                    $op['BarCode'] = $products[$op['ProductId']]['BarCode'];
                }
                $op['ProductKindId'] = $products[$op['ProductId']]['ProductKindId'];
                $quantity = 0;
                foreach($listProductQuantity as $pq){
                    if($pq['ProductId'] == $op['ProductId'] && $pq['ProductChildId'] == $op['ProductChildId']) $quantity += $pq['Quantity'];
                }
                $op['Quantity'] = $quantity;
                $retVal[] = $op;
            }
        }
        return $retVal;
    }

    public function orderReason($postData){
        $query = "SELECT OrderReasonId, DATE_FORMAT(CrDateTime, '%Y-%m-%d') AS CrDate, COUNT(OrderId) AS CountOrder FROM orders
                WHERE ((OrderStatusId = 6 AND DeliveryTypeId = 1) OR (DeliveryTypeId = 2 AND OrderId IN(SELECT OrderId FROM transports)))".
                $this->buildQuery($postData)." GROUP BY OrderReasonId, CrDate";
        return $this->getByQuery($query);
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $postData = array()){
        $queryCount = "select orders.OrderId AS totalRow from orders {joins} where {wheres}";
        $query = "select {selects} from orders {joins} where {wheres} ORDER BY orders.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'orders.*',
            'customers.FullName',
            'transports.TransportStatusId',
            //'stores.StoreName'
        ];
        $joins = [
            'customers' => "left join customers on customers.CustomerId = orders.CustomerId",
            'transports' => "left join transports on transports.TransportId = (SELECT TransportId FROM transports t2 WHERE t2.OrderId = orders.OrderId ORDER BY TransportId DESC LIMIT 1)",
            'stores' => "left join stores on stores.StoreId = orders.StoreId"
        ];
        $wheres = array('OrderStatusId > 0');
        $whereSearch= '';
        $dataBind = [];
        $searchText = strtolower($searchText);
        if(empty($searchText)) {
            if (isset($postData['CustomerId']) && $postData['CustomerId'] > 0) {
                $wheres[] = 'orders.CustomerId = ?';
                $dataBind[] = $postData['CustomerId'];
            }
            if (isset($postData['CustomerKindId']) && $postData['CustomerKindId'] > 0) {
                $wheres[] = 'customers.CustomerKindId = ?';
                $dataBind[] = $postData['CustomerKindId'];
            }
            if (isset($postData['CrUserId']) && $postData['CrUserId'] > 0) {
                $wheres[] = 'orders.CrUserId = ?';
                $dataBind[] = $postData['CrUserId'];
            }
            if (isset($postData['HasCTV']) && $postData['HasCTV'] == 1) {
                $wheres[] = 'orders.CTVId > 0';
                $selects[] = 'ctv.FullName AS CTVName';
                $joins['CTV'] = 'left join customers as ctv on ctv.CustomerId = orders.CTVId';
            }
            if(isset($postData['IsSearchExactlyPhone']) && $postData['IsSearchExactlyPhone'] == 1) $whereSearch = "customers.PhoneNumber = '0'";
            if(isset($postData['OrderTypeId'])){
                if($postData['OrderTypeId'] > 0){
                    $wheres[] = 'orders.OrderTypeId = ?';
                    $dataBind[] = $postData['OrderTypeId'];
                }
                else{
                    $wheres[] = 'orders.OrderTypeId != ?';
                    $dataBind[] = OFFSET_ORDER_TYPE_ID;
                }
            }
        }
        else{
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'orders.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            /*elseif(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'orders.OrderCode like ? or orders.CrDateTime like ?';
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
            }
            elseif(strpos("chua",$searchText) > -1 )$whereSearch = 'orders.OrderStatusId = 3 or orders.PaymentStatusId = 1 or orders.VerifyStatusId = 1 or orders.CODStatusId = 2';
            elseif(strpos('da',$searchText) > -1) $whereSearch = 'orders.OrderStatusId in (1,2) or orders.PaymentStatusId = 2 or orders.VerifyStatusId = 2';
            elseif(strpos('dang',$searchText) > -1) $whereSearch = 'orders.OrderStatusId = 4';*/
            else{
                if(isset($postData['IsSearchExactlyPhone']) && $postData['IsSearchExactlyPhone'] == 1){
                    if(strlen($searchText) >= 8) {
                        $whereSearch = 'customers.PhoneNumber = ? or customers.PhoneNumber2 = ? or orders.OrderCode = ?';
                        for ($i = 0; $i < 3; $i++) $dataBind[] = $searchText;
                    }
                    else $whereSearch = "customers.PhoneNumber = '0'";
                }
                else {
                    $whereSearch = 'orders.OrderCode like ? or customers.FullName like ? or customers.Email like ? or customers.PhoneNumber like ? or customers.PhoneNumber2 like ?';
                    for ($i = 0; $i < 5; $i++) $dataBind[] = "%$searchText%";
                }
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                switch ($filed_name) {
                    case 'order_status':
                        $wheres[] = "orders.OrderStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_status_payment':
                        $wheres[] = "orders.PaymentStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_create':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'orders.CrDateTime between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "orders.CrDateTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "orders.CrDateTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(orders.CrDateTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    case 'order_status_transport' :
                        if($conds[1] == 4){
                            $wheres[] = "(transports.TransportStatusId = 4 AND orders.DeliveryTypeId = 2) or (orders.OrderStatusId = 6 AND orders.DeliveryTypeId = 1)";
                        }
                        else{
                            $wheres[] = "transports.TransportStatusId = ? AND orders.DeliveryTypeId = 2";
                            $dataBind[] = $conds[1];
                        }
                        /*$wheres[] = "(transports.TransportStatusId = ? AND orders.DeliveryTypeId = 2) or (orders.OrderStatusId = 6 AND orders.DeliveryTypeId = 1)";
                        $dataBind[] = $conds[1];*/
                        break;
                    case 'order_chanels':
                        $wheres[] = "orders.OrderChanelId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_type':
                        $wheres[] = "orders.OrderTypeId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'customer_kind':
                        $wheres[] = "customers.CustomerKindId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_status_cod':
                        $wheres[] = "transports.CODStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_transporter':
                        $wheres[] = "transports.TransporterId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_status_authen':
                        $wheres[] = "orders.VerifyStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_method_payment':
                        $wheres[] = "orders.PaymentStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_method_transaction':
                        $wheres[] = "transports.TransportTypeId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_store':
                        $wheres[] = "orders.StoreId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_cost':
                        $wheres[] = "orders.TotalCost $conds[0] ?";
                        $dataBind[] = replacePrice($conds[1]);
                        break;
                    case 'order_user_create':
                        $wheres[] = "orders.CrUserId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'transport_day_process':
                        $wheres[] = "transports.CrDateTime IS NOT NULL AND DATEDIFF(NOW(), transports.CrDateTime) $conds[0] ?";
                        $dataBind[] = replacePrice($conds[1]);
                        break;
                    case 'order_tag':
                        $wheres[] = "orders.OrderId $conds[0](SELECT ItemId FROM itemtags WHERE ItemTypeId = 6 AND TagId = ?)";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $dataOrders = $this->getByQuery($query, $dataBind);
        for ($i = 0; $i < count($dataOrders); $i++) {
            $dataOrders[$i]['OrderStatus'] = $dataOrders[$i]['OrderStatusId'] > 0 ? $this->Mconstants->orderStatus[$dataOrders[$i]['OrderStatusId']] : '';
            if($dataOrders[$i]['DeliveryTypeId'] == 1){
                $dataOrders[$i]['TransportStatusId'] = 4;
                $dataOrders[$i]['PaymentStatusId'] = 3;
            }
            $dataOrders[$i]['TransportStatus'] = $dataOrders[$i]['TransportStatusId'] > 0 ? $this->Mconstants->transportStatus[$dataOrders[$i]['TransportStatusId']] : '';
            $dataOrders[$i]['PaymentStatus'] = $dataOrders[$i]['PaymentStatusId'] > 0 ? $this->Mconstants->paymentStatus[$dataOrders[$i]['PaymentStatusId']] : '';
            $dataOrders[$i]['OrderChanel'] = $dataOrders[$i]['OrderChanelId'] > 0 ? $this->Mconstants->orderChannels[$dataOrders[$i]['OrderChanelId']] : '';
            $dayDiff = getDayDiff($dataOrders[$i]['CrDateTime'], $now);
            $dataOrders[$i]['CrDateTime'] = ddMMyyyy($dataOrders[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataOrders[$i]['DayDiff'] = $dayDiff;
            $dataOrders[$i]['labelCss'] = $this->Mconstants->labelCss;
            $dataOrders[$i]['orderLabelCss'] = $this->labelCss;
            $dataOrders[$i]['transportStatusCss'] = $this->Mtransports->labelCss['TransportStatusCss'];
            if(isset($postData['OrderTypeId']) && $postData['OrderTypeId'] == 7){
                $dataOrders[$i]['OrderOffsetCode'] = $dataOrders[$i]['OrderCode'];
                $dataOrders[$i]['OrderCode'] = $dataOrders[$i]['ParentOrderId'] > 0 ? $this->Morders->get($dataOrders[$i]['ParentOrderId'], true, '', 'OrderCode')['OrderCode']: '';
            }
        }
        $data = array();
        $totalRows = $this->getByQuery($queryCount, $dataBind);
        $totalRow = count($totalRows);
        $totalIds = array();
        foreach ($totalRows as $v) $totalIds[] = intval($v['totalRow']);
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $dataOrders;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentOrders';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        $data['totalIds'] = json_encode($totalIds);
        return $data;
    }

    public function searchByOrderStatus($postData = array()){
        //$query = "select {selects} from orders {joins} where orders.OrderStatusId IN {$postData['OrderStatusIds']} ". $this->buildQuery($postData)." ORDER BY orders.CrDateTime DESC";
        $query = "select {selects} from orders {joins} where {wheres} ORDER BY orders.CrDateTime DESC";
        $selects = [
            'orders.*',
            'customers.CustomerId',
            'customers.FullName',
            //'customers.Email',
            //'customers.PhoneNumber',
            //'customers.PhoneNumber2',
            'transports.TransportStatusId',
            //'stores.StoreName'
        ];
        $joins = [
            'customers' => "left join customers on customers.CustomerId = orders.CustomerId",
            'transports' => "left join transports on transports.TransportId = (SELECT TransportId FROM transports t2 WHERE t2.OrderId = orders.OrderId ORDER BY TransportId DESC LIMIT 1)",
            //'stores' => "left join stores on stores.StoreId = orders.StoreId"
        ];
        $wheres = array('orders.OrderStatusId > 0');
        if(isset($postData['OrderStatusIds'])) {
            $wheres[] = 'orders.OrderStatusId IN '.$postData['OrderStatusIds'];
            //if(strpos($postData['OrderStatusIds'], '2') !== false) $wheres[] = 'transports.CODCost > 0';
            if($postData['OrderStatusIds'] == '(2)') $wheres[] = 'transports.CODCost > 0';
        }
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $wheres[] = "orders.CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $wheres[] = "orders.CrDateTime <= '{$postData['EndDate']}'";
        $joins_string = implode(' ', $joins);
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $now = new DateTime(date('Y-m-d'));
        $dataOrders = $this->getByQuery($query);
        for ($i = 0; $i < count($dataOrders); $i++) {
            $dataOrders[$i]['OrderStatus'] = $dataOrders[$i]['OrderStatusId'] > 0 ? $this->Mconstants->orderStatus[$dataOrders[$i]['OrderStatusId']] : '';
            if($dataOrders[$i]['DeliveryTypeId'] == 1){
                $dataOrders[$i]['TransportStatusId'] = 4;
                $dataOrders[$i]['PaymentStatusId'] = 3;
            }
            $dataOrders[$i]['TransportStatus'] = $dataOrders[$i]['TransportStatusId'] > 0 ? $this->Mconstants->transportStatus[$dataOrders[$i]['TransportStatusId']] : '';
            $dataOrders[$i]['PaymentStatus'] = $dataOrders[$i]['PaymentStatusId'] > 0 ? $this->Mconstants->paymentStatus[$dataOrders[$i]['PaymentStatusId']] : '';
            $dataOrders[$i]['OrderChanel'] = $dataOrders[$i]['OrderChanelId'] > 0 ? $this->Mconstants->orderChannels[$dataOrders[$i]['OrderChanelId']] : '';
            $dayDiff = getDayDiff($dataOrders[$i]['CrDateTime'], $now);
            $dataOrders[$i]['CrDateTime'] = ddMMyyyy($dataOrders[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataOrders[$i]['DayDiff'] = $dayDiff;
            $dataOrders[$i]['labelCss'] = $this->Mconstants->labelCss;
            $dataOrders[$i]['orderLabelCss'] = $this->labelCss;
            $dataOrders[$i]['transportStatusCss'] = $this->Mtransports->labelCss['TransportStatusCss'];
        }
        return $dataOrders;
    }

    public function searchByOrderStatus_v2($postData = array()){
        $query = "select {selects} from orders {joins} where {wheres} ORDER BY orders.CrDateTime DESC";
        $selects = [
            'orders.*',
            'customers.CustomerId',
            'customers.FullName',
            'orderproducts.ProductId', 
            'orderproducts.ProductChildId'
        ];
        $joins = [
            'orderproducts' => "INNER JOIN orderproducts ON orderproducts.OrderId = orders.OrderId",
            'customers' => "left join customers on customers.CustomerId = orders.CustomerId",
        ];
        $wheres = array('orders.OrderStatusId > 0');
        if(isset($postData['OrderStatusIds'])) {
            $wheres[] = 'orders.OrderStatusId IN '.$postData['OrderStatusIds'];
            //if(strpos($postData['OrderStatusIds'], '2') !== false) $wheres[] = 'transports.CODCost > 0';
            // if($postData['OrderStatusIds'] == '(2)') $wheres[] = 'transports.CODCost > 0';
        }
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $wheres[] = "orders.CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $wheres[] = "orders.CrDateTime <= '{$postData['EndDate']}'";
        $joins_string = implode(' ', $joins);
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $now = new DateTime(date('Y-m-d'));
        $dataOrders = $this->getByQuery($query);
        $this->load->model('Mpricechanges');
        for ($i = 0; $i < count($dataOrders); $i++) {
            $dayDiff = getDayDiff($dataOrders[$i]['CrDateTime'], $now);
            $dataOrders[$i]['CrDateTime'] = ddMMyyyy($dataOrders[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataOrders[$i]['DayDiff'] = $dayDiff;
            $dataOrders[$i]['PriceChange'] = $this->Mpricechanges->getFieldValue(array('ProductId' => $dataOrders[$i]['ProductId'], 'ProductChildId' => $dataOrders[$i]['ProductChildId']), 'Price', 0);
        }
        return $dataOrders;
    }

    public function getCount($postData){
        $query = "OrderStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1, $select = '*'){
        $query = "SELECT {$select} FROM orders WHERE OrderStatusId > 0" . $this->buildQuery($postData).' ORDER BY CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    public function genOrderCode($orderId, $prefix = 'DH'){
        return $prefix . '-' . ($orderId + 10000);
    }

    public function searchShipCost($postData, $perPage = 0, $page = 1){
        $query = "SELECT orders.OrderId, orders.OrderCode, orders.TransportCost, transports.ShipCost, transports.CrDateTime FROM orders left join transports on transports.TransportId = (SELECT TransportId FROM transports t2 WHERE t2.OrderId = orders.OrderId ORDER BY TransportId DESC LIMIT 1) WHERE orders.OrderStatusId > 0 " . $this->buildQuery($postData).' ORDER BY orders.CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    public function searchExportOrder($postData){
        $query = "SELECT orders.OrderCode, orders.CrDateTime, orders.OrderStatusId, orders.OrderReasonId, orders.Discount, orders.TransportCost, orders.PaymentStatusId, orders.DeliveryTypeId, orders.OrderChanelId,
                customers.FullName, customers.PhoneNumber, customers.ProvinceId,
                transports.TransportId, transports.TransportStatusId, transports.CrDateTime as TransportCrDateTime, transports.TransportTypeId, transports.TransporterId,
                transports.Tracking,transports.ShipCost, transports.CODCost, transports.CODStatusId,
                provinces.ProvinceName, stores.StoreName, ordertypes.OrderTypeName, orderreasons.OrderReasonName,
                SUM(orderproducts.Quantity * orderproducts.Price) AS OrderCost,
                SUM(orderproducts.Quantity * orderproducts.PriceCapital) AS OrderCostCapital
                FROM orders
                left join customers on customers.CustomerId = orders.CustomerId
                left join provinces on provinces.ProvinceId = customers.ProvinceId
                left join stores on stores.StoreId = orders.StoreId
                left join ordertypes on ordertypes.OrderTypeId = orders.OrderTypeId
                left join orderreasons on orderreasons.OrderReasonId = orders.OrderReasonId
                inner join orderproducts ON orderproducts.OrderId = orders.OrderId
                left join transports on transports.TransportId = (SELECT TransportId FROM transports t2 WHERE t2.OrderId = orders.OrderId ORDER BY TransportId DESC LIMIT 1)
                WHERE orders.OrderStatusId > 0" . $this->buildQuery($postData) . "GROUP BY orders.OrderId ORDER BY orders.CrDateTime DESC";
        $dataOrders = $this->getByQuery($query);
        $transportTypeNames = array();
        $transporterNames = array();
        for ($i = 0; $i < count($dataOrders); $i++) {
            $dataOrders[$i]['OrderStatus'] = $dataOrders[$i]['OrderStatusId'] > 0 ? $this->Mconstants->orderStatus[$dataOrders[$i]['OrderStatusId']] : '';
            $dataOrders[$i]['TransportCrDateTime'] = $dataOrders[$i]['TransportId'] > 0 ? ddMMyyyy($dataOrders[$i]['TransportCrDateTime'],'d/m/Y H:i') : "";
            $dataOrders[$i]['TransportStatus'] = $dataOrders[$i]['TransportId'] > 0 ? $this->Mconstants->transportStatus[$dataOrders[$i]['TransportStatusId']] : '';
            if($dataOrders[$i]['TransportId'] > 0 && $dataOrders[$i]['TransportTypeId'] > 0){
                $id = $dataOrders[$i]['TransportTypeId'];
                if(!isset($transportTypeNames[$id])) $transportTypeNames[$id] = $this->Mtransporttypes->getFieldValue(array('TransportTypeId' => $id), 'TransportTypeName');
                $transportTypeName = $transportTypeNames[$id];
            }
            else $transportTypeName = '';
            $dataOrders[$i]['TransportTypeName'] = $transportTypeName;
            if($dataOrders[$i]['TransportId'] > 0 && $dataOrders[$i]['TransporterId'] > 0){
                $id = $dataOrders[$i]['TransporterId'];
                if(!isset($transporterNames[$id])) $transporterNames[$id] = $this->Mtransporters->getFieldValue(array('TransporterId' => $id), 'TransporterName');
                $transporterName = $transporterNames[$id];
            }
            else $transporterName = '';
            $dataOrders[$i]['TransporterName'] = $transporterName;
            $dataOrders[$i]['Tracking'] = $dataOrders[$i]['TransportId'] > 0 ? $dataOrders[$i]['Tracking']:'';
            $dataOrders[$i]['CODStatus'] = $dataOrders[$i]['CODStatusId'] > 0 ? $this->Mconstants->CODStatus[$dataOrders[$i]['CODStatusId']] : '';
            if($dataOrders[$i]['DeliveryTypeId'] == 1){
                $dataOrders[$i]['TransportStatusId'] = 4;
                $dataOrders[$i]['PaymentStatusId'] = 3;
            }
            $dataOrders[$i]['TransportStatus'] = $dataOrders[$i]['TransportStatusId'] > 0 ? $this->Mconstants->transportStatus[$dataOrders[$i]['TransportStatusId']] : '';
            $dataOrders[$i]['PaymentStatus'] = $dataOrders[$i]['PaymentStatusId'] > 0 ? $this->Mconstants->paymentStatus[$dataOrders[$i]['PaymentStatusId']] : '';
            $dataOrders[$i]['OrderChanel'] = $dataOrders[$i]['OrderChanelId'] > 0 ? $this->Mconstants->orderChannels[$dataOrders[$i]['OrderChanelId']] : '';
            $dataOrders[$i]['CrDateTime'] = ddMMyyyy($dataOrders[$i]['CrDateTime'],'d/m/Y H:i');
        }
        return $dataOrders;
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['OrderStatusId']) && $postData['OrderStatusId'] > 0) $query.=" AND orders.OrderStatusId=".$postData['OrderStatusId'];
        if(isset($postData['CustomerId']) && $postData['CustomerId'] > 0) $query.=" AND orders.CustomerId=".$postData['CustomerId'];
        if(isset($postData['StoreId']) && $postData['StoreId'] > 0) $query.=" AND orders.StoreId=".$postData['StoreId'];
        if(isset($postData['DeliveryTypeId']) && $postData['DeliveryTypeId'] > 0) $query.=" AND orders.DeliveryTypeId=".$postData['DeliveryTypeId'];
        if(isset($postData['CrUserId']) && $postData['CrUserId'] > 0) $query.=" AND orders.CrUserId=".$postData['CrUserId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND orders.CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND orders.CrDateTime <= '{$postData['EndDate']}'";
        if(isset($postData['OrderIds']) && !empty($postData['OrderIds'])) $query.=" AND orders.OrderId IN(".implode(',', $postData['OrderIds']).")";
        if(isset($postData['OrderStatusIds']) && !empty($postData['OrderStatusIds'])) $query.=" AND orders.OrderStatusId IN({$postData['OrderStatusIds']})";
        if(isset($postData['TransportIds']) && !empty($postData['TransportIds'])) $query.=" AND orders.OrderId IN(SELECT OrderId FROM transports WHERE TransportId IN(".implode(',', $postData['TransportIds'])."))";
        if(isset($postData['ProductTypeId']) && !empty($postData['ProductTypeId'])) $query.=" AND orderproducts.ProductTypeId=".$postData['ProductTypeId'];
        return $query;
    }
}