<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcustomergroups extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "customergroups";
        $this->_primary_key = "CustomerGroupId";
    }

    public function update($postData, $customerGroupId){
    	$this->db->trans_begin();
    	$customerGroupId = $this->save($postData, $customerGroupId);
    	if($customerGroupId > 0) $this->db->update('customergroups', array('CustomerGroupCode' => $this->genCustomerGroupCode($customerGroupId)), array('CustomerGroupId' => $customerGroupId));
    	if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $customerGroupId;
        }
    }

    public function genCustomerGroupCode($customerGroupId){
        return 'NKH-' . ($customerGroupId + 10000);
    }
}
