<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mitemcontents extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "itemcontents";
        $this->_primary_key = "ItemContentId";
    }

    public function getCount($postData){
        $query = "ItemContentId > 0". $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function getList($postData){
        $query = "SELECT * FROM `itemcontents` WHERE ItemContentId > 0 ".$this->buildQuery($postData)." ORDER BY  ContentTypeId ASC, DisplayOrder";
        return $this->getByQuery($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM `itemcontents` WHERE ItemContentId > 0 ".$this->buildQuery($postData)." ORDER BY  ContentTypeId ASC, DisplayOrder";
        if ($perPage > 0) {
            $from = ($page - 1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    public function buildQuery($postData){
        $query = '';
        if (isset($postData['ContentTypeId']) && $postData['ContentTypeId'] > 0) $query .= " AND ContentTypeId=".$postData['ContentTypeId'];
        return $query;
    }
}