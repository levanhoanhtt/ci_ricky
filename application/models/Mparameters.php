<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mparameters extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "parameters";
        $this->_primary_key = "ParameterId";
    }

    /*public function getList($parameterCode){
        $query = "SELECT * FROM parametervalues_"+$parameterCode+" WHERE StatusId = 2";
        return $this->getByQuery($query);
    }*/

    public function update($postData, $parameterId){
        $isUpdate = $parameterId > 0 ? true : false;
        $this->db->trans_begin();
        $parameterId = $this->save($postData, $parameterId, array('UpdateUserId', 'UpdateDateTime'));
        if ($parameterId > 0) {
            if(!$isUpdate){
                $code = $this->genParameterCode($parameterId);
                $this->db->update('parameters', array('ParameterCode' => $code), array('ParameterId' => $parameterId));
                $code = strtolower($code);
                $this->load->dbforge();
                if(!$this->db->table_exists('parametervalues_'.$code)){
                    $fields = array(
                        'ParameterValueId' => array('type' => 'INT','constraint' => 11,'unsigned' => TRUE,'auto_increment' => TRUE),
                        'ParameterId' => array('type' => 'smallint','constraint' => '6',),
                        'ParameterValue' => array('type' => 'VARCHAR','constraint' => '250',),
                        'ParentParameterValueId' => array('type' => 'int','constraint' => '11',),
                        'ParameterLevel' => array('type' => 'smallint','constraint' => '6',),
                        'DisplayOrder' => array('type' => 'smallint','constraint' => '6',),
                        'StatusId' => array('type' => 'tinyint','constraint' => '4',),
                        'CrUserId' => array('type' => 'int','constraint' => '11',),
                        'CrDateTime' => array('type' => 'datetime',),
                        'UpdateUserId' => array('type' => 'int','constraint' => '11',),
                        'UpdateDateTime' => array('type' => 'datetime',),
                    );
                    $this->dbforge->add_field($fields);
                    $this->dbforge->add_key('ParameterValueId', TRUE);
                    $this->dbforge->create_table('parametervalues_'.$code);
                }
                $this->load->model('Mparametervalues');
                $this->Mparametervalues->setTable($code);
                $this->Mparametervalues->save(array(
                    'ParameterId' => $parameterId,
                    'ParameterValue' => $postData['ParameterName'],
                    'ParentParameterValueId' => 0,
                    'ParameterLevel' => 1,
                    'DisplayOrder' => 1,
                    'StatusId' => STATUS_ACTIVED,
                    'CrUserId' => $postData['CrUserId'],
                    'CrDateTime' => $postData['CrDateTime']
                ));
            }
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $parameterId;
        }
    }

    public function genParameterCode($parameterId){
        return 'TS_' . ($parameterId + 10000);
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $postData = array()){
        $queryCount = "select parameters.ParameterId AS totalRow from parameters {joins} where {wheres} GROUP BY parameters.ParameterId";
        $query = "select {selects} from parameters {joins} where {wheres} GROUP BY parameters.ParameterId ORDER BY parameters.ParameterId DESC LIMIT {limits}";
        $selects = [
            'parameters.*',
            'parametertypes.ParameterTypeName'
        ];
        $joins = [
            'parametertypes' => 'inner join parametertypes on parametertypes.ParameterTypeId = parameters.ParameterTypeId'
        ];
        $wheres = array('parameters.ParameterId > 0');
        $whereSearch= '';
        $dataBind = [];
        if(isset($postData['ParameterKindId']) && $postData['ParameterKindId'] > 0){
            $whereSearch = 'parameters.ParameterKindId = ?';
            $dataBind[] = $postData['ParameterKindId'];
        }
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'parameters.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            else{
                $whereSearch = 'parameters.ParameterCode like ? or parameters.ParameterName like ?';
                for( $i = 0; $i < 2; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                switch ($filed_name) {
                    case 'parameter_crdatetime':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'parameters.CrDateTime between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "parameters.CrDateTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "parameters.CrDateTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(parameters.CrDateTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    case 'parameter_type':
                        $wheres[] = "parameters.ParameterTypeId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $dataParameters = $this->getByQuery($query, $dataBind);
        for ($i = 0; $i < count($dataParameters); $i++) {
            $dayDiff = getDayDiff($dataParameters[$i]['CrDateTime'], $now);
            $dataParameters[$i]['CrDateTime'] = ddMMyyyy($dataParameters[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataParameters[$i]['DayDiff'] = $dayDiff;
        }
        $data = array();
        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $dataParameters;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentParameters';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }

    public function getClassifyParameter($id)
    {
        $ParameterId = $id;
        $query = 'SELECT p.ParameterId, p.ParameterCode, p.ParameterName
            FROM parameters p
            INNER JOIN relatedparameters r ON r.OtherParameterId = p.ParameterId 
            WHERE r.ParameterId = ?';
        return $this->getByQuery($query, array($ParameterId));
    }
}