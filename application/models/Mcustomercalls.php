<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcustomercalls extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "customercalls";
        $this->_primary_key = "CustomerCallId";
    }

    public function getListByCustomerId($customerId, $userId){
        if($userId > 0){
            return $this->getByQuery('SELECT customercalls.*, users.FullName, users.Avatar
                                                  FROM customercalls
                                                  INNER JOIN exts ON customercalls.UserId = exts.UserId
                                                  INNER JOIN userextensions ON userextensions.ExtId = exts.ExtId
                                                  INNER JOIN users ON users.UserId = userextensions.UserId
                                                  WHERE customercalls.CustomerId = ? AND userextensions.UserId = ?', array($customerId, $userId));
        }
        else{
            return $this->getByQuery('SELECT customercalls.*, users.FullName, users.Avatar
                                                  FROM customercalls
                                                  INNER JOIN exts ON customercalls.UserId = exts.UserId
                                                  INNER JOIN userextensions ON userextensions.ExtId = exts.ExtId
                                                  INNER JOIN users ON users.UserId = userextensions.UserId
                                                  WHERE customercalls.CustomerId = ?', array($customerId));
        }
    }

    public function getListByPhoneNumber($phoneNumber, $userId){
        if($userId > 0){
            return $this->getByQuery('SELECT customercalls.*, users.FullName, users.Avatar
                                                  FROM customercalls
                                                  INNER JOIN exts ON customercalls.UserId = exts.UserId
                                                  INNER JOIN userextensions ON userextensions.ExtId = exts.ExtId
                                                  INNER JOIN users ON users.UserId = userextensions.UserId
                                                  WHERE customercalls.PhoneNumber = ? AND userextensions.UserId = ?', array($phoneNumber, $userId));
        }
        else{
            return $this->getByQuery('SELECT customercalls.*, users.FullName, users.Avatar
                                                  FROM customercalls
                                                  INNER JOIN exts ON customercalls.UserId = exts.UserId
                                                  INNER JOIN userextensions ON userextensions.ExtId = exts.ExtId
                                                  INNER JOIN users ON users.UserId = userextensions.UserId
                                                  WHERE customercalls.PhoneNumber = ?', array($phoneNumber));
        }
    }

    public function getListNumber() {
        $this->db->select('PhoneNumber, max(BeginTime) as timeMax, BeginTime');
        $this->db->where('PhoneNumber > ', 0);
        $this->db->group_by('PhoneNumber');
        $this->db->order_by('timeMax', 'desc');
        $query = $this->db->get($this->_table_name);
        return $query->result();
    }

    public function getListNumberAll($userId) {
        $this->db->select('customercalls.PhoneNumber, max(customercalls.BeginTime) as timeMax, customercalls.BeginTime');
        if($userId > 0){
            $this->db->join('exts', 'exts.UserId = customercalls.UserId');
            $this->db->join('userextensions', 'userextensions.ExtId = exts.ExtId');
            $this->db->where('userextensions.UserId ', $userId);
        }
        $this->db->where('customercalls.PhoneNumber > ', 0);
        $this->db->group_by('customercalls.PhoneNumber');
        $this->db->order_by('timeMax', 'desc');
        $query = $this->db->get($this->_table_name);
        return $query->result();
    }

    public function getListCustomer() {
        $this->db->distinct();
        $this->db->select('PhoneNumber');
        $query = $this->db->get($this->_table_name);
        return $query->result();
    }

    public function getListMissCall() {
        $this->db->select('PhoneNumber, max(BeginTime) as timeMax, BeginTime');
        $this->db->where('AnswerTime', '0000-00-00 00:00:00');
        $this->db->where('IsCallAgain', 0);
        $this->db->where('CallType', 1);
        $this->db->where('PhoneNumber > ', 0);
        $this->db->group_by('PhoneNumber');
        $this->db->order_by('timeMax', 'desc');
        $query = $this->db->get($this->_table_name);
        return $query->result();
    }

    public function searchByFilterReportCall($searchText, $itemFilters, $limit, $page, $postData = array()){
        $queryCount = "select customercalls.UserId AS totalRow from customercalls {joins} where {wheres} GROUP BY users.UserId";
        $query = "select {selects} from customercalls {joins} where {wheres} GROUP BY users.UserId ORDER BY customercalls.AnswerTime DESC LIMIT {limits}";
        $selects = [
            'users.FullName',
            'users.UserId',
            'exts.ExtId',
            '(SUM((TIME_TO_SEC(TIMEDIFF(customercalls.EndTime , customercalls.BeginTime)))/60)/count(customercalls.PhoneNumber)) AS AvgTotalMinutes',
            'count(case when customercalls.IsFromCustomer = 0 then 0 end) AS TotalPhoneCall',
            'count(case when customercalls.IsFromCustomer = 1 then 1 end) AS TotalPhoneCore',
            'SUM(if(customercalls.IsFromCustomer = 0, (TIME_TO_SEC(TIMEDIFF(customercalls.EndTime , customercalls.BeginTime)))/60,0)) AS TotalMinutesCall',
            'SUM(if(customercalls.IsFromCustomer = 1, (TIME_TO_SEC(TIMEDIFF(customercalls.EndTime , customercalls.BeginTime)))/60,0)) AS TotalMinutesCore',
            'CONCAT("NV-",users.UserId + 10000) AS UserCode',
            'departments.DepartmentName',
            'exts.UserId AS Branch',
            'customercalls.AnswerTime'
        ];
        $joins = [
            'exts' => "INNER JOIN exts ON exts.UserId = customercalls.UserId",
            'userextensions' => "INNER JOIN userextensions ON userextensions.ExtId = exts.ExtId",
            'users' => "LEFT JOIN users ON userextensions.UserId = users.UserId",
            'userdepartments' => "LEFT JOIN userdepartments ON users.UserId = userdepartments.UserId",
            'departments' => "LEFT JOIN departments ON departments.DepartmentId = userdepartments.DepartmentId",

        ];
        $wheres = array(" customercalls.UserId > 0 AND customercalls.EndTime != '0000-00-00 00:00:00'");
        $dataBind = [];
        $whereSearch= '';
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'customercalls.AnswerTime like ?';
                $dataBind[] = "$searchText%";
            }
            /*elseif(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'transports.TransportCode like ? or orders.OrderCode like ? or transports.CrDateTime like ?';
                for( $i = 0; $i < 3; $i++) $dataBind[] = "%$searchText%";
            }*/
            else{
                $whereSearch = 'users.FullName like ? or departments.DepartmentName like ? ';
                for( $i = 0; $i < 2; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        if(!empty($postData)){
            $wheres[] = 'customercalls.AnswerTime between ? and ?';
            $dataBind[] = @ddMMyyyyToDate($postData['BeginDate']);
            $dataBind[] = @ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                //$cond[0] là điều kiện ví dụ : < > = like .....   $cons[1] và $cond[2]  là gía trị điều kiện như 2017-01-02 và 2017-01-01
                switch ($filed_name) {
                    case 'call_create':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'customercalls.AnswerTime between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "customercalls.AnswerTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "customercalls.AnswerTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(customercalls.AnswerTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    case 'call_tag':
                        $wheres[] = "customercalls.CustomerCallId $conds[0](SELECT ItemId FROM itemtags WHERE ItemTypeId = 1 AND TagId = ?)";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $dataCalls = $this->getByQuery($query, $dataBind);
        for ($i = 0; $i < count($dataCalls); $i++) {
            $dataCalls[$i]['DepartmentName'] = $dataCalls[$i]['DepartmentName'] != NULL ? $dataCalls[$i]['DepartmentName']: $this->Mdepartments->getFieldValue(array('HeadUserId' => $dataCalls[$i]['UserId']), 'DepartmentName');
            //     $dataTransports[$i]['CODStatus'] = $dataTransports[$i]['CODStatusId'] > 0 ? $this->Mconstants->CODStatus[$dataTransports[$i]['CODStatusId']] : '';
            //     $dataTransports[$i]['TransportStatus'] = $dataTransports[$i]['TransportStatusId'] > 0 ? $this->Mconstants->transportStatus[$dataTransports[$i]['TransportStatusId']] : '';
            //     $dayDiff = getDayDiff($dataTransports[$i]['CrDateTime'], $now);
            //     $dataTransports[$i]['CrDateTime'] = ddMMyyyy($dataTransports[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            //     $dataTransports[$i]['DayDiff'] = $dayDiff;
            //     $dataTransports[$i]['labelCss'] = $this->labelCss;
        }
        $data = array();
        $totalRows = $this->getByQuery($queryCount, $dataBind);
        $totalRow = count($totalRows);
        $totalIds = array();
        foreach ($totalRows as $v) $totalIds[] = intval($v['totalRow']);
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $dataCalls;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentCalls';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        $data['totalIds'] = json_encode($totalIds);
        return $data;
    }

    public function getAllExportExcel($userIds){
        $query = "SELECT users.FullName, users.UserId, exts.ExtId,
        (SUM((TIME_TO_SEC(TIMEDIFF(customercalls.EndTime , customercalls.BeginTime)))/60)/count(customercalls.PhoneNumber)) AS AvgTotalMinutes,
        CONCAT('NV-',users.UserId + 10000) AS UserCode,
        departments.DepartmentName,
        exts.UserId AS Branch,
        departments.HeadUserId,
        count(case when customercalls.IsFromCustomer = 0 then 0 end) AS TotalPhoneCall,
        count(case when customercalls.IsFromCustomer = 1 then 1 end) AS TotalPhoneCore,
        SUM(if(customercalls.IsFromCustomer = 0, (TIME_TO_SEC(TIMEDIFF(customercalls.EndTime , customercalls.BeginTime)))/60,0)) AS TotalMinutesCall,
        SUM(if(customercalls.IsFromCustomer = 1, (TIME_TO_SEC(TIMEDIFF(customercalls.EndTime , customercalls.BeginTime)))/60,0)) AS TotalMinutesCore
        FROM `customercalls`
        INNER JOIN exts ON exts.UserId = customercalls.UserId
        INNER JOIN userextensions ON userextensions.ExtId = exts.ExtId
        LEFT JOIN users ON userextensions.UserId = users.UserId
        LEFT JOIN userdepartments ON users.UserId = userdepartments.UserId
        LEFT JOIN departments ON departments.DepartmentId = userdepartments.DepartmentId
        WHERE customercalls.UserId > 0 AND customercalls.EndTime != '0000-00-00 00:00:00' and customercalls.UserId IN(".implode(",",$userIds).")
        GROUP BY users.UserId";
        $dataCalls = $this->getByQuery($query);
        $this->load->model('Mdepartments');
        if(!empty($dataCalls)){
            for ($i = 0; $i < count($dataCalls); $i++) {
                $dataCalls[$i]['DepartmentName'] = $dataCalls[$i]['DepartmentName'] != NULL ? $dataCalls[$i]['DepartmentName']: $this->Mdepartments->getFieldValue(array('HeadUserId' => $dataCalls[$i]['UserId']), 'DepartmentName');
            }
            return $dataCalls;
        }else return "";

    }
}