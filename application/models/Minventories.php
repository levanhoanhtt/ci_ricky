<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Minventories extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "inventories";
        $this->_primary_key = "InventoryId";
    }

    public function getListPending($storeId = 0){
        $query = 'SELECT inventories.*, users.FullName, products.ProductName, products.ProductKindId, products.BarCode, products.ProductStatusId, productchilds.ProductName AS ProductChildName, productchilds.BarCode AS ProductChildBarCode, productquantity.Quantity AS InventoryQuantity
            FROM inventories INNER JOIN products ON products.ProductId = inventories.ProductId
            LEFT JOIN productchilds ON productchilds.ProductChildId = inventories.ProductChildId
            LEFT JOIN productquantity ON (productquantity.ProductId = inventories.ProductId AND productquantity.ProductChildId = inventories.ProductChildId AND productquantity.StoreId = inventories.StoreId)
            LEFT JOIN users ON inventories.CrUserId = users.UserId
            WHERE inventories.StatusId = 1 AND products.ProductStatusId > 0';
        if($storeId > 0) $query .= ' AND inventories.StoreId = '.$storeId;
        return $this->getByQuery($query);
    }

    public function getListJustUpdate($minute = 0){
        if($minute > 0) return $this->getByQuery('SELECT ProductId, ProductChildId, StoreId FROM inventories WHERE StatusId = ? AND UpdateDateTime >= NOW() - INTERVAL ? MINUTE', array(STATUS_ACTIVED, $minute));
        return $this->getByQuery('SELECT ProductId, ProductChildId, StoreId FROM inventories WHERE StatusId = ?', array(STATUS_ACTIVED));
    }

    public function update($postData, $inventoryId = 0){
        $crUserId = isset($postData['UpdateUserId']) ? $postData['UpdateUserId'] : $postData['CrUserId'];
        $crDateTime = isset($postData['UpdateDateTime']) ? $postData['UpdateDateTime'] : $postData['CrDateTime'];
        $this->db->trans_begin();
        $inventoryId = $this->save($postData, $inventoryId, array('UpdateUserId', 'UpdateDateTime'));
        if($inventoryId > 0 && $postData['StatusId'] == STATUS_ACTIVED && $postData['Quantity'] > 0){
            $productQuantityId = $this->Mproductquantity->getFieldValue(array('ProductId' => $postData['ProductId'], 'ProductChildId' => $postData['ProductChildId'], 'StoreId' => $postData['StoreId']), 'ProductQuantityId', 0);
            if($productQuantityId > 0){
                if($postData['InventoryTypeId'] == 1) $this->db->query('UPDATE productquantity SET Quantity = Quantity + ?, UpdateUserId = ?, UpdateDateTime = ? WHERE ProductQuantityId = ?', array($postData['Quantity'], $crUserId, $crDateTime, $productQuantityId));
                elseif($postData['InventoryTypeId'] == 2) $this->db->query('UPDATE productquantity SET Quantity = Quantity - ?, UpdateUserId = ?, UpdateDateTime = ? WHERE ProductQuantityId = ?', array($postData['Quantity'], $crUserId, $crDateTime, $productQuantityId));
            }
            else{
                $quantity = 0;
                if($postData['InventoryTypeId'] == 1) $quantity = $postData['Quantity'];
                elseif($postData['InventoryTypeId'] == 2) $quantity = -$postData['Quantity'];
                $this->Mproductquantity->save(array(
                    'ProductId' => $postData['ProductId'],
                    'ProductChildId' => $postData['ProductChildId'],
                    'Quantity' => $quantity,
                    'StoreId' => $postData['StoreId'],
                    'CrUserId' => $crUserId,
                    'CrDateTime' => $crDateTime
                ), 0, array('UpdateUserId', 'UpdateDateTime'));
            }
        }
        if($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $inventoryId;
        }
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $postData = array()){
        $queryCount = "select inventories.InventoryId AS totalRow from inventories {joins} where {wheres} GROUP BY inventories.InventoryId";
        $query = "select {selects} from inventories {joins} where {wheres} GROUP BY inventories.InventoryId ORDER BY inventories.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'inventories.*',
            'products.ProductName AS ProductName1',
            'products.BarCode AS BarCode1',
            'products.ProductKindId',
            'productchilds.ProductName AS ProductName2',
            'productchilds.BarCode AS BarCode2',
            'stores.StoreName',
            'users.FullName'
        ];
        $joins = [
            'products' => "left join products on products.ProductId = inventories.ProductId",
            'productchilds' => "left join productchilds on productchilds.ProductChildId = inventories.ProductChildId",
            'stores' => "left join stores on stores.StoreId = inventories.StoreId",
            'users' => "left join users on users.UserId = inventories.UpdateUserId"
        ];
        $wheres = array('inventories.StatusId > 0');
        $whereSearch= '';
        $dataBind = [];
        if(isset($postData['StoreIds']) && !empty($postData['StoreIds'])){
            $wheres[] = 'inventories.StoreId IN ?';
            $dataBind[] = $postData['StoreIds'];
        }
        /*if(isset($postData['IsManual']) && $postData['IsManual'] > 0){
            $wheres[] = 'inventories.IsManual = ?';
            $dataBind[] = $postData['IsManual'];
        }*/
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'inventories.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            /*elseif(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'products.BarCode like ? or productchilds.BarCode like ? or inventories.CrDateTime like ?';
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
            }*/
            elseif(strpos("dang kinh doanh",$searchText) > -1 )$whereSearch = 'products.ProductStatusId = 2';
            elseif(strpos('tam dung kinh doanh',$searchText) > -1) $whereSearch = 'products.ProductStatusId = 1';
            elseif(strpos('khong con kinh doanh',$searchText) > -1) $whereSearch = 'products.ProductStatusId = 3';
            else{
                //$whereSearch = 'products.ProductName like ? or categories.CategoryName like ? or producttypes.ProductTypeName like ? or suppliers.SupplierName like ?';
                //for( $i = 0; $i < 4; $i++) $dataBind[] = "%$searchText%";
                $whereSearch = 'products.ProductName like ? or productchilds.ProductName like ? or stores.StoreName like ?';
                for( $i = 0; $i < 3; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                //$cond[0] là điều kiện ví dụ : < > = like .....   $cons[1] và $cond[2]  là gía trị điều kiện như 2017-01-02 và 2017-01-01
                switch ($filed_name) {
                    case 'product_store':
                        $wheres[] = "inventories.StoreId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_status_trade':
                        $wheres[] = "products.ProductStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_status_display':
                        $wheres[] = "products.ProductDisplayTypeId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'inventory_status':
                        $wheres[] = "inventories.StatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'inventory_manual':
                        $wheres[] = "inventories.IsManual $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_type' :
                        $wheres[] = "products.ProductTypeId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_kind':
                        $wheres[] = "products.ProductKindId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_suppliers':
                        $wheres[] = "products.SupplierId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_manufacturer':
                        $wheres[] = "products.ManufacturerId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_group_1':
                    case 'product_group_2':
                        $wheres[] = "categoryitems.CategoryId $conds[0] ? and categoryitems.ItemTypeId = 3";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $crFullNames = array();
        $now = new DateTime(date('Y-m-d'));
        $dataProducts = $this->getByQuery($query, $dataBind);
        for ($i = 0; $i < count($dataProducts); $i++) {
            $dataProducts[$i]['Status'] = $dataProducts[$i]['StatusId'] > 0 ? $this->Mconstants->status[$dataProducts[$i]['StatusId']] : '';
            $dataProducts[$i]['ProductName'] = $dataProducts[$i]['ProductName1'].(empty($dataProducts[$i]['ProductName2']) ? '' : " ({$dataProducts[$i]['ProductName2']})");
            $dataProducts[$i]['BarCode'] = empty($dataProducts[$i]['BarCode2']) ? $dataProducts[$i]['BarCode1'] : $dataProducts[$i]['BarCode2'];
            $dayDiff = getDayDiff($dataProducts[$i]['UpdateDateTime'], $now);
            $dataProducts[$i]['UpdateDateTime'] = empty($dataProducts[$i]['UpdateDateTime']) ? '' : ddMMyyyy($dataProducts[$i]['UpdateDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataProducts[$i]['DayDiff'] = $dayDiff;
            if($dataProducts[$i]['StatusId'] == 1){
                $crDayDiff = getDayDiff($dataProducts[$i]['CrDateTime'], $now);
                $dataProducts[$i]['CrDateTime'] = ddMMyyyy($dataProducts[$i]['CrDateTime'], $crDayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
                $dataProducts[$i]['CrDayDiff'] = $crDayDiff;
                if(!isset($crFullNames[$dataProducts[$i]['CrUserId']])) $crFullNames[$dataProducts[$i]['CrUserId']] = $this->Musers->getFieldValue(array('UserId' => $dataProducts[$i]['CrUserId']), 'FullName');
                $dataProducts[$i]['CrFullName'] = $crFullNames[$dataProducts[$i]['CrUserId']];
            }
            $dataProducts[$i]['labelCss'] = $this->Mconstants->labelCss;
        }
        $data = array();
        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $dataProducts;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentProducts';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }
}