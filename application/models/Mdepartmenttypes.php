<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdepartmenttypes extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "departmenttypes";
        $this->_primary_key = "DepartmentTypeId";
    }

    public function getListByUserId($userId){
        return $this->getByQuery('SELECT DepartmentTypeId, DepartmentTypeName FROM departmenttypes WHERE StatusId = ? AND (UserId = 0 OR UserId = ?)', array(STATUS_ACTIVED, $userId));
    }

    public function checkDepartmentTypeExist($departmentTypeId, $departmentTypeName, $userId){
        $users = $this->getByQuery("SELECT DepartmentTypeId FROM departmenttypes 
                              WHERE DepartmentTypeId != ? AND StatusId = ? AND DepartmentTypeName = ? AND (UserId = 0 OR UserId = ?)", array($departmentTypeId, STATUS_ACTIVED, $departmentTypeName, $userId));
        if (!empty($users)) return true;
        return false;
    }
}