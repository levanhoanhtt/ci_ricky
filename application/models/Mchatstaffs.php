<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mchatstaffs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "chatstaffs";
        $this->_primary_key = "ChatStaffId";
    }

    public function search($postData, $perPage = 0, $from){
        $query = "SELECT * FROM chatstaffs WHERE 1 = 1" . $this->buildQuery($postData);
        if($perPage > 0) $query .= " LIMIT {$from}, {$perPage}";
        return $this->getByQuery($query);
    }

    private function buildQuery($postData, $orderBy = true){
        $query = '';
        if(isset($postData['CustomerId']) && $postData['CustomerId'] > 0) $query.=" AND CustomerId=".$postData['CustomerId'];
        if(isset($postData['StaffId']) && $postData['StaffId'] > 0) $query.=" AND StaffId=".$postData['StaffId'];
        if(isset($postData['StaffRoleId']) && $postData['StaffRoleId'] > 0) $query.=" AND StaffRoleId=".$postData['StaffRoleId'];
        if(isset($postData['PhoneNumber']) && !empty($postData['PhoneNumber'])) $query.=" AND PhoneNumber='{$postData['PhoneNumber']}'";
        if(isset($postData['IsCustomerSend']) && $postData['IsCustomerSend'] >= 0) $query.=" AND IsCustomerSend=".$postData['IsCustomerSend'];
        if(isset($postData['IsRead']) && $postData['IsRead'] >= 0) $query.=" AND IsRead=".$postData['IsRead'];
        if($orderBy) $query .= " ORDER BY ChatStaffId DESC";
        return $query;
    }

    public function getLastedChat($staffId = 0, $staffRoleId = 0, $phoneNumber = ''){
        $retVal = array();
        if($staffId > 0){
            if($staffRoleId > 0) $retVal = $this->getByQuery("SELECT * FROM chatstaffs WHERE ChatStaffId IN(SELECT MAX(ChatStaffId) FROM chatstaffs WHERE StaffId = ? AND StaffRoleId = ? GROUP BY PhoneNumber, StaffId, StaffRoleId) ORDER BY ChatStaffId DESC", array($staffId, $staffRoleId));
            else $retVal = $this->getByQuery("SELECT * FROM chatstaffs WHERE ChatStaffId IN(SELECT MAX(ChatStaffId) FROM chatstaffs WHERE StaffId = ? GROUP BY PhoneNumber, StaffId) ORDER BY ChatStaffId DESC", array($staffId));
        }
        elseif(!empty($phoneNumber)){
            if($staffRoleId > 0) $retVal = $this->getByQuery("SELECT * FROM chatstaffs WHERE ChatStaffId IN(SELECT MAX(ChatStaffId) FROM chatstaffs WHERE PhoneNumber = ? AND StaffRoleId = ? GROUP BY PhoneNumber, StaffId) ORDER BY ChatStaffId DESC", array($phoneNumber, $staffRoleId));
            else $retVal = $this->getByQuery("SELECT * FROM chatstaffs WHERE ChatStaffId IN(SELECT MAX(ChatStaffId) FROM chatstaffs WHERE PhoneNumber = ? GROUP BY PhoneNumber, StaffId) ORDER BY ChatStaffId DESC", array($phoneNumber));
        }
        return $retVal;
    }

    public function updateReadMessage($phoneNumber, $staffId, $isStaff = true){
        if($isStaff) $query = "UPDATE chatstaffs SET IsRead = 1 WHERE PhoneNumber = ? AND StaffId = ? AND IsRead = 0 AND IsCustomerSend = 0";
        else $query = "UPDATE chatstaffs SET IsRead = 1 WHERE PhoneNumber = ? AND StaffId = ? AND IsRead = 0 AND IsCustomerSend = 1";
        $this->db->query($query, array($phoneNumber, $staffId));
    }
}