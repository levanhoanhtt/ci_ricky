<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransports extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transports";
        $this->_primary_key = "TransportId";
    }

    public $labelCss = array(
        'TransportStatusCss' => array(
            1 => 'label lable-grey',
            2 => 'label lable-yellow',
            3 => 'label lable-yellow',
            4 => 'label lable-green',
            5 => 'label lable-red',
            6 => 'label lable-yellow',
            7 => 'label lable-red',
            8 => 'label lable-red',
            9 => 'label lable-grey'
        ),
        'CODStatusCss' => array(
            1 => 'label lable-orange',
            2 => 'label lable-green',
            3 => 'label lable-red',
            4 => 'label lable-green',
            5 => 'label lable-yellow'
        )
    );

    public function getCount($orderId){
        $query = "OrderId = ".$orderId;
        return $this->countRows($query);
    }

    public function update($postData, $transportId, $tagNames = array(), $actionLog = array(), $metaData = array()){
        $this->load->model('Mtags');
        $this->load->model('Mtransactions');
        $this->load->model('Morderpromotions');
        $this->load->model('Minventories');
        $this->load->model('Mtransportstatuslogs');
        $itemTypeId = 9;
        $isUpdate = $transportId > 0;
        $actionLogs = array();
        $transportCount = 0;
        if(!$isUpdate) $transportCount = $this->countRows(array('OrderId' => $postData['OrderId']));
        $this->db->trans_begin();
        $transportId = $this->save($postData, $transportId, array('UpdateUserId', 'UpdateDateTime'));
        if($transportId > 0){
            if(!empty($actionLog)){
                $actionLog['ItemId'] = $transportId;
                $actionLogs[] = $actionLog;
            }
            if($isUpdate) $this->db->delete('itemtags', array('ItemId' => $transportId, 'ItemTypeId' => $itemTypeId));
            else{
                $transportCode =  'VC-' . ($postData['OrderId'] + 10000);
                if($transportCount > 0) $transportCode .= '-L'.($transportCount + 1);
                $this->db->update('transports', array('TransportCode' => $transportCode), array('TransportId' => $transportId));
                $this->db->update('orders', array('OrderStatusId' => 2, 'StoreId' => $postData['StoreId'], 'OrderReasonId' => $metaData['OrderReasonId'], 'OrderChanelId' => $metaData['OrderChanelId'], 'UpdateUserId' => $postData['CrUserId'], 'UpdateDateTime' => $postData['CrDateTime']), array('OrderId' => $postData['OrderId']));
                $actionLogs[] = array(
                    'ItemId' => $postData['OrderId'],
                    'ItemTypeId' => 6,
                    'ActionTypeId' => 2,
                    'Comment' => $metaData['FullName'] . ' chốt đơn hàng',
                    'CrUserId' => $postData['CrUserId'],
                    'CrDateTime' => $postData['CrDateTime']
                );
                if($metaData['PaymentCost'] > 0){
                    $transaction = array(
                        'TransactionTypeId' => 2,
                        'TransactionStatusId' => STATUS_ACTIVED,
                        'MoneySourceId' => 5,
                        'MoneyPhoneId' => 0,
                        'VerifyLevelId' => 3,
                        'StoreId' => $postData['StoreId'],
                        'TransactionReasonId' => 0,
                        'CustomerId' => $postData['CustomerId'],
                        'BankId' => 0,
                        'PaidCost' => $metaData['PaymentCost'],
                        'Comment' => 'Thanh toán đơn hàng '.$metaData['OrderCode'],
                        'OrderId' => $postData['OrderId'],
                        'PrintStatusId' => 1,
                        'HasDebt' => 1,
                        'DebtComment' => '',
                        'TransportId' => $transportId,
                        'FundId' => 0,
                        'TransactionKindId' => 0,
                        'CrUserId' => $postData['CrUserId'],
                        'CrDateTime' => $postData['CrDateTime']
                    );
                    $transactionId = $this->Mtransactions->update($transaction);
                    if($transactionId > 0){
                        $actionLogs[] = array(
                            'ItemId' => $transactionId,
                            'ItemTypeId' => 18,
                            'ActionTypeId' => 1,
                            'Comment' => $metaData['FullName'] . ' thêm phiếu chi thanh toán đơn hàng '.$metaData['OrderCode'],
                            'CrUserId' => $postData['CrUserId'],
                            'CrDateTime' => $postData['CrDateTime']
                        );
                    }
                }
                if($metaData['DebitCost'] > 0){
                    $actionLogs[] = array(
                        'ItemId' => $postData['OrderId'],
                        'ItemTypeId' => 6,
                        'ActionTypeId' => 1,
                        'Comment' => $metaData['FullName'] . ' thêm phiếu ghi nợ đơn hàng '.$metaData['OrderCode'],
                        'CrUserId' => $postData['CrUserId'],
                        'CrDateTime' => $postData['CrDateTime']
                    );
                    $transaction = array(
                        'TransactionTypeId' => 2,
                        'TransactionStatusId' => STATUS_ACTIVED,
                        'MoneySourceId' => 5,
                        'MoneyPhoneId' => 0,
                        'VerifyLevelId' => 3,
                        'StoreId' => $postData['StoreId'],
                        'TransactionReasonId' => 0,
                        'CustomerId' => $postData['CustomerId'],
                        'BankId' => 0,
                        'PaidCost' => $metaData['DebitCost'],
                        'OrderId' => $postData['OrderId'],
                        'Comment' => 'Ghi nợ đơn hàng '.$metaData['OrderCode'],
                        'PrintStatusId' => 1,
                        'HasDebt' => 1,
                        'DebtComment' => '',
                        'TransportId' => $transportId,
                        'FundId' => 0,
                        'TransactionKindId' => 0,
                        'CrUserId' => $postData['CrUserId'],
                        'CrDateTime' => $postData['CrDateTime']
                    );
                    $transactionId = $this->Mtransactions->update($transaction);
                    if($transactionId > 0){
                        $actionLogs[] = array(
                            'ItemId' => $transactionId,
                            'ItemTypeId' => 18,
                            'ActionTypeId' => 1,
                            'Comment' => $metaData['FullName'] . ' thêm phiếu ghi nợ đơn hàng '.$metaData['OrderCode'],
                            'CrUserId' => $postData['CrUserId'],
                            'CrDateTime' => $postData['CrDateTime']
                        );
                    }
                }
                //ma Khuyen mai
                $promotionId = $this->Morderpromotions->getFieldValue(array('OrderId' => $postData['OrderId'], 'StatusId' => 1), 'PromotionId', 0);
                if($promotionId > 0){
                    $this->db->query('UPDATE orderpromotions SET StatusId = 2 WHERE PromotionId = ?', array($promotionId));
                    $this->db->query('UPDATE promotions SET NumberUse = NumberUse - 1 WHERE PromotionId = ? AND IsUnLimit = 1', array($promotionId));
                }
                //tru so luong ton kho
                if(!empty($metaData['InventoryData'])){
                    foreach($metaData['InventoryData'] as $id) $this->Minventories->update($id);
                }
                if(!empty($metaData['TransportStatusLog'])){
                    $metaData['TransportStatusLog']['TransportId'] = $transportId;
                    $this->Mtransportstatuslogs->save($metaData['TransportStatusLog']);
                }
            }
            if(!empty($tagNames)){
                $itemTags = array();
                foreach($tagNames as $tagName){
                    $tagId = $this->Mtags->getTagId($tagName, $itemTypeId);
                    if($tagId > 0){
                        $itemTags[] = array(
                            'ItemId' => $transportId,
                            'ItemTypeId' => $itemTypeId,
                            'TagId' => $tagId
                        );
                    }
                }
                if(!empty($itemTags)) $this->db->insert_batch('itemtags', $itemTags);
            }
            if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $transportId;
        }
    }

    public function updateField($postData, $transportId, $actionLog = array(), $metaData = array()){
        $actionLogs = array();
        $this->db->trans_begin();
        $transportId = $this->save($postData, $transportId);
        if($transportId > 0){
            if(!empty($actionLog)) $actionLogs[] = $actionLog;
            $crDateTime = $postData['UpdateDateTime'];
            if(isset($postData['StoreId'])){
                $this->db->update('orders', array('StoreId' => $postData['StoreId']), array('OrderId' => $metaData['OrderId']));
                $actionLogs[] = array(
                    'ItemId' => $metaData['OrderId'],
                    'ItemTypeId' => 6,
                    'ActionTypeId' => 2,
                    'Comment' => $metaData['FullName'] . ' cập nhật cơ sở thành '.$metaData['StoreName'].' từ vận chuyển',
                    'CrUserId' => $metaData['UserId'],
                    'CrDateTime' => $crDateTime
                );
                //cong trừ so luong sp do đỏi kho
                if(!empty($metaData['InventoryData'])){
                    foreach($metaData['InventoryData'] as $id) $this->Minventories->update($id);
                }
            }
            if(isset($postData['TransportStatusId'])){
                if($postData['TransportStatusId'] == 2){ //dong hang
                    if(isset($metaData['TransportImages']) && !empty($metaData['TransportImages'])) $this->db->insert_batch('transportimages', $metaData['TransportImages']);
                }
                elseif($postData['TransportStatusId'] == 4){ //Đã giao hàng
                    //don hang thANH CONG
                    $this->db->update('orders', array('OrderStatusId' => 6, 'UpdateUserId' => $metaData['UserId'], 'UpdateDateTime' => $crDateTime), array('OrderId' => $metaData['OrderId']));
                    $actionLogs[] = array(
                        'ItemId' => $metaData['OrderId'],
                        'ItemTypeId' => 6,
                        'ActionTypeId' => 2,
                        'Comment' => $metaData['FullName'] . ' chuyển trạng thái đơn hàng thành THÀNH CÔNG',
                        'CrUserId' => $metaData['UserId'],
                        'CrDateTime' => $crDateTime
                    );
                }
                elseif($postData['TransportStatusId'] == 5 || $postData['TransportStatusId'] == 7) { //huy hoac chuyen hoan thanh cong
                    $this->load->model('Mtransactions');
                    $this->load->model('Morderpromotions');
                    $transaction = $this->Mtransactions->getBy(array('TransportId' => $transportId, 'TransactionTypeId' => 2, 'TransactionStatusId >' => 0), true, '', 'TransactionId, CustomerId, StoreId, TransactionStatusId');
                    if($transaction){
                        if($transaction['TransactionStatusId'] == STATUS_ACTIVED){ //tao phieu thu moi bu vao
                            $paidCost = $this->Mtransactions->getTotalPaidCost($transportId, 2);
                            $transactionNew = array(
                                'TransactionTypeId' => 1,
                                'TransactionStatusId' => STATUS_ACTIVED,
                                'MoneySourceId' => 5,
                                'MoneyPhoneId' => 0,
                                'VerifyLevelId' => 3,
                                'StoreId' => $transaction['StoreId'],
                                'TransactionReasonId' => 0,
                                'CustomerId' => $transaction['CustomerId'],
                                'BankId' => 0,
                                'PaidCost' => $paidCost,
                                'OrderId' => $metaData['OrderId'],
                                'Comment' => 'Hoàn lại tiền đơn hàng '.$metaData['OrderCode'].' do vận chuyển bị hủy bỏ',
                                'PrintStatusId' => 1,
                                'HasDebt' => 1,
                                'DebtComment' => '',
                                'TransportId' => $transportId,
                                'FundId' => 0,
                                'TransactionKindId' => 0,
                                'CrUserId' => $metaData['UserId'],
                                'CrDateTime' => $crDateTime
                            );
                            $transactionId = $this->Mtransactions->update($transactionNew);
                            $actionLogs[] = array(
                                'ItemId' => $transactionId,
                                'ItemTypeId' => 17,
                                'ActionTypeId' => 1,
                                'Comment' => $metaData['FullName'] . ' hoàn lại tiền đơn hàng '.$metaData['OrderCode'].' do vận chuyển bị hủy bỏ',
                                'CrUserId' => $metaData['UserId'],
                                'CrDateTime' => $crDateTime
                            );
                        }
                        else{ //huy phieu
                            $this->Mtransactions->changeStatus(3, $transaction['TransactionId'], 'TransactionStatusId', $metaData['UserId']);
                            $actionLogs[] = array(
                                'ItemId' => $transaction['TransactionId'],
                                'ItemTypeId' => 17,
                                'ActionTypeId' => 3,
                                'Comment' => $metaData['FullName'].' hủy phiếu do vận chuyển của đơn hàng '.$metaData['OrderCode'].' bị hủy bỏ',
                                'CrUserId' => $metaData['UserId'],
                                'CrDateTime' => $crDateTime
                            );
                        }
                    }
                    //don hang thất bai
                    $this->db->update('orders', array('OrderStatusId' => 5, 'UpdateUserId' => $metaData['UserId'], 'UpdateDateTime' => $crDateTime), array('OrderId' => $metaData['OrderId']));
                    $actionLogs[] = array(
                        'ItemId' => $metaData['OrderId'],
                        'ItemTypeId' => 6,
                        'ActionTypeId' => 2,
                        'Comment' => $metaData['FullName'] . ' chuyển trạng thái đơn hàng thành THẤT BẠI',
                        'CrUserId' => $metaData['UserId'],
                        'CrDateTime' => $crDateTime
                    );
                    //ma Khuyen mai
                    $promotionId = $this->Morderpromotions->getFieldValue(array('OrderId' => $metaData['OrderId'], 'StatusId' => STATUS_ACTIVED), 'PromotionId', 0);
                    if($promotionId > 0) {
                        $this->db->update('orderpromotions', array('StatusId' => 1), array('OrderId' => $metaData['OrderId']));
                        $this->db->query('UPDATE promotions SET NumberUse = NumberUse + 1 WHERE PromotionId = ? AND IsUnLimit = 1', array($promotionId));
                    }
                    //cong lai so luong sp do don hang bi huy
                    if(!empty($metaData['InventoryData'])){
                        foreach($metaData['InventoryData'] as $id) $this->Minventories->update($id);
                    }
                }
                if(!empty($metaData['TransportStatusLog'])){
                    $this->load->model('Mtransportstatuslogs');
                    $this->Mtransportstatuslogs->save($metaData['TransportStatusLog']);
                }
            }
            if(isset($postData['CODStatusId'])){
                if($postData['CODStatusId'] == 2){ //da thu khach
                    //tao phieu thu - chi KH | thu da ap dung nhng chua duyet
                    $this->load->model('Mtransactions');
                    //phieu chi
                    $transactionNew = array(
                        'TransactionTypeId' => 2,
                        'TransactionStatusId' => STATUS_ACTIVED,
                        'MoneySourceId' => 0,
                        'MoneyPhoneId' => 0,
                        'VerifyLevelId' => 3,
                        'StoreId' => $metaData['StoreId'],
                        'TransactionReasonId' => 0,
                        'CustomerId' => $metaData['CustomerId'],
                        'BankId' => 0,
                        'PaidCost' => $metaData['CODCost'],
                        'OrderId' => $metaData['OrderId'],
                        'Comment' => 'Chi tiền COD đơn hàng '.$metaData['OrderCode'],
                        'PrintStatusId' => 1,
                        'HasDebt' => 1,
                        'DebtComment' => '',
                        'TransportId' => $transportId,
                        'FundId' => 0,
                        'TransactionKindId' => 0,
                        'CrUserId' => $metaData['UserId'],
                        'CrDateTime' => $crDateTime
                    );
                    $transactionId = $this->Mtransactions->update($transactionNew);
                    $actionLogs[] = array(
                        'ItemId' => $transactionId,
                        'ItemTypeId' => 18,
                        'ActionTypeId' => 1,
                        'Comment' => $metaData['FullName'] . ' tạo phiếu chi tiền COD đơn hàng '.$metaData['OrderCode'],
                        'CrUserId' => $metaData['UserId'],
                        'CrDateTime' => $crDateTime
                    );
                    //phieu thu
                    $transactionNew['TransactionTypeId'] = 1;
                    $transactionNew['VerifyLevelId'] = 2;
                    $transactionNew['Comment'] = 'Thu tiền COD đơn hàng '.$metaData['OrderCode'];
                    $transactionId = $this->Mtransactions->update($transactionNew);
                    $actionLogs[] = array(
                        'ItemId' => $transactionId,
                        'ItemTypeId' => 17,
                        'ActionTypeId' => 1,
                        'Comment' => $metaData['FullName'] . ' tạo phiếu thu tiền COD đơn hàng '.$metaData['OrderCode'],
                        'CrUserId' => $metaData['UserId'],
                        'CrDateTime' => $crDateTime
                    );
                }
            }
            if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function changeStatusBatch($transportIds, $transportStatusId, $user){
        $statusName = $this->Mconstants->transportStatus[$transportStatusId];
        $actionLogs = array();
        $transportStatusLogs = array();
        $crDateTime = getCurentDateTime();
        $this->db->trans_begin();
        $this->db->query('UPDATE transports SET TransportStatusId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE TransportId IN ?', array($transportStatusId, $user['UserId'], $crDateTime, $transportIds));
        foreach($transportIds as $transportId) {
            $actionLogs[] = array(
                'ItemId' => $transportId,
                'ItemTypeId' => 9,
                'ActionTypeId' => 2,
                'Comment' => $user['FullName'] . ' cập nhật trạng thái vận chuyển thành '.$statusName,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
            $transportStatusLogs[] = array(
                'TransportId' => $transportId,
                'TransportStatusId' => $transportStatusId,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
        }
        if($transportStatusId == 4){ //Đã giao hàng
            foreach($transportIds as $transportId) {
                //don hang thANH CONG
                $orderId = $this->getFieldValue(array('TransportId' => $transportId), 'OrderId', 0);
                if($orderId > 0) {
                    $this->db->update('orders', array('OrderStatusId' => 6, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime), array('OrderId' => $orderId));
                    $actionLogs[] = array(
                        'ItemId' => $orderId,
                        'ItemTypeId' => 6,
                        'ActionTypeId' => 2,
                        'Comment' => $user['FullName'] . ' chuyển trạng thái đơn hàng thành THÀNH CÔNG',
                        'CrUserId' => $user['UserId'],
                        'CrDateTime' => $crDateTime
                    );
                }
            }
        }
        elseif($transportStatusId == 5 || $transportStatusId == 7) { //huy hoac chuyen hoan thanh cong
            $this->load->model('Mtransactions');
            $this->load->model('Morders');
            $this->load->model('Morderpromotions');
            $this->load->model('Morderproducts');
            $this->load->model('Mproductquantity');
            $this->load->model('Minventories');
            foreach($transportIds as $transportId) {
                $transport = $this->get($transportId, true, '', 'OrderId, StoreId');
                if($transport){
                    $orderCode = $this->Morders->genOrderCode($transport['OrderId']);
                    $transaction = $this->Mtransactions->getBy(array('TransportId' => $transportId, 'TransactionTypeId' => 2, 'TransactionStatusId >' => 0), true, '', 'TransactionId, CustomerId, StoreId, TransactionStatusId');
                    if ($transaction) {
                        if ($transaction['TransactionStatusId'] == STATUS_ACTIVED) { //tao phieu thu moi bu vao
                            $paidCost = $this->Mtransactions->getTotalPaidCost($transportId, 2);
                            $transactionNew = array(
                                'TransactionTypeId' => 1,
                                'TransactionStatusId' => STATUS_ACTIVED,
                                'MoneySourceId' => 5,
                                'MoneyPhoneId' => 0,
                                'VerifyLevelId' => 3,
                                'StoreId' => $transaction['StoreId'],
                                'TransactionReasonId' => 0,
                                'CustomerId' => $transaction['CustomerId'],
                                'BankId' => 0,
                                'PaidCost' => $paidCost,
                                'OrderId' => $transaction['OrderId'],
                                'Comment' => 'Hoàn lại tiền đơn hàng ' . $orderCode . ' do vận chuyển bị hủy bỏ',
                                'PrintStatusId' => 1,
                                'HasDebt' => 1,
                                'DebtComment' => '',
                                'TransportId' => $transportId,
                                'FundId' => 0,
                                'TransactionKindId' => 0,
                                'CrUserId' => $user['UserId'],
                                'CrDateTime' => $crDateTime
                            );
                            $transactionId = $this->Mtransactions->update($transactionNew);
                            $actionLogs[] = array(
                                'ItemId' => $transactionId,
                                'ItemTypeId' => 18,
                                'ActionTypeId' => 1,
                                'Comment' => $user['FullName'] . ' hoàn lại tiền đơn hàng ' . $orderCode . ' do vận chuyển bị hủy bỏ',
                                'CrUserId' => $user['UserId'],
                                'CrDateTime' => $crDateTime
                            );
                        }
                        else { //huy phieu
                            $this->Mtransactions->changeStatus(3, $transaction['TransactionId'], 'TransactionStatusId', $user['UserId']);
                            $actionLogs[] = array(
                                'ItemId' => $transaction['TransactionId'],
                                'ItemTypeId' => 17,
                                'ActionTypeId' => 3,
                                'Comment' => $user['FullName'] . ' hủy phiếu do vận chuyển của đơn hàng ' . $orderCode . ' bị hủy bỏ',
                                'CrUserId' => $user['UserId'],
                                'CrDateTime' => $crDateTime
                            );
                        }
                    }
                    //don hang thất bai
                    $this->db->update('orders', array('OrderStatusId' => 5, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime), array('OrderId' => $transport['OrderId']));
                    $actionLogs[] = array(
                        'ItemId' => $transport['OrderId'],
                        'ItemTypeId' => 6,
                        'ActionTypeId' => 2,
                        'Comment' => $user['FullName'] . ' chuyển trạng thái đơn hàng thành THẤT BẠI',
                        'CrUserId' => $user['UserId'],
                        'CrDateTime' => $crDateTime
                    );
                    //ma Khuyen mai
                    $promotionId = $this->Morderpromotions->getFieldValue(array('OrderId' => $transport['OrderId'], 'StatusId' => STATUS_ACTIVED), 'PromotionId', 0);
                    if ($promotionId > 0) {
                        $this->db->update('orderpromotions', array('StatusId' => 1), array('OrderId' => $transport['OrderId']));
                        $this->db->query('UPDATE promotions SET NumberUse = NumberUse + 1 WHERE PromotionId = ? AND IsUnLimit = 1', array($promotionId));
                    }
                    //cong lai so luong sp do don hang bi huy
                    $listOrderProducts = $this->Morderproducts->getBy(array('OrderId' => $transport['OrderId']));
                    foreach($listOrderProducts as $op){
                        $this->Minventories->update(array(
                            'ProductId' => $op['ProductId'],
                            'ProductChildId' => $op['ProductChildId'],
                            'OldQuantity' => $this->Mproductquantity->getQuantity($op['ProductId'], $op['ProductChildId'], $transport['StoreId']),
                            'Quantity' => $op['Quantity'],
                            'InventoryTypeId' => 1,
                            'StoreId' => $transport['StoreId'],
                            'StatusId' => STATUS_ACTIVED,
                            'Comment' => 'Cộng số lượng từ đơn hàng '.$orderCode,
                            'CrUserId' => $user['UserId'],
                            'CrDateTime' => $crDateTime,
                            'UpdateUserId' => $user['UserId'],
                            'UpdateDateTime' => $crDateTime
                        ));
                    }
                }
            }
        }
        if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        if(!empty($transportStatusLogs)) $this->db->insert_batch('transportstatuslogs', $transportStatusLogs);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function getByOrderCode($orderCode){
        $t =  $this->getByQuery('SELECT TransportId, TransportStatusId FROM transports INNER JOIN orders ON transports.OrderId = orders.OrderId WHERE OrderCode = ? LIMIT 1', array($orderCode));
        if(!empty($t)) return $t[0];
        return false;
    }

    public function getOrderProduct($transportId){
        $sql = "SELECT  orderproducts.ProductId as ProductId,
                        products.ProductName as ProductName,
                        products.ProductImage as ProductImage,
                        SUM(orderproducts.Quantity) as Quantity, 
                        orderproducts.Price as Price,
                        transports.CustomerId as CustomerId,
                        count(orderproducts.OrderId) as OrderId

        FROM `transports` 
        LEFT JOIN orderproducts ON transports.OrderId = orderproducts.OrderId
        LEFT JOIN products ON products.ProductId = orderproducts.ProductId
        WHERE transports.TransportId = '".$transportId."'
        GROUP BY orderproducts.ProductId";
        return $this->getByQuery($sql);
    }

    public function getByIds($ids){
        return $this->getByQuery('SELECT * FROM transports  WHERE TransportId IN ?', array($ids));
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $storeIds = array(), $userId){
        $queryCount = "select transports.TransportId AS totalRow from transports {joins} where {wheres}";
        $query = "select {selects} from transports {joins} where {wheres} ORDER BY transports.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'transports.*',
            'orders.OrderCode',
            'customers.FullName',
            'customers.CustomerId',
            'transporters.TransporterName',
            'stores.StoreName'
        ];
        $joins = [
            'orders' => "left join orders on orders.OrderId = transports.OrderId",
            'customers' => "left join customers on customers.CustomerId = transports.CustomerId",
            'transporters' => "left join transporters on transporters.TransporterId = transports.TransporterId",
            'stores' => "left join stores on stores.StoreId = transports.StoreId",

        ];
        $wheres = array('TransportStatusId > 0');
        $dataBind = [];
        if(!empty($storeIds)){
            if(count($storeIds) == 1){
                $wheres[] = 'transports.StoreId = ?';
                $dataBind[] = $storeIds[0];
            }
            else {
                $wheres[] = 'transports.StoreId IN ?';
                $dataBind[] = $storeIds;
            }
        }
        $whereSearch= '';
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'transports.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            /*elseif(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'transports.TransportCode like ? or orders.OrderCode like ? or transports.CrDateTime like ?';
                for( $i = 0; $i < 3; $i++) $dataBind[] = "%$searchText%";
            }*/
            else{
                $whereSearch = 'transports.TransportCode like ? or orders.OrderCode like ? or customers.FullName like ? or customers.PhoneNumber like ? or customers.PhoneNumber2 like ?';
                for( $i = 0; $i < 5; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                //$cond[0] là điều kiện ví dụ : < > = like .....   $cons[1] và $cond[2]  là gía trị điều kiện như 2017-01-02 và 2017-01-01
                switch ($filed_name) {
                    case 'transport_store':
                        $wheres[] = "transports.StoreId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'transport_status':
                        $wheres[] = "transports.TransportStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'transport_status_cod':
                        $wheres[] = "transports.CODStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'transport_create':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'transports.CrDateTime between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "transports.CrDateTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "transports.CrDateTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(transports.CrDateTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    case 'transport_transport_type':
                        $wheres[] = "transports.TransportTypeId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'transport_transporter':
                        $wheres[] = "transports.TransporterId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'transport_day_process':
                        $wheres[] = "DATEDIFF(NOW(), transports.CrDateTime) $conds[0] ?";
                        $dataBind[] = replacePrice($conds[1]);
                        break;
                    case 'transport_tag':
                        $wheres[] = "transports.TransportId $conds[0](SELECT ItemId FROM itemtags WHERE ItemTypeId = 9 AND TagId = ?)";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));

        $dataConfigTable = $this->Mconfigtables->genHtmlThTable('transport', $userId);
        
        $dataTransports = $this->getByQuery($query, $dataBind);
        $html = '';
        $trClass = '';
        $labelCss = $this->labelCss;
        $sumCODCost = 0;
        for ($i = 0; $i < count($dataTransports); $i++) {
            $sumCODCost += intval($dataTransports[$i]['CODCost']);
            $tables = $dataConfigTable['tableTh'];
            $trClass = '';
            if($dataTransports[$i]['TransportStatusId'] == '5') $trClass = ' trItemCancel';
            $html .= '<tr id="trItem_'.$dataTransports[$i]['TransportId'].'" class="dnd-moved trItem '.$trClass.'">';
            for ($z = 0; $z < count($tables); $z++) {
                $columnName = $tables[$z]['ColumnName'];
                if($columnName == 'Check'){
                    $html .= '<td class="th_fix_width"><a href="javascript:void(0);" transport-id="'.$dataTransports[$i]['TransportId'].'" class="treetable_open fa fa-angle-right  parent_all" style="margin-right: 9px;"></a><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="'.$dataTransports[$i]['TransportId'].'"></td>';
                } else if(!empty($tables[$z]['Status']) && $tables[$z]['Status'] != NULL){
                    $arrStatus = $this->Mconstants->$columnName;
                    $nameStatus = $dataTransports[$i][$tables[$z]['Status']] > 0 ? $arrStatus[$dataTransports[$i][$tables[$z]['Status']]] : '';
                    if($tables[$z]['Status'] == 'TransportStatusId'){
                        
                        $html .= '<td class="text-center" id="tdStatus_'.$dataTransports[$i]['TransportId'].'"><span class="'.$labelCss['TransportStatusCss'][$dataTransports[$i]['TransportStatusId']].'">' . $nameStatus . '</span></td>';
                    }else{
                        $html .= '<td class="text-center"><span class="'.$labelCss['CODStatusCss'][$dataTransports[$i][$tables[$z]['Status']]].'">' . $nameStatus . '</span></td>';
                    }
                } else if($columnName == 'CrDateTime'){
                    $dayDiff = getDayDiff($dataTransports[$i][$columnName], $now);
                    $crDateTime = ddMMyyyy($dataTransports[$i][$columnName], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
                    $html .= '<td>'.getDayDiffText($dayDiff).$crDateTime.'</td>';
                }else if(!empty($tables[$z]['Edit']) && !empty($tables[$z]['IdEdit'])){
                    $html .= '<td><a href="'.base_url().$tables[$z]['Edit'].$dataTransports[$i][$tables[$z]['IdEdit']].'">'.$dataTransports[$i][$columnName].'</a></td>';
                }else if(!empty($tables[$z]['Number'])) {
                    $html .= '<td>'.priceFormat($dataTransports[$i][$columnName]).'</td>';
                }else{
                    $html .= '<td>'.$dataTransports[$i][$columnName].'</td>';
                }
              
            }
            $html .= '<tr class="chose-all" style="display:none;" id="child_'.$dataTransports[$i]['TransportId'].'"><td colspan="10" id="content_data_detail_'.$dataTransports[$i]['TransportStatusId'].'"></td></tr>';
        }
        $htmlPaginate = '';
        // if($html != '') $html += '<tr><td colspan="8"></td><td class="text-right">'.priceFormat($sumCODCost).'</td><td></td></tr>';
        
        $data = array();
        $totalRows = $this->getByQuery($queryCount, $dataBind);
        $totalRow = count($totalRows);
        $totalIds = array();
        foreach ($totalRows as $v) $totalIds[] = intval($v['totalRow']);
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = array('htmlTableTh' => $dataConfigTable['htmlTableTh'], 'dataTransports' => $html);
        $data['page'] = $page;
        $data['limit'] = $limit;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentTransports';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        $data['totalIds'] = json_encode($totalIds);
        $data['totalDataShow'] = count($dataTransports);
        return $data;
    }
}