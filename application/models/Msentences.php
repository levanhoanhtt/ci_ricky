<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msentences extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "sentences";
        $this->_primary_key = "SentenceId";
    }

    public function checkExist($sentenceTitle, $librarySentenceId, $sentenceId = 0){
    	$sql = "SELECT SentenceId FROM sentences WHERE SentenceTitle = ? AND LibrarySentenceId = ?  AND SentenceId != ?";
    	$stc = $this->getByQuery($sql, array($sentenceTitle, $librarySentenceId, $sentenceId));
        return empty($stc);
    }

    public function getCount($postData){
        $query = "1=1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM sentences WHERE 1=1" . $this->buildQuery($postData);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['SentenceGroupId']) && $postData['SentenceGroupId'] > 0) $query.=" AND SentenceGroupId=".$postData['SentenceGroupId'];
        if(isset($postData['LibrarySentenceId']) && $postData['LibrarySentenceId'] > 0) $query.=" AND LibrarySentenceId=".$postData['LibrarySentenceId'];
        if(isset($postData['SearchText']) && !empty($postData['SearchText'])) $query.=" AND (SentenceTitle LIKE '%{$postData['SearchText']}%' OR SentenceContent LIKE '%{$postData['SearchText']}%')";
        if(isset($postData['FbPageId']) && $postData['FbPageId'] > 0) $query.=" AND LibrarySentenceId IN(SELECT LibrarySentenceId FROM librarypages WHERE FbPageId = {$postData['FbPageId']})";
        return $query;
    }
}