<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mmanufacturers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "manufacturers";
        $this->_primary_key = "ManufacturerId";
    }
}