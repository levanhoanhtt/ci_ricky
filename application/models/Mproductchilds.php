<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproductchilds extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "productchilds";
        $this->_primary_key = "ProductChildId";
    }

    public function getByProductId($productId){
        return $this->getBy(array('ProductId' => $productId, 'StatusId' => STATUS_ACTIVED, 'ParentProductChildId' => 0));
    }

    public function getAllByProductId($productId){
        $retVal = array();
        $listProductChilds = $this->getBy(array('ProductId' => $productId, 'StatusId' => STATUS_ACTIVED));
        foreach($listProductChilds as $pc){
            if($pc['ParentProductChildId'] == 0){
                $productOlds = array();
                foreach($listProductChilds as $pc1){
                    if($pc1['ParentProductChildId'] == $pc['ProductChildId']) $productOlds[] = $pc1;
                }
                $pc['ProductOlds'] = $productOlds;
                $retVal[] = $pc;
            }
        }
        return $retVal;
    }
}