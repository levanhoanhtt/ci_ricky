<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mstorecirculationtransports extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "storecirculationtransports";
        $this->_primary_key = "StoreCirculationTransportId";
    }

    public function update($postData, $storeCirculationTransportId = 0, $metaData = array(), $actionLog = array()){
        $isUpdate = $storeCirculationTransportId > 0;
        $actionLogs = array();
        $transportCount = 0;
        if(!$isUpdate) $transportCount = $this->countRows(array('StoreCirculationId' => $postData['StoreCirculationId']));
        $this->db->trans_begin();
        $storeCirculationTransportId = $this->save($postData, $storeCirculationTransportId);
        if($storeCirculationTransportId > 0){
            if(!empty($actionLog)){
                $actionLog['ItemId'] = $storeCirculationTransportId;
                $actionLogs[] = $actionLog;
            }
            if(!$isUpdate) {
                $transportCode =  'VC-LCK-' . ($postData['StoreCirculationId'] + 10000);
                if($transportCount > 0) $transportCode .= '-L'.($transportCount + 1);
                $this->db->update('storecirculationtransports', array('TransportCode' => $transportCode), array('StoreCirculationTransportId' => $storeCirculationTransportId));
                $this->db->update('storecirculations', array('StoreCirculationStatusId' => 2, 'UpdateUserId' => $postData['CrUserId'], 'UpdateDateTime' => $postData['CrDateTime']), array('StoreCirculationId' => $postData['StoreCirculationId']));
                $actionLogs[] = array(
                    'ItemId' => $postData['StoreCirculationId'],
                    'ItemTypeId' => 7,
                    'ActionTypeId' => 2,
                    'Comment' => $metaData['FullName'] . ' duyệt giao hàng',
                    'CrUserId' => $postData['CrUserId'],
                    'CrDateTime' => $postData['CrDateTime']
                );
                //tru so luong ton kho
                if(!empty($metaData['InventoryData'])){
                    foreach($metaData['InventoryData'] as $id) $this->Minventories->update($id);
                }
            }
            if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $storeCirculationTransportId;
        }
    }

    public function updateField($postData, $storeCirculationTransportId, $actionLog = array(), $metaData = array()){
        $this->db->trans_begin();
        $actionLogs = array();
        $storeCirculationTransportId = $this->save($postData, $storeCirculationTransportId);
        if($storeCirculationTransportId > 0){
            if(!empty($actionLog)) $actionLogs[] = $actionLog;
            if(isset($postData['TransportStatusId'])){
                if($postData['TransportStatusId'] == 4) { //Đã giao hàng
                    //LCK thANH CONG
                    $this->db->update('storecirculations', array('StoreCirculationStatusId' => 3, 'UpdateUserId' => $metaData['UserId'], 'UpdateDateTime' => $postData['UpdateDateTime']), array('StoreCirculationId' => $metaData['StoreCirculationId']));
                    $actionLogs[] = array(
                        'ItemId' => $metaData['StoreCirculationId'],
                        'ItemTypeId' => 7,
                        'ActionTypeId' => 2,
                        'Comment' => $metaData['FullName'] . ' chuyển trạng thái LCK thành Đã nhận hàng',
                        'CrUserId' => $metaData['UserId'],
                        'CrDateTime' => $postData['UpdateDateTime']
                    );
                }
                elseif($postData['TransportStatusId'] == 5 || $postData['TransportStatusId'] == 7) { //huy hoac chuyen hoan thanh cong
                    //LCK thất bai
                    $this->db->update('storecirculations', array('StoreCirculationStatusId' => 5, 'UpdateUserId' => $metaData['UserId'], 'UpdateDateTime' => $postData['UpdateDateTime']), array('StoreCirculationId' => $metaData['StoreCirculationId']));
                    $actionLogs[] = array(
                        'ItemId' => $metaData['StoreCirculationId'],
                        'ItemTypeId' => 7,
                        'ActionTypeId' => 2,
                        'Comment' => $metaData['FullName'] . ' chuyển trạng thái LCK thành THẤT BẠI',
                        'CrUserId' => $metaData['UserId'],
                        'CrDateTime' => $postData['UpdateDateTime']
                    );
                }
                //cong lai so luong sp
                if(!empty($metaData['InventoryData'])){
                    foreach($metaData['InventoryData'] as $id) $this->Minventories->update($id);
                }
            }
        }
        if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $storeIds = array()){
        $queryCount = "select storecirculationtransports.StoreCirculationTransportId AS totalRow from storecirculationtransports {joins} where {wheres}";
        $query = "select {selects} from storecirculationtransports {joins} where {wheres} ORDER BY storecirculationtransports.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'storecirculationtransports.StoreCirculationTransportId',
            'storecirculationtransports.TransportCode',
            'storecirculationtransports.TransportStatusId',
            'storecirculationtransports.CrDateTime',
            'storecirculations.StoreSourceId',
            'storecirculations.StoreDestinationId',
            'transporters.TransporterName',
            // 'stores.StoreName'
        ];
        $joins = [
            'storecirculations' => "left join storecirculations on storecirculations.StoreCirculationId = storecirculationtransports.StoreCirculationId",
            'transporters' => "left join transporters on transporters.TransporterId = storecirculationtransports.TransporterId",
            // 'stores' => "left join stores on stores.StoreId = storecirculationtransports.StoreId",

        ];
        $wheres = array('TransportStatusId > 0');
        $dataBind = [];
        if(!empty($storeIds)){
            $wheres[] = '(storecirculations.StoreSourceId IN ? OR storecirculations.StoreDestinationId IN ?)';
            $dataBind[] = $storeIds;
            $dataBind[] = $storeIds;
        }
        $whereSearch= '';
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'storecirculationtransports.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            /*elseif(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'transports.TransportCode like ? or orders.OrderCode like ? or transports.CrDateTime like ?';
                for( $i = 0; $i < 3; $i++) $dataBind[] = "%$searchText%";
            }*/
            else{
                $whereSearch = 'storecirculationtransports.TransportCode like ? or storecirculations.StoreCirculationCode like ?';
                for( $i = 0; $i < 2; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                switch ($filed_name) {
                    case 'transport_status':
                        $wheres[] = "storecirculationtransports.TransportStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'transport_transporter':
                        $wheres[] = "storecirculationtransports.TransporterId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                   
                    case 'transport_create':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'storecirculationtransports.CrDateTime between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "storecirculationtransports.CrDateTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "storecirculationtransports.CrDateTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(storecirculationtransports.CrDateTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    case 'store_source':
                        $wheres[] = "storecirculations.StoreSourceId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'store_destination':
                        $wheres[] = "storecirculations.StoreDestinationId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'transport_tag':
                        $wheres[] = "storecirculationtransports.StoreCirculationTransportId $conds[0](SELECT ItemId FROM itemtags WHERE ItemTypeId = 25 AND TagId = ?)";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $dataStorecirculationtransports = $this->getByQuery($query, $dataBind);
        $listStores = $this->Mstores->getBy(array('ItemStatusId >' => 0));
        for ($i = 0; $i < count($dataStorecirculationtransports); $i++) {
            $dataStorecirculationtransports[$i]['TransportStatus'] = $dataStorecirculationtransports[$i]['TransportStatusId'] > 0 ? $this->Mconstants->transportStatus[$dataStorecirculationtransports[$i]['TransportStatusId']] : '';
            $dayDiff = getDayDiff($dataStorecirculationtransports[$i]['CrDateTime'], $now);
            $dataStorecirculationtransports[$i]['CrDateTime'] = ddMMyyyy($dataStorecirculationtransports[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataStorecirculationtransports[$i]['DayDiff'] = $dayDiff;
            $dataStorecirculationtransports[$i]['labelCss'] = $this->Mtransports->labelCss['TransportStatusCss'];
            $dataStorecirculationtransports[$i]['StoreSource'] = $this->Mconstants->getObjectValue($listStores, 'StoreId', $dataStorecirculationtransports[$i]['StoreSourceId'], 'StoreName');
            $dataStorecirculationtransports[$i]['StoreDestination'] = $this->Mconstants->getObjectValue($listStores, 'StoreId', $dataStorecirculationtransports[$i]['StoreDestinationId'], 'StoreName');
        }
        $data = array();
        $totalRows = $this->getByQuery($queryCount, $dataBind);
        $totalRow = count($totalRows);
        $totalIds = array();
        foreach ($totalRows as $v) $totalIds[] = intval($v['totalRow']);
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $dataStorecirculationtransports;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderStorecirculationtransports';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        $data['totalIds'] = json_encode($totalIds);
        return $data;
    }
}