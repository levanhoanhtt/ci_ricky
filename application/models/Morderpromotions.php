<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Morderpromotions extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "orderpromotions";
        $this->_primary_key = "OrderPromotionId";
    }

    public function getByOrderId($orderId){
        return $this->getBy(array('OrderId' => $orderId, 'StatusId >' => 0), true, 'OrderPromotionId');
    }

    public function update($postData, $orderPromotionId, $orderActionLog = array()){
        $this->db->trans_begin();
        $orderPromotionId = $this->save($postData, $orderPromotionId);
        if($orderPromotionId > 0) $this->Morders->updateField(array('Discount' => $postData['DiscountCost'], 'UpdateUserId' => $postData['CrUserId'], 'UpdateDateTime' => $postData['CrDateTime']), $postData['OrderId'], $orderActionLog);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }
}