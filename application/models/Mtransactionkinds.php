<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransactionkinds extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transactionkinds";
        $this->_primary_key = "TransactionKindId";
    }

    public function getList($transactionTypeId = 0, $transactionKindTypeId = 0){
        $retVal = array();
        $where = array('StatusId' => STATUS_ACTIVED);
        if($transactionTypeId > 0) $where['TransactionTypeId'] = $transactionTypeId;
        if($transactionKindTypeId > 0) $where['TransactionKindTypeId'] = $transactionKindTypeId;
        $listTransactionKinds = $this->getBy($where, false, 'TransactionTypeId');
        foreach($listTransactionKinds as $tk){
            if($tk['ParentTransactionKindId'] == 0){
                $retVal[] = $tk;
                foreach($listTransactionKinds as $tk1){
                    if($tk1['ParentTransactionKindId'] == $tk['TransactionKindId']) $retVal[] = $tk1;
                }
            }
        }
        return $retVal;
    }
}
