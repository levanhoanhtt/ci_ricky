<?php
/**
 * Created by PhpStorm.
 * User: MAN - DEV
 * Date: 7/16/2018
 * Time: 4:11 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Moffsettypes extends MY_Model
{
    function __construct()
    {
        parent::__construct();
        $this->_table_name = "offsettypes";
        $this->_primary_key = "OffsetTypeId";
    }
}