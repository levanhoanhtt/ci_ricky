<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcustomeraddress extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "customeraddress";
        $this->_primary_key = "CustomerAddressId";
    }

    public function update($postData){
        $searchData = $postData;
        unset($searchData['CrUserId']);
        unset($searchData['CrDateTime']);
        $customerAddressId = $this->getFieldValue($searchData, 'CustomerAddressId', 0);
        if($customerAddressId == 0) $customerAddressId = $this->save($postData);
        return $customerAddressId;
    }

    public function getInfo($customerAddressId, $customerId){
        if($customerAddressId > 0) return $this->get($customerAddressId);
        elseif($customerId > 0){
            $customer = $this->Mcustomers->get($customerId);
            if($customer){
                return array(
                    'CustomerAddressId' => 0,
                    'CustomerId' => $customerId,
                    'CustomerName' => $customer['FullName'],
                    'Email' => $customer['Email'],
                    'PhoneNumber' => $customer['PhoneNumber'],
                    'Address' => $customer['Address'],
                    'ProvinceId' => $customer['ProvinceId'],
                    'DistrictId' => $customer['DistrictId'],
                    'WardId' => $customer['WardId'],
                    'CountryId' => $customer['CountryId'],
                    'ZipCode' => $customer['ZipCode']
                );
            }
        }
        return false;
    }
}