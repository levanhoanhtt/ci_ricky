<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfb_threads extends MY_Model {

    private $prefix = '';

    function __construct() {
        parent::__construct();
        $this->_primary_key = "FbThreadId";
    }

    public function setPrefix($prefix){
        $this->prefix = $prefix;
        $this->_table_name = "fb_threads_".$prefix;
    }

    public function getPrefix(){
        return $this->prefix;
    }
}