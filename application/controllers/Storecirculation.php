<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Storecirculation extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Lưu chuyển kho',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css', 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/search_item.js', 'js/store_circulation_list.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'storecirculation')) {
            $this->loadModel(array('Mfilters', 'Mstores', 'Mtags'));
            $itemTypeId = 7;
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], true);
            $data['listFilters'] = $this->Mfilters->getList($itemTypeId);
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => $itemTypeId));
            $this->load->view('storecirculation/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Tạo đơn Lưu chuyển kho',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/choose_item.js', 'js/store_circulation.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'storecirculation')){
            $data['canEdit'] = true;
            $this->loadModel(array('Mcategories', 'Mstores', 'Mtags'));
            $listAllStores =  $this->Mstores->getByUserId($user['UserId'], true);
            $data['listDestinationStores'] = $listAllStores;
            $data['listSourceStores'] = $listAllStores;
                /*if($this->Mactions->checkAccess($data['listActions'], 'store/viewAll')) $data['listSourceStores'] = $listAllStores;
                else $data['listSourceStores'] = $this->Mstores->getByUserId($user['UserId'], false);*/
            $data['listCategories'] = $this->Mcategories->getListByItemType(1);
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 7));
            $this->load->view('storecirculation/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($storeCirculationId = 0){
        if ($storeCirculationId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Cập nhật Lưu chuyển kho',
                array(
                    'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                    'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'vendor/plugins/bxslider/jquery.bxslider.min.js', 'js/choose_item.js', 'js/store_circulation.js'))
                )
            );
            $this->loadModel(array('Mcategories', 'Mstores', 'Mstorecirculations', 'Mtags', 'Mstorecirculationproducts', 'Mproducts', 'Mproductchilds', 'Mstorecirculationcomments', 'Mproductunits', 'Mactionlogs','Mtransporttypes','Mtransporters','Mstorecirculationtransports', 'Mcancelreasons'));
            $storeCirculation = $this->Mstorecirculations->get($storeCirculationId);
            if ($storeCirculation) {
                $listActions = $data['listActions'];
                if ($this->Mactions->checkAccess($listActions, 'storecirculation')){
                    $data['title'] .= ' ' . $storeCirculation['StoreCirculationCode'];
                    $data['canActive'] = $this->Mactions->checkAccess($listActions, 'storecirculation/active');
                    $data['canEdit'] = $storeCirculation['StoreCirculationStatusId'] == 1 || $storeCirculation['StoreCirculationStatusId'] == 5;
                    $data['storeCirculationId'] = $storeCirculationId;
                    $data['storeCirculation'] = $storeCirculation;
                    $listAllStores =  $this->Mstores->getByUserId($user['UserId'], true);
                    $data['listDestinationStores'] = $listAllStores;
                    $data['listSourceStores'] = $listAllStores;
                    /*if($this->Mactions->checkAccess($data['listActions'], 'store/viewAll')) $data['listSourceStores'] = $listAllStores;
                    else $data['listSourceStores'] = $this->Mstores->getByUserId($user['UserId'], false);*/
                    $data['listCategories'] = $this->Mcategories->getListByItemType(1);
                    $data['tagNames'] = $this->Mtags->getTagNames($storeCirculationId, 7);
                    $data['listStoreCirculationProducts'] = $this->Mstorecirculationproducts->getBy(array('StoreCirculationId' => $storeCirculationId));
                    $data['listStoreCirculationComments'] = $this->Mstorecirculationcomments->getListByStoreCirculationId($storeCirculationId);
                    $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 7));
                    $data['listActionLogs'] = $this->Mactionlogs->getList($storeCirculationId, 7);
                    $data['listTransportTypes'] = $this->Mtransporttypes->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listCancelReasons'] = $this->Mcancelreasons->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listTransporters'] = $this->Mtransporters->getBy(array('ItemStatusId' => STATUS_ACTIVED));
                    $listTransports = $this->Mstorecirculationtransports->getBy(array('StoreCirculationId' => $storeCirculationId, 'TransportStatusId >' => 0), false, 'StoreCirculationTransportId');
                    $transport = false;
                    $transportId = 0;
                    foreach($listTransports as $t){
                        if($t['StoreCirculationTransportId'] > $transportId){
                            $transportId = $t['StoreCirculationTransportId'];
                            $transport = $t;
                        }
                    }
                    $data['listTransports'] = $listTransports;
                    $data['transport'] = $transport;
                    $this->load->view('storecirculation/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else {
                $data['canEdit'] = false;
                $data['canActive'] = false;
                $data['storeCirculationId'] = 0;
                $data['txtError'] = "Không tìm thấý Lưu chuyển kho";
                $this->load->view('storecirculation/edit', $data);
            }
        }
        else redirect('storecirculation');
    }

    public function printPdf($storeCirculationId = 0){
        if($storeCirculationId > 0) {
            $this->checkUserLogin(true);
            $this->loadModel(array('Mstores', 'Mstorecirculations', 'Mstorecirculationproducts', 'Mproducts', 'Mproductchilds', 'Mprovinces', 'Mdistricts', 'Mwards'));
            $storeCirculation = $this->Mstorecirculations->get($storeCirculationId);
            if($storeCirculation) {
                $customerAddress = array(
                    'CustomerName' => '',
                    'PhoneNumber' => '',
                    'Address' => ''
                );
                $flag = false;
                if($storeCirculation['StoreDestinationId'] > 0){
                    $store = $this->Mstores->get($storeCirculation['StoreDestinationId']);
                    if($store){
                        if($store['HeadUserId'] > 0){
                            $headUser = $this->Musers->get($store['HeadUserId'], true, '', 'FullName, PhoneNumber');
                            if($headUser){
                                $flag = true;
                                $customerAddress['CustomerName'] = $headUser['FullName'];
                                $customerAddress['PhoneNumber'] = $headUser['PhoneNumber'];
                            }
                        }
                        if(!$flag) $customerAddress['CustomerName'] = $store['StoreName'];
                        $address = $store['Address'];
                        if($store['WardId'] > 0) $address .= ', xã '.$this->Mwards->getFieldValue(array('WardId' => $store['WardId']), 'WardName');
                        if($store['DistrictId'] > 0) $address .= ', huyện '.$this->Mdistricts->getFieldValue(array('DistrictId' => $store['DistrictId']), 'DistrictName');
                        if($store['ProvinceId'] > 0) $address .= ', tỉnh '.$this->Mprovinces->getFieldValue(array('ProvinceId' => $store['ProvinceId']), 'ProvinceName');
                        $customerAddress['Address'] = $address;
                    }
                }
                $this->load->library('ciqrcode');
                $params = array();
                $storeCirculationCode = $storeCirculation['StoreCirculationCode'];
                $params['data'] = $storeCirculationCode;
                $params['savename'] = "./assets/uploads/qr/{$storeCirculationCode}.png";
                $this->ciqrcode->generate($params);
                $data = array(
                    'configs' => $this->session->userdata('configs'),
                    'storeCirculation' => $storeCirculation,
                    'customerAddress' => $customerAddress,
                    'listStoreCirculationProducts' => $this->Mstorecirculationproducts->getBy(array('StoreCirculationId' => $storeCirculationId)),
                    'barcodeSrc' => $params['savename'],
                );
                $this->load->view('storecirculation/print_pdf', $data);
            }
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
    }
}