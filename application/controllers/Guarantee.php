<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guarantee extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách bảo hành',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_item.js', 'js/guarantee_list.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'guarantee')) {
            $this->loadModel(array('Mfilters', 'Mcustomers', 'Mreceipttypes', 'Mpaymenttypes', 'Mstores','Mtags'));
            $whereStatus = array('StatusId' => STATUS_ACTIVED);
            $itemTypeId = 27;
            $data['listCustomers'] = $this->Mcustomers->getBy($whereStatus);
            $data['listReceiptTypes'] = $this->Mreceipttypes->getBy($whereStatus);
            $data['listPaymentTypes'] = $this->Mpaymenttypes->getBy($whereStatus);
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $data['listFilters'] = $this->Mfilters->getList($itemTypeId);
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => $itemTypeId));
            $this->load->view('guarantee/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm bảo hành',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css',  'vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'ckfinder/ckfinder.js', 'vendor/plugins/jwerty/jwerty.js', 'js/choose_item.js', 'js/guarantee_update.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'guarantee')){
            $data['canEditAll'] = true;
            $this->loadModel(array('Mstores', 'Mreceipttypes', 'Mpaymenttypes', 'Musagestatus', 'Mparts'));
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $whereStatus = array('StatusId' => STATUS_ACTIVED);
            $data['listReceiptTypes'] = $this->Mreceipttypes->getBy($whereStatus);
            $data['listPaymentTypes'] = $this->Mpaymenttypes->getBy($whereStatus);
            $data['listParts'] = $this->Mparts->getList(true);
            $data['listUsageStatus'] = $this->Musagestatus->getBy(array('StatusId' => STATUS_ACTIVED, 'UsageStatusTypeId' => 1));
            $data['listUsers'] = $this->Musers->getListForSelect($user['UserId'], 'Chỉ mình tôi');
            $this->load->view('guarantee/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($guaranteeId = 0){
        if($guaranteeId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Cập nhật Bảo hành',
                array(
                    'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css',  'vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css'),
                    'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'ckfinder/ckfinder.js', 'vendor/plugins/jwerty/jwerty.js', 'js/choose_item.js', 'js/guarantee_update.js'))
                )
            );
            $this->loadModel(array('Mguarantees', 'Morders', 'Morderproducts', 'Mguaranteeproducts', 'Mproductunits', 'Mproducts', 'Mproductchilds', 'Mproductaccessories', 'Mguaranteecomments', 'Mstores', 'Mreceipttypes', 'Mpaymenttypes', 'Musagestatus', 'Mparts', 'Mactionlogs'));
            $guarantee = $this->Mguarantees->get($guaranteeId);
            if ($guarantee && $guarantee['GuaranteeStatusId'] > 0){
                if($this->Mactions->checkAccess($data['listActions'], 'guarantee')){
                    $data['title'] .= ' ' . $guarantee['GuaranteeCode'];
                    $data['canEditAll'] = $guarantee['GuaranteeStep'] < 3;
                    $data['guaranteeId'] = $guaranteeId;
                    $data['guarantee'] = $guarantee;
                    $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], true);
                    $whereStatus = array('StatusId' => STATUS_ACTIVED);
                    $data['listReceiptTypes'] = $this->Mreceipttypes->getBy($whereStatus);
                    $data['listPaymentTypes'] = $this->Mpaymenttypes->getBy($whereStatus);
                    $data['listProductUnits'] = $this->Mproductunits->getBy($whereStatus);
                    $data['listGuaranteeProducts'] = $this->Mguaranteeproducts->getBy(array('GuaranteeId' => $guaranteeId));
                    $data['listActionLogs'] = $this->Mactionlogs->getList($guaranteeId, 27);
                    $listGuaranteeComments = $this->Mguaranteecomments->getListByGuaranteeId($guaranteeId);
                    $listGuaranteeComments1 = $listGuaranteeComments2 = array();
                    $data['listGuaranteeComments'] = $listGuaranteeComments;
                    foreach($listGuaranteeComments as $gc){
                        if($gc['CommentTypeId'] == 1) $listGuaranteeComments1[] = $gc;
                        else $listGuaranteeComments2[] = $gc;
                    }
                    $data['listGuaranteeComments1'] = $listGuaranteeComments1;
                    $data['listGuaranteeComments2'] = $listGuaranteeComments2;
                    $data['listParts'] = $this->Mparts->getList(true);
                    $data['listUsageStatus'] = $this->Musagestatus->getBy($whereStatus);
                    $data['listUsers'] = $this->Musers->getListForSelect($user['UserId'], 'Chỉ mình tôi');
                    $this->load->view('guarantee/edit_2', $data); //guarantee/edit
                }
                else $this->load->view('user/permission', $data);
            }
            else {
                $data['guaranteeId'] = 0;
                $data['txtError'] = "Không tìm thấy Bảo hành";
                $this->load->view('guarantee/edit', $data);
            }
        }
        else redirect('guarantee');
    }
}