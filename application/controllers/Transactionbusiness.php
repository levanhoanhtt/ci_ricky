<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactionbusiness extends MY_Controller{

    public function index($transactionTypeId = 0){
        $user = $this->checkUserLogin();
        if(!in_array($transactionTypeId, array(1, 2))) $transactionTypeId = 1;
        $data = $this->commonData($user,
            'Danh sách phiếu '.($transactionTypeId == 1 ? 'thu' : 'chi').' kinh doanh',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/search_item.js', 'js/transactionbusiness_list.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'transactionbusiness/'.$transactionTypeId)) {
            $this->load->model(array('Mfilters','Mtags'));
            $itemTypeId = $transactionTypeId == 1 ? 31 : 32;
            $data['itemTypeId'] = $itemTypeId;
            $data['transactionTypeId'] = $transactionTypeId;
            $data['listFilters'] = $this->Mfilters->getList($itemTypeId);
             $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => $itemTypeId));
            $this->load->view('transactionbusiness/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($transactionBusinessId = 0){
        if ($transactionBusinessId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Cập nhật phiếu thu chi',
                array('scriptFooter' => array('js' => 'js/transactionbusiness_update.js'))
            );
            $this->loadModel(array('Mtransactionbusiness', 'Mtransactionkinds', 'Mtransactionbusinessitems'));
            $transactionBusiness = $this->Mtransactionbusiness->get($transactionBusinessId);
            if($transactionBusiness) {
                $transactionTypeId = $transactionBusiness['TransactionTypeId'];
                if ($this->Mactions->checkAccess($data['listActions'], 'transactionbusiness/'.$transactionTypeId)){
                    $data['title'] = 'Cập nhật phiếu ' . $this->Mconstants->transactionTypes[$transactionTypeId].' tháng '.ddMMyyyy($transactionBusiness['TransactionDate'], 'm/Y');
                    $data['transactionTypeId'] = $transactionTypeId;
                    $data['transactionBusinessId'] = $transactionBusinessId;
                    $data['transactionBusiness'] = $transactionBusiness;
                    $data['listTransactionKinds'] = $this->Mtransactionkinds->getList($transactionTypeId, 3);
                    $postData = array('TransactionBusinessId' => $transactionBusinessId, 'TransactionStatusId' => 2);
                    /*$searchData = $this->arrayFromPost(array('TransactionKindId1', 'TransactionKindId2'));
                    if($searchData['TransactionKindId2'] > 0) $postData['TransactionKindId'] = $searchData['TransactionKindId2'];
                    elseif($searchData['TransactionKindId1'] > 0) $postData['TransactionKindId'] = $searchData['TransactionKindId1'];*/
                    $data['listTransactionBusinessItems'] = $this->Mtransactionbusinessitems->getBy($postData);
                    $this->load->view('transactionbusiness/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else{
                $data['transactionTypeId'] = 0;
                $data['txtError'] = "Không tìm thấý phiếu";
                $this->load->view('transactionbusiness/edit', $data);
            }
        }
        else redirect('transaction');
    }

    public function searchByFilter($transactionTypeId = 0){
        $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if(!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if(!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->loadModel(array('Mtransactionkinds', 'Mtransactionbusiness'));
        $data1 = $this->Mtransactionbusiness->searchByFilter($searchText, $itemFilters, $limit, $page, $transactionTypeId);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $items = $this->input->post('Items');
        if(!empty($items)){
            $itemInserts = array();
            $itemUpdates = array();
            $crDateTime = getCurentDateTime();
            foreach($items as $item){
                if($item['TransactionBusinessItemId'] > 0){
                    $item['UpdateUserId'] = $user['UserId'];
                    $item['UpdateDateTime'] = $crDateTime;
                    $itemUpdates[] = $item;
                }
                else{
                    $item['CrUserId'] = $user['UserId'];
                    $item['CrDateTime'] = $crDateTime;
                    unset($item['TransactionBusinessItemId']);
                    $itemInserts[] = $item;
                }
            }
            if(!empty($itemInserts)) $this->db->insert_batch('transactionbusinessitems', $itemInserts);
            if(!empty($itemUpdates)) $this->db->update_batch('transactionbusinessitems', $itemUpdates, 'TransactionBusinessItemId');
            echo json_encode(array('code' => 1, 'message' => "Cập nhật chi phí thành công"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}