<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paymenttype extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Phương thức thanh toán',
            array('scriptFooter' => array('js' => 'js/payment_type.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'paymenttype')) {
            $this->load->model('Mpaymenttypes');
            $data['listPaymentTypes'] = $this->Mpaymenttypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/payment_type', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('PaymentTypeName'));
        if(!empty($postData['PaymentTypeName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $paymentTypeId = $this->input->post('PaymentTypeId');
            $this->load->model('Mpaymenttypes');
            $flag = $this->Mpaymenttypes->save($postData, $paymentTypeId);
            if ($flag > 0) {
                $postData['PaymentTypeId'] = $flag;
                $postData['IsAdd'] = ($paymentTypeId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật phương thức thanh toán thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $paymentTypeId = $this->input->post('PaymentTypeId');
        if($paymentTypeId > 0){
            $this->load->model('Mpaymenttypes');
            $flag = $this->Mpaymenttypes->changeStatus(0, $paymentTypeId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa phương thức thanh toán thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}