<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Position extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Quản lý vị trí',
            array(
                'scriptHeader' => array('css' => array('css/jquery.orgchart.css', 'css/position.css')),
                'scriptFooter' => array('js' => array('ckfinder/ckfinder.js', 'js/jquery.orgchart.js', 'js/position_list.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'position')) {
			$this->loadModel(array('Mpositionparameters', 'Mpositions'));
			$listPositions = $this->Mpositions->getBy(array('StatusId' => STATUS_ACTIVED), false, 'PositionLevel', '', 0, 0, 'asc');
            $data['listPositions'] = $listPositions;		
            $this->load->view('position/list', $data);
        }
        else $this->load->view('user/permission', $data);
	}
	
    public function staff(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Sơ đồ vị trí',
            array(
                'scriptHeader' => array('css' => array('css/jquery.orgchart.css', 'css/position_staff.css')),
                'scriptFooter' => array('js' => array('js/jquery.orgchart.js', 'js/position_staff.js', 'js/position_to_department.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'position/staff')) {
            $data['listUsers'] = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('position/staff', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Trang khởi tạo vị trí động',
            array(
                'scriptHeader' => array('css' => array('css/jquery.orgchart.css', /*'css/style.css', */'css/parametervalues.css')),
                'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'js/jquery.orgchart.js', 'js/position_update.js'/*, 'js/dragdrop.js', 'js/parametervalues.js'*/))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'position/edit')) {
            $this->load->view('position/edit', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update_add(){
        $user = $this->checkUserLogin();
        $PositionId = $this->input->post('PositionId');
        $ParameterValueId = $this->input->post('ParameterValueId');
        $ParameterId = $this->input->post('ParameterId');

        if(!empty($PositionId) && !empty($ParameterValueId) && !empty($ParameterId)){

            if(!preg_match("/^\d+\.?\d*$/", $PositionId) || !preg_match("/^\d+\.?\d*$/", $ParameterValueId) || !preg_match("/^\d+\.?\d*$/", $ParameterId)){
                echo json_encode(array('error' => 1, 'message' => "Dữ liệu truyền không đúng quy định"));
                die;
            }
            $this->loadModel(array('Mpositionparameters'));
            $Mpositionparameters = $this->Mpositionparameters;

            if(($Mpositionparameters->CheckHas_PositionId($PositionId))>0){
                echo json_encode(array('error' => 1, 'message' => "Khu vực làm việc đã có trong hệ thống"));
                die;
            }

            $data=[
                'PositionId' => $PositionId,
                'ParameterValueId' => $ParameterValueId,
                'ParameterId' => $ParameterId,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => getCurentDateTime(),
            ];

            $PositionParameterId = $Mpositionparameters->save($data,0);
            if($PositionParameterId){
                echo json_encode(array('error' => 0, 'message' => "Cập nhật khu vực làm việc thành công", 'PositionId'=>$PositionId));
                die;
            }else{
                echo json_encode(array('error' => 1, 'message' => "Quá trình lưu thông tin lỗi"));
                die;
            }
        }else{
            echo json_encode(array('error' => 1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            die;
        }
    }

    public function update_edit(){

        $user = $this->checkUserLogin();
        $PositionId = $this->input->post('PositionId');
        $ParameterValueId = $this->input->post('ParameterValueId');
        $ParameterId = $this->input->post('ParameterId');

        if(!empty($PositionId) && !empty($ParameterValueId) && !empty($ParameterId)){

            if(!preg_match("/^\d+\.?\d*$/", $PositionId) || !preg_match("/^\d+\.?\d*$/", $ParameterValueId) || !preg_match("/^\d+\.?\d*$/", $ParameterId)){
                echo json_encode(array('error' => 1, 'message' => "Dữ liệu truyền không đúng quy định"));
                die;
            }
            $this->loadModel(array('Mpositionparameters'));

            $Mpositionparameters = $this->Mpositionparameters;

            if(($Mpositionparameters->CheckHas_PositionId($PositionId))!=1){
                echo json_encode(array('error' => 1, 'message' => "Khu vực làm việc chưa có trong hệ thống"));
                die;
            }

            $data=[
                'ParameterValueId' => $ParameterValueId,
                'ParameterId' => $ParameterId,
                'UpdateUserId' => $user['UpdateUserId'],
                'UpdateDateTime' => getCurentDateTime(),
            ];

            $flag = $Mpositionparameters->Update_ByPositionId($data,$PositionId);
            if($flag>0){
                echo json_encode(array('error' => 0, 'message' => "Sửa khu vực làm việc thành công", 'PositionId'=>$PositionId));
                die;
            }else{
                echo json_encode(array('error' => -1, 'message' => "Quá trình lưu thông tin lỗi"));
                die;
            }
        }else{
            echo json_encode(array('error' => -2, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            die;
        }
    }

}