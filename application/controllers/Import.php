<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends MY_Controller{

    public function index() {
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Nhập kho',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_item.js', 'js/import.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'import')) {
            $this->loadModel(array('Mfilters', 'Mstores', 'Msuppliers','Mtags'));
            $itemTypeId = 8;
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listFilters'] = $this->Mfilters->getList($itemTypeId);
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => $itemTypeId));
            $this->load->view('import/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Tạo phiếu nhập kho',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/choose_item.js', 'js/choose_item_product_errors.js', 'js/import_update.js'))
            )
        );
        $listActions = $data['listActions'];
        if($this->Mactions->checkAccess($listActions, 'import')) {
            $data['canActive'] = false;// $this->Mactions->checkAccess($listActions, 'import/active');
            $this->loadModel(array('Mstores', 'Msuppliers', 'Mcategories', 'Mtags', 'Motherservices'));
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($listActions, 'store/viewAll'));
            $data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(1);
            $data['listOtherServices'] = $this->Motherservices->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 8));
            $this->load->view('import/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($importId = 0){
        if($importId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Cập nhật phiếu nhập kho',
                array(
                    'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css'),
                    'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/choose_item.js', 'js/choose_item_product_errors.js', 'js/import_update.js'))
                )
            );
            $this->loadModel(array('Mimports', 'Mstores', 'Mtags', 'Mproductchilds', 'Mproducts', 'Mimportproducts', 'Mproductunits', 'Msuppliers', 'Mcategories', 'Mactionlogs', 'Motherservices', 'Mimportservices','Mimportproducterrors'));
            $import = $this->Mimports->get($importId);
            if ($import) {
                $listActions = $data['listActions'];
                if ($this->Mactions->checkAccess($listActions, 'import')) {
                    $canActive = $this->Mactions->checkAccess($listActions, 'import/active');
                    $data['canActive'] = $canActive;
                    $data['title'] .= ' ' . $import['ImportCode'];
                    $canEdit =  $import['ImportStatusId'] == 5;
                    if($canActive) $canEdit = $canEdit || $import['ImportStatusId'] == 1;
                    $data['canEdit'] = $canEdit;
                    $data['importId'] = $importId;
                    $data['import'] = $import;
                    $data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
                    $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
                    $data['listCategories'] = $this->Mcategories->getListByItemType(1);
                    $data['tagNames'] = $this->Mtags->getTagNames($importId, 8);
                    $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 8));
                    $data['listActionLogs'] = $this->Mactionlogs->getList($importId, 8);
                    $data['listImportProduct'] = $this->Mimportproducts->getBy(array('ImportId' => $importId));
                    $data['listImportProductErrors'] = $this->Mimportproducterrors->getBy(array('ImportId' => $importId));
                    $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listOtherServices'] = $this->Motherservices->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listImportServices'] = $this->Mimportservices->getBy(array('ImportId' => $importId));
                    $this->load->view('import/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else {
                $data['canEdit'] = false;
                $data['orderId'] = 0;
                $data['txtError'] = "Không tìm thấý phiếu nhập kho";
                $this->load->view('import/edit', $data);
            }
        }
        else redirect('import');
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ImportStatusId', 'SupplierId', 'DeliverName', 'DeliverPhone', 'StoreId', 'TransportCost', 'DiscountCost', 'PaymentCost', 'Comment', 'FileExcel', 'ScanBarCodeId'));
        if($postData['ImportStatusId'] > 0 && $postData['SupplierId'] > 0 && $postData['StoreId'] > 0){
            $importId = $this->input->post('ImportId');
            $products = json_decode(trim($this->input->post('Products')), true);
            $this->loadModel(array('Mimports', 'Mproductquantity','Mimportproducterrors'));
            $flag = true;
            $inventoryData = array();
            $crDateTime = getCurentDateTime();
            if($postData['ImportStatusId'] == STATUS_ACTIVED){
                $importStatusIdOld = $importId > 0 ? $this->Mimports->getFieldValue(array('ImportId' => $importId), 'ImportStatusId', 0) : 1;
                if($importStatusIdOld == 1 || $importStatusIdOld == 5){
                    foreach($products as $op){
                        $inventoryData[] = array(
                            'ProductId' => $op['ProductId'],
                            'ProductChildId' => $op['ProductChildId'],
                            'OldQuantity' => $this->Mproductquantity->getQuantity($op['ProductId'], $op['ProductChildId'], $postData['StoreId']),
                            'Quantity' => $op['Quantity'],
                            'InventoryTypeId' => 1,
                            'StoreId' => $postData['StoreId'],
                            'StatusId' => STATUS_ACTIVED,
                            'IsManual' => 1,
                            'CrUserId' => $user['UserId'],
                            'CrDateTime' => $crDateTime,
                            'UpdateUserId' => $user['UserId'],
                            'UpdateDateTime' => $crDateTime
                        );
                    }
                }
                else $flag = false;
            }
            if($flag) {
                $postData['TransportCost'] = replacePrice($postData['TransportCost']);
                $postData['DiscountCost'] = replacePrice($postData['DiscountCost']);
                $postData['PaymentCost'] = replacePrice($postData['PaymentCost']);
                $actionLogs = array(
                    'ItemTypeId' => 8,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
                if($importId > 0) {
                    $postData['UpdateUserId'] = $user['UserId'];
                    $postData['UpdateDateTime'] = $crDateTime;
                    $actionLogs['ActionTypeId'] = 2;
                    $actionLogs['Comment'] = $user['FullName'] . ': Cập nhật phiếu nhập kho';
                }
                else {
                    $postData['CrUserId'] = $user['UserId'];
                    $postData['CrDateTime'] = $crDateTime;
                    $actionLogs['ActionTypeId'] = 1;
                    $actionLogs['Comment'] = $user['FullName'] . ': Thêm phiếu nhập kho';
                }
                $productsError = json_decode(trim($this->input->post('ProductsError')), true);
                $tagNames = json_decode(trim($this->input->post('TagNames')), true);
                $services = json_decode(trim($this->input->post('ImportServices')), true);
                $importId = $this->Mimports->update($postData, $importId, $products, $productsError, $tagNames, $services, $inventoryData, $actionLogs);
                if ($importId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật phiếu nhập kho thành công", 'data' => $importId));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $importId = $this->input->post('ImportId');
        $importStatusId = $this->input->post('ImportStatusId');
        if($importId > 0 && $importStatusId == 3){
            $this->load->model('Mimports');
            $importStatusIdOld = $this->Mimports->getFieldValue(array('ImportId' => $importId), 'ImportStatusId', 0);
            if($importStatusIdOld == 1 || $importStatusIdOld == 5){
                $crDateTime = getCurentDateTime();
                $actionLogs = array(
                    'ItemId' => $importId,
                    'ActionTypeId' => 2,
                    'Comment' => $user['FullName'] . ': Hủy phiếu nhập kho',
                    'ItemTypeId' => 8,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );

                $flag = $this->Mimports->updateField(array('ImportStatusId' => $importStatusId, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime), $importId, $actionLogs);
                if ($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật phiếu nhập kho thành công"));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Trạng thái phiếu đang là ".$this->Mconstants->importStatus[$importStatusIdOld]));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function searchByFilter(){
        $user=$this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->loadModel(array('Mimports', 'Mstores'));
        $flag = $this->Mactions->checkAccessFromDb('store/viewAll', $user['UserId']);
        if(!$flag) $storeIds = $this->Mstores->getByUserId($user['UserId'], $flag, true);
        else $storeIds = array();
        $data1 = $this->Mimports->searchByFilter($searchText, $itemFilters, $limit, $page, $storeIds);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }
}