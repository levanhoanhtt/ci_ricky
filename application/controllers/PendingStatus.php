<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendingstatus extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách Trạng thái chờ xử lý',
			array('scriptFooter' => array('js' => 'js/pending_status.js'))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'pendingstatus')) {
			$this->load->model('Mpendingstatus');
			$data['listPendingStatus'] = $this->Mpendingstatus->getBy(array('StatusId' => STATUS_ACTIVED), false, 'ItemTypeId');
			$this->load->view('setting/pending_status', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('PendingStatusName', 'ItemTypeId'));
		if(!empty($postData['PendingStatusName']) && $postData['ItemTypeId'] > 0) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$pendingStatusId = $this->input->post('PendingStatusId');
			$this->load->model('Mpendingstatus');
			$flag = $this->Mpendingstatus->save($postData, $pendingStatusId);
			if ($flag > 0) {
				$postData['PendingStatusId'] = $flag;
				$postData['ItemTypeName'] = '<span class="'.$this->Mconstants->labelCss[$postData['ItemTypeId']].'">'.($postData['ItemTypeId'] == 6 ? 'Đơn hàng' : 'Vận chuyển').'</span>';
				$postData['IsAdd'] = ($pendingStatusId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Cập nhật Trạng thái chờ xử lý thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$this->checkUserLogin(true);
		$pendingStatusId = $this->input->post('PendingStatusId');
		if($pendingStatusId > 0){
			$this->load->model('Mpendingstatus');
			$flag = $this->Mpendingstatus->changeStatus(0, $pendingStatusId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Trạng thái chờ xử lý thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
