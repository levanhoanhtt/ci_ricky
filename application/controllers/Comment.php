<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách Bình luận - Đánh giá',
			array('scriptFooter' => array('js' => 'js/comment.js'))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'comment')) {
			$this->load->model('Mcomments');
			$postData = $this->arrayFromPost(array('StatusId', 'CommentStarId', 'IpAddress'));
			$rowCount = $this->Mcomments->getCount($postData);
			$data['listComments'] = array();
			if($rowCount > 0){
				$perPage = 20;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listComments'] = $this->Mcomments->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('comment/list', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function edit($commentId){
		if($commentId > 0) {
			$user = $this->checkUserLogin();
			$data = $this->commonData($user,
				'Cập nhật Bình luận - Đánh giá',
				array('scriptFooter' => array('js' => 'js/comment_update.js'))
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'comment')) {
				$this->load->model('Mcomments');
				$comment = $this->Mcomments->get($commentId);
				if ($comment) {
					$data['commentId'] = $commentId;
					$data['comment'] = $comment;
					$data['listCustomers'] = $this->Musers->getListByRole(4);
					$this->load->view('comment/edit', $data);
				}
				else {
					$data['commentId'] = 0;
					$data['txtError'] = "Không tìm thấy Bình luận - Đánh giá";
					$this->load->view('comment/edit', $data);
				}
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('comment');
	}

	public function update(){
		$postData = $this->arrayFromPost(array('ItemId', 'ItemTypeId', 'Comment', 'CustomerId', 'CustomerName', 'CustomerEmail', 'CustomerWeb', 'CommentStarId', 'StatusId', 'ParentCommentId'));
		if(!empty($postData['Comment'])){
			$commentId = $this->input->post('CommentId');
			if($commentId == 0) {
				$postData['UserAgent'] = $this->input->user_agent();
				$postData['IpAddress'] = $this->input->ip_address();
				$postData['CrDateTime'] = getCurentDateTime();
			}
			$this->load->model('Mcomments');
			$flag = $this->Mcomments->save($postData, $commentId);
			if ($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật Bình luận - Đánh giá thành công", 'data' => $flag));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeStatus(){
		$commentId = $this->input->post('CommentId');
		$statusId = $this->input->post('StatusId');
		if($commentId > 0 && $statusId >= 0 && $statusId <= count($this->Mconstants->status)) {
			$this->load->model('Mcomments');
			$flag = $this->Mcomments->changeStatus($statusId, $commentId);
			if($flag) {
				$txtSuccess = "";
				$statusName = "";
				if($statusId == 0) $txtSuccess = "Xóa Bình luận - Đánh giá thành công";
				else{
					$txtSuccess = "Đổi trạng thái thành công";
					$statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
				}
				echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function getList(){
		$this->load->model('Mcomments');
		$postData = $this->arrayFromPost(array('ArticleId'));
		$postData['StatusId'] = STATUS_ACTIVED;
		$listCommentRaws = $this->Mcomments->search($postData, PHP_INT_MAX, 1);
		$listComments = array();
		$data = array();
		$customers = array();
		foreach ($listCommentRaws as $c) {
			if ($c['CrUserId'] > 0) {
				if (!isset($customers[$c['CrUserId']])) $customers[$c['CrUserId']] = $this->Musers->get($c['CrUserId'], true, '', 'FullName,Avatar');
				$customer = $customers[$c['CrUserId']];
				$c['FullName'] = $customer['FullName'];
				$c['Avatar'] = empty($customer['Avatar']) ? NO_IMAGE : $customer['Avatar'];
			}
			else $c['Avatar'] = NO_IMAGE;
			$listComments[] = $c;
		}
		if($postData['ArticleId'] > 0){
			foreach ($listComments as $c) {
				if($c['ParentCommentId'] == 0){
					$c['Childs'] = array();
					foreach ($listComments as $c1) {
						if($c1['ParentCommentId'] == $c['CommentId']){
							$c['Childs'][] = $c1;
						}
					}
					$data[] = $c;
				}
			}
		}
		else $data = $listComments;
		echo json_encode(array('code' => 1, 'data' => $data));
	}
}
