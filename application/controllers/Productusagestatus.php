<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productusagestatus extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Tình trạng sử dụng',
            array('scriptFooter' => array('js' => 'js/product_usage_status.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'productusagestatus')) {
            $this->load->model('Mproductusagestatus');
            $data['listProductUsageStatus'] = $this->Mproductusagestatus->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/product_usage_status', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ProductUsageStatusName'));
        if(!empty($postData['ProductUsageStatusName'])){
            $postData['StatusId'] = STATUS_ACTIVED;
            $productUsageStatusId = $this->input->post('ProductUsageStatusId');
            $this->load->model('Mproductusagestatus');
            $flag = $this->Mproductusagestatus->save($postData, $productUsageStatusId);
            if ($flag > 0) {
                $postData['ProductUsageStatusId'] = $flag;
                $postData['IsAdd'] = ($productUsageStatusId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật Tình trạng sử dụng thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $productUsageStatusId = $this->input->post('ProductUsageStatusId');
        if($productUsageStatusId > 0){
            $this->load->model('Mproductusagestatus');
            $flag = $this->Mproductusagestatus->changeStatus(0, $productUsageStatusId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Tình trạng sử dụng thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}