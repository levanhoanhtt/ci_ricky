<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->loadModel(array('Mfbchats', 'Mfbusers', 'Mfbpages'));
    }

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Chat Facebook',
            array('scriptFooter' => array('js' => 'js/fb/chat.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'fb/chat')){
            $get_prefix = $this->input->get('prefix', TRUE);
            $pages = $this->Mfbpages->get_first_page($get_prefix);
            if (!empty($pages)) 
            {
                $fb_page_prefix_posts = $this->db->from("fb_page_".$pages['Prefix']."_posts")
                    ->where(array())
                    ->get()
                    ->result_array();

                $fb_page_prefix_messages = $this->db->from("fb_page_".$pages['Prefix']."_messages")
                    ->where(array())
                    ->get()
                    ->result_array();

                $fb_page_prefix_conversations = $this->db->from("fb_page_".$pages['Prefix']."_conversations")
                    ->where(array())
                    ->get()
                    ->result_array();

                $fb_page_prefix_comments = $this->db->from("fb_page_".$pages['Prefix']."_comments")
                    ->where(array())
                    ->get()
                    ->result_array();
            }
            $data['pages'] = $pages;
            $this->load->view('chat/index', $data);
        }
        else $this->load->view('user/permission', $data);
    }
}