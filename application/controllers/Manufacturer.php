<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manufacturer extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách Hãng sản xuất',
			array('scriptFooter' => array('js' => 'js/manufacturer.js'))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'manufacturer')) {
			$this->load->model('Mmanufacturers');
			$data['listManufacturers'] = $this->Mmanufacturers->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('setting/manufacturer', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('ManufacturerName'));
		if(!empty($postData['ManufacturerName'])) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$manufacturerId = $this->input->post('ManufacturerId');
			$this->load->model('Mmanufacturers');
			$flag = $this->Mmanufacturers->save($postData, $manufacturerId);
			if ($flag > 0) {
				$postData['ManufacturerId'] = $flag;
				$postData['IsAdd'] = ($manufacturerId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Cập nhật Hãng sản xuất thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$this->checkUserLogin(true);
		$manufacturerId = $this->input->post('ManufacturerId');
		if($manufacturerId > 0){
			$this->load->model('Mmanufacturers');
			$flag = $this->Mmanufacturers->changeStatus(0, $manufacturerId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Hãng sản xuất thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}