<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dragmenu extends MY_Controller {
    /**
     * View edit menu
     */
    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Cài đặt menu',
            array(
                'scriptHeader' => array(
                    'css' => array(
                        'vendor/menucustom/css/jquery.nestable.css',
                        'vendor/menucustom/css/core.css',
                    )
                ),
                'scriptFooter' => array(
                    'js' => array(
                        'js/menu_update.js',
                        'vendor/menucustom/js/jquery.uniform.min.js',
                        'vendor/menucustom/js/bootstrap-datepicker.min.js',
                        'vendor/menucustom/js/core.js',
                        'vendor/menucustom/js/jquery.waypoints.min.js',
                        'vendor/menucustom/js/jquery.nestable.js',
                        'vendor/menucustom/js/jquery-ui.min.js',
                        'vendor/menucustom/js/menu.js',
                    )
                )
            )
        );
        $this->loadModel(array('Mmenus', 'Mcategories', 'Marticles', 'Mmenuitems'));
        $id = isset($_GET['id'])?$_GET['id']:0;
        $data['menuEdit'] = $this->Mmenus->getMenuEdit($id);
        $data['menuProduct'] = $this->Mcategories->getBy(array('ItemTypeId' => 1, 'StatusId' => STATUS_ACTIVED));
        $data['menuNews'] = $this->Mcategories->getBy(array('ItemTypeId' => 4, 'StatusId' => STATUS_ACTIVED));
        $data['menuArticles'] = $this->Marticles->getPage();
        $data['menuId'] = $id;
        $data['menuParentItems'] = $this->Mmenuitems->getWithCondition(['ParentItemId'=>0, 'MenuId'=>$id]);
        $data['groupChildrenItems'] = $this->Mmenuitems->getChildrenItem($id);
        $this->load->view('setting/drag_menu', $data);
    }
    
    /**
     * Create menu with item
     */
    public function createMenuItem() {
        $user = $this->checkUserLogin();
        $postData = json_decode($this->input->post('value'), true);
        $menuId = $this->input->post('menuId');
        $this->loadModel(array('Mmenuitems', 'Mmenus'));
        $dataMenu['Name'] = $this->input->post('menuName');
        
        if ($dataMenu['Name']=="") {
            echo json_encode(array('code' => -1, 'message' => "Tên menu không được bỏ trống"));
        }elseif ($this->Mmenus->checkExist($menuId, $dataMenu['Name']) > 0) {
            echo json_encode(array('code' => -1, 'message' => "Tên menu đã tồn tại!"));
        }
        else {
            $this->Mmenus->update($dataMenu, $menuId);
            $createMenuItem = $this->Mmenuitems->createMenuItem($user, $postData, $menuId);
            if ($createMenuItem) {
                echo json_encode(array('code' => 1, 'message' => "Cập nhật menu thành công"));
            } else {
                echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
        }
        
    }
}
