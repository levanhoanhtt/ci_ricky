<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offsettype extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách loại gửi bù',
            array('scriptFooter' => array('js' => 'js/offset_type.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'offsettype')) {
            $this->load->model('Moffsettypes');
            $data['listOffsetTypes'] = $this->Moffsettypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/offset_type', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('OffsetTypeName'));
        if(!empty($postData['OffsetTypeName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $offsetTypeId = $this->input->post('OffsetTypeId');
            $this->load->model('Moffsettypes');
            $flag = $this->Moffsettypes->save($postData, $offsetTypeId);
            if ($flag > 0) {
                $postData['OffsetTypeId'] = $flag;
                $postData['IsAdd'] = ($offsetTypeId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật loại gửi bù thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $offsetTypeId = $this->input->post('OffsetTypeId');
        if($offsetTypeId > 0){
            $this->load->model('Moffsettypes');
            $flag = $this->Moffsettypes->changeStatus(0, $offsetTypeId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa loại gửi bù thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}
