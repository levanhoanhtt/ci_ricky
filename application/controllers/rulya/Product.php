<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('cart');
        $this->loadModel(array('Mconfigs'));
    }

    public function index(){
        $data = array('title' => 'Chi tiết sản phẩm - RULYA');
        $this->load->view('rulya/home', $data);
	}
    /*public function products( $page = 1 ){
        $this->loadModel(array('Mproducts', 'Mproductchilds', 'Msuppliers', 'Mcategories'));
        $data = array('title' => 'Sản phẩm - RULYA');
        $data = array(
            'bodyClass' => 'out-home',
            'commonData' => $this->commonDataSite(),
            'listSuppliers' => $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED)),
            'listCategories' => $this->Mcategories->getListByItemType(1, false, false, true),
            'categoryUrlPage' => base_url('rulya/san-pham-trang-{$1}.html'),
            'pageTitle' => 'Tất cả sản phẩm',
            'categoryId' => 0
        );
        $data = array_merge($data, $this->commonDataSite('Danh sách sản phẩm - '));
        $data['configSites']['pageUrl'] = base_url('products');
        if (!is_numeric($page)) $page = 1;
        if ($page <= 0) $page = 1;
        $productCount = $this->Mproducts->getCount(array('ProductStatusId' => STATUS_ACTIVED, 'ProductDisplayTypeId' => 1));
        $pageCount = 1;
        $listProducts = array();
        if ($productCount > 0) {
            $perPage = 20;
            $pageCount = ceil($productCount / $perPage);
            if ($page > $pageCount) $page = $pageCount;
            $listProducts = $this->Mproducts->search(array('ProductStatusId' => STATUS_ACTIVED, 'ProductDisplayTypeId' => 1), $perPage, $page);
        }
        $data['pageCurrent'] = $page;
        $data['pageCount'] = $pageCount;
        $data['listProducts'] = $listProducts;
        $this->load->view('rulya/all_products', $data);
    }*/

    public function vision(){
        $data = array('title' => 'Tầm nhìn - RULYA');
        $this->load->view('rulya/vision', $data);
    }

    public function contact(){
        $data = array('title' => 'Liên hệ - RULYA');
        $this->load->view('rulya/contact', $data);
    }

    public function blog(){
        $data = array('title' => 'Blog - RULYA');
        $this->load->view('rulya/blog', $data);
    }

    public function detail($productId){
        $data = array('title' => 'Chi tiết sản phẩm - RULYA');
        // lay thong tin product
        if ($productId > 0) {
            $this->loadModel(array('Mproducts'));
            $product = $this->Mproducts->get($productId);
            $data['product'] = $product;
        }

        $this->load->view('rulya/detail', $data);
    }

    public function account(){
        $data = array('title' => 'Tài khoản - RULYA');
        $this->load->view('rulya/account', $data);
    }
}
