<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('cart');
        $this->loadModel(array('Mconfigs', 'Mcategories'));
        $this->load->model('Mconfigs');
    }

    private function commonDataSite($title = ''){
        $configSites =  $this->Mconfigs->getListMap();
        return array(
            'title' => $title . $configSites['COMPANY_NAME'],
            'totalItemCart' => $this->cart->total_items(),
            'totalPriceCart' => $this->cart->contents(),
            'configSites' => $configSites,
            'listProductCategories' => $this->Mcategories->getListByItemType(1, false, false, true)
        );
    }

    public function index(){
        $this->loadModel(array('Msliders', 'Mproducts', 'Mcategories'));
        $data = array(
            'listSliders' => $this->Msliders->getList(2),
            'listProducts' => $this->Mproducts->search(array('CategoryId' => 1), 8, 1, 'PublishDateTime')
        );
        $data = array_merge($data, $this->commonDataSite('Trang chủ - '));
        $data['configSites']['pageUrl'] = base_url();
        $this->load->view('rulya/home', $data);
	}

    public function allProduct($page = 1){
        $this->loadModel(array('Mproducts', 'Mcategories'));
        $data = array(
            'categoryUrlPage' => base_url('shop/trang-{$1}.html'),
            'pageTitle' => 'Tất cả sản phẩm',
            'categoryId' => 0
        );
        $data = array_merge($data, $this->commonDataSite('Danh sách sản phẩm - '));
        $data['configSites']['pageUrl'] = base_url('shop');
        if (!is_numeric($page)) $page = 1;
        if ($page <= 0) $page = 1;
        $productCount = $this->Mproducts->getCount(array('ProductStatusId' => STATUS_ACTIVED, 'ProductDisplayTypeId' => 1));
        $pageCount = 1;
        $listProducts = array();
        if ($productCount > 0) {
            $perPage = 12;
            $pageCount = ceil($productCount / $perPage);
            if ($page > $pageCount) $page = $pageCount;
            $listProducts = $this->Mproducts->search(array('ProductStatusId' => STATUS_ACTIVED, 'ProductDisplayTypeId' => 1), $perPage, $page);
        }
        $data['pageCurrent'] = $page;
        $data['pageCount'] = $pageCount;
        $data['listProducts'] = $listProducts;
        $this->load->view('rulya/all_products', $data);
    }

    public function categoryProduct($categorySlug = '', $page = 1){
        if(!empty($categorySlug)) {
            $this->loadModel(array('Mproducts', 'Mcategories'));
            $category = $this->Mcategories->getBy(array('CategorySlug' => $categorySlug, 'StatusId' => STATUS_ACTIVED, 'ItemTypeId' => 1), true);
            if($category) {
                $data = array(
                    'categoryUrlPage' => base_url('danh-muc/'.$categorySlug.'/trang-{$1}.html'),
                    'pageTitle' => $category['CategoryName'],
                    'categoryId' => $category['CategoryId']
                );
                $data = array_merge($data, $this->commonDataSite($category['CategoryName'].' - '));
                $data['configSites']['pageUrl'] = base_url('danh-muc/'.$categorySlug);
                if (!is_numeric($page)) $page = 1;
                if ($page <= 0) $page = 1;
                $searchData = array('ProductStatusId' => STATUS_ACTIVED, 'ProductDisplayTypeId' => 1, 'CategoryId' => $category['CategoryId']);
                $productCount = $this->Mproducts->getCount($searchData);
                $pageCount = 1;
                $listProducts = array();
                if ($productCount > 0) {
                    $perPage = 12;
                    $pageCount = ceil($productCount / $perPage);
                    if ($page > $pageCount) $page = $pageCount;
                    $listProducts = $this->Mproducts->search($searchData, $perPage, $page);
                }
                $data['pageCurrent'] = $page;
                $data['pageCount'] = $pageCount;
                $data['listProducts'] = $listProducts;
                $this->load->view('rulya/all_products', $data);
            }
            else redirect(base_url());
        }
        else redirect(base_url());
    }

    public function detailProduct($productSlug = ''){
        if(!empty($productSlug)){
            $this->loadModel(array('Mproducts', 'Mcategories', 'Mitemmetadatas', 'Mfiles', 'Mcomments'));
            $product = $this->Mproducts->getBy(array('ProductSlug' => $productSlug, 'ProductStatusId' => STATUS_ACTIVED, 'ProductDisplayTypeId' => 1), true);
            if($product){
                $productId = $product['ProductId'];
                $data = array(
                    'product' => $product,
                    'listProducts' => $this->Mproducts->getOtherProducts($productId, 4),
                    'listCategories' => $this->Mcategories->getListByItem($productId, 3),
                    'productUrl' => $this->Mconstants->getUrl($productSlug, $productId, 3, 2),
                    'listComments' => $this->Mcomments->search(array('ItemId' => $productId, 'ItemTypeId' => 3)),
                    'scriptFooter' => array('js' => 'front/rulya/js/product_detail.js')
                );
                $data = array_merge($data, $this->commonDataSite($product['ProductName'].' - '));
                $data['configSites']['pageUrl'] = $data['productUrl'];
                //$data['listImages'] = $this->Mfiles->getFileUrls($productId, 3, 1);
                $productSeo = $this->Mitemmetadatas->getBy(array('ItemId' => $productId, 'ItemTypeId' => 3), true);
                if(!empty($productSeo['TitleSEO'])) $data['title'] = $productSeo['TitleSEO'] . $data['configSites']['COMPANY_NAME'];
                if(!empty($productSeo['MetaDesc'])) $data['configSites']['META_DESC'] = $productSeo['MetaDesc'];
                $this->load->view('rulya/detail_product', $data);
            }
            else redirect(base_url());
        }
        else redirect(base_url());
    }

    public function blog($page = 1 ){
        $this->loadModel(array('Marticles'));
        if (!is_numeric($page)) $page = 1;
        if ($page <= 0) $page = 1;
        $postData = array('ArticleTypeId' => 1, 'ArticleStatusId' => STATUS_ACTIVED);
        $blogCount = $this->Marticles->getCount($postData);
        $pageCount = 1;
        $listBlogs = array();
        if ($blogCount > 0) {
            $perPage = 9;
            $pageCount = ceil($blogCount / $perPage);
            if ($page > $pageCount) $page = $pageCount;
            $listBlogs = $this->Marticles->search($postData, $perPage, $page);
        }
        $data = array(
            'categoryUrlPage' => base_url('blog/trang-{$1}'),
            'pageCurrent' => $page,
            'pageCount' => $pageCount,
            'listBlogs' => $listBlogs,
            'listNews' => $this->Marticles->search($postData, 5, 1)
        );
        $data = array_merge($data, $this->commonDataSite('Tin tức - '));
        $data['configSites']['pageUrl'] = base_url('blog');
        $this->load->view('rulya/blog', $data);
    }

    public function detailBlog($articleSlug = 0){
        if(!empty($articleSlug)){
            $this->loadModel(array('Marticles'));
            $article = $this->Marticles->getBy(array('ArticleSlug' => $articleSlug, 'ArticleStatusId' => STATUS_ACTIVED, 'ArticleTypeId' => 1), true);
            if($article){
                $data = array(
                    'article' => $article,
                    'listNews' => $this->Marticles->search(array('ArticleStatusId' => STATUS_ACTIVED, 'ArticleTypeId' => 1), 5, 1),
                    'scriptFooter' => array('js' => 'front/rulya/js/blog_detail.js')
                );
                $data = array_merge($data, $this->commonDataSite($article['ArticleTitle'].' - '));
                $data['configSites']['pageUrl'] = base_url('blog/' . $articleSlug);
                $this->load->view('rulya/detail_blog', $data);
            }
            else redirect(base_url());
        }
        else redirect(base_url());
    }

    public function contact(){
        $this->loadModel(array('Marticles'));
        $data = $this->commonDataSite('Liên hệ - ');
        $data['listNews']=$this->Marticles->search(array('ArticleStatusId' => STATUS_ACTIVED, 'ArticleTypeId' => 1), 5, 1);
        $data['articleContent'] = $this->Marticles->getFieldValue(array('ArticleId' => 2), 'ArticleContent');
        $data['scriptFooter'] = array('js' => 'front/rulya/js/contact.js');
        $data['configSites']['pageUrl'] = base_url('lien-he');
        $this->load->view('rulya/contact', $data);
    }

    public function introduction(){
        $data = $this->commonDataSite('Giới thiệu chung - ');
        $data['configSites']['pageUrl'] = base_url('gioi-thieu-chung');
        $this->load->view('rulya/introduction', $data);
    }

    public function vision(){
        $this->loadModel(array('Marticles'));
        $data = $this->commonDataSite('Tầm nhìn - Sứ mệnh - ');
        $data['listNews'] = $this->Marticles->search(array('ArticleStatusId' => STATUS_ACTIVED, 'ArticleTypeId' => 1), 5, 1);
        $data['articleContent'] = $this->Marticles->getFieldValue(array('ArticleId' => 1), 'ArticleContent');
        $data['configSites']['pageUrl'] = base_url('tam-nhin-su-menh');
        $this->load->view('rulya/vision', $data);
    }

    public function account(){
        $data = $this->commonDataSite('Tài khoản - ');
        $data['scriptFooter'] = array('js' => 'front/rulya/js/account.js');
        $data['configSites']['pageUrl'] = base_url('tai-khoan');
        $this->load->view('rulya/account', $data);
    }

    // public function cart(){
    //     $data = $this->commonDataSite('Giỏ hàng - ');
    //     $data['configSites']['pageUrl'] = base_url('gio-hang');
    //     $this->load->view('rulya/cart', $data);
    // }

    public function cart(){
        $this->loadModel(array('Mprovinces', 'Mdistricts'));
        $data = array(
            'bodyClass' => 'out-home',
            'carts' => $this->cart->contents(),
            'listProvinces' => $this->Mprovinces->getList()
        );
        $data = array_merge($data, $this->commonDataSite('Giỏ hàng - '));
        $data['configSites']['pageUrl'] = base_url('gio-hang');
        $this->load->view('rulya/cart', $data);
    }

    public function agency(){
        redirect(base_url());
        /*$data = $this->commonDataSite('Đại lý - ');
        $data['configSites']['pageUrl'] = base_url('dai-ly');
        $this->load->view('rulya/agency', $data);*/
    }

}
