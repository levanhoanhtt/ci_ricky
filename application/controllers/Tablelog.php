<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tablelog extends MY_Controller {

    private $tableInfo = array(
        6 => array(
            'TableName' => 'orders',
            'PrimaryKeyName' => 'OrderId',
            'StatusFieldName' => 'OrderStatusId',
            'ItemTypeName' => 'đơn hàng'
        ),
        7 => array(
            'TableName' => 'storecirculations',
            'PrimaryKeyName' => 'StoreCirculationId',
            'StatusFieldName' => 'StoreCirculationStatusId',
            'ItemTypeName' => 'lưu chuyển kho'
        ),
        8 => array(
            'TableName' => 'imports',
            'PrimaryKeyName' => 'ImportId',
            'StatusFieldName' => 'ImportStatusId',
            'ItemTypeName' => 'nhập kho'
        ),
        9 => array(
            'TableName' => 'transports',
            'PrimaryKeyName' => 'TransportId',
            'StatusFieldName' => 'TransportStatusId',
            'ItemTypeName' => 'vận chuyển'
        ),
        17 => array(
            'TableName' => 'transactions',
            'PrimaryKeyName' => 'TransactionId',
            'StatusFieldName' => 'TransactionStatusId',
            'ItemTypeName' => 'phiếu thu'
        ),
        18 => array(
            'TableName' => 'transactions',
            'PrimaryKeyName' => 'TransactionId',
            'StatusFieldName' => 'TransactionStatusId',
            'ItemTypeName' => 'phiếu chi'
        ),
        20 => array(
            'TableName' => 'transactioninternals',
            'PrimaryKeyName' => 'TransactionInternalId',
            'StatusFieldName' => 'TransactionStatusId',
            'ItemTypeName' => 'phiếu thu'
        ),
        21 => array(
            'TableName' => 'transactioninternals',
            'PrimaryKeyName' => 'TransactionInternalId',
            'StatusFieldName' => 'TransactionStatusId',
            'ItemTypeName' => 'phiếu chi'
        ),


    );

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Lịch sử xóa',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_item.js', 'js/tablelog_list.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'tablelog')) {
            $this->load->model(array('Mfilters','Mtags'));
            $itemTypeId = 34;
            $data['listUsers'] = $this->Musers->getListForSelect();
            $data['listFilters'] = $this->Mfilters->getList($itemTypeId);
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => $itemTypeId));
            $this->load->view('tablelog/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function insert(){
        $user = $this->checkUserLogin(true);
        // if($user['UserId'] == 3) {
            $itemIds = trim($this->input->post('ItemIds'));
            $itemTypeId = $this->input->post('ItemTypeId');
            $comment = trim($this->input->post('Comment'));
            if (!empty($itemIds) && !empty($comment) && $itemTypeId > 0) {
                if (isset($this->tableInfo[$itemTypeId])) {
                    $tableInfo = $this->tableInfo[$itemTypeId];
                    $tableName = $tableInfo['TableName'];
                    $primaryKeyName = $tableInfo['PrimaryKeyName'];
                    $statusFieldName = $tableInfo['StatusFieldName'];
                    $itemTypeName = $tableInfo['ItemTypeName'];
                    $modelName = 'M' . $tableName;
                    $this->loadModel(array($modelName, 'Mtablelogs'));
                    $crDateTime = getCurentDateTime();
                    $tableLogs = array();
                    $actionLogs = array();
                    $itemIds = json_decode($itemIds, true);
                    foreach ($itemIds as $itemId) {
                        $tableLogs[] = array(
                            'ItemId' => $itemId,
                            'ItemTypeId' => $itemTypeId,
                            'TableName' => $tableName,
                            'PrimaryKeyName' => $primaryKeyName,
                            'StatusIdOld' => $this->$modelName->getFieldValue(array($primaryKeyName => $itemId), $statusFieldName, 0),
                            'IsBack' => 1,
                            'Comment' => $comment,
                            'CrUserId' => $user['UserId'],
                            'CrDateTime' => $crDateTime
                        );
                        $actionLogs[] = array(
                            'ItemId' => $itemId,
                            'ItemTypeId' => $itemTypeId,
                            'ActionTypeId' => 2,
                            'Comment' => $user['FullName'] . ' xóa ' . $itemTypeName,
                            'CrUserId' => $user['UserId'],
                            'CrDateTime' => $crDateTime
                        );
                    }
                    $tableInfo['UserId'] = $user['UserId'];
                    $tableInfo['CrDateTime'] = $crDateTime;
                    $tableInfo['ItemIds'] = $itemIds;
                    $flag = $this->Mtablelogs->insert($tableLogs, $actionLogs, $tableInfo);
                    if ($flag) echo json_encode(array('code' => 1, 'message' => "Xóa {$itemTypeName} thành công"));
                    else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                }
                else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        // }
        // else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        if($user['UserId'] == 3) {
            $tableLogId = $this->input->post('TableLogId');
            if($tableLogId > 0) {
                $this->loadModel(array('Mtablelogs', 'Mactionlogs'));
                $tableLog = $this->Mtablelogs->get($tableLogId);
                if($tableLog) {
                    $crDateTime = getCurentDateTime();
                    $postData = array(
                        'IsBack' => 2,
                        'UpdateUserId' => $user['UserId'],
                        'UpdateDateTime' => $crDateTime
                    );
                    $itemTypeId = $tableLog['ItemTypeId'];
                    $actionLog = array(
                        'ItemId' => $tableLog['ItemId'],
                        'ItemTypeId' => $itemTypeId,
                        'ActionTypeId' => 2,
                        'Comment' => $user['FullName'] . ' phục hồi ' . $this->tableInfo[$itemTypeId]['ItemTypeName'],
                        'CrUserId' => $user['UserId'],
                        'CrDateTime' => $crDateTime
                    );
                    $modelName = "M" . $tableLog["TableName"];
                    $this->load->model($modelName);
                    $tableInfo = array(
                        'ModelName' => $modelName,
                        'ItemId' => $tableLog['ItemId'],
                        'StatusIdOld' => $tableLog['StatusIdOld'],
                        'StatusFieldName' => $this->tableInfo[$itemTypeId]['StatusFieldName']
                    );
                    $flag = $this->Mtablelogs->restore($postData, $tableLogId, $actionLog, $tableInfo);
                    if($flag) echo json_encode(array('code' => 1, 'message' => 'Khôi phục thành công'));
                    else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                }
                else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function searchByFilter($searchTypeId = 0){
        $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $postData['SearchTypeId'] = $searchTypeId;
        $this->load->model('Mtablelogs');
        $data1 = $this->Mtablelogs->searchByFilter($searchText, $itemFilters, $limit, $page, $postData);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }
}