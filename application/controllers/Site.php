<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('cart');
        $this->load->model('Mconfigs');
	}

	private function commonDataSite($title = ''){
		$configSites =  $this->Mconfigs->getListMap();
		return array(
			'title' => $title . $configSites['COMPANY_NAME'],
			'totalItemCart' => $this->cart->total_items(),
			'configSites' => $configSites
		);
	}

    public function index(){
        $this->load->model('Mproducts');
		$data = array(
			'bodyClass' => 'in-home',
			'listSoundCards' => $this->Mproducts->search(array('ProductStatusId' => STATUS_ACTIVED, 'ProductDisplayTypeId' => 1, 'CategoryId' => 3), 10, 1),
			'listSells' => $this->Mproducts->getListBestSell(10),
		);
		$data = array_merge($data, $this->commonDataSite());
		$data['configSites']['pageUrl'] = base_url();
		$this->load->view('site/home', $data);
	}

	public function allProduct($page = 1){
		$this->loadModel(array('Mproducts', 'Mproductchilds', 'Msuppliers', 'Mcategories'));
		$data = array(
			'bodyClass' => 'out-home',
			'listSuppliers' => $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED)),
			'listCategories' => $this->Mcategories->getListByItemType(1, false, false, true),
			'categoryUrlPage' => base_url('san-pham-trang-{$1}.html'),
			'pageTitle' => 'Tất cả sản phẩm',
			'categoryId' => 0
		);
		$data = array_merge($data, $this->commonDataSite('Danh sách sản phẩm - '));
		$data['configSites']['pageUrl'] = base_url('san-pham.html');
		if (!is_numeric($page)) $page = 1;
		if ($page <= 0) $page = 1;
		$productCount = $this->Mproducts->getCount(array('ProductStatusId' => STATUS_ACTIVED, 'ProductDisplayTypeId' => 1));
		$pageCount = 1;
		$listProducts = array();
		if ($productCount > 0) {
			$perPage = 20;
			$pageCount = ceil($productCount / $perPage);
			if ($page > $pageCount) $page = $pageCount;
			$listProducts = $this->Mproducts->search(array('ProductStatusId' => STATUS_ACTIVED, 'ProductDisplayTypeId' => 1), $perPage, $page);
		}
		$data['pageCurrent'] = $page;
		$data['pageCount'] = $pageCount;
		$data['listProducts'] = $listProducts;
		$this->load->view('site/all_products', $data);
	}

	public function categoryProduct($categorySlug = '', $categoryId = 0, $page = 1){
		if($categoryId > 0) {
			$this->loadModel(array('Mproducts', 'Mproductchilds', 'Msuppliers', 'Mcategories'));
			$category = $this->Mcategories->get($categoryId);
			if($category && $category['StatusId'] == 2 && $category['ItemTypeId'] == 1) {
				if($category['CategorySlug'] == $categorySlug) {
					$data = array(
						'bodyClass' => 'out-home',
						'listSuppliers' => $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED)),
						'listCategories' => $this->Mcategories->getListByItemType(1, false, false, true),
						'categoryUrlPage' => base_url($category['CategorySlug'].'-c'.$categoryId.'-trang-{$1}.html'),
						'pageTitle' => $category['CategoryName'],
						'categoryId' => $categoryId
					);
					$data = array_merge($data, $this->commonDataSite($category['CategoryName'].' - '));
					$data['configSites']['pageUrl'] = base_url($category['CategorySlug'].'-c'.$categoryId.'.html');
					if (!is_numeric($page)) $page = 1;
					if ($page <= 0) $page = 1;
					$searchData = array('ProductStatusId' => STATUS_ACTIVED, 'ProductDisplayTypeId' => 1, 'CategoryId' => $categoryId);
					$productCount = $this->Mproducts->getCount($searchData);
					$pageCount = 1;
					$listProducts = array();
					if ($productCount > 0) {
						$perPage = 20;
						$pageCount = ceil($productCount / $perPage);
						if ($page > $pageCount) $page = $pageCount;
						$listProducts = $this->Mproducts->search($searchData, $perPage, $page);
					}
					$data['pageCurrent'] = $page;
					$data['pageCount'] = $pageCount;
					$data['listProducts'] = $listProducts;
					$this->load->view('site/all_products', $data);
				}
				else redirect(base_url($category['CategorySlug'].'-c'.$categoryId.'-trang-'.$page.'.html'));
			}
			else redirect(base_url());
		}
		else redirect(base_url());
	}

	public function detailProduct($productSlug = ''){//, $productId = 0){
		//if($productId > 0){
		if(!empty($productSlug)){
            $this->loadModel(array('Mproducts', 'Mproducttypes', 'Mcategories', 'Msuppliers', 'Mtags', 'Mcategoryitems', 'Mitemmetadatas', 'Mfiles', 'Mvariants', 'Mproductchilds'));
			$this->load->helper('slug');
            //$product = $this->Mproducts->get($productId);
			$product = $this->Mproducts->getBy(array('ProductSlug' => $productSlug, 'ProductStatusId >' => 0, 'ProductWebStatusId >' => 0,  'ProductDisplayTypeId' => 1), true);
			if($product){
				$productId = $product['ProductId'];
				//if($product['ProductSlug'] == $productSlug){
				$data = array(
					'bodyClass' => 'out-home',
					'product' => $product,
					'listProducts' => $this->Mproducts->getOtherProducts($productId),
					'productUrl' => $this->Mconstants->getUrl($productSlug, $productId, 3),
					'scriptFooter' => array('js' => 'front/v1/js/product_detail.js?20171107')
				);
				$data = array_merge($data, $this->commonDataSite($product['ProductName'].' - '));
				$data['configSites']['pageUrl'] = $data['productUrl'];
				$data['listImages'] = $this->Mfiles->getFileUrls($productId, 3, 1);
				$productSeo = $this->Mitemmetadatas->getBy(array('ItemId' => $productId, 'ItemTypeId' => 3), true);
				if(!empty($productSeo['TitleSEO'])) $data['title'] = $productSeo['TitleSEO'] . $data['configSites']['COMPANY_NAME'];
				if(!empty($productSeo['MetaDesc'])) $data['configSites']['META_DESC'] = $productSeo['MetaDesc'];
				$data['listVariants'] = $this->Mvariants->get();
				$data['variants'] = array();
				$data['listProductChilds'] = array();
				if($product['ProductKindId'] == 2) {
					$listProductChilds = $this->Mproductchilds->getByProductId($productId);
					$numbers_price = array_column($listProductChilds, 'Price');
					if(!empty($numbers_price)) 	$min_price = min($numbers_price);
					else $min_price = 0;
					$data['listProductChilds'] = $listProductChilds;
					$variants = array();
					foreach($listProductChilds as $pc){
						if($pc['VariantId1'] > 0) $variants[$pc['VariantId1']][] = $pc['VariantValue1'];
						if($pc['VariantId2'] > 0) $variants[$pc['VariantId2']][] = $pc['VariantValue2'];
						if($pc['VariantId3'] > 0) $variants[$pc['VariantId3']][] = $pc['VariantValue3'];
					}
					$data['variants'] = $variants;
					$data['min_price'] = $min_price;
					$data['listProductChilds'] = $listProductChilds;
				}
				//check cookie affiliate
				$linkAffId = isset($_GET['affiliate_id']) ? intval($_GET['affiliate_id']) : 0;
				$ctvId = isset($_GET['ctv_id']) ? intval($_GET['ctv_id']) : 0;
				if($linkAffId > 0 && $ctvId > 0){
					/*$cookie_key = 'User_LinkAffId_' . $linkAffId;
					if(isset($_COOKIE[$cookie_key])){
						$listLinkAffs = $this->checkProductAffiliate($productId, $linkAffId);
						if(!empty($listLinkAffs)){
							if($listLinkAffs[0]['CustomerId'] != $ctvId){
								$linkAffId = 0;
								$ctvId = 0;
							}
						}
						else{
							$linkAffId = 0;
							$ctvId = 0;
						}
					}
					else{
						$linkAffId = 0;
						$ctvId = 0;
					}*/
				}
				else {
					$linkAffId = 0;
					$ctvId = 0;
					$listLinkAffs = $this->checkProductAffiliate($productId);
					foreach ($listLinkAffs as $linkAff) {
						$cookie_key = 'User_LinkAffId_' . $linkAff['LinkAffId'];
						if(isset($_COOKIE[$cookie_key])) {
							$linkAffId = $linkAff['LinkAffId'];
							$ctvId = $linkAff['CustomerId'];
							break;
						}
					}
				}
				$data['linkAffId'] = $linkAffId;
				$data['ctvId'] = $ctvId;
				$this->load->view('site/detail_products', $data);
				//} else redirect(base_url($product['ProductSlug'].'-p'.$productId.'.html'));
			}
			else redirect(base_url());
		}
		else redirect(base_url());
	}

	private function checkProductAffiliate($productId){
		$retVal = array();
		$this->load->helper('slug');
		$json = curlCrawl('https://aff.ricky.vn/api/link/getListByProductId', "UserName=hoanmuada&UserPass=123456789&ProductPartnerId={$productId}");
		$json = @json_decode($json, true);
		if($json['code'] == 1) $retVal = $json['data'];
		return $retVal;
	}

	public function search(){
		//$this->load->helper('security');
		$q = isset($_GET['q']) ? trim($_GET['q']) : '';
		//$q = xss_clean($q);
		$this->loadModel(array('Mproducts', 'Mproductchilds', 'Msuppliers', 'Mcategories'));
		$data = array(
			'bodyClass' => 'out-home',
			'listSuppliers' => $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED)),
			'listCategories' => $this->Mcategories->getListByItemType(1, false, false, true),
			'categoryUrlPage' => base_url('tim-kiem.html?type=product&q=Loa&page={$1}'),
			'pageTitle' => 'Tìm kiếm sản phẩm',
			'categoryId' => 0
		);
		$data = array_merge($data, $this->commonDataSite('Tìm kiếm - '));
		$data['configSites']['pageUrl'] = base_url('tim-kiem.html?q='.$q);
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		if (!is_numeric($page)) $page = 1;
		if ($page <= 0) $page = 1;
		$searchData = array('ProductStatusId' => STATUS_ACTIVED, 'ProductDisplayTypeIds' => '1,2', 'SearchText' => $q, 'IsSearchExactly' => 1);
		$productCount = $this->Mproducts->getCount($searchData);
		$pageCount = 1;
		$listProducts = array();
		if ($productCount > 0) {
			$perPage = 20;
			$pageCount = ceil($productCount / $perPage);
			if($page > $pageCount) $page = $pageCount;
			$listProducts = $this->Mproducts->search($searchData, $perPage, $page);
		}
		$data['pageCurrent'] = $page;
		$data['pageCount'] = $pageCount;
		$data['listProducts'] = $listProducts;
		$this->load->view('site/all_products', $data);
	}

	public function page($articleSlug = ''){
		if(!empty($articleSlug)){
			$this->loadModel(array('Marticles'));
			$article = $this->Marticles->getBy(array('ArticleSlug' => $articleSlug, 'ArticleStatusId' => STATUS_ACTIVED), true);
			if($article){
				$data = array(
					'bodyClass' => 'out-home',
					'article' => $article,
					'articleUrl' => $this->Mconstants->getUrl($articleSlug, $article['ArticleId'], 4)
				);
				$data = array_merge($data, $this->commonDataSite($article['ArticleTitle'].' - '));
				$data['configSites']['pageUrl'] = $data['articleUrl'];
				$this->load->view('site/detail_articles', $data);
			}
			else redirect(base_url());
		}
		else redirect(base_url());
	}

	public function cart(){
		//$this->loadModel(array('Mprovinces', 'Mdistricts'));
		$data = array(
			'bodyClass' => 'out-home',
			'carts' => $this->cart->contents()
			//'listProvinces' => $this->Mprovinces->getList()
		);
		$data = array_merge($data, $this->commonDataSite('Giỏ hàng - '));
		$data['configSites']['pageUrl'] = base_url('gio-hang.html');
		$this->load->view('site/cart', $data);
	}

	public function news($page = 1){
        $this->loadModel(array('Marticles'));
        $data = array(
            'bodyClass' => 'out-home',
            'listArticles' => $this->Marticles->getBy(array('ArticleStatusId' => STATUS_ACTIVED)),
            'categoryUrlPage' => base_url('tin-tuc-trang-{$1}.html'),
            'pageTitle' => 'Tin tức',
            'categoryId' => 0
        );
        $data = array_merge($data, $this->commonDataSite('Tin tức - '));
        $data['configSites']['pageUrl'] = base_url('tin-tuc.html');
        if (!is_numeric($page)) $page = 1;
        if ($page <= 0) $page = 1;
        $categoryId = 16;
        $articlesCount = $this->Marticles->getCount(array('ArticleStatusId' => STATUS_ACTIVED, 'CategoryId' => $categoryId));
        $pageCount = 1;
        $listArticles = array();
        if ($articlesCount > 0) {
            $perPage = 5;
            $pageCount = ceil($articlesCount / $perPage);
            if ($page > $pageCount) $page = $pageCount;
            $listArticles = $this->Marticles->search(array('ArticleStatusId' => STATUS_ACTIVED, 'CategoryId' => $categoryId), $perPage, $page);
        }
        $data['pageCurrent'] = $page;
        $data['pageCount'] = $pageCount;
        $data['listArticles'] = $listArticles;
        $data['listArticlesNews'] = $this->Marticles->search(array('ArticleStatusId' => STATUS_ACTIVED, 'CategoryId' => $categoryId), 3, 1);
        $this->load->view('site/news', $data);
    }
    public function detailNews($articleSlug = ''){
        if(!empty($articleSlug)){
            $this->loadModel(array('Marticles'));
            $article = $this->Marticles->getBy(array('ArticleSlug' => $articleSlug, 'ArticleStatusId' => STATUS_ACTIVED), true);
            if($article){
                $data = array(
                    'bodyClass' => 'out-home',
                    'article' => $article,
                    'articleUrl' => $this->Mconstants->getUrl($articleSlug, $article['ArticleId'], 5),
                    'pageTitle' => 'Tin tức',
                );
                $data = array_merge($data, $this->commonDataSite($article['ArticleTitle'].' - '));
                $data['configSites']['pageUrl'] = $data['articleUrl'];
                $categoryId = 16;
                $data['listArticlesNews'] = $this->Marticles->$this->Marticles->search(array('ArticleStatusId' => STATUS_ACTIVED, 'CategoryId' => $categoryId), 3, 1);
                $this->load->view('site/detail_news', $data);
            }
            else redirect(base_url());
        }
        else redirect(base_url());
    }
}