<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facebook extends MY_Controller {

    public $keyFaceBook = array(
        'pageId'    => '1759192184381061',
        'appId'     => '2020899998139436',
        'appToken'  => '2020899998139436|dvRUN1GCgvUv2P3aP3EXQqSrLlk',
        'userToken' => 'EAActZC3KEBCwBAJ1MMVJRXhce86XybZCx3bl1dfdmgYEH0sTrnmtJXN78nZBiXgUtxLhr9JSOVGZBjoWYCbpGliHI8eGJT4mCCLUWIjShoYOEPmGt5R7DIdbVfFMHy6KIFljCOvNlggqGIyYIN3HgNbC01K0annZCAenIKzkdcwZDZD'
    );

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Chat Facebook',
            array(
                'scriptHeader' => array('css' => array('vendor/dist/fb/facebook.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                'scriptFooter' => array('js' => array('ckfinder/ckfinder.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/moment/moment.min.js','js/facebook.js'))
            )
        );

        if($this->Mactions->checkAccess($data['listActions'], 'facebook')){
            $this->loadModel(array('Msentencegroups','Mfbpages'));
            $data['keyFaceBook'] = $this->keyFaceBook;
            $data['listSentenceGroups'] = $this->Msentencegroups->get();
            $data['listFbpages'] = $this->Mfbpages->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('facebook/index', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function getDataFbUser(){
        $this->checkUserLogin();
        $fbPageCode = $this->input->post('FbPageId');
        $viewStatusId = $this->input->post('ViewStatusId');
        $answerStatusId = $this->input->post('AnswerStatusId');
        $keySearch = $this->input->post('keySearch');
        if($fbPageCode > 0){
            $this->loadModel(array('Mfbpages', 'Mfb_chats'));
            $fbPages = $this->Mfbpages->getList($fbPageCode);
            $fb_chats = $this->Mfbpages->getListChats($fbPages,$viewStatusId,$answerStatusId,$keySearch);
            if(count($fb_chats) > 0){
                echo json_encode(array('code' => 1, 'datas' => $fb_chats));
            }else echo json_encode(array('code' => -1, 'message' => "Không có dữ liệu"));
        }
        else redirect('facebook');
    }

    public function getDataFbMess(){
        $this->checkUserLogin();
        $fbPageCode = $this->input->post('FbPageCode');
        $fbId       = $this->input->post('FbId');
        $prefix     = $this->input->post('Prefix');
        if($fbPageCode > 0 && $fbId > 0){
            $this->loadModel(array('Mfbpages'));
            $message = $this->Mfbpages->getMessageChats($fbPageCode, $fbId, $prefix);
            if(count($message) > 0){
                echo json_encode(array('code' => 1, 'datas' => $message));
            }else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));

        }else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function saveMessage(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('FbPageId', 'StaffId', 'FbUserId', 'Message', 'ChatStatusId', 'IsCustomerSend', 'CreatedDate'));
        $fbChatId = $this->input->post('FbChatId');
        $prefix = $this->input->post('Prefix');
        if($fbChatId == 0){
            $postData['StaffId']        = $this->input->post('StaffId');
            $postData['FbUserId']       = $this->input->post('FbUserId');
            $postData['Message']        = $this->input->post('Message');
            $postData['ChatStatusId']   = $this->input->post('ChatStatusId');
            $postData['IsCustomerSend'] = $this->input->post('IsCustomerSend');
            $postData['CreatedDate']    = $this->input->post('CreatedDate');
            $postData['CrDateTime']     = getCurentDateTime();
        }
        if($prefix != ""){
            $this->load->model('Mfbpages');
            $tableName = 'fb_chats_'.$prefix;
            $chatId = $this->Mfbpages->saveGeneral($postData, $fbChatId,$tableName);
            if ($chatId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật thành công", 'data' => $chatId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        
    }

    // api
    public function getFbPostComment(){
        header('Content-Type: application/json');
        $fbPageCode = $this->input->post('FbPageCode');
        $viewStatusId = $this->input->post('ViewStatusId');
        $answerStatusId = $this->input->post('AnswerStatusId');
        if($fbPageCode > 0){
            $this->loadModel(array('Mfbpages'));
            $fbPages = $this->Mfbpages->getList($fbPageCode);

            if(!empty($fbPages)){
                $fbPost = $this->Mfbpages->getListPost($fbPages, $viewStatusId, $answerStatusId);
                if(count($fbPost)){
                    echo json_encode(array('code' => 1, 'message' => 'success', 'data' => $fbPost));
                }else echo json_encode(array('code' => -1, 'message' => "Không có dữ liệu"));
                
            }else echo json_encode(array('code' => -1, 'message' => 'Page id không tồn tại, vui lòng thử lại'));
            
        }else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra, vui lòng thử lại"));
    }

    public function searchPost(){
        header('Content-Type: application/json');
        $fbPostId = $this->input->post('FbPostId');
        $fbPageId = $this->input->post('FbPageId');
        if($fbPostId > 0){
            $this->loadModel(array('Mfbpages'));
            $fbPages = $this->Mfbpages->searchList($fbPageId);

            if(!empty($fbPages)){
                $fbPost = $this->Mfbpages->searchPost($fbPages,$fbPostId);
                // var_dump($fbPost);
                if(count($fbPost)){
                    echo json_encode(array('code' => 1, 'message' => 'success', 'data' => $fbPost));
                }
            }
            
        }else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra, vui lòng thử lại"));
    }

    public function getFbDetailCommentPost(){
        header('Content-Type: application/json');
        $fbPostId = $this->input->post('FbPostId');
        $prefix = $this->input->post('Prefix');
        if($fbPostId > 0){
            $this->loadModel(array('Mfbpages'));
            $fbPost = $this->Mfbpages->getByPost($fbPostId, $prefix);
            $fbComments = $this->Mfbpages->getByComment($fbPostId, $prefix);
            $parentComment = array();
            $childComment = array();
            foreach ($fbComments as $key => $value) {
                if($value['ParentCommentId'] == 0){
                    $parentComment[] = $value;
                }else if($value['FbCommentId'] = $value['ParentCommentId']){
                    $childComment[] = $value;
                }
            }
            if(count($fbComments)){
                echo json_encode(array('code' => 1, 'message' => 'success','post' => $fbPost, 'parentComment' => $parentComment,'childComment' => $childComment));
            }else echo json_encode(array('code' => -1, 'message' => "Không có dữ liệu"));
        }else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra, vui lòng thử lại"));
    }

    public function fbSaveReplyCommnent(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('SenderId', 'FbPostId', 'Message', 'ParentCommentId', 'CommentId', 'FbPageId'));
        $fbCommentId = $this->input->post('FbCommentId');
        $prefix = $this->input->post('Prefix');
        if($fbCommentId == 0){
            $postData['SenderId']        = $this->input->post('SenderId');
            $postData['FbPostId']        = $this->input->post('FbPostId');
            $postData['Message']         = $this->input->post('Message');
            $postData['ParentCommentId'] = $this->input->post('ParentCommentId');
            $postData['CommentId']       = $this->input->post('CommentId');
            $postData['FbPageId']        = $this->input->post('FbPageId');
            $postData['CrDateTime']      = getCurentDateTime();
            $postData['CreatedDateTime'] = getCurentDateTime();
        }
        if($prefix != ""){
            $this->load->model('Mfbpages');
            $tableName = 'fb_comments_'.$prefix;
            // var_dump($postData);
            $commentId = $this->Mfbpages->saveGeneral($postData, $fbCommentId,$tableName);
            if ($commentId > 0){
                $data = $this->Mfbpages->getByCommentOne($commentId,$tableName);
                echo json_encode(array('code' => 1, 'message' => "Cập nhật thành công", 'data' => $data));
            }else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        
    }

    public function getContentSentences(){
        $this->checkUserLogin();
        $postData = $this->arrayFromPost(array('SentenceGroupId', 'SearchText', 'FbPageId'));
        if($postData['FbPageId'] > 0){
            $this->load->model('Msentences');
            $listSentences = $this->Msentences->search($postData);
            echo json_encode(array('code' => 1,  'data' => $listSentences));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function changeCommentStatus(){
        $this->checkUserLogin();
        $comment_id = $this->input->post('comment_id');
        $fbPageCode = $this->input->post('FbPageCode'); 
        $fbCommentId = $this->input->post('fbCommentId');
        $type = $this->input->post('type');
        if($fbPageCode > 0){
            $this->loadModel(array('Mfb_comments'));
            $this->Mfb_comments->setPrefix($fbPageCode);
            // if($type == 'delete'){
                $flag = $this->Mfb_comments->deleteCmt($comment_id, $fbPageCode, $fbCommentId, $type);
                if($flag == true) echo json_encode(array('code' => 1,  'message' => 'Xóa bình luận thành công.'));
                else echo json_encode(array('code' => 0,  'message' => 'Xóa bình luận không thành công.'));
            // }else{

            // }
        }else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        
    }
}