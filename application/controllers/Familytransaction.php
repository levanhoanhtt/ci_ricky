<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Familytransaction extends MY_Controller{

    public function index($transactionTypeId = 0){
        $user = $this->checkUserLogin();
        if(!in_array($transactionTypeId, array(1, 2))) $transactionTypeId = 1;
        $data = $this->commonData($user,
            'Danh sách phiếu '.($transactionTypeId == 1 ? 'thu' : 'chi').' gia đình',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/search_item.js', 'js/familytransaction_list.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'familytransaction/'.$transactionTypeId)) {
            $this->loadModel(array('Mfilters', 'Mbanks', 'Mtransactionkinds', 'Mtags'));
            $itemTypeId = $transactionTypeId == 1 ? 28 : 29;
            $data['itemTypeId'] = $itemTypeId;
            $data['transactionTypeId'] = $transactionTypeId;
            //$data['listFunds'] = $this->Mfunds->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listBanks'] = $this->Mbanks->search(array('StatusId' => STATUS_ACTIVED));
            $data['listTransactionKinds'] = $this->Mtransactionkinds->getList($transactionTypeId, 2);
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => $itemTypeId));
            $data['listFilters'] = $this->Mfilters->getList($itemTypeId);
            $this->load->view('familytransaction/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add($transactionTypeId = 0){
        if(in_array($transactionTypeId, array(1, 2))) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Thêm mới '.$this->Mconstants->transactionTypes[$transactionTypeId].' gia đình',
                array(
                    'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css'),
                    'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/choose_item.js', 'js/familytransaction_update.js'))
                )
            );
            $listAction = $data['listActions'];
            $mAction = $this->Mactions;
            if ($mAction->checkAccess($listAction, 'familytransaction/'.$transactionTypeId)) {
                $data['transactionTypeId'] = $transactionTypeId;
                $data['canEditLevel'] = $this->canEditLevel($mAction, $listAction);
                $this->loadModel(array('Mmoneyphones', 'Mtransactionkinds', 'Mbanks'));
                $whereStatus = array('StatusId' => STATUS_ACTIVED);
                $data['listMoneyPhones'] = $this->Mmoneyphones->getBy($whereStatus);
                //$data['listFunds'] = $this->Mfunds->getBy($whereStatus);
                $data['listBanks'] = $this->Mbanks->search($whereStatus);
                $data['listTransactionKinds'] = $this->Mtransactionkinds->getList($transactionTypeId, 2);
                $this->load->view('familytransaction/add', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('familytransaction');
    }

    public function edit($familyTransactionId = 0){
        if ($familyTransactionId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Cập nhật phiếu thu chi gia đình',
                array(
                    'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css'),
                    'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/choose_item.js', 'js/familytransaction_update.js',))
                )
            );
            $this->loadModel(array('Mfamilytransactions', 'Mmoneyphones', 'Mtransactionkinds', 'Mbanks', 'Mactionlogs', 'Mtags', 'Mtransactions'));
            $familyTransaction = $this->Mfamilytransactions->get($familyTransactionId);
            if($familyTransaction) {
                $transactionTypeId = $familyTransaction['TransactionTypeId'];
                $listAction = $data['listActions'];
                $mAction = $this->Mactions;
                if ($mAction->checkAccess($listAction, 'familytransaction/'.$transactionTypeId)){
                    $data['title'] = 'Cập nhật phiếu ' . $this->Mconstants->transactionTypes[$transactionTypeId].' gia đình';
                    $data['canEditLevel'] = $this->canEditLevel($mAction, $listAction);
                    $data['canEdit'] = $familyTransaction['VerifyLevelId'] == 1;
                    $data['transactionTypeId'] = $transactionTypeId;
                    $data['familyTransaction'] = $familyTransaction;
                    $whereStatus = array('StatusId' => STATUS_ACTIVED);
                    $data['listMoneyPhones'] = $this->Mmoneyphones->getBy($whereStatus);
                    $data['listTransactionKinds'] = $this->Mtransactionkinds->getList($transactionTypeId, 2);
                    //$data['listFunds'] = $this->Mfunds->getBy($whereStatus);
                    $data['listBanks'] = $this->Mbanks->search($whereStatus);
                    $itemTypeId = $transactionTypeId == 1 ? 28 : 29;
                    $data['tagNames'] = $this->Mtags->getTagNames($familyTransactionId, $itemTypeId);
                    $data['listActionLogs'] = $this->Mactionlogs->getList($familyTransactionId, $itemTypeId);
                    $this->load->view('familytransaction/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else{
                $data['canEdit'] = false;
                $data['transactionTypeId'] = 0;
                $data['txtError'] = "Không tìm thấý phiếu";
                $this->load->view('familytransaction/edit', $data);
            }
        }
        else redirect('familytransaction');
    }

    private function canEditLevel($mAction, $listAction){
        $canEditLevel = 0;
        for($i = 2; $i > 0; $i--) {
            if ($mAction->checkAccess($listAction, 'familytransaction/editLevel_'.$i)){
                $canEditLevel = $i;
                break;
            }
        }
        return $canEditLevel;
    }

    public function searchByFilter($transactionTypeId = 0){
        $user = $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if(!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if(!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->loadModel(array('Mfamilytransactions', 'Mtransactions'));
        $data1 = $this->Mfamilytransactions->searchByFilter($searchText, $itemFilters, $limit, $page, $transactionTypeId);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('TransactionTypeId', 'TransactionStatusId', 'VerifyLevelId', 'MoneySourceId', 'MoneyPhoneId', 'BankId', 'BankToText', 'TransactionKindId', 'PayerName', 'PayerPhone', 'PaidCost', 'Comment'));
        $postData['PaidCost'] = replacePrice($postData['PaidCost']);
        if($postData['PaidCost'] > 0 && $postData['TransactionTypeId'] > 0 && $postData['TransactionStatusId'] > 0 && $postData['VerifyLevelId'] > 0){
            $crDateTime = getCurentDateTime();
            $transactionTypeName = $this->Mconstants->transactionTypes[$postData['TransactionTypeId']];
            $actionLogs = array(
                'ItemTypeId' => $postData['TransactionTypeId'] == 1 ? 28 : 29,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
            $canUpdate = true;
            $this->load->model('Mfamilytransactions');
            $familyTransactionId = $this->input->post('FamilyTransactionId');
            if($familyTransactionId > 0){
                $transactionStatusIdOld = $this->Mfamilytransactions->getFieldValue(array('FamilyTransactionId' => $familyTransactionId), 'TransactionStatusId', 0);
                if($transactionStatusIdOld == 1){
                    $actionLogs['ActionTypeId'] = 2;
                    $postData['UpdateUserId'] = $user['UserId'];
                    $postData['UpdateDateTime'] = $crDateTime;
                    $comment = $user['FullName'] . ': ';
                    if($postData['VerifyLevelId'] == 1) $comment .= 'Cập nhật ';
                    elseif($postData['VerifyLevelId'] == 2) $comment .= 'Xác thực ';
                    elseif($postData['VerifyLevelId'] == 3) $comment .= 'Duyệt ';
                    elseif($postData['VerifyLevelId'] == 4) $comment .= 'Yêu cầu check lại ';
                    elseif($postData['VerifyLevelId'] == 6) $comment .= 'Không duyệt ';
                    $actionLogs['Comment'] = $comment.$transactionTypeName;
                }
                else $canUpdate = false;
            }
            else{
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = $crDateTime;
                $actionLogs['ActionTypeId'] = 1;
                $comment = $user['FullName'] . ': Thêm mới ';
                if($postData['VerifyLevelId'] == 2) $comment .= ' và Xác thực ';
                elseif($postData['VerifyLevelId'] == 3) $comment .= ' và Duyệt ';
                $actionLogs['Comment'] = $comment.$transactionTypeName;
            }
            if($canUpdate) {
                $tagNames = json_decode(trim($this->input->post('TagNames')), true);
                $familyTransactionId = $this->Mfamilytransactions->update($postData, $familyTransactionId, $tagNames, $actionLogs);
                if($familyTransactionId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật phiếu {$transactionTypeName} thành công", 'data' => $familyTransactionId));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function cancel(){
        $user = $this->checkUserLogin(true);
        $familyTransactionId = $this->input->post('FamilyTransactionId');
        if($familyTransactionId > 0){
            $this->load->model('Mfamilytransactions');
            $transactionStatusIdOld = $this->Mfamilytransactions->getFieldValue(array('FamilyTransactionId' => $familyTransactionId), 'TransactionStatusId', 0);
            if($transactionStatusIdOld == 1){
                $crDateTime = getCurentDateTime();
                $postData = array('TransactionStatusId' => 3, 'VerifyLevelId' => 6, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime);
                $transactionTypeId = $this->input->post('TransactionTypeId');
                $actionLogs = array(
                    'ItemId' => $familyTransactionId,
                    'ItemTypeId' => $transactionTypeId == 1 ? 28 : 29,
                    'ActionTypeId' => 2,
                    'Comment' => $user['FullName'] . ': Hủy phiếu '.$this->Mconstants->transactionTypes[$transactionTypeId],
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
                $flag = $this->Mfamilytransactions->updateField($postData, $familyTransactionId, $actionLogs);
                if($flag) echo json_encode(array('code' => 1, 'message' => "Hủy phiếu thành công"));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));

            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateVerifyLevel(){
        $user = $this->checkUserLogin(true);
        $familyTransactionId = $this->input->post('FamilyTransactionId');
        $verifyLevelId = $this->input->post('VerifyLevelId');
        if($familyTransactionId > 0 && in_array($verifyLevelId, array(3, 4, 5, 6))){
            $this->load->model('Mfamilytransactions');
            $familyTransaction = $this->Mfamilytransactions->get($familyTransactionId);
            if($familyTransaction && $familyTransaction['TransactionStatusId'] > 0){
                $transactionTypeName = $familyTransaction['TransactionTypeId'] == 1 ? 'thu' : 'chi';
                $familyTransaction['CrUserId'] = $user['UserId'];
                $verifyLevelIdOld = $familyTransaction['VerifyLevelId'];
                if($this->Mactions->checkAccessFromDb('familytransaction/editLevel_2', $user['UserId'])){ //QL
                    if(in_array($verifyLevelIdOld, array(1, 2)) && $verifyLevelId == 3){
                        $familyTransaction['Comment'] = $user['FullName'].' đã duyệt phiếu '.$transactionTypeName;
                        $flag = $this->Mfamilytransactions->updateVerifyLevel($familyTransactionId, $verifyLevelId, $familyTransaction);
                        if($flag) echo json_encode(array('code' => 1, 'message' => "Duyệt phiếu {$transactionTypeName} thành công"));
                        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                    }
                    elseif(in_array($verifyLevelIdOld, array(2, 5)) && in_array($verifyLevelId, array(4, 6))){
                        $comment = $user['FullName'].' '.($verifyLevelId == 4 ? 'gửi nhân viên check lại phiếu' : 'hủy phiểu');
                        $message = $verifyLevelId == 4 ? 'Gửi nhân viên check lại phiếu thành công' : 'Hủy phiểu thành công';
                        $familyTransaction['Comment'] = $comment;
                        $flag = $this->Mfamilytransactions->updateVerifyLevel($familyTransactionId, $verifyLevelId, $familyTransaction);
                        if($flag) echo json_encode(array('code' => 1, 'message' => $message));
                        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                    }
                    else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                }
                elseif($this->Mactions->checkAccessFromDb('familytransaction/editLevel_1', $user['UserId'])){ //NV
                    if($verifyLevelIdOld == 4 && in_array($verifyLevelId, array(5, 6))){
                        $comment = $user['FullName'].' '.($verifyLevelId == 5 ? 'gửi quản lý duyệt lại phiếu' : 'hủy phiểu');
                        $message = $verifyLevelId == 5 ? 'Gửi quản lý duyệt lại phiếu thành công' : 'Hủy phiểu thành công';
                        $familyTransaction['Comment'] = $comment;
                        $flag = $this->Mfamilytransactions->updateVerifyLevel($familyTransactionId, $verifyLevelId, $familyTransaction);
                        if($flag) echo json_encode(array('code' => 1, 'message' => $message));
                        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                    }
                    else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                }
                else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}