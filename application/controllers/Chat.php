<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Chat Facebook',
            array(
                'scriptHeader' => array('css' => array('facebookresource/css.css', 'facebookresource/alertify.min.css')),
                'scriptFooter' => array('js' => array('facebookresource/alertify.min.js', 'facebookresource/moment.min.js', /*'js/chat.js'*/))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'chat')){
            $data['pageId'] = '1759192184381061';
            $data['appId'] = '2020899998139436';
            $data['appToken'] = '2020899998139436|dvRUN1GCgvUv2P3aP3EXQqSrLlk';
            $this->load->view("facebook/chat",$data);
        }
        else $this->load->view('user/permission', $data);
    }
}