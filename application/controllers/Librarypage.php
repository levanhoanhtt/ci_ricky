<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Librarypage extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Gắn thư viên vào page',
            array(
            	'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
            	'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js','js/librarypage.js')))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'librarypage')) {
            $this->load->model(array('Mlibrarypages','Mfbpages','Mlibrarysentences'));
            $data['listFbPages'] = $this->Mfbpages->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listLibrarySentences'] = $this->Mlibrarysentences->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('librarypage/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
    	$this->checkUserLogin(true);
    	$fbPageId = $this->input->post('FbPageId');
    	$librarySentenceIds = json_decode(trim($this->input->post('LibrarySentenceIds')), true);
    	if($fbPageId > 0 && !empty($librarySentenceIds)){
    		$this->load->model('Mlibrarypages');
    		$flag = $this->Mlibrarypages->update($fbPageId, $librarySentenceIds);
    		if($flag) echo json_encode(array('code' => 1, 'message' => "Gắn thư viên vào page thành công"));
    		else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    	}
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}