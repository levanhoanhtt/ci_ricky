<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends MY_Controller {

	public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách tài khoản ngân hàng',
            array('scriptFooter' => array('js' => 'js/bank_list.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'bank')) {
            $this->loadModel(array('Mbanks', 'Mbanktypes', 'Mstores'));
            $postData = $this->arrayFromPost(array('BankNumber', 'BankName', 'BankHolder', 'BankTypeId', 'StatusId'));
            $rowCount = $this->Mbanks->getCount($postData);
            $data['listBanks'] = array();
            if($rowCount > 0){
                $perPage = DEFAULT_LIMIT;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $data['listBanks'] = $this->Mbanks->search($postData, $perPage, $page);
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('bank/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }


    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm mới ngân hàng',
            array('scriptFooter' => array('js' => 'js/bank_update.js'))
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'bank')) $this->load->view('bank/add', $data);
        else $this->load->view('user/permission', $data);
    }

    public function edit($bankId = 0){
    	if ($bankId > 0) {
	        $user = $this->checkUserLogin();
	        $data = $this->commonData($user,
	            'Cập nhật ngân hàng',
                array('scriptFooter' => array('js' => 'js/bank_update.js'))
	        );
	        if ($this->Mactions->checkAccess($data['listActions'], 'bank')) {
                $this->load->model('Mbanks');
	            $bank = $this->Mbanks->get($bankId);
	            if($bank){
                    $data['bankId'] = $bankId;
                    $data['bank'] = $bank;
                }
                else{
                    $data['bankId'] = 0;
                    $data['txtError'] = "Không tìm thấy ngân hàng";
                }
	            $this->load->view('bank/edit', $data);
	        }
	        else $this->load->view('user/permission', $data);
	    }
        else redirect('bank');
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('BankNumber', 'BankCode', 'BankName', 'BankHolder', 'BankTypeId', 'BranchName', 'StatusId', 'TaxCode', 'Address', 'DisplayOrder'));
        $bankId = $this->input->post('BankId');
        if($bankId > 0){
            $postData['UpdateUserId'] = $user['UserId'];
            $postData['UpdateDatetime'] = getCurentDateTime();
        }
        else{
            $postData['Balance'] = 0;
            $postData['CrUserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
        }
        $this->load->model('Mbanks');
        $bankId = $this->Mbanks->update($postData, $bankId);
        if ($bankId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật ngân hàng thành công", 'data' => $bankId));
        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $bankId = $this->input->post('BankId');
        $statusId = $this->input->post('StatusId');
        if($bankId > 0 && $statusId >= 0) {
            $this->load->model('Mbanks');
            $flag = $this->Mbanks->changeStatus($statusId, $bankId ,'StatusId', $user['UserId']);
            if($flag) {
                $statusName = "";
                if($statusId == 0) $txtSuccess = "Xóa Tài khoản ngân hàng thành công";
                else{
                     $txtSuccess = "Đổi trạng thái thành công";
                     $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function changeDisplayOrder(){
        $user = $this->checkUserLogin(true);
        $bankId = $this->input->post('BankId');
        $displayOrder = $this->input->post('DisplayOrder');
        if($bankId > 0 && $displayOrder > 0) {
            $this->load->model('Mbanks');
            $bankId = $this->Mbanks->update(array('DisplayOrder' => $displayOrder, 'UpdateUserId' => $user['UserId'], 'UpdateDatetime' => getCurentDateTime()), $bankId);
            if ($bankId > 0) echo json_encode(array('code' => 1, 'message' => "Thay đổi thứ tự thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}