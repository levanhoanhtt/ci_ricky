<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Librarysentence extends MY_Controller {

	public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách thư viện',
            array('scriptFooter' => array('js' => array('js/librarysentence.js','ckfinder/ckfinder.js')))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'librarysentence')) {
            $this->load->model('Mlibrarysentences');
            $data['listLibrarySentences'] = $this->Mlibrarysentences->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('librarysentence/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $user =$this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('LibraryName'));
        if(!empty($postData['LibraryName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $postData['CrUserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
            $librarySentenceId = $this->input->post('LibrarySentenceId');
            $this->load->model('Mlibrarysentences');
            $flag = $this->Mlibrarysentences->save($postData, $librarySentenceId);
            if ($flag > 0) {
                $postData['LibrarySentenceId'] = $flag;
                $postData['IsAdd'] = ($librarySentenceId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật thư viện thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $librarySentenceId = $this->input->post('LibrarySentenceId');
        if($librarySentenceId > 0){
            $this->load->model('Mlibrarysentences');
            $flag = $this->Mlibrarysentences->changeStatus(0, $librarySentenceId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa thư viện thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function importExcel(){
    	$user = $this->checkUserLogin(true);
        $fileUrl = trim($this->input->post('FileUrl'));
        $librarySentenceId = $this->input->post('LibrarySentenceId');
        if (!empty($fileUrl) && $librarySentenceId > 0){
        	if (ROOT_PATH != '/') $fileUrl = str_replace(ROOT_PATH, '', $fileUrl);
            $fileUrl = FCPATH . $fileUrl;
            $this->load->library('excel');
            $inputFileType = PHPExcel_IOFactory::identify($fileUrl);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($fileUrl);
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $countRows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $scanData = array();
            $barCodes = array();
            $this->load->model(array('Msentences','Msentencegroups'));
            $flag = false;
            for($row = 2; $row <= $countRows; $row++) {
            	$sentenceGroupName = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
            	$sentenceTitle = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
	            $sentenceContent = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                if(!$this->Msentences->checkExist($sentenceTitle, $librarySentenceId)){
                    $this->Msentences->save(array(
                        'SentenceTitle' => $sentenceTitle,
                        'SentenceContent' => $sentenceContent,
                        'SentenceGroupId' => $this->Msentencegroups->update($sentenceGroupName),
                        'LibrarySentenceId' => $librarySentenceId,
                        'FileTypeId' => 3
                    ));
                }
            }
            if($flag == true) echo json_encode(array('code' => 1, 'message' => "Import thư viện thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
            unlink($fileUrl);
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

}
