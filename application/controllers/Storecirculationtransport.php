<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Storecirculationtransport extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Giao hàng lưu chuyển kho',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_item.js', 'js/store_circulation_transport_list.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'storecirculationtransport')) {
            $this->loadModel(array('Mfilters', 'Mstorecirculationtransports', 'Mstores', 'Mtransporttypes', 'Mtransporters', 'Mtags'));
            $itemTypeId = 25;
            $data['listStorecirculationtransports'] = $this->Mtransporters->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listTransportTypes'] = $this->Mtransporttypes->getBy(['StatusId' => STATUS_ACTIVED]);
            $data['listTransporters'] = $this->Mtransporters->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $data['listFilters'] = $this->Mfilters->getList($itemTypeId);
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => $itemTypeId));
            $this->load->view('storecirculationtransport/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($storeCirculationTransportId = 0){
        if($storeCirculationTransportId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Giao hàng lưu chuyển kho',
                array(
                    'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css',  'vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css'),
                    'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'vendor/plugins/bxslider/jquery.bxslider.min.js', 'js/store_circulation_transport_update.js'))
                )
            );
            $this->loadModel(array('Mtransports', 'Mstorecirculationtransports', 'Mtransporttypes', 'Mtransporters', 'Mpendingstatus', 'Mstores', 'Mactionlogs', 'Mtags', 'Mproducts', 'Mproductchilds','Mstorecirculationtransportcomments', 'Mparts', 'Mcancelreasons','Mstorecirculationproducts','Mstorecirculations', 'Mprovinces', 'Mdistricts', 'Mwards'));
            $storeCirculationTransport = $this->Mstorecirculationtransports->get($storeCirculationTransportId);
            if ($storeCirculationTransport) {
                if ($this->Mactions->checkAccess($data['listActions'], 'storecirculationtransport')) {
                    $data['title'] = 'Giao hàng lưu chuyển kho / ' . $storeCirculationTransport['TransportCode'];
                    $data['canEdit'] = true;
                    $data['storeCirculationTransportId'] = $storeCirculationTransportId;
                    $data['storeCirculationTransport'] = $storeCirculationTransport;
                    $data['listTransportTypes'] = $this->Mtransporttypes->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listTransporters'] = $this->Mtransporters->getBy(array('ItemStatusId' => STATUS_ACTIVED));
                    $data['tagNames'] = $this->Mtags->getTagNames($storeCirculationTransportId, 25);
                    $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 25));
                    $data['listPendingStatus'] = $this->Mpendingstatus->getList(9);
                    $data['listActionLogs'] = $this->Mactionlogs->getList($storeCirculationTransportId, 25);
                    $data['listCancelReasons'] = $this->Mcancelreasons->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listUsers'] = $this->Musers->getListForSelect($user['UserId'], 'Chỉ mình tôi');
                    $data['listParts'] = $this->Mparts->getList(true);
                    $data['listStoreCirculationProducts'] = $this->Mstorecirculationproducts->getBy(array('StoreCirculationId' => $storeCirculationTransport['StoreCirculationId']));
                    $data['listStoreCirculationTransportComments'] = $this->Mstorecirculationtransportcomments->getList($storeCirculationTransportId);
                    $listStores = $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED));
                    $data['listStores'] = $listStores;
                    $storeCirculation = $this->Mstorecirculations->get($storeCirculationTransport['StoreCirculationId']);
                    $data['storeCirculation'] = $storeCirculation;
                    $customerAddress = array(
                        'CustomerName' => '',
                        'PhoneNumber' => '',
                        'Email' => '',
                        'ZipCode' => '',
                        'Address' => '',
                        'WardName' => '',
                        'DistrictName' => '',
                        'ProvinceName' => '',
                        'CountryName' => 'Việt Nam',
                        'CountryId' => 232,
                        'ProvinceId' => 0,
                        'DistrictId' => 0,
                        'WardId' => 0,
                    );
                    $store = false;
                    foreach($listStores as $s){
                        if($s['StoreId'] == $storeCirculation['StoreDestinationId']){
                            $store = $s;
                            break;
                        }
                    }
                    if($store){
                        $flag = false;
                        if($store['HeadUserId'] > 0){
                            $headUser = $this->Musers->get($store['HeadUserId'], true, '', 'FullName, PhoneNumber, Email');
                            if($headUser){
                                $flag = true;
                                $customerAddress['CustomerName'] = $headUser['FullName'];
                                $customerAddress['PhoneNumber'] = $headUser['PhoneNumber'];
                                $customerAddress['Email'] = $headUser['Email'];
                            }
                        }
                        if(!$flag) $customerAddress['CustomerName'] = $store['StoreName'];
                        $customerAddress['Address'] = $store['Address'];
                        if($store['WardId'] > 0){
                            $customerAddress['WardId'] = $store['WardId'];
                            $customerAddress['WardName'] = $this->Mwards->getFieldValue(array('WardId' => $store['WardId']), 'WardName');
                        }
                        if($store['DistrictId'] > 0){
                            $customerAddress['DistrictId'] = $store['DistrictId'];
                            $customerAddress['DistrictName'] = $this->Mdistricts->getFieldValue(array('DistrictId' => $store['DistrictId']), 'DistrictName');
                        }
                        if($store['ProvinceId'] > 0){
                            $customerAddress['ProvinceId'] = $store['ProvinceId'];
                            $customerAddress['ProvinceName'] = $this->Mprovinces->getFieldValue(array('ProvinceId' => $store['ProvinceId']), 'ProvinceName');
                        }
                    }
                    $data['customerAddress'] = $customerAddress;
                    $this->load->view('storecirculationtransport/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else{
                $data['canEdit'] = false;
                $data['storeCirculationTransportId'] = 0;
                $data['txtError'] = "Không tìm thấý Giao hàng lưu chuyển kho";
                $this->load->view('storecirculationtransport/edit', $data);
            }
        }
        else redirect('storecirculationtransport');
    }

    public function printPdf($storeCirculationTransportId = 0){
        if($storeCirculationTransportId > 0) {
            $this->checkUserLogin();
            $this->loadModel(array('Mstorecirculationtransports','Mstorecirculations','Mstorecirculationproducts', 'Mcustomers', 'Mcustomeraddress', 'Morderproducts', 'Mproducts', 'Mproductchilds','Mstores', 'Mcountries', 'Mprovinces', 'Mdistricts', 'Mwards'));
            $storeCirculationTransport = $this->Mstorecirculationtransports->get($storeCirculationTransportId);
            $storecirculation = $this->Mstorecirculations->get($storeCirculationTransport['StoreCirculationId']);
            $listStore = $this->Mstores->get($storecirculation['StoreDestinationId']);
            if ($storeCirculationTransportId) {
                $customerAddress = $this->Mcustomeraddress->getInfo('', $listStore['HeadUserId']);
                // var_dump($customerAddress);
                if($customerAddress){
                    if($customerAddress['CountryId'] == 0 || $customerAddress['CountryId'] == 232){
                        $address = $customerAddress['Address'];
                        if($customerAddress['WardId'] > 0) $address .= ', xã '.$this->Mwards->getFieldValue(array('WardId' => $customerAddress['WardId']), 'WardName');
                        if($customerAddress['DistrictId'] > 0) $address .= ', huyện '.$this->Mdistricts->getFieldValue(array('DistrictId' => $customerAddress['DistrictId']), 'DistrictName');
                        if($customerAddress['ProvinceId'] > 0) $address .= ', tỉnh '.$this->Mprovinces->getFieldValue(array('ProvinceId' => $customerAddress['ProvinceId']), 'ProvinceName');
                        $customerAddress['Address'] = $address;
                    }
                    else $customerAddress['Address'] .= $customerAddress['ZipCode'].' '.$this->Mcountries->getFieldValue(array('CountryId' => $customerAddress['CountryId']), 'CountryName');
                }
                $this->load->library('ciqrcode');
                $params = array();
                $transportCode = $storeCirculationTransport['TransportCode'];
                $params['data'] = $transportCode;
                $params['savename'] = "./assets/uploads/qr/{$transportCode}.png";
                $this->ciqrcode->generate($params);
                $data = array(
                    'configs' => $this->session->userdata('configs'),
                    'storecirculationtransport' => $storeCirculationTransport,
                    'storeName' => $listStore['StoreId'] > 0 ? $this->Mstores->getFieldValue(array('StoreId' => $storecirculation['StoreDestinationId']), 'StoreName') : '',
                    'customerAddress' => $customerAddress,
                    'listStoreCirculationProducts' => $this->Mstorecirculationproducts->getBy(array('StoreCirculationId' => $storeCirculationTransport['StoreCirculationId'])),
                    'barcodeSrc' => $params['savename']
                );
                $this->load->view('storecirculationtransport/print_pdf', $data);
            }
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";

    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('StoreCirculationId', 'TransportStatusId', 'PendingStatusId', 'TransportTypeId', 'TransporterId', 'Tracking', 'Weight', 'ShipCost','Comment','CancelReasonId','CancelComment'));
        $postData['Weight'] = replacePrice($postData['Weight']);
        $postData['ShipCost'] = replacePrice($postData['ShipCost']);
        if($postData['StoreCirculationId'] > 0 && $postData['TransportStatusId'] > 0){
            $this->loadModel(array('Mstorecirculations', 'Mstorecirculationproducts', 'Mproductchilds', 'Mproductquantity', 'Minventories', 'Mstorecirculationtransports'));
            $storeCirculationTransportId = $this->input->post('StoreCirculationTransportId');
            $crDateTime = getCurentDateTime();
            $actionLog = array(
                'ItemTypeId' => 25,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
            $inventoryData = array();
            if ($storeCirculationTransportId > 0){
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = $crDateTime;
                $actionLog['ActionTypeId'] = 2;
                $actionLog['Comment'] = $user['FullName'] . ': Cập nhật Giao hàng LCK';
            }
            else{
                $transportStatusIds = $this->Mstorecirculationtransports->getListFieldValue(array('StoreCirculationId' => $postData['StoreCirculationId'], 'TransportStatusId >' => 0), 'TransportStatusId');
                $count = count($transportStatusIds);
                if($count > 0) {
                    $i = 0;
                    foreach ($transportStatusIds as $transportStatusId) {
                        if($transportStatusId == 5) $i++;
                    }
                    if($i < $count){
                        echo json_encode(array('code' => -1, 'message' => "Vận chuyển đã được tạo trước đó"));
                        die();
                    }
                }
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = $crDateTime;
                $actionLog['ActionTypeId'] = 1;
                $actionLog['Comment'] = $user['FullName'] . ': Thêm mới Giao hàng LCK';
                $products = $this->Mstorecirculationproducts->getBy(array('StoreCirculationId' => $postData['StoreCirculationId']));
                $storeId = $this->Mstorecirculations->getFieldValue(array('StoreCirculationId' => $postData['StoreCirculationId']), 'StoreSourceId');
                if(!empty($products) && $storeId > 0) $inventoryData = $this->getInventoryData($storeId, $user['UserId'], $crDateTime, 2, 'Giao hàng '.$this->Mstorecirculations->genStoreCirculationCode($postData['StoreCirculationId']), $products);
                else{
                    echo json_encode(array('code' => -1, 'message' => "Không có sản phẩm Lưu chuyển kho"));
                    die();
                }
            }
            $metaData = array(
                'FullName' => $user['FullName'],
                'InventoryData' => $inventoryData
            );
            $storeCirculationTransportId = $this->Mstorecirculationtransports->update($postData, $storeCirculationTransportId, $metaData, $actionLog);
            if ($storeCirculationTransportId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật vận chuyển thành công", 'data' => $storeCirculationTransportId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateField(){
        $user = $this->checkUserLogin(true);
        $storeCirculationTransportId = $this->input->post('StoreCirculationTransportId');
        $fieldName = trim($this->input->post('FieldName'));
        $fieldValue = trim($this->input->post('FieldValue'));
        if($storeCirculationTransportId > 0 && !empty($fieldName) && !empty($fieldValue)){
            $this->load->model('Mstorecirculationtransports');
            $label = '';
            $value = '';
            $data = array();
            $metaData = array('UserId' => $user['UserId'], 'FullName' => $user['FullName']);
            $crDateTime = getCurentDateTime();
            $transportData = array($fieldName => $fieldValue, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime);
            switch($fieldName){
                case 'TransportStatusId':
                    if($fieldValue > 0) {
                        $transportStatusId = $this->Mstorecirculationtransports->getFieldValue(array('StoreCirculationTransportId' => $storeCirculationTransportId), 'TransportStatusId', 0);
                        $flag = $transportStatusId != 4;
                        if($flag) $flag = $transportStatusId != $fieldValue;
                        if($flag) {
                            $label = 'Trạng thái vận chuyển';
                            $value = $this->Mconstants->transportStatus[$fieldValue];
                            $data['StatusName'] = '<span class="' . $this->Mconstants->labelCss[$fieldValue] . '">' . $value . '</span>';
                            $typeId = 0;
                            if ($fieldValue == 5 || $fieldValue == 7) { //huy hoac chuyen hoan thanh cong
                                $transportData['CancelReasonId'] = $this->input->post('CancelReasonId');
                                $transportData['CancelComment'] = trim($this->input->post('CancelComment'));
                                $typeId = 1;
                            }
                            elseif ($fieldValue == 4) $typeId = 2; // thanh cong
                            if($typeId > 0){
                                $storeCirculationId = $this->input->post('StoreCirculationId');
                                $storeId = $typeId == 1 ? $this->input->post('StoreSourceId') : $this->input->post('StoreDestinationId');
                                $this->loadModel(array('Mstorecirculationproducts', 'Mproductchilds', 'Mproductquantity', 'Minventories', 'Mstorecirculations'));
                                $products = $this->Mstorecirculationproducts->getBy(array('StoreCirculationId' => $storeCirculationId));
                                $metaData['InventoryData'] = $this->getInventoryData($storeId, $user['UserId'], $crDateTime, 1, ($typeId == 1 ? 'Hoàn hàng ' : 'Nhận hàng ').$this->Mstorecirculations->genStoreCirculationCode($storeCirculationId), $products);
                                $metaData['StoreCirculationId'] = $storeCirculationId;
                            }
                            else $metaData['InventoryData'] = array();
                        }
                    }
                    break;
                case 'PendingStatusId':
                    if($fieldValue > 0) {
                        $label = 'Trạng thái chờ xử lý';
                        $this->load->model('Mpendingstatus');
                        $value = $this->Mconstants->transportStatus[$fieldValue].' ('.$this->Mpendingstatus->getFieldValue(array('PendingStatusId' => $fieldValue), 'PendingStatusName').')';
                        $data['StatusName'] = '<span class="' . $this->Mconstants->labelCss[1] . '">'.$value.'</span>';
                    }
                    break;
                case 'Tracking':
                    $label = 'Mã vận đơn';
                    $value = $fieldValue;
                    $data['Tracking'] = $fieldValue;
                    break;
                case 'ShipCost':
                    $fieldValue = replacePrice($fieldValue);
                    if($fieldValue > 0) {
                        $label = 'Phí giao hàng';
                        $value = priceFormat($fieldValue) . ' VNĐ';
                    }
                    break;
                case 'TransporterId':
                    if($fieldValue > 0) {
                        $label = 'Nhà vận chuyển';
                        $this->load->model('Mtransporters');
                        $value = $this->Mtransporters->getFieldValue(array('TransporterId' => $fieldValue), 'TransporterName');
                        $data['TransporterName'] = $value;
                    }
                    break;
                default: break;
            }
            if(!empty($label)) {
                $comment = $user['FullName'] . " cập nhật {$label} thành {$value}";
                $data['Comment'] = $comment;
                $actionLog = array(
                    'ItemId' => $storeCirculationTransportId,
                    'ItemTypeId' => 25,
                    'ActionTypeId' => 2,
                    'Comment' => $comment,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
                $flag = $this->Mstorecirculationtransports->updateField($transportData, $storeCirculationTransportId, $actionLog, $metaData);
                if ($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật vận chuyển thành công", 'data' => $data));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    private function getInventoryData($storeId, $userId, $crDateTime, $inventoryTypeId, $comment, $listProducts){
        $inventoryData = array();
        foreach($listProducts as $op){
            if($op['ProductKindId'] == 3){
                $listProductChilds = $this->Mproductchilds->getByProductId($op['ProductId']);
                foreach($listProductChilds as $pc){
                    if($pc['ProductPartId'] > 0){
                        $inventoryData[] = array(
                            'ProductId' => $pc['ProductPartId'],
                            'ProductChildId' => $pc['ProductPartChildId'],
                            'OldQuantity' => $this->Mproductquantity->getQuantity($pc['ProductPartId'], $pc['ProductPartChildId'], $storeId),
                            'Quantity' => $op['Quantity'] * $pc['Quantity'],
                            'InventoryTypeId' => $inventoryTypeId,
                            'StoreId' => $storeId,
                            'StatusId' => STATUS_ACTIVED,
                            'IsManual' => 1,
                            'Comment' => $comment,
                            'CrUserId' => $userId,
                            'CrDateTime' => $crDateTime,
                            'UpdateUserId' => $userId,
                            'UpdateDateTime' => $crDateTime
                        );
                    }
                }
            }
            $inventoryData[] = array(
                'ProductId' => $op['ProductId'],
                'ProductChildId' => $op['ProductChildId'],
                'OldQuantity' => $this->Mproductquantity->getQuantity($op['ProductId'], $op['ProductChildId'], $storeId),
                'Quantity' => $op['Quantity'],
                'InventoryTypeId' => $inventoryTypeId,
                'StoreId' => $storeId,
                'StatusId' => STATUS_ACTIVED,
                'IsManual' => 1,
                'Comment' => $comment,
                'CrUserId' => $userId,
                'CrDateTime' => $crDateTime,
                'UpdateUserId' => $userId,
                'UpdateDateTime' => $crDateTime
            );
        }
        return $inventoryData;
    }

    public function insertComment(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('StoreCirculationTransportId', 'Comment'));
        if($postData['StoreCirculationTransportId'] > 0 && !empty($postData['Comment'])){
            $postData['UserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
            $this->load->model('Mstorecirculationtransportcomments');
            $flag = $this->Mstorecirculationtransportcomments->save($postData);
            if($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật ghi chú thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function searchByFilter(){
        $user = $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->loadModel(array('Mstorecirculationtransports', 'Mstores', 'Mtransports'));
        $flag = $this->Mactions->checkAccessFromDb('store/viewAll', $user['UserId']);
        if(!$flag) $storeIds = $this->Mstores->getByUserId($user['UserId'], $flag, true);
        else $storeIds = array();
        $data1 = $this->Mstorecirculationtransports->searchByFilter($searchText, $itemFilters, $limit, $page, $storeIds);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }
}