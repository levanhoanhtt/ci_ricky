<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customerconsult extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Tư vấn lại',
			array(
				'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
				'scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js','js/search_item.js', 'js/customer_consult_list.js'))
			)
		);
		if($this->Mactions->checkAccess($data['listActions'], 'customerconsult')) {
			$this->loadModel(array('Mfilters','Mtags'));
			$itemTypeId = 30;
			$data['listFilters'] = $this->Mfilters->getList($itemTypeId);
			$data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => $itemTypeId));
			$this->load->view('customerconsult/list', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function index_new(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Tư vấn lại',
			array(
				'scriptHeader' => array('css' => array('vendor/plugins/star_rating/starrr.css','vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
				'scriptFooter' => array('js' => array('vendor/plugins/star_rating/starrr.js','vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js','js/search_item.js', 'js/customer_consult_list_new.js'))
			)
		);
		if($this->Mactions->checkAccess($data['listActions'], 'customerconsult')) {
			$this->loadModel(array('Mfilters','Mtags'));
			$itemTypeId = 30;
			$data['listFilters'] = $this->Mfilters->getList($itemTypeId);
			$data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => $itemTypeId));
			$this->load->view('customerconsult/list_new', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function listNotCustomer(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách DATA',
			array(
				'scriptHeader' => array('css' => array('vendor/plugins/star_rating/starrr.css','vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
				'scriptFooter' => array('js' => array('vendor/plugins/star_rating/starrr.js','vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js','js/search_item.js', 'js/customer_consult_list_not_customer.js'))
			)
		);
		if($this->Mactions->checkAccess($data['listActions'], 'customerconsult')) {
			$this->loadModel(array('Mfilters','Mtags'));
			$itemTypeId = 30;
			$data['listFilters'] = $this->Mfilters->getList($itemTypeId);
			$data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => $itemTypeId));
			$this->load->view('customerconsult/list_customer_not', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function user(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Tư vấn lại của tôi',
			array(
				'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
				'scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js','js/search_item.js', 'js/customer_consult_list.js'))
			)
		);
		if($this->Mactions->checkAccess($data['listActions'], 'customerconsult/user')) {
			$this->load->model('Mfilters');
			$data['listFilters'] = $this->Mfilters->getList(30);
			$this->load->view('customerconsult/user', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function web(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Khách hàng quan tâm',
			array(
				'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
				'scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js','js/search_item.js', 'js/customer_consult_web_list.js'))
			)
		);
		if($this->Mactions->checkAccess($data['listActions'], 'customerconsult/web')) {
			$this->loadModel(array('Mfilters', 'Mparts', 'Mstores'));
			$data['listFilters'] = $this->Mfilters->getList(30);
			$data['listParts'] = $this->Mparts->getList();
			$data['listUsers'] = $this->Musers->getListForSelect();
			$this->load->view('customerconsult/web', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function add($isUser = 0){
		$user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Tạo tư vấn lại',
            array(
				'scriptHeader' => array('css' => 'vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css'),
				'scriptFooter' => array('js' => array('vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'js/choose_item_customer_consult.js', 'js/customer_consult_update.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'customerconsult')) {
            $this->loadModel(array('Mcustomerconsults','Mcategories', 'Mprovinces', 'Mdistricts', 'Mcountries', 'Mwards', 'Mcustomergroups', 'Mparts'));
			if($isUser != 0 && $isUser != 1) $isUser = 0;
			$data['isUser'] = $isUser;
            $data['listCategories'] = $this->Mcategories->getListByItemType(1);
			$data['listCountries'] = $this->Mcountries->getList();
			$data['listProvinces'] = $this->Mprovinces->getList();
			$data['listDistricts'] = $this->Mdistricts->getList();
			$data['listUsers'] = $this->Musers->getListForSelect($user['UserId'], 'Chỉ mình tôi');
			$data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
			$objName = '';
			if($isUser == 1) $objName = $user['FullName'];
			else $objName = $this->Mparts->getFieldValue(array('PartId' => PART_SALE_ID), 'PartName');
			$data['objName'] = $objName;
			$this->load->view('customerconsult/add', $data);
        }
        else $this->load->view('user/permission', $data);
	}

	public function edit($customerConsultId = 0){
	    if ($customerConsultId > 0) {
	        $user = $this->checkUserLogin();
	        $data = $this->commonData($user,
                'Thông tin tư vấn lại',
                array(
					'scriptHeader' => array('css' => array('vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css', 'vendor/plugins/bootstrap-slider/slider.css','css/modal_call.css')),
					'scriptFooter' => array('js' => array('vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'vendor/plugins/bootstrap-slider/bootstrap-slider.js', 'js/choose_item.js', 'js/customer_consult_update.js'))
                )
            );
	        if($this->Mactions->checkAccess($data['listActions'], 'customerconsult')) {
	            $this->loadModel(array('Mcustomers', 'Mcustomerconsults', 'Mprovinces', 'Mdistricts', 'Mcountries', 'Mwards', 'Mcategories', 'Mproducts', 'Mproductchilds', 'Mcustomergroups','Mconsultproducts','Mconsultcomments', 'Mcustomerconsultusers', 'Mparts', 'Morders', 'Mactionlogs'));
	            $customerConsult = $this->Mcustomerconsults->get($customerConsultId);
	            if ($customerConsult) {
	                $data['customerConsultId'] = $customerConsultId;
					$data['listCountries'] = $this->Mcountries->getList();
					$data['listProvinces'] = $this->Mprovinces->getList();
					$data['listDistricts'] = $this->Mdistricts->getList();
		            $data['listCategories'] = $this->Mcategories->getListByItemType(1);
					$data['listConsultProducts'] = $this->Mconsultproducts->getBy(array('CustomerConsultId' => $customerConsultId));
					$customerConsult['CustomerEmail'] = '';
					$customerConsult['CustomerAddress'] = '';
					$customerConsult['ProvinceId'] = 0;
					$customerConsult['DistrictId'] = 0;
					$customerConsult['WardId'] = 0;
					if($customerConsult['CustomerId'] > 0){
						$customer = $this->Mcustomers->get($customerConsult['CustomerId']);
						if($customer){
							$customerConsult['CustomerEmail'] = $customer['Email'];
							$customerConsult['CustomerAddress'] = $customer['Address'];
							$customerConsult['ProvinceId'] = $customer['ProvinceId'];
							$customerConsult['DistrictId'] = $customer['DistrictId'];
							$customerConsult['WardId'] = $customer['WardId'];
						}
					}
					else if(!empty($customerConsult['CustomerInfo'])){
						$customerInfo = json_decode($customerConsult['CustomerInfo'], true);
						$customerConsult['CustomerEmail'] = $customerInfo['CustomerEmail'];
						$customerConsult['CustomerAddress'] = $customerInfo['CustomerAddress'];
						$customerConsult['ProvinceId'] = $customerInfo['DistrictId'];
						$customerConsult['DistrictId'] = $customerInfo['DistrictId'];
						$customerConsult['WardId'] = $customerInfo['WardId'];
					}
					$data['customerConsult'] = $customerConsult;
					$data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
					/*$data['listUsers'] = $this->Musers->getListForSelect($user['UserId'], 'Chỉ mình tôi');
					$data['listParts'] = $this->Mparts->getList(true);*/
					$objName = '';
					if($customerConsult['UserId'] > 0) $objName = $this->Musers->getFieldValue(array('UserId' => $customerConsult['UserId']), 'FullName');
					elseif($customerConsult['PartId'] > 0) $objName = $this->Mparts->getFieldValue(array('PartId' => $customerConsult['PartId']), 'PartName');
					$data['objName'] = $objName;
					//$data['listCustomerConsultUsers'] = $this->Mcustomerconsultusers->getList($customerConsultId);
					$data['listConsultComments'] = $this->Mconsultcomments->getList($customerConsultId);
					$data['listActionLogs'] = $this->Mactionlogs->getList($customerConsultId, 30);
					$listCustomerConsultUsers = $this->Mcustomerconsultusers->getList($customerConsultId);
					$consultUsers1 = $consultUsers2 = array();
					foreach($listCustomerConsultUsers as $cu){
						if($cu['ConsultSellId'] > 0) $consultUsers1[] = $cu;
						else $consultUsers2[] = $cu;
					}
					$data['consultUsers1'] = $consultUsers1;
					$data['consultUsers2'] = $consultUsers2;
					$data['listOrders'] = $this->Morders->search(array('CustomerId' => $customerConsult['CustomerId']));
	            }
	            else {
	                $data['customerConsultId'] = 0;
	                $data['txtError'] = "Không tìm thấy dữ liệu";
	            }
	            $this->load->view('customerconsult/edit', $data);
	        }
	        else $this->load->view('user/permission', $data);
	    }
	    else redirect('customerconsult');
	}

	private $remindStatus = array(
        1 => 'alert-warning',
        2 => 'alert-warning',
        3 => 'alert-danger',
        4 => 'alert-danger',
        5 => 'alert-success',
        6 => 'alert-danger'
    );

	public function edit2($customerConsultId = 0){
	    if ($customerConsultId > 0) {
	        $user = $this->checkUserLogin();
	        $data = $this->commonData($user,
                'Thông tin tư vấn lại',
                array(
					'scriptHeader' => array('css' => array('vendor/plugins/star_rating/starrr.css','vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css', 'vendor/plugins/bootstrap-slider/slider.css','css/modal_call.css')),
					'scriptFooter' => array('js' => array('vendor/plugins/star_rating/starrr.js','vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'vendor/plugins/bootstrap-slider/bootstrap-slider.js', 'js/choose_item.js', 'js/customer_consult_update_2.js'))
                )
            );
	        if($this->Mactions->checkAccess($data['listActions'], 'customerconsult')) {
	            $this->loadModel(array('Mcustomers', 'Mcustomerconsults', 'Mprovinces', 'Mdistricts', 'Mcountries', 'Mwards', 'Mcategories', 'Mproducts', 'Mproductchilds', 'Mcustomergroups','Mconsultproducts','Mconsultcomments', 'Mcustomerconsultusers', 'Mparts', 'Morders', 'Mactionlogs'));
	            $customerConsult = $this->Mcustomerconsults->get($customerConsultId);
	            if ($customerConsult) {
	                $data['customerConsultId'] = $customerConsultId;
					$data['listCountries'] = $this->Mcountries->getList();
					$data['listProvinces'] = $this->Mprovinces->getList();
					$data['listDistricts'] = $this->Mdistricts->getList();
		            $data['listCategories'] = $this->Mcategories->getListByItemType(1);
					$data['listConsultProducts'] = $this->Mconsultproducts->getBy(array('CustomerConsultId' => $customerConsultId));
					$customerConsult['CustomerEmail'] = '';
					$customerConsult['CustomerAddress'] = '';
					$customerConsult['ProvinceId'] = 0;
					$customerConsult['DistrictId'] = 0;
					$customerConsult['WardId'] = 0;
					if($customerConsult['CustomerId'] > 0){
						$customer = $this->Mcustomers->get($customerConsult['CustomerId']);
						if($customer){
							$customerConsult['CustomerEmail'] = $customer['Email'];
							$customerConsult['CustomerAddress'] = $customer['Address'];
							$customerConsult['ProvinceId'] = $customer['ProvinceId'];
							$customerConsult['DistrictId'] = $customer['DistrictId'];
							$customerConsult['WardId'] = $customer['WardId'];
						}
					}
					else if(!empty($customerConsult['CustomerInfo'])){
						$customerInfo = json_decode($customerConsult['CustomerInfo'], true);
						$customerConsult['CustomerEmail'] = $customerInfo['CustomerEmail'];
						$customerConsult['CustomerAddress'] = $customerInfo['CustomerAddress'];
						$customerConsult['ProvinceId'] = $customerInfo['DistrictId'];
						$customerConsult['DistrictId'] = $customerInfo['DistrictId'];
						$customerConsult['WardId'] = $customerInfo['WardId'];
					}
					$data['customerConsult'] = $customerConsult;
					$data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
					$data['listUsers'] = $this->Musers->getListForSelect($user['UserId'], 'Chỉ mình tôi');
					$data['listParts'] = $this->Mparts->getList(true);
					$objName = '';
					if($customerConsult['UserId'] > 0) $objName = $this->Musers->getFieldValue(array('UserId' => $customerConsult['UserId']), 'FullName');
					elseif($customerConsult['PartId'] > 0) $objName = $this->Mparts->getFieldValue(array('PartId' => $customerConsult['PartId']), 'PartName');
					$data['objName'] = $objName;
					//$data['listCustomerConsultUsers'] = $this->Mcustomerconsultusers->getList($customerConsultId);
					$data['listConsultComments'] = $this->Mconsultcomments->getList($customerConsultId);
					$data['listActionLogs'] = $this->Mactionlogs->getList($customerConsultId, 30);
					$listCustomerConsultUsers = $this->Mcustomerconsultusers->getList($customerConsultId);
					$consultUsers1 = $consultUsers2 = array();
					foreach($listCustomerConsultUsers as $cu){
						if($cu['ConsultSellId'] > 0) $consultUsers1[] = $cu;
						else $consultUsers2[] = $cu;
					}
					$data['consultUsers1'] = $consultUsers1;
					$data['consultUsers2'] = $consultUsers2;
					$data['listOrders'] = $this->Morders->search(array('CustomerId' => $customerConsult['CustomerId']));
                    $data['alertCss'] = $this->remindStatus;
	            }
	            else {
	                $data['customerConsultId'] = 0;
	                $data['txtError'] = "Không tìm thấy dữ liệu";
	            }
	            $this->load->view('customerconsult/edit_2', $data);
	        }
	        else $this->load->view('user/permission', $data);
	    }
	    else redirect('customerconsult');
	}

	public function updateDataError(){
		$user = $this->checkUserLogin();
		$customerConsultId = $this->input->post('CustomerConsultId');
		if($customerConsultId > 0){
			$this->loadModel(array('Mcustomerconsults'));
			$postData = array(
				'IsError' => 1,
			);
			$flag = $this->Mcustomerconsults->save($postData, $customerConsultId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật Data lỗi thành công."));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function info($customerConsultId = 0){
        if ($customerConsultId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Thông tin Khách hàng',
                array(
                    'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                    'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js','js/choose_item.js', 'js/customerconsult_info.js'))
                )
            );
            if ($this->Mactions->checkAccess($data['listActions'], 'customerconsultinfo')) {
                $this->loadModel(array('Mcustomers', 'Mcustomerconsults', 'Mprovinces', 'Mdistricts', 'Mcountries', 'Mwards', 'Mcategories', 'Mproducts', 'Mproductchilds', 'Mcustomergroups','Mconsultproducts','Mconsultcomments', 'Mcustomerconsultusers', 'Mparts', 'Morders', 'Mactionlogs','Msmslogs'));
	            $customerConsult = $this->Mcustomerconsults->get($customerConsultId);
	            if ($customerConsult) {
	                $data['customerConsultId'] = $customerConsultId;
					$data['listCountries'] = $this->Mcountries->getList();
					$data['listProvinces'] = $this->Mprovinces->getList();
					$data['listDistricts'] = $this->Mdistricts->getList();
		            $data['listCategories'] = $this->Mcategories->getListByItemType(1);
					$data['listConsultProducts'] = $this->Mconsultproducts->getBy(array('CustomerConsultId' => $customerConsultId));
					$customerConsult['CustomerEmail'] = '';
					$customerConsult['CustomerAddress'] = '';
					$customerConsult['ProvinceId'] = 0;
					$customerConsult['DistrictId'] = 0;
					$customerConsult['WardId'] = 0;
					$flagCustomerInfo = false;
					if($customerConsult['CustomerId'] > 0){
						$flagCustomerInfo = true;
						$customer = $this->Mcustomers->get($customerConsult['CustomerId']);
						if($customer){
							$customerConsult['CustomerEmail'] = $customer['Email'];
							$customerConsult['CustomerAddress'] = $customer['Address'];
							$customerConsult['ProvinceId'] = $customer['ProvinceId'];
							$customerConsult['DistrictId'] = $customer['DistrictId'];
							$customerConsult['CountryId'] = $customer['CountryId'];
							$customerConsult['WardId'] = $customer['WardId'];
							$customerConsult['CustomerKindId'] = $customer['CustomerKindId'];
							$customerConsult['CustomerTypeId'] = $customer['CustomerTypeId'];
							$customerConsult['FirstName'] = $customer['FirstName'];
							$customerConsult['LastName'] = $customer['LastName'];
							$customerConsult['PhoneNumber'] = $customer['PhoneNumber'];
							$customerConsult['GenderId'] = $customer['GenderId'];
							$customerConsult['BirthDay'] = $customer['BirthDay'];
							$customerConsult['Facebook'] = $customer['Facebook'];
							$customerConsult['Comment'] = $customer['Comment'];
							$customerConsult['IsReceiveAd'] = $customer['IsReceiveAd'];
							$customerConsult['CustomerGroupId'] = $customer['CustomerGroupId'];
							$customerConsult['CareStaffId'] = $customer['CareStaffId'];
							$customerConsult['DiscountTypeId'] = $customer['DiscountTypeId'];
							$data['listSmsLog'] = $this->Msmslogs->getListSmsCustomer($customer['PhoneNumber']);
						}
					}
					else if(!empty($customerConsult['CustomerInfo'])){
						$customerInfo = json_decode($customerConsult['CustomerInfo'], true);
						$customerConsult['CustomerEmail'] = $customerInfo['Email'];
						$customerConsult['CustomerAddress'] = $customerInfo['Address'];
						$customerConsult['ProvinceId'] = $customerInfo['ProvinceId'];
						$customerConsult['DistrictId'] = $customerInfo['DistrictId'];
						$customerConsult['WardId'] = $customerInfo['WardId'];
					}
					$data['flagCustomerInfo'] = $flagCustomerInfo;
					$data['customerConsult'] = $customerConsult;
					$data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
					$data['listUsers'] = $this->Musers->getListForSelect($user['UserId'], 'Chỉ mình tôi');
					$data['listParts'] = $this->Mparts->getList(true);
					$objName = '';
					if($customerConsult['UserId'] > 0) $objName = $this->Musers->getFieldValue(array('UserId' => $customerConsult['UserId']), 'FullName');
					elseif($customerConsult['PartId'] > 0) $objName = $this->Mparts->getFieldValue(array('PartId' => $customerConsult['PartId']), 'PartName');
					$data['objName'] = $objName;
					//$data['listCustomerConsultUsers'] = $this->Mcustomerconsultusers->getList($customerConsultId);
					$data['listConsultComments'] = $this->Mconsultcomments->getList($customerConsultId);
					$data['listActionLogs'] = $this->Mactionlogs->getList($customerConsultId, 30);
					$listCustomerConsultUsers = $this->Mcustomerconsultusers->getList($customerConsultId);
					$consultUsers1 = $consultUsers2 = array();
					foreach($listCustomerConsultUsers as $cu){
						if($cu['ConsultSellId'] > 0) $consultUsers1[] = $cu;
						else $consultUsers2[] = $cu;
					}
					$data['consultUsers1'] = $consultUsers1;
					$data['consultUsers2'] = $consultUsers2;
					$data['listOrders'] = $this->Morders->search(array('CustomerId' => $customerConsult['CustomerId']));
                    $data['alertCss'] = $this->remindStatus;
                }
                else {
                    $data['customerConsultId'] = 0;
                    $data['txtError'] = "Không tìm thấy khách hàng";
                }
                $this->load->view('customerconsult/info', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('customerconsult');
    }

	public function sell(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Đăng ký doanh số',
			array(
                'scriptHeader' => array('css' => array('vendor/plugins/daterangepicker/daterangepicker.css', 'vendor/plugins/bootstrap-slider/slider.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/bootstrap-slider/bootstrap-slider.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js', 'js/customer_consult_sell.js'))
            )
		);
		if ($this->Mactions->checkAccess($data['listActions'], 'customerconsult/sell')) {
			$postData = $this->arrayFromPost(array('UserId'));
			$dateRange = trim($this->input->post('DateRange'));
			if(!empty($dateRange)){
				$dateRange = explode('-', $dateRange);
				if(count($dateRange) == 2) {
					$postData['BeginDate'] = ddMMyyyyToDate($dateRange[0]);
					$postData['EndDate'] = ddMMyyyyToDate($dateRange[1], 'd/m/Y', 'Y-m-d 23:59:59');
				}
			}
			$this->loadModel(array('Mconsultsells', 'Mcustomerconsultusers'));
			$data['listUsers'] = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED));
			$rowCount = $this->Mconsultsells->getCount($postData);
			$data['listConsultSells'] = array();
			if ($rowCount > 0) {
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if (!is_numeric($page) || $page < 1) $page = 1;
				$data['listConsultSells'] = $this->Mconsultsells->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('customerconsult/sell', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$isFront = $this->input->post('IsFront');
		$crDateTime = getCurentDateTime();
		if($isFront == 1){
			$postData = $this->arrayFromPost(array('ConsultTitle', 'FullName', 'PhoneNumber'));
			$postData = array_merge($postData, array(
				'Facebook' => '',
				'ConsultDate' => $crDateTime,
				'RemindStatusId' => 1,
				'UserId' => 0,
				'PartId' => PART_SALE_ID,
				'OrderChannelId' => 1,
				'CTVId' => 0,
				'TimeProcessed' => 0,
				'OrderId' => 0
			));
			$user = false;
		}
		else{
			$user = $this->checkUserLogin(true);
			$postData = $this->arrayFromPost(array('ConsultTitle', 'CustomerId', 'FullName', 'PhoneNumber', 'Facebook', 'ConsultDate', 'RemindStatusId', 'UserId', 'PartId', 'OrderChannelId', 'CTVId', 'TimeProcessed', 'OrderId','ConsultStar'));
			if(!empty($postData['ConsultDate'])){
				$consultDate = $postData['ConsultDate'];
				$postData['ConsultDate'] = ddMMyyyyToDate($consultDate, 'd/m/Y H:i', 'Y-m-d H:i');
				$isOutOfDate = strtotime($postData['ConsultDate']) <= strtotime($crDateTime);
				if($postData['RemindStatusId'] == 1 && $isOutOfDate){
					echo json_encode(array('code' => -1, 'message' => "Thời điểm cần xử lý không hợp lệ"));
					die();
				}
			}
		}
		if(!empty($postData['PhoneNumber'])) {
			$this->loadModel(array('Mcustomers', 'Mcustomerconsults', 'Mcustomerconsultusers','Mspeedsms'));
			$customerConsultId = $this->input->post('CustomerConsultId');
			if($customerConsultId == 0 && $isFront == 0){
				$flag = $this->Mcustomerconsults->getFieldValue(array('PhoneNumber' => $postData['PhoneNumber'], 'RemindStatusId' => 1), 'CustomerConsultId', 0);
				if($flag > 0){
					echo json_encode(array('code' => -1, 'message' => "Tư vấn lại đã có"));
					die();
				}
			}
			if($isFront == 1) $postData['CustomerId'] = $this->Mcustomers->checkExist(0, array('PhoneNumber' => $postData['PhoneNumber']));
			elseif($postData['CustomerId'] == 0) $postData['CustomerId'] = $this->Mcustomers->checkExist(0, array('PhoneNumber' => $postData['PhoneNumber']));
			$commentStatus = $this->input->post('CommentStatus');
			if($postData['RemindStatusId'] == 4 || $postData['RemindStatusId'] == 6) $postData['CommentCancel'] = $commentStatus;
			else if($postData['RemindStatusId'] == 5) $postData['CommentComplete'] = $commentStatus;
			$comment = 'Khách tự thêm Tư vấn lại';
			if($user) $comment = $customerConsultId == 0 ? $user['FullName'] . ': Thêm mới tư vấn lại ':  $user['FullName'] . ': Cập nhật TVL sang trạng thái '.$this->Mconstants->remindStatus[$postData['RemindStatusId']];
			$actionLogs = array(
                'ItemTypeId' => 30,
                'ActionTypeId' => 1,
                'Comment' => $comment,
                'CrUserId' => $user ? $user['UserId'] : 0,
                'CrDateTime' => $crDateTime
            );
			if($customerConsultId > 0){
				$postData['UpdateUserId'] = $user ? $user['UserId'] : 0;
				$postData['UpdateDateTime'] = $crDateTime;
			}
			else{
			    $postData['CustomerInfo'] = '';
				$postData['CrUserId'] = $user ? $user['UserId'] : 0;
				$postData['CrDateTime'] = $crDateTime;
			}
			$products = json_decode(trim($this->input->post('Products')), true);
			$comments = json_decode(trim($this->input->post('Comments')), true);
			$customerConsultId = $this->Mcustomerconsults->update($postData, $customerConsultId, $products, $comments, $actionLogs, $isFront);
			if ($customerConsultId > 0){
				if($isFront == 1){
					$this->load->helper('slug');
					if(!empty($postData['FullName'])){
						$expFullName = explode("_",trim(removeAccent($postData['FullName'])));
						$name = "Cam on ".$expFullName[count($expFullName)-1];
					}
					else $name ="Cam on ban";
	                $message = $name." da de lai loi nhan, Tu van vien RICKY se lien he lai som nhat. Chuc ban mot ngay vui ve ! Hotline : 0971.477.007 | Web : RICKY.VN";
                    $this->Mspeedsms->send(array($postData['PhoneNumber']), $message, $user['UserId']);
				} 
				else $message = 'Cập nhật tư vấn lại thành công';
				echo json_encode(array('code' => 1, 'message' => $message, 'data' => $customerConsultId));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function insertFromWeb(){
		$postData = $this->arrayFromPost(array('CustomerId', 'ConsultTitle', 'FullName', 'PhoneNumber', 'CustomerInfo', 'CTVId'));
		if(!empty($postData['ConsultTitle']) && !empty($postData['PhoneNumber'])){
			$this->loadModel(array('Mcustomers', 'Mcustomerconsults','Mpromotions','Mspeedsms'));
			$postData['CustomerId'] = $this->Mcustomers->checkExist(0, array('PhoneNumber' => $postData['PhoneNumber']));
			$crDateTime = getCurentDateTime();
			$couponCode = trim($this->input->post('CouponCode'));
			$promotion = $this->Mpromotions->getBy(array('PromotionName' => $couponCode), true);
            if($promotion && $promotion['PromotionStatusId'] == 2){
                $flag = false;
                if($promotion['NumberUse'] > 0 || $promotion['IsUnLimit'] == 2) {
                    $flag = true;
                    if (!empty($promotion['EndDate'])) {
                        if (strtotime(getCurentDateTime()) > strtotime($promotion['EndDate'])) $flag = false;
                    }
                }
                if($flag){
	            	$postData['PromotionId'] = $promotion['PromotionId'];
	            	$postData['DiscountPrice'] = $promotion['ReduceNumber'];
	            }
            }
			$postData['Facebook'] = '';
			$postData['ConsultDate'] = $crDateTime;
			$postData['RemindStatusId'] = 1;
			$postData['UserId'] = 0;
			$postData['PartId'] = PART_SALE_ID;
			$postData['OrderChannelId'] = 1;
			$postData['OutOfDate'] = 1;
			$postData['TimeProcessed'] = 0;
			$postData['OrderId'] = 0;
			$postData['CrUserId'] = 0;
			$postData['CrDateTime'] = $crDateTime;
			$actionLogs = array(
				'ItemTypeId' => 30,
				'ActionTypeId' => 1,
				'Comment' => 'Khách hàng đặt trên web',
				'CrUserId' => 0,
				'CrDateTime' => $crDateTime,
			);
			$products = json_decode(trim($this->input->post('Products')), true);
			if($postData['CTVId'] > 0){
				$affiliateData = $this->arrayFromPost(array('LinkAffId', 'AffiliateName', 'OfferId', 'OfferName'));
				if($affiliateData['LinkAffId'] > 0 && $affiliateData['AffiliateName'] == 'Ricky' && $affiliateData['OfferId'] == 1 && $affiliateData['OfferName'] == 'Ricky'){
					//check api linklogs
					$this->load->model('Mproducts');
					$productSlugs = array();
					$productJson = array();
					foreach($products as $p){
						if(!isset($productSlugs[$p['ProductId']])) $productSlugs[$p['ProductId']] = $this->Mproducts->getFieldValue(array('ProductId' => $p['ProductId']), 'ProductSlug');
						$productJson[] = array(
							'ProductId' => $p['ProductId'],
							'ProductChildId' => $p['ProductChildId'],
							'ProductUrl' => 'https://ricky.vn/products/'.$productSlugs[$p['ProductId']]
						);
					}
					$productJson = json_encode($productJson);
					$this->load->helper('slug');
					$json = curlCrawl('https://aff.ricky.vn/api/link/check', "UserName=hoanmuada&UserPass=123456789&CTVId={$postData['CTVId']}&LinkAffId={$affiliateData['LinkAffId']}&AffiliateName={$affiliateData['AffiliateName']}&OfferId={$affiliateData['OfferId']}&OfferName={$affiliateData['OfferName']}&Products={$productJson}");
					$json = @json_decode($json, true);
					if($json['code'] != 1) $postData['CTVId'] = 0;
				}
			}
			$comments = json_decode(trim($this->input->post('Comments')), true);
			$customerConsultId = $this->Mcustomerconsults->update($postData, 0, $products, $comments, $actionLogs);
			if ($customerConsultId > 0){
				$this->load->helper('slug');
				if(!empty($postData['FullName'])){
					$expFullName = explode("_",trim(removeAccent($postData['FullName'])));
					$name = "Cam on ".$expFullName[count($expFullName)-1];
				}
				else $name ="Cam on ban";
                $message = $name." da dat hang tai RICKY.VN, Tu van vien RICKY se lien he lai som nhat, Chuc ban mot ngay vui ve ! Hotline : 0971.477.007 | Web : RICKY.VN";
                $this->Mspeedsms->send(array($postData['PhoneNumber']), $message, 0);
				echo json_encode(array('code' => 1, 'message' => "Thêm đơn hàng thành công"));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function updateCustomerFacebook(){
		$user = $this->checkUserLogin(true);
		$customerId = $this->input->post('CustomerId');
		$facebook = $this->input->post('Facebook');
		if($customerId > 0 && !empty($facebook)){
			$customerConsultId = $this->input->post('CustomerConsultId');
			$crDateTime = getCurentDateTime();
			$this->loadModel(array('Mcustomers', 'Mcustomerconsults'));
			$this->load->model('Mcustomerconsults');
			$flag = $this->Mcustomerconsults->updateCustomerFacebook(array('Facebook' => $facebook, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime), $customerId, $customerConsultId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật Facebook thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function insertComment(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('CustomerConsultId', 'Comment', 'CommentTypeId'));
		// var_dump($postData);
		if($postData['CustomerConsultId'] > 0 && !empty($postData['Comment'])){ // && $postData['CommentTypeId'] > 0
			$postData['UserId'] = $user['UserId']; 
			$postData['CrDateTime'] = getCurentDateTime();
			$customerConsultUser = array();
			$this->loadModel(array('Mcustomerconsultusers', 'Mconsultcomments'));
			$customerConsultUserId = $this->Mcustomerconsultusers->getFieldValue(array('CustomerConsultId' => $postData['CustomerConsultId'], 'ConsultSellId' => 0, 'UserId' => $user['UserId']), 'CustomerConsultUserId', 0);
			if($customerConsultUserId == 0){
				$customerConsultUser = array(
					'ConsultSellId' => 0,
					'CustomerConsultId' => $postData['CustomerConsultId'],
					'UserId' => $user['UserId'],
					'IsCrOrder' => 1,
					'Percent' => 0,
					'Comment' => '',
					'CrDateTime' => $postData['CrDateTime']
				);
			}
			$data = $this->Mconsultcomments->insert($postData, $customerConsultUser, $customerConsultUserId);
			if($data) echo json_encode(array('code' => 1, 'message' => "Cập nhật ghi chú thành công", 'data' => $data));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function updateCustomerConsultUser(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('ConsultSellId', 'Percent', 'Comment', 'ConsultCommentId'));
		$customerConsultUserId = $this->input->post('CustomerConsultUserId');
		if($customerConsultUserId > 0 && $postData['Percent'] >= 0){
			$postData['UpdateDateTime'] = getCurentDateTime();
			$this->load->model('Mcustomerconsultusers');
			$customerConsultUserId = $this->Mcustomerconsultusers->save($postData, $customerConsultUserId);
			if($customerConsultUserId > 0){
				$listConsultUsers = $postData['ConsultSellId'] > 0 ? $this->Mcustomerconsultusers->getBy(array('ConsultSellId' => $postData['ConsultSellId'])) : array();
				echo json_encode(array('code' => 1, 'message' => "Cập nhật đăng ký chốt thành công", 'data' => $listConsultUsers));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeStatus(){
		$user = $this->checkUserLogin(true);
		$customerConsultId = $this->input->post('CustomerConsultId');
		if($customerConsultId > 0){
			$postData = array(
				'RemindStatusId' => 2,
				'CrUserId' => $user['UserId'],
				'UpdateUserId' => $user['UserId'],
				'UpdateDateTime' => getCurentDateTime()
			);
			$this->load->model('Mcustomerconsults');
			$customerConsultId = $this->Mcustomerconsults->save($postData, $customerConsultId);
			if ($customerConsultId > 0) echo json_encode(array('code' => 1, 'message' => "Chuyển sang bên tư vấn lại thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));

		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function searchByFilter($isHasCrUserId = 0, $userId = 0){
        $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
		$postData = $this->arrayFromPost(array('CustomerId'));
		$postData['UserId'] = $userId;
		$postData['IsHasCrUserId'] = $isHasCrUserId;
        $this->load->model(array('Mcustomerconsults','Mconsultproducts','Mproducts','Mproductchilds','Mconsultcomments'));
        $data1 = $this->Mcustomerconsults->searchByFilter($searchText, $itemFilters, $limit, $page, $postData);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }

    public function searchByFilterNew(){
    	$user = $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->load->model(array('Mcustomerconsults','Mconsultcomments'));
        $data1 = $this->Mcustomerconsults->searchByFilterNew($searchText, $itemFilters, $limit, $page);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }

    public function searchByFilterNotCustomer(){
    	$user = $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->load->model(array('Mcustomerconsults','Mconsultcomments'));
        $data1 = $this->Mcustomerconsults->searchByFilterNotCustomer($searchText, $itemFilters, $limit, $page);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }

    public function showComment(){
    	$this->checkUserLogin(true);
    	$customerConsultId = $this->input->post('CustomerConsultId');
    	if($customerConsultId > 0){
    		$this->loadModel(array('Mconsultcomments', 'Mcustomerconsultusers'));
    		$listCustomerConsultUsers = $this->Mcustomerconsultusers->getList($customerConsultId);
            $consultUsers1 = $consultUsers2 = array();
            foreach($listCustomerConsultUsers as $cu){
                if($cu['ConsultSellId'] > 0) $consultUsers1[] = $cu;
                else $consultUsers2[] = $cu;
            }
    		$data = $this->Mconsultcomments->getBy(array('CustomerConsultId' => $customerConsultId));
    		$now = new DateTime(date('Y-m-d'));
    		for ($i = 0; $i < count($data); $i++) {
    			$dayDiff = getDayDiff($data[$i]['CrDateTime'], $now);
    			$data[$i]['CrDateTime'] = getDayDiffText($dayDiff).ddMMyyyy($data[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
    			$data[$i]['Avatar'] = (empty($data[$i]['Avatar']) ? USER_PATH.NO_IMAGE : USER_PATH.$data[$i]['Avatar']);
    			$flag = false;
    			$data[$i]['FullName'] = $data[$i]['UserId'] > 0 ? $this->Musers->get($data[$i]['UserId'])['FullName']: '';
    			foreach($consultUsers2 as $cu){
                    if($data[$i]['ConsultCommentId'] == $cu['ConsultCommentId']){
                        $flag = $cu['Percent'] > 0;
                        break;
                    }
                }
                $data[$i]['Flag'] = $flag;
    		}
    		echo json_encode(array('code' => 1,'message' => 'Danh sách quá trình xử lý', 'data' => $data));
    	}
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateStar(){
        $user = $this->checkUserLogin(true);
        $customerConsultId = $this->input->post('CustomerConsultId');
        $consultStar = $this->input->post('ConsultStar');
        if($customerConsultId > 0 && $consultStar > 0){
            $this->load->model('Mcustomerconsults');
            $flag = $this->Mcustomerconsults->changeStatus($consultStar, $customerConsultId, 'ConsultStar', $user['UserId']);
            if($flag) echo json_encode(array('code' => 1,'message' => 'Cập nhật mức độ tìm năng thành công'));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateInfoCustomer(){
    	$user = $this->checkUserLogin(true);
    	$itemIds = json_decode($this->input->post('ItemIds'), true);
    	if(!empty($itemIds)){
    		$flag = false;
    		$this->loadModel(array('Mcustomers','Mcustomerconsults'));
    		$crDateTime = getCurentDateTime();
    		foreach ($itemIds as $key => $id) {
    			$infoCustomerConsult = $this->Mcustomerconsults->get($id);
    			$fullName = trim($infoCustomerConsult['FullName']);
    			$info = json_decode($infoCustomerConsult['CustomerInfo'], true);
    			$lastName = '';
    			$firstName = '';
    			if(!empty($fullName)){
    				$expFullName = explode(" ", $fullName);
	    			$lastName = $expFullName[count($expFullName) -1];
	    			$firstName = str_replace($lastName,'', $fullName);
    			}
    			$postData = array(
    				'FullName' => $fullName,
    				'LastName' => $lastName,
    				'FirstName' => $firstName,
    				'PhoneNumber' => $infoCustomerConsult['PhoneNumber'],
    				'Email' => !empty($info) ? $info['CustomerEmail']: '',
    				'CountryId' => !empty($info) ? $info['CountryId']: 0,
    				'ProvinceId' => !empty($info) ? $info['ProvinceId']: 0,
    				'DistrictId' => !empty($info) ? $info['DistrictId']: 0,
    				'WardId' => !empty($info) ? $info['WardId']: 0,
    				'ZipCode' => !empty($info) ? $info['ZipCode']: '',
    				'Address' => !empty($info) ? $info['CustomerAddress']: '',
    				'CrUserId' => $user['UserId'],
    				'CrDateTime' => $crDateTime,
    				'StatusId' => STATUS_ACTIVED
    			);
    			$customerId = $this->Mcustomers->save($postData);
    			if($customerId > 0){
    				$flag = $this->db->update('customerconsults', array('CustomerId' => $customerId, 'CustomerConsultCode' => $this->genCustomerCode($id)), array('CustomerConsultId' => $id));
    			}
    		}
    		if($flag) echo json_encode(array('code' => 1,'message' => 'Cập nhật thành công', 'data' => $itemIds));
    		else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    	}
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    private function genCustomerCode($CustomerConsultId, $prefix = 'KH'){
        return $prefix . '-' . ($CustomerConsultId + 10000);
    }
}