<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sentencegroup extends MY_Controller {

	public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách nhóm câu',
            array('scriptFooter' => array('js' => array('js/sentencegroup.js','ckfinder/ckfinder.js')))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'sentencegroup')) {
            $this->load->model('Msentencegroups');
            $data['listSentencegroups'] = $this->Msentencegroups->getList();
            $this->load->view('sentencegroup/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $user =$this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('SentenceGroupName'));

        if(!empty($postData['SentenceGroupName'])) {
            $sentenceGroupId = $this->input->post('SentenceGroupId');
            $this->load->model('Msentencegroups');
            $check = $this->Msentencegroups->checkName($postData['SentenceGroupName']);
          
            if(count($check) == 0){
            	$flag = $this->Msentencegroups->save($postData, $sentenceGroupId);
	            if ($flag > 0) {
	                $postData['SentenceGroupId'] = $flag;
	                $postData['IsAdd'] = ($sentenceGroupId > 0) ? 0 : 1;
	                echo json_encode(array('code' => 1, 'message' => "Cập nhật nhóm câu thành công", 'data' => $postData));
	            }
	            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }else echo json_encode(array('code' => -2, 'message' => "Nhóm câu {$postData['SentenceGroupName']} bị trùng"));
            
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}