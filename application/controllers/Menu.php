<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Controller {
    
    /**
     * List menu
     */
    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách menu',
            array(
            )
        );
        $this->loadModel(array('Mmenus'));
        $data['listMenus'] = $this->Mmenus->getList();
        $this->load->view('menu/list', $data);
    }
    
    /**
     * Add menu
     */
    public function create() {
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm mới Menu',
            array(
                'scriptFooter' => array('js' => array('js/menu_update.js'))
            )
            );
        $this->loadModel(array('Mcustomergroups'));
        $this->load->view('menu/add_menu', $data);
    }
    
    /**
     * Example menu
     */
    public function exampleMenu() {
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Menu mẫu',
            array(
            )
            );
        $this->loadModel(array('Mmenuitems'));
        $id = isset($_GET['id'])?$_GET['id']:0;
        $data['menuParentItems'] = $this->Mmenuitems->getWithCondition(['ParentItemId'=>0, 'MenuId'=>$id]);
        $data['groupChildrenItems'] = $this->Mmenuitems->getChildrenItem($id);
        $this->load->view('menu/example', $data);
    }
}
