<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fbpage extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách trang facebook',
            array('scriptFooter' => array('js' => 'js/fb_page.js'))
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'fbpage')) {
            $this->load->model('Mfbpages');
            $data['listFbpages'] = $this->Mfbpages->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('fbpage/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }


    public function index_1(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách trang facebook',
            array('scriptFooter' => array('js' => 'js/fb_page_1.js'))
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'fbpage')){
            $this->load->model('Mfbpages');
            $data['listFbPages'] = $this->Mfbpages->getBy(array('StatusId > ' => 0));
            $this->load->view('fbpage/list_1', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function loadFbPage(){
        $user = $this->checkUserLogin();
        $this->load->model('Mfbpages');
        $fbPageNotActive = $this->Mfbpages->getBy(array('StatusId' => 1));
        $fbPageActive = $this->Mfbpages->getBy(array('StatusId' => STATUS_ACTIVED));
        echo json_encode(array('code' => 1, 'fbPageNotActive' => $fbPageNotActive, 'fbPageActive' => $fbPageActive));
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('FbPageName', 'FbPageCode', 'Prefix'));
        if (!empty($postData['FbPageName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $postData['UserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
            $fbPageId = $this->input->post('FbPageId');
            $this->load->model('Mfbpages');
            //check trung 
            $checPrefix = $this->Mfbpages->checkPrefix($fbPageId, $postData['Prefix']);
            if ($checPrefix == 0) {
                if ($fbPageId == 0) $this->Mfbpages->createTableDB($postData['Prefix']);
                $flag = $this->Mfbpages->save($postData, $fbPageId);
                if ($flag > 0) {
                    $postData['FbPageId'] = $flag;
                    $postData['IsAdd'] = ($fbPageId > 0) ? 0 : 1;
                    echo json_encode(array('code' => 1, 'message' => "Cập nhật page FB thành công", 'data' => $postData));
                } else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => 0, 'message' => "Prefix đã trùng vui lòng ghi lại"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function save(){
        $user = $this->checkUserLogin(true);
        $datas = json_decode(trim($this->input->post('data')),true);
        if(!empty($datas)){
           $this->load->model('Mfbpages');
           $flag = 0;
           $checkPages = 0;
            foreach ($datas["data"] as $data) {
                $checkPage = $this->Mfbpages->checkPage($data['id']);
                if($checkPage == 0){
                    $postData = array(
                        'FbPageName'    => $data['name'],
                        'FbPageCode'    => $data['id'],
                        'Prefix'        => $data['id'],
                        'StatusId'      => 1,
                        'CrDateTime'    => getCurentDateTime(),
                        'UserId'        => $user['UserId'],
                    );
                    $flag = $this->Mfbpages->save($postData, 0);
                    
                }else $checkPages ++;
            }
            if ($flag > 0)echo json_encode(array('code' => 1, 'message' => "Cập nhật page FB thành công"));
            if($checkPages > 0)echo json_encode(array('code' => -1, 'message' => "Có {$checkPages} Page này đã tồn tại"));
        }else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function changeStatus(){
        $user       = $this->checkUserLogin(true);
        $fbPageId   = $this->input->post('FbPageId');
        $statusId   = $this->input->post('StatusId');
        $prefix     = $this->input->post('Prefix');
        if($fbPageId > 0){
            $postData['StatusId'] = $statusId;
            $postData['UserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();

            $this->load->model('Mfbpages');
            $flag = $this->Mfbpages->save($postData, $fbPageId);
            if ($flag > 0){
                $this->Mfbpages->createTableDB($prefix);
                echo json_encode(array('code' => 1, 'message' => "Cập nhật trạng thái Page FB thành công"));
            }else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $user = $this->checkUserLogin(true);
        $fbPageId = $this->input->post('FbPageId');
        if ($fbPageId > 0) {
            $this->load->model('Mfbpages');
            $flag = $this->Mfbpages->changeStatus(0, $fbPageId);
            if ($flag) echo json_encode(array('code' => 1, 'message' => "Xóa loại page FB thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}