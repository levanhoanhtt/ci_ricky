<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offsetreason extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách lý do gửi bù',
            array('scriptFooter' => array('js' => 'js/offset_reason.js'))
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'offsetreason')) {
            $this->load->model('Moffsetreasons');
            $data['listOffsetReasons'] = $this->Moffsetreasons->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/offset_reason', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('OffsetReasonName'));
        if(!empty($postData['OffsetReasonName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $offsetReasonId = $this->input->post('OffsetReasonId');
            $this->load->model('Moffsetreasons');
            $flag = $this->Moffsetreasons->save($postData, $offsetReasonId);
            if ($flag > 0) {
                $postData['OffsetReasonId'] = $flag;
                $postData['IsAdd'] = ($offsetReasonId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật lý do gửi bù thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $offsetReasonId = $this->input->post('OffsetReasonId');
        if($offsetReasonId > 0){
            $this->load->model('Moffsetreasons');
            $flag = $this->Moffsetreasons->changeStatus(0, $offsetReasonId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa lý do gửi bù thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}