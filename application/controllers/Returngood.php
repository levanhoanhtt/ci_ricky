<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Returngood extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Đơn hoàn hàng về',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_item.js', 'js/returngood.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'returngood')) {
            $this->loadModel(array('Mfilters', 'Mcustomers', 'Mstores','Mtransporters','Mtransporttypes','Mtags'));
            $itemTypeId = 14;
            $data['listCustomers'] = $this->Mcustomers->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listFilters'] = $this->Mfilters->getList($itemTypeId);
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => $itemTypeId));
            $this->load->view('returngood/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Tạo Đơn hoàn hàng về',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/choose_item.js', 'js/returngood_update.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'returngood')) {
            $this->loadModel(array('Mprovinces', 'Mdistricts', 'Mcategories', 'Mstores'));
            $data['listStores'] = $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listProvinces'] = $this->Mprovinces->getList();
            $data['listCategories'] = $this->Mcategories->getListByItemType(1);
            $this->load->view('returngood/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($returnGoodId = 0){
        if($returnGoodId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Cập nhật Đơn hoàn hàng về',
                array(
                    'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css'),
                    'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/choose_item.js', 'js/returngood_update.js'))
                )
            );
            $this->loadModel(array('Mreturngoods', 'Mreturngoodproducts', 'Mproductunits', 'Mproducts', 'Mproductchilds', 'Mcustomers', 'Mcustomeraddress', 'Mtags', 'Mprovinces', 'Mdistricts', 'Mcategories', 'Mstores', 'Mactionlogs'));
            $returnGood = $this->Mreturngoods->get($returnGoodId);
            if ($returnGood) {
                if ($this->Mactions->checkAccess($data['listActions'], 'returngood')) {
                    $data['title'] .= ' ' . $returnGood['ReturnGoodCode'];
                    $data['canEdit'] = true;// $returnGood['ReturngoodStatusId'] == 1 || $returnGood['ReturngoodStatusId'] == 9;
                    $data['returnGoodId'] = $returnGoodId;
                    $data['returnGood'] = $returnGood;
                    $data['customerAddress'] = $this->Mcustomeraddress->getInfo($returnGood['CustomerAddressId'], $returnGood['CustomerId']);
                    $data['listStores'] = $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED));
                    $data['listProvinces'] = $this->Mprovinces->getList();
                    $data['listCategories'] = $this->Mcategories->getListByItemType(1);
                    $data['tagNames'] = $this->Mtags->getTagNames($returnGoodId, 14);
                    $data['listReturnGoodProducts'] = $this->Mreturngoodproducts->getBy(array('ReturnGoodId' => $returnGoodId));
                    $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listActionLogs'] = $this->Mactionlogs->getList($returnGoodId, 14);
                    $this->load->view('returngood/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else {
                $data['canEdit'] = false;
                $data['returnGoodId'] = 0;
                $data['txtError'] = "Không tìm thấý Đơn hoàn hàng về";
                $this->load->view('returngood/edit', $data);
            }
        }
        else redirect('order');
    }

    public function searchByFilter(){
        $user = $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->loadModel(array('Mreturngoods', 'Mtransports'));
        $data1 = $this->Mreturngoods->searchByFilter($searchText, $itemFilters, $limit, $page);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }
}