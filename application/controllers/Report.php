<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MY_Controller {

	public function order($reportTypeId = 0){
        $user = $this->checkUserLogin();
        if(!is_numeric($reportTypeId) || $reportTypeId < 1 || $reportTypeId > 2) $reportTypeId = 1;
        $reportTypeName = 'Doanh '.($reportTypeId == 1 ? 'thu' : 'số');
        $data = $this->commonData($user,
            'Báo cáo '.$reportTypeName,
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/daterangepicker/daterangepicker.css', 'css/revenue.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js', 'js/report_order.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'report/order/'.$reportTypeId)) {
            $data['reportTypeId'] = $reportTypeId;
            $data['reportTypeName'] = $reportTypeName;
            $this->loadModel(array('Mstores','Mproducttypes'));
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listUsers'] = $this->Musers->getListCreatedOrder();
            $this->load->view('report/order', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function order_v2($reportTypeId = 0){
        $user = $this->checkUserLogin();
        if(!is_numeric($reportTypeId) || $reportTypeId < 1 || $reportTypeId > 2) $reportTypeId = 1;
        $reportTypeName = 'Doanh '.($reportTypeId == 1 ? 'thu' : 'số');
        $data = $this->commonData($user,
            'Báo cáo '.$reportTypeName,
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/daterangepicker/daterangepicker.css', 'css/revenue.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js', 'js/report_order_v2.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'report/order_v2/'.$reportTypeId)) {
            $data['reportTypeId'] = $reportTypeId;
            $data['reportTypeName'] = $reportTypeName;
            $this->loadModel(array('Mstores','Mproducttypes'));
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listUsers'] = $this->Musers->getListCreatedOrder();
            $this->load->view('report/order_v2', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function product(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thống kê sản phẩm bán được',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/daterangepicker/daterangepicker.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js', 'js/report_product.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'report/product')) $this->load->view('report/product', $data);
        else $this->load->view('user/permission', $data);
    }

    public function fundBalance(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Sổ quỹ',
            array(
                /*'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css', 'css/revenue.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/search_item.js', 'js/report_fun_balance.js'))*/

                'scriptHeader' => array('css' => array('vendor/plugins/daterangepicker/daterangepicker.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css', 'css/revenue.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/search_item.js', 'js/report_fun_balance.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'report/fundBalance')) {
            $this->loadModel(array('Mfilters', 'Mbanks'));
            $data['listFilters'] = $this->Mfilters->getList(26);
            $data['listBanks'] = $this->Mbanks->search(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('report/fund_balance', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function orderReason(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thống kê Lý do mua hàng',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/daterangepicker/daterangepicker.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js', 'js/report_order_reason.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'report/orderReason')){
            $this->load->model('Morderreasons');
            $data['listOrderReasons'] = $this->Morderreasons->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('report/order_reason', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function customer(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Báo cáo khách hàng',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/daterangepicker/daterangepicker.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js', 'js/report_customer.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'report/customer')) {
            $this->load->model('Mcustomers');
            $data['listUsers'] = $this->Musers->getListForSelect();
            $postData = $this->arrayFromPost(array('UserId'));
            $dateRangePicker =  trim($this->input->post('DateRangePicker'));
            if(!empty($dateRangePicker)){
                $parts = explode('-', $dateRangePicker);
                if(count($parts) == 2){
                    $postData['BeginDate'] = ddMMyyyyToDate($parts[0]);
                    $postData['EndDate'] = ddMMyyyyToDate($parts[1], 'd/m/Y', 'Y-m-d 23:59:59');
                }
            }
            $rowCount = $this->Mcustomers->getCountReport($postData);
            $data['listCustomers'] = array();
            if($rowCount > 0){
                $perPage = DEFAULT_LIMIT;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $listCustomers = $this->Mcustomers->reportSearch($postData, $perPage, $page);
                $data['listCustomers'] = $listCustomers;
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('report/customer', $data);
        }
        else $this->load->view('user/permission', $data);
    }
}