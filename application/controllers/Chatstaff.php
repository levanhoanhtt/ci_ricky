<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chatstaff extends MY_Controller {

    public function index(){ //danh cho staff
        $user = $this->checkUserLogin();
        $this->load->model('Mchatstaffs');
        $data = array(
            'title' => 'Chat với khách hàng',
            'user' => $user,
            'listChatCares' => $this->Mchatstaffs->getLastedChat($user['UserId'], 1),
            'listChatTechs' => $this->Mchatstaffs->getLastedChat($user['UserId'], 2)
        );
        $this->load->view('chatstaff/staff', $data);
    }

    public function customer(){ //danh cho KH
        $data = array(
            'title' => 'Chat với tư vấn - kĩ thuật',
            'user' => false,
            //'scriptFooter' => array('js' => array('js/chat_customer.js')),
            'customerChat' => $this->session->userdata('customerChat')
        );
        $this->load->view('chatstaff/customer', $data);
    }

    public function admin(){ //danh cho admin vao xem tat ca
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Xem tin nhắn chat của nhân viên'
        );
        if($this->Mactions->checkAccess($data['listActions'], 'chatstaff/admin')){
            $data['listStaffs'] = $this->Musers->getListForSelect();
            $data['techIds'] = $this->Musers->getUserIdByActionCde('chatstaff/tech');
            $data['careIds'] = $this->Musers->getUserIdByActionCde('chatstaff/care');
            $this->load->view('chatstaff/admin', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function setCustomerChat(){
        $postData = $this->arrayFromPost(array('FullName', 'PhoneNumber'));
        if(!empty($postData['FullName']) && !empty($postData['PhoneNumber'])){
            $this->load->model('Mcustomers');
            $postData['CustomerId'] = $this->Mcustomers->getFieldValue(array('PhoneNumber' => $postData['PhoneNumber']), 'CustomerId', 0);
            $this->session->set_userdata('customerChat', $postData);
            echo $postData['CustomerId'];
        }
        else echo 0;
    }

    public function insert(){
        header('Access-Control-Allow-Origin: *');
        $postData = $this->arrayFromPost(array('CustomerId', 'CustomerName', 'PhoneNumber', 'StaffId', 'StaffRoleId', 'Message', 'FileUrl', 'IsCustomerSend'));
        if($postData['StaffId'] > 0 && $postData['StaffRoleId'] > 0 && !empty($postData['CustomerName']) && !empty($postData['PhoneNumber']) && $postData['IsCustomerSend'] >= 0){
            $postData['IsRead'] = 0;
            $ipAddress = $this->input->ip_address();
            $postData['IpAddress'] = $ipAddress;
            $crDateTime = getCurentDateTime();
            $postData['CrDateTime'] = $crDateTime;
            $postData['Message'] = $this->makeLink($postData['Message']);
            if(!empty($postData['FileUrl'])) $postData['FileUrl'] = replaceFileUrl($postData['FileUrl'], PRODUCT_PATH);
            $this->load->model('Mchatstaffs');
            $chatId = $this->Mchatstaffs->save($postData);
            if($chatId > 0){
                echo json_encode(array('code' => 1, 'data' => array(
                        'ChatId' => $chatId,
                        'Message' => $postData['Message'],
                        'FileUrl' => $postData['FileUrl'],
                        'IpAddress' => $ipAddress,
                        'CrDateTime' => ddMMyyyy($crDateTime, 'd/m/Y H:i:s'))
                    )
                );
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getList(){
        $postData = $this->arrayFromPost(array('PhoneNumber', 'StaffId', 'StaffRoleId'));
        if($postData['StaffId'] > 0){
            $this->load->model('Mchatstaffs');
            $limit = $this->input->post('Limit');
            $start = $this->input->post('Start');
            if (!is_numeric($limit) || $limit <= 0) $limit = 20;
            if (!is_numeric($start) || $start < 0) $start = 0;
            $listChats = $this->Mchatstaffs->search($postData, $limit, $start);
            $data = array();
            foreach ($listChats as $c) {
                $c['CrDateTime'] = ddMMyyyy($c['CrDateTime'], 'd/m/Y H:i:s');
                $data[] = $c;
            }
            echo json_encode(array('code' => 1, 'data' => array_reverse($data)));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getListStaff(){
        $customerPhone = trim($this->input->post('CustomerPhone'));
        if(!empty($customerPhone)){
            $this->load->model('Mchatstaffs');
            $listStaffs = $this->Musers->getListForSelect();
            $techIds = $this->Musers->getUserIdByActionCde('chatstaff/tech');
            $careIds = $this->Musers->getUserIdByActionCde('chatstaff/care');
            $listChatCares = $this->Mchatstaffs->getLastedChat(0, 1, $customerPhone);
            $listChatTechs = $this->Mchatstaffs->getLastedChat(0, 2, $customerPhone);
            $data1 = array();
            $data2 = array();
            foreach($listStaffs as $s){
                if(in_array($s['UserId'], $careIds)){
                    $s['Message'] = $this->Mconstants->getObjectValue($listChatCares, 'StaffId', $s['UserId'], 'Message');
                    $data1[] = $s;
                }
                if(in_array($s['UserId'], $techIds)){
                    $s['Message'] = $this->Mconstants->getObjectValue($listChatTechs, 'StaffId', $s['UserId'], 'Message');
                    $data2[] = $s;
                }
            }
            echo json_encode(array('code' => 1, 'data' => array('ChatCares' => $data1, 'ChatTechs' => $data2)));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getListCustomer(){
        $this->checkUserLogin(true);
        $staffId = $this->input->post('StaffId');
        $staffRoleId = $this->input->post('StaffRoleId');
        if($staffId > 0 && $staffRoleId > 0){
            $this->load->model('Mchatstaffs');
            $listChats = $this->Mchatstaffs->getLastedChat($staffId, $staffRoleId);
            echo json_encode(array('code' => 1, 'data' => $listChats));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    //chat o header
    public function getListChatUnRead(){
        $user = $this->checkUserLogin(true);
        $this->load->model('Mchatstaffs');
        $listChats = $this->Mchatstaffs->getLastedChat($user['UserId']);
        $data = array();
        foreach($listChats as $c){
            $c['Message'] = strip_tags($c['Message']);
            $c['CrTime'] = ddMMyyyy($c['CrDateTime'], 'H:i:s');
            $c['CrDate'] = ddMMyyyy($c['CrDateTime'], 'd/m/Y');
            $data[]=$c;
        }
        echo json_encode(array('code' => 1, 'data' => $data));
    }

    public function updateCountChatUnRead(){
        $phoneNumber = $this->input->post('PhoneNumber');
        $staffId = $this->input->post('StaffId');
        if(!empty($phoneNumber) && $staffId > 0){
            $this->load->model('Mchatstaffs');
            $isStaff = ($this->input->post('IsStaff') == 1) ? true : false;
            $this->Mchatstaffs->updateReadMessage($phoneNumber, $staffId, $isStaff);
            echo 1;
        }
        else echo 0;
    }

    private function makeLink($text){
        if($num_found = preg_match_all('~[a-z]+://\S+~', $text, $out)){
            $links = array();
            foreach($out[0] as $link){
                if(!in_array($link, $links)) $links[]=$link;
            }
            foreach($links as $link) $text = str_replace($link, '<a href="'.$link.'" target="_blank">'.$link.'</a>', $text);
        }
        return $text;
    }
}