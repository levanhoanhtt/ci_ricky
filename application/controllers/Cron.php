<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends MY_Controller {

    public function setRemindOutOfDate(){
        $this->db->query('UPDATE reminds SET OutOfDate = 2 WHERE OutOfDate = 1 AND RemindDate <= NOW()');
        $this->db->query('UPDATE customerconsults SET OutOfDate = 2 WHERE OutOfDate = 1 AND ConsultDate <= NOW()');
    }

    public function calcQuantityCombo(){
        $this->loadModel(array('Minventories', 'Mproducts', 'Mproductchilds', 'Mproductquantity'));
        $listInventories = $this->Minventories->getListJustUpdate();
        if(!empty($listInventories)) {
            $productIds = array();//id con
            $storeIds = array();
            foreach ($listInventories as $inventory) {
                if(!in_array($inventory['ProductId'], $productIds)) $productIds[] = $inventory['ProductId'];
                if(!in_array($inventory['StoreId'], $storeIds)) $storeIds[] = $inventory['StoreId'];
            }
            $comboIds = $this->Mproducts->getListProductIdCombo($productIds);
            $productCombo = array();
            $productIds = array();
            foreach ($comboIds as $productId) {
                $listProductChilds = $this->Mproductchilds->getBy(array('ProductId' => $productId, 'StatusId' => STATUS_ACTIVED), false, '', 'ProductPartId, ProductPartChildId, Quantity');
                $productCombo[$productId] = $listProductChilds;
                foreach ($listProductChilds as $pc) {
                    if (!in_array($pc['ProductPartId'], $productIds)) $productIds[] = $pc['ProductPartId'];
                }
            }
            if (!empty($productIds)) {
                $productQuantityUpdates = array();
                $listProductQuantity = $this->Mproductquantity->getListQuantity($productIds);
                foreach ($storeIds as $storeId) {
                    foreach ($productCombo as $productId => $listProductChilds) {
                        $quantityChilds = array();
                        foreach ($listProductChilds as $pc) {
                            foreach ($listProductQuantity as $pq) {
                                if ($pq['StoreId'] == $storeId && $pq['ProductId'] == $pc['ProductPartId'] && $pq['ProductChildId'] == $pc['ProductPartChildId']) {
                                    $key = $pc['ProductPartId'] . '_' . $pc['ProductPartChildId'];
                                    if ($pc['Quantity'] > 0) $quantityChilds[$key] = round($pq['Quantity'] / $pc['Quantity']);
                                    else $quantityChilds[$key] = 0;
                                }
                            }
                        }
                        $n = count($listProductChilds);
                        $comboQuantity = 0;
                        if($n > 0 && $n == count($quantityChilds)) {
                            $comboQuantity = PHP_INT_MAX;
                            foreach ($quantityChilds as $key => $quantity) {
                                if ($quantity < $comboQuantity) $comboQuantity = $quantity;
                            }
                        }
                        $productQuantityUpdates[] = array(
                            'ProductId' => $productId,
                            'ProductChildId' => 0,
                            'Quantity' => $comboQuantity,
                            'StoreId' => $storeId
                        );
                    }
                }
                $crDateTime = getCurentDateTime();
                foreach ($productQuantityUpdates as $pq) {
                    $productQuantityId = $this->Mproductquantity->getFieldValue(array('ProductId' => $pq['ProductId'], 'ProductChildId' => $pq['ProductChildId'], 'StoreId' => $pq['StoreId']), 'ProductQuantityId', 0);
                    if($productQuantityId > 0) {
                        $this->Mproductquantity->save(array(
                            'Quantity' => $pq['Quantity'],
                            'UpdateUserId' => 0,
                            'UpdateDateTime' => $crDateTime
                        ), $productQuantityId);
                    }
                    else {
                        $pq['CrUserId'] = 0;
                        $pq['CrDateTime'] = $crDateTime;
                        $this->Mproductquantity->save($pq, 0, array('UpdateUserId', 'UpdateDateTime'));
                    }
                    preData($pq, false);
                }
            }
        }
    }

    public function insertTransactionBusinessNewMonth(){
        if(date('d') == 1){
            $this->load->model('Mtransactionbusiness');
            $this->Mtransactionbusiness->save(array(
                'TransactionTypeId' => 2,
                'TransactionDate' => date('Y-m-d'),
                'CrUserId' => 0,
                'CrDateTime' => getCurentDateTime()
            ));
        }
    }

    public function warningSmsBalance(){
        require_once APPPATH."/libraries/SpeedSMSAPI.php";
        $speedSMSAPI = new SpeedSMSAPI();
        $info = $speedSMSAPI->getUserInfo();
        $errorTypeId = 0;
        $balance = 0;
        if(!empty($info) && $info['status'] == 'success'){
            $balance = $info['data']['balance'];
            if($balance < 10000) $errorTypeId = 1;
        }
        else $errorTypeId = 2;
        if($errorTypeId > 0){
            $subject = 'Cảnh báo SMS Speed - '.($errorTypeId == 1) ? 'Sắp hết tiền' : 'API không kết nối được';
            $message = 'Gói SMS Speed của bạn '.($errorTypeId == 1) ? "sắp hết tiền ({$balance} VNĐ)" : 'API không kết nối được';;
            $flag = $this->sendMail('contact@ricky.vn', 'Ricky System', 'levanhoanhtt@gmail.com', $subject, $message);
            echo $flag ? 'Send mail OK'.PHP_EOL : 'Send mail failed'.PHP_EOL;
        }
        else echo 'Everything OK'.PHP_EOL;
    }
}