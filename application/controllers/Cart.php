<?php 

class Cart extends MY_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library('cart');
	}

	public function getCart(){
		$data = array(
			'carts' => $this->cart->contents(),
		);
		
		die(json_encode($data));
	}

	public function getSelectAddress(){
		$this->loadModel(array('Mcountries', 'Mprovinces', 'Mdistricts', 'Mwards'));
		$districts = $this->Mdistricts->selectHtml();
		$wards = $this->Mwards->selectHtml();
		$listProvinces = $this->Mprovinces->getList();
		$listCountries = $this->Mcountries->getList();
		$provinces = '<select class="form-control" name="ProvinceId" id="provinceId"><option value="0">--Chọn--</option>';
		foreach ($listProvinces as $pro) $provinces .= '<option value="'. $pro['ProvinceId'] .'">'. $pro['ProvinceName'] .'</option>';
		$provinces .= '</select>';
		$countries = '<select class="form-control" name="CountryId" id="countryId">';
		foreach($listCountries as $c) $countries .= '<option value="'. $c['CountryId'] .'">'. $c['CountryName'] .'</option>';
		$countries .= '</select>';
		echo json_encode(array('wards' => $wards, 'districts'=> $districts,'provinces' => $provinces, 'countries' => $countries));
	}

	private function totalItems(){
		return $this->cart->total_items();
	}

	public function add(){
		$id = $this->input->post('id');
		$this->loadModel(array('Mproducts','Mproductchilds'));
		$product = $this->Mproducts->get(intval($id));
		if(!$product) die(json_encode(array('error'=>1)));
		$qty = intval($this->input->post('qty'));
		$data  = array();
		$data['id'] = $product['ProductId'];
		$data['name'] = trim(str_replace(array('|', '(', ')'), ' ', $product['ProductName']));
		$data['product_child'] = 0;
		$data['child_name'] = '';

		if($this->input->post('productChild') > 0){
			 $product = $this->Mproductchilds->get($this->input->post('productChild'));
			 $data['product_child'] = $this->input->post('productChild');
			 $data['child_name']    =  $product['ProductName'];
			 $data['id'] = $product['ProductId'] . '-'.  $product['ProductChildId'];
		}
		$data['qty'] = $qty;
		$data['image_link'] = empty($product['ProductImage']) ? NO_IMAGE : $product['ProductImage'];
		$data['guaranteemonth'] = $product['GuaranteeMonth'];
		$data['sku'] = $product['Sku'];
		$data['price'] = $product['Price'];
		$data['old_price'] = $product['OldPrice'];
		if($this->cart->insert($data)) die(json_encode(array('error'=>0,'totalItems' => $this->totalItems())));
		else die(json_encode(array('error'=>2)));
	}

	public function update(){
		$carts = $this->cart->contents();
		foreach ($carts as $key => $val){
			$total_qty = $this->input->post("qty_$val[id]");
			$this->cart->update(array(
				'rowid' => $key,
				'qty' => $total_qty
			));
		}
		die(json_encode(array('error'=>0,'totalItems' => $this->totalItems())));
	}

	public function delete(){
		$id = $this->input->post('id');
		if($id > 0){
			$carts = $this->cart->contents();
			foreach ($carts as $key => $val) {
				if($val['id'] == $id){
					$data = array(
						'rowid' => $key,
						'qty' => 0
					);
					if($this->cart->update($data)) die(json_encode(array('error'=>0,'totalItems' => $this->totalItems())));
				}
			}
		}
	}

	public function destroy(){
		$this->cart->destroy();
	}
}