<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guarantee extends MY_Controller {

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('GuaranteeStep', 'GuaranteeImage', 'CustomerId', 'ReceiptTypeId', 'PaymentTypeId', 'ShipCost', 'StoreId', 'GuaranteeStatusId'));
        if($postData['GuaranteeStep'] > 0 && $postData['CustomerId'] > 0 && $postData['StoreId'] > 0 && $postData['GuaranteeStatusId'] > 0){
            $postData['GuaranteeImage'] = replaceFileUrl($postData['GuaranteeImage'], GUARANTEE_PATH);
            $postData['ShipCost'] = replacePrice($postData['ShipCost']);
            $crDateTime = getCurentDateTime();
            $actionLog = array(
                'ItemTypeId' => 27,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
            $guaranteeId = $this->input->post('GuaranteeId');
            if($guaranteeId > 0){
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = $crDateTime;
                $actionLog['ActionTypeId'] = 2;
                $actionLog['Comment'] = $user['FullName'] . ': Cập nhật bảo hành';
            }
            else {
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = $crDateTime;
                $actionLog['ActionTypeId'] = 1;
                $actionLog['Comment'] = $user['FullName'] . ': Thêm mới bảo hành';
            }
            $products = trim($this->input->post('Products'));
            $products = replaceFileUrl($products, GUARANTEE_PATH);
            $products = json_decode($products, true);
            $comments = json_decode(trim($this->input->post('Comments')), true);
            $this->load->model('Mguarantees');
            $guaranteeId = $this->Mguarantees->update($postData, $guaranteeId, $products, $comments, $actionLog);
            if($guaranteeId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật bảo hành thành công", 'data' => $guaranteeId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateField(){
        $user = $this->checkUserLogin(true);
        $guaranteeId = $this->input->post('GuaranteeId');
        $guaranteeStep = $this->input->post('GuaranteeStep');
        if($guaranteeId > 0 && $guaranteeStep > 0){
            $crDateTime = getCurentDateTime();
            $actionLog = array(
                'ItemId' => $guaranteeId,
                'ItemTypeId' => 27,
                'ActionTypeId' => 2,
                'Comment' => $user['FullName'] . " chuyển sang giai đoạn ".$this->Mconstants->guaranteeSteps[$guaranteeStep],
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
            $this->load->model('Mguarantees');
            $flag = $this->Mguarantees->updateField(array('GuaranteeStep' => $guaranteeStep, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime), $guaranteeId, $actionLog);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Chuyển sang CSKH thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function insertComment(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('GuaranteeId', 'Comment', 'CommentTypeId'));
        if($postData['GuaranteeId'] > 0 && !empty($postData['Comment']) && $postData['CommentTypeId'] > 0){
            $postData['UserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
            $this->load->model('Mguaranteecomments');
            $flag = $this->Mguaranteecomments->save($postData);
            if($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật ghi chú thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateProduct(){
        $user = $this->checkUserLogin(true);
        $guaranteeId = $this->input->post('GuaranteeId');
        $guaranteeStep = $this->input->post('GuaranteeStep');
        $products = trim($this->input->post('Products'));
        $products = replaceFileUrl($products, GUARANTEE_PATH);
        $products = json_decode($products, true);
        $count1 = count($products);
        if($guaranteeId > 0 && $guaranteeStep > 0 && $count1 > 0){
            $crDateTime = getCurentDateTime();
            $productUpdates = array();
            foreach($products as $p){
                $p['UpdateUserId'] = $user['UserId'];
                $p['UpdateDateTime'] = $crDateTime;
                $productUpdates[] = $p;
            }
            $comment = $user['FullName'].': ';
            if($guaranteeStep == 3) $comment .= 'Cập nhật Kiểm tra kỹ thuật';
            elseif($guaranteeStep == 4) $comment .= 'Cập nhật Phương án xử lý';
            else $comment .= 'Cập nhật sản phẩm';
            $actionLog = array(
                'ItemId' => $guaranteeId,
                'ItemTypeId' => 27,
                'ActionTypeId' => 2,
                'Comment' => $comment,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
            $this->load->model('Mguarantees');
            $count2 = $this->Mguarantees->updateProduct($productUpdates, $actionLog);
            if($count1 == $count2) echo json_encode(array('code' => 1, 'message' => "Cập nhật bảo hành thành công", 'data' => array('Comment' => $comment)));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateProductComment(){
        $user = $this->checkUserLogin(true);
        $guaranteeProductId = $this->input->post('GuaranteeProductId');
        $comment = trim($this->input->post('Comment'));
        $commentNo = $this->input->post('CommentNo');
        if($guaranteeProductId > 0 && !empty($comment) && in_array($commentNo, array(1, 2))){
            $postData = array('UpdateUserId' => $user['UserId'], 'UpdateDateTime' => getCurentDateTime());
            if($commentNo == 1) $postData['Comment1'] = $comment;
            else $postData['Comment2'] = $comment;
            $this->load->model('Mguaranteeproducts');
            $guaranteeProductId = $this->Mguaranteeproducts->save($postData, $guaranteeProductId);
            if($guaranteeProductId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật ghi chú thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function uploadImage(){
        header('Content-Type: application/json');
        $userId = trim($this->input->post('UserId'));//ng thuc hien
        $guaranteeImageGroupId = trim($this->input->post('GuaranteeImageGroupId'));
        if($userId > 0 && $guaranteeImageGroupId > 0){
            $guaranteeImages = array();
            if(!empty($_FILES['Image'])){
                $images = $this->reArrayFiles($_FILES['Image']);
                $crDateTime = getCurentDateTime();
                foreach ($images as $img) {
                    $newName = date('YmdHis', time()) . mt_rand() . '.jpg';
                    move_uploaded_file($img['tmp_name'], 'assets/uploads/guarantees/' . $newName);
                    $guaranteeImages[] = array(
                        'Image' => $newName,
                        'GuaranteeImageGroupId' => $guaranteeImageGroupId,
                        'UserId' => $userId,
                        'Comment' => trim($this->input->post('Comment')),
                        'CrDateTime' => $crDateTime
                    );
                }
            }
            if(!empty($guaranteeImages)){
                $this->db->insert_batch('guaranteeimages', $guaranteeImages);
                echo json_encode(array('code' => 1, 'message' => "Up ảnh bảo hành thành công"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Chưa chọn ảnh để up"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    private function reArrayFiles($file){
        $file_ary = array();
        $file_count = count($file['name']);
        $file_key = array_keys($file);
        for ($i = 0; $i < $file_count; $i++) {
            foreach ($file_key as $val) $file_ary[$i][$val] = $file[$val][$i];
        }
        return $file_ary;
    }

    public function searchByFilter(){
        $user = $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->loadModel(array('Mcustomers', 'Mreceipttypes', 'Mpaymenttypes','Mstores','Mguarantees'));
        $flag = $this->Mactions->checkAccessFromDb('store/viewAll', $user['UserId']);
        if(!$flag) $storeIds = $this->Mstores->getByUserId($user['UserId'], $flag, true);
        else $storeIds = array();
        $data1 = $this->Mguarantees->searchByFilter($searchText, $itemFilters, $limit, $page, $storeIds);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }
}