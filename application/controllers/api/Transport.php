<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transport extends MY_Controller{

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('TransportCode', 'OrderId', 'CustomerId', 'CustomerAddressId', 'TransportUserId', 'TransportStatusId', 'PendingStatusId', 'TransportTypeId', 'TransporterId', 'StoreId', 'Tracking', 'Weight', 'CODCost', 'CODStatusId', 'ShipCost', 'Comment', 'CancelReasonId', 'CancelComment'));
        $postData['Weight'] = replacePrice($postData['Weight']);
        $postData['CODCost'] = replacePrice($postData['CODCost']);
        $postData['ShipCost'] = replacePrice($postData['ShipCost']);
        if($postData['OrderId'] > 0 && $postData['CustomerId'] > 0 && $postData['TransportStatusId'] > 0) {
            $flag = true;
            $this->loadModel(array('Mcustomers', 'Morderservices', 'Mtransports', 'Morders', 'Morderproducts', 'Mproductchilds', 'Mproductquantity'));
            $paymentCost = replacePrice($this->input->post('PaymentCost'));
            if($paymentCost > 0){
                $customerBalance = $this->Mcustomers->getBalance($postData['CustomerId']);
                if($paymentCost > $customerBalance){
                    $flag = false;
                    echo json_encode(array('code' => -1, 'message' => "Tài khoản khách hàng không đủ tiền, cần hoàn thành phiếu chờ thanh toán"));
                }
            }
            $transportId = $this->input->post('TransportId');
            if($transportId == 0 && $flag){ //ktra xem co chua

                $transportStatusIds = $this->Mtransports->getListFieldValue(array('OrderId' => $postData['OrderId'], 'TransportStatusId >' => 0), 'TransportStatusId');
                $count = count($transportStatusIds);
                if($count > 0) {
                    $i = 0;
                    foreach ($transportStatusIds as $transportStatusId) {
                        if($transportStatusId == 5) $i++;
                    }
                    if($i < $count){
                        $flag = false;
                        echo json_encode(array('code' => -1, 'message' => "Vận chuyển đã được tạo trước đó"));
                    }
                }
            }
            if($flag){
                $crDateTime = getCurentDateTime();
                $actionLog = array(
                    'ItemTypeId' => 9,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
                $debitCost = 0;
                $inventoryData = array();
                if ($transportId > 0) {
                    $postData['UpdateUserId'] = $user['UserId'];
                    $postData['UpdateDateTime'] = $crDateTime;
                    $actionLog['ActionTypeId'] = 2;
                    $actionLog['Comment'] = $user['FullName'] . ': Cập nhật vận chuyển';
                }
                else {
                    $postData['CrUserId'] = $user['UserId'];
                    $postData['CrDateTime'] = $crDateTime;
                    $actionLog['ActionTypeId'] = 1;
                    $actionLog['Comment'] = $user['FullName'] . ': Thêm mới vận chuyển';
                    $debitCost = $this->Morderservices->getServiceCost($postData['OrderId'], DEBIT_OTHER_TYPE_ID);
                    $inventoryData = $this->getInventoryData($postData['OrderId'], $postData['StoreId'], $user['UserId'], $crDateTime, 2, 'Trừ số lượng từ đơn hàng '.$this->Morders->genOrderCode($postData['OrderId']));
                }
                $transportStatusLog = array(
                    'TransportStatusId' => $postData['TransportStatusId'],
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
                $tagNames = json_decode(trim($this->input->post('TagNames')), true);
                $metaData = array(
                    'OrderCode' => $this->input->post('OrderCode'),
                    'OrderReasonId' => $this->input->post('OrderReasonId'),
                    'OrderChanelId' => $this->input->post('OrderChanelId'),
                    'FullName' => $user['FullName'],
                    'PaymentCost' => $paymentCost,
                    'DebitCost' => $debitCost,
                    'InventoryData' => $inventoryData,
                    'TransportStatusLog' => $transportStatusLog
                );
                $transportId = $this->Mtransports->update($postData, $transportId, $tagNames, $actionLog, $metaData);
                if ($transportId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật vận chuyển thành công", 'data' => $transportId));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateField(){
        $user = $this->checkUserLogin(true);
        $transportId = $this->input->post('TransportId');
        $fieldName = trim($this->input->post('FieldName'));
        $fieldValue = trim($this->input->post('FieldValue'));
        if($transportId > 0 && !empty($fieldName) && !empty($fieldValue)){
            $this->load->model('Mtransports');
            $label = '';
            $value = '';
            $data = array();
            $metaData = array('UserId' => $user['UserId'], 'FullName' => $user['FullName']);
            $crDateTime = getCurentDateTime();
            $transportData = array($fieldName => $fieldValue, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime);
            $isSendSms = false;
            switch($fieldName){
                case 'CODStatusId':
                    if($fieldValue > 0) {
                        $cODStatusId = $this->Mtransports->getFieldValue(array('TransportId' => $transportId), 'CODStatusId', 0);
                        if($cODStatusId != 4) {
                            if($cODStatusId == 2 && $fieldValue < 4){}
                            else {
                                $label = 'Trạng thái thu hộ COD';
                                $value = $this->Mconstants->CODStatus[$fieldValue];
                                $data['StatusName'] = '<span class="' . $this->Mtransports->labelCss['CODStatusCss'][$fieldValue] . '">' . $value . '</span>';
                            }
                        }
                    }
                    break;
                case 'TransportStatusId':
                    if($fieldValue > 0){
                        $transportStatusId = $this->Mtransports->getFieldValue(array('TransportId' => $transportId), 'TransportStatusId', 0);
                        $flag = $transportStatusId != 4;
                        if($flag) $flag = $transportStatusId != $fieldValue;
                        if($flag) {
                            $label = 'Trạng thái vận chuyển';
                            $value = $this->Mconstants->transportStatus[$fieldValue];
                            $data['StatusName'] = '<span class="' . $this->Mtransports->labelCss['TransportStatusCss'][$fieldValue] . '">' . $value . '</span>';
                            if ($fieldValue == 4) { //Đã giao hàng
                                $transportData['CODStatusId'] = 2;
                                $this->load->model('Morders');
                                $orderId = $this->input->post('OrderId');
                                $metaData['OrderId'] = $orderId;
                                $metaData['OrderCode'] = $this->Morders->genOrderCode($orderId);
                                $metaData['StoreId'] = $this->input->post('StoreId');
                                $metaData['CustomerId'] = $this->input->post('CustomerId');
                                $metaData['CODCost'] = replacePrice($this->input->post('CODCost'));
                                $isSendSms = true;
                            }
                            if ($fieldValue == 5 || $fieldValue == 7){ //huy hoac chuyen hoan thanh cong
                                $transportData['CancelReasonId'] = $this->input->post('CancelReasonId');
                                $transportData['CancelComment'] = trim($this->input->post('CancelComment'));
                                $this->loadModel(array('Morders', 'Morderproducts', 'Mproductchilds', 'Mproductquantity', 'Minventories'));
                                $orderId = $this->input->post('OrderId');
                                $metaData['OrderId'] = $orderId;
                                $metaData['OrderCode'] = $this->Morders->genOrderCode($orderId);
                                $metaData['InventoryData'] = $this->getInventoryData($orderId, $this->input->post('StoreId'), $user['UserId'], $crDateTime, 1, 'Cộng số lượng từ đơn hàng ' . $metaData['OrderCode']);
                            }
                            $metaData['TransportStatusLog'] = array(
                                'TransportId' => $transportId,
                                'TransportStatusId' => $fieldValue,
                                'CrUserId' => $user['UserId'],
                                'CrDateTime' => $crDateTime
                            );
                        }
                    }
                    break;
                case 'PendingStatusId':
                    if($fieldValue > 0) {
                        $label = 'Trạng thái chờ xử lý';
                        $this->load->model('Mpendingstatus');
                        $value = $this->Mconstants->transportStatus[$fieldValue].' ('.$this->Mpendingstatus->getFieldValue(array('PendingStatusId' => $fieldValue), 'PendingStatusName').')';
                        $data['StatusName'] = '<span class="' . $this->Mconstants->labelCss[1] . '">'.$value.'</span>';
                    }
                    break;
                case 'StoreId':
                    $storeIdOld = $this->input->post('StoreId');
                    if($fieldValue > 0 && $fieldValue != $storeIdOld){
                        $label = 'Cơ sở';
                        $this->loadModel(array('Mstores', 'Morders', 'Morderproducts', 'Mproductchilds', 'Mproductquantity', 'Minventories'));
                        $value = $this->Mstores->getFieldValue(array('StoreId' => $fieldValue), 'StoreName');
                        $data['StoreName'] = $value;
                        $metaData['StoreName'] = $value;
                        $orderId = $this->input->post('OrderId');
                        $metaData['OrderId'] = $orderId;
                        $metaData['OrderCode'] = $this->Morders->genOrderCode($orderId);
                        $transportCode = trim($this->input->post('TransportCode'));
                        $listOrderProducts = $this->Morderproducts->getBy(array('OrderId' => $orderId));
                        $inventoryData = $this->getInventoryData($orderId, $storeIdOld, $user['UserId'], $crDateTime, 1, 'Cộng số lượng do đổi cơ sở từ vận chuyển '.$transportCode, $listOrderProducts);
                        $inventoryData = array_merge($inventoryData, $this->getInventoryData($orderId, $fieldValue, $user['UserId'], $crDateTime, 2, 'Trừ số lượng do đổi cơ sở từ vận chuyển '.$transportCode, $listOrderProducts));
                        $metaData['InventoryData'] = $inventoryData;
                    }
                    break;
                case 'Tracking':
                    $label = 'Mã vận đơn';
                    $value = $fieldValue;
                    $data['Tracking'] = $fieldValue;
                    break;
                case 'ShipCost':
                    $fieldValue = replacePrice($fieldValue);
                    if($fieldValue > 0) {
                        $label = 'Phí giao hàng';
                        $value = priceFormat($fieldValue) . ' VNĐ';
                    }
                    break;
                case 'TransporterId':
                    if($fieldValue > 0) {
                        $label = 'Nhà vận chuyển';
                        $this->load->model('Mtransporters');
                        $value = $this->Mtransporters->getFieldValue(array('TransporterId' => $fieldValue), 'TransporterName');
                        $data['TransporterName'] = $value;
                    }
                    break;
                default: break;
            }
            if(!empty($label)) {
                $comment = $user['FullName'] . " cập nhật {$label} thành {$value}";
                $data['Comment'] = $comment;
                $actionLog = array(
                    'ItemId' => $transportId,
                    'ItemTypeId' => 9,
                    'ActionTypeId' => 2,
                    'Comment' => $comment,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
                $flag = $this->Mtransports->updateField($transportData, $transportId, $actionLog, $metaData);
                if ($flag){
                    if($isSendSms){
                        $this->loadModel(array('Mcustomers', 'Mspeedsms'));
                        $this->load->helper('slug');
                        $this->sendSms($metaData['CustomerId'], $user['UserId']);
                    }
                    echo json_encode(array('code' => 1, 'message' => "Cập nhật vận chuyển thành công", 'data' => $data));
                }
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function changeStatusBatch(){
        $user = $this->checkUserLogin(true);
        $transportIds = json_decode(trim($this->input->post('TransportIds')), true);
        $transportStatusId = $this->input->post('TransportStatusId');
        if(!empty($transportIds) && $transportStatusId > 0){
            $this->load->model('Mtransports');
            $flag = $this->Mtransports->changeStatusBatch($transportIds, $transportStatusId, $user);
            if ($flag){
                if($transportStatusId == 4){ //send sms
                    foreach($transportIds as $transportId){
                        $customerId = $this->Mtransports->getFieldValue(array('TransportId' => $transportId), 'CustomerId', 0);
                        if($customerId > 0) $this->sendSms($customerId, $user['UserId']);
                    }
                }
                echo json_encode(array('code' => 1, 'message' => "Cập nhật vận chuyển thành công", 'data' => array('StatusName' => '<span class="'.$this->Mtransports->labelCss['TransportStatusCss'][$transportStatusId].'">'.$this->Mconstants->transportStatus[$transportStatusId].'</span>')));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    private function sendSms($customerId, $userId){
        $customer = $this->Mcustomers->get($customerId, true, '', 'FullName, PhoneNumber');
        if($customer) {
            if(!empty($customer['FullName'])){
                $expFullName = explode("_",trim(removeAccent($customer['FullName'])));
                $name = "Chao ".$expFullName[count($expFullName)-1];
            }
            else $name ="Chao ban";
            $message = $name.", Cam on ban da su dung dich vu tai RICKY, de duoc ho tro tot nhat, vui long lien he: 0971.477.007! Chuc ban mot ngay tot lanh!";
            $this->Mspeedsms->send(array($customer['PhoneNumber']), $message, $userId);
        }
    }

    private function getInventoryData($orderId, $storeId, $userId, $crDateTime, $inventoryTypeId, $comment, $listOrderProducts = array()){
        $inventoryData = array();
        if(empty($listOrderProducts)) $listOrderProducts = $this->Morderproducts->getBy(array('OrderId' => $orderId));
        foreach($listOrderProducts as $op){
            if($op['ProductKindId'] == 3){
                $listProductChilds = $this->Mproductchilds->getByProductId($op['ProductId']);
                foreach($listProductChilds as $pc){
                    if($pc['ProductPartId'] > 0){
                        $inventoryData[] = array(
                            'ProductId' => $pc['ProductPartId'],
                            'ProductChildId' => $pc['ProductPartChildId'],
                            'OldQuantity' => $this->Mproductquantity->getQuantity($pc['ProductPartId'], $pc['ProductPartChildId'], $storeId),
                            'Quantity' => $op['Quantity'] * $pc['Quantity'],
                            'InventoryTypeId' => $inventoryTypeId,
                            'StoreId' => $storeId,
                            'StatusId' => STATUS_ACTIVED,
                            'IsManual' => 1,
                            'Comment' => $comment,
                            'CrUserId' => $userId,
                            'CrDateTime' => $crDateTime,
                            'UpdateUserId' => $userId,
                            'UpdateDateTime' => $crDateTime
                        );
                    }
                }
            }
            //tru ca combo
            $inventoryData[] = array(
                'ProductId' => $op['ProductId'],
                'ProductChildId' => $op['ProductChildId'],
                'OldQuantity' => $this->Mproductquantity->getQuantity($op['ProductId'], $op['ProductChildId'], $storeId),
                'Quantity' => $op['Quantity'],
                'InventoryTypeId' => $inventoryTypeId,
                'StoreId' => $storeId,
                'StatusId' => STATUS_ACTIVED,
                'IsManual' => 1,
                'Comment' => $comment,
                'CrUserId' => $userId,
                'CrDateTime' => $crDateTime,
                'UpdateUserId' => $userId,
                'UpdateDateTime' => $crDateTime
            );
        }
        return $inventoryData;
    }

    public function insertComment(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('TransportId', 'Comment'));
        if($postData['TransportId'] > 0 && !empty($postData['Comment'])){
            $postData['UserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
            $this->load->model('Mtransportcomments');
            $flag = $this->Mtransportcomments->save($postData);
            if($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật ghi chú thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function searchByFilter(){
        $user = $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->loadModel(array('Mtransports', 'Mstores'));
        $flag = $this->Mactions->checkAccessFromDb('store/viewAll', $user['UserId']);
        if(!$flag) $storeIds = $this->Mstores->getByUserId($user['UserId'], $flag, true);
        else $storeIds = array();
        $data1 = $this->Mtransports->searchByFilter($searchText, $itemFilters, $limit, $page, $storeIds, $user['UserId']);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }

     public function searchByDataFilter(){
        $user = $this->checkUserLogin(true);
        $data = array();
        $itemTypeId = $this->input->post('itemTypeId');
         $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = 2;
        $this->loadModel(array('Mfilters'));
        $data1 = $this->Mfilters->searchByFilter($searchText, $limit, $page, $itemTypeId);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }

    public function getInfo(){
        header('Content-Type: application/json');
        $barCode = trim($this->input->post('BarCode'));
        if (!empty($barCode)) {
            $this->loadModel(array('Morders', 'Mcustomers', 'Mtransports', 'Mtransporttypes', 'Mtransporters'));
            $transport = $this->Mtransports->getBy(array('TransportCode' => $barCode), true);
            if ($transport && $transport['TransportStatusId'] > 0){
                $code = 1;
                $errors = array();
                if($transport['TransportStatusId'] != 2){
                    $code = 0;
                    $errors[] = 'Lỗi! Trạng thái vận chuyển đang là '.$this->Mconstants->transportStatus[$transport['TransportStatusId']];
                }
                $data = array(
                    'message' => 'Lấy thông tin vận chuyển thành công',
                    'BarCode' => $barCode,
                    'Customer' => $this->Mcustomers->get($transport['CustomerId'], true, '', 'FullName, PhoneNumber'),
                    'CODCost' => priceFormat($transport['CODCost']),
                    'TransportTypeId' => array('id' => $transport['TransportTypeId'], 'name' => $this->Mtransporttypes->getFieldValue(array('TransportTypeId' => $transport['TransportTypeId']), 'TransportTypeName')),
                    'Comment' => $transport['Comment'],
                    'CrDateTime' => ddMMyyyy($transport['CrDateTime']),
                    'Transporters' => $this->Mtransporters->getBy(array('ItemStatusId' => STATUS_ACTIVED)),
                    'ErrorList' => $errors
                );
                echo json_encode(array('code' => $code, 'message' => 'Lấy thông tin vận chuyển thành công', 'data' => $data));
            }
            else echo json_encode(array('code' => -1, 'message' => "Vận chuyển không tồn tại"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Mã vạch không được bỏ trống"));
    }

    public function getDetail(){
        $this->checkUserLogin(true);
        $transportId = $this->input->post('TransportId');
        if($transportId > 0){
            $this->loadModel(array('Mtransports', 'Mtransportcomments', 'Mcustomers', 'Mcustomeraddress', 'Morderproducts', 'Mprovinces', 'Mdistricts', 'Mcountries', 'Mwards', 'Mproducts', 'Mproductchilds'));
            $transport = $this->Mtransports->get($transportId, true, '', 'OrderId, CustomerId, CustomerAddressId, CODCost, ShipCost, Tracking, TransportStatusId, CancelReasonId, StoreId');
            if($transport){
                $customerAddress = $this->Mcustomeraddress->getInfo($transport['CustomerAddressId'], $transport['CustomerId']);
                if($customerAddress){
                    if($customerAddress['CountryId'] == 0 || $customerAddress['CountryId'] == 232) {
                        if ($customerAddress['ProvinceId'] > 0) $customerAddress['ProvinceName'] = $this->Mprovinces->getFieldValue(array('ProvinceId' => $customerAddress['ProvinceId']), 'ProvinceName');
                        else $customerAddress['ProvinceName'] = '';
                        if ($customerAddress['DistrictId'] > 0) $customerAddress['DistrictName'] = $this->Mdistricts->getFieldValue(array('DistrictId' => $customerAddress['DistrictId']), 'DistrictName');
                        else $customerAddress['DistrictName'] = '';
                        if ($customerAddress['WardId'] > 0) $customerAddress['WardName'] = $this->Mwards->getFieldValue(array('WardId' => $customerAddress['WardId']), 'WardName');
                        else $customerAddress['WardName'] = '';
                        $customerAddress['CountryName'] = 'Việt Nam';
                    }
                    else $customerAddress['CountryName'] = $this->Mcountries->getFieldValue(array('CountryId' => $customerAddress['CountryId']), 'CountryName');
                }
                $listProducts = array();
                $products = array();
                $productChilds = array();
                $listOrderProducts = $this->Morderproducts->getBy(array('OrderId' => $transport['OrderId']));
                foreach($listOrderProducts as $op){
                    if($op['ProductKindId'] == 3){
                        $listProductChilds = $this->Mproductchilds->getByProductId($op['ProductId']);
                        foreach ($listProductChilds as $pc){
                            if(!isset($products[$pc['ProductPartId']])) $products[$pc['ProductPartId']] = $this->Mproducts->get($pc['ProductPartId'], true, '', 'ProductName, ProductImage, ProductUnitId, Price');
                            $productName = $products[$pc['ProductPartId']]['ProductName'];
                            $productImage = $products[$pc['ProductPartId']]['ProductImage'];
                            $productChildName = '';
                            if($pc['ProductPartChildId'] > 0){
                                if(!isset($productChilds[$pc['ProductPartChildId']])) $productChilds[$pc['ProductPartChildId']] = $this->Mproductchilds->get($pc['ProductPartChildId'], true, '', 'ProductName, ProductImage, Price');
                                $productChildName = $productChilds[$pc['ProductPartChildId']]['ProductName'];
                                $productImage = $productChilds[$pc['ProductPartChildId']]['ProductImage'];
                            }
                            if(empty($productImage)) $productImage = NO_IMAGE;
                            $listProducts[] = array(
                                'ProductId' => $op['ProductId'],
                                'ProductName' => $productName,
                                'ProductChildId' => $op['ProductChildId'],
                                'ProductChildName' => $productChildName,
                                'ProductImage' => $productImage,
                                'Quantity' => $op['Quantity'] * $pc['Quantity'],
                                'Price' => $op['Price'],
                                'ProductKindId' => $op['ProductKindId'],
                            );
                        }
                    }
                    else{
                        if(!isset($products[$op['ProductId']])) $products[$op['ProductId']] = $this->Mproducts->get($op['ProductId'], true, '', 'ProductName, ProductImage, BarCode');
                        $productName = $products[$op['ProductId']]['ProductName'];
                        $productImage = $products[$op['ProductId']]['ProductImage'];
                        $productChildName = '';
                        if($op['ProductChildId'] > 0){
                            if(!isset($productChilds[$op['ProductChildId']])) $productChilds[$op['ProductChildId']] = $this->Mproductchilds->get($op['ProductChildId'], true, '', 'ProductName, ProductImage, BarCode');
                            $productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
                            $productImage = $productChilds[$op['ProductChildId']]['ProductImage'];
                        }
                        if(empty($productImage)) $productImage = NO_IMAGE;
                        $listProducts[] = array(
                            'ProductId' => $op['ProductId'],
                            'ProductName' => $productName,
                            'ProductChildId' => $op['ProductChildId'],
                            'ProductChildName' => $productChildName,
                            'ProductImage' => $productImage,
                            'Quantity' => $op['Quantity'],
                            'Price' => $op['Price'],
                            'ProductKindId' => $op['ProductKindId'],
                        );
                    }
                }
                $data = array(
                    'transport' => $transport,
                    'customerAddress' => $customerAddress,
                    'Products' => $listProducts,
                    'listTransportComments' => $this->Mtransportcomments->getList($transportId)
                );
                echo json_encode(array('code' => 1, 'data' => $data));
            }
            else echo json_encode(array('code' => -1, 'message' => "Không tìm thấy vận chuyển"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateFieldApi(){
        header('Content-Type: application/json');
        $barCode = trim($this->input->post('BarCode'));
        $transporterId = $this->input->post('TransporterId');
        $shipCost = $this->input->post('ShipCost');
        $userId = $this->input->post('UserId');
        if(!empty($barCode) && $transporterId > 0 && $shipCost >= 0){
            $this->load->model('Mtransports');
            $transport = $this->Mtransports->getBy(array('TransportCode' => $barCode), true, '', 'TransportId, TransportStatusId');
            if($transport && $transport['TransportStatusId'] == 2){
                $crDateTime = getCurentDateTime();
                $actionLog = array(
                    'ItemId' => $transport['TransportId'],
                    'ItemTypeId' => 9,
                    'ActionTypeId' => 2,
                    'Comment' => $this->Musers->getFieldValue(array('UserId' => $userId), 'FullName') . " cập nhật Trạng thái vận chuyển thành ".$this->Mconstants->transportStatus[3],
                    'CrUserId' => $userId,
                    'CrDateTime' => $crDateTime
                );
                $metaData = array('TransportStatusLog' => array(
                    'TransportId' => $transport['TransportId'],
                    'TransportStatusId' => 3,
                    'CrUserId' => $userId,
                    'CrDateTime' => $crDateTime
                ));
                $this->load->model('Mtransports');
                $flag = $this->Mtransports->updateField(array('TransportStatusId' => 3, 'TransporterId' => $transporterId, 'ShipCost' => $shipCost, 'UpdateUserId' => $userId, 'UpdateDateTime' => $crDateTime), $transport['TransportId'], $actionLog, $metaData);
                if ($flag) echo json_encode(array('code' => 1, 'message' => 'Cập nhật thông tin vận chuyển thành công'));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Đơn hàng không tồn tại"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Dữ liệu truyền lên không hợp lệ"));
    }
}