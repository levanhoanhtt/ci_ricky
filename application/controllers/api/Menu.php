<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Controller {
    public function create() {
        $user = $this->checkUserLogin(true);
        $menuName = $this->input->post('MenuName');
        $menuId = $this->input->post('MenuId');
        $this->load->model('Mmenus');
        if ($this->Mmenus->checkExist($menuId, $menuName) > 0) {
            echo json_encode(array('code' => -1, 'message' => "Tên menu đã tồn tại!"));
        }
        else {
            $crDateTime = getCurentDateTime();
            if ($menuId > 0) {
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = $crDateTime;
            }
            else {
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = $crDateTime;
            }
            $postData['Name'] = $menuName;
            $postData['SlugName'] = makeSlug($menuName);
            $menuId = $this->Mmenus->update($postData, $menuId);
            if ($menuId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật menu thành công", 'data' => $menuId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
    }
}