<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contributor extends MY_Controller
{
    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Cổ đông',
            array('scriptFooter' => array('js' => 'js/contributor.js'))
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'contributor')) {
            $this->load->model('Mcontributors');
            $data['listContributors'] = $this->Mcontributors->getBy(array('ItemStatusId >' => 0));
            $this->load->view('contributor/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm Cổ đông',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/contributor_update.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'contributor')) {
            $this->loadModel(array('Mprovinces', 'Mdistricts', 'Mwards'));
            $data['listProvinces'] = $this->Mprovinces->getList();
            $this->load->view('contributor/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($contributorId){
        if($contributorId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Cập nhật cổ đông',
                array(
                    'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                    'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/contributor_update.js'))
                )
            );
            $this->loadModel(array('Mcontributors', 'Mprovinces', 'Mdistricts', 'Mwards', 'Mproducttypes', 'Mcontributorproducttypes'));
            $contributor = $this->Mcontributors->get($contributorId);
            if ($contributor) {
                if($this->Mactions->checkAccess($data['listActions'], 'contributor')) {
                    $data['contributorId'] = $contributorId;
                    $data['contributor'] = $contributor;
                    $data['listProvinces'] = $this->Mprovinces->getList();
                    $data['listContributorProductTypes'] = $this->Mcontributorproducttypes->getByContributorId($contributorId);
                    $data['listProductTypes'] = $this->Mproducttypes->getList();
                    $this->load->view('contributor/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else {
                $data['contributorId'] = 0;
                $data['txtError'] = "Không tìm thấy Cổ đông";
                $this->load->view('contributor/edit', $data);
            }
        }
        else redirect('contributor');
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ContributorName', 'ContributorPhone', 'BirthDay', 'ItemStatusId', 'Address', 'ProvinceId', 'DistrictId', 'WardId'));
        if(!empty($postData['BirthDay'])) $postData['BirthDay'] = ddMMyyyyToDate($postData['BirthDay']);
        $contributorId = $this->input->post('ContributorId');
        if($contributorId == 0){
            $postData['CrUserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
        }
        else{
            $postData['UpdateUserId'] = $user['UserId'];
            $postData['UpdateDateTime'] = getCurentDateTime();
        }
        $this->load->model('Mcontributors');
        $contributorId = $this->Mcontributors->update($postData, $contributorId);
        if ($contributorId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật cổ đông thành công", 'data' => $contributorId));
        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $contributorId = $this->input->post('ContributorId');
        $itemStatusId = $this->input->post('ItemStatusId');
        if($contributorId  > 0 && $itemStatusId >= 0 && $itemStatusId <= count($this->Mconstants->itemStatus)){
            $this->load->model('Mcontributors');
            $flag = $this->Mcontributors->changeStatus($itemStatusId, $contributorId, 'ItemStatusId', $user['UserId']);
            if($flag){
                $statusName = "";
                if($itemStatusId == 0) $txtSuccess = "Xóa cổ phần thành công";
                else{
                    $txtSuccess = "Đổi trạng thái thành công";
                    $statusName = '<span class="' . $this->Mconstants->labelCss[$itemStatusId] . '">' . $this->Mconstants->itemStatus[$itemStatusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('ItemStatusName' => $statusName)));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateContribute(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ContributorId', 'ProductTypeId', 'PaidVN', 'PaidDateTime'));
        $postData['StatusId'] = STATUS_ACTIVED;
        $postData['PaidVN'] = replacePrice($postData['PaidVN']);
        $paidDateTime = $postData['PaidDateTime'];
        if(!empty($paidDateTime)) $postData['PaidDateTime'] = ddMMyyyyToDate($paidDateTime);
        $contributorProductTypeId = $this->input->post('ContributorProductTypeId');
        if($contributorProductTypeId == 0){
            $postData['CrUserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
        }
        else{
            $postData['UpdateUserId'] = $user['UserId'];
            $postData['UpdateDateTime'] = getCurentDateTime();
        }
        $this->loadModel(array('Mcontributorproducttypes', 'Mproducttypes'));
        $this->load->model('Mcontributorproducttypes');
        $flag = $this->Mcontributorproducttypes->save($postData, $contributorProductTypeId, array('UpdateUserId', 'UpdateDateTime'));
        if ($flag > 0){
            $postData['ContributorProductTypeId'] = $flag;
            $postData['IsAdd'] = ($contributorProductTypeId > 0) ? 0 : 1;
            $postData['PaidDateTime'] = $paidDateTime;
            $postData['ProductTypeName'] = $this->Mproducttypes->getFieldValue(array('ProductTypeId' => $postData['ProductTypeId']), 'ProductTypeName');
            echo json_encode(array('code' => 1, 'message' => "Cập nhật đóng góp thành công", 'data' => $postData));
        }
        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function deleteContribute(){
        $user = $this->checkUserLogin(true);
        $contributorProductTypeId = $this->input->post('ContributorProductTypeId');
        if($contributorProductTypeId > 0){
            $this->load->model('Mcontributorproducttypes');
            $flag = $this->Mcontributorproducttypes->changeStatus(0, $contributorProductTypeId, '', $user['UserId']);
            if ($flag) echo json_encode(array('code' => 1, 'message' => "Xóa đóng góp thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}