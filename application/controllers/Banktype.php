<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banktype extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách loại ngân hàng',
            array('scriptFooter' => array('js' => 'js/bank_type.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'banktype')) {
            $this->load->model('Mbanktypes');
            $data['listBankTypes'] = $this->Mbanktypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/bank_type', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('BankTypeName'));
        if(!empty($postData['BankTypeName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $bankTypeId = $this->input->post('BankTypeId');
            $this->load->model('Mbanktypes');
            $flag = $this->Mbanktypes->save($postData, $bankTypeId);
            if ($flag > 0) {
                $postData['BankTypeId'] = $flag;
                $postData['IsAdd'] = ($bankTypeId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật loại ngân hàng thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $bankTypeId = $this->input->post('BankTypeId');
        if($bankTypeId > 0){
            $this->load->model('Mbanktypes');
            $flag = $this->Mbanktypes->changeStatus(0, $bankTypeId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa loại ngân hàng thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}
