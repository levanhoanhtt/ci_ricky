<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Formatdate extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
            '',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css'),
				'scriptFooter' => array('js' => array('vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'js/formatdate.js'))
            )
		);
		if($this->Mactions->checkAccess($data['listActions'], 'group')) {
			$this->load->view('formatdate/list', $data);
		}
		else $this->load->view('user/permission', $data);
    }

    public function showDate(){
        $startDate = $this->input->post('StartDate');
        $startDate = ddMMyyyyToDate($startDate, 'd/m/Y H:i', 'Y-m-d H:i');
        $endDate = date('Y-m-d H:i');

        // $startDate = '2020-03-10 00:00:00';
        // $endDate = '2020-08-03 00:00:00';

        $diff = abs(strtotime(date('Y-m-d',  strtotime($startDate))) - strtotime(date('Y-m-d',  strtotime($endDate))));

        //Tính ra tông số ngày của 2 khoản thời gian
        $interval = date_diff(date_create(date('Y-m-d', strtotime($startDate))), date_create(date('Y-m-d', strtotime($endDate))))->format('%a %d %R %y');
    //    var_dump($interval);
        $parts = explode(' ', $interval);

        // Lấy ra tổng số ngày
        $totalDays = intval($parts[0]);
        $beforeOrAfter = $parts[2];

        // Tính ra ngày tháng năm
        
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
      
        $hours = strtotime(date('H:i', strtotime($startDate))); // Giờ
        $showTime = '';
        //Nếu >= 365 lấy ra năm
        if ($totalDays >= 365) {
            //Nếu  Delta X >= 15 >> M = M + 1 Lên tháng
            if ($days >= 15) {
                $months += 1;
            }

            /**
             * + là tương lai
             * - là quá khứ
             */

            if ($beforeOrAfter == '-' && intval($parts[1]) == 1) {
                $showTime = "Ngày mai, ";
            } else if ($beforeOrAfter == '+' && intval($parts[1]) == 1) {
                $showTime = "Hôm qua, ";
            } else if ($beforeOrAfter == '-' && intval($parts[1]) == 2) {
                $showTime = "Hôm kia, ";
            } else if ($beforeOrAfter == '+' && intval($parts[1]) == 2) {
                $showTime = "Ngày kia, ";
            } else if (($beforeOrAfter == '+' || $beforeOrAfter == '-') && intval($parts[1]) == 0) {
                $showTime = "Hôm nay, ";
            }

            if (in_array($months, [1,2])) {
                $showTime .= 'Hơn '.$years. ' năm';
            } else if (in_array($months, [3,4,8,9])) {
                $showTime .= $years.' năm '.$months. ' tháng';
            } else if ($months == 5) {
                $showTime .= 'Gần '.$years.' năm rưỡi';
            } else if ($months == 6) { 
                $showTime .= $years.' năm rưỡi';
            } else if ($months == 7) {
                $showTime .= 'Hơn '.$years.' năm rưỡi';
            } else if (in_array($months, [10,11])) {
                $showTime .= 'Gần '.($years + 1). ' năm';
            } else if ($months == 0) {
                $showTime .= $years. ' năm';
            } else if ($months == 12){
                $showTime .= ($years + 1). ' năm';
            }
            
            $showTime .= $this->dateBeforeOrAgain($beforeOrAfter);
        } else if ($totalDays >= 30) {
            if ($days >=1 && $days <= 10) {
                $showTime = 'Hơn '.$months. ' tháng';
            } else if (in_array($days, [11,12])) {
                $showTime = 'Gần '.$months. ' tháng rưỡi';
            } else if ($days >= 13 && $days <= 17) {
                $showTime = $months. ' tháng rưỡi';
            } else if (in_array($days, [18,19])) {
                $showTime = 'Hơn '.$months. ' tháng rưỡi';
            } else if ($days >= 20 && $days <= 29) {
                $showTime = 'Gần '.($months + 1). ' tháng';
            }
            $showTime .= $this->dateBeforeOrAgain($beforeOrAfter);

        } else if ($totalDays >= 1) {
            //Nếu >= 1 lấy ra ngày
            if ($days == 1) {
                $showTime = $this->dateDiff($hours, $beforeOrAfter, $days);
            } else if ($days == 2) {
                $showTime = $this->dateDiff($hours, $beforeOrAfter, $days);
            } else if ($days >= 3 && $days <= 6) {
                $showTime = $days.' ngày';
            } else if ($days == 7) {
                $showTime = '1 tuần';
            } else if ($days >= 8 && $days <= 11) {
                $showTime = $days.' ngày';
            } else if (in_array($days, [12,13])) {
                $showTime = 'Gần 2 tuần';
            } else if ($days == 14) {
                $showTime = '2 tuần';
            } else if (in_array($days, [15,16,17])) {
                $showTime = 'Hơn nữa tháng';
            } else if (in_array($days, [18,19,20])) {
                $showTime = 'Gần 3 tuần';
            } else if ($days == 21) {
                $showTime = '3 tuần';
            } else if (in_array($days, [22,23,24])) {
                $showTime = 'Hơn 3 tuần';
            } else if ($days >= 25 && $days <= 29){
                $showTime = 'Gần 1 tháng';
            }

            if (!in_array($days, [0,1,2])){
                $showTime .= $this->dateBeforeOrAgain($beforeOrAfter);
            }
 
        } else if ($totalDays == 0){
            $showTime .= $this->dateDiff($hours, $beforeOrAfter, $days);
        }
        
        echo json_encode(array('code' => 1, 'data' => $showTime));
    
    }

    public function dateDiff($time = 0, $beforeOrAfter = '', $days = 0){
        $dayTime = '';
        if ($time > strtotime("00:00") && $time < strtotime("11:00")) {
            $dayTime = 'Sáng ';
        } else if ($time >= strtotime("11:00") && $time < strtotime("13:30")) {
            $dayTime = 'Trưa ';
        } else if ($time >= strtotime("13:30") && $time < strtotime("17:30")) {
            $dayTime = 'Chiều ';
        } else if ($time >= strtotime("17:30") && $time < strtotime("24:00")) {
            $dayTime = 'Tối ';
        }

        /**
         * + là tương lai
         * - là quá khứ
         */

        if ($beforeOrAfter == '-' && $days == 1) {
            $dayTime .= "ngày mai";
        } else if ($beforeOrAfter == '+' && $days == 1) {
            $dayTime .= "hôm qua";
        } else if ($beforeOrAfter == '-' && $days == 2) {
            $dayTime .= "hôm kia";
        } else if ($beforeOrAfter == '+' && $days == 2) {
            $dayTime .= "ngày kia";
        } else if (($beforeOrAfter == '+' || $beforeOrAfter == '-') && $days == 0) {
            $dayTime .= "hôm nay";
        }


        return $dayTime;
    }

    public function dateBeforeOrAgain($beforeOrAfter = ''){
        $text = '';
        if ($beforeOrAfter == '-') {
            $text = ' nữa';
        } else if ($beforeOrAfter == '+') {
            $text = ' trước';
        }
        return $text;
    }
}