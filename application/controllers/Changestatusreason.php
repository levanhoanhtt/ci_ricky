<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Changestatusreason extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách lý do thay đổi trạng thái',
            array('scriptFooter' => array('js' => 'js/change_status_reason.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'changestatusreason')) {
            $this->load->model('Mchangestatusreasons');
            $data['listChangeStatusReasons'] = $this->Mchangestatusreasons->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/change_status_reason', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ChangeStatusReasonName'));
        if(!empty($postData['ChangeStatusReasonName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $changeStatusReasonId = $this->input->post('ChangeStatusReasonId');
            $this->load->model('Mchangestatusreasons');
            $flag = $this->Mchangestatusreasons->save($postData, $changeStatusReasonId);
            if ($flag > 0) {
                $postData['ChangeStatusReasonId'] = $flag;
                $postData['IsAdd'] = ($changeStatusReasonId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật lý do thay đổi trạng thái thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $changeStatusReasonId = $this->input->post('ChangeStatusReasonId');
        if($changeStatusReasonId > 0){
            $this->load->model('Mchangestatusreasons');
            $flag = $this->Mchangestatusreasons->changeStatus(0, $changeStatusReasonId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa lý do thay đổi trạng thái thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}
