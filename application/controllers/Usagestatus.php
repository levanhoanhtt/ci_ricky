<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usagestatus extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Hình thức/ tình trạng sử dụng',
            array('scriptFooter' => array('js' => 'js/usage_status.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'usagestatus')) {
            $this->load->model('Musagestatus');
            $data['listUsageStatus'] = $this->Musagestatus->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/usage_status', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('UsageStatusName', 'UsageStatusTypeId', 'ParentUsageStatusId'));
        if(!empty($postData['UsageStatusName']) && $postData['UsageStatusTypeId'] > 0) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $usageStatusId = $this->input->post('UsageStatusId');
            $this->load->model('Musagestatus');
            $flag = $this->Musagestatus->save($postData, $usageStatusId);
            if ($flag > 0) {
                $postData['UsageStatusId'] = $flag;
                $postData['UsageStatusTypeName'] = $this->Mconstants->usageStatusTypes[$postData['UsageStatusTypeId']];
                $postData['IsAdd'] = ($usageStatusId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật hình thức/ tình trạng sử dụng thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $usageStatusId = $this->input->post('UsageStatusId');
        if($usageStatusId > 0){
            $this->load->model('Musagestatus');
            $flag = $this->Musagestatus->changeStatus(0, $usageStatusId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa hình thức/ tình trạng sử dụng sản phẩm thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}