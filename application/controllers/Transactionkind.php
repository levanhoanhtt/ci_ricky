<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactionkind extends MY_Controller {

    public function index($transactionKindTypeId = 0){
        $user = $this->checkUserLogin();
        if(!in_array($transactionKindTypeId, array(1, 2, 3))) $transactionKindTypeId = 1;
        $data = $this->commonData($user,
            'Loại thu chi '.($transactionKindTypeId == 2 ? 'gia đình' : 'kinh doanh'),
            array('scriptFooter' => array('js' => 'js/transaction_kind.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'transactionkind/'.$transactionKindTypeId)) {
            $data['transactionKindTypeId'] = $transactionKindTypeId;
            $this->load->model('Mtransactionkinds');
            $data['listTransactionKinds'] = $this->Mtransactionkinds->getList(0, $transactionKindTypeId);
            $this->load->view('setting/transaction_kind', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('TransactionKindName', 'TransactionTypeId', 'ParentTransactionKindId', 'TransactionKindTypeId'));
        if(!empty($postData['TransactionKindName']) && $postData['TransactionTypeId'] > 0 && $postData['TransactionKindTypeId'] > 0) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $transactionKindId = $this->input->post('TransactionKindId');
            $this->load->model(array('Mtransactionkinds'));
            $flag = $this->Mtransactionkinds->save($postData, $transactionKindId);
            if ($flag > 0) {
                $postData['TransactionKindId'] = $flag;
                $postData['IsAdd'] = ($transactionKindId > 0) ? 0 : 1;
                $postData['TransportTypeName'] = '<span class="'.$this->Mconstants->labelCss[$postData['TransactionTypeId']].'">'.$this->Mconstants->transactionTypes[$postData['TransactionTypeId']].'</span>';
                echo json_encode(array('code' => 1, 'message' => "Cập nhật loại thu chi thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $transactionKindId = $this->input->post('TransactionKindId');
        if($transactionKindId > 0){
            $this->load->model('Mtransactionkinds');
            $flag = $this->Mtransactionkinds->changeStatus(0, $transactionKindId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa loại thu chi thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}
