<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guaranteeimagegroup extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách nhóm ảnh bảo hành',
            array('scriptFooter' => array('js' => 'js/guarantee_image_group.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'guaranteeimagegroup')) {
            $this->load->model('Mguaranteeimagegroups');
            $data['listGuaranteeImageGroups'] = $this->Mguaranteeimagegroups->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/guarantee_image_group', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('GuaranteeImageGroupName'));
        if(!empty($postData['GuaranteeImageGroupName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $guaranteeImageGroupId = $this->input->post('GuaranteeImageGroupId');
            $this->load->model('Mguaranteeimagegroups');
            $flag = $this->Mguaranteeimagegroups->save($postData, $guaranteeImageGroupId);
            if ($flag > 0) {
                $postData['GuaranteeImageGroupId'] = $flag;
                $postData['IsAdd'] = ($guaranteeImageGroupId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật nhóm ảnh bảo hành thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $guaranteeImageGroupId = $this->input->post('GuaranteeImageGroupId');
        if($guaranteeImageGroupId > 0){
            $this->load->model('Mguaranteeimagegroups');
            $flag = $this->Mguaranteeimagegroups->changeStatus(0, $guaranteeImageGroupId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa nhóm ảnh bảo hành thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}
