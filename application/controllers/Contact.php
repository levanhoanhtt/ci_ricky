<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách liên hệ',
			array('scriptFooter' => array('js' => 'js/contact.js'))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'contact')) {
            $this->loadModel(array('Mcontacts', 'Mcomments'));
			$postData = $this->arrayFromPost(array('StatusId', 'ContactId'));
			$rowCount = $this->Mcomments->getCount($postData);
			$data['listContacts'] = array();
			if($rowCount > 0){
				$perPage = 20;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listComments'] = $this->Mcomments->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('contact/list', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function edit($commentId){
		if($commentId > 0) {
			$user = $this->checkUserLogin();
			// $data = $this->commonData($user,
			// 	'Cập nhật Bình luận - Đánh giá',
			// 	array('scriptFooter' => array('js' => 'js/comment_update.js'))
			// );
			if ($this->Mactions->checkAccess($data['listActions'], 'cms/comment/edit')) {
				$this->load->model('Mcomments');
				$comment = $this->Mcomments->get($commentId);
				if ($comment) {
					$data['commentId'] = $commentId;
					$data['comment'] = $comment;
					$data['listCustomers'] = $this->Musers->getListByRole(4);
					$this->load->view('comment/edit', $data);
				}
				else {
					$data['commentId'] = 0;
					$data['txtError'] = "Không tìm thấy Bình luận - Đánh giá";
					$this->load->view('comment/edit', $data);
				}
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('comment');
	}

	public function update(){
		$postData = $this->arrayFromPost(array('CustomerId', 'GenderId', 'CustomerName', 'CustomerEmail', 'CustomerPhone', 'ContactTitle', 'ContactContent'));
		if(!empty($postData['ContactContent'])){
			$contactId = $this->input->post('ContactId');
			if($contactId == 0) {
				$postData['CrDateTime'] = getCurentDateTime();
			}
			$this->load->model('Mcontacts');
			$flag = $this->Mcontacts->save($postData, $contactId);
			if ($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật liên hệ thành công", 'data' => $flag));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
