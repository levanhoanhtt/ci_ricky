<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Speedsms extends MY_Controller {

    public $statusSMS = array(
        'statusSms' => array(
            0   => 'Trạng thái đang chờ gửi',
            -1  => 'Trạng thái đang gửi',
            1   => 'Gửi thành công',
            2   => 'Gửi lỗi',
        ),
        'lableCss' => array(
            0   => 'label label-warning',
            -1  => 'label label-default',
            1   => 'label label-success',
            2   => 'label label-danger',
        ),
    );

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Lịch sử gửi SMS',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_item.js', 'js/speed_sms.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'speedsms')) {
            $this->loadModel(array('Mfilters', 'Mcustomergroups', 'Msmscampaigns','Mtags'));
            $itemTypeId = 35;
            $data['listSmsCampaigns'] = $this->Msmscampaigns->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listFilters'] = $this->Mfilters->getList($itemTypeId);
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => $itemTypeId));
            $this->load->view('speedsms/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Gửi SMS',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/send_sms.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'speedsms')) {
            $this->loadModel(array('Mcustomergroups','Msmscampaigns','Msentencegroups'));
            $data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listSmscampaigns'] = $this->Msmscampaigns->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listSentenceGroups'] = $this->Msentencegroups->get();
            $this->load->view('speedsms/send_sms', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function sendSms(){
        $user = $this->checkUserLogin(true);
        $phone = json_decode(trim($this->input->post('PhoneNumbers')), true);
        $customerGroupId = $this->input->post('CustomerGroupId');
        $sMSCampaignId = $this->input->post('SMSCampaignId');
        $sMSCampaignDesc = trim($this->input->post('SMSCampaignDesc'));
        $sMSCampaignName =  trim($this->input->post('SMSCampaignName'));
        $message = trim($this->input->post('Message'));
        if((!empty($phone) || $customerGroupId > 0) && $message !=""){
            $this->loadModel(array('Mspeedsms', 'Msmscampaigns', 'Mcustomers'));
            $customer = $this->Mcustomers->getListCustomerGroup($customerGroupId);
            // phone thuộc kiểu array
            $arrPhone = array();
            if($phone != "" && count($customer) > 0){
                foreach ($customer as $key => $c) {
                    $arrPhone[] = $c['PhoneNumber'];
                }
                array_push($arrPhone, $phone);
            }
            else if($phone != "" && count($customer) == 0) $arrPhone = $phone;
            else if($phone == "" && count($customer) > 0){
                foreach ($customer as $key => $c) {
                    $arrPhone[] = $c['PhoneNumber'];
                }
            }
            if($sMSCampaignId == 0){
                $postData = array(
                    'SMSCampaignName' => $sMSCampaignName,
                    'SMSCampaignDesc' => $sMSCampaignDesc,
                    'CustomerGroupId' => $customerGroupId,
                    'StatusId' => STATUS_ACTIVED,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => getCurentDateTime(),
                );
                $this->Msmscampaigns->save($postData, $sMSCampaignId);
            }
            $flag = $this->Mspeedsms->send($arrPhone, $message, $user['UserId']);
            if($flag > 0) echo json_encode(array('code' => 1, 'message' => "Đã gửi yêu cầu gửi tin nhắn"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function showStatusSMS(){
        $user = $this->checkUserLogin(true);
        $sMSLogId = $this->input->post('SMSLogId');
        $tranId = $this->input->post('TranId');
        if($sMSLogId > 0 && $tranId > 0){
            $this->loadModel(array('Msmslogs', 'Msendsmsstatus'));
            $sendSmsStatus = $this->Msendsmsstatus->getBy(array('SMSLogId' => $sMSLogId));
            if(count($sendSmsStatus) > 0) echo json_encode(array('code' => 1,'data' => $sendSmsStatus[0], 'status' => $this->statusSMS));
            else{
                $flag = $this->Msendsmsstatus->send($sMSLogId, $tranId, $user['UserId']);
                if(count($flag) > 0) echo json_encode(array('code' => 1, 'message' => "Lưu thành công", 'data'=> $flag, 'status' => $this->statusSMS));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function searchByFilter(){
        $user = $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->loadModel(array('Msmslogs','Mcustomers','Mcustomergroups'));
        $data1 = $this->Msmslogs->searchByFilter($searchText, $itemFilters, $limit, $page);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }

    //web hook
    public function delivered(){
        //KFZS5CbyNhhaDuYt
        $user = $this->checkUserLogin(true);
        $sMSLogId = $this->input->post('SMSLogId');
        $tranId = $this->input->post('TranId');
        if($sMSLogId > 0 && $tranId > 0){
            $this->loadModel(array('Mdeliverysmsstatus'));
            $deliverysmsstatus = $this->Mdeliverysmsstatus->getBy(array('SMSLogId' =>$sMSLogId));
            if(count($deliverysmsstatus) > 0) echo json_encode(array('code' => 1,'data' => $deliverysmsstatus));
            else{
                /**
                    Giả sử có mảng trả về có dang 
                    {"type": "report", "tranId": 1234, "phone": "0912345678", "status": 0} 
                    {"type": "report", "tranId": 1234, "phone": "0912345678", "status": 69}
                    status có dạng:
                    status = 0: success
                    0 < status < 64 temporary fail 
                    status >= 64: failed
                */
                $delivereds = array(
                    array('type' => 'report', 'tranId' => 1234, 'phone' => '01685524224', 'status' => 0),
                    array('type' => 'report', 'tranId' => 1234, 'phone' => '01685524225', 'status' => 1),
                    array('type' => 'report', 'tranId' => 1234, 'phone' => '01685524226', 'status' => 69),
                );
                $postData = array();
                foreach ($delivereds as $delivered) {
                    $postData[] = array(
                        'PhoneNumber' => $delivered['phone'],
                        'StatusId' => $delivered['status'],
                        'SMSLogId' => $sMSLogId,
                        'TranId' => $tranId,
                        'CrDateTime' => getCurentDateTime(),
                    );
                }
                $flag = $this->Mdeliverysmsstatus->update($postData);
                if($flag > 0) echo json_encode(array('code' => 1, 'message' => "Lưu thành công", 'data'=> $postData));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }

        }else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function incoming(){
        //kt7D497chEnUuTSh
        $user = $this->checkUserLogin(true);
        /*
        Giả sử api trả về co dạng như:
        {"type": "sms", "phone": "phone number", "content": "sms content"}
        */
        $incoming = array('type' => 'sms', 'phone' => '01685524224', 'content' => 'sms content');
        if($incoming){
            $this->loadModel(array('Mincomingsms'));
            $postData['PhoneNumber'] = $incoming['phone'];
            $postData['SMSContent'] = $incoming['content'];
            $postData['CrDateTime'] = getCurentDateTime();
            $flag = $this->Mincomingsms->save($postData, 0);
            if($flag > 0) echo json_encode(array('code' => 1, 'message' => "Lưu thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function listIncoming(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách nhận tin nhắn phản hồi',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/daterangepicker/daterangepicker.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js', 'js/incoming.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'speedsms/listIncoming')) {
            $this->loadModel(array('Mincomingsms'));
            $postData = $this->arrayFromPost(array('SearchText'));
            $dateRangePicker = trim($this->input->post('DateRangePicker'));
            if(!empty($dateRangePicker)){
                $parts = explode('-', $dateRangePicker);
                if(count($parts) == 2){
                    $postData['BeginDate'] = ddMMyyyyToDate($parts[0]);
                    $postData['EndDate'] = ddMMyyyyToDate($parts[1], 'd/m/Y', 'Y-m-d 23:59:59');
                }
            }
            $rowCount = $this->Mincomingsms->getCount($postData);
            $data['listIncomingsms'] = array();
            if($rowCount > 0){
                $perPage = DEFAULT_LIMIT;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $listIncomingsms = $this->Mincomingsms->search($postData, $perPage, $page);
                $now = new DateTime(date('Y-m-d'));
                for ($i = 0; $i < count($listIncomingsms); $i++) {
                    $dayDiff = getDayDiff($listIncomingsms[$i]['CrDateTime'], $now);
                    $listIncomingsms[$i]['CrDateTime'] = ddMMyyyy($listIncomingsms[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
                    $listIncomingsms[$i]['DayDiff'] = $dayDiff;
                }
                $data['listIncomingsms'] = $listIncomingsms;
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('speedsms/incomingsms', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function getContentSentences(){
        $this->checkUserLogin();
        $postData = $this->arrayFromPost(array('SentenceGroupId', 'SearchText'));
        $this->load->model('Msentences');
        $listSentences = $this->Msentences->search($postData);
        echo json_encode(array('code' => 1,  'data' => $listSentences));
    }
}