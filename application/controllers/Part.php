<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Part extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách Phòng ban',
			array('scriptFooter' => array('js' => 'js/part.js'))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'part')) {
			$this->load->model('Mparts');
			$data['listParts'] = $this->Mparts->getList();
			$this->load->view('setting/part', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('PartCode','PartName', 'ParentPartId'));
		if(!empty($postData['PartCode']) && !empty($postData['PartName'])) {
			$this->load->model('Mparts');
			$partId = $this->input->post('PartId');
			if($this->Mparts->checkExist($partId, $postData['PartCode'])) echo json_encode(array('code' => -1, 'message' => "Phòng ban đã tồn tại trong hệ thống"));
			else{
				$postData['StatusId'] = STATUS_ACTIVED;
				if($partId > 0){
					$postData['UpdateUserId'] = $user['UserId'];
					$postData['UpdateDateTime'] = getCurentDateTime();
				}
				else{
					$postData['CrUserId'] = $user['UserId'];
					$postData['CrDateTime'] = getCurentDateTime();
				}
				$flag = $this->Mparts->save($postData, $partId);
				if ($flag > 0) {
					$postData['PartId'] = $flag;
					$postData['IsAdd'] = ($partId > 0) ? 0 : 1;
					echo json_encode(array('code' => 1, 'message' => "Cập nhật Phòng ban thành công", 'data' => $postData));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$user = $this->checkUserLogin(true);
		$partId = $this->input->post('PartId');
		if($partId > 0){
			$this->load->model('Mparts');
			$flag = $this->Mparts->changeStatus(0, $partId, '', $user['UserId']);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa phòng ban thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
