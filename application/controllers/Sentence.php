<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sentence extends MY_Controller {

	public function index($librarySentenceId = 0){
        if($librarySentenceId > 0){
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Danh sách mẫu câu của thư viện ',
                array('scriptFooter' => array('js' => array('ckfinder/ckfinder.js', 'js/sentence.js')))
            );
            if($this->Mactions->checkAccess($data['listActions'], 'librarysentence')) {
                $this->loadModel(array('Mlibrarysentences', 'Msentencegroups', 'Msentences'));
                $libraryName = $this->Mlibrarysentences->getFieldValue(array('LibrarySentenceId' => $librarySentenceId), 'LibraryName');
                if(!empty($libraryName)){
                    $data['librarySentenceId'] = $librarySentenceId;
                    $data['title'] .= $libraryName;
                    $data['listSentenceGroups'] = $this->Msentencegroups->get();
                    $postData = $this->arrayFromPost(array('SentenceGroupId', 'SearchText'));
                    $postData['LibrarySentenceId'] = $librarySentenceId;
                    $rowCount = $this->Msentences->getCount($postData);
                    $data['listSentences'] = array();
                    if($rowCount > 0){
                        $perPage = 20;
                        $pageCount = ceil($rowCount / $perPage);
                        $page = $this->input->post('PageId');
                        if(!is_numeric($page) || $page < 1) $page = 1;
                        $data['listSentences'] = $this->Msentences->search($postData, $perPage, $page);
                        $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
                    }
                }
                else{
                    $data['librarySentenceId'] = 0;
                    $data['txtError'] = "Không tìm thấy Thư viện";
                }
                $this->load->view('sentence/list', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('librarysentence');
    }

    public function delete(){
    	$this->checkUserLogin(true);
    	$sentenceId = $this->input->post('SentenceId');
    	if($sentenceId > 0){
    		$this->load->model(array('Msentences'));
    		$flag = $this->Msentences->delete($sentenceId);
    		if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa mẫu câu thành công"));
    		else  echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    	}
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function update(){
    	$this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('SentenceTitle', 'SentenceContent', 'SentenceGroupId', 'LibrarySentenceId', 'FileTypeId'));
        if(!empty($postData['SentenceTitle']) && !empty($postData['SentenceContent']) && $postData['LibrarySentenceId'] > 0 && $postData['FileTypeId'] > 0){
            $sentenceId = $this->input->post('SentenceId');
            $this->load->model(array('Msentences','Msentencegroups'));
            $flag= $this->Msentences->checkExist($postData['SentenceTitle'], $postData['LibrarySentenceId'],$sentenceId);
            if($flag){
                if($postData['FileTypeId'] == 1) $postData['SentenceContent'] = replaceFileUrl($postData['SentenceContent']);
            	$flag = $this->Msentences->save($postData, $sentenceId);
	            if ($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật mẫu câu thành công"));
	            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Mẫu câu này đã tồn tại"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}