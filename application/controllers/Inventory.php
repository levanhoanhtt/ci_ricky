<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Cập nhật tồn kho',
            array('scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','js/search_item.js', 'js/inventory.js')))
        );
        $listActions = $data['listActions'];
        if($this->Mactions->checkAccess($listActions, 'inventory')){
            $data['canActive'] = $this->Mactions->checkAccess($listActions, 'inventory/active');
            $this->load->model('Mstores');
             $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $this->load->view('inventory/set', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function active(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Duyệt cập nhật tồn kho',
            array('scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','js/search_item.js', 'js/inventory_active.js')))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'inventory/active')) {
            $this->loadModel(array('Mproducts', 'Mproductchilds', 'Mstores', 'Minventories'));
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $storeId = $this->input->post('StoreId');
            if(empty($storeId) || $storeId < 0) $storeId = 0;
            $data['storeId'] = $storeId;
            $data['listInventoríes'] = $this->Minventories->getListPending($storeId);
            $this->load->view('inventory/active', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function log(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Lịch sử thay đổi tồn kho',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/sortable/jquery-ui.js', 'vendor/plugins/sortable/Sortable.min.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/search_item.js', 'js/inventory_log.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'inventory')) {
            $this->loadModel(array('Mproductchilds', 'Mproducttypes', 'Msuppliers', 'Mmanufacturers', 'Mfilters','Mcategories', 'Mstores'));
            $data['listProductTypes'] = $this->Mproducttypes->getList();
            $data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listManufacturers'] = $this->Mmanufacturers->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $data['listFilters'] = $this->Mfilters->getList(15);
            $this->load->view('inventory/log', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ProductId', 'ProductChildId', 'OldQuantity', 'Quantity', 'InventoryTypeId', 'StoreId', 'StatusId', 'IsManual', 'Comment'));
        $postData['Quantity'] = replacePrice($postData['Quantity']);
        if($postData['ProductId'] > 0 && $postData['Quantity'] > 0 && $postData['Quantity'] > 0 && $postData['InventoryTypeId'] > 0 && $postData['StoreId'] > 0 && $postData['StatusId'] > 0 && $postData['IsManual'] > 0) {
            $inventoryId = $this->input->post('InventoryId');
            if ($inventoryId > 0) {
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = getCurentDateTime();
            }
            else {
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = getCurentDateTime();
            }
            $this->loadModel(array('Minventories', 'Mproductquantity'));
            $inventoryId = $this->Minventories->update($postData, $inventoryId);
            if ($inventoryId > 0) {
                $message = 'Cập nhật tồn kho thành công, yêu cầu đã được chuyển đến quản lý';
                $quantity = 0;
                if ($postData['StatusId'] == STATUS_ACTIVED) {
                    $message = 'Cập nhật tồn kho thành công';
                    $quantity = $this->Mproductquantity->getQuantity($postData['ProductId'], $postData['ProductChildId'], $postData['StoreId']);
                }
                echo json_encode(array('code' => 1, 'message' => $message, 'data' => $quantity));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateStatus(){
        $user = $this->checkUserLogin(true);
        $statusId = $this->input->post('StatusId');
        $inventoryIds = json_decode(trim($this->input->post('InventoryIds')), true);
        if($statusId > 0 && !empty($inventoryIds)){
            $this->loadModel(array('Minventories', 'Mproductquantity'));
            $updateDateTime = getCurentDateTime();
            foreach($inventoryIds as $inventoryId){
                $inventory = $this->Minventories->get($inventoryId);
                if($inventory){
                    unset($inventory['InventoryId']);
                    $inventory['StatusId'] = $statusId;
                    $inventory['UpdateUserId'] = $user['UserId'];
                    $inventory['UpdateDateTime'] = $updateDateTime;
                    $this->Minventories->update($inventory, $inventoryId);
                }
            }
            echo json_encode(array('code' => 1, 'message' => 'Cập nhật tồn kho thành công'));
        }
        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function searchByFilter(){
        $user=$this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->loadModel(array('Minventories', 'Mstores'));
        $flag = $this->Mactions->checkAccessFromDb('store/viewAll', $user['UserId']);
        if(!$flag) $storeIds = $this->Mstores->getByUserId($user['UserId'], $flag, true);
        else $storeIds = array();
        $postData = array('StoreIds' => $storeIds);
        $data1 = $this->Minventories->searchByFilter($searchText, $itemFilters, $limit, $page, $postData);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }
}