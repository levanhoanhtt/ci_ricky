var app = app || {};

app.init = function (orderId, canEdit, transportId) {
    app.initLibrary();
    app.customer(canEdit, orderId);
    app.promotion();
    app.configTransport();
    app.configPayment();
    app.configExpand();
    app.sendOffset(); //gui bu
    app.orderComment(orderId);
    app.product(canEdit);
    app.checkOrder();
    if(orderId > 0){
        if ($('#tbodyProduct tr').length > 0) {
            keyUpInput();
            calcPrice(0);
        }
        app.cancelOrder(orderId);
        app.verifyOrder(orderId);
        app.updatePendingStatus(orderId);
        app.chooseStore(orderId);
        //app.createOrderChild(orderId);
        if (transportId == 0 || $('input#transportStatusId').val() == '5') app.addTransport(orderId);
        else app.cancelTransport(transportId, orderId);
        app.shareOrder(orderId);
    }
    app.remind();
};

app.initLibrary = function(){
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('input.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('input.datetimepicker').datetimepicker({
        format: 'dd/mm/yyyy hh:ii',
        startDate: new Date()
    }).on('changeDate', function(e){
        $(this).datetimepicker('hide');
    });
    $('body').on('keydown', 'input.cost', function (e) {
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8)) {
            e.preventDefault();
        }
    }).on('keyup', 'input.cost', function () {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    });
    $('#orderForm').on('submit', function (e) {
        e.preventDefault();
    });
    if($('.bxslider').length > 0) {
        var minNumber = 3;
        var maxNumber = 3;
        if($('.bxslider.short').length > 0) {
            minNumber = 3;
            maxNumber = 3;
        }
        else{
            minNumber = 4;
            maxNumber = 4;
        }
        if ($(window).width() < 1300) {
           if($('.bxslider.short').length > 0) {
                minNumber = 2;
                maxNumber = 2;
            }
            else{
                minNumber = 3;
                maxNumber = 3;
            }
        }
        $('.bxslider').bxSlider({
            minSlides: minNumber,
            maxSlides: maxNumber,
            moveSlides: 1,
            slideWidth: 500,
            pager: false,
            infiniteLoop: false,
            nextText: ">",
            prevText: "<"
        });
    }
};

app.customer = function(canEdit, orderId){
    if($('input#verifyStatusId').val() == '2') $('#btnCloseBoxCustomer').remove();
    if(canEdit) {
        chooseCustomer(function (li) {
            var customerId = parseInt(li.attr('data-id'));
            $('input#customerId').val('0');
            $('.btnTransport').addClass('actived');
            //$('#aCustomer').attr('href', 'javascript:void(0)').text('');
            $('p#customerGroupName').text('');
            $('input#customerName').val('');
            $('input#customerEmail').val('');
            $('input#customerPhone').val('');
            $('input#customerAddress').val('');
            if (customerId > 0) showInfoCustomer(customerId, true);
        });
        province('provinceId', 'districtId', 'wardId');
        province('customerProvinceId', 'customerDistrictId', 'customerWardId');
        $('input.iCheckCustomerType').on('ifToggled', function (e) {
            if (e.currentTarget.checked) {
                if (e.currentTarget.value == '2') $('#divCompany').show();
                else $('#divCompany').hide();
            }
        });
        $('select#countryId, select#customerCountryId').change(function () {
            if ($(this).val() == '232' || $(this).val() == '0') {
                $('.VNoff').css('display', 'none');
                $('.VNon').fadeIn();
            }
            else {
                $('.VNon').css('display', 'none');
                $('.VNoff').fadeIn();
            }
        });
        $('#btnCloseBoxCustomer').click(function(){
            if($('input#verifyStatusId').val() != '2') {
                $('#divCustomer').hide();
                $('#boxChooseCustomer').show();
                $('#customerForm')[0].reset();
                $('input#customerId').val('0');
                $('#customerForm .select2').val(0).trigger('change');
                $.each($("#customerForm select"), function (index, val) {
                    $(this).val(($(this).find("option:first").val()));
                });
                $('.VNoff').css('display', 'none');
                $('.VNon').fadeIn();
                $('.btnTransport').addClass('actived');
            }
            return false;
        });
        $('#btnAddCustomer').click(function () {
            $('#customerForm').trigger('reset');
            $('#modalAddCustomer').modal('show');
        });
        $('#btnUpdateCustomer').click(function (){
            var btn = $(this);
            if(validateEmpty('#customerForm')){
                var password = $('input#password').val().trim();
                if(password != ''){
                    if(password != $('input#rePass').val().trim()){
                        showNotification('Mật khẩu không trùng', 0);
                        return false;
                    }
                }
                btn.prop('disabled', true);
                var isReceiveAd = 1;
                if ($('input#isReceiveAd').parent('div').hasClass('checked')) isReceiveAd = 2;
                $.ajax({
                    type: "POST",
                    url: $('#customerForm').attr('action'),
                    data: {
                        CustomerId: 0,
                        FirstName: $('input#firstName').val().trim(),
                        LastName: $('input#lastName').val().trim(),
                        //FullName: $('input#fullName').val().trim(),
                        Email: $('input#email').val().trim(),
                        PhoneNumber: $('input#phoneNumber').val().trim(),
                        //PhoneNumber2: $('input#phoneNumber2').val().trim(),
                        GenderId: $('input[name="GenderId"]:checked').val(),
                        StatusId: 2,
                        BirthDay: $('input#birthDay').val(),
                        CustomerTypeId: $('input[name="CustomerTypeId"]:checked').val(),
                        CountryId: $('select#countryId').val(),
                        ProvinceId: $('select#provinceId').val(),
                        DistrictId: $('select#districtId').val(),
                        WardId: $('select#wardId').val(),
                        ZipCode: $('input#zipCode').val(),
                        Address: $('input#address').val().trim(),
                        CustomerGroupId: $('select#customerGroupId').val(),
                        FaceBook: '',
                        Comment: '',// $('#customerComment').val().trim(),
                        CareStaffId: $('select#careStaffId').val(),
                        DiscountTypeId: $('select#discountTypeId').val(),
                        PaymentTimeId: $('select#paymentTimeId').val(),
                        PositionName: $('input#positionName').val().trim(),
                        CompanyName: $('input#companyName').val().trim(),
                        TaxCode: $('input#taxCode').val().trim(),
                        DebtCost: replaceCost($('input#debtCost').val().trim(), true),
                        Password: password,
                        //TagNames: JSON.stringify(tags),
                        IsReceiveAd: isReceiveAd
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1) {
                            showInfoCustomer(json.data, true);
                            $('#modalAddCustomer').modal('hide');
                        }
                        btn.prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop('disabled', false);
                    }
                });
            }
            return false;
        });
    }
    else $('#btnCloseBoxCustomer').remove();
    if($('input#customerId').val() != '0') showInfoCustomer($('input#customerId').val(), orderId == 0);
    $('#aCustomerAddress').click(function(){
        if($('input#orderStatusId').val() == '1') {
            $('input#customerName').val($('.i-name').first().text());
            $('input#customerEmail').val($('.i-email').first().text());
            $('input#customerPhone').val($('.i-phone').first().text());
            $('input#customerAddress').val($('.spanAddress').first().text());
            var iCountry = $('.i-country').first();
            var countryId = iCountry.attr('data-id');
            if (countryId == '0' || countryId == '') countryId = '232';
            $('select#customerCountryId').val(countryId);
            $('select#customerProvinceId').val(iCountry.attr('data-province'));
            $('select#customerDistrictId').val(iCountry.attr('data-district'));
            $('select#customerWardId').val(iCountry.attr('data-ward'));
            $('input#customerZipCode').val(iCountry.attr('data-zip'));
            $('#modalCustomerAddress').modal('show');
        }
        return false;
    });
    $('#btnUpdateCustomerAddress').click(function(){
        var btn = $(this);
        if(validateEmpty('#modalCustomerAddress')) {
            btn.prop('disabled', true);
            var data = {
                CustomerId: $('input#customerId').val(),
                CustomerName: $('input#customerName').val().trim(),
                Email: $('input#customerEmail').val().trim(),
                PhoneNumber: $('input#customerPhone').val().trim(),
                Address: $('input#customerAddress').val().trim(),
                ProvinceId: $('select#customerProvinceId').val(),
                DistrictId: $('select#customerDistrictId').val(),
                WardId: $('select#customerWardId').val(),
                CountryId: $('select#customerCountryId').val(),
                ZipCode: $('input#customerZipCode').val().trim(),

                ItemId: orderId,
                ItemTypeId: 6
            };
            $.ajax({
                type: "POST",
                url: $('input#insertCustomerAddressUrl').val(),
                data: data,
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        $('input#customerAddressId').val(json.data);
                        $('.i-name').text(data.CustomerName);
                        $('.i-phone').text(data.PhoneNumber);
                        $('.i-email').text(data.Email);
                        $('.i-country').text($('select#customerCountryId option[value="' + data.CountryId + '"]').text()).attr('data-id', data.CountryId).attr('data-province', data.ProvinceId).attr('data-district', data.DistrictId).attr('data-ward', data.WardId).attr('data-zip', data.ZipCode);
                        var address = '';
                        if(data.CountryId == '232' || data.CountryId == '0') {
                            address += '<i class="fa fa-map-marker" aria-hidden="true"></i>';
                            var itemName  = '';
                            if(data.WardId != '0') itemName = $('select#customerWardId option[value="' + data.WardId + '"]').text();
                            address += '<span class="i-ward"><spam class="spanAddress">' + data.Address + '</spam> ' + itemName + '</span>';
                            itemName = data.DistrictId != '0' ? $('select#customerDistrictId option[value="' + data.DistrictId + '"]').text() : '';
                            if(itemName != '') address += '<span class="br-line i-district">' + itemName + '</span>';
                            itemName = data.ProvinceId != '0' ? $('select#customerProvinceId option[value="' + data.ProvinceId + '"]').text() : '';
                            if(itemName != '') address += '<span class="br-line i-province">' + itemName + '</span>';
                        }
                        else {
                            address += '<i class="fa fa-list-alt" aria-hidden="true"></i>';
                            address += '<span class="i-province">ZipCode: ' + data.ZipCode + '</span>';
                        }
                        $('.i-address').html(address);
                        $('#modalCustomerAddress').modal('hide');
                    }
                    else showNotification(json.message, json.code);
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
    });
};

app.product = function(canEdit){
    if(canEdit == 1) {
        chooseProduct(function (tr) {
            var id = tr.attr('data-id');
            var childId = tr.attr('data-child');
            var flag = false;
            $('#tbodyProduct tr').each(function () {
                if ($(this).attr('data-id') == id && $(this).attr('data-child') == childId) {
                    flag = true;
                    var quantity = replaceCost($(this).find('input.quantity').val(), true) + 1;
                    $(this).find('input.quantity').val(formatDecimal(quantity.toString()));
                    quantity = quantity * replaceCost($(this).find('.spanPrice').text(), true);
                    $(this).find('input.sumPrice').val(formatDecimal(quantity.toString()));
                    calcPrice(0);
                    return false;
                }
            });
            if (!flag) {
                $.ajax({
                    type: "POST",
                    url: $('input#getProductDetailUrl').val(),
                    data: {
                        ProductId: id,
                        ProductChildId: childId
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) {
                            var data = json.data;
                            var products = data.Products;
                            var productPath = $('input#productPath').val();
                            var html = '';
                            var totalPrice = replaceCost($('span#totalPrice').text(), true);
                            var totalWeight = 0;
                            for (var i = 0; i < products.length; i++) {
                                html += '<tr data-id="' + products[i].ProductId + '" data-child="' + products[i].ProductChildId + '" data-weight="' + products[i].Weight + '" data-kind="' + products[i].ProductKindId + '">';
                                html += '<td><img src="' + productPath + products[i].ProductImage + '" class="productImg"><a href="javascript:void(0)" class="light-dark aProductLink" target="_blank">' + products[i].ProductName + '</a></td>';
                                html += '<td class="text-center">' + products[i].BarCode + '</td>';
                                html += '<td class="text-center">' + products[i].GuaranteeMonth + ' tháng</td>';
                                html += '<td class="tdPrice text-right"><span class="spanPrice">' + formatDecimal(products[i].Price.toString()) + '</span> ₫</td>';
                                html += '<td><input class="form-control quantity sll" value="1"></td>';
                                html += '<td><input class="form-control sumPrice text-right" disabled value="' + formatDecimal(products[i].Price.toString()) + '"></td>';
                                html += '<td class="text-right"><a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a>';
                                html += '<span class="productPrices" style="display: none;">' + JSON.stringify(data.ProductPrices) + '</span><span class="originalPrice" style="display: none;">' + products[i].Price + '</span></td></tr>';
                                totalPrice += parseInt(products[i].Price.toString());
                                totalWeight += parseInt(products[i].Weight);
                            }
                            $('#tbodyProduct').append(html);
                            $('span#totalPrice').text(formatDecimal(totalPrice.toString()));
                            calcPrice(0);
                            keyUpInput();
                            var inputWeight = $('input#transportWeight');
                            if(inputWeight.length > 0){
                                totalWeight += replaceCost(inputWeight.val(), true);
                                inputWeight.val(formatDecimal(totalWeight.toString()));
                            }
                        }
                        else showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        });
    }
    $('#tbodyProduct').on('click', '.link_delete', function () {
        if(canEdit == 1) {
            var tr = $(this).parent().parent();
            tr.remove();
            calcPrice(0);
            if($('input#transportWeight').length > 0){
                var totalWeight = 0;
                $('#tbodyProduct tr').each(function(){
                    totalWeight += parseInt($(this).attr('data-weight')) * replaceCost($(this).find('input.quantity').val(), true);
                });
                $('input#transportWeight').val(formatDecimal(totalWeight.toString()));
            }
        }
        return false;
    }).on('click', '.aProductLink', function () {
        var tr = $(this).parent().parent();
        if(tr.attr('data-kind') == '3'){
            var productName = $(this).text();
            $.ajax({
                type: "POST",
                url: $('input#getProductChildComboUrl').val(),
                data: {
                    ProductId: tr.attr('data-id')
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        $('#spanProductComboName').text(productName);
                        var products = json.data;
                        var productPath = $('input#productPath').val();
                        var html = '';
                        for (var i = 0; i < products.length; i++) {
                            html += '<tr><td><img src="' + productPath + products[i].ProductImage + '" class="productImg"><a href="javascript:void(0)" class="light-dark" target="_blank">' + products[i].ProductName + '</a></td>';
                            html += '<td class="text-center">' + products[i].BarCode + '</td>';
                            html += '<td class="text-center">' + products[i].GuaranteeMonth + ' tháng</td>';
                            html += '<td class="text-right">' + formatDecimal(products[i].Price.toString()) + ' ₫</td>';
                            html += '<td class="text-center">' + products[i].Quantity + '</td>';
                            html += '<td class="text-center">';
                            if(products[i].VATStatusId) html += '<span class="label label-success">Có</span>';
                            else html += '<span class="label label-default">Không</span>';
                            html += '</td></tr>';
                        }
                        $('#tbodyProductCombo').html(html);
                        $('#modalProductCombo').modal('show');
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
};

app.checkOrder = function(){
    $('#aCheckOrder').click(function(){
        $('#modalCheckQuantity .modal-body').html('');
        var products = getListOrderProducts();
        if(products.length == 0){
            showNotification('Vui lòng chọn sản phẩm', 0);
            return false;
        }
        /*var storeId = parseInt($('input[name="OrderStoreId"]:checked').val());
        storeId = (isNaN(storeId)) ? 0 : storeId;*/
        $.ajax({
            type: "POST",
            url: $('input#checkOrderUrl').val(),
            data: {
                Products: products,
                StoreId: 0//storeId
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1){
                    var data = json.data;
                    var html = '';
                    var j;
                    for(var i = 0; i < data.length; i++){
                        html += '<div class="no-padding">';
                        if(data[i].IsInStock) html += '<label class="text-success"><i class="fa fa-check-circle">';
                        else html += '<label class="text-danger"><i class="fa fa-times-circle">';
                        html += '</i> ' + data[i].StoreName + '</label>';
                        html += '<div class="table-responsive divTable"><table class="table table-hover"><thead class="theadNormal">';
                        html += '<tr><th>Sản phẩm</th><th class="text-center">Số lượng cần</th><th class="text-center">Số lượng tồn</th><th class="text-center"></th></tr></thead><tbody>';
                        for(j = 0; j < data[i].Products.length; j++){
                            html += '<tr>';
                            html += '<td>' + $('#tbodyProduct tr[data-id="'+data[i].Products[j].ProductId+'"][data-child="'+data[i].Products[j].ProductChildId+'"] td').first().text() + '</td>';
                            html += '<td class="text-center">' + data[i].Products[j].Quantity + '</td>';
                            html += '<td class="text-center">' + data[i].Products[j].StockQuantity + '</td>';
                            if(data[i].Products[j].StockQuantity >= data[i].Products[j].Quantity) html += '<td class="text-center text-success"><i class="fa fa-check-circle"></i></td>';
                            else html += '<td class="text-center text-danger"><i class="fa fa-times-circle"></i></td>';
                            html += '</tr>';
                        }
                        html += '</tbody></table></div></div>';
                    }
                    $('#modalCheckQuantity .modal-body').html(html);
                    $('#modalCheckQuantity').modal('show');
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
        return false;
    });
};

app.promotion = function(){
    $('body').click(function (e) {
        var div = $('#box-promotion');
        if (div.css('display') == 'block' && !div.is(':hover')) div.toggle();
    });
    $('#aPromotion').click(function () {
        $(this).next().toggle();
        return false;
    });
    $('.footer-promotion .close-promotion').click(function (e) {
        e.preventDefault();
        $(this).parent().parent().hide();
    });
    $('#btn-cost').click(function (e) {
        $('input#reduceTypeId').val('1');
        $('.promotion-input').find('li').removeClass('active');
        $(this).addClass('active');
        $('input#reduceNumber').val('0');
    });

    $('#btn-precent').click(function (e) {
        $('input#reduceTypeId').val('2');
        $('.promotion-input').find('li').removeClass('active');
        $(this).addClass('active');
        $('input#reduceNumber').val('0');
    });

    $('input#reduceNumber').keydown(function (e) {
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8)) {
            e.preventDefault();
        }
    }).keyup(function (e) {
        var value = formatDecimal($(this).val());
        $(this).val(value);
        if(value != '0') $('input#promotionCode').val('').prop('disabled', true);
        else $('input#promotionCode').prop('disabled', false);
    });
    $('input#promotionCode').keyup(function (e) {
        if($(this).val().trim() != '') $('input#reduceNumber').val('0').prop('disabled', true);
        else $('input#reduceNumber').prop('disabled', false);
    });

    $('#btnApplyPromotion').click(function (e) {
        $('input#promotionId').val('0');
        $('input#discountPercent').val('0');
        var customerId = $('input#customerId').val();
        if(customerId == '0'){
            showNotification('Vui lòng chọn khách hàng', 0);
            return false;
        }
        var products = getListOrderProducts();
        if(products.length == 0){
            showNotification('Vui lòng chọn sản phẩm', 0);
            return false;
        }
        var reduceNumber = replaceCost($('input#reduceNumber').val(), true);
        var totalPrice = replaceCost($('span#totalPrice').text(), true);
        var promotionCode = $('input#promotionCode').val().trim();
        if(promotionCode == ''){
            var promotionComment = $('input#promotionComment').val().trim();
            if(promotionComment == ''){
                showNotification('Lý do giảm giá không được bỏ trống', 0);
                return false;
            }
            if($('input#reduceTypeId').val() == '1'){
                if(reduceNumber < 0){
                    showNotification('Số tiền giảm không được nhỏ hơn 0', 0);
                    return false;
                }
                if(reduceNumber > totalPrice){
                    showNotification('Số tiền giảm không được lớn hơn tổng tiền hàng', 0);
                    return false;
                }
                $('span#promotionCost').text(formatDecimal(reduceNumber.toString()));
                $('span#vnd1').show();
                $('#iPromotionText').text('(' + promotionComment + ')');
                calcPrice(0);
                $('#aPromotion').next().toggle();
            }
            else{
                if(reduceNumber <= 0 || reduceNumber > 100){
                    showNotification('Phần trăm giảm giá không hợp lệ', 0);
                    return false;
                }
                reduceNumber = Math.ceil(totalPrice * reduceNumber / 100);
                $('span#promotionCost').text(formatDecimal(reduceNumber.toString()));
                $('span#vnd1').show();
                $('#iPromotionText').text('(' + promotionComment + ')');
                calcPrice(0);
                $('#aPromotion').next().toggle();
            }
        }
        else {
            var transportCost = $('span#transportCost').text();
            if(transportCost == '-') transportCost = 0;
            else transportCost = replaceCost(transportCost, true);
            $.ajax({
                type: 'POST',
                url: $('input#checkPromotionUrl').val(),
                data: {
                    Products: products,
                    PromotionCode: promotionCode,
                    CustomerId: customerId,
                    CustomerGroupId: $('input#customerGroupId1').val(),
                    TotalPrice: totalPrice,
                    TransportCost: transportCost,
                    ProvinceId: $('.i-country').first().attr('data-province')
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        var data = json.data;
                        $('input#promotionId').val(data.PromotionId);
                        $('input#reduceTypeId').val(data.DiscountPercent > 0 ? 2 : 1);
                        $('input#discountPercent').val(data.DiscountPercent);
                        $('span#promotionCost').text(formatDecimal(data.DiscountCost.toString()));
                        $('span#vnd1').show();
                        $('#iPromotionText').text('(' + promotionCode + ')');
                        $('#aPromotion').next().toggle();
                        calcPrice(0);
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
    });
};

app.configTransport = function(){
    $('#aTransport').click(function(){
        $('#modalConfigTransport').modal('show');
        return false;
    });
    $('.transportType').on('ifChecked', function (event) {
        if (event.target.value == '1') {
            $('#divFreeShip').show();
            $('input#transportFee').val(0).prop('disabled', true);
            $('select#transportTypeId1').prop('disabled', true);
        }
        else {
            $('#divFreeShip').hide();
            $('select#transportReasonId').val('0');
            $('input#transportFee').prop('disabled', false);
            $('select#transportTypeId1').prop('disabled', false);
        }
    });
    $('input#transportFee').keydown(function (e) {
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8)) {
            e.preventDefault();
        }
    }).keyup(function (e) {
        var value = formatDecimal($(this).val());
        $(this).val(value);
    });

    $('#btnApplyConfigTransport').click(function() {
        var transportTypeId = 0;
        var transportFee = 0;
        var transportText = '(Miễn phí vận chuyển)';
        var type = $('input[name="TransportType"]:checked').val();
        if(type == '1'){
            var transportReasonId = $('select#transportReasonId').val();
            if(transportReasonId == '0'){
                showNotification('Vui lòng chọn lý do miễn phí ship', 0);
                return false;
            }
            transportText = '(' + $('select#transportReasonId option[value="'+transportReasonId+'"]').text() + ')';
        }
        else{
            transportTypeId = $('select#transportTypeId1').val();
            if(transportTypeId == '0'){
                showNotification('Vui lòng chọn phương thức vận chuyển', 0);
                return false;
            }
            transportFee = replaceCost($('input#transportFee').val().trim(), true);
            if(transportFee <= 0){
                showNotification('Phí vận chuyển phải lớn hơn 0', 0);
                return false;
            }
            transportText = '(' + $('select#transportTypeId1 option[value="'+transportTypeId+'"]').text() + ')';
        }
        $('span#transportCost').text(formatDecimal(transportFee.toString()));
        $('span#vnd2').show();
        $('#iTransportText').text(transportText);
        $('#modalConfigTransport').modal('hide');
        calcPrice(0);
    });
};

app.configPayment = function(){
    $('#aPayment').click(function(){
        $('#modalConfigPaymnent').modal('show');
        return false;
    });
    $('select#paymentStatusId').change(function(){
        if($(this).val() == '1'){
            $("#divPaymentFee").hide();
            $("input#paymentFee").prop('disabled', true).removeClass("border-input").val('0');
        }
        else if($(this).val() == '2'){
            $("input#paymentFee").prop('disabled', false).addClass("border-input").val('0');
            $("#divPaymentFee").show();
        }
        else if($(this).val() == '3'){
            var paymentCost = $('span#paymentCost').text();
            paymentCost = paymentCost == '-' ? 0 : replaceCost(paymentCost, true);
            var paymentFee = replaceCost($('span#totalCost').text(), true) + paymentCost;
            $("input#paymentFee").removeClass("border-input").prop('disabled', true).val(formatDecimal(paymentFee.toString()));
            $("#divPaymentFee").show();
        }
    });
    $('#btnApplyConfigPayment').click(function() {
        var paymentStatusId = $('select#paymentStatusId').val();
        if(paymentStatusId == '0'){
            showNotification('Vui lòng chọn loại hình thanh toán', 0);
            return false;
        }
        var paymentFee = replaceCost($('input#paymentFee').val().trim(), true);
        if(paymentFee < 0){
            showNotification('Số tiền thanh toán phải lớn hơn 0', 0);
            return false;
        }
        var totalPrice = replaceCost($('span#totalPrice').text(), true);
        if(paymentFee > replaceCost($('span#orderCost').text(), true)){
            showNotification('Số tiền thanh toán không được lớn hơn tổng cần thanh toán', 0);
            return false;
        }
        $('span#paymentCost').text(formatDecimal(paymentFee.toString()));
        $('span#vnd3').show();
        $('#iPaymentText').text('(' + $('select#paymentStatusId option[value="'+paymentStatusId+'"]').text() + ')');
        $('#modalConfigPaymnent').modal('hide');
        calcPrice(totalPrice);
    });
};

app.configExpand = function(){
    $('#orderForm').on('click', '.aExpand', function(){
        $('#modalConfigExpand').modal('show');
        return false;
    });
    $('.btnApplyConfigExpand').click(function(){
        var id = $(this).attr('data-id');
        var cost = replaceCost($('input#otherCost_' + id).val().trim(), true);
        if(cost <= 0){
            showNotification('Số tiền phải lớn hơn 0', 0);
            return false;
        }
        var isDebit = $('input#debitOtherTypeId').val() == id;
        if(isDebit){
            if(!checkDebtCost(cost, 'Ghi nợ lớn hơn số tiền cho phép')) return false;
        }
        var flag = true;
        $('.divConfigExpand div.item').each(function(){
            if($(this).attr('data-id') == id){
                flag = false;
                return false;
            }
        });
        if(flag){
            var html = '<div class="item" data-id="' + id + '">';
            html += '<div class="col-md-6" style="clear: both">';
            html += '<a href="javascript:void(0)" class="aExpand">' + $('#pOtherServiceName_' + id).text() + '</a></div>';
            html += '<div class="col-md-6 text-right"><span id="spanOtherCost_' + id + '" class="spanOtherCost">' + formatDecimal(cost.toString()) + '</span> đ</div>';
            html += '</div>';
            $('#divService').append(html);
        }
        else {
            $('span#spanOtherCost_' + id).text(formatDecimal(cost.toString()));
            if(isDebit) $('#divOrderOwn').show();
        }
        $('#modalConfigExpand').modal('hide');
        calcPrice(0);
    });
};

app.sendOffset = function(){
    $('#aTotalPrice').click(function(){
        if($('select#orderTypeId').val() == $('input#offsetOrderTypeId').val()){
            $('input#offsetTotalPrice').val($('span#totalPrice').text());
            $('#modalSendOffset').modal('show');
        }
        return false;
    });
    $('#btnSendOffset').click(function(){
        var value = replaceCost($('input#offsetTotalPrice').val(), true);
        if(value >= 0){
            $('span#totalPrice, span#totalCost').text(formatDecimal(value.toString()));
            calcPrice(value);
            $('#modalSendOffset').modal('hide');
        }
        else showNotification('Tổng tiền hàng không được nhỏ hơn 0', 0);
    });
};

app.orderComment = function(orderId){
    $('.btnInsertComment').click(function(){
        var id = $(this).attr('data-id');
        var comment = $('input#comment_' + id).val().trim();
        if(comment != ''){
            if(orderId > 0){
                var btn = $(this);
                btn.prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('input#insertOrderCommentUrl').val(),
                    data: {
                        OrderId: orderId,
                        Comment: comment
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1) {
                            $('div#listComment_' + id).prepend(genItemComment(comment));
                            $('input#comment_' + id).val('');
                        }
                        else showNotification(json.message, json.code);
                        btn.prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop('disabled', false);
                    }
                });
            }
            else{
                $('div#listComment_' + id).prepend(genItemComment(comment));
                $('input#comment_' + id).val('');
            }
        }
        else{
            showNotification('Vui lòng nhập ghi chú', 0);
            $('input#comment_' + id).focus();
        }
    });
    if(orderId > 0){
        $('#aShowComment').click(function(){
            $('#modalItemComment').modal('show');
            return false;
        });
    }
};

app.cancelOrder = function(orderId){
    $('#aCancelOrder').click(function(){
        if($('input#orderStatusId').val() == '1') $('#modalCancelOrder').modal('show');
        return false;
    });
    $('#btnCancelOrder').click(function(){
        var btn = $(this);
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#changeOrderStatusUrl').val(),
            data: {
                ItemIds: JSON.stringify([orderId]),
                StatusId: 3,
                CancelReasonId: $('select#cancelReasonId').val(),
                CancelComment: $('input#cancelComment').val().trim()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1){
                    $('#aCancelOrder').hide();
                    $('#divCancelOrder').fadeIn();
                    $('#modalCancelOrder').modal('hide');
                    $('#ulActionLogs').prepend('<li>' + $('input#fullNameLoginId').val() + ' thay đổi trạng thái đơn hàng về Đã hủy bỏ<span class="time">' + getCurrentDateTime(2) + '</span></li>');
                }
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
    });
};

app.verifyOrder = function(orderId){
    var aVerifyOrder = $('#aVerifyOrder');
    if(aVerifyOrder.attr('title') != '') aVerifyOrder.tooltip();
    aVerifyOrder.click(function(){
        if($('input#verifyStatusId').val() == '1') $('#modalVerifyOrder').modal('show');
        return false;
    });
    $('#btnVerifyOrder').click(function () {
        var btn = $(this);
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#updateVerifyOrderUrl').val(),
            data: {
                OrderIds: JSON.stringify([orderId]),
                VerifyStatusId: 2
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) {
                    $('input#verifyStatusId').val('2');
                    aVerifyOrder.addClass('verify').attr('title', 'Đã được xác thực bởi ' + $('input#fullNameLoginId').val() + ' lúc ' + getCurrentDateTime(1)).tooltip();
                    $('#btnCloseBoxCustomer').remove();
                    $('#modalVerifyOrder').modal('hide');
                }
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });

    });
};

app.updatePendingStatus = function(orderId){
    $('#aUpdatePending').click(function(){
        if($('input#orderStatusId').val() == '1') $('#modalUpdatePending').modal('show');
        return false;
    });
    $('#btnUpdatePending').click(function(){
        var pendingStatusId = parseInt($('input[name="PendingStatusId"]:checked').val());
        if(pendingStatusId > 0){
            if(pendingStatusId != parseInt($('input#pendingStatusId').val())){
                var btn = $(this);
                btn.prop('disabled', true);
                updateOrderField(orderId, 'PendingStatusId', pendingStatusId, function(data){
                    $('input#pendingStatusId').val(pendingStatusId);
                    $('#spanOrderStatus').text(data.StatusName);
                    $('#modalUpdatePending').modal('hide');
                    btn.prop('disabled', false);
                }, function(){
                    btn.prop('disabled', false);
                });
            }
            else showNotification('Vui lòng chọn trạng thái Chờ xử lý khác', 0);
        }
        else showNotification('Vui lòng chọn trạng thái Chờ xử lý', 0);
    });
};

app.chooseStore = function(orderId){
    $('#aChooseStore').click(function(){
        $('#modalChooseStore').modal('show');
        return false;
    });
    $('#btnUpdateStore').click(function(){
        var orderStoreId = parseInt($('input[name="OrderStoreId"]:checked').val());
        if(orderStoreId > 0){
            if(orderStoreId != parseInt($('input#orderStoreId').val())){
                var btn = $(this);
                btn.prop('disabled', true);
                updateOrderField(orderId, 'StoreId', orderStoreId, function(data){
                    $('input#orderStoreId').val(orderStoreId);
                    $('#spanStoreName').text(data.StoreName);
                    $('#modalChooseStore').modal('hide');
                    $('select#storeId').val(orderStoreId).trigger('change');
                    btn.prop('disabled', false);
                }, function(){
                    btn.prop('disabled', false);
                });
            }
            else showNotification('Vui lòng chọn Cơ sở xử lý khác', 0);
        }
        else showNotification('Vui lòng chọn Cơ sở xử lý', 0);
    });
};

app.addTransport = function(orderId){
    /*var dataProductExits = {
        index: 0,
        data: []
    };*/
    /*$('#btnCheckOrder').click(function () {
        if ($('#checkOrder').length > 0 || typeof dataProductExits.data.StoreData == 'object') {
            $('#modalCheckOrder').modal('show');
            return;
        }
        $.ajax({
            type: "POST",
            url: $('input#checkOrderUrl').val(),
            data: {
                OrderId: orderId
            },
            success: function (respone) {
                //render table prodcut extis
                respone = JSON.parse(respone);
                if (respone.status != undefined && respone.status == -1) {
                    var productExits = JSON.parse(respone.productExits);
                    var html = "<div id='checkOrder'>" +
                        "<div>Không đủ sản phẩm để cung cấp cho đơn hàng</div>" +
                        '<h4>Đơn hàng có thể đóng gói và chuyển đi</h4>' +
                        "<div>{productsExit}</div>" +
                        '<h4>Đơn hàng đang chờ hàng về</h4>' +
                        "<div>{productsWait}</div>" +
                        "<p style='font-size: 12px;font-style: italic'>Bạn có thể lưu những sản phẩm có sẵn thành các đơn hàng rồi chuyển trước và các sản phẩm thiếu , chưa có thành 1 đơn hàng chờ hoặc chờ đủ hết sản phẩm rồi chuyển</p>" +
                        "<button class='btn' id='transactionProduct'>Chuyển trước</button>" +
                        "<button class='btn margin-left-10' id='waitProduct'>Chờ</button>" +
                        "</div>";
                    var productsExitsTable = "";
                    for (var key in productExits) {
                        var tableHtml = "<div class='products'>" +
                            "<h5 class='title'>{{title}}</h5>" +
                            "<table class='table table-hover table-bordered'>" +
                            "<thead class='theadNormal'>" +
                            "<tr>" +
                            "<th style='width: 70%;'>Sản phẩm</th>" +
                            "<th style='width: 30%;'>Số lượng hiện còn</th>" +
                            "</tr>" +
                            "</thead>" +
                            "<tbody class='productExitsTbody'>{{tbody}}</tbody>" +
                            "</table>" +
                            "</div>";
                        var products = productExits[key];
                        delete products.OrderCode;
                        var title = products[0].Location;
                        //console.log(products);
                        var tbody = '';
                        for (var item in products) {
                            tbody += "<tr>" +
                                "<td>" + products[item].ProductName + " " + products[item].ProductNameChild + "</td>" +
                                "<td>" + products[item].Quantity + "</td>" + +"</tr>";
                        }
                        tableHtml = tableHtml.replace('{{title}}', title).replace('{{tbody}}', tbody);
                        productsExitsTable += tableHtml;
                    }

                    var productsWait = JSON.parse(respone.productWait);
                    delete productsWait.OrderCode;
                    var tableWaitHtml = "<div class='products'>" +
                        "<table class='table table-hover table-bordered'>" +
                        "<thead class='theadNormal'>" +
                        "<tr>" +
                        "<th style='width: 70%;'>Sản phẩm</th>" +
                        "<th style='width: 30%;'>Số lượng hiện còn</th>" +
                        "</tr>" +
                        "</thead>" +
                        "<tbody class='productWaitTbody'>{{tbody}}</tbody>" +
                        "</table>" +
                        "</div>";
                    var tbodyWait = '';
                    for (var key in productsWait) {
                        var trHtml = "<tr>" +
                            "<td>" + productsWait[key]['ProductName'] + " " + productsWait[key]['ProductNameChild'] + "</td>" +
                            "<td>" + productsWait[key]['order_quantity'] + "</td>" + +"</tr>";
                        tbodyWait += trHtml;
                    }
                    tableWaitHtml = tableWaitHtml.replace('{{tbody}}', tbodyWait);
                    html = html.replace('{productsExit}', productsExitsTable).replace('{productsWait}', tableWaitHtml).replace(/NaN+/igm, '');
                    $('#productExits').html(html);
                }
                else {
                    dataProductExits.index = 0;
                    dataProductExits.data = respone;
                    renderTableProductExits(dataProductExits);
                }
                $('#modalCheckOrder').modal('show');
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
    $('#modalCheckOrder').on('click', '#transactionProduct', function () {
        $.ajax({
            type: "POST",
            url: $('input#insertOrderWaitUrl').val(),
            data: {},
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) $('#modalCheckOrder').modal('hide');
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    }).on('click', '#waitProduct', function () {
        $.ajax({
            type: "POST",
            url: $('input#changeOrderStatusUrl').val(),
            data: {
                ItemIds: JSON.stringify([orderId]),
                StatusId: 1
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) $('#modalCheckOrder').modal('hide');
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });*/
    $('.btnTransport').click(function () {
        if(!$(this).hasClass('actived')){
            if($('input#verifyStatusId').val() == '2'){
                if($('select#orderReasonId').val() == '0'){
                    showNotification('Bạn chưa chọn Lý do mua hàng', 0);
                    return false;
                }
                else if($('select#orderChanelId').val() == '0'){
                    showNotification('Bạn chưa chọn Kênh bán hàng', 0);
                    return false;
                }
                var paymentCost = $('span#paymentCost').text();
                var flag = false;
                var paymentStatusId = parseInt($('select#paymentStatusId').val());
                if(paymentStatusId > 0) {
                    paymentCost = paymentCost == '-' ? 0 : replaceCost(paymentCost, true);
                    if (paymentStatusId == 1) {
                        paymentCost = 0;
                        flag = true;
                    }
                    else flag = paymentCost > 0;
                }
                if(!flag){
                    showNotification('Bạn chưa cấu hình thanh toán', 0);
                    return false;
                }
                if(!checkDebtCost(replaceCost($('#divOrderOwn .spanOtherCost').text(), true), 'Tài khoản khách hàng không đủ tiền, cần hoàn thành phiếu chờ thanh toán')){
                    if(replaceCost($('input#remindOwnCost').val(), true) != replaceCost($('input#ownCost').val(), true)) $('#modalWaitPayment').modal('show');
                    return false;
                }
                $('#modalTransport').modal('show');
            }
            else showNotification('Đơn hàng chưa được xác thực', 0);
        }
    });
    $('#btnAddTransport').click(function () {
        var btn = $(this);
        btn.prop('disabled', true);
        var transportCODCost = replaceCost($('input#transportCODCost').val(), true);
        var transportCost = $('span#transportCost').text();
        transportCost = transportCost == '-' ? 0 : replaceCost(transportCost, true);
        var paymentCost = $('span#paymentCost').text();
        paymentCost = paymentCost == '-' ? 0 : replaceCost(paymentCost, true);
        $.ajax({
            type: "POST",
            url: $('input#updateTransportUrl').val(),
            data: {
                TransportId: 0,
                TransportCode: '',
                OrderId: orderId,
                CustomerId: $('input#customerId').val(),
                CustomerAddressId: $('input#customerAddressId').val(),
                TransportUserId: 0,
                TransportStatusId: 1,
                PendingStatusId: 0,
                TransportTypeId: $('select#transportTypeId').val(),
                TransporterId: 0, //$('select#transporterId').val(),
                StoreId: $('select#storeId').val(),
                Tracking: '',//$('input#transportTracking').val().trim(),
                Weight: $('input#transportWeight').val().trim(),
                CODCost: transportCODCost,
                CODStatusId: transportCODCost == 0 ? 1 : 3,
                ShipCost: 0,
                Comment: $('#transportComment').val().trim(),
                CancelReasonId: 0,
                CancelReasonText: '',

                TagNames: JSON.stringify([]),

                PaymentCost: paymentCost,
                OrderCode: $('#aVerifyOrder').text().trim(),
                OrderReasonId: $('select#orderReasonId').val(),
                OrderChanelId: $('select#orderChanelId').val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) redirect(true, '');
                else btn.prop('disabled', true);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', true);
            }
        });
    });
};

app.cancelTransport = function(transportId, orderId){
    $('.btn-cancle-cart').click(function(){
        $('#modalCancelTransport').modal('show');
        return false;
    });
    $('#btnCancelTransport').click(function(){
        var btn = $(this);
        var transportStatusId = parseInt($('input#transportStatusId').val());
        if(transportStatusId == 1){ //huy thang, ko can thong bao
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#changeTransportStatusUrl').val(),
                data: {
                    TransportId: transportId,
                    FieldName: 'TransportStatusId',
                    FieldValue: 5,
                    OrderId: orderId,
                    CancelReasonId: $('select#cancelReasonId1').val(),
                    CancelComment: $('input#cancelComment1').val().trim(),
                    StoreId: $('input#transportStoreId').val()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(true, '');
                    else btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else if(transportStatusId != 5){ //gui thong bao den VC

        }
    });
};

app.shareOrder = function(orderId){
    $('#aShareOrder').click(function(){
        if($('input#orderStatusId').val() == '1' && $('input#isShare').val() != '2') $('#modalShareOrder').modal('show');
        return false;
    });
    $('#btnShareOrder').click(function(){
        var btn = $(this);
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#updateFieldUrl').val(),
            data: {
                OrderId: orderId,
                FieldName: 'IsShare',
                FieldValue: 2,
                ShareComment: $('input#shareComment').val().trim()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) {
                    $('input#isShare').val('2');
                    $('#modalShareOrder').modal('hide');
                }
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
    });
};

app.remind = function(){
    $('.aRemind').click(function(){
        $('#modalRemind').modal('show');
        return false;
    });
    $('select#userId').change(function(){
        if($(this).val() != '0') $('select#partId').val('0').trigger('change');
    });
    $('select#partId').change(function(){
        if($(this).val() != '0') $('select#userId').val('0').trigger('change');
    });
    $('#btnAddRemind').click(function(){
        if(validateEmpty('#modalRemind')){
            var btn = $(this);
            btn.prop('disabled', true);
            var comments = [];
            var remindComment = $('input#remindComment').val().trim();
            if(remindComment != '') comments.push(remindComment);
            $.ajax({
                type: "POST",
                url: $('input#insertRemindUrl').val(),
                data: {
                    RemindId: 0,
                    RemindTitle: $('input#remindTitle').val().trim(),
                    RemindDate: $('input#remindDate1').val().trim(),
                    IsRepeat: 1,
                    RepeatDay: 0,
                    RepeatHour: 0,
                    RemindStatusId: 1,
                    UserId: $('select#userId').val(),
                    PartId: $('select#partId').val(),

                    Comments: JSON.stringify(comments)
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) {
                        $('#modalRemind input').val('');
                        $('select#userId').val($('input#userLoginId').val());
                        $('select#partId').val('0');
                        $('#modalRemind').modal('hide');
                    }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
    });
};

$(document).ready(function () {
    var orderId = parseInt($('input#orderId').val());
    var canEdit = parseInt($('input#canEdit').val());
    var transportId = parseInt($('input#transportId').val());
    app.init(orderId, canEdit, transportId);
    var tags = [];
    //var transportTags = [];
    var inputTag = $('input#tags');
    inputTag.tagsInput({
        'width': '100%',
        'height': '90px',
        'interactive': true,
        'defaultText': '',
        'onAddTag': function (tag) {
            tags.push(tag);
        },
        'onRemoveTag': function (tag) {
            var index = tags.indexOf(tag);
            if (index >= 0) tags.splice(index, 1);
        },
        'delimiter': [',', ';'],
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 0
    });
    $('.ntags').click(function(){
        var tag = $(this).text();
        if(!inputTag.tagExist(tag)) inputTag.addTag(tag);
        return false;
    });
    if (orderId > 0) {
        $('input.tagName').each(function () {
            inputTag.addTag($(this).val());
        });
    }
    $('.submit').click(function () {
        var orderStatusId = $(this).attr('data-id');
        if($(this).hasClass('btnSubmitPos')){
            if($('input#verifyStatusId').val() != '2'){
                showNotification('Đơn hàng chưa được xác thực', 0);
                return false;
            }
            if($('input#orderStoreId').val() == '0'){
                showNotification('Chưa chọn cơ sở cho đơn hàng', 0);
                return false;
            }
            $('input#orderStatusId').val(orderStatusId);
        }
        var products = getListOrderProducts();
        if (products.length > 0) {
            var customerId = parseInt($('input#customerId').val());
            if (customerId > 0) {
                if(validateEmpty('#orderForm') && validateNumber('#orderForm', true, ' không được bỏ trống')){
                    if($('input#deliveryTypeId').val() == '1'){ //pos
                        if(!checkDebtCost(replaceCost($('#divOrderOwn .spanOtherCost').text(), true), 'Tài khoản khách hàng không đủ tiền, cần hoàn thành phiếu chờ thanh toán')){
                            if(replaceCost($('input#remindOwnCost').val(), true) != replaceCost($('input#ownCost').val(), true)) $('#modalWaitPayment').modal('show');
                        }
                        else saveOrder(orderId, customerId, orderStatusId, replaceCost($('span#totalCost').text(), true), 3, tags, products, 0, '', '');
                    }
                    else{
                        var paymentStatusId = parseInt($('select#paymentStatusId').val());
                        if(paymentStatusId > 0){
                            var paymentCost = $('span#paymentCost').text();
                            paymentCost = paymentCost == '-' ? 0 : replaceCost(paymentCost, true);
                            var flag = false;
                            if(paymentStatusId == 1){
                                paymentCost = 0;
                                flag = true;
                            }
                            else flag = paymentCost > 0;
                            if(flag){
                                flag = checkDebtCost(replaceCost($('#divOrderOwn .spanOtherCost').text(), true), 'Tài khoản khách hàng không đủ tiền, cần hoàn thành phiếu chờ thanh toán');
                                if(flag) saveOrder(orderId, customerId, orderStatusId, paymentCost, paymentStatusId, tags, products, 0, '', '');
                                else if(replaceCost($('input#remindOwnCost').val(), true) != replaceCost($('input#ownCost').val(), true)) $('#modalWaitPayment').modal('show');
                            }
                            else showNotification('Bạn chưa cấu hình thanh toán', 0);
                        }
                        else showNotification('Bạn chưa cấu hình thanh toán', 0);
                    }
                }
            }
            else showNotification('Vui lòng chọn khách hàng', 0);
        }
        else showNotification('Vui lòng chọn sản phẩm', 0);
        return false;
    });

    $('#btnSaveOwnOrder').click(function(){
        var ownCost = replaceCost($('input#ownCost').val(), true);
        if(ownCost <= 0){
            showNotification('Số tiền chờ thanh toán phải lớn hơn 0', 0);
            $('input#ownCost').focus();
            return false;
        }
        var remindDate = $('input#remindDate').val().trim();
        /*if(remindDate == ''){
            showNotification('Vui lòng chọn ngày nhắc thanh toán', 0);
            $('input#remindDate').focus();
            return false;
        }*/
        saveOrder(orderId, $('input#customerId').val(), $('input#orderStatusId').val(), replaceCost($('span#paymentCost').text(), true), $('select#paymentStatusId').val(), tags, getListOrderProducts(), ownCost, remindDate, $('input#ownComment').val().trim());
        return false;
    });
});

//=====================================================================

function saveOrder(orderId, customerId, orderStatusId, paymentCost, paymentStatusId, tags, products, ownCost, remindDate, ownComment){
    $('.submit').prop('disabled', true);
    var orderServices = [];
    var otherServiceId = 0;
    var serviceCost = 0;
    var debtCost = 0;
    var debitOtherTypeId = $('input#debitOtherTypeId').val();
    $('.divConfigExpand div.item').each(function(){
        otherServiceId = $(this).attr('data-id');
        serviceCost = replaceCost($(this).find('.spanOtherCost').text(), true);
        if (otherServiceId != '0' && serviceCost > 0){
            orderServices.push({
                OtherServiceId: otherServiceId,
                ServiceCost: serviceCost
            });
            if(otherServiceId == debitOtherTypeId) debtCost = serviceCost;
        }
    });
    var comments = [];
    if(orderId == 0){
        $('.listComment .pComment').each(function(){
            comments.push($(this).text());
        });
    }
    var deliveryTypeId = $('input#deliveryTypeId').val();
    $.ajax({
        type: "POST",
        url: $('#orderForm').attr('action'),
        data: {
            OrderId: orderId,
            CustomerId: customerId,
            CustomerAddressId: $('input#customerAddressId').val(),
            StaffId: $('input#userLoginId').val(),
            OrderChanelId: $('select#orderChanelId').val(),
            OrderStatusId: orderStatusId,
            PendingStatusId: $('input#pendingStatusId').val(),
            OrderParentId: 0,
            Discount: $('span#promotionCost').text() == '-' ? 0 : replaceCost($('span#promotionCost').text(), true),
            TransportCost: $('span#transportCost').text() == '-' ? 0 : replaceCost($('span#transportCost').text(), true),
            TransportTypeId: $('select#transportTypeId1').val(),
            PaymentCost: paymentCost,
            PaymentStatusId: paymentStatusId,
            PaymentComment: $('#paymentComment').val().trim(),
            TotalCost: replaceCost($('span#orderCost').text(), true),
            VerifyStatusId: $('input#verifyStatusId').val(),
            OrderTypeId: $('select#orderTypeId').val(),
            DeliveryTypeId: deliveryTypeId,
            StoreId: $('input#orderStoreId').val(),
            OrderReasonId: $('select#orderReasonId').val(),

            TagNames: JSON.stringify(tags),
            Products: JSON.stringify(products),
            OrderServices: JSON.stringify(orderServices),

            PromotionId: $('input#promotionId').val(),
            PromotionCode: $('input#promotionCode').val().trim(),
            DiscountPercent: $('input#discountPercent').val(),
            PromotionComment: $('input#promotionComment').val().trim(),

            Comments: JSON.stringify(comments),

            OwnCost: ownCost,
            RemindDate: remindDate,
            RemindComment: ownComment
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if (json.code == 1) {
                if(orderId == 0) redirect(false, $('input#orderEditUrl').val() + '/' + json.data);
                else{
                    $('input#remindOwnCost').val(ownCost);
                    if (ownCost > 0) $('#modalWaitPayment').modal('hide');
                    if($('input#deliveryTypeId').val() == '1' && orderStatusId == 6){
                        $('.btnSubmitPos, #aCheckOrder').remove();
                        $('#aNoneCOD').show();
                        $('#spanOrderStatus').text('Thành công');
                        showInfoCustomer($('input#customerId').val(), false);
                    }
                }
            }
            $('.submit').prop('disabled', false);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            $('.submit').prop('disabled', false);
        }
    });
}

function showInfoCustomer(customerId, isShowCustomerAdress) {
    $.ajax({
        type: "POST",
        url: $('input#getCustomerDetailUrl').val(),
        data: {
            CustomerId: customerId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if(json.code == 1) {
                var data = json.data;
                $('input#customerId').val(customerId);
                $('input#customerGroupId1').val(data.CustomerGroupId);
                $('input#debtCost1').val(data.DebtCost);
                if (data.PhoneNumber != '' && data.PhoneNumber2 != '') data.PhoneNumber = data.PhoneNumber + ' - ' + data.PhoneNumber2;
                if(isShowCustomerAdress) {
                    var address = '';
                    if(data.CountryId == '232' || data.CountryId == '0') {
                        address += '<i class="fa fa-map-marker" aria-hidden="true"></i>';
                        address += '<span class="i-ward"><spam class="spanAddress">' + data.Address + '</spam> ' + data.WardName + '</span>';
                        if (data.DistrictName != '') address += '<span class="br-line i-district">' + data.DistrictName + '</span>';
                        if (data.ProvinceName != '') address += '<span class="br-line i-province">' + data.ProvinceName + '</span>';
                    }
                    else {
                        address += '<i class="fa fa-list-alt" aria-hidden="true"></i>';
                        address += '<span class="i-province">ZipCode: ' + data.ZipCode + '</span>';
                    }
                    $('.i-name').text(data.FullName);
                    $('.i-phone').text(data.PhoneNumber);
                    $('.i-email').text(data.Email);
                    $('.i-address').html(address);
                    $('.i-country').text(data.CountryName).attr('data-id', data.CountryId).attr('data-province', data.ProvinceId).attr('data-district', data.DistrictId).attr('data-ward', data.WardId).attr('data-zip', data.ZipCode);
                }
                var editCustomerUrl = $('input#editCustomerUrl').val() + '/' + customerId + '/';
                $('h4.i-name').html('<a href="' + editCustomerUrl + '3" target="_blank">' + data.FullName + '</a>');
                $('div.i-phone').text(data.PhoneNumber);
                if(data.CustomerGroupName != '') $('.i-cusType').html('<span class="label label-success">' + data.CustomerGroupName + '</span>').show();
                else $('.i-cusType').hide();
                $('.i-total-orders').html('<a href="' + editCustomerUrl + '1" target="_blank">' + data.TotalOrders + '</a>');
                $('span#customerBalance').html('<a href="' + editCustomerUrl + '2" target="_blank">' + formatDecimal(data.Balance) + '</a>');
                if($('select#orderTypeId').val() != $('input#offsetOrderTypeId').val() && replaceCost($('span#totalPrice').text(), true) > 0 && $('input#deliveryTypeId').val() == '2')  $('.btnTransport').removeClass('actived');
                else if($('select#orderTypeId').val() == $('input#offsetOrderTypeId').val() && $('input#deliveryTypeId').val() == '2')  $('.btnTransport').removeClass('actived');
                else $('.btnTransport').addClass('actived');
                $('#boxChooseCustomer').hide();
                $('#divCustomer').show();
            }
            else{
                showNotification(json.message, json.code);
                $('input#customerId').val('0');
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            $('input#customerId').val('0');
        }
    });
}

function keyUpInput() {
    var totalPrice = 0;
    $('#tbodyProduct').on('keyup', 'input.quantity', function () {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
        var tr = $(this).parent().parent();
        var price = replaceCost(tr.find('span.originalPrice').text(), true);
        value = replaceCost(value, true);
        var productPrices = tr.find('span.productPrices').text();
        if(productPrices != ''){
            var productId = tr.attr('data-id');
            var childid = tr.attr('data-child');
            productPrices = $.parseJSON(productPrices);
            for(var i = 0; i < productPrices.length; i++){
                if(productPrices[i].ProductId == productId && productPrices[i].ProductChildId == childid && value >= parseInt(productPrices[i].Quantity)) price = parseInt(productPrices[i].Price);
            }
        }
        tr.find('.spanPrice').text(formatDecimal(price.toString()));
        totalPrice = value * price;
        tr.find('input.sumPrice').val(formatDecimal(totalPrice.toString()));
        totalPrice = 0;
        $('#tbodyProduct tr').each(function () {
            totalPrice += replaceCost($(this).find('input.sumPrice').val(), true);
        });
        $('span#totalPrice').text(formatDecimal(totalPrice.toString()));
        calcPrice(totalPrice);
    }).on('keydown', 'input.cost', function (e) {
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8)) {
            e.preventDefault();
        }
    });
}

function calcPrice(totalPrice){
    var orderTypeId = parseInt($('select#orderTypeId').val());
    var offsetOrderTypeId = parseInt($('input#offsetOrderTypeId').val());
    if(orderTypeId != offsetOrderTypeId) {
        if (totalPrice == 0) {
            $('#tbodyProduct tr').each(function () {
                totalPrice += replaceCost($(this).find('input.sumPrice').val(), true);
            });
        }
    }
    else totalPrice = replaceCost($('span#totalCost').text(), true);
    $('span#totalPrice').text(formatDecimal(totalPrice.toString()));
    var isShip = $('input#deliveryTypeId').val() == '2';
    var orderCost = totalPrice;
    var cost = $('span#promotionCost').text();
    orderCost -= (cost == '-' ? 0 : replaceCost(cost, true));
    cost = $('span#transportCost').text();
    orderCost += (cost == '-' ? 0 : replaceCost(cost, true));
    var totalCost = orderCost;
    if(isShip) {
        cost = $('span#paymentCost').text();
        totalCost -= (cost == '-' ? 0 : replaceCost(cost, true));
    }
    var debitOtherTypeId = $('input#debitOtherTypeId').val();
    var serviceCost = 0;
    $('.divConfigExpand div.item').each(function(){
        serviceCost = replaceCost($(this).find('.spanOtherCost').text(), true);
        if($(this).attr('data-id') == debitOtherTypeId) totalCost -= serviceCost;
        else{
            orderCost += serviceCost;
            totalCost += serviceCost
        }
    });
    $('span#orderCost').text(formatDecimal(orderCost.toString()));
    totalCost = formatDecimal(totalCost.toString());
    if(orderTypeId == offsetOrderTypeId) totalCost = formatDecimal(totalPrice.toString());
    $('span#totalCost').text(totalCost);
    var flag = $('input#customerId').val() != '0' && isShip;
    if(orderTypeId != offsetOrderTypeId && totalPrice > 0 && flag) $('.btnTransport').removeClass('actived');
    else if(orderTypeId == offsetOrderTypeId && flag) $('.btnTransport').removeClass('actived');
    else $('.btnTransport').addClass('actived');
    if($('input#transportCODCost').length > 0) $('input#transportCODCost').val(totalCost);
}

function checkDebtCost(debtCost, msg){
    var ownCost = 0;
    var isShip = $('input#deliveryTypeId').val() == '2';
    if(isShip){
        var paymentCost = $('span#paymentCost').text();
        paymentCost = paymentCost == '-' ? 0 : replaceCost(paymentCost, true);
        ownCost = debtCost + paymentCost - replaceCost($('span#customerBalance').text(), true);
        if(ownCost > replaceCost($('input#debtCost1').val(), true)) {
            $('input#ownCost').val(formatDecimal(ownCost.toString()));
            showNotification(msg, 0);
            return false;
        }
    }
    else{
        ownCost = debtCost + replaceCost($('span#orderCost').text(), true) - replaceCost($('span#customerBalance').text(), true);
        if(ownCost > replaceCost($('input#debtCost1').val(), true)) {
            $('input#ownCost').val(formatDecimal(ownCost.toString()));
            showNotification(msg, 0);
            return false;
        }
    }
    return true;
}

function getListOrderProducts(){
    var products = [];
    $('#tbodyProduct tr').each(function () {
        products.push({
            ProductId: parseInt($(this).attr('data-id')),
            ProductChildId: parseInt($(this).attr('data-child')),
            Quantity: replaceCost($(this).find('input.quantity').val(), true),
            Price: replaceCost($(this).find('.spanPrice').text(), true),
            OriginalPrice: 0,
            DiscountReason: ''
        });
    });
    return products;
}

function renderTableProductExits(dataProductExits) {
    var data = null;
    var paginate =
        '<tr style="background: white">' +
        '<td colspan="2">' +
        '<ul class="pull-right controls" style="list-style-type: none;margin: 0px !important;padding: 0px!important;">' +
        '<li class="prev" style="display: inline-block;"><a {disable_prev} style="{style_disable_prev}margin: 5px;padding: 5px;border: 1px solid #EEEEEE"  href="javascript:;"> Prev </a></li>' +
        '<li class="next" style="display: inline-block;"><a {disable_next} style="{style_disable_next}padding: 5px;border: 1px solid #EEEEEE" href="javascript:;"> Next </a></li>' +
        '</ul>' +
        '</td>' +
        '</tr>';
    if (dataProductExits.data.length > 0) {
        data = dataProductExits.data[dataProductExits.index];
        if (dataProductExits.index == 0) {
            paginate = paginate.replace('{disable_prev}', 'disabled').replace('{style_disable_prev}', 'cursor:not-allowed;background :#eeeeee;');
            paginate = paginate.replace('{disable_next}', '').replace('{style_disable_next}', '');
        }
        else if (dataProductExits.index == dataProductExits.data.length - 1) {
            paginate = paginate.replace('{disable_next}', 'disabled').replace('{style_disable_next}', 'cursor:not-allowed;background :#eeeeee;');
            paginate = paginate.replace('{disable_prev}', '').replace('{style_disable_prev}', '');
        }
        else {
            paginate = paginate.replace('{disable_next}', '').replace('{style_disable_next}', '');
            paginate = paginate.replace('{disable_prev}', '').replace('{style_disable_prev}', '');
        }
    }
    else {
        data = dataProductExits.data;
        paginate = '';
    }
    $('#productExits').find('.title').text(data.StoreData.StoreName + ", " + data.StoreData.DistrictName + ", " + data.StoreData.ProvinceName + " (" + formatDecimal(data.DataProduct.distance.toString()) + "m)");
    var tbody = $('#productExitsTbody');
    var html = "";
    tbody.html('');
    for (var key in data.DataProduct) {
        if (!isNaN(key)) {
            var product = data.DataProduct[key];
            html += '<tr>' +
                '<td>' + product.ProductName + " " + (product.ProductNameChild == null ? '' : product.ProductNameChild) + '</td>' +
                '<td>' + product.product_quantity + '</td>' +
                '</tr>';
        }
    }
    html += paginate;
    tbody.html(html);
    tbody.find('.controls .prev a').click(function () {
        if (!$(this).is('[disabled]')) {
            dataProductExits.index = dataProductExits.index - 1 <= 0 ? 0 : dataProductExits.index - 1;
            renderTableProductExits(dataProductExits);
        }
    });

    tbody.find('.controls .next a').click(function () {
        if (!$(this).is('[disabled]')) {
            dataProductExits.index = dataProductExits.index + 1 >= dataProductExits.data.length ? dataProductExits.data.length - 1 : dataProductExits.index + 1;
            renderTableProductExits(dataProductExits);
        }
    });
}

function updateOrderField(orderId, fieldName, fieldValue, fnSuccess, fnError){
    $.ajax({
        type: "POST",
        url: $('input#updateFieldUrl').val(),
        data: {
            OrderId: orderId,
            FieldName: fieldName,
            FieldValue: fieldValue
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1){
                $('#ulActionLogs').prepend('<li>' + json.data.Comment + '<span class="time">' + getCurrentDateTime(2) + '</span></li>');
                fnSuccess(json.data);
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            fnError();
        }
    });
}