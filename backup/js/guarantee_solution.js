$(document).ready(function(){
    $("#tbodyGuaranteeSolution").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#guaranteeSolutionId').val(id);
        $('input#guaranteeSolutionName').val($('td#guaranteeSolutionName_' + id).text());
        $('select#parentGuaranteeSolutionId').val($('input#parentGuaranteeSolutionId_' + id).val());
        $('select#guaranteeSolutionTypeId').val($('input#guaranteeSolutionTypeId_' + id).val());
        scrollTo('input#guaranteeSolutionName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteGuaranteeSolutionUrl').val(),
                data: {
                    GuaranteeSolutionId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#guaranteeSolution_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#guaranteeSolutionForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#guaranteeSolutionForm')) {
            var form = $('#guaranteeSolutionForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        var parentGuaranteeSolutionName = '';
                        if(data.ParentGuaranteeSolutionId != '0') parentGuaranteeSolutionName = $('td#guaranteeSolutionName_' + data.ParentGuaranteeSolutionId).text();
                        if(data.IsAdd == 1){
                            var html = '<tr id="guaranteeSolution_' + data.GuaranteeSolutionId + '">';
                            html += '<td id="guaranteeSolutionName_' + data.GuaranteeSolutionId + '">' + data.GuaranteeSolutionName + '</td>';
                            html += '<td id="parentGuaranteeSolutionName_' + data.GuaranteeSolutionId + '">' + parentGuaranteeSolutionName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.GuaranteeSolutionId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.GuaranteeSolutionId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="parentGuaranteeSolutionId_' + data.GuaranteeSolutionId + '" value="' + data.ParentGuaranteeSolutionId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyGuaranteeSolution').prepend(html);
                            if(data.ParentGuaranteeSolutionId ==  '0') $('select#parentGuaranteeSolutionId').append('<option value="' + data.GuaranteeSolutionId + '">' + data.GuaranteeSolutionName + '</option>');
                        }
                        else{
                            $('td#guaranteeSolutionName_' + data.GuaranteeSolutionId).text(data.GuaranteeSolutionName);
                            $('td#parentGuaranteeSolutionName_' + data.GuaranteeSolutionId).text(parentGuaranteeSolutionName);
                            $('input#parentGuaranteeSolutionId_' + data.GuaranteeSolutionId).val(data.ParentGuaranteeSolutionId);
                            var option = $('select#parentGuaranteeSolutionId option[value="' + data.GuaranteeSolutionId + '"]');
                            if(data.ParentGuaranteeSolutionId == '0'){
                                if(option.length > 0) option.text(data.GuaranteeSolutionName);
                                else $('select#parentGuaranteeSolutionId').append('<option value="' + data.GuaranteeSolutionId + '">' + data.GuaranteeSolutionName + '</option>');
                            }
                            else if(option.length > 0) option.remove();
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});