$(document).ready(function(){
    $("#tbodyGuaranteeReason").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#guaranteeReasonId').val(id);
        $('input#guaranteeReasonName').val($('td#guaranteeReasonName_' + id).text());
        $('select#usageStatusId').val($('input#usageStatusId_' + id).val());
        scrollTo('input#guaranteeReasonName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteGuaranteeReasonUrl').val(),
                data: {
                    GuaranteeReasonId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#guaranteeReason_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#guaranteeReasonForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#guaranteeReasonForm')) {
            var form = $('#guaranteeReasonForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        var usageStatusName = '';
                        if(data.UsageStatusId != '0') usageStatusName = $('select#usageStatusId option[value="' + data.UsageStatusId + '"]').text();
                        if(data.IsAdd == 1){
                            var html = '<tr id="guaranteeReason_' + data.GuaranteeReasonId + '">';
                            html += '<td id="guaranteeReasonName_' + data.GuaranteeReasonId + '">' + data.GuaranteeReasonName + '</td>';
                            html += '<td id="usageStatusName_' + data.GuaranteeReasonId + '">' + usageStatusName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.GuaranteeReasonId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.GuaranteeReasonId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="usageStatusId_' + data.GuaranteeReasonId + '" value="' + data.UsageStatusId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyGuaranteeReason').prepend(html);
                        }
                        else{
                            $('td#guaranteeReasonName_' + data.GuaranteeReasonId).text(data.GuaranteeReasonName);
                            $('td#usageStatusName_' + data.GuaranteeReasonId).text(usageStatusName);
                            $('input#usageStatusId_' + data.GuaranteeReasonId).val(data.UsageStatusId);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});