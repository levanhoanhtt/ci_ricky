<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guaranteesolution extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Hướng xử lý Bảo hành đổi trả',
            array('scriptFooter' => array('js' => 'js/guarantee_solution.js'))
        );
        $listActions = $data['listActions'];
        if($this->Mactions->checkAccess($listActions, 'guaranteesolution')) {
            $this->load->model('Mguaranteesolutions');
            $data['listGuaranteeSolutions'] = $this->Mguaranteesolutions->getList();
            $this->load->view('setting/guarantee_solution', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('GuaranteeSolutionName', 'ParentGuaranteeSolutionId'));
        if(!empty($postData['GuaranteeSolutionName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $guaranteeSolutionId = $this->input->post('GuaranteeSolutionId');
            $this->load->model('Mguaranteesolutions');
            $flag = $this->Mguaranteesolutions->save($postData, $guaranteeSolutionId);
            if ($flag > 0) {
                $postData['GuaranteeSolutionId'] = $flag;
                $postData['IsAdd'] = ($guaranteeSolutionId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật hướng xử lý Bảo hành đổi trả thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $guaranteeSolutionId = $this->input->post('GuaranteeSolutionId');
        if($guaranteeSolutionId > 0){
            $this->load->model('Mguaranteesolutions');
            $flag = $this->Mguaranteesolutions->changeStatus(0, $guaranteeSolutionId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa hướng xử lý Bảo hành đổi trả thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}