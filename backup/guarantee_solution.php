<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('guaranteesolution/update', array('id' => 'guaranteeSolutionForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Hướng xử lý Bảo hành đổi trả</th>
                                <th>Hướng xử lý cha</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyGuaranteeSolution">
                            <?php
                            foreach($listGuaranteeSolutions as $bt){ ?>
                                <tr id="guaranteeSolution_<?php echo $bt['GuaranteeSolutionId']; ?>">
                                    <td id="guaranteeSolutionName_<?php echo $bt['GuaranteeSolutionId']; ?>"><?php echo $bt['GuaranteeSolutionName']; ?></td>
                                    <td id="parentGuaranteeSolutionName_<?php echo $bt['GuaranteeSolutionId']; ?>"><?php echo $bt['ParentGuaranteeSolutionId'] > 0 ? $this->Mconstants->getObjectValue($listGuaranteeSolutions, 'GuaranteeSolutionId', $bt['ParentGuaranteeSolutionId'], 'GuaranteeSolutionName') : ''; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $bt['GuaranteeSolutionId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $bt['GuaranteeSolutionId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="parentGuaranteeSolutionId_<?php echo $bt['GuaranteeSolutionId']; ?>" value="<?php echo $bt['ParentGuaranteeSolutionId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="guaranteeSolutionName" name="GuaranteeSolutionName" value="" data-field="Hướng xử lý Bảo hành đổi trả"></td>
                                <td>
                                    <select class="form-control" name="ParentGuaranteeSolutionId" id="parentGuaranteeSolutionId">
                                        <option value="0">--</option>
                                        <?php foreach($listGuaranteeSolutions as $bt1){
                                            if($bt1['ParentGuaranteeSolutionId'] == 0) echo '<option value="'.$bt1['GuaranteeSolutionId'].'">'.$bt1['GuaranteeSolutionName'].'</option>';
                                        } ?>
                                    </select>
                                </td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="GuaranteeSolutionId" id="guaranteeSolutionId" value="0" hidden="hidden">
                                    <input type="text" id="deleteGuaranteeSolutionUrl" value="<?php echo base_url('guaranteesolution/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>