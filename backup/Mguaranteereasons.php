<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mguaranteereasons extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "guaranteereasons";
        $this->_primary_key = "GuaranteeReasonId";
    }
}