<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Changefirstphone extends MY_Controller {
	// change phone number
    public $firstPhoneNumber = array(
        '0120'  => '070',
        '0121'  =>   '079',
        '0122'  =>  '077',
        '0126'  =>  '076',
        '0128'  =>  '078',
        '0123'  =>  '083',
        '0124'  =>  '084',
        '0125'  =>  '085',
        '0127'  =>  '081',
        '0129'  =>  '082',
        '0162'  =>  '032',
        '0163'  =>  '033',
        '0164'  =>  '034',
        '0165'  =>  '035',
        '0166'  =>  '036',
        '0167'  =>  '037',
        '0168'  =>  '038',
        '0169'  =>  '039',
        '0186'  =>  '056',
        '0188'  =>  '058',
        '0199'  =>  '059',
        '0992'  =>  '0672',
    );
	public function index(){
		/*$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Đổi đầu số điện thoại',
			array()
		);*/
		//if ($this->Mactions->checkAccess($data['listActions'], 'changefirstphone')) {
			$this->loadModel(array('Mcustomers','Mcustomeraddress'));
			$customers = $this->Mcustomers->getBy(array('StatusId >= ' => 0));
			$arrCustomer = array();
			foreach ($customers as $key => $c) {
				$phoneNumber = $c['PhoneNumber'];
				$subPhone = substr($phoneNumber,0, 4);
				if(!empty($this->firstPhoneNumber[$subPhone])){
					$phoneNumberNew = str_replace($subPhone,$this->firstPhoneNumber[$subPhone],$phoneNumber);
					$arrCustomer[] = array(
						'PhoneNumber' => $phoneNumberNew,
						'CustomerId' => $c['CustomerId']
						//'PhoneBak' => $c['PhoneNumber'],
					);
				}
                /*else{
					$arrCustomer[] = array(
						'PhoneNumber' => $c['PhoneNumber'],
						'CustomerId' => $c['CustomerId'],
						'PhoneBak' => $c['PhoneNumber'],
					);
				}*/
			}
			$customeraddress = $this->Mcustomeraddress->getBy(array('CustomerAddressId > ' => 0));
			$arrCustomeraddress = array();
			foreach ($customeraddress as $key => $ca) {
				$phoneNumberAd = $ca['PhoneNumber'];
				$subPhoneAd = substr($phoneNumberAd,0, 4);
				if(!empty($this->firstPhoneNumber[$subPhoneAd])){
					$phoneNumberNewAd = str_replace($subPhoneAd,$this->firstPhoneNumber[$subPhoneAd],$phoneNumberAd);
					$arrCustomeraddress[] = array(
						'PhoneNumber' => $phoneNumberNewAd,
						'CustomerAddressId' => $ca['CustomerAddressId'],
					);
				}
			}
			$flag = $this->db->update_batch('customers',$arrCustomer,'CustomerId');
			$flag2 = $this->db->update_batch('customeraddress',$arrCustomeraddress,'CustomerAddressId');
			if($flag) echo "update thành công customer";
			else echo "Update thất bại customer";

			if($flag2) echo "update thành công customeraddress";
			else echo "Update thất bại customeraddress";
		//} else $this->load->view('user/permission', $data);
	}
}