<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('guaranteereason/update', array('id' => 'guaranteeReasonForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Lý do bảo hành</th>
                                <th>Tình trạng sử dụng</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyGuaranteeReason">
                            <?php
                            foreach($listGuaranteeReasons as $bt){ ?>
                                <tr id="guaranteeReason_<?php echo $bt['GuaranteeReasonId']; ?>">
                                    <td id="guaranteeReasonName_<?php echo $bt['GuaranteeReasonId']; ?>"><?php echo $bt['GuaranteeReasonName']; ?></td>
                                    <td id="usageStatusName_<?php echo $bt['GuaranteeReasonId']; ?>"><?php echo $bt['UsageStatusId'] > 0 ? $this->Mconstants->getObjectValue($listUsageStatus, 'UsageStatusId', $bt['UsageStatusId'], 'UsageStatusName') : ''; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $bt['GuaranteeReasonId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $bt['GuaranteeReasonId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="usageStatusId_<?php echo $bt['GuaranteeReasonId']; ?>" value="<?php echo $bt['UsageStatusId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="guaranteeReasonName" name="GuaranteeReasonName" value="" data-field="Lý do bảo hành"></td>
                                <td><?php $this->Mconstants->selectObject($listUsageStatus, 'UsageStatusId', 'UsageStatusName', 'UsageStatusId', 0, true, '--Chọn--') ?></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="GuaranteeReasonId" id="guaranteeReasonId" value="0" hidden="hidden">
                                    <input type="text" id="deleteGuaranteeReasonUrl" value="<?php echo base_url('guaranteereason/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>