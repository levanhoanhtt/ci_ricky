<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guaranteereason extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Lý do bảo hành',
            array('scriptFooter' => array('js' => 'js/guarantee_reason.js'))
        );
        $listActions = $data['listActions'];
        if($this->Mactions->checkAccess($listActions, 'guaranteereason')) {
            $this->loadModel(array('Musagestatus', 'Mguaranteereasons'));
            $data['listUsageStatus'] = $this->Musagestatus->getBy(array('StatusId' => STATUS_ACTIVED, 'UsageStatusTypeId' => 2));
            $data['listGuaranteeReasons'] = $this->Mguaranteereasons->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/guarantee_reason', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('GuaranteeReasonName', 'UsageStatusId'));
        if(!empty($postData['GuaranteeReasonName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $guaranteeReasonId = $this->input->post('GuaranteeReasonId');
            $this->load->model('Mguaranteereasons');
            $flag = $this->Mguaranteereasons->save($postData, $guaranteeReasonId);
            if ($flag > 0) {
                $postData['GuaranteeReasonId'] = $flag;
                $postData['IsAdd'] = ($guaranteeReasonId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật lý do bảo hành thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $guaranteeReasonId = $this->input->post('GuaranteeReasonId');
        if($guaranteeReasonId > 0){
            $this->load->model('Mguaranteereasons');
            $flag = $this->Mguaranteereasons->changeStatus(0, $guaranteeReasonId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa lý do bảo hành thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}