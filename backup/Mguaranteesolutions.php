<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mguaranteesolutions extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "guaranteesolutions";
        $this->_primary_key = "GuaranteeSolutionId";
    }

    public function getList(){
        $retVal = array();
        $gs = $this->getBy(array('StatusId' => STATUS_ACTIVED));
        foreach($gs as $g){
            if($g['ParentGuaranteeSolutionId'] == 0){
                $retVal[] = $g;
                foreach($gs as $g1){
                    if($g1['ParentGuaranteeSolutionId'] == $g['GuaranteeSolutionId']) $retVal[] = $g1;
                }
            }
        }
        return $retVal;
    }
}